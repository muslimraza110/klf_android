
# Khoja Leadership Forum
<br />


## Requirements

- Android 
- Android Studio

## To Run App
Open the terminal and navigate to your project directory containing `Gradle` then execute:
```
install Gradle Dependency
```
Now open Android Studio and run your app.
(If you want to run on real device)

## Installation

Connect an Android device to your development machine.

## Android Studio

Select Run -> Run 'app' (or Debug 'app') from the menu bar
Select the device you wish to run the app on and click 'OK'

```bash

(These instructions were tested with Android Studio version 2.2.2, 2.2.3, 2.3, 2021.3.1)

Open Android Studio and select File->Open... or from the Android Launcher select Import project (Gradle) and navigate to the root directory of your project.
Select the directory or drill in and select the file build.gradle in the cloned repo.
Click 'OK' to open the the project in Android Studio.
A Gradle sync should start, but you can force a sync and build the 'app' module as needed.

```

## Installation Gradle

```bash
Install the debug APK on your device ./gradlew installDebug
```

## Keystore 

/app/KLF.jks

## Keystore Password
```bash
JKS PASSWORD
khojaleadership
```

## Authors

- [@KoderLabs](https://www.koderlabs.com/)



