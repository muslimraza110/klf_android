package com.khojaleadership.KLF.Fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.khojaleadership.KLF.Activities.Resources.News_Detail_Activity;
import com.khojaleadership.KLF.Adapter.Resource.News_Fragments_Adapter;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Resource.Add_Active_News_ToArchieve_RequestModel;
import com.khojaleadership.KLF.Model.Resource.Add_Active_News_ToArchieve_ResponseModel;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Data_Model;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class News_Active_Fragment extends Fragment implements RecyclerViewClickListner {

    RecyclerView recyclerView;
    News_Fragments_Adapter adapter;

    ArrayList<News_Fragments_Data_Model> list = new ArrayList<>();
    RecyclerViewClickListner listner;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    View view;

    ArrayList<News_Fragments_Data_Model> active_list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.initiative_active_fragment, container, false);
        listner = this;
        //initialize_view(v);

        view = v;

        loading_dialog = Common.LoadingDilaog(getActivity());
        loading_dialog.show();
        getNewsListFunction();
        //initialize_view(v);

        return v;

    }

    public void initialize_view(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.active_fragment_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //getData();
        //adapter = new News_Fragments_Adapter(getActivity(), listner, Common.news_active_fragment_list);
        adapter = new News_Fragments_Adapter(getActivity(), listner, active_list,true);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onRowClick(int position) {
        try {
            //Common.is_news_falg = true;
//            Common.news_list_data = Common.news_active_fragment_list.get(position);

            News_Fragments_Data_Model news_list_data=new News_Fragments_Data_Model();
            news_list_data = active_list.get(position);

            Intent detail = new Intent(getActivity(), News_Detail_Activity.class);
            detail.putExtra("NewsData",news_list_data);
            detail.putExtra("isNewsFlag","1");
            detail.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(detail);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onViewClcik(int position, View v) {

        try {
            if (v.getId() == R.id.add_to_archive_btn) {
                loading_dialog.show();
//                AddToArchieve(Common.news_active_fragment_list.get(position).getPosts__id());
                AddToArchieve(active_list.get(position).getPosts__id());
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }


    private void AddToArchieve(String post_id) {

        Add_Active_News_ToArchieve_RequestModel request = new Add_Active_News_ToArchieve_RequestModel();
        request.setPost_id(post_id);

        Call<Add_Active_News_ToArchieve_ResponseModel> responseCall = retrofitInterface.AddNewsToArchieve("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<Add_Active_News_ToArchieve_ResponseModel>() {
            @Override
            public void onResponse(Call<Add_Active_News_ToArchieve_ResponseModel> call, Response<Add_Active_News_ToArchieve_ResponseModel> response) {

                if (response.isSuccessful()) {

                    //loading_dialog.dismiss();
                    String status, message;

                    Add_Active_News_ToArchieve_ResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("success")) {
                        Toast.makeText(getActivity(), "News has been archived successfully", Toast.LENGTH_SHORT).show();
                        getNewsListFunction();
                    }


                } else {
                    loading_dialog.dismiss();

                    Log.d("NewsList", "reponse is not successfull.");
                }


            }

            @Override
            public void onFailure(Call<Add_Active_News_ToArchieve_ResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }


    private void getNewsListFunction() {

        try {
            Call<News_Fragments_Model> responseCall = retrofitInterface.getUpdatedNewsList("application/json", Common.auth_token);

            responseCall.enqueue(new Callback<News_Fragments_Model>() {
                @Override
                public void onResponse(Call<News_Fragments_Model> call, Response<News_Fragments_Model> response) {

                    if (response.isSuccessful()) {


                        String status, message;

                        News_Fragments_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        ArrayList<News_Fragments_Data_Model> list = new ArrayList<>();
                        list = responseModel.getData();

                        ArrayList<News_Fragments_Data_Model> local_active_list = new ArrayList<>();
                        if (list != null) {


//                            Common.news_active_fragment_list.clear();
//                            Common.news_archieve_fragment_list.clear();

                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).getPosts__archived().equals("0")) {
                                    if (Common.is_Admin_flag == true) {
//                                        Common.news_active_fragment_list.add(list.get(i));
                                        local_active_list.add(list.get(i));
                                    } else if (Common.is_Admin_flag == false) {
                                        if (list.get(i).getIs_published().equals("1")) {
                                    //        Common.news_active_fragment_list.add(list.get(i));
                                            local_active_list.add(list.get(i));
                                        }
                                    }

                                } else {
//                                    if (Common.is_Admin_flag == true) {
//                                        Common.news_archieve_fragment_list.add(list.get(i));
//                                    } else if (Common.is_Admin_flag == false) {
//                                        if (list.get(i).getIs_published().equals("1")) {
//                                            Common.news_archieve_fragment_list.add(list.get(i));
//                                        }
//                                    }
                                }

                            }

                            active_list=local_active_list;

//                            adapter.updateList(Common.news_active_fragment_list);
                            adapter.updateList(active_list);
                            loading_dialog.dismiss();

                        }


                    } else {
                        loading_dialog.dismiss();
                    }


                }

                @Override
                public void onFailure(Call<News_Fragments_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        initialize_view(view);
    }
}
