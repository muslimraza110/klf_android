package com.khojaleadership.KLF.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Activities.Group.EditGroup;
import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparator;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Contact_Layout.widget.SortAdapter;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Group_Models.DeleteGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.DeleteGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListModel;
import com.khojaleadership.KLF.Model.Group_Models.ListCatagoryDataModel;
import com.khojaleadership.KLF.Model.Group_Models.ListCatagoryModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Charitie_Group_Fragment extends Fragment implements RecyclerViewClickListner {


    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private SortAdapter adapter;
    private List<GetGroupListDataModel> sourceDataList;

    /**
     * 根据拼音来排列RecyclerView里面的数据类
     */
    private PinyinComparator pinyinComparator;

    List<GetGroupListDataModel> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;

    View view;

    List<GetGroupListDataModel> C_group_list=new ArrayList<>();
    GetGroupListDataModel group_data_detail=new GetGroupListDataModel();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.charitie_fragment, container, false);

        view=v;

        loading_dialog = Common.LoadingDilaog(getActivity());
        loading_dialog.show();
        onGroupBtnClick("C");
        //initViews(v);
        return v;
    }

    private void initViews(View v) {
        pinyinComparator = new PinyinComparator();

        sideBar = (SideBar) v.findViewById(R.id.sideBar);
        dialog = (TextView) v.findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

         sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                 int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        sourceDataList = filledData(C_group_list);

         Collections.sort(sourceDataList, pinyinComparator);

         manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        listner = this;
        adapter = new SortAdapter(getActivity(), sourceDataList, listner);
        mRecyclerView.setAdapter(adapter);


        mClearEditText = (ClearEditText) v.findViewById(R.id.filter_edit);

        mClearEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Common.hideKeyboard(v, getActivity());
                }
            }
        });

         mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }



    private List<GetGroupListDataModel> filledData(List<GetGroupListDataModel> data) {
        List<GetGroupListDataModel> mSortList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            GetGroupListDataModel sortModel = new GetGroupListDataModel();
            sortModel.setCreatedByUserID(data.get(i).getCreatedByUserID());
            sortModel.setGroups__id(data.get(i).getGroups__id());
            sortModel.setGroups__name(data.get(i).getGroups__name());
            sortModel.setGroups__category_id(data.get(i).getGroups__category_id());
            sortModel.setGroups__description(data.get(i).getGroups__description());
            sortModel.setGroups__email(data.get(i).getGroups__email());
            sortModel.setGroups__phone(data.get(i).getGroups__phone());

            sortModel.setGroups__address(data.get(i).getGroups__address());
            sortModel.setGroups__address2(data.get(i).getGroups__address2());
            sortModel.setGroups__website(data.get(i).getGroups__website());
            sortModel.setGroups__city(data.get(i).getGroups__city());
            sortModel.setGroups__region(data.get(i).getGroups__region());
            sortModel.setGroups__country(data.get(i).getGroups__country());

            sortModel.setGroups__postal_code(data.get(i).getGroups__postal_code());
            sortModel.setGroups__bio(data.get(i).getGroups__bio());
            sortModel.setGroups__hidden(data.get(i).getGroups__hidden());
            sortModel.setGroups__pending(data.get(i).getGroups__pending());
            sortModel.setGroups__vote_decision(data.get(i).getGroups__vote_decision());
            sortModel.setGroups__vote_decision_date(data.get(i).getGroups__vote_decision_date());

            sortModel.setGroups__created(data.get(i).getGroups__created());
            sortModel.setGroups__status(data.get(i).getGroups__status());
            sortModel.setGroups__donations(data.get(i).getGroups__donations());
            sortModel.setGroups__passive(data.get(i).getGroups__passive());
            sortModel.setCategories__id(data.get(i).getCategories__id());
            sortModel.setCategories__model(data.get(i).getCategories__model());

            sortModel.setCategories__name(data.get(i).getCategories__name());
            sortModel.setCategories__description(data.get(i).getCategories__description());
            sortModel.setIs_member(data.get(i).getIs_member());
            sortModel.setGroupimage(data.get(i).getGroupimage());
            //汉字转换成拼音
//            String pinyin = PinyinUtils.getPingYin(data[i]);
//            String sortString = pinyin.substring(0, 1).toUpperCase();

            String pinyin_english = data.get(i).getGroups__name();
            String sortString_english = pinyin_english.substring(0, 1).toUpperCase();

            if (sortString_english.matches("[A-Z]")) {
                sortModel.setLetters(sortString_english.toUpperCase());
            } else {
                sortModel.setLetters("#");
            }


            mSortList.add(sortModel);
        }
        return mSortList;
    }


    private void filterData(String filterStr) {
        filter_flag = true;


        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(C_group_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (GetGroupListDataModel sortModel : sourceDataList) {
                String name = sortModel.getGroups__name();

                if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().contains(filterStr)
                        || name.toUpperCase().contains(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
         Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }


    @Override
    public void onRowClick(int position) {

        try {

            if (filter_flag == true) {
                group_data_detail = filterDataList.get(position);
            } else {
                group_data_detail = sourceDataList.get(position);

            }

            Intent i = new Intent(getActivity(), GroupsDetail.class);
            i.putExtra("group_data_detail",group_data_detail);
            i.putExtra("isAllGroupDetail","0");
            getActivity().startActivity(i);
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    @Override
    public void onViewClcik(int position, View v) {
        try {

            if (filter_flag == true) {
                group_data_detail = filterDataList.get(position);
            } else {
                group_data_detail = sourceDataList.get(position);
            }


            if (v.getId() == R.id.group_edit_btn) {

                if (sourceDataList.get(position) != null) {

                    loading_dialog.show();
                    onListCatagoryBtnClick();
                }


            } else if (v.getId() == R.id.group_delete_btn) {

                Confirm_Dialog_Delete("Delete Group", "Are you sure to delete this group?", "Yes");


            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    public void Confirm_Dialog_Delete(String title, String message, String btn) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                DeleteGroupBtnClick();

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void onListCatagoryBtnClick() {

        try {

            Call<ListCatagoryModel> call = retrofitInterface.getCatagoryList("application/json",Common.auth_token);

            call.enqueue(new Callback<ListCatagoryModel>() {
                @Override
                public void onResponse(Call<ListCatagoryModel> call, Response<ListCatagoryModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        ListCatagoryModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                            loading_dialog.dismiss();

                            ArrayList<ListCatagoryDataModel> list = responseModel.getData();
                            //Common.list_of_catagory = list;

                           // ArrayList<String> cat_list = new ArrayList<>();
                            for (int i = 0; i < list.size(); i++) {
//                                cat_list.add(list.get(i).getName());
                                Common.cat_list.add(list.get(i).getName());
                            }

                            Intent i = new Intent(getActivity(), EditGroup.class);
                            i.putExtra("group_data_detail",group_data_detail);
                           // i.putExtra("group_data_detail",group_data_detail);
                            startActivity(i);
                        }

                    }
                }

                @Override
                public void onFailure(Call<ListCatagoryModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }

            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    private void DeleteGroupBtnClick() {

        try {

            DeleteGroupRequestModel requestModel = new DeleteGroupRequestModel();
            requestModel.setGroup_id(group_data_detail.getGroups__id());


            Call<DeleteGroupResponseModel> call = retrofitInterface.DeleteGroup("application/json",Common.auth_token, requestModel);


            call.enqueue(new Callback<DeleteGroupResponseModel>() {
                @Override
                public void onResponse(Call<DeleteGroupResponseModel> call, Response<DeleteGroupResponseModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        DeleteGroupResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                          onGroupBtnClick("C");
                            Toast.makeText(getActivity(), "Group deleted successfully", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<DeleteGroupResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }

            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    private void onGroupBtnClick(final String type) {

        try {

            Call<GetGroupListModel> call = retrofitInterface.getGroups("application/json",Common.auth_token,
                    type, 1, 1000, Common.login_data.getData().getId());
            call.enqueue(new Callback<GetGroupListModel>() {
                @Override
                public void onResponse(Call<GetGroupListModel> call, Response<GetGroupListModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;
                        GetGroupListModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GetGroupListDataModel> list = new ArrayList<>();
                        list = responseModel.getData();



                        if (list != null) {
                            //pd.dismiss();
                            loading_dialog.dismiss();
                            C_group_list = list;


                            initViews(view);

                        }


                     }
                }

                @Override
                public void onFailure(Call<GetGroupListModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }
}
