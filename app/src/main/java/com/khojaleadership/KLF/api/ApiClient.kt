package com.khojaleadership.KLF.api

import com.khojaleadership.KLF.Helper.Common
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

        val mockApi = "https://5f7ded36834b5c0016b06cd2.mockapi.io/KLF/API/klf_api/"
        val LOCAL_SERVER = "http://172.16.202.77:85/KLF/API/klf_api/"
//    const val ngrokUrl = "https://stgapi.khojaleadershipforum.org/"




    fun getEventsApi(authToken: String): EventsApi{
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(HeaderInterceptor(authToken))
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(Common.LIVE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()
        return retrofit.create(EventsApi::class.java)
    }


    val mockEventApi: EventsApi
        get() {
            val httpClient = OkHttpClient.Builder()
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(mockApi)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
            return retrofit.create(EventsApi::class.java)
        }

}

class HeaderInterceptor(val authToken: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
                request()
                        .newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Auth-token", authToken)
                        .build()
        )
    }
}