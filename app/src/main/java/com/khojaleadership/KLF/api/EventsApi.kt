package com.khojaleadership.KLF.api

import com.khojaleadership.KLF.Helper.Response
import com.khojaleadership.KLF.Model.event_new.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface EventsApi {

    // REAL ENDPOINTS
    @GET("summitEvents/get_current_summit_events.json")
    suspend fun getCurrentEventsList(
            @Query("user_id") userId: String
    ): Response<List<SingleEventResponse>>

    @GET("summitEvents/get_previous_summit_events.json")
    suspend fun getPreviousEventsList(
            @Query("user_id") userId: String
    ): Response<List<SingleEventResponse>>

    @GET("summitEvents/get_summit_events_sections.json")
        suspend fun getEventSections(
            @Query("summit_event_id") eventId: String
    ): Response<List<EventSectionsResponse>>

    //    @Headers(
//            "Content-Type: application/json"
//    )
    @GET("summitEvents/get_summit_events_programs.json")
    suspend fun getProgrammeDetails(
            @Query("summit_events_id") eventId: String,
            @Query("section_name") sectionTag: String,
            @Query("user_id") userId: String
//            @Header("Auth-token") authToken: String
    ): Response<ProgrammeDetailResponse>

    @GET("summitEvents/get_summit_events_programs.json")
    suspend fun getMyItineraryData(
            @Query("summit_events_id") eventId: String,
            @Query("user_id") userId: String
    )   : Response<ProgrammeDetailResponse>




    @POST("summitEvents/set_summit_events_registration.json")
    suspend fun setSummitEventsRegistration(
        @Body summitEventsRegistrationRequest: SummitEventsRegistrationRequest
    ): Response<SummitEventsRegistrationResponse>


    @POST("summitEvents/set_summit_event_program_detail_check_my_availability.json")
    suspend fun setSummitEventProgramCheckMyAvailability(
            @Body changeAvailabilityRequest: ChangeAvailabilityRequest
    ): Response<Any>

    @POST("summitEvents/set_summit_events_meeting_request.json")
    suspend fun setSummitEventMeetingRequestStatus(
            @Body meetingStatusSetRequest: MeetingStatusSetRequest
    ): Response<MeetingStatusSetResponse>


    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventSpeakersDelegatesForProgramDetailsId(
            @Query("summit_events_id") eventId: String,
            @Query("summit_events_program_details_id") programDetailsId: String,
            @Query("check_attendee_availibility") attendeeAvailabilityStatus: String,
            @Query("user_id") user_id: String
//            @Query("is_speaker") isSpeaker: String,
//            @Query("is_delegate") isDelegate: String
    ): Response<SummitEventPeopleListResponse>

    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventSpeakersDelegatesForProgramDetailsIdWithSearch(
            @Query("summit_events_id") eventId: String,
            @Query("summit_events_program_details_id") programDetailsId: String,
            @Query("search") searchText: String,
            @Query("check_attendee_availibility") attendeeAvailabilityStatus: String,
            @Query("user_id") user_id: String
    ): Response<SummitEventPeopleListResponse>


    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventSpeakers(
            @Query("summit_events_id") eventId: String,
            @Query("user_id") user_id: String,
            @Query("is_speaker") isSpeaker: String
    ): Response<SummitEventPeopleListResponse>

    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventSpeakersWithSearch(
            @Query("summit_events_id") eventId: String,
            @Query("p") searchText: String,
            @Query("user_id") user_id: String,
            @Query("is_speaker") isSpeaker: String
    ): Response<SummitEventPeopleListResponse>


    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventDelegates(
            @Query("summit_events_id") eventId: String,
            @Query("user_id") user_id: String,
            @Query("is_delegate") isDelegate: String
    ): Response<SummitEventPeopleListResponse>

    @GET("summitEvents/get_summit_event_faculty_users.json")
    suspend fun getSummitEventDelegatesWithSearch(
            @Query("summit_events_id") eventId: String,
            @Query("search") searchText: String,
            @Query("user_id") user_id: String,
            @Query("is_delegate") isDelegate: String
    ): Response<SummitEventPeopleListResponse>


    @GET("summitEvents/set_summit_event_breakout_session_registration.json")
    suspend fun joinBreakoutSession(
            @Query("summit_events_faculty_id") facultyId: String,
            @Query("summit_events_program_details_sessions_id") sessionId: String,
            @Query("enrolled") enroll: String
    ): Response<Any>


    @POST("summitEvents/set_summit_events_meeting_request_to_delegates.json")
    suspend fun setMeetingRequest(
            @Body setMeetingRequestBody: SetMeetingRequest
    ): Response<Any>

    @POST("summitEvents/update_summit_events_meeting_delegates.json")
    suspend fun updateWholeMeetingRequest(
            @Body updateMeetingRequestBody: UpdateWholeMeetingRequest
    ): Response<Any>

    @POST("summitEvents/update_summit_events_meeting_request_to_delegates_status.json")
    suspend fun updateMeetingStatus(
            @Body updateMeetingRequest: UpdateMeetingRequest
    ): Response<Any>

    @POST("summitEvents/update_summit_events_meeting_request_to_delegates_notes.json")
    suspend fun updateMeetingNotes(
            @Body updateMeetingNotesRequest: UpdateMeetingNotesRequest
    ): Response<Any>

    @POST("summitEvents/cancel_summit_events_meeting_request_to_delegates.json")
    suspend fun cancelMeeting(
            @Body cancelMeetingRequest: CancelMeetingRequest
    ): Response<Any>

    @GET("summitEvents/get_summit_events_meeting_request_to_delegates.json")
    suspend fun getMeetings(
            @Query("summit_events_id") eventId: String,
            @Query("user_faculty_id") facultyId: String,
            @Query("user_id") userId: String
    ): Response<List<MeetingsResponse>>


    // MOCK ENDPOINTS
//    @GET("get_current_summit_events")
//    suspend fun getEventsList(): Response<List<SingleEventResponse>>
//
//    @GET("get_summit_events_sections")
//    suspend fun getEventSections(
//            @Query("summit_event_id") eventId: String
//    ): Response<List<EventSectionsResponse>>
//
//    //    @Headers(
////            "Content-Type: application/json"
////    )
//    @GET("get_summit_events_programs")
//    suspend fun getProgrammeDetails(
//            @Query("summit_events_id") eventId: String,
//            @Query("section_name") sectionTag: String,
//            @Query("user_id") userId: String
////            @Header("Auth-token") authToken: String
//    ): Response<ProgrammeDetailResponse>
//
//    @GET("get_summit_events_programs")
//    suspend fun getMyItineraryData(
//            @Query("summit_events_id") eventId: String,
//            @Query("user_id") userId: String
//    ): Response<ProgrammeDetailResponse>
//
//
//    @POST("set_summit_event_program_detail_check_my_availability.json")
//    suspend fun setSummitEventProgramCheckMyAvailability(
//            @Body changeAvailabilityRequest: ChangeAvailabilityRequest
//    ) : Response<ChangeAvailabilityResponse>
//


//    @GET("delegates")
//    suspend fun getDelegates(): Response<List<SummitEventPeopleListResponse>>
//
//    @GET("delegates")
//    suspend fun getSpeakers(): Response<List<SpeakerListResponse>>


    @GET("summit_event_notifications")
    suspend fun getPlannerNotifications(
            //TODO
            @Query("____") userId: String,
            @Query("____") eventId: String
    ): Response<List<PlannerNotificationsResponse>>

}