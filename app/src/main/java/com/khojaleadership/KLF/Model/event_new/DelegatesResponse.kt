package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class SummitEventPeopleListResponse(
//        @SerializedName("sortBy") val sortBy: String?,
//        @SerializedName("peoples") val list: List<PersonDetailResponse?>?
        @SerializedName("recently_added") val recentlyAdded: List<PersonDetailResponse?>?,
        @SerializedName("all_country") val allCountry: List<PersonDetailResponse?>?,
        @SerializedName("all_industry") val allIndustry: List<PersonDetailResponse?>?,
        @SerializedName("all_users_by_type") val all: List<PersonDetailResponse?>?
)

data class DelegateResponse(
        @SerializedName("image") val image: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("designation") val designation: String?,
        @SerializedName("short_bio") val shortBio: String?,
        @SerializedName("twitter_url") val twitterUrl: String?,
        @SerializedName("facebook_url") val facebookUrl: String?
)