package com.khojaleadership.KLF.Model.Resource;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Khoja_Summit_Model implements Parcelable {

    String status,message;
    ArrayList<Khoja_Summit_Data_Model> data;

    public Khoja_Summit_Model() {
    }

    public Khoja_Summit_Model(String status, String message, ArrayList<Khoja_Summit_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected Khoja_Summit_Model(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<Khoja_Summit_Model> CREATOR = new Creator<Khoja_Summit_Model>() {
        @Override
        public Khoja_Summit_Model createFromParcel(Parcel in) {
            return new Khoja_Summit_Model(in);
        }

        @Override
        public Khoja_Summit_Model[] newArray(int size) {
            return new Khoja_Summit_Model[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Khoja_Summit_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<Khoja_Summit_Data_Model> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
