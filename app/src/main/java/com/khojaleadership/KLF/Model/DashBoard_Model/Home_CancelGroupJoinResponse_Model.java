package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_CancelGroupJoinResponse_Model {
    String status,message;

    public Home_CancelGroupJoinResponse_Model() {
    }

    public Home_CancelGroupJoinResponse_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
