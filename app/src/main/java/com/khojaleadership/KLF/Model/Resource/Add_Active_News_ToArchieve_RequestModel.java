package com.khojaleadership.KLF.Model.Resource;

public class Add_Active_News_ToArchieve_RequestModel {

    String post_id;

    public Add_Active_News_ToArchieve_RequestModel() {
    }

    public Add_Active_News_ToArchieve_RequestModel(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
