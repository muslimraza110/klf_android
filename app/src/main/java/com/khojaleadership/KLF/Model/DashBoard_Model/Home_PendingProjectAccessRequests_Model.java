package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_PendingProjectAccessRequests_Model {
    String Projects__name;
    String ProjectRequests__id,ProjectRequests__project_id;


    public Home_PendingProjectAccessRequests_Model() {
    }

    public Home_PendingProjectAccessRequests_Model(String projects__name, String projectRequests__id, String projectRequests__project_id) {
        Projects__name = projects__name;
        ProjectRequests__id = projectRequests__id;
        ProjectRequests__project_id = projectRequests__project_id;
    }

    public String getProjectRequests__project_id() {
        return ProjectRequests__project_id;
    }

    public void setProjectRequests__project_id(String projectRequests__project_id) {
        ProjectRequests__project_id = projectRequests__project_id;
    }

    public String getProjectRequests__id() {
        return ProjectRequests__id;
    }

    public void setProjectRequests__id(String projectRequests__id) {
        ProjectRequests__id = projectRequests__id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }
}
