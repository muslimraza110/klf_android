package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class AddQouteResponseModel {

    String status,message;

    public AddQouteResponseModel() {
    }

    public AddQouteResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
