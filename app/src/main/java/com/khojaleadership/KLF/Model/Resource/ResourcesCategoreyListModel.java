package com.khojaleadership.KLF.Model.Resource;

import java.util.ArrayList;

public class ResourcesCategoreyListModel {
    String status,message;
    ArrayList<ResourcesCategoreyListDataModel> data;

    public ResourcesCategoreyListModel() {
    }

    public ResourcesCategoreyListModel(String status, String message, ArrayList<ResourcesCategoreyListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ResourcesCategoreyListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<ResourcesCategoreyListDataModel> data) {
        this.data = data;
    }
}

