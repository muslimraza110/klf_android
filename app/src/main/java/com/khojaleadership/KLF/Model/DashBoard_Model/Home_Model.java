package com.khojaleadership.KLF.Model.DashBoard_Model;

import java.util.ArrayList;

public class Home_Model {
    String status,message;

    ArrayList<Home_PendingGroupAccessRequests_Model> PendingGroupAccessRequests;
    ArrayList<Home_PendingProjectAccessRequests_Model> PendingProjectAccessRequests;
    ArrayList<Home_ForumsJointheDiscussion_Model> ForumsJointheDiscussion;
    ArrayList<Home_ForumSubscriptions_Model> ForumSubscriptions;
    ArrayList<Home_Initiative_Model> Initiatives;

    public Home_Model() {
    }

    public Home_Model(String status, String message, ArrayList<Home_PendingGroupAccessRequests_Model> pendingGroupAccessRequests, ArrayList<Home_PendingProjectAccessRequests_Model> pendingProjectAccessRequests, ArrayList<Home_ForumsJointheDiscussion_Model> forumsJointheDiscussion, ArrayList<Home_ForumSubscriptions_Model> forumSubscriptions, ArrayList<Home_Initiative_Model> initiatives) {
        this.status = status;
        this.message = message;
        PendingGroupAccessRequests = pendingGroupAccessRequests;
        PendingProjectAccessRequests = pendingProjectAccessRequests;
        ForumsJointheDiscussion = forumsJointheDiscussion;
        ForumSubscriptions = forumSubscriptions;
        Initiatives = initiatives;
    }

    public ArrayList<Home_Initiative_Model> getInitiatives() {
        return Initiatives;
    }

    public void setInitiatives(ArrayList<Home_Initiative_Model> initiatives) {
        Initiatives = initiatives;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Home_PendingGroupAccessRequests_Model> getPendingGroupAccessRequests() {
        return PendingGroupAccessRequests;
    }

    public void setPendingGroupAccessRequests(ArrayList<Home_PendingGroupAccessRequests_Model> pendingGroupAccessRequests) {
        PendingGroupAccessRequests = pendingGroupAccessRequests;
    }

    public ArrayList<Home_PendingProjectAccessRequests_Model> getPendingProjectAccessRequests() {
        return PendingProjectAccessRequests;
    }

    public void setPendingProjectAccessRequests(ArrayList<Home_PendingProjectAccessRequests_Model> pendingProjectAccessRequests) {
        PendingProjectAccessRequests = pendingProjectAccessRequests;
    }

    public ArrayList<Home_ForumsJointheDiscussion_Model> getForumsJointheDiscussion() {
        return ForumsJointheDiscussion;
    }

    public void setForumsJointheDiscussion(ArrayList<Home_ForumsJointheDiscussion_Model> forumsJointheDiscussion) {
        ForumsJointheDiscussion = forumsJointheDiscussion;
    }

    public ArrayList<Home_ForumSubscriptions_Model> getForumSubscriptions() {
        return ForumSubscriptions;
    }

    public void setForumSubscriptions(ArrayList<Home_ForumSubscriptions_Model> forumSubscriptions) {
        ForumSubscriptions = forumSubscriptions;
    }
}
