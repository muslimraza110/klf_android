package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailMembersListDataModel {
    String GroupsUsers__user_id,GroupsUsers__id,GroupsUsers__group_id,GroupsUsers__admin,Users__id;
    String Users__role,Users__email,Users__password,Users__title,Users__first_name,Users__last_name;
    String ProfileImg__name;

    public GroupDetailMembersListDataModel() {
    }

    public GroupDetailMembersListDataModel(String groupsUsers__user_id, String groupsUsers__id, String groupsUsers__group_id, String groupsUsers__admin, String users__id, String users__role, String users__email, String users__password, String users__title, String users__first_name, String users__last_name, String profileImg__name) {
        GroupsUsers__user_id = groupsUsers__user_id;
        GroupsUsers__id = groupsUsers__id;
        GroupsUsers__group_id = groupsUsers__group_id;
        GroupsUsers__admin = groupsUsers__admin;
        Users__id = users__id;
        Users__role = users__role;
        Users__email = users__email;
        Users__password = users__password;
        Users__title = users__title;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        ProfileImg__name = profileImg__name;
    }

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }

    public String getGroupsUsers__user_id() {
        return GroupsUsers__user_id;
    }

    public void setGroupsUsers__user_id(String groupsUsers__user_id) {
        GroupsUsers__user_id = groupsUsers__user_id;
    }

    public String getGroupsUsers__id() {
        return GroupsUsers__id;
    }

    public void setGroupsUsers__id(String groupsUsers__id) {
        GroupsUsers__id = groupsUsers__id;
    }

    public String getGroupsUsers__group_id() {
        return GroupsUsers__group_id;
    }

    public void setGroupsUsers__group_id(String groupsUsers__group_id) {
        GroupsUsers__group_id = groupsUsers__group_id;
    }

    public String getGroupsUsers__admin() {
        return GroupsUsers__admin;
    }

    public void setGroupsUsers__admin(String groupsUsers__admin) {
        GroupsUsers__admin = groupsUsers__admin;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__password() {
        return Users__password;
    }

    public void setUsers__password(String users__password) {
        Users__password = users__password;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }
}
