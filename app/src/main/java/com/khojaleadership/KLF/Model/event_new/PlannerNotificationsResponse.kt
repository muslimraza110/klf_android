package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class PlannerNotificationsResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("date_time") val dateTime: String?,
        @SerializedName("identifier") val identifier: String?,
        @SerializedName("data") val data: Data?
)

data class Data(
        @SerializedName("event_day_index") val event_day_index: Int?
)