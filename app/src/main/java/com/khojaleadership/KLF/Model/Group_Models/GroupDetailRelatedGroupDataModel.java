package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailRelatedGroupDataModel {

    String group_image,id,category_id,name;



    public GroupDetailRelatedGroupDataModel() {
    }

    public GroupDetailRelatedGroupDataModel(String group_image, String id, String category_id, String name) {
        this.group_image = group_image;
        this.id = id;
        this.category_id = category_id;
        this.name = name;
    }

    public String getGroup_image() {
        return group_image;
    }

    public void setGroup_image(String group_image) {
        this.group_image = group_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
