package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_Expandable_Child_Model {


    Boolean is_group_access_request;
    Boolean is_project_access_request;
    public String Child_Model;
    public String id;
    int parent_number;

    public Home_Expandable_Child_Model() {
    }

    public Home_Expandable_Child_Model(Boolean is_group_access_request, Boolean is_project_access_request, String child_Model, String id, int parent_number) {
        this.is_group_access_request = is_group_access_request;
        this.is_project_access_request = is_project_access_request;
        Child_Model = child_Model;
        this.id = id;
        this.parent_number = parent_number;
    }

    public int getParent_number() {
        return parent_number;
    }

    public void setParent_number(int parent_number) {
        this.parent_number = parent_number;
    }

    public Boolean getIs_project_access_request() {
        return is_project_access_request;
    }

    public void setIs_project_access_request(Boolean is_project_access_request) {
        this.is_project_access_request = is_project_access_request;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIs_group_access_request() {
        return is_group_access_request;
    }

    public void setIs_group_access_request(Boolean is_group_access_request) {
        this.is_group_access_request = is_group_access_request;
    }

    public String getChild_Model() {
        return Child_Model;
    }

    public void setChild_Model(String child_Model) {
        Child_Model = child_Model;
    }
}
