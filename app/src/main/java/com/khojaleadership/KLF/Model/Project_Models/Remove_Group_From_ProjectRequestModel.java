package com.khojaleadership.KLF.Model.Project_Models;

public class Remove_Group_From_ProjectRequestModel {
    String group_id,project_id;

    public Remove_Group_From_ProjectRequestModel() {
    }

    public Remove_Group_From_ProjectRequestModel(String group_id, String project_id) {
        this.group_id = group_id;
        this.project_id = project_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }
}
