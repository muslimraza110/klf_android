package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_PendingGroupAccessRequests_Model {
    String Groups__name;
    String GroupRequests__id;

    public Home_PendingGroupAccessRequests_Model() {
    }

    public Home_PendingGroupAccessRequests_Model(String groups__name, String groupRequests__id) {
        Groups__name = groups__name;
        GroupRequests__id = groupRequests__id;
    }

    public String getGroupRequests__id() {
        return GroupRequests__id;
    }

    public void setGroupRequests__id(String groupRequests__id) {
        GroupRequests__id = groupRequests__id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }
}

