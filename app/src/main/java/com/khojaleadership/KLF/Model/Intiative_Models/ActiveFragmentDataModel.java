package com.khojaleadership.KLF.Model;

public class ActiveFragmentDataModel {
    String Initiatives__id, Initiatives__user_id, Initiatives__name, Initiatives__description, Initiatives__show_on_dashboard, Initiatives__broadcast;
    String Initiatives__email_broadcast,Initiatives__created, Users__first_name, Users__last_name,Amount,Fundraisings__amount,amount_collected;


    public ActiveFragmentDataModel() {
    }

    public ActiveFragmentDataModel(String initiatives__id, String initiatives__user_id, String initiatives__name, String initiatives__description, String initiatives__show_on_dashboard, String initiatives__broadcast, String initiatives__email_broadcast, String initiatives__created, String users__first_name, String users__last_name, String amount, String fundraisings__amount, String amount_collected) {
        Initiatives__id = initiatives__id;
        Initiatives__user_id = initiatives__user_id;
        Initiatives__name = initiatives__name;
        Initiatives__description = initiatives__description;
        Initiatives__show_on_dashboard = initiatives__show_on_dashboard;
        Initiatives__broadcast = initiatives__broadcast;
        Initiatives__email_broadcast = initiatives__email_broadcast;
        Initiatives__created = initiatives__created;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        Amount = amount;
        Fundraisings__amount = fundraisings__amount;
        this.amount_collected = amount_collected;
    }

    public String getInitiatives__show_on_dashboard() {
        return Initiatives__show_on_dashboard;
    }

    public void setInitiatives__show_on_dashboard(String initiatives__show_on_dashboard) {
        Initiatives__show_on_dashboard = initiatives__show_on_dashboard;
    }

    public String getInitiatives__broadcast() {
        return Initiatives__broadcast;
    }

    public void setInitiatives__broadcast(String initiatives__broadcast) {
        Initiatives__broadcast = initiatives__broadcast;
    }

    public String getInitiatives__email_broadcast() {
        return Initiatives__email_broadcast;
    }

    public void setInitiatives__email_broadcast(String initiatives__email_broadcast) {
        Initiatives__email_broadcast = initiatives__email_broadcast;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getFundraisings__amount() {
        return Fundraisings__amount;
    }

    public void setFundraisings__amount(String fundraisings__amount) {
        Fundraisings__amount = fundraisings__amount;
    }

    public String getAmount_collected() {
        return amount_collected;
    }

    public void setAmount_collected(String amount_collected) {
        this.amount_collected = amount_collected;
    }

    public String getInitiatives__id() {
        return Initiatives__id;
    }

    public void setInitiatives__id(String initiatives__id) {
        Initiatives__id = initiatives__id;
    }

    public String getInitiatives__user_id() {
        return Initiatives__user_id;
    }

    public void setInitiatives__user_id(String initiatives__user_id) {
        Initiatives__user_id = initiatives__user_id;
    }

    public String getInitiatives__name() {
        return Initiatives__name;
    }

    public void setInitiatives__name(String initiatives__name) {
        Initiatives__name = initiatives__name;
    }

    public String getInitiatives__description() {
        return Initiatives__description;
    }

    public void setInitiatives__description(String initiatives__description) {
        Initiatives__description = initiatives__description;
    }

    public String getInitiatives__created() {
        return Initiatives__created;
    }

    public void setInitiatives__created(String initiatives__created) {
        Initiatives__created = initiatives__created;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }
}
