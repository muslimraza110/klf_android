package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class DeletePostRequestModel {
    String forum_post_id;

    public DeletePostRequestModel() {
    }

    public DeletePostRequestModel(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }

    public String getForum_post_id() {
        return forum_post_id;
    }

    public void setForum_post_id(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }
}
