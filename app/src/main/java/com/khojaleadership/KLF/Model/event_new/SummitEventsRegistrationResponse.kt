package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName


data class SummitEventsRegistrationResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("user_id") val userId: String?,
        @SerializedName("summit_event_id") val summitEventId: String?,
        @SerializedName("is_paid") val isPaid: String?


)