package com.khojaleadership.KLF.Model.Contact;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetContactListModel implements Parcelable {
    String status,message;
    ArrayList<GetContactDataListModel> data;

    public GetContactListModel() {
    }

    public GetContactListModel(String status, String message, ArrayList<GetContactDataListModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected GetContactListModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetContactListModel> CREATOR = new Creator<GetContactListModel>() {
        @Override
        public GetContactListModel createFromParcel(Parcel in) {
            return new GetContactListModel(in);
        }

        @Override
        public GetContactListModel[] newArray(int size) {
            return new GetContactListModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetContactDataListModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetContactDataListModel> data) {
        this.data = data;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
