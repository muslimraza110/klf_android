package com.khojaleadership.KLF.Model.Project_Models;

public class GetAlreadyGroup_ProjectDataModel {

    private String letters;//Display the initials of the pinyin
    String name,project_id,group_id,IsSelected;
    String profileimg;
//    int position_for_already;

    public GetAlreadyGroup_ProjectDataModel() {
    }

    public GetAlreadyGroup_ProjectDataModel(String letters, String name, String project_id, String group_id, String isSelected, String profileimg) {
        this.letters = letters;
        this.name = name;
        this.project_id = project_id;
        this.group_id = group_id;
        IsSelected = isSelected;
        this.profileimg = profileimg;
    }


    public String getProfileimg() {
        return profileimg;
    }

    public void setProfileimg(String profileimg) {
        this.profileimg = profileimg;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getIsSelected() {
        return IsSelected;
    }

    public void setIsSelected(String isSelected) {
        IsSelected = isSelected;
    }
}
