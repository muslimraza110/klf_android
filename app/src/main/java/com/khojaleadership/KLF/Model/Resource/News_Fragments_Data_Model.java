package com.khojaleadership.KLF.Model.Resource;

import android.os.Parcel;
import android.os.Parcelable;

public class News_Fragments_Data_Model implements Parcelable {
    String Posts__id,Posts__user_id,Posts__category_id,Posts__post_theme_id,Posts__topic,Posts__name,Posts__content;
    String Posts__archived,Posts__created,is_published,profileimage,Posts__published;


    public News_Fragments_Data_Model() {
    }


    public News_Fragments_Data_Model(String posts__id, String posts__user_id, String posts__category_id, String posts__post_theme_id, String posts__topic, String posts__name, String posts__content, String posts__archived, String posts__created, String is_published, String profileimage, String posts__published) {
        Posts__id = posts__id;
        Posts__user_id = posts__user_id;
        Posts__category_id = posts__category_id;
        Posts__post_theme_id = posts__post_theme_id;
        Posts__topic = posts__topic;
        Posts__name = posts__name;
        Posts__content = posts__content;
        Posts__archived = posts__archived;
        Posts__created = posts__created;
        this.is_published = is_published;
        this.profileimage = profileimage;
        Posts__published = posts__published;
    }

    protected News_Fragments_Data_Model(Parcel in) {
        Posts__id = in.readString();
        Posts__user_id = in.readString();
        Posts__category_id = in.readString();
        Posts__post_theme_id = in.readString();
        Posts__topic = in.readString();
        Posts__name = in.readString();
        Posts__content = in.readString();
        Posts__archived = in.readString();
        Posts__created = in.readString();
        is_published = in.readString();
        profileimage = in.readString();
        Posts__published = in.readString();
    }

    public static final Creator<News_Fragments_Data_Model> CREATOR = new Creator<News_Fragments_Data_Model>() {
        @Override
        public News_Fragments_Data_Model createFromParcel(Parcel in) {
            return new News_Fragments_Data_Model(in);
        }

        @Override
        public News_Fragments_Data_Model[] newArray(int size) {
            return new News_Fragments_Data_Model[size];
        }
    };

    public String getPosts__created() {
        return Posts__created;
    }

    public void setPosts__created(String posts__created) {
        Posts__created = posts__created;
    }

    public String getIs_published() {
        return is_published;
    }

    public void setIs_published(String is_published) {
        this.is_published = is_published;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getPosts__published() {
        return Posts__published;
    }

    public void setPosts__published(String posts__published) {
        Posts__published = posts__published;
    }

    public String getPosts__archived() {
        return Posts__archived;
    }

    public void setPosts__archived(String posts__archived) {
        Posts__archived = posts__archived;
    }

    public String getPosts__id() {
        return Posts__id;
    }

    public void setPosts__id(String posts__id) {
        Posts__id = posts__id;
    }

    public String getPosts__user_id() {
        return Posts__user_id;
    }

    public void setPosts__user_id(String posts__user_id) {
        Posts__user_id = posts__user_id;
    }

    public String getPosts__category_id() {
        return Posts__category_id;
    }

    public void setPosts__category_id(String posts__category_id) {
        Posts__category_id = posts__category_id;
    }

    public String getPosts__post_theme_id() {
        return Posts__post_theme_id;
    }

    public void setPosts__post_theme_id(String posts__post_theme_id) {
        Posts__post_theme_id = posts__post_theme_id;
    }

    public String getPosts__topic() {
        return Posts__topic;
    }

    public void setPosts__topic(String posts__topic) {
        Posts__topic = posts__topic;
    }

    public String getPosts__name() {
        return Posts__name;
    }

    public void setPosts__name(String posts__name) {
        Posts__name = posts__name;
    }

    public String getPosts__content() {
        return Posts__content;
    }

    public void setPosts__content(String posts__content) {
        Posts__content = posts__content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Posts__id);
        dest.writeString(Posts__user_id);
        dest.writeString(Posts__category_id);
        dest.writeString(Posts__post_theme_id);
        dest.writeString(Posts__topic);
        dest.writeString(Posts__name);
        dest.writeString(Posts__content);
        dest.writeString(Posts__archived);
        dest.writeString(Posts__created);
        dest.writeString(is_published);
        dest.writeString(profileimage);
        dest.writeString(Posts__published);
    }
}
