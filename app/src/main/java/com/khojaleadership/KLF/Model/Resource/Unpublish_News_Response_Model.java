package com.khojaleadership.KLF.Model.Resource;

public class Unpublish_News_Response_Model {

    String status,message;

    public Unpublish_News_Response_Model() {
    }

    public Unpublish_News_Response_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
