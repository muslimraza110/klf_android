package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetAllGroup_ProjectListModel implements Parcelable {

    String status,message;
    ArrayList<GetAllGroup_ProjectListDataModel> data;

    public GetAllGroup_ProjectListModel() {
    }

    public GetAllGroup_ProjectListModel(String status, String message, ArrayList<GetAllGroup_ProjectListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected GetAllGroup_ProjectListModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetAllGroup_ProjectListModel> CREATOR = new Creator<GetAllGroup_ProjectListModel>() {
        @Override
        public GetAllGroup_ProjectListModel createFromParcel(Parcel in) {
            return new GetAllGroup_ProjectListModel(in);
        }

        @Override
        public GetAllGroup_ProjectListModel[] newArray(int size) {
            return new GetAllGroup_ProjectListModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetAllGroup_ProjectListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetAllGroup_ProjectListDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
