package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class EditGroup_PendingAccessRequests_Model {

    String status,message;
    ArrayList<EditGroup_PendingAccessRequestsData_Model> data;

    public EditGroup_PendingAccessRequests_Model() {
    }

    public EditGroup_PendingAccessRequests_Model(String status, String message, ArrayList<EditGroup_PendingAccessRequestsData_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<EditGroup_PendingAccessRequestsData_Model> getData() {
        return data;
    }

    public void setData(ArrayList<EditGroup_PendingAccessRequestsData_Model> data) {
        this.data = data;
    }
}
