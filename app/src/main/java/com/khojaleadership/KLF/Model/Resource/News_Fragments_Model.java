package com.khojaleadership.KLF.Model.Resource;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class News_Fragments_Model implements Parcelable {
    String status,message;
    ArrayList<News_Fragments_Data_Model> data;

    public News_Fragments_Model() {
    }

    public News_Fragments_Model(String status, String message, ArrayList<News_Fragments_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected News_Fragments_Model(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<News_Fragments_Model> CREATOR = new Creator<News_Fragments_Model>() {
        @Override
        public News_Fragments_Model createFromParcel(Parcel in) {
            return new News_Fragments_Model(in);
        }

        @Override
        public News_Fragments_Model[] newArray(int size) {
            return new News_Fragments_Model[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<News_Fragments_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<News_Fragments_Data_Model> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
