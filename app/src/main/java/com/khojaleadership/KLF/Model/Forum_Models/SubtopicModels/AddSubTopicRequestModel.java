package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class AddSubTopicRequestModel {

    String user_id,subforum_id,topic,content,pinned,sod,eblast,post_visibility,post_approved_post;

    public AddSubTopicRequestModel() {
    }

    public AddSubTopicRequestModel(String user_id, String subforum_id, String topic, String content, String pinned, String sod, String eblast, String post_visibility, String post_approved_post) {
        this.user_id = user_id;
        this.subforum_id = subforum_id;
        this.topic = topic;
        this.content = content;
        this.pinned = pinned;
        this.sod = sod;
        this.eblast = eblast;
        this.post_visibility = post_visibility;
        this.post_approved_post = post_approved_post;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSubforum_id() {
        return subforum_id;
    }

    public void setSubforum_id(String subforum_id) {
        this.subforum_id = subforum_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPinned() {
        return pinned;
    }

    public void setPinned(String pinned) {
        this.pinned = pinned;
    }

    public String getSod() {
        return sod;
    }

    public void setSod(String sod) {
        this.sod = sod;
    }

    public String getEblast() {
        return eblast;
    }

    public void setEblast(String eblast) {
        this.eblast = eblast;
    }

    public String getPost_visibility() {
        return post_visibility;
    }

    public void setPost_visibility(String post_visibility) {
        this.post_visibility = post_visibility;
    }

    public String getPost_approved_post() {
        return post_approved_post;
    }

    public void setPost_approved_post(String post_approved_post) {
        this.post_approved_post = post_approved_post;
    }
}
