package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class GroupDetailRelatedGroupModel {
    String status,message;
    ArrayList<GroupDetailRelatedGroupDataModel> data;

    public GroupDetailRelatedGroupModel(String status, String message, ArrayList<GroupDetailRelatedGroupDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public GroupDetailRelatedGroupModel() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GroupDetailRelatedGroupDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GroupDetailRelatedGroupDataModel> data) {
        this.data = data;
    }
}
