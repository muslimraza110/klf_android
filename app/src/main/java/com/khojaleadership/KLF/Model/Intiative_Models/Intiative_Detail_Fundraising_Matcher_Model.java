package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Fundraising_Matcher_Model {

    String Matcher__name,Matcher__match_full_amount,Matcher__amount,Matcher__hide_amount,Matcher__anonymous;

    public Intiative_Detail_Fundraising_Matcher_Model() {
    }

    public Intiative_Detail_Fundraising_Matcher_Model(String matcher__name, String matcher__match_full_amount, String matcher__amount, String matcher__hide_amount, String matcher__anonymous) {
        Matcher__name = matcher__name;
        Matcher__match_full_amount = matcher__match_full_amount;
        Matcher__amount = matcher__amount;
        Matcher__hide_amount = matcher__hide_amount;
        Matcher__anonymous = matcher__anonymous;
    }

    public String getMatcher__name() {
        return Matcher__name;
    }

    public void setMatcher__name(String matcher__name) {
        Matcher__name = matcher__name;
    }

    public String getMatcher__match_full_amount() {
        return Matcher__match_full_amount;
    }

    public void setMatcher__match_full_amount(String matcher__match_full_amount) {
        Matcher__match_full_amount = matcher__match_full_amount;
    }

    public String getMatcher__amount() {
        return Matcher__amount;
    }

    public void setMatcher__amount(String matcher__amount) {
        Matcher__amount = matcher__amount;
    }

    public String getMatcher__hide_amount() {
        return Matcher__hide_amount;
    }

    public void setMatcher__hide_amount(String matcher__hide_amount) {
        Matcher__hide_amount = matcher__hide_amount;
    }

    public String getMatcher__anonymous() {
        return Matcher__anonymous;
    }

    public void setMatcher__anonymous(String matcher__anonymous) {
        Matcher__anonymous = matcher__anonymous;
    }
}
