package com.khojaleadership.KLF.Model.Group_Models;

public class AddGroupRequestModel {
    String category_id,name,description,email,phone,address,address2,website,city,region,country,postal_code;
    String bio,hidden,pending,vote_decision,donations,passive,user_id,is_admin;

    public AddGroupRequestModel() {
    }

    public AddGroupRequestModel(String category_id, String name, String description, String email, String phone, String address, String address2, String website, String city, String region, String country, String postal_code, String bio, String hidden, String pending, String vote_decision, String donations, String passive, String user_id, String is_admin) {
        this.category_id = category_id;
        this.name = name;
        this.description = description;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.address2 = address2;
        this.website = website;
        this.city = city;
        this.region = region;
        this.country = country;
        this.postal_code = postal_code;
        this.bio = bio;
        this.hidden = hidden;
        this.pending = pending;
        this.vote_decision = vote_decision;
        this.donations = donations;
        this.passive = passive;
        this.user_id = user_id;
        this.is_admin = is_admin;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getVote_decision() {
        return vote_decision;
    }

    public void setVote_decision(String vote_decision) {
        this.vote_decision = vote_decision;
    }

    public String getDonations() {
        return donations;
    }

    public void setDonations(String donations) {
        this.donations = donations;
    }

    public String getPassive() {
        return passive;
    }

    public void setPassive(String passive) {
        this.passive = passive;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(String is_admin) {
        this.is_admin = is_admin;
    }
}
