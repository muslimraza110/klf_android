package com.khojaleadership.KLF.Model.Resource;

public class Add_Archieve_News_ToActive_RequestModel {
    String post_id;

    public Add_Archieve_News_ToActive_RequestModel() {
    }

    public Add_Archieve_News_ToActive_RequestModel(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
