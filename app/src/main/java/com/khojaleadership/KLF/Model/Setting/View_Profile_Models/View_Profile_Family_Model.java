package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Family_Model implements Parcelable {
    String UserFamilies__id,UserFamilies__relation,RelatedUsers__id,RelatedUsers__role,RelatedUsers__email;
    String RelatedUsers__first_name,RelatedUsers__last_name;
    String ProfileImg__name;

    public View_Profile_Family_Model() {
    }

    public View_Profile_Family_Model(String userFamilies__id, String userFamilies__relation, String relatedUsers__id, String relatedUsers__role, String relatedUsers__email, String relatedUsers__first_name, String relatedUsers__last_name, String profileImg__name) {
        UserFamilies__id = userFamilies__id;
        UserFamilies__relation = userFamilies__relation;
        RelatedUsers__id = relatedUsers__id;
        RelatedUsers__role = relatedUsers__role;
        RelatedUsers__email = relatedUsers__email;
        RelatedUsers__first_name = relatedUsers__first_name;
        RelatedUsers__last_name = relatedUsers__last_name;
        ProfileImg__name = profileImg__name;
    }

    protected View_Profile_Family_Model(Parcel in) {
        UserFamilies__id = in.readString();
        UserFamilies__relation = in.readString();
        RelatedUsers__id = in.readString();
        RelatedUsers__role = in.readString();
        RelatedUsers__email = in.readString();
        RelatedUsers__first_name = in.readString();
        RelatedUsers__last_name = in.readString();
        ProfileImg__name = in.readString();
    }

    public static final Creator<View_Profile_Family_Model> CREATOR = new Creator<View_Profile_Family_Model>() {
        @Override
        public View_Profile_Family_Model createFromParcel(Parcel in) {
            return new View_Profile_Family_Model(in);
        }

        @Override
        public View_Profile_Family_Model[] newArray(int size) {
            return new View_Profile_Family_Model[size];
        }
    };

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }

    public String getUserFamilies__id() {
        return UserFamilies__id;
    }

    public void setUserFamilies__id(String userFamilies__id) {
        UserFamilies__id = userFamilies__id;
    }

    public String getUserFamilies__relation() {
        return UserFamilies__relation;
    }

    public void setUserFamilies__relation(String userFamilies__relation) {
        UserFamilies__relation = userFamilies__relation;
    }

    public String getRelatedUsers__id() {
        return RelatedUsers__id;
    }

    public void setRelatedUsers__id(String relatedUsers__id) {
        RelatedUsers__id = relatedUsers__id;
    }

    public String getRelatedUsers__role() {
        return RelatedUsers__role;
    }

    public void setRelatedUsers__role(String relatedUsers__role) {
        RelatedUsers__role = relatedUsers__role;
    }

    public String getRelatedUsers__email() {
        return RelatedUsers__email;
    }

    public void setRelatedUsers__email(String relatedUsers__email) {
        RelatedUsers__email = relatedUsers__email;
    }

    public String getRelatedUsers__first_name() {
        return RelatedUsers__first_name;
    }

    public void setRelatedUsers__first_name(String relatedUsers__first_name) {
        RelatedUsers__first_name = relatedUsers__first_name;
    }

    public String getRelatedUsers__last_name() {
        return RelatedUsers__last_name;
    }

    public void setRelatedUsers__last_name(String relatedUsers__last_name) {
        RelatedUsers__last_name = relatedUsers__last_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserFamilies__id);
        dest.writeString(UserFamilies__relation);
        dest.writeString(RelatedUsers__id);
        dest.writeString(RelatedUsers__role);
        dest.writeString(RelatedUsers__email);
        dest.writeString(RelatedUsers__first_name);
        dest.writeString(RelatedUsers__last_name);
        dest.writeString(ProfileImg__name);
    }
}
