package com.khojaleadership.KLF.Model.Project_Models;

public class Percentage_List_Data_Model {
    String ProjectsGroups__group_id,ProjectsGroups__id,ProjectsGroups__project_id,ProjectsGroups__percent,Groups__id;
    String Groups__name;

    public Percentage_List_Data_Model() {
    }

    public Percentage_List_Data_Model(String projectsGroups__group_id, String projectsGroups__id, String projectsGroups__project_id, String projectsGroups__percent, String groups__id, String groups__name) {
        ProjectsGroups__group_id = projectsGroups__group_id;
        ProjectsGroups__id = projectsGroups__id;
        ProjectsGroups__project_id = projectsGroups__project_id;
        ProjectsGroups__percent = projectsGroups__percent;
        Groups__id = groups__id;
        Groups__name = groups__name;
    }

    public String getProjectsGroups__group_id() {
        return ProjectsGroups__group_id;
    }

    public void setProjectsGroups__group_id(String projectsGroups__group_id) {
        ProjectsGroups__group_id = projectsGroups__group_id;
    }

    public String getProjectsGroups__id() {
        return ProjectsGroups__id;
    }

    public void setProjectsGroups__id(String projectsGroups__id) {
        ProjectsGroups__id = projectsGroups__id;
    }

    public String getProjectsGroups__project_id() {
        return ProjectsGroups__project_id;
    }

    public void setProjectsGroups__project_id(String projectsGroups__project_id) {
        ProjectsGroups__project_id = projectsGroups__project_id;
    }

    public String getProjectsGroups__percent() {
        return ProjectsGroups__percent;
    }

    public void setProjectsGroups__percent(String projectsGroups__percent) {
        ProjectsGroups__percent = projectsGroups__percent;
    }

    public String getGroups__id() {
        return Groups__id;
    }

    public void setGroups__id(String groups__id) {
        Groups__id = groups__id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }
}
