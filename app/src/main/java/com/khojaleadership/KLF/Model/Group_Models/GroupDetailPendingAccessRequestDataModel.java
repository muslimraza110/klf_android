package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailPendingAccessRequestDataModel {
    String GroupRequests__id,GroupRequests__group_id,GroupRequests__user_id,GroupRequests__status,GroupRequests__created;
    String GroupRequests__modified,Users__id,Users__role,Users__email,Users__first_name,Users__last_name,Users__gender;
    String Users__status;

    public GroupDetailPendingAccessRequestDataModel() {
    }

    public GroupDetailPendingAccessRequestDataModel(String groupRequests__id, String groupRequests__group_id, String groupRequests__user_id, String groupRequests__status, String groupRequests__created, String groupRequests__modified, String users__id, String users__role, String users__email, String users__first_name, String users__last_name, String users__gender, String users__status) {
        GroupRequests__id = groupRequests__id;
        GroupRequests__group_id = groupRequests__group_id;
        GroupRequests__user_id = groupRequests__user_id;
        GroupRequests__status = groupRequests__status;
        GroupRequests__created = groupRequests__created;
        GroupRequests__modified = groupRequests__modified;
        Users__id = users__id;
        Users__role = users__role;
        Users__email = users__email;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        Users__gender = users__gender;
        Users__status = users__status;
    }

    public String getGroupRequests__id() {
        return GroupRequests__id;
    }

    public void setGroupRequests__id(String groupRequests__id) {
        GroupRequests__id = groupRequests__id;
    }

    public String getGroupRequests__group_id() {
        return GroupRequests__group_id;
    }

    public void setGroupRequests__group_id(String groupRequests__group_id) {
        GroupRequests__group_id = groupRequests__group_id;
    }

    public String getGroupRequests__user_id() {
        return GroupRequests__user_id;
    }

    public void setGroupRequests__user_id(String groupRequests__user_id) {
        GroupRequests__user_id = groupRequests__user_id;
    }

    public String getGroupRequests__status() {
        return GroupRequests__status;
    }

    public void setGroupRequests__status(String groupRequests__status) {
        GroupRequests__status = groupRequests__status;
    }

    public String getGroupRequests__created() {
        return GroupRequests__created;
    }

    public void setGroupRequests__created(String groupRequests__created) {
        GroupRequests__created = groupRequests__created;
    }

    public String getGroupRequests__modified() {
        return GroupRequests__modified;
    }

    public void setGroupRequests__modified(String groupRequests__modified) {
        GroupRequests__modified = groupRequests__modified;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getUsers__gender() {
        return Users__gender;
    }

    public void setUsers__gender(String users__gender) {
        Users__gender = users__gender;
    }

    public String getUsers__status() {
        return Users__status;
    }

    public void setUsers__status(String users__status) {
        Users__status = users__status;
    }
}
