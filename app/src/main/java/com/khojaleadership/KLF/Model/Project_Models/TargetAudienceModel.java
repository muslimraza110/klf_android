package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class TargetAudienceModel {
    String status,message;
    ArrayList<TargetAudienceDataModel> data;

    public TargetAudienceModel() {
    }

    public TargetAudienceModel(String status, String message, ArrayList<TargetAudienceDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TargetAudienceDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<TargetAudienceDataModel> data) {
        this.data = data;
    }
}
