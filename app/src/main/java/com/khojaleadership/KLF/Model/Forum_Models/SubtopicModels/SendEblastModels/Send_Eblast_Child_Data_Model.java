package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels;

import android.os.Parcel;
import android.os.Parcelable;

public class Send_Eblast_Child_Data_Model implements Parcelable {
    private String letters;//Display the initials of the pinyin
    String UserID,FirstName,LastName,Email,ProfileURL;
    String is_checked;


    public Send_Eblast_Child_Data_Model() {
    }




    public Send_Eblast_Child_Data_Model(String letters, String userID, String firstName, String lastName, String email, String profileURL, String is_checked) {
        this.letters = letters;
        UserID = userID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        ProfileURL = profileURL;
        this.is_checked = is_checked;
    }

    protected Send_Eblast_Child_Data_Model(Parcel in) {
        letters = in.readString();
        UserID = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
        Email = in.readString();
        ProfileURL = in.readString();
        is_checked = in.readString();
    }

    public static final Creator<Send_Eblast_Child_Data_Model> CREATOR = new Creator<Send_Eblast_Child_Data_Model>() {
        @Override
        public Send_Eblast_Child_Data_Model createFromParcel(Parcel in) {
            return new Send_Eblast_Child_Data_Model(in);
        }

        @Override
        public Send_Eblast_Child_Data_Model[] newArray(int size) {
            return new Send_Eblast_Child_Data_Model[size];
        }
    };

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getIs_checked() {
        return is_checked;
    }

    public void setIs_checked(String is_checked) {
        this.is_checked = is_checked;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfileURL() {
        return ProfileURL;
    }

    public void setProfileURL(String profileURL) {
        ProfileURL = profileURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(letters);
        dest.writeString(UserID);
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(Email);
        dest.writeString(ProfileURL);
        dest.writeString(is_checked);
    }
}
