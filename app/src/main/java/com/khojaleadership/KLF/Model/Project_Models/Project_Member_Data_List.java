package com.khojaleadership.KLF.Model.Project_Models;

public class Project_Member_Data_List {
    private String letters;//Display the initials of the pinyin;
    String role,title,first_name,middle_name,last_name,gender,chair_person,id,email,project_id,profile_image;
    int Ismember;

    public Project_Member_Data_List() {
    }


    public Project_Member_Data_List(String letters, String role, String title, String first_name, String middle_name, String last_name, String gender, String chair_person, String id, String email, String project_id, String profile_image, int ismember) {
        this.letters = letters;
        this.role = role;
        this.title = title;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.gender = gender;
        this.chair_person = chair_person;
        this.id = id;
        this.email = email;
        this.project_id = project_id;
        this.profile_image = profile_image;
        Ismember = ismember;
    }



    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getChair_person() {
        return chair_person;
    }

    public void setChair_person(String chair_person) {
        this.chair_person = chair_person;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public int getIsmember() {
        return Ismember;
    }

    public void setIsmember(int ismember) {
        Ismember = ismember;
    }
}
