package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class SpeakerListResponse(
        @SerializedName("sortBy") val sortBy: String?,
        @SerializedName("peoples") val list: List<PersonDetailResponse?>?
)

data class SpeakerResponse(
        @SerializedName("image") val image: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("designation") val designation: String?,
        @SerializedName("short_bio") val shortBio: String?,
        @SerializedName("twitter_url") val twitterUrl: String?,
        @SerializedName("facebook_url") val facebookUrl: String?
)