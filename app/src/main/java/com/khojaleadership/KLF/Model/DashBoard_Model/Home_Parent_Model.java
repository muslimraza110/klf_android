package com.khojaleadership.KLF.Model.DashBoard_Model;

import java.util.ArrayList;

public class Home_Parent_Model {
    String parent_name;
    ArrayList<Home_Child_Model> childList;

    public Home_Parent_Model() {
    }

    public Home_Parent_Model(String parent_name, ArrayList<Home_Child_Model> childList) {
        this.parent_name = parent_name;
        this.childList = childList;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public ArrayList<Home_Child_Model> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<Home_Child_Model> childList) {
        this.childList = childList;
    }
}
