package com.khojaleadership.KLF.Model.Intiative_Models;

public class Add_Plege_Response_Model {
    String status,message;

    public Add_Plege_Response_Model() {
    }

    public Add_Plege_Response_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
