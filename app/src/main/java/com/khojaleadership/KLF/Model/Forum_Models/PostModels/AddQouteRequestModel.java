package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class AddQouteRequestModel {
    String user_id,parent_id,lft,rght,topic,content,statuspost;

    public AddQouteRequestModel() {
    }

    public AddQouteRequestModel(String user_id, String parent_id, String lft, String rght, String topic, String content, String statuspost) {
        this.user_id = user_id;
        this.parent_id = parent_id;
        this.lft = lft;
        this.rght = rght;
        this.topic = topic;
        this.content = content;
        this.statuspost = statuspost;
    }

    public String getStatuspost() {
        return statuspost;
    }

    public void setStatuspost(String statuspost) {
        this.statuspost = statuspost;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getLft() {
        return lft;
    }

    public void setLft(String lft) {
        this.lft = lft;
    }

    public String getRght() {
        return rght;
    }

    public void setRght(String rght) {
        this.rght = rght;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

