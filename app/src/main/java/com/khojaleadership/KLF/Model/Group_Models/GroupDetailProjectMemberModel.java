package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailProjectMemberModel {
    String Project__name,Member__name,Member_id,ProfileImg__name;

    public GroupDetailProjectMemberModel() {
    }

    public GroupDetailProjectMemberModel(String project__name, String member__name, String member_id, String profileImg__name) {
        Project__name = project__name;
        Member__name = member__name;
        Member_id = member_id;
        ProfileImg__name = profileImg__name;
    }

    public String getProject__name() {
        return Project__name;
    }

    public void setProject__name(String project__name) {
        Project__name = project__name;
    }

    public String getMember__name() {
        return Member__name;
    }

    public void setMember__name(String member__name) {
        Member__name = member__name;
    }

    public String getMember_id() {
        return Member_id;
    }

    public void setMember_id(String member_id) {
        Member_id = member_id;
    }

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }
}
