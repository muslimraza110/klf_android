package com.khojaleadership.KLF.Model.Forum_Models.TopicModels;

public class AddTopicResponseModel {
    String status,message;

    public AddTopicResponseModel() {
    }

    public AddTopicResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
