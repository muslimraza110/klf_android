package com.khojaleadership.KLF.Model.Group_Models;

public class DeleteGroupResponseModel {

    String status,message;

    public DeleteGroupResponseModel() {
    }

    public DeleteGroupResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
