package com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models;

public class Edit_ProfileRequest_Model {
    String user_id,description,private_bio,address,address2,title,city,country,region,postal_code,email2;
    String website,facebook,linkedin,phone,phone2,first_name,middle_name,last_name,gender,email,business_title;
    String bio,copywriter_user_id,comment,status,related_user_id,relation;

    public Edit_ProfileRequest_Model() {
    }

    public Edit_ProfileRequest_Model(String user_id, String description, String private_bio, String address, String address2, String title, String city, String country, String region, String postal_code, String email2, String website, String facebook, String linkedin, String phone, String phone2, String first_name, String middle_name, String last_name, String gender, String email, String business_title, String bio, String copywriter_user_id, String comment, String status, String related_user_id, String relation) {
        this.user_id = user_id;
        this.description = description;
        this.private_bio = private_bio;
        this.address = address;
        this.address2 = address2;
        this.title = title;
        this.city = city;
        this.country = country;
        this.region = region;
        this.postal_code = postal_code;
        this.email2 = email2;
        this.website = website;
        this.facebook = facebook;
        this.linkedin = linkedin;
        this.phone = phone;
        this.phone2 = phone2;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.gender = gender;
        this.email = email;
        this.business_title = business_title;
        this.bio = bio;
        this.copywriter_user_id = copywriter_user_id;
        this.comment = comment;
        this.status = status;
        this.related_user_id = related_user_id;
        this.relation = relation;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrivate_bio() {
        return private_bio;
    }

    public void setPrivate_bio(String private_bio) {
        this.private_bio = private_bio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusiness_title() {
        return business_title;
    }

    public void setBusiness_title(String business_title) {
        this.business_title = business_title;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCopywriter_user_id() {
        return copywriter_user_id;
    }

    public void setCopywriter_user_id(String copywriter_user_id) {
        this.copywriter_user_id = copywriter_user_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelated_user_id() {
        return related_user_id;
    }

    public void setRelated_user_id(String related_user_id) {
        this.related_user_id = related_user_id;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
}
