package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class EditSubTopicRequestModel {

    String user_id,post_id,topic,content,post_visibility,eblast,pinned,sod,post_approved_post;

    public EditSubTopicRequestModel() {
    }



    public EditSubTopicRequestModel(String user_id, String post_id, String topic, String content, String post_visibility, String eblast, String pinned, String sod, String post_approved_post) {
        this.user_id = user_id;
        this.post_id = post_id;
        this.topic = topic;
        this.content = content;
        this.post_visibility = post_visibility;
        this.eblast = eblast;
        this.pinned = pinned;
        this.sod = sod;
        this.post_approved_post = post_approved_post;
    }

    public String getPost_approved_post() {
        return post_approved_post;
    }

    public void setPost_approved_post(String post_approved_post) {
        this.post_approved_post = post_approved_post;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPost_visibility() {
        return post_visibility;
    }

    public void setPost_visibility(String post_visibility) {
        this.post_visibility = post_visibility;
    }

    public String getEblast() {
        return eblast;
    }

    public void setEblast(String eblast) {
        this.eblast = eblast;
    }

    public String getPinned() {
        return pinned;
    }

    public void setPinned(String pinned) {
        this.pinned = pinned;
    }

    public String getSod() {
        return sod;
    }

    public void setSod(String sod) {
        this.sod = sod;
    }
}
