package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

import java.util.ArrayList;

public class IsWhiteListed_Model {
    String status,message;
    ArrayList<IsWhiteListed_Data_Model> data;

    public IsWhiteListed_Model() {
    }

    public IsWhiteListed_Model(String status, String message, ArrayList<IsWhiteListed_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<IsWhiteListed_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<IsWhiteListed_Data_Model> data) {
        this.data = data;
    }
}
