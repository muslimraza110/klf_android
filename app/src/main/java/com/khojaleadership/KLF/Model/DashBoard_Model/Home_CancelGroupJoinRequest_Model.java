package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_CancelGroupJoinRequest_Model {
    String group_requests_id;

    public Home_CancelGroupJoinRequest_Model() {
    }

    public Home_CancelGroupJoinRequest_Model(String group_requests_id) {
        this.group_requests_id = group_requests_id;
    }

    public String getGroup_requests_id() {
        return group_requests_id;
    }

    public void setGroup_requests_id(String group_requests_id) {
        this.group_requests_id = group_requests_id;
    }
}
