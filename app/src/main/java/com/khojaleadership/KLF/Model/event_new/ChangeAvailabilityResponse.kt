package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName


data class ChangeAvailabilityResponse(
        @SerializedName("status") val status: String?,
        @SerializedName("message") val message: String?
)