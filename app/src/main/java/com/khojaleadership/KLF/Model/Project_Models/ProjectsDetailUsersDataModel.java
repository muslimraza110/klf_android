package com.khojaleadership.KLF.Model.Project_Models;

public class ProjectsDetailUsersDataModel {

    String Users__title,Users__id,Users__email,Users__first_name,Users__last_name;
    String ProfileImg__name;

    public ProjectsDetailUsersDataModel() {
    }


    public ProjectsDetailUsersDataModel(String users__title, String users__id, String users__email, String users__first_name, String users__last_name, String profileImg__name) {
        Users__title = users__title;
        Users__id = users__id;
        Users__email = users__email;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        ProfileImg__name = profileImg__name;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }
}
