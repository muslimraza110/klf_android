package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Comments_Model implements Parcelable {
    String UserComments__comment;

    public View_Profile_Comments_Model() {
    }

    public View_Profile_Comments_Model(String userComments__comment) {
        UserComments__comment = userComments__comment;
    }

    protected View_Profile_Comments_Model(Parcel in) {
        UserComments__comment = in.readString();
    }

    public static final Creator<View_Profile_Comments_Model> CREATOR = new Creator<View_Profile_Comments_Model>() {
        @Override
        public View_Profile_Comments_Model createFromParcel(Parcel in) {
            return new View_Profile_Comments_Model(in);
        }

        @Override
        public View_Profile_Comments_Model[] newArray(int size) {
            return new View_Profile_Comments_Model[size];
        }
    };

    public String getUserComments__comment() {
        return UserComments__comment;
    }

    public void setUserComments__comment(String userComments__comment) {
        UserComments__comment = userComments__comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserComments__comment);
    }
}
