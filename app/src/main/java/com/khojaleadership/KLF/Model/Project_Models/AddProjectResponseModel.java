package com.khojaleadership.KLF.Model.Project_Models;

public class AddProjectResponseModel {
    String status,message;
    AddProjectResponseDataModel data;

    public AddProjectResponseModel() {
    }

    public AddProjectResponseModel(String status, String message, AddProjectResponseDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public AddProjectResponseDataModel getData() {
        return data;
    }

    public void setData(AddProjectResponseDataModel data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
