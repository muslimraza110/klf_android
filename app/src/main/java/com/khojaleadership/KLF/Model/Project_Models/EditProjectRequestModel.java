package com.khojaleadership.KLF.Model.Project_Models;

public class EditProjectRequestModel {
    String creator_id,khoja_care_category_id,name,website,description,start_date,est_completion_date,city,country,status,project_id,approval_stamp;
    String charity_type,targetaudience,targetanualspent;


    public EditProjectRequestModel() {
    }

    public EditProjectRequestModel(String creator_id, String khoja_care_category_id, String name, String website, String description, String start_date, String est_completion_date, String city, String country, String status, String project_id, String approval_stamp, String charity_type, String targetaudience, String targetanualspent) {
        this.creator_id = creator_id;
        this.khoja_care_category_id = khoja_care_category_id;
        this.name = name;
        this.website = website;
        this.description = description;
        this.start_date = start_date;
        this.est_completion_date = est_completion_date;
        this.city = city;
        this.country = country;
        this.status = status;
        this.project_id = project_id;
        this.approval_stamp = approval_stamp;
        this.charity_type = charity_type;
        this.targetaudience = targetaudience;
        this.targetanualspent = targetanualspent;
    }

    public String getCharity_type() {
        return charity_type;
    }

    public void setCharity_type(String charity_type) {
        this.charity_type = charity_type;
    }

    public String getTargetaudience() {
        return targetaudience;
    }

    public void setTargetaudience(String targetaudience) {
        this.targetaudience = targetaudience;
    }

    public String getTargetanualspent() {
        return targetanualspent;
    }

    public void setTargetanualspent(String targetanualspent) {
        this.targetanualspent = targetanualspent;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getKhoja_care_category_id() {
        return khoja_care_category_id;
    }

    public void setKhoja_care_category_id(String khoja_care_category_id) {
        this.khoja_care_category_id = khoja_care_category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEst_completion_date() {
        return est_completion_date;
    }

    public void setEst_completion_date(String est_completion_date) {
        this.est_completion_date = est_completion_date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getApproval_stamp() {
        return approval_stamp;
    }

    public void setApproval_stamp(String approval_stamp) {
        this.approval_stamp = approval_stamp;
    }
}
