package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class ProjectDetailModel {
    String status, message, totalcount, totalpages;



    ArrayList<ProjectsDetailGroupDataModel> project_group;
    ArrayList<ProjectsDetailUsersDataModel> project_users;
    ArrayList<ProjectDetailTagsDataModel> project_tags;


    public ProjectDetailModel() {
    }


    public ProjectDetailModel(String status, String message, String totalcount, String totalpages, ArrayList<ProjectsDetailGroupDataModel> project_group, ArrayList<ProjectsDetailUsersDataModel> project_users, ArrayList<ProjectDetailTagsDataModel> project_tags) {
        this.status = status;
        this.message = message;
        this.totalcount = totalcount;
        this.totalpages = totalpages;
        this.project_group = project_group;
        this.project_users = project_users;
        this.project_tags = project_tags;
    }

    public ArrayList<ProjectDetailTagsDataModel> getProject_tags() {
        return project_tags;
    }

    public void setProject_tags(ArrayList<ProjectDetailTagsDataModel> project_tags) {
        this.project_tags = project_tags;
    }

    public ArrayList<ProjectsDetailGroupDataModel> getProject_group() {
        return project_group;
    }

    public void setProject_group(ArrayList<ProjectsDetailGroupDataModel> project_group) {
        this.project_group = project_group;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }

    public String getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(String totalpages) {
        this.totalpages = totalpages;
    }

    public ArrayList<ProjectsDetailUsersDataModel> getProject_users() {
        return project_users;
    }

    public void setProject_users(ArrayList<ProjectsDetailUsersDataModel> project_users) {
        this.project_users = project_users;
    }
}
