package com.khojaleadership.KLF.Model.Project_Models;

public class AddPercentageRequestModel {
    String project_id,groups_id,percentagevalue;

    public AddPercentageRequestModel() {
    }

    public AddPercentageRequestModel(String project_id, String groups_id, String percentagevalue) {
        this.project_id = project_id;
        this.groups_id = groups_id;
        this.percentagevalue = percentagevalue;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getGroups_id() {
        return groups_id;
    }

    public void setGroups_id(String groups_id) {
        this.groups_id = groups_id;
    }

    public String getPercentagevalue() {
        return percentagevalue;
    }

    public void setPercentagevalue(String percentagevalue) {
        this.percentagevalue = percentagevalue;
    }
}
