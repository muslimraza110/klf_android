package com.khojaleadership.KLF.Model.Forum_Models.TopicModels;

public class DeleteTopicRequestModel {
    String subforum_id;

    public DeleteTopicRequestModel() {
    }

    public DeleteTopicRequestModel(String subforum_id) {
        this.subforum_id = subforum_id;
    }

    public String getSubforum_id() {
        return subforum_id;
    }

    public void setSubforum_id(String subforum_id) {
        this.subforum_id = subforum_id;
    }
}
