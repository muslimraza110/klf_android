package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class ApproveCommentRequestModel {
    String reply_id;

    public ApproveCommentRequestModel() {
    }

    public ApproveCommentRequestModel(String reply_id) {
        this.reply_id = reply_id;
    }

    public String getReply_id() {
        return reply_id;
    }

    public void setReply_id(String reply_id) {
        this.reply_id = reply_id;
    }
}
