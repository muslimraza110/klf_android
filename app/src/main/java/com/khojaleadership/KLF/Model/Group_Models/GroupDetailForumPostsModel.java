package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class GroupDetailForumPostsModel {

    String status,message;
    ArrayList<GroupDetailForumPostsDataModel> data;

    public GroupDetailForumPostsModel() {
    }

    public GroupDetailForumPostsModel(String status, String message, ArrayList<GroupDetailForumPostsDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GroupDetailForumPostsDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GroupDetailForumPostsDataModel> data) {
        this.data = data;
    }
}
