package com.khojaleadership.KLF.Model.Project_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class ProjectTargetAudienceModel implements Parcelable {
    String  id,name,project_id;

    protected ProjectTargetAudienceModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        project_id = in.readString();
    }

    public static final Creator<ProjectTargetAudienceModel> CREATOR = new Creator<ProjectTargetAudienceModel>() {
        @Override
        public ProjectTargetAudienceModel createFromParcel(Parcel in) {
            return new ProjectTargetAudienceModel(in);
        }

        @Override
        public ProjectTargetAudienceModel[] newArray(int size) {
            return new ProjectTargetAudienceModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(project_id);
    }

    public ProjectTargetAudienceModel() {
    }

    public ProjectTargetAudienceModel(String id, String name, String project_id) {
        this.id = id;
        this.name = name;
        this.project_id = project_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }
}
