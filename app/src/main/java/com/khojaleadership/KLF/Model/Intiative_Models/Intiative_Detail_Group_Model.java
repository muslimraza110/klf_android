package com.khojaleadership.KLF.Model.Intiative_Models;

import java.util.ArrayList;

public class Intiative_Detail_Group_Model {
    String status,message;
    ArrayList<Intiative_Detail_Group_Data_Model> data;

    public Intiative_Detail_Group_Model() {
    }

    public Intiative_Detail_Group_Model(String status, String message, ArrayList<Intiative_Detail_Group_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Intiative_Detail_Group_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<Intiative_Detail_Group_Data_Model> data) {
        this.data = data;
    }
}
