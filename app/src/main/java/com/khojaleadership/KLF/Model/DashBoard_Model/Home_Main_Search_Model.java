package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_Main_Search_Model {
    String name,id;


    public Home_Main_Search_Model() {
    }

    public Home_Main_Search_Model(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
