package com.khojaleadership.KLF.Model.Resource;

public class Publish_News_Request_Model {
    String post_id;

    public Publish_News_Request_Model() {
    }

    public Publish_News_Request_Model(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
