package com.khojaleadership.KLF.Model.Intiative_Models;

public class Add_Plege_Request_Model {
    String fundraising_id,user_id,group_id,amount,anonymous,hide_amount,initative_id,blocks;

    public Add_Plege_Request_Model() {

    }

    public Add_Plege_Request_Model(String fundraising_id, String user_id, String group_id, String amount, String anonymous, String hide_amount, String initative_id, String blocks) {
        this.fundraising_id = fundraising_id;
        this.user_id = user_id;
        this.group_id = group_id;
        this.amount = amount;
        this.anonymous = anonymous;
        this.hide_amount = hide_amount;
        this.initative_id = initative_id;
        this.blocks = blocks;
    }

    public String getFundraising_id() {
        return fundraising_id;
    }

    public void setFundraising_id(String fundraising_id) {
        this.fundraising_id = fundraising_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getHide_amount() {
        return hide_amount;
    }

    public void setHide_amount(String hide_amount) {
        this.hide_amount = hide_amount;
    }

    public String getInitative_id() {
        return initative_id;
    }

    public void setInitative_id(String initative_id) {
        this.initative_id = initative_id;
    }

    public String getBlocks() {
        return blocks;
    }

    public void setBlocks(String blocks) {
        this.blocks = blocks;
    }
}
