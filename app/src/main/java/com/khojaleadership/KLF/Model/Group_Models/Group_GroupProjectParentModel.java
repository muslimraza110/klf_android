package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class Group_GroupProjectParentModel {
    String project_id,project_name;
    ArrayList<Group_GroupProjectChildModel> child_data;

    public Group_GroupProjectParentModel() {
    }

    public Group_GroupProjectParentModel(String project_id, String project_name, ArrayList<Group_GroupProjectChildModel> child_data) {
        this.project_id = project_id;
        this.project_name = project_name;
        this.child_data = child_data;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public ArrayList<Group_GroupProjectChildModel> getChild_data() {
        return child_data;
    }

    public void setChild_data(ArrayList<Group_GroupProjectChildModel> child_data) {
        this.child_data = child_data;
    }
}
