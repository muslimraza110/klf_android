package com.khojaleadership.KLF.Model.DashBoard_Model;

import java.util.ArrayList;
import java.util.List;

public class OfflineExpandableParentModel {


    public int imageResource;
    public String TitleName;
    public List<OfflineExpandableChildModel> mobiles = new ArrayList<OfflineExpandableChildModel>();


    public OfflineExpandableParentModel(int imageResource, String TitleName, List<OfflineExpandableChildModel> mobiles) {
        this.imageResource = imageResource;
        this.TitleName = TitleName;
        this.mobiles = mobiles;
    }
}
