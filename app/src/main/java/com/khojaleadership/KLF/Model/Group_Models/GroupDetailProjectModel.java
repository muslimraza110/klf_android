package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class GroupDetailProjectModel {
    String status,message;
    ArrayList<GroupDetailProjectDataModel> data;

    public GroupDetailProjectModel() {
    }

    public GroupDetailProjectModel(String status, String message, ArrayList<GroupDetailProjectDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GroupDetailProjectDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GroupDetailProjectDataModel> data) {
        this.data = data;
    }
}
