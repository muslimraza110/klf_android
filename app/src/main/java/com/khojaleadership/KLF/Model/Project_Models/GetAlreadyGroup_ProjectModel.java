package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class GetAlreadyGroup_ProjectModel {
    String status,message;
    ArrayList<GetAlreadyGroup_ProjectDataModel> data;

    public GetAlreadyGroup_ProjectModel() {
    }

    public GetAlreadyGroup_ProjectModel(String status, String message, ArrayList<GetAlreadyGroup_ProjectDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetAlreadyGroup_ProjectDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetAlreadyGroup_ProjectDataModel> data) {
        this.data = data;
    }
}
