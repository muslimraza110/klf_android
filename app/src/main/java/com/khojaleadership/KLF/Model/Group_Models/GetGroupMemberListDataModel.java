package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class GetGroupMemberListDataModel implements Parcelable {

    private String letters;//Display the initials of the pinyin

    String role,title,first_name,middle_name,last_name,gender,chair_person,id,email,group_id;
    int Ismember;
    String profile_image;

    public GetGroupMemberListDataModel() {
    }

    public GetGroupMemberListDataModel(String letters, String role, String title, String first_name, String middle_name, String last_name, String gender, String chair_person, String id, String email, String group_id, int ismember, String profile_image) {
        this.letters = letters;
        this.role = role;
        this.title = title;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.gender = gender;
        this.chair_person = chair_person;
        this.id = id;
        this.email = email;
        this.group_id = group_id;
        Ismember = ismember;
        this.profile_image = profile_image;
    }

    protected GetGroupMemberListDataModel(Parcel in) {
        letters = in.readString();
        role = in.readString();
        title = in.readString();
        first_name = in.readString();
        middle_name = in.readString();
        last_name = in.readString();
        gender = in.readString();
        chair_person = in.readString();
        id = in.readString();
        email = in.readString();
        group_id = in.readString();
        Ismember = in.readInt();
        profile_image = in.readString();
    }

    public static final Creator<GetGroupMemberListDataModel> CREATOR = new Creator<GetGroupMemberListDataModel>() {
        @Override
        public GetGroupMemberListDataModel createFromParcel(Parcel in) {
            return new GetGroupMemberListDataModel(in);
        }

        @Override
        public GetGroupMemberListDataModel[] newArray(int size) {
            return new GetGroupMemberListDataModel[size];
        }
    };

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getChair_person() {
        return chair_person;
    }

    public void setChair_person(String chair_person) {
        this.chair_person = chair_person;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public int getIsmember() {
        return Ismember;
    }

    public void setIsmember(int ismember) {
        Ismember = ismember;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(letters);
        dest.writeString(role);
        dest.writeString(title);
        dest.writeString(first_name);
        dest.writeString(middle_name);
        dest.writeString(last_name);
        dest.writeString(gender);
        dest.writeString(chair_person);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(group_id);
        dest.writeInt(Ismember);
        dest.writeString(profile_image);
    }
}
