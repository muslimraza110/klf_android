package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Project_Model implements Parcelable {

    String project_id,group_id,ProjectsGroups__percent,Projects__creator_id,Projects__name,Projects__description;

    public View_Profile_Project_Model() {
    }

    public View_Profile_Project_Model(String project_id, String group_id, String projectsGroups__percent, String projects__creator_id, String projects__name, String projects__description) {
        this.project_id = project_id;
        this.group_id = group_id;
        ProjectsGroups__percent = projectsGroups__percent;
        Projects__creator_id = projects__creator_id;
        Projects__name = projects__name;
        Projects__description = projects__description;
    }

    protected View_Profile_Project_Model(Parcel in) {
        project_id = in.readString();
        group_id = in.readString();
        ProjectsGroups__percent = in.readString();
        Projects__creator_id = in.readString();
        Projects__name = in.readString();
        Projects__description = in.readString();
    }

    public static final Creator<View_Profile_Project_Model> CREATOR = new Creator<View_Profile_Project_Model>() {
        @Override
        public View_Profile_Project_Model createFromParcel(Parcel in) {
            return new View_Profile_Project_Model(in);
        }

        @Override
        public View_Profile_Project_Model[] newArray(int size) {
            return new View_Profile_Project_Model[size];
        }
    };

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getProjectsGroups__percent() {
        return ProjectsGroups__percent;
    }

    public void setProjectsGroups__percent(String projectsGroups__percent) {
        ProjectsGroups__percent = projectsGroups__percent;
    }

    public String getProjects__creator_id() {
        return Projects__creator_id;
    }

    public void setProjects__creator_id(String projects__creator_id) {
        Projects__creator_id = projects__creator_id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }

    public String getProjects__description() {
        return Projects__description;
    }

    public void setProjects__description(String projects__description) {
        Projects__description = projects__description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(project_id);
        dest.writeString(group_id);
        dest.writeString(ProjectsGroups__percent);
        dest.writeString(Projects__creator_id);
        dest.writeString(Projects__name);
        dest.writeString(Projects__description);
    }
}
