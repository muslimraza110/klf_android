package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Pleger_Model implements Parcelable {
    String Pledge__name,Pledge__amount,Pledge__initiative_id;
    int is_deleted;

    public View_Profile_Pleger_Model() {
    }

    public View_Profile_Pleger_Model(String pledge__name, String pledge__amount, String pledge__initiative_id, int is_deleted) {
        Pledge__name = pledge__name;
        Pledge__amount = pledge__amount;
        Pledge__initiative_id = pledge__initiative_id;
        this.is_deleted = is_deleted;
    }

    protected View_Profile_Pleger_Model(Parcel in) {
        Pledge__name = in.readString();
        Pledge__amount = in.readString();
        Pledge__initiative_id = in.readString();
        is_deleted = in.readInt();
    }

    public static final Creator<View_Profile_Pleger_Model> CREATOR = new Creator<View_Profile_Pleger_Model>() {
        @Override
        public View_Profile_Pleger_Model createFromParcel(Parcel in) {
            return new View_Profile_Pleger_Model(in);
        }

        @Override
        public View_Profile_Pleger_Model[] newArray(int size) {
            return new View_Profile_Pleger_Model[size];
        }
    };

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getPledge__name() {
        return Pledge__name;
    }

    public void setPledge__name(String pledge__name) {
        Pledge__name = pledge__name;
    }

    public String getPledge__amount() {
        return Pledge__amount;
    }

    public void setPledge__amount(String pledge__amount) {
        Pledge__amount = pledge__amount;
    }

    public String getPledge__initiative_id() {
        return Pledge__initiative_id;
    }

    public void setPledge__initiative_id(String pledge__initiative_id) {
        Pledge__initiative_id = pledge__initiative_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Pledge__name);
        dest.writeString(Pledge__amount);
        dest.writeString(Pledge__initiative_id);
        dest.writeInt(is_deleted);
    }
}
