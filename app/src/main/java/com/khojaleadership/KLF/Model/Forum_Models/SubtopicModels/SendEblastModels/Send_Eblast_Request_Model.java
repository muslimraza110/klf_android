package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels;

public class Send_Eblast_Request_Model {
    String emails,content,subject,buttonlabel,buttonlink;

    public Send_Eblast_Request_Model() {
    }

    public Send_Eblast_Request_Model(String emails, String content, String subject, String buttonlabel, String buttonlink) {
        this.emails = emails;
        this.content = content;
        this.subject = subject;
        this.buttonlabel = buttonlabel;
        this.buttonlink = buttonlink;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getButtonlabel() {
        return buttonlabel;
    }

    public void setButtonlabel(String buttonlabel) {
        this.buttonlabel = buttonlabel;
    }

    public String getButtonlink() {
        return buttonlink;
    }

    public void setButtonlink(String buttonlink) {
        this.buttonlink = buttonlink;
    }
}
