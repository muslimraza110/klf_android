package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetSubTopicModel implements Parcelable {
    String status,message;
    ArrayList<GetSubTopicDataModel> data;

    public GetSubTopicModel() {
    }

    public GetSubTopicModel(String status, String message, ArrayList<GetSubTopicDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected GetSubTopicModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetSubTopicModel> CREATOR = new Creator<GetSubTopicModel>() {
        @Override
        public GetSubTopicModel createFromParcel(Parcel in) {
            return new GetSubTopicModel(in);
        }

        @Override
        public GetSubTopicModel[] newArray(int size) {
            return new GetSubTopicModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetSubTopicDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetSubTopicDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
