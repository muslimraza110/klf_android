package com.khojaleadership.KLF.Model.SplashLogin_Models;

public class LoginRequestModel {
    String email,password,fcm_token, platform;

    public LoginRequestModel(String email, String password, String fcm_token, String platform) {
        this.email = email;
        this.password = password;
        this.fcm_token = fcm_token;
        this.platform = platform;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public LoginRequestModel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
