package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Group_Model implements Parcelable {
    String group_id,Groups__category_id,Groups__name,Groups__description,Groups__email;
    String Groups__phone,Groups__address,Groups__address2,Groups__bio,Groups__groupimage;

    public View_Profile_Group_Model() {
    }

    public View_Profile_Group_Model(String group_id, String groups__category_id, String groups__name, String groups__description, String groups__email, String groups__phone, String groups__address, String groups__address2, String groups__bio, String groups__groupimage) {
        this.group_id = group_id;
        Groups__category_id = groups__category_id;
        Groups__name = groups__name;
        Groups__description = groups__description;
        Groups__email = groups__email;
        Groups__phone = groups__phone;
        Groups__address = groups__address;
        Groups__address2 = groups__address2;
        Groups__bio = groups__bio;
        Groups__groupimage = groups__groupimage;
    }

    protected View_Profile_Group_Model(Parcel in) {
        group_id = in.readString();
        Groups__category_id = in.readString();
        Groups__name = in.readString();
        Groups__description = in.readString();
        Groups__email = in.readString();
        Groups__phone = in.readString();
        Groups__address = in.readString();
        Groups__address2 = in.readString();
        Groups__bio = in.readString();
        Groups__groupimage = in.readString();
    }

    public static final Creator<View_Profile_Group_Model> CREATOR = new Creator<View_Profile_Group_Model>() {
        @Override
        public View_Profile_Group_Model createFromParcel(Parcel in) {
            return new View_Profile_Group_Model(in);
        }

        @Override
        public View_Profile_Group_Model[] newArray(int size) {
            return new View_Profile_Group_Model[size];
        }
    };

    public String getGroups__phone() {
        return Groups__phone;
    }

    public void setGroups__phone(String groups__phone) {
        Groups__phone = groups__phone;
    }

    public String getGroups__address() {
        return Groups__address;
    }

    public void setGroups__address(String groups__address) {
        Groups__address = groups__address;
    }

    public String getGroups__address2() {
        return Groups__address2;
    }

    public void setGroups__address2(String groups__address2) {
        Groups__address2 = groups__address2;
    }

    public String getGroups__bio() {
        return Groups__bio;
    }

    public void setGroups__bio(String groups__bio) {
        Groups__bio = groups__bio;
    }

    public String getGroups__groupimage() {
        return Groups__groupimage;
    }

    public void setGroups__groupimage(String groups__groupimage) {
        Groups__groupimage = groups__groupimage;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroups__category_id() {
        return Groups__category_id;
    }

    public void setGroups__category_id(String groups__category_id) {
        Groups__category_id = groups__category_id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }

    public String getGroups__email() {
        return Groups__email;
    }

    public void setGroups__email(String groups__email) {
        Groups__email = groups__email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(group_id);
        dest.writeString(Groups__category_id);
        dest.writeString(Groups__name);
        dest.writeString(Groups__description);
        dest.writeString(Groups__email);
        dest.writeString(Groups__phone);
        dest.writeString(Groups__address);
        dest.writeString(Groups__address2);
        dest.writeString(Groups__bio);
        dest.writeString(Groups__groupimage);
    }
}
