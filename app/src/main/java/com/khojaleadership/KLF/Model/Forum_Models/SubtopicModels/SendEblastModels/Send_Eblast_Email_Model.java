package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels;

public class Send_Eblast_Email_Model {
    String id,email;

    public Send_Eblast_Email_Model() {
    }

    public Send_Eblast_Email_Model(String id, String email) {
        this.id = id;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
