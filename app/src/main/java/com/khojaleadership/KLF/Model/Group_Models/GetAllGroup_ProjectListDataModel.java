package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class GetAllGroup_ProjectListDataModel implements Parcelable {

    private String letters;//Display the initials of the pinyin
    String group_id,category_id,name,description,email,address,city,region,country,bio,CreatedByUserId,groups_image;

    public GetAllGroup_ProjectListDataModel() {
    }

    public GetAllGroup_ProjectListDataModel(String letters, String group_id, String category_id, String name, String description, String email, String address, String city, String region, String country, String bio, String createdByUserId, String groups_image) {
        this.letters = letters;
        this.group_id = group_id;
        this.category_id = category_id;
        this.name = name;
        this.description = description;
        this.email = email;
        this.address = address;
        this.city = city;
        this.region = region;
        this.country = country;
        this.bio = bio;
        CreatedByUserId = createdByUserId;
        this.groups_image = groups_image;
    }

    protected GetAllGroup_ProjectListDataModel(Parcel in) {
        letters = in.readString();
        group_id = in.readString();
        category_id = in.readString();
        name = in.readString();
        description = in.readString();
        email = in.readString();
        address = in.readString();
        city = in.readString();
        region = in.readString();
        country = in.readString();
        bio = in.readString();
        CreatedByUserId = in.readString();
        groups_image = in.readString();
    }

    public static final Creator<GetAllGroup_ProjectListDataModel> CREATOR = new Creator<GetAllGroup_ProjectListDataModel>() {
        @Override
        public GetAllGroup_ProjectListDataModel createFromParcel(Parcel in) {
            return new GetAllGroup_ProjectListDataModel(in);
        }

        @Override
        public GetAllGroup_ProjectListDataModel[] newArray(int size) {
            return new GetAllGroup_ProjectListDataModel[size];
        }
    };

    public String getGroups_image() {
        return groups_image;
    }

    public void setGroups_image(String groups_image) {
        this.groups_image = groups_image;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }



    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCreatedByUserId() {
        return CreatedByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        CreatedByUserId = createdByUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(letters);
        dest.writeString(group_id);
        dest.writeString(category_id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(region);
        dest.writeString(country);
        dest.writeString(bio);
        dest.writeString(CreatedByUserId);
        dest.writeString(groups_image);
    }
}
