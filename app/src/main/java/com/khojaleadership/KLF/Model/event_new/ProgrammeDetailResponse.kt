package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName


data class ProgrammeDetailResponse(
//        @SerializedName("eventDetails") val eventResponse: SingleEventResponse,
        @SerializedName("program_data") val programmeDetails: List<ProgrammeDayResponse?>?
)

data class ProgrammeDayResponse(
        @SerializedName("program_day") val day: String?,
        @SerializedName("program_date") val date: String?,
        @SerializedName("program_details") val programmeItems: List<ProgrammeItemResponse?>?
)

data class ProgrammeItemResponse(
        @SerializedName("id") val id: String?,
//        @SerializedName("parentid") val parentId: String?,
//        @SerializedName("day") val day: String?,
//        @SerializedName("sort") val sort: String?,
        @SerializedName("start_time") val startTime: String?,
        @SerializedName("end_time") val endTime: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("event_session_type") val sessionType: String?,
        @SerializedName("can_request_a_meeting") val canRequestMeeting: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("check_my_availability_is_available") val checkMyAvailabilityIsAvailable: String?,
        @SerializedName("start_end_time") val startEndTime: String?,
        @SerializedName("speakers") val speakers: List<PersonDetailResponse?>?,
        @SerializedName("delegates") val delegates: List<PersonDetailResponse?>?,
        @SerializedName("meeting_request_details") val meetingRequestDetails: List<MeetingRequestDetails?>?,
        @SerializedName("session_details") val sessionDetails: List<SessionDetailResponse?>?
)

data class PersonDetailResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("summit_events_faculty_id") val summitEventsFacultyId: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("designation") val designation: String?,
        @SerializedName("short_description") val shortDescription: String?,
        @SerializedName("long_description") val longDescription: String?,
        @SerializedName("facebook_link") val facebookLink: String?,
        @SerializedName("twitter_link") val twitterLink: String?,
        @SerializedName("linkedin_link") val linkedInLink: String?,
        @SerializedName("user_image") val userImage: String?,
        @SerializedName("is_speaker") val isSpeaker: String?,
        @SerializedName("is_delegate") val isDelegate: String?,
        @SerializedName("summit_event_program_time") val speechTime: String?,
        @SerializedName("summit_event_program_date") val speechDate: String?
//        @SerializedName("meeting_slots") val meetingSlots: List<SummitEventDayAvailability>?
)

data class MeetingRequestDetails(
        @SerializedName("is_received") val isReceived: Boolean?,
        @SerializedName("requested_by_id") val requestedById: String?,
        @SerializedName("requested_to_id") val requestedToId: String?,
        @SerializedName("requested_to_details") val requestedToDetails: PersonDetailResponse?,
        @SerializedName("requested_by_details") val requestedByDetails: PersonDetailResponse?,
        @SerializedName("note") val meetingNotes: String?,
        @SerializedName("meeting_request_status") val meetingRequestStatus: List<MeetingRequestStatus?>?
)

data class MeetingRequestStatus(
        @SerializedName("status") val status: String?,
        @SerializedName("created") val created: String?
)


data class SessionDetailResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("parent_id") val parentId: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("room_limit") val roomLimit: Int?,
        @SerializedName("zoom_link") val zoomLink: String?,
        @SerializedName("room_occupied") val roomOccupied: Int?,
        @SerializedName("i_am_enrolled") val amIEnrolled: Boolean?,
        @SerializedName("delegates_enrolled") val enrolledDelegateSpeakers: List<PersonDetailResponse?>?,
        @SerializedName("session_faculties") val sessionFaculties: SessionFacultiesResponse?
)

data class SessionFacultiesResponse(
        @SerializedName("speakers") val speakers: List<SessionFaculty>?,
        @SerializedName("delegates") val delegates: List<SessionFaculty>?
)

data class SessionFaculty(
        @SerializedName("id") val id: String?,
        @SerializedName("summit_events_program_details_sessions_id") val sessionId: String?,
        @SerializedName("summit_events_faculty_id") val summitEventsFacultyId: String?,
        @SerializedName("is_speaker") val isSpeaker: String?,
        @SerializedName("is_delegate") val isDelegate: String?,
        @SerializedName("summit_events_session_faculties_detail") val facultyDetails: PersonDetailResponse?
)


data class SummitEventDayAvailability(
        @SerializedName("program_date") val programDate: String?,
        @SerializedName("program_day") val programDay: String?,
        @SerializedName("slots") val slots: MutableList<SummitEventTimeAvailability>?
)

data class SummitEventTimeAvailability(
        @SerializedName("parentid") val parentId: String?,
        @SerializedName("day") val day: String?,
        @SerializedName("start_time") val startTime: String?,
        @SerializedName("end_time") val endTime: String?,
        @SerializedName("event_start_date") val eventStartDate: String?,
        @SerializedName("current_status") val currentStatus: String?,
        @SerializedName("times") val time: String?,
        @SerializedName("program_id") val programId: String?
)


enum class SessionType(val value: String) {
    PLENARY("plenary"),
    BREAKOUT("breakout")
}