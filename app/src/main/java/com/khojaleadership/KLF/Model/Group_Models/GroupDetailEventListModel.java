package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class GroupDetailEventListModel {
    String status,message;
    ArrayList<GroupDetailEventListDataModel> data;

    public GroupDetailEventListModel() {
    }

    public GroupDetailEventListModel(String status, String message, ArrayList<GroupDetailEventListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GroupDetailEventListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GroupDetailEventListDataModel> data) {
        this.data = data;
    }
}
