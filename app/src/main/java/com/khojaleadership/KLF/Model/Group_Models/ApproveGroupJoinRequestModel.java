package com.khojaleadership.KLF.Model.Group_Models;

public class ApproveGroupJoinRequestModel {
    String group_id,user_id,pending_request_id;

    public ApproveGroupJoinRequestModel() {
    }

    public ApproveGroupJoinRequestModel(String group_id, String user_id, String pending_request_id) {
        this.group_id = group_id;
        this.user_id = user_id;
        this.pending_request_id = pending_request_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPending_request_id() {
        return pending_request_id;
    }

    public void setPending_request_id(String pending_request_id) {
        this.pending_request_id = pending_request_id;
    }
}
