package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class EventSectionsResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("summit_events_id") val eventId: String?,
//        @SerializedName("background_image") val backgroundImage: String,
        @SerializedName("tag") val tag: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("short_description") val shortDescription: String?
)
