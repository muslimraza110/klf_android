package com.khojaleadership.KLF.Model.SplashLogin_Models;

public class ForgetPasswordResponseModel {
    String status,message;

    public ForgetPasswordResponseModel() {
    }

    public ForgetPasswordResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

