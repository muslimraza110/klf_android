package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class EditPostRequestModel {
    String forum_post_id,topic,content;

    public EditPostRequestModel() {
    }

    public EditPostRequestModel(String forum_post_id, String topic, String content) {
        this.forum_post_id = forum_post_id;
        this.topic = topic;
        this.content = content;
    }

    public String getForum_post_id() {
        return forum_post_id;
    }

    public void setForum_post_id(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
