package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Activity_Model {

    String tv_title;
    String tv_admin;
    String tv_amount;

    int progress;


    public Intiative_Activity_Model(String tv_title, String tv_admin, String tv_amount, int progress) {
        this.tv_title = tv_title;
        this.tv_admin = tv_admin;
        this.tv_amount = tv_amount;
        this.progress = progress;
    }

    public String getTv_title() {
        return tv_title;
    }

    public void setTv_title(String tv_title) {
        this.tv_title = tv_title;
    }

    public String getTv_admin() {
        return tv_admin;
    }

    public void setTv_admin(String tv_admin) {
        this.tv_admin = tv_admin;
    }

    public String getTv_amount() {
        return tv_amount;
    }

    public void setTv_amount(String tv_amount) {
        this.tv_amount = tv_amount;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
