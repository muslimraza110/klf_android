package com.khojaleadership.KLF.Model.Project_Models;

public class ProjectsDetailGroupDataModel {

    String ProjectsGroups__group_id,ProjectsGroups__id,ProjectsGroups__project_id,ProjectsGroups__percent,Groups__id;
    String Groups__category_id,Groups__name,Groups__description,Groups__email,Groups__phone,Groups__address,Groups__address2;
    String Groups__website,Groups__city,Groups__region,Groups__country,Groups__bio;
    String group_image;

    public ProjectsDetailGroupDataModel() {
    }


    public ProjectsDetailGroupDataModel(String projectsGroups__group_id, String projectsGroups__id, String projectsGroups__project_id, String projectsGroups__percent, String groups__id, String groups__category_id, String groups__name, String groups__description, String groups__email, String groups__phone, String groups__address, String groups__address2, String groups__website, String groups__city, String groups__region, String groups__country, String groups__bio, String group_image) {
        ProjectsGroups__group_id = projectsGroups__group_id;
        ProjectsGroups__id = projectsGroups__id;
        ProjectsGroups__project_id = projectsGroups__project_id;
        ProjectsGroups__percent = projectsGroups__percent;
        Groups__id = groups__id;
        Groups__category_id = groups__category_id;
        Groups__name = groups__name;
        Groups__description = groups__description;
        Groups__email = groups__email;
        Groups__phone = groups__phone;
        Groups__address = groups__address;
        Groups__address2 = groups__address2;
        Groups__website = groups__website;
        Groups__city = groups__city;
        Groups__region = groups__region;
        Groups__country = groups__country;
        Groups__bio = groups__bio;
        this.group_image = group_image;
    }

    public String getGroup_image() {
        return group_image;
    }

    public void setGroup_image(String group_image) {
        this.group_image = group_image;
    }

    public String getProjectsGroups__group_id() {
        return ProjectsGroups__group_id;
    }

    public void setProjectsGroups__group_id(String projectsGroups__group_id) {
        ProjectsGroups__group_id = projectsGroups__group_id;
    }

    public String getProjectsGroups__id() {
        return ProjectsGroups__id;
    }

    public void setProjectsGroups__id(String projectsGroups__id) {
        ProjectsGroups__id = projectsGroups__id;
    }

    public String getProjectsGroups__project_id() {
        return ProjectsGroups__project_id;
    }

    public void setProjectsGroups__project_id(String projectsGroups__project_id) {
        ProjectsGroups__project_id = projectsGroups__project_id;
    }

    public String getProjectsGroups__percent() {
        return ProjectsGroups__percent;
    }

    public void setProjectsGroups__percent(String projectsGroups__percent) {
        ProjectsGroups__percent = projectsGroups__percent;
    }

    public String getGroups__id() {
        return Groups__id;
    }

    public void setGroups__id(String groups__id) {
        Groups__id = groups__id;
    }

    public String getGroups__category_id() {
        return Groups__category_id;
    }

    public void setGroups__category_id(String groups__category_id) {
        Groups__category_id = groups__category_id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }

    public String getGroups__email() {
        return Groups__email;
    }

    public void setGroups__email(String groups__email) {
        Groups__email = groups__email;
    }

    public String getGroups__phone() {
        return Groups__phone;
    }

    public void setGroups__phone(String groups__phone) {
        Groups__phone = groups__phone;
    }

    public String getGroups__address() {
        return Groups__address;
    }

    public void setGroups__address(String groups__address) {
        Groups__address = groups__address;
    }

    public String getGroups__address2() {
        return Groups__address2;
    }

    public void setGroups__address2(String groups__address2) {
        Groups__address2 = groups__address2;
    }

    public String getGroups__website() {
        return Groups__website;
    }

    public void setGroups__website(String groups__website) {
        Groups__website = groups__website;
    }

    public String getGroups__city() {
        return Groups__city;
    }

    public void setGroups__city(String groups__city) {
        Groups__city = groups__city;
    }

    public String getGroups__region() {
        return Groups__region;
    }

    public void setGroups__region(String groups__region) {
        Groups__region = groups__region;
    }

    public String getGroups__country() {
        return Groups__country;
    }

    public void setGroups__country(String groups__country) {
        Groups__country = groups__country;
    }

    public String getGroups__bio() {
        return Groups__bio;
    }

    public void setGroups__bio(String groups__bio) {
        Groups__bio = groups__bio;
    }
}
