package com.khojaleadership.KLF.Model.Project_Models;

public class Project_Access_Request_List_Data_Model {
    String project_id,user_id,fullname;

    public Project_Access_Request_List_Data_Model() {
    }

    public Project_Access_Request_List_Data_Model(String project_id, String user_id, String fullname) {
        this.project_id = project_id;
        this.user_id = user_id;
        this.fullname = fullname;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
