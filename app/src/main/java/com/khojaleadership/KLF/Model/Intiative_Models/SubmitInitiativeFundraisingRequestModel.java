package com.khojaleadership.KLF.Model.Intiative_Models;

public class SubmitInitiativeFundraisingRequestModel {

    String
    user_id,  group_id, fundraising_id, anonymous, hide_amount, pledge_blocks, amount;

    public SubmitInitiativeFundraisingRequestModel() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getFundraising_id() {
        return fundraising_id;
    }

    public void setFundraising_id(String fundraising_id) {
        this.fundraising_id = fundraising_id;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getHide_amount() {
        return hide_amount;
    }

    public void setHide_amount(String hide_amount) {
        this.hide_amount = hide_amount;
    }

    public String getPledge_blocks() {
        return pledge_blocks;
    }

    public void setPledge_blocks(String pledge_blocks) {
        this.pledge_blocks = pledge_blocks;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}

