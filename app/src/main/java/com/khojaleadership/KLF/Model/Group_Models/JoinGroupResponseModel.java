package com.khojaleadership.KLF.Model.Group_Models;

public class JoinGroupResponseModel {
    String status,message;

    public JoinGroupResponseModel() {
    }

    public JoinGroupResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
