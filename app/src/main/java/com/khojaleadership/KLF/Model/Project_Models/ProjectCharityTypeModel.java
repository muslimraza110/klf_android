package com.khojaleadership.KLF.Model.Project_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class ProjectCharityTypeModel implements Parcelable {
    String id,name,project_id;

    public ProjectCharityTypeModel() {
    }

    public ProjectCharityTypeModel(String id, String name, String project_id) {
        this.id = id;
        this.name = name;
        this.project_id = project_id;
    }

    protected ProjectCharityTypeModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        project_id = in.readString();
    }

    public static final Creator<ProjectCharityTypeModel> CREATOR = new Creator<ProjectCharityTypeModel>() {
        @Override
        public ProjectCharityTypeModel createFromParcel(Parcel in) {
            return new ProjectCharityTypeModel(in);
        }

        @Override
        public ProjectCharityTypeModel[] newArray(int size) {
            return new ProjectCharityTypeModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(project_id);
    }
}
