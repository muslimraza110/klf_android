package com.khojaleadership.KLF.Model.Project_Models;

public class Remove_Group_From_ProjectResponseModel {
    String status,message;

    public Remove_Group_From_ProjectResponseModel() {
    }

    public Remove_Group_From_ProjectResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
