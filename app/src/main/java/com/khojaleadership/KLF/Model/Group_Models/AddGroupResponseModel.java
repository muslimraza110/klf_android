package com.khojaleadership.KLF.Model.Group_Models;

public class AddGroupResponseModel {
    String status,message;

    public AddGroupResponseModel() {
    }

    public AddGroupResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
