package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class GetGroupListDataModel implements Parcelable {
    private String letters;//Display the initials of the pinyin
    String createdByUserID;
    String Groups__id,Groups__category_id,Groups__name,Groups__description,Groups__email,Groups__phone;
    String Groups__address,Groups__address2,Groups__website,Groups__city,Groups__region,Groups__country;
    String Groups__postal_code,Groups__bio,Groups__hidden,Groups__pending,Groups__vote_decision,Groups__vote_decision_date;
    String Groups__created,Groups__status,Groups__donations,Groups__passive,Categories__id,Categories__model,Categories__name,Categories__description;
    String is_member;
    String groupimage;
    public GetGroupListDataModel() {
    }

    public GetGroupListDataModel(String letters, String createdByUserID, String groups__id, String groups__category_id, String groups__name, String groups__description, String groups__email, String groups__phone, String groups__address, String groups__address2, String groups__website, String groups__city, String groups__region, String groups__country, String groups__postal_code, String groups__bio, String groups__hidden, String groups__pending, String groups__vote_decision, String groups__vote_decision_date, String groups__created, String groups__status, String groups__donations, String groups__passive, String categories__id, String categories__model, String categories__name, String categories__description, String is_member, String groupimage) {
        this.letters = letters;
        this.createdByUserID = createdByUserID;
        Groups__id = groups__id;
        Groups__category_id = groups__category_id;
        Groups__name = groups__name;
        Groups__description = groups__description;
        Groups__email = groups__email;
        Groups__phone = groups__phone;
        Groups__address = groups__address;
        Groups__address2 = groups__address2;
        Groups__website = groups__website;
        Groups__city = groups__city;
        Groups__region = groups__region;
        Groups__country = groups__country;
        Groups__postal_code = groups__postal_code;
        Groups__bio = groups__bio;
        Groups__hidden = groups__hidden;
        Groups__pending = groups__pending;
        Groups__vote_decision = groups__vote_decision;
        Groups__vote_decision_date = groups__vote_decision_date;
        Groups__created = groups__created;
        Groups__status = groups__status;
        Groups__donations = groups__donations;
        Groups__passive = groups__passive;
        Categories__id = categories__id;
        Categories__model = categories__model;
        Categories__name = categories__name;
        Categories__description = categories__description;
        this.is_member = is_member;
        this.groupimage = groupimage;
    }

    protected GetGroupListDataModel(Parcel in) {
        letters = in.readString();
        createdByUserID = in.readString();
        Groups__id = in.readString();
        Groups__category_id = in.readString();
        Groups__name = in.readString();
        Groups__description = in.readString();
        Groups__email = in.readString();
        Groups__phone = in.readString();
        Groups__address = in.readString();
        Groups__address2 = in.readString();
        Groups__website = in.readString();
        Groups__city = in.readString();
        Groups__region = in.readString();
        Groups__country = in.readString();
        Groups__postal_code = in.readString();
        Groups__bio = in.readString();
        Groups__hidden = in.readString();
        Groups__pending = in.readString();
        Groups__vote_decision = in.readString();
        Groups__vote_decision_date = in.readString();
        Groups__created = in.readString();
        Groups__status = in.readString();
        Groups__donations = in.readString();
        Groups__passive = in.readString();
        Categories__id = in.readString();
        Categories__model = in.readString();
        Categories__name = in.readString();
        Categories__description = in.readString();
        is_member = in.readString();
        groupimage = in.readString();
    }

    public static final Creator<GetGroupListDataModel> CREATOR = new Creator<GetGroupListDataModel>() {
        @Override
        public GetGroupListDataModel createFromParcel(Parcel in) {
            return new GetGroupListDataModel(in);
        }

        @Override
        public GetGroupListDataModel[] newArray(int size) {
            return new GetGroupListDataModel[size];
        }
    };

    public String getGroupimage() {
        return groupimage;
    }

    public void setGroupimage(String groupimage) {
        this.groupimage = groupimage;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }

    public String getCreatedByUserID() {
        return createdByUserID;
    }

    public void setCreatedByUserID(String createdByUserID) {
        this.createdByUserID = createdByUserID;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getGroups__id() {
        return Groups__id;
    }

    public void setGroups__id(String groups__id) {
        Groups__id = groups__id;
    }

    public String getGroups__category_id() {
        return Groups__category_id;
    }

    public void setGroups__category_id(String groups__category_id) {
        Groups__category_id = groups__category_id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }

    public String getGroups__email() {
        return Groups__email;
    }

    public void setGroups__email(String groups__email) {
        Groups__email = groups__email;
    }

    public String getGroups__phone() {
        return Groups__phone;
    }

    public void setGroups__phone(String groups__phone) {
        Groups__phone = groups__phone;
    }

    public String getGroups__address() {
        return Groups__address;
    }

    public void setGroups__address(String groups__address) {
        Groups__address = groups__address;
    }

    public String getGroups__address2() {
        return Groups__address2;
    }

    public void setGroups__address2(String groups__address2) {
        Groups__address2 = groups__address2;
    }

    public String getGroups__website() {
        return Groups__website;
    }

    public void setGroups__website(String groups__website) {
        Groups__website = groups__website;
    }

    public String getGroups__city() {
        return Groups__city;
    }

    public void setGroups__city(String groups__city) {
        Groups__city = groups__city;
    }

    public String getGroups__region() {
        return Groups__region;
    }

    public void setGroups__region(String groups__region) {
        Groups__region = groups__region;
    }

    public String getGroups__country() {
        return Groups__country;
    }

    public void setGroups__country(String groups__country) {
        Groups__country = groups__country;
    }

    public String getGroups__postal_code() {
        return Groups__postal_code;
    }

    public void setGroups__postal_code(String groups__postal_code) {
        Groups__postal_code = groups__postal_code;
    }

    public String getGroups__bio() {
        return Groups__bio;
    }

    public void setGroups__bio(String groups__bio) {
        Groups__bio = groups__bio;
    }

    public String getGroups__hidden() {
        return Groups__hidden;
    }

    public void setGroups__hidden(String groups__hidden) {
        Groups__hidden = groups__hidden;
    }

    public String getGroups__pending() {
        return Groups__pending;
    }

    public void setGroups__pending(String groups__pending) {
        Groups__pending = groups__pending;
    }

    public String getGroups__vote_decision() {
        return Groups__vote_decision;
    }

    public void setGroups__vote_decision(String groups__vote_decision) {
        Groups__vote_decision = groups__vote_decision;
    }

    public String getGroups__vote_decision_date() {
        return Groups__vote_decision_date;
    }

    public void setGroups__vote_decision_date(String groups__vote_decision_date) {
        Groups__vote_decision_date = groups__vote_decision_date;
    }

    public String getGroups__created() {
        return Groups__created;
    }

    public void setGroups__created(String groups__created) {
        Groups__created = groups__created;
    }

    public String getGroups__status() {
        return Groups__status;
    }

    public void setGroups__status(String groups__status) {
        Groups__status = groups__status;
    }

    public String getGroups__donations() {
        return Groups__donations;
    }

    public void setGroups__donations(String groups__donations) {
        Groups__donations = groups__donations;
    }

    public String getGroups__passive() {
        return Groups__passive;
    }

    public void setGroups__passive(String groups__passive) {
        Groups__passive = groups__passive;
    }

    public String getCategories__id() {
        return Categories__id;
    }

    public void setCategories__id(String categories__id) {
        Categories__id = categories__id;
    }

    public String getCategories__model() {
        return Categories__model;
    }

    public void setCategories__model(String categories__model) {
        Categories__model = categories__model;
    }

    public String getCategories__name() {
        return Categories__name;
    }

    public void setCategories__name(String categories__name) {
        Categories__name = categories__name;
    }

    public String getCategories__description() {
        return Categories__description;
    }

    public void setCategories__description(String categories__description) {
        Categories__description = categories__description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(letters);
        dest.writeString(createdByUserID);
        dest.writeString(Groups__id);
        dest.writeString(Groups__category_id);
        dest.writeString(Groups__name);
        dest.writeString(Groups__description);
        dest.writeString(Groups__email);
        dest.writeString(Groups__phone);
        dest.writeString(Groups__address);
        dest.writeString(Groups__address2);
        dest.writeString(Groups__website);
        dest.writeString(Groups__city);
        dest.writeString(Groups__region);
        dest.writeString(Groups__country);
        dest.writeString(Groups__postal_code);
        dest.writeString(Groups__bio);
        dest.writeString(Groups__hidden);
        dest.writeString(Groups__pending);
        dest.writeString(Groups__vote_decision);
        dest.writeString(Groups__vote_decision_date);
        dest.writeString(Groups__created);
        dest.writeString(Groups__status);
        dest.writeString(Groups__donations);
        dest.writeString(Groups__passive);
        dest.writeString(Categories__id);
        dest.writeString(Categories__model);
        dest.writeString(Categories__name);
        dest.writeString(Categories__description);
        dest.writeString(is_member);
        dest.writeString(groupimage);
    }
}
