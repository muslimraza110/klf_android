package com.khojaleadership.KLF.Model.SplashLogin_Models;

public class LoginResponseDataModel {
    String id,role,email,title,first_name,middle_name,last_name,gender,status,chair_person;
    String chat_status,last_online,last_login,expiration_date,pending,vote_decision,vote_decision_date;
    String vote_admin_decision,terms,created,modified,reset_key;
    String profile_img;
    String auth_token;


    public LoginResponseDataModel() {
    }

    public LoginResponseDataModel(String id, String role, String email, String title, String first_name, String middle_name, String last_name, String gender, String status, String chair_person, String chat_status, String last_online, String last_login, String expiration_date, String pending, String vote_decision, String vote_decision_date, String vote_admin_decision, String terms, String created, String modified, String reset_key, String profile_img, String auth_token) {
        this.id = id;
        this.role = role;
        this.email = email;
        this.title = title;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.gender = gender;
        this.status = status;
        this.chair_person = chair_person;
        this.chat_status = chat_status;
        this.last_online = last_online;
        this.last_login = last_login;
        this.expiration_date = expiration_date;
        this.pending = pending;
        this.vote_decision = vote_decision;
        this.vote_decision_date = vote_decision_date;
        this.vote_admin_decision = vote_admin_decision;
        this.terms = terms;
        this.created = created;
        this.modified = modified;
        this.reset_key = reset_key;
        this.profile_img = profile_img;
        this.auth_token = auth_token;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }



    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChair_person() {
        return chair_person;
    }

    public void setChair_person(String chair_person) {
        this.chair_person = chair_person;
    }

    public String getChat_status() {
        return chat_status;
    }

    public void setChat_status(String chat_status) {
        this.chat_status = chat_status;
    }

    public String getLast_online() {
        return last_online;
    }

    public void setLast_online(String last_online) {
        this.last_online = last_online;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getVote_decision() {
        return vote_decision;
    }

    public void setVote_decision(String vote_decision) {
        this.vote_decision = vote_decision;
    }

    public String getVote_decision_date() {
        return vote_decision_date;
    }

    public void setVote_decision_date(String vote_decision_date) {
        this.vote_decision_date = vote_decision_date;
    }

    public String getVote_admin_decision() {
        return vote_admin_decision;
    }

    public void setVote_admin_decision(String vote_admin_decision) {
        this.vote_admin_decision = vote_admin_decision;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getReset_key() {
        return reset_key;
    }

    public void setReset_key(String reset_key) {
        this.reset_key = reset_key;
    }
}
