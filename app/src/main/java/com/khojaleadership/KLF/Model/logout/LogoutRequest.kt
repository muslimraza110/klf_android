package com.khojaleadership.KLF.Model.logout

import com.google.gson.annotations.SerializedName

data class LogoutRequest(
        @SerializedName("user_id") val userId: String?,
        @SerializedName("fcm_token") val fcmToken: String?
)