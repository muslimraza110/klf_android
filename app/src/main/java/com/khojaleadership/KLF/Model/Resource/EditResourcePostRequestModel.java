package com.khojaleadership.KLF.Model.Resource;

public class EditResourcePostRequestModel {
    String user_id,category_id,topic,name,content,post_id,post_theme_id,published;

    public EditResourcePostRequestModel() {
    }

    public EditResourcePostRequestModel(String user_id, String category_id, String topic, String name, String content, String post_id, String post_theme_id, String published) {
        this.user_id = user_id;
        this.category_id = category_id;
        this.topic = topic;
        this.name = name;
        this.content = content;
        this.post_id = post_id;
        this.post_theme_id = post_theme_id;
        this.published = published;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_theme_id() {
        return post_theme_id;
    }

    public void setPost_theme_id(String post_theme_id) {
        this.post_theme_id = post_theme_id;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }
}
