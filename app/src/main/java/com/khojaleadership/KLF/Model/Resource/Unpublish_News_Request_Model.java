package com.khojaleadership.KLF.Model.Resource;

public class Unpublish_News_Request_Model {
    String post_id;

    public Unpublish_News_Request_Model() {
    }

    public Unpublish_News_Request_Model(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
