package com.khojaleadership.KLF.Model.Intiative_Models;

import java.util.ArrayList;

public class Intiative_Detail_Data_Fundraising_Model {
    String Fundraisings__id,Fundraisings__name,Fundraisings__description,Fundraisings__amount,Fundraisings__type,Fundraisings__blocks;
    String Fundraisings__max_blocks,Fundraisings__allow_hidden_amount,Fundraisings__allow_anonymous,Fundraisings__broadcast;
    String Fundraisings__fundraising_status,Fundraisings__created,Fundraisings__status,totalfundraising,fundrasingcollectedamount;
    double Pledge_1_Block;
    int is_plege;

    ArrayList<Intiative_Detail_Fundraising_Matcher_Model> matchers;
    public Intiative_Detail_Data_Fundraising_Model() {
    }

    public Intiative_Detail_Data_Fundraising_Model(String fundraisings__id, String fundraisings__name, String fundraisings__description, String fundraisings__amount, String fundraisings__type, String fundraisings__blocks, String fundraisings__max_blocks, String fundraisings__allow_hidden_amount, String fundraisings__allow_anonymous, String fundraisings__broadcast, String fundraisings__fundraising_status, String fundraisings__created, String fundraisings__status, String totalfundraising, String fundrasingcollectedamount, double pledge_1_Block, int is_plege, ArrayList<Intiative_Detail_Fundraising_Matcher_Model> matchers) {
        Fundraisings__id = fundraisings__id;
        Fundraisings__name = fundraisings__name;
        Fundraisings__description = fundraisings__description;
        Fundraisings__amount = fundraisings__amount;
        Fundraisings__type = fundraisings__type;
        Fundraisings__blocks = fundraisings__blocks;
        Fundraisings__max_blocks = fundraisings__max_blocks;
        Fundraisings__allow_hidden_amount = fundraisings__allow_hidden_amount;
        Fundraisings__allow_anonymous = fundraisings__allow_anonymous;
        Fundraisings__broadcast = fundraisings__broadcast;
        Fundraisings__fundraising_status = fundraisings__fundraising_status;
        Fundraisings__created = fundraisings__created;
        Fundraisings__status = fundraisings__status;
        this.totalfundraising = totalfundraising;
        this.fundrasingcollectedamount = fundrasingcollectedamount;
        Pledge_1_Block = pledge_1_Block;
        this.is_plege = is_plege;
        this.matchers = matchers;
    }

    public ArrayList<Intiative_Detail_Fundraising_Matcher_Model> getMatchers() {
        return matchers;
    }

    public void setMatchers(ArrayList<Intiative_Detail_Fundraising_Matcher_Model> matchers) {
        this.matchers = matchers;
    }

    public String getFundraisings__id() {
        return Fundraisings__id;
    }

    public void setFundraisings__id(String fundraisings__id) {
        Fundraisings__id = fundraisings__id;
    }

    public String getFundraisings__name() {
        return Fundraisings__name;
    }

    public void setFundraisings__name(String fundraisings__name) {
        Fundraisings__name = fundraisings__name;
    }

    public String getFundraisings__description() {
        return Fundraisings__description;
    }

    public void setFundraisings__description(String fundraisings__description) {
        Fundraisings__description = fundraisings__description;
    }

    public String getFundraisings__amount() {
        return Fundraisings__amount;
    }

    public void setFundraisings__amount(String fundraisings__amount) {
        Fundraisings__amount = fundraisings__amount;
    }

    public String getFundraisings__type() {
        return Fundraisings__type;
    }

    public void setFundraisings__type(String fundraisings__type) {
        Fundraisings__type = fundraisings__type;
    }

    public String getFundraisings__blocks() {
        return Fundraisings__blocks;
    }

    public void setFundraisings__blocks(String fundraisings__blocks) {
        Fundraisings__blocks = fundraisings__blocks;
    }

    public String getFundraisings__max_blocks() {
        return Fundraisings__max_blocks;
    }

    public void setFundraisings__max_blocks(String fundraisings__max_blocks) {
        Fundraisings__max_blocks = fundraisings__max_blocks;
    }

    public String getFundraisings__allow_hidden_amount() {
        return Fundraisings__allow_hidden_amount;
    }

    public void setFundraisings__allow_hidden_amount(String fundraisings__allow_hidden_amount) {
        Fundraisings__allow_hidden_amount = fundraisings__allow_hidden_amount;
    }

    public String getFundraisings__allow_anonymous() {
        return Fundraisings__allow_anonymous;
    }

    public void setFundraisings__allow_anonymous(String fundraisings__allow_anonymous) {
        Fundraisings__allow_anonymous = fundraisings__allow_anonymous;
    }

    public String getFundraisings__broadcast() {
        return Fundraisings__broadcast;
    }

    public void setFundraisings__broadcast(String fundraisings__broadcast) {
        Fundraisings__broadcast = fundraisings__broadcast;
    }

    public String getFundraisings__fundraising_status() {
        return Fundraisings__fundraising_status;
    }

    public void setFundraisings__fundraising_status(String fundraisings__fundraising_status) {
        Fundraisings__fundraising_status = fundraisings__fundraising_status;
    }

    public String getFundraisings__created() {
        return Fundraisings__created;
    }

    public void setFundraisings__created(String fundraisings__created) {
        Fundraisings__created = fundraisings__created;
    }

    public String getFundraisings__status() {
        return Fundraisings__status;
    }

    public void setFundraisings__status(String fundraisings__status) {
        Fundraisings__status = fundraisings__status;
    }

    public String getTotalfundraising() {
        return totalfundraising;
    }

    public void setTotalfundraising(String totalfundraising) {
        this.totalfundraising = totalfundraising;
    }

    public String getFundrasingcollectedamount() {
        return fundrasingcollectedamount;
    }

    public void setFundrasingcollectedamount(String fundrasingcollectedamount) {
        this.fundrasingcollectedamount = fundrasingcollectedamount;
    }

    public double getPledge_1_Block() {
        return Pledge_1_Block;
    }

    public void setPledge_1_Block(double pledge_1_Block) {
        Pledge_1_Block = pledge_1_Block;
    }

    public int getIs_plege() {
        return is_plege;
    }

    public void setIs_plege(int is_plege) {
        this.is_plege = is_plege;
    }
}
