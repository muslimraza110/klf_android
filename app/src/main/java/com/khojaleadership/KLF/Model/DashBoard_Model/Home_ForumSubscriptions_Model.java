package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_ForumSubscriptions_Model {
    String ForumPosts__topic,ForumPostSubscriptions__id,ForumPosts__id,postaprovepost,ForumPosts__created;

    public Home_ForumSubscriptions_Model() {
    }

    public Home_ForumSubscriptions_Model(String forumPosts__topic, String forumPostSubscriptions__id, String forumPosts__id, String postaprovepost, String forumPosts__created) {
        ForumPosts__topic = forumPosts__topic;
        ForumPostSubscriptions__id = forumPostSubscriptions__id;
        ForumPosts__id = forumPosts__id;
        this.postaprovepost = postaprovepost;
        ForumPosts__created = forumPosts__created;
    }

    public String getForumPostSubscriptions__id() {
        return ForumPostSubscriptions__id;
    }

    public void setForumPostSubscriptions__id(String forumPostSubscriptions__id) {
        ForumPostSubscriptions__id = forumPostSubscriptions__id;
    }

    public String getForumPosts__id() {
        return ForumPosts__id;
    }

    public void setForumPosts__id(String forumPosts__id) {
        ForumPosts__id = forumPosts__id;
    }

    public String getPostaprovepost() {
        return postaprovepost;
    }

    public void setPostaprovepost(String postaprovepost) {
        this.postaprovepost = postaprovepost;
    }

    public String getForumPosts__created() {
        return ForumPosts__created;
    }

    public void setForumPosts__created(String forumPosts__created) {
        ForumPosts__created = forumPosts__created;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }
}
