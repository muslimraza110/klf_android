package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

class JoinBreakoutSessionResponse(
        @SerializedName("___") val s: String
)