package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class A_GetPostListDataModel {

    String ForumPosts__id,ForumPosts__user_id,ForumPosts__parent_id,ForumPosts__subforum_id;
    String ForumPosts__lft,ForumPosts__rght,ForumPosts__topic;
    String ForumPosts__content,ForumPosts__pending,ForumPosts__moderated,ForumPosts__pinned;

    String Users__title,Users__first_name,Users__last_name,ForumPosts__created,Users__last_login;

    String Posts,likescount,dislikescount;
    int islike,isdislike;
    String frequency;

    public A_GetPostListDataModel() {
    }



    public A_GetPostListDataModel(String forumPosts__id, String forumPosts__user_id, String forumPosts__parent_id, String forumPosts__subforum_id, String forumPosts__lft, String forumPosts__rght, String forumPosts__topic, String forumPosts__content, String forumPosts__pending, String forumPosts__moderated, String forumPosts__pinned, String users__title, String users__first_name, String users__last_name, String forumPosts__created, String users__last_login, String posts, String likescount, String dislikescount, int islike, int isdislike, String frequency) {
        ForumPosts__id = forumPosts__id;
        ForumPosts__user_id = forumPosts__user_id;
        ForumPosts__parent_id = forumPosts__parent_id;
        ForumPosts__subforum_id = forumPosts__subforum_id;
        ForumPosts__lft = forumPosts__lft;
        ForumPosts__rght = forumPosts__rght;
        ForumPosts__topic = forumPosts__topic;
        ForumPosts__content = forumPosts__content;
        ForumPosts__pending = forumPosts__pending;
        ForumPosts__moderated = forumPosts__moderated;
        ForumPosts__pinned = forumPosts__pinned;
        Users__title = users__title;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        ForumPosts__created = forumPosts__created;
        Users__last_login = users__last_login;
        Posts = posts;
        this.likescount = likescount;
        this.dislikescount = dislikescount;
        this.islike = islike;
        this.isdislike = isdislike;
        this.frequency = frequency;
    }

    public int getIslike() {
        return islike;
    }

    public void setIslike(int islike) {
        this.islike = islike;
    }

    public int getIsdislike() {
        return isdislike;
    }

    public void setIsdislike(int isdislike) {
        this.isdislike = isdislike;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getPosts() {
        return Posts;
    }

    public void setPosts(String posts) {
        Posts = posts;
    }

    public String getLikescount() {
        return likescount;
    }

    public void setLikescount(String likescount) {
        this.likescount = likescount;
    }

    public String getDislikescount() {
        return dislikescount;
    }

    public void setDislikescount(String dislikescount) {
        this.dislikescount = dislikescount;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getForumPosts__created() {
        return ForumPosts__created;
    }

    public void setForumPosts__created(String forumPosts__created) {
        ForumPosts__created = forumPosts__created;
    }

    public String getUsers__last_login() {
        return Users__last_login;
    }

    public void setUsers__last_login(String users__last_login) {
        Users__last_login = users__last_login;
    }

    public String getForumPosts__id() {
        return ForumPosts__id;
    }

    public void setForumPosts__id(String forumPosts__id) {
        ForumPosts__id = forumPosts__id;
    }

    public String getForumPosts__user_id() {
        return ForumPosts__user_id;
    }

    public void setForumPosts__user_id(String forumPosts__user_id) {
        ForumPosts__user_id = forumPosts__user_id;
    }

    public String getForumPosts__parent_id() {
        return ForumPosts__parent_id;
    }

    public void setForumPosts__parent_id(String forumPosts__parent_id) {
        ForumPosts__parent_id = forumPosts__parent_id;
    }

    public String getForumPosts__subforum_id() {
        return ForumPosts__subforum_id;
    }

    public void setForumPosts__subforum_id(String forumPosts__subforum_id) {
        ForumPosts__subforum_id = forumPosts__subforum_id;
    }

    public String getForumPosts__lft() {
        return ForumPosts__lft;
    }

    public void setForumPosts__lft(String forumPosts__lft) {
        ForumPosts__lft = forumPosts__lft;
    }

    public String getForumPosts__rght() {
        return ForumPosts__rght;
    }

    public void setForumPosts__rght(String forumPosts__rght) {
        ForumPosts__rght = forumPosts__rght;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }

    public String getForumPosts__content() {
        return ForumPosts__content;
    }

    public void setForumPosts__content(String forumPosts__content) {
        ForumPosts__content = forumPosts__content;
    }

    public String getForumPosts__pending() {
        return ForumPosts__pending;
    }

    public void setForumPosts__pending(String forumPosts__pending) {
        ForumPosts__pending = forumPosts__pending;
    }

    public String getForumPosts__moderated() {
        return ForumPosts__moderated;
    }

    public void setForumPosts__moderated(String forumPosts__moderated) {
        ForumPosts__moderated = forumPosts__moderated;
    }

    public String getForumPosts__pinned() {
        return ForumPosts__pinned;
    }

    public void setForumPosts__pinned(String forumPosts__pinned) {
        ForumPosts__pinned = forumPosts__pinned;
    }
}
