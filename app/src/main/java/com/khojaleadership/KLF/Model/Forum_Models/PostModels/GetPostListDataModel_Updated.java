package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetPostListDataModel_Updated implements Parcelable{
    String ForumPosts__id,ForumPosts__user_id,ForumPosts__subforum_id,ForumPosts__topic,ForumPosts__content,ForumPosts__created;
    String ForumPosts__pending,ForumPosts__moderated,ForumPosts__pinned,ForumPosts__sod,ForumPosts__notify_users;
    String ForumPosts__visibility,ForumPosts__post_visibility,ForumPosts__status,Users__id,Users__first_name,Users__last_name;
    String ProfileImg__name,Posts,likescount,dislikescount,frequency;
    int islike,isdislike,is_moderator;
    String Users__role,Users__last_login;

    String Total_NoOfPages,page_no;
    ArrayList<GetPostListDataCommentsModel_Updated> comments;

    public GetPostListDataModel_Updated() {
    }


    public GetPostListDataModel_Updated(String forumPosts__id, String forumPosts__user_id, String forumPosts__subforum_id, String forumPosts__topic, String forumPosts__content, String forumPosts__created, String forumPosts__pending, String forumPosts__moderated, String forumPosts__pinned, String forumPosts__sod, String forumPosts__notify_users, String forumPosts__visibility, String forumPosts__post_visibility, String forumPosts__status, String users__id, String users__first_name, String users__last_name, String profileImg__name, String posts, String likescount, String dislikescount, String frequency, int islike, int isdislike, int is_moderator, String users__role, String users__last_login, String total_NoOfPages, String page_no, ArrayList<GetPostListDataCommentsModel_Updated> comments) {
        ForumPosts__id = forumPosts__id;
        ForumPosts__user_id = forumPosts__user_id;
        ForumPosts__subforum_id = forumPosts__subforum_id;
        ForumPosts__topic = forumPosts__topic;
        ForumPosts__content = forumPosts__content;
        ForumPosts__created = forumPosts__created;
        ForumPosts__pending = forumPosts__pending;
        ForumPosts__moderated = forumPosts__moderated;
        ForumPosts__pinned = forumPosts__pinned;
        ForumPosts__sod = forumPosts__sod;
        ForumPosts__notify_users = forumPosts__notify_users;
        ForumPosts__visibility = forumPosts__visibility;
        ForumPosts__post_visibility = forumPosts__post_visibility;
        ForumPosts__status = forumPosts__status;
        Users__id = users__id;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        ProfileImg__name = profileImg__name;
        Posts = posts;
        this.likescount = likescount;
        this.dislikescount = dislikescount;
        this.frequency = frequency;
        this.islike = islike;
        this.isdislike = isdislike;
        this.is_moderator = is_moderator;
        Users__role = users__role;
        Users__last_login = users__last_login;
        Total_NoOfPages = total_NoOfPages;
        this.page_no = page_no;
        this.comments = comments;
    }

    protected GetPostListDataModel_Updated(Parcel in) {
        ForumPosts__id = in.readString();
        ForumPosts__user_id = in.readString();
        ForumPosts__subforum_id = in.readString();
        ForumPosts__topic = in.readString();
        ForumPosts__content = in.readString();
        ForumPosts__created = in.readString();
        ForumPosts__pending = in.readString();
        ForumPosts__moderated = in.readString();
        ForumPosts__pinned = in.readString();
        ForumPosts__sod = in.readString();
        ForumPosts__notify_users = in.readString();
        ForumPosts__visibility = in.readString();
        ForumPosts__post_visibility = in.readString();
        ForumPosts__status = in.readString();
        Users__id = in.readString();
        Users__first_name = in.readString();
        Users__last_name = in.readString();
        ProfileImg__name = in.readString();
        Posts = in.readString();
        likescount = in.readString();
        dislikescount = in.readString();
        frequency = in.readString();
        islike = in.readInt();
        isdislike = in.readInt();
        is_moderator = in.readInt();
        Users__role = in.readString();
        Users__last_login = in.readString();
        Total_NoOfPages = in.readString();
        page_no = in.readString();
        comments = in.createTypedArrayList(GetPostListDataCommentsModel_Updated.CREATOR);
    }

    public static final Creator<GetPostListDataModel_Updated> CREATOR = new Creator<GetPostListDataModel_Updated>() {
        @Override
        public GetPostListDataModel_Updated createFromParcel(Parcel in) {
            return new GetPostListDataModel_Updated(in);
        }

        @Override
        public GetPostListDataModel_Updated[] newArray(int size) {
            return new GetPostListDataModel_Updated[size];
        }
    };


    public String getUsers__last_login() {
        return Users__last_login;
    }

    public void setUsers__last_login(String users__last_login) {
        Users__last_login = users__last_login;
    }

    public String getTotal_NoOfPages() {
        return Total_NoOfPages;
    }

    public void setTotal_NoOfPages(String total_NoOfPages) {
        Total_NoOfPages = total_NoOfPages;
    }

    public String getPage_no() {
        return page_no;
    }

    public void setPage_no(String page_no) {
        this.page_no = page_no;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getForumPosts__created() {
        return ForumPosts__created;
    }

    public void setForumPosts__created(String forumPosts__created) {
        ForumPosts__created = forumPosts__created;
    }

    public String getForumPosts__id() {
        return ForumPosts__id;
    }

    public void setForumPosts__id(String forumPosts__id) {
        ForumPosts__id = forumPosts__id;
    }

    public String getForumPosts__user_id() {
        return ForumPosts__user_id;
    }

    public void setForumPosts__user_id(String forumPosts__user_id) {
        ForumPosts__user_id = forumPosts__user_id;
    }

    public String getForumPosts__subforum_id() {
        return ForumPosts__subforum_id;
    }

    public void setForumPosts__subforum_id(String forumPosts__subforum_id) {
        ForumPosts__subforum_id = forumPosts__subforum_id;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }

    public String getForumPosts__content() {
        return ForumPosts__content;
    }

    public void setForumPosts__content(String forumPosts__content) {
        ForumPosts__content = forumPosts__content;
    }

    public String getForumPosts__pending() {
        return ForumPosts__pending;
    }

    public void setForumPosts__pending(String forumPosts__pending) {
        ForumPosts__pending = forumPosts__pending;
    }

    public String getForumPosts__moderated() {
        return ForumPosts__moderated;
    }

    public void setForumPosts__moderated(String forumPosts__moderated) {
        ForumPosts__moderated = forumPosts__moderated;
    }

    public String getForumPosts__pinned() {
        return ForumPosts__pinned;
    }

    public void setForumPosts__pinned(String forumPosts__pinned) {
        ForumPosts__pinned = forumPosts__pinned;
    }

    public String getForumPosts__sod() {
        return ForumPosts__sod;
    }

    public void setForumPosts__sod(String forumPosts__sod) {
        ForumPosts__sod = forumPosts__sod;
    }

    public String getForumPosts__notify_users() {
        return ForumPosts__notify_users;
    }

    public void setForumPosts__notify_users(String forumPosts__notify_users) {
        ForumPosts__notify_users = forumPosts__notify_users;
    }

    public String getForumPosts__visibility() {
        return ForumPosts__visibility;
    }

    public void setForumPosts__visibility(String forumPosts__visibility) {
        ForumPosts__visibility = forumPosts__visibility;
    }

    public String getForumPosts__post_visibility() {
        return ForumPosts__post_visibility;
    }

    public void setForumPosts__post_visibility(String forumPosts__post_visibility) {
        ForumPosts__post_visibility = forumPosts__post_visibility;
    }

    public String getForumPosts__status() {
        return ForumPosts__status;
    }

    public void setForumPosts__status(String forumPosts__status) {
        ForumPosts__status = forumPosts__status;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }

    public String getPosts() {
        return Posts;
    }

    public void setPosts(String posts) {
        Posts = posts;
    }

    public String getLikescount() {
        return likescount;
    }

    public void setLikescount(String likescount) {
        this.likescount = likescount;
    }

    public String getDislikescount() {
        return dislikescount;
    }

    public void setDislikescount(String dislikescount) {
        this.dislikescount = dislikescount;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public int getIslike() {
        return islike;
    }

    public void setIslike(int islike) {
        this.islike = islike;
    }

    public int getIsdislike() {
        return isdislike;
    }

    public void setIsdislike(int isdislike) {
        this.isdislike = isdislike;
    }

    public int getIs_moderator() {
        return is_moderator;
    }

    public void setIs_moderator(int is_moderator) {
        this.is_moderator = is_moderator;
    }

    public ArrayList<GetPostListDataCommentsModel_Updated> getComments() {
        return comments;
    }

    public void setComments(ArrayList<GetPostListDataCommentsModel_Updated> comments) {
        this.comments = comments;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ForumPosts__id);
        dest.writeString(ForumPosts__user_id);
        dest.writeString(ForumPosts__subforum_id);
        dest.writeString(ForumPosts__topic);
        dest.writeString(ForumPosts__content);
        dest.writeString(ForumPosts__created);
        dest.writeString(ForumPosts__pending);
        dest.writeString(ForumPosts__moderated);
        dest.writeString(ForumPosts__pinned);
        dest.writeString(ForumPosts__sod);
        dest.writeString(ForumPosts__notify_users);
        dest.writeString(ForumPosts__visibility);
        dest.writeString(ForumPosts__post_visibility);
        dest.writeString(ForumPosts__status);
        dest.writeString(Users__id);
        dest.writeString(Users__first_name);
        dest.writeString(Users__last_name);
        dest.writeString(ProfileImg__name);
        dest.writeString(Posts);
        dest.writeString(likescount);
        dest.writeString(dislikescount);
        dest.writeString(frequency);
        dest.writeInt(islike);
        dest.writeInt(isdislike);
        dest.writeInt(is_moderator);
        dest.writeString(Users__role);
        dest.writeString(Users__last_login);
        dest.writeString(Total_NoOfPages);
        dest.writeString(page_no);
        dest.writeTypedList(comments);
    }
}
