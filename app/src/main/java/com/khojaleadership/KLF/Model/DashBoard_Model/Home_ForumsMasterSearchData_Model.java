package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_ForumsMasterSearchData_Model {

    String post_approved_post,Forum__id,Forum__topic,Forum__content;

    public Home_ForumsMasterSearchData_Model() {
    }

    public Home_ForumsMasterSearchData_Model(String post_approved_post, String forum__id, String forum__topic, String forum__content) {
        this.post_approved_post = post_approved_post;
        Forum__id = forum__id;
        Forum__topic = forum__topic;
        Forum__content = forum__content;
    }

    public String getPost_approved_post() {
        return post_approved_post;
    }

    public void setPost_approved_post(String post_approved_post) {
        this.post_approved_post = post_approved_post;
    }

    public String getForum__id() {
        return Forum__id;
    }

    public void setForum__id(String forum__id) {
        Forum__id = forum__id;
    }

    public String getForum__topic() {
        return Forum__topic;
    }

    public void setForum__topic(String forum__topic) {
        Forum__topic = forum__topic;
    }

    public String getForum__content() {
        return Forum__content;
    }

    public void setForum__content(String forum__content) {
        Forum__content = forum__content;
    }
}
