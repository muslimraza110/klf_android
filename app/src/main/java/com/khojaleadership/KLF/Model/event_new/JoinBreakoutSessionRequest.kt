package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class JoinBreakoutSessionRequest(
        @SerializedName("summit_events_faculty_id") val summitEventsFacultyId: String,
//        @SerializedName("summit_events_summit_events_program_details_id") val programDetailsId: String,
        @SerializedName("summit_events_program_details_sessions_id") val sessionId: String
)