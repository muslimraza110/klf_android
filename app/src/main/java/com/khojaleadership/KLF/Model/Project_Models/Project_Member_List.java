package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class Project_Member_List {
    String status,message;
    ArrayList<Project_Member_Data_List> data;

    public Project_Member_List() {
    }

    public Project_Member_List(String status, String message, ArrayList<Project_Member_Data_List> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Project_Member_Data_List> getData() {
        return data;
    }

    public void setData(ArrayList<Project_Member_Data_List> data) {
        this.data = data;
    }
}
