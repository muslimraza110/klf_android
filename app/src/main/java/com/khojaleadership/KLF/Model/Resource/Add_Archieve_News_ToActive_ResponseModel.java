package com.khojaleadership.KLF.Model.Resource;

public class Add_Archieve_News_ToActive_ResponseModel {
    String status,message;

    public Add_Archieve_News_ToActive_ResponseModel() {
    }

    public Add_Archieve_News_ToActive_ResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
