package com.khojaleadership.KLF.Model.Project_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class ProjectsDataModel implements Parcelable{

    String Projects__id, Projects__creator_id, Projects__khoja_care_category_id, Projects__name, Projects__website;
    String Projects__description, Projects__start_date, Projects__est_completion_date, Projects__city;
    String Projects__country, Projects__created, Projects__modified, Creators__id, Creators__role, Creators__email;
    String Creators__status;
    String Projects__approval_stamp, Projects__status;

    String Creators__first_name,Creators__last_name;

    ProjectCharityTypeModel charity_type;
    ProjectTargetAudienceModel target_audience;
    ProjectTargetAnnualSpendModel target_annual_spend;


    public ProjectsDataModel() {
    }


    public ProjectsDataModel(String projects__id, String projects__creator_id, String projects__khoja_care_category_id, String projects__name, String projects__website, String projects__description, String projects__start_date, String projects__est_completion_date, String projects__city, String projects__country, String projects__created, String projects__modified, String creators__id, String creators__role, String creators__email, String creators__status, String projects__approval_stamp, String projects__status, String creators__first_name, String creators__last_name, ProjectCharityTypeModel charity_type, ProjectTargetAudienceModel target_audience, ProjectTargetAnnualSpendModel target_annual_spend) {
        Projects__id = projects__id;
        Projects__creator_id = projects__creator_id;
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
        Projects__name = projects__name;
        Projects__website = projects__website;
        Projects__description = projects__description;
        Projects__start_date = projects__start_date;
        Projects__est_completion_date = projects__est_completion_date;
        Projects__city = projects__city;
        Projects__country = projects__country;
        Projects__created = projects__created;
        Projects__modified = projects__modified;
        Creators__id = creators__id;
        Creators__role = creators__role;
        Creators__email = creators__email;
        Creators__status = creators__status;
        Projects__approval_stamp = projects__approval_stamp;
        Projects__status = projects__status;
        Creators__first_name = creators__first_name;
        Creators__last_name = creators__last_name;
        this.charity_type = charity_type;
        this.target_audience = target_audience;
        this.target_annual_spend = target_annual_spend;
    }


    protected ProjectsDataModel(Parcel in) {
        Projects__id = in.readString();
        Projects__creator_id = in.readString();
        Projects__khoja_care_category_id = in.readString();
        Projects__name = in.readString();
        Projects__website = in.readString();
        Projects__description = in.readString();
        Projects__start_date = in.readString();
        Projects__est_completion_date = in.readString();
        Projects__city = in.readString();
        Projects__country = in.readString();
        Projects__created = in.readString();
        Projects__modified = in.readString();
        Creators__id = in.readString();
        Creators__role = in.readString();
        Creators__email = in.readString();
        Creators__status = in.readString();
        Projects__approval_stamp = in.readString();
        Projects__status = in.readString();
        Creators__first_name = in.readString();
        Creators__last_name = in.readString();
        charity_type = in.readParcelable(ProjectCharityTypeModel.class.getClassLoader());
        target_audience = in.readParcelable(ProjectTargetAudienceModel.class.getClassLoader());
        target_annual_spend = in.readParcelable(ProjectTargetAnnualSpendModel.class.getClassLoader());
    }

    public static final Creator<ProjectsDataModel> CREATOR = new Creator<ProjectsDataModel>() {
        @Override
        public ProjectsDataModel createFromParcel(Parcel in) {
            return new ProjectsDataModel(in);
        }

        @Override
        public ProjectsDataModel[] newArray(int size) {
            return new ProjectsDataModel[size];
        }
    };

    public String getCreators__first_name() {
        return Creators__first_name;
    }

    public void setCreators__first_name(String creators__first_name) {
        Creators__first_name = creators__first_name;
    }

    public String getCreators__last_name() {
        return Creators__last_name;
    }

    public void setCreators__last_name(String creators__last_name) {
        Creators__last_name = creators__last_name;
    }

    public String getProjects__approval_stamp() {
        return Projects__approval_stamp;
    }

    public void setProjects__approval_stamp(String projects__approval_stamp) {
        Projects__approval_stamp = projects__approval_stamp;
    }

    public String getProjects__status() {
        return Projects__status;
    }

    public void setProjects__status(String projects__status) {
        Projects__status = projects__status;
    }

    public ProjectCharityTypeModel getCharity_type() {
        return charity_type;
    }

    public void setCharity_type(ProjectCharityTypeModel charity_type) {
        this.charity_type = charity_type;
    }

    public ProjectTargetAudienceModel getTarget_audience() {
        return target_audience;
    }

    public void setTarget_audience(ProjectTargetAudienceModel target_audience) {
        this.target_audience = target_audience;
    }

    public ProjectTargetAnnualSpendModel getTarget_annual_spend() {
        return target_annual_spend;
    }

    public void setTarget_annual_spend(ProjectTargetAnnualSpendModel target_annual_spend) {
        this.target_annual_spend = target_annual_spend;
    }

    public String getProjects__id() {
        return Projects__id;
    }

    public void setProjects__id(String projects__id) {
        Projects__id = projects__id;
    }

    public String getProjects__creator_id() {
        return Projects__creator_id;
    }

    public void setProjects__creator_id(String projects__creator_id) {
        Projects__creator_id = projects__creator_id;
    }

    public String getProjects__khoja_care_category_id() {
        return Projects__khoja_care_category_id;
    }

    public void setProjects__khoja_care_category_id(String projects__khoja_care_category_id) {
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }

    public String getProjects__website() {
        return Projects__website;
    }

    public void setProjects__website(String projects__website) {
        Projects__website = projects__website;
    }

    public String getProjects__description() {
        return Projects__description;
    }

    public void setProjects__description(String projects__description) {
        Projects__description = projects__description;
    }

    public String getProjects__start_date() {
        return Projects__start_date;
    }

    public void setProjects__start_date(String projects__start_date) {
        Projects__start_date = projects__start_date;
    }

    public String getProjects__est_completion_date() {
        return Projects__est_completion_date;
    }

    public void setProjects__est_completion_date(String projects__est_completion_date) {
        Projects__est_completion_date = projects__est_completion_date;
    }

    public String getProjects__city() {
        return Projects__city;
    }

    public void setProjects__city(String projects__city) {
        Projects__city = projects__city;
    }

    public String getProjects__country() {
        return Projects__country;
    }

    public void setProjects__country(String projects__country) {
        Projects__country = projects__country;
    }

    public String getProjects__created() {
        return Projects__created;
    }

    public void setProjects__created(String projects__created) {
        Projects__created = projects__created;
    }

    public String getProjects__modified() {
        return Projects__modified;
    }

    public void setProjects__modified(String projects__modified) {
        Projects__modified = projects__modified;
    }

    public String getCreators__id() {
        return Creators__id;
    }

    public void setCreators__id(String creators__id) {
        Creators__id = creators__id;
    }

    public String getCreators__role() {
        return Creators__role;
    }

    public void setCreators__role(String creators__role) {
        Creators__role = creators__role;
    }

    public String getCreators__email() {
        return Creators__email;
    }

    public void setCreators__email(String creators__email) {
        Creators__email = creators__email;
    }

    public String getCreators__status() {
        return Creators__status;
    }

    public void setCreators__status(String creators__status) {
        Creators__status = creators__status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Projects__id);
        dest.writeString(Projects__creator_id);
        dest.writeString(Projects__khoja_care_category_id);
        dest.writeString(Projects__name);
        dest.writeString(Projects__website);
        dest.writeString(Projects__description);
        dest.writeString(Projects__start_date);
        dest.writeString(Projects__est_completion_date);
        dest.writeString(Projects__city);
        dest.writeString(Projects__country);
        dest.writeString(Projects__created);
        dest.writeString(Projects__modified);
        dest.writeString(Creators__id);
        dest.writeString(Creators__role);
        dest.writeString(Creators__email);
        dest.writeString(Creators__status);
        dest.writeString(Projects__approval_stamp);
        dest.writeString(Projects__status);
        dest.writeString(Creators__first_name);
        dest.writeString(Creators__last_name);
        dest.writeParcelable(charity_type, flags);
        dest.writeParcelable(target_audience, flags);
        dest.writeParcelable(target_annual_spend, flags);
    }
}

