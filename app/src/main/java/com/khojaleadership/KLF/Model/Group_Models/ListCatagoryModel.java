package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class ListCatagoryModel {
    String status,message;
    ArrayList<ListCatagoryDataModel> data;

    public ListCatagoryModel() {
    }

    public ListCatagoryModel(String status, String message, ArrayList<ListCatagoryDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ListCatagoryDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<ListCatagoryDataModel> data) {
        this.data = data;
    }
}
