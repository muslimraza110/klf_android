package com.khojaleadership.KLF.Model.Mission;

import java.util.ArrayList;

public class GetMissionModel {
    String status,message;
    ArrayList<GetMissionDataModel> data;

    public GetMissionModel() {
    }

    public GetMissionModel(String status, String message, ArrayList<GetMissionDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetMissionDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetMissionDataModel> data) {
        this.data = data;
    }
}
