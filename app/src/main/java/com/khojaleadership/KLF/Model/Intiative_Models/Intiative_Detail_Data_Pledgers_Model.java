package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Data_Pledgers_Model {

    String FundraisingPledges__id,FundraisingPledges__amount,FundraisingPledges__from_amount,FundraisingPledges__amount_pending;
    String FundraisingPledges__anonymous,FundraisingPledges__hide_amount,FundraisingPledges__created;
    String fundraising,charity,Users__first_name,Users__last_name;
    String FundraisingPledges__user_id;
    String Users__title;

    public Intiative_Detail_Data_Pledgers_Model() {
    }

    public Intiative_Detail_Data_Pledgers_Model(String fundraisingPledges__id, String fundraisingPledges__amount, String fundraisingPledges__from_amount, String fundraisingPledges__amount_pending, String fundraisingPledges__anonymous, String fundraisingPledges__hide_amount, String fundraisingPledges__created, String fundraising, String charity, String users__first_name, String users__last_name, String fundraisingPledges__user_id, String users__title) {
        FundraisingPledges__id = fundraisingPledges__id;
        FundraisingPledges__amount = fundraisingPledges__amount;
        FundraisingPledges__from_amount = fundraisingPledges__from_amount;
        FundraisingPledges__amount_pending = fundraisingPledges__amount_pending;
        FundraisingPledges__anonymous = fundraisingPledges__anonymous;
        FundraisingPledges__hide_amount = fundraisingPledges__hide_amount;
        FundraisingPledges__created = fundraisingPledges__created;
        this.fundraising = fundraising;
        this.charity = charity;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        FundraisingPledges__user_id = fundraisingPledges__user_id;
        Users__title = users__title;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getFundraisingPledges__user_id() {
        return FundraisingPledges__user_id;
    }

    public void setFundraisingPledges__user_id(String fundraisingPledges__user_id) {
        FundraisingPledges__user_id = fundraisingPledges__user_id;
    }

    public String getFundraising() {
        return fundraising;
    }

    public void setFundraising(String fundraising) {
        this.fundraising = fundraising;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getFundraisingPledges__id() {
        return FundraisingPledges__id;
    }

    public void setFundraisingPledges__id(String fundraisingPledges__id) {
        FundraisingPledges__id = fundraisingPledges__id;
    }

    public String getFundraisingPledges__amount() {
        return FundraisingPledges__amount;
    }

    public void setFundraisingPledges__amount(String fundraisingPledges__amount) {
        FundraisingPledges__amount = fundraisingPledges__amount;
    }

    public String getFundraisingPledges__from_amount() {
        return FundraisingPledges__from_amount;
    }

    public void setFundraisingPledges__from_amount(String fundraisingPledges__from_amount) {
        FundraisingPledges__from_amount = fundraisingPledges__from_amount;
    }

    public String getFundraisingPledges__amount_pending() {
        return FundraisingPledges__amount_pending;
    }

    public void setFundraisingPledges__amount_pending(String fundraisingPledges__amount_pending) {
        FundraisingPledges__amount_pending = fundraisingPledges__amount_pending;
    }

    public String getFundraisingPledges__anonymous() {
        return FundraisingPledges__anonymous;
    }

    public void setFundraisingPledges__anonymous(String fundraisingPledges__anonymous) {
        FundraisingPledges__anonymous = fundraisingPledges__anonymous;
    }

    public String getFundraisingPledges__hide_amount() {
        return FundraisingPledges__hide_amount;
    }

    public void setFundraisingPledges__hide_amount(String fundraisingPledges__hide_amount) {
        FundraisingPledges__hide_amount = fundraisingPledges__hide_amount;
    }

    public String getFundraisingPledges__created() {
        return FundraisingPledges__created;
    }

    public void setFundraisingPledges__created(String fundraisingPledges__created) {
        FundraisingPledges__created = fundraisingPledges__created;
    }
}
