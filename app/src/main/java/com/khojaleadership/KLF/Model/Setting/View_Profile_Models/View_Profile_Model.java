package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class View_Profile_Model implements Parcelable {
    String status,message;
    ArrayList<View_Profile_Pleger_Model> pledges;
    ArrayList<View_profile_User_Info_Model> userinfo1;
    ArrayList<View_Profile_Bussiness_Project_Model> businessandprojects;
    ArrayList<View_Profile_Charity_Project_Model> charitiesandprojects;
    ArrayList<View_Profile_Group_Model> groups;
    ArrayList<View_Profile_Project_Model> groupsproject;
    ArrayList<View_Profile_Family_Model> families;
    ArrayList<View_Profile_Comments_Model> usercomments;

    public View_Profile_Model() {
    }


    public View_Profile_Model(String status, String message, ArrayList<View_Profile_Pleger_Model> pledges, ArrayList<View_profile_User_Info_Model> userinfo1, ArrayList<View_Profile_Bussiness_Project_Model> businessandprojects, ArrayList<View_Profile_Charity_Project_Model> charitiesandprojects, ArrayList<View_Profile_Group_Model> groups, ArrayList<View_Profile_Project_Model> groupsproject, ArrayList<View_Profile_Family_Model> families, ArrayList<View_Profile_Comments_Model> usercomments) {
        this.status = status;
        this.message = message;
        this.pledges = pledges;
        this.userinfo1 = userinfo1;
        this.businessandprojects = businessandprojects;
        this.charitiesandprojects = charitiesandprojects;
        this.groups = groups;
        this.groupsproject = groupsproject;
        this.families = families;
        this.usercomments = usercomments;
    }

    protected View_Profile_Model(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<View_Profile_Model> CREATOR = new Creator<View_Profile_Model>() {
        @Override
        public View_Profile_Model createFromParcel(Parcel in) {
            return new View_Profile_Model(in);
        }

        @Override
        public View_Profile_Model[] newArray(int size) {
            return new View_Profile_Model[size];
        }
    };

    public ArrayList<View_Profile_Comments_Model> getUsercomments() {
        return usercomments;
    }

    public void setUsercomments(ArrayList<View_Profile_Comments_Model> usercomments) {
        this.usercomments = usercomments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<View_Profile_Pleger_Model> getPledges() {
        return pledges;
    }

    public void setPledges(ArrayList<View_Profile_Pleger_Model> pledges) {
        this.pledges = pledges;
    }

    public ArrayList<View_profile_User_Info_Model> getUserinfo1() {
        return userinfo1;
    }

    public void setUserinfo1(ArrayList<View_profile_User_Info_Model> userinfo1) {
        this.userinfo1 = userinfo1;
    }

    public ArrayList<View_Profile_Bussiness_Project_Model> getBusinessandprojects() {
        return businessandprojects;
    }

    public void setBusinessandprojects(ArrayList<View_Profile_Bussiness_Project_Model> businessandprojects) {
        this.businessandprojects = businessandprojects;
    }

    public ArrayList<View_Profile_Charity_Project_Model> getCharitiesandprojects() {
        return charitiesandprojects;
    }

    public void setCharitiesandprojects(ArrayList<View_Profile_Charity_Project_Model> charitiesandprojects) {
        this.charitiesandprojects = charitiesandprojects;
    }

    public ArrayList<View_Profile_Group_Model> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<View_Profile_Group_Model> groups) {
        this.groups = groups;
    }

    public ArrayList<View_Profile_Project_Model> getGroupsproject() {
        return groupsproject;
    }

    public void setGroupsproject(ArrayList<View_Profile_Project_Model> groupsproject) {
        this.groupsproject = groupsproject;
    }

    public ArrayList<View_Profile_Family_Model> getFamilies() {
        return families;
    }

    public void setFamilies(ArrayList<View_Profile_Family_Model> families) {
        this.families = families;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
