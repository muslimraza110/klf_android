package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetGroupListModel implements Parcelable {
    String status,message;
    ArrayList<GetGroupListDataModel> data;

    public GetGroupListModel() {
    }

    public GetGroupListModel(String status, String message, ArrayList<GetGroupListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected GetGroupListModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetGroupListModel> CREATOR = new Creator<GetGroupListModel>() {
        @Override
        public GetGroupListModel createFromParcel(Parcel in) {
            return new GetGroupListModel(in);
        }

        @Override
        public GetGroupListModel[] newArray(int size) {
            return new GetGroupListModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetGroupListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetGroupListDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
