package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

public class View_Profile_Bussiness_Child_Model {
    String id,name,project_id;

    public View_Profile_Bussiness_Child_Model() {
    }

    public View_Profile_Bussiness_Child_Model(String id, String name, String project_id) {
        this.id = id;
        this.name = name;
        this.project_id = project_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
