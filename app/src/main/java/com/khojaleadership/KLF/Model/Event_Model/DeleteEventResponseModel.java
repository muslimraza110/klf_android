package com.khojaleadership.KLF.Model.Event_Model;

public class DeleteEventResponseModel {

    String status,message;

    public DeleteEventResponseModel() {
    }

    public DeleteEventResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
