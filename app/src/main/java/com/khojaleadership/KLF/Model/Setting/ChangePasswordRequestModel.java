package com.khojaleadership.KLF.Model.Setting;

public class ChangePasswordRequestModel {

    String user_id,newpassword,confirmpassword;

    public ChangePasswordRequestModel() {
    }

    public ChangePasswordRequestModel(String user_id, String newpassword, String confirmpassword) {
        this.user_id = user_id;
        this.newpassword = newpassword;
        this.confirmpassword = confirmpassword;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }
}
