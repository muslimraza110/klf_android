package com.khojaleadership.KLF.Model.DashBoard_Model;

import java.util.ArrayList;

public class Home_ForumsMasterSearch_Model {
    String status,message;
    ArrayList<Home_ForumsMasterSearchData_Model> data;

    public Home_ForumsMasterSearch_Model() {
    }

    public Home_ForumsMasterSearch_Model(String status, String message, ArrayList<Home_ForumsMasterSearchData_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Home_ForumsMasterSearchData_Model> getData() {
        return data;
    }

    public void setData(ArrayList<Home_ForumsMasterSearchData_Model> data) {
        this.data = data;
    }
}
