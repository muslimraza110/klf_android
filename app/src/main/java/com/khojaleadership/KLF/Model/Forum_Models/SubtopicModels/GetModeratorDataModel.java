package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class GetModeratorDataModel {
    private String letters;//Display the initials of the pinyin
    String email,id;
    int is_moderator;




    public GetModeratorDataModel(String letters, String email, String id, int is_moderator) {
        this.letters = letters;
        this.email = email;
        this.id = id;
        this.is_moderator = is_moderator;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public GetModeratorDataModel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIs_moderator() {
        return is_moderator;
    }

    public void setIs_moderator(int is_moderator) {
        this.is_moderator = is_moderator;
    }
}
