package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class SubcribeRequestModel {
    String frequency,user_id,forum_post_id;

    public SubcribeRequestModel() {
    }

    public SubcribeRequestModel(String frequency, String user_id, String forum_post_id) {
        this.frequency = frequency;
        this.user_id = user_id;
        this.forum_post_id = forum_post_id;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getForum_post_id() {
        return forum_post_id;
    }

    public void setForum_post_id(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }
}
