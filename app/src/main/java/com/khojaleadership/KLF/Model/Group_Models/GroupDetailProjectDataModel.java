package com.khojaleadership.KLF.Model.Group_Models;

import java.util.ArrayList;

public class GroupDetailProjectDataModel {
    GroupDetailProjectDetailModel project;
    ArrayList<GroupDetailProjectMemberModel> members;

    public GroupDetailProjectDataModel() {
    }

    public GroupDetailProjectDataModel(GroupDetailProjectDetailModel project, ArrayList<GroupDetailProjectMemberModel> members) {
        this.project = project;
        this.members = members;
    }

    public GroupDetailProjectDetailModel getProject() {
        return project;
    }

    public void setProject(GroupDetailProjectDetailModel project) {
        this.project = project;
    }

    public ArrayList<GroupDetailProjectMemberModel> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<GroupDetailProjectMemberModel> members) {
        this.members = members;
    }
}
