package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_profile_User_Info_Model implements Parcelable {


    String Users__title,Users__id,Users__role,Users__email,UserDetails__email2,Users__first_name,Users__middle_name,Users__last_name,UserDetails__phone,UserDetails__phone2;
    String UserDetails__title,UserDetails__description,UserDetails__bio,UserDetails__private_bio,UserDetails__address,UserDetails__address2;
    String UserDetails__city,UserDetails__region,UserDetails__country,UserDetails__postal_code,UserDetails__website,UserDetails__facebook;
    String UserDetails__linkedin,ProfileImg__name;
    String Users__gender;


    public View_profile_User_Info_Model() {
    }

    public View_profile_User_Info_Model(String users__title, String users__id, String users__role, String users__email, String userDetails__email2, String users__first_name, String users__middle_name, String users__last_name, String userDetails__phone, String userDetails__phone2, String userDetails__title, String userDetails__description, String userDetails__bio, String userDetails__private_bio, String userDetails__address, String userDetails__address2, String userDetails__city, String userDetails__region, String userDetails__country, String userDetails__postal_code, String userDetails__website, String userDetails__facebook, String userDetails__linkedin, String profileImg__name, String users__gender) {
        Users__title = users__title;
        Users__id = users__id;
        Users__role = users__role;
        Users__email = users__email;
        UserDetails__email2 = userDetails__email2;
        Users__first_name = users__first_name;
        Users__middle_name = users__middle_name;
        Users__last_name = users__last_name;
        UserDetails__phone = userDetails__phone;
        UserDetails__phone2 = userDetails__phone2;
        UserDetails__title = userDetails__title;
        UserDetails__description = userDetails__description;
        UserDetails__bio = userDetails__bio;
        UserDetails__private_bio = userDetails__private_bio;
        UserDetails__address = userDetails__address;
        UserDetails__address2 = userDetails__address2;
        UserDetails__city = userDetails__city;
        UserDetails__region = userDetails__region;
        UserDetails__country = userDetails__country;
        UserDetails__postal_code = userDetails__postal_code;
        UserDetails__website = userDetails__website;
        UserDetails__facebook = userDetails__facebook;
        UserDetails__linkedin = userDetails__linkedin;
        ProfileImg__name = profileImg__name;
        Users__gender = users__gender;
    }

    protected View_profile_User_Info_Model(Parcel in) {
        Users__title = in.readString();
        Users__id = in.readString();
        Users__role = in.readString();
        Users__email = in.readString();
        UserDetails__email2 = in.readString();
        Users__first_name = in.readString();
        Users__middle_name = in.readString();
        Users__last_name = in.readString();
        UserDetails__phone = in.readString();
        UserDetails__phone2 = in.readString();
        UserDetails__title = in.readString();
        UserDetails__description = in.readString();
        UserDetails__bio = in.readString();
        UserDetails__private_bio = in.readString();
        UserDetails__address = in.readString();
        UserDetails__address2 = in.readString();
        UserDetails__city = in.readString();
        UserDetails__region = in.readString();
        UserDetails__country = in.readString();
        UserDetails__postal_code = in.readString();
        UserDetails__website = in.readString();
        UserDetails__facebook = in.readString();
        UserDetails__linkedin = in.readString();
        ProfileImg__name = in.readString();
        Users__gender = in.readString();
    }

    public static final Creator<View_profile_User_Info_Model> CREATOR = new Creator<View_profile_User_Info_Model>() {
        @Override
        public View_profile_User_Info_Model createFromParcel(Parcel in) {
            return new View_profile_User_Info_Model(in);
        }

        @Override
        public View_profile_User_Info_Model[] newArray(int size) {
            return new View_profile_User_Info_Model[size];
        }
    };

    public String getUsers__gender() {
        return Users__gender;
    }

    public void setUsers__gender(String users__gender) {
        Users__gender = users__gender;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getUserDetails__email2() {
        return UserDetails__email2;
    }

    public void setUserDetails__email2(String userDetails__email2) {
        UserDetails__email2 = userDetails__email2;
    }

    public String getUserDetails__title() {
        return UserDetails__title;
    }

    public void setUserDetails__title(String userDetails__title) {
        UserDetails__title = userDetails__title;
    }

    public String getUserDetails__description() {
        return UserDetails__description;
    }

    public void setUserDetails__description(String userDetails__description) {
        UserDetails__description = userDetails__description;
    }

    public String getUserDetails__bio() {
        return UserDetails__bio;
    }

    public void setUserDetails__bio(String userDetails__bio) {
        UserDetails__bio = userDetails__bio;
    }

    public String getUserDetails__private_bio() {
        return UserDetails__private_bio;
    }

    public void setUserDetails__private_bio(String userDetails__private_bio) {
        UserDetails__private_bio = userDetails__private_bio;
    }

    public String getUserDetails__address() {
        return UserDetails__address;
    }

    public void setUserDetails__address(String userDetails__address) {
        UserDetails__address = userDetails__address;
    }

    public String getUserDetails__address2() {
        return UserDetails__address2;
    }

    public void setUserDetails__address2(String userDetails__address2) {
        UserDetails__address2 = userDetails__address2;
    }

    public String getUserDetails__city() {
        return UserDetails__city;
    }

    public void setUserDetails__city(String userDetails__city) {
        UserDetails__city = userDetails__city;
    }

    public String getUserDetails__region() {
        return UserDetails__region;
    }

    public void setUserDetails__region(String userDetails__region) {
        UserDetails__region = userDetails__region;
    }

    public String getUserDetails__country() {
        return UserDetails__country;
    }

    public void setUserDetails__country(String userDetails__country) {
        UserDetails__country = userDetails__country;
    }

    public String getUserDetails__postal_code() {
        return UserDetails__postal_code;
    }

    public void setUserDetails__postal_code(String userDetails__postal_code) {
        UserDetails__postal_code = userDetails__postal_code;
    }

    public String getUserDetails__website() {
        return UserDetails__website;
    }

    public void setUserDetails__website(String userDetails__website) {
        UserDetails__website = userDetails__website;
    }

    public String getUserDetails__facebook() {
        return UserDetails__facebook;
    }

    public void setUserDetails__facebook(String userDetails__facebook) {
        UserDetails__facebook = userDetails__facebook;
    }

    public String getUserDetails__linkedin() {
        return UserDetails__linkedin;
    }

    public void setUserDetails__linkedin(String userDetails__linkedin) {
        UserDetails__linkedin = userDetails__linkedin;
    }

    public String getProfileImg__name() {
        return ProfileImg__name;
    }

    public void setProfileImg__name(String profileImg__name) {
        ProfileImg__name = profileImg__name;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__middle_name() {
        return Users__middle_name;
    }

    public void setUsers__middle_name(String users__middle_name) {
        Users__middle_name = users__middle_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getUserDetails__phone() {
        return UserDetails__phone;
    }

    public void setUserDetails__phone(String userDetails__phone) {
        UserDetails__phone = userDetails__phone;
    }

    public String getUserDetails__phone2() {
        return UserDetails__phone2;
    }

    public void setUserDetails__phone2(String userDetails__phone2) {
        UserDetails__phone2 = userDetails__phone2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Users__title);
        dest.writeString(Users__id);
        dest.writeString(Users__role);
        dest.writeString(Users__email);
        dest.writeString(UserDetails__email2);
        dest.writeString(Users__first_name);
        dest.writeString(Users__middle_name);
        dest.writeString(Users__last_name);
        dest.writeString(UserDetails__phone);
        dest.writeString(UserDetails__phone2);
        dest.writeString(UserDetails__title);
        dest.writeString(UserDetails__description);
        dest.writeString(UserDetails__bio);
        dest.writeString(UserDetails__private_bio);
        dest.writeString(UserDetails__address);
        dest.writeString(UserDetails__address2);
        dest.writeString(UserDetails__city);
        dest.writeString(UserDetails__region);
        dest.writeString(UserDetails__country);
        dest.writeString(UserDetails__postal_code);
        dest.writeString(UserDetails__website);
        dest.writeString(UserDetails__facebook);
        dest.writeString(UserDetails__linkedin);
        dest.writeString(ProfileImg__name);
        dest.writeString(Users__gender);
    }
}
