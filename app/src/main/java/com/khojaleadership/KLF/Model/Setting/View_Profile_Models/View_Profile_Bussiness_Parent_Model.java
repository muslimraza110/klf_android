package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import java.util.ArrayList;

public class View_Profile_Bussiness_Parent_Model {
    String id,bussiness_name,Groups__description,Groups__email;
    String Groups__phone,Groups__address,Groups__address2,Groups__bio,Groups__groupimage;
    ArrayList<View_Profile_Bussiness_Child_Model> child_list;

    public View_Profile_Bussiness_Parent_Model() {
    }

    public View_Profile_Bussiness_Parent_Model(String id, String bussiness_name, String groups__description, String groups__email, String groups__phone, String groups__address, String groups__address2, String groups__bio, String groups__groupimage, ArrayList<View_Profile_Bussiness_Child_Model> child_list) {
        this.id = id;
        this.bussiness_name = bussiness_name;
        Groups__description = groups__description;
        Groups__email = groups__email;
        Groups__phone = groups__phone;
        Groups__address = groups__address;
        Groups__address2 = groups__address2;
        Groups__bio = groups__bio;
        Groups__groupimage = groups__groupimage;
        this.child_list = child_list;
    }

    public String getGroups__email() {
        return Groups__email;
    }

    public void setGroups__email(String groups__email) {
        Groups__email = groups__email;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }

    public String getGroups__phone() {
        return Groups__phone;
    }

    public void setGroups__phone(String groups__phone) {
        Groups__phone = groups__phone;
    }

    public String getGroups__address() {
        return Groups__address;
    }

    public void setGroups__address(String groups__address) {
        Groups__address = groups__address;
    }

    public String getGroups__address2() {
        return Groups__address2;
    }

    public void setGroups__address2(String groups__address2) {
        Groups__address2 = groups__address2;
    }

    public String getGroups__bio() {
        return Groups__bio;
    }

    public void setGroups__bio(String groups__bio) {
        Groups__bio = groups__bio;
    }

    public String getGroups__groupimage() {
        return Groups__groupimage;
    }

    public void setGroups__groupimage(String groups__groupimage) {
        Groups__groupimage = groups__groupimage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBussiness_name() {
        return bussiness_name;
    }

    public void setBussiness_name(String bussiness_name) {
        this.bussiness_name = bussiness_name;
    }

    public ArrayList<View_Profile_Bussiness_Child_Model> getChild_list() {
        return child_list;
    }

    public void setChild_list(ArrayList<View_Profile_Bussiness_Child_Model> child_list) {
        this.child_list = child_list;
    }
}
