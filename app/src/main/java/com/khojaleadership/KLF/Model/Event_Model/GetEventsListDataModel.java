package com.khojaleadership.KLF.Model.Event_Model;

import android.os.Parcel;
import android.os.Parcelable;

public class GetEventsListDataModel implements Parcelable {

    String id, group_id, project_id,user_id, name, start_date, end_date, description, city, country, created, modified;
    String first_name,last_name;

    public GetEventsListDataModel() {
    }

    public GetEventsListDataModel(String id, String group_id, String project_id, String user_id, String name, String start_date, String end_date, String description, String city, String country, String created, String modified, String first_name, String last_name) {
        this.id = id;
        this.group_id = group_id;
        this.project_id = project_id;
        this.user_id = user_id;
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.description = description;
        this.city = city;
        this.country = country;
        this.created = created;
        this.modified = modified;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    protected GetEventsListDataModel(Parcel in) {
        id = in.readString();
        group_id = in.readString();
        project_id = in.readString();
        user_id = in.readString();
        name = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        description = in.readString();
        city = in.readString();
        country = in.readString();
        created = in.readString();
        modified = in.readString();
        first_name = in.readString();
        last_name = in.readString();
    }

    public static final Creator<GetEventsListDataModel> CREATOR = new Creator<GetEventsListDataModel>() {
        @Override
        public GetEventsListDataModel createFromParcel(Parcel in) {
            return new GetEventsListDataModel(in);
        }

        @Override
        public GetEventsListDataModel[] newArray(int size) {
            return new GetEventsListDataModel[size];
        }
    };

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(group_id);
        dest.writeString(project_id);
        dest.writeString(user_id);
        dest.writeString(name);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(description);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(first_name);
        dest.writeString(last_name);
    }
}
