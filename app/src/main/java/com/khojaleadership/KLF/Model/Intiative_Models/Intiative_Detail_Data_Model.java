package com.khojaleadership.KLF.Model.Intiative_Models;

import java.util.ArrayList;

public class Intiative_Detail_Data_Model {

    ArrayList<Intiative_Detail_Data_Basic_Info_Model> basicinfo;
    ArrayList<Intiative_Detail_Data_Projects_Model> projects;
    ArrayList<Intiative_Detail_Data_Fundraising_Model> fundraising;
    ArrayList<Intiative_Detail_Data_Pledgers_Model> Pledgers;

    public Intiative_Detail_Data_Model() {
    }

    public Intiative_Detail_Data_Model(ArrayList<Intiative_Detail_Data_Basic_Info_Model> basicinfo, ArrayList<Intiative_Detail_Data_Projects_Model> projects, ArrayList<Intiative_Detail_Data_Fundraising_Model> fundraising, ArrayList<Intiative_Detail_Data_Pledgers_Model> pledgers) {
        this.basicinfo = basicinfo;
        this.projects = projects;
        this.fundraising = fundraising;
        Pledgers = pledgers;
    }

    public ArrayList<Intiative_Detail_Data_Basic_Info_Model> getBasicinfo() {
        return basicinfo;
    }

    public void setBasicinfo(ArrayList<Intiative_Detail_Data_Basic_Info_Model> basicinfo) {
        this.basicinfo = basicinfo;
    }

    public ArrayList<Intiative_Detail_Data_Projects_Model> getProjects() {
        return projects;
    }

    public void setProjects(ArrayList<Intiative_Detail_Data_Projects_Model> projects) {
        this.projects = projects;
    }

    public ArrayList<Intiative_Detail_Data_Fundraising_Model> getFundraising() {
        return fundraising;
    }

    public void setFundraising(ArrayList<Intiative_Detail_Data_Fundraising_Model> fundraising) {
        this.fundraising = fundraising;
    }

    public ArrayList<Intiative_Detail_Data_Pledgers_Model> getPledgers() {
        return Pledgers;
    }

    public void setPledgers(ArrayList<Intiative_Detail_Data_Pledgers_Model> pledgers) {
        Pledgers = pledgers;
    }
}
