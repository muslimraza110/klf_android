package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class AddModeratorResponseModel {
    String status,message;


    public AddModeratorResponseModel() {
    }

    public AddModeratorResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
