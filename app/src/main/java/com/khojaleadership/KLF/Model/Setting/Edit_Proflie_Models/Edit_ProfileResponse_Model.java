package com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models;

public class Edit_ProfileResponse_Model {
    String status,message;

    public Edit_ProfileResponse_Model() {
    }

    public Edit_ProfileResponse_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
