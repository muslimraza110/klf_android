package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Data_Projects_Model {

    String InitiativesProjects__percentage,Projects__id,Projects__creator_id,Projects__khoja_care_category_id,Projects__name;
    String Projects__website,Projects__description,Projects__city,Projects__country,Projects__approval_stamp,Projects__status,Projects__created;
    String Groups_details, ProjectFundraisings__amount;

    public String getGroups_details() {
        return Groups_details;
    }

    public void setGroups_details(String groups_details) {
        Groups_details = groups_details;
    }

    public String getProjectFundraisings__amount() {
        return ProjectFundraisings__amount;
    }

    public void setProjectFundraisings__amount(String projectFundraisings__amount) {
        ProjectFundraisings__amount = projectFundraisings__amount;
    }

    public Intiative_Detail_Data_Projects_Model() {
    }
    public Intiative_Detail_Data_Projects_Model(String initiativesProjects__percentage, String projects__id, String projects__creator_id, String projects__khoja_care_category_id, String projects__name, String projects__website, String projects__description, String projects__city, String projects__country, String projects__approval_stamp, String projects__status, String projects__created, String groups_details, String projectFundraisings__amount) {
        InitiativesProjects__percentage = initiativesProjects__percentage;
        Projects__id = projects__id;
        Projects__creator_id = projects__creator_id;
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
        Projects__name = projects__name;
        Projects__website = projects__website;
        Projects__description = projects__description;
        Projects__city = projects__city;
        Projects__country = projects__country;
        Projects__approval_stamp = projects__approval_stamp;
        Projects__status = projects__status;
        Projects__created = projects__created;
        Groups_details = groups_details;
        ProjectFundraisings__amount = projectFundraisings__amount;
    }


    public String getInitiativesProjects__percentage() {
        return InitiativesProjects__percentage;
    }

    public void setInitiativesProjects__percentage(String initiativesProjects__percentage) {
        InitiativesProjects__percentage = initiativesProjects__percentage;
    }

    public String getProjects__id() {
        return Projects__id;
    }

    public void setProjects__id(String projects__id) {
        Projects__id = projects__id;
    }

    public String getProjects__creator_id() {
        return Projects__creator_id;
    }

    public void setProjects__creator_id(String projects__creator_id) {
        Projects__creator_id = projects__creator_id;
    }

    public String getProjects__khoja_care_category_id() {
        return Projects__khoja_care_category_id;
    }

    public void setProjects__khoja_care_category_id(String projects__khoja_care_category_id) {
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }

    public String getProjects__website() {
        return Projects__website;
    }

    public void setProjects__website(String projects__website) {
        Projects__website = projects__website;
    }

    public String getProjects__description() {
        return Projects__description;
    }

    public void setProjects__description(String projects__description) {
        Projects__description = projects__description;
    }

    public String getProjects__city() {
        return Projects__city;
    }

    public void setProjects__city(String projects__city) {
        Projects__city = projects__city;
    }

    public String getProjects__country() {
        return Projects__country;
    }

    public void setProjects__country(String projects__country) {
        Projects__country = projects__country;
    }

    public String getProjects__approval_stamp() {
        return Projects__approval_stamp;
    }

    public void setProjects__approval_stamp(String projects__approval_stamp) {
        Projects__approval_stamp = projects__approval_stamp;
    }

    public String getProjects__status() {
        return Projects__status;
    }

    public void setProjects__status(String projects__status) {
        Projects__status = projects__status;
    }

    public String getProjects__created() {
        return Projects__created;
    }

    public void setProjects__created(String projects__created) {
        Projects__created = projects__created;
    }
}
