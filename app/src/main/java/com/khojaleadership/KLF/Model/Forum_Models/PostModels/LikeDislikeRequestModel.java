package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class LikeDislikeRequestModel {

    String  user_id,likes,dislikes,forum_post_id;

    public LikeDislikeRequestModel() {
    }

    public LikeDislikeRequestModel(String user_id, String likes, String dislikes, String forum_post_id) {
        this.user_id = user_id;
        this.likes = likes;
        this.dislikes = dislikes;
        this.forum_post_id = forum_post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDislikes() {
        return dislikes;
    }

    public void setDislikes(String dislikes) {
        this.dislikes = dislikes;
    }

    public String getForum_post_id() {
        return forum_post_id;
    }

    public void setForum_post_id(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }
}
