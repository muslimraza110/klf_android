package com.khojaleadership.KLF.Model.Khoja_Care_Models;

public class Khoja_care_child_model {

    String Projects__id,Projects__name,Projects__description,Creators__first_name,Creators__last_name,Projects__city,Projects__country,charity;

    public Khoja_care_child_model() {
    }

    public Khoja_care_child_model(String projects__id, String projects__name, String projects__description, String creators__first_name, String creators__last_name, String projects__city, String projects__country, String charity) {
        Projects__id = projects__id;
        Projects__name = projects__name;
        Projects__description = projects__description;
        Creators__first_name = creators__first_name;
        Creators__last_name = creators__last_name;
        Projects__city = projects__city;
        Projects__country = projects__country;
        this.charity = charity;
    }

    public String getCreators__first_name() {
        return Creators__first_name;
    }

    public void setCreators__first_name(String creators__first_name) {
        Creators__first_name = creators__first_name;
    }

    public String getCreators__last_name() {
        return Creators__last_name;
    }

    public void setCreators__last_name(String creators__last_name) {
        Creators__last_name = creators__last_name;
    }

    public String getProjects__id() {
        return Projects__id;
    }

    public void setProjects__id(String projects__id) {
        Projects__id = projects__id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }

    public String getProjects__description() {
        return Projects__description;
    }

    public void setProjects__description(String projects__description) {
        Projects__description = projects__description;
    }

    public String getProjects__city() {
        return Projects__city;
    }

    public void setProjects__city(String projects__city) {
        Projects__city = projects__city;
    }

    public String getProjects__country() {
        return Projects__country;
    }

    public void setProjects__country(String projects__country) {
        Projects__country = projects__country;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }
}
