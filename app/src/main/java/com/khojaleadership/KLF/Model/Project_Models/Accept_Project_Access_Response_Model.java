package com.khojaleadership.KLF.Model.Project_Models;

public class Accept_Project_Access_Response_Model {
    String status,message;

    public Accept_Project_Access_Response_Model() {
    }

    public Accept_Project_Access_Response_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
