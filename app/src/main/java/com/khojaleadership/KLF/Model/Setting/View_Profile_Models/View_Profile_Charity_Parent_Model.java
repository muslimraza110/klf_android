package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import java.util.ArrayList;

public class View_Profile_Charity_Parent_Model {

    String id,charity_name;
    ArrayList<View_Profile_Charity_Child_Model> child_list;

    public View_Profile_Charity_Parent_Model() {
    }

    public View_Profile_Charity_Parent_Model(String id, String charity_name, ArrayList<View_Profile_Charity_Child_Model> child_list) {
        this.id = id;
        this.charity_name = charity_name;
        this.child_list = child_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharity_name() {
        return charity_name;
    }

    public void setCharity_name(String charity_name) {
        this.charity_name = charity_name;
    }

    public ArrayList<View_Profile_Charity_Child_Model> getChild_list() {
        return child_list;
    }

    public void setChild_list(ArrayList<View_Profile_Charity_Child_Model> child_list) {
        this.child_list = child_list;
    }
}
