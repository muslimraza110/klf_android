package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class AddGroupToWhitelistRequestModel {
    String group_id,post_id;

    public AddGroupToWhitelistRequestModel() {
    }

    public AddGroupToWhitelistRequestModel(String group_id, String post_id) {
        this.group_id = group_id;
        this.post_id = post_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
