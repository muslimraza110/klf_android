package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Group_Data_Model {
    String Groups__id,Groups__category_id,Groups__name,Groups__description;

    public Intiative_Detail_Group_Data_Model() {
    }

    public Intiative_Detail_Group_Data_Model(String groups__id, String groups__category_id, String groups__name, String groups__description) {
        Groups__id = groups__id;
        Groups__category_id = groups__category_id;
        Groups__name = groups__name;
        Groups__description = groups__description;
    }

    public String getGroups__id() {
        return Groups__id;
    }

    public void setGroups__id(String groups__id) {
        Groups__id = groups__id;
    }

    public String getGroups__category_id() {
        return Groups__category_id;
    }

    public void setGroups__category_id(String groups__category_id) {
        Groups__category_id = groups__category_id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }
}
