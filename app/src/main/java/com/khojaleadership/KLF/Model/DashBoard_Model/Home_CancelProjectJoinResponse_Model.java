package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_CancelProjectJoinResponse_Model {
    String status,message;

    public Home_CancelProjectJoinResponse_Model() {
    }

    public Home_CancelProjectJoinResponse_Model(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
