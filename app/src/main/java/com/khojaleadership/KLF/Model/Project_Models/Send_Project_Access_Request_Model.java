package com.khojaleadership.KLF.Model.Project_Models;

public class Send_Project_Access_Request_Model {
    String user_id, project_id;

    public Send_Project_Access_Request_Model() {
    }

    public Send_Project_Access_Request_Model(String user_id, String project_id) {
        this.user_id = user_id;
        this.project_id = project_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }
}
