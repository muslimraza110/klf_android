package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_ForumsJointheDiscussion_Model {
    String ForumPosts__topic;
    String ForumPosts__id;
    String ForumPosts__category;
    String postapprovedpost;

    public Home_ForumsJointheDiscussion_Model() {
    }

    public Home_ForumsJointheDiscussion_Model(String forumPosts__topic, String forumPosts__id, String forumPosts__category, String postapprovedpost) {
        ForumPosts__topic = forumPosts__topic;
        ForumPosts__id = forumPosts__id;
        ForumPosts__category = forumPosts__category;
        this.postapprovedpost = postapprovedpost;
    }

    public String getForumPosts__id() {
        return ForumPosts__id;
    }

    public String getForumPosts__category() {
        return ForumPosts__category;
    }

    public void setForumPosts__category(String forumPosts__category) {
        ForumPosts__category = forumPosts__category;
    }

    public String getPostapprovedpost() {
        return postapprovedpost;
    }

    public void setPostapprovedpost(String postapprovedpost) {
        this.postapprovedpost = postapprovedpost;
    }

    public void setForumPosts__id(String forumPosts__id) {
        ForumPosts__id = forumPosts__id;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }
}
