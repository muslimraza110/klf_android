package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Encrypt_Request_Model {
    String fundraising_id,user_id,group_id,initative_id;

    public Intiative_Detail_Encrypt_Request_Model() {
    }

    public Intiative_Detail_Encrypt_Request_Model(String fundraising_id, String user_id, String group_id, String initative_id) {
        this.fundraising_id = fundraising_id;
        this.user_id = user_id;
        this.group_id = group_id;
        this.initative_id = initative_id;
    }

    public String getFundraising_id() {
        return fundraising_id;
    }

    public void setFundraising_id(String fundraising_id) {
        this.fundraising_id = fundraising_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getInitative_id() {
        return initative_id;
    }

    public void setInitative_id(String initative_id) {
        this.initative_id = initative_id;
    }
}
