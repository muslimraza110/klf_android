package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class MeetingStatusSetResponse(
        @SerializedName("is_error") val isError: Boolean?,
        @SerializedName("updated_status") val updatedStatus: String?
//        @SerializedName("data_message") val message: String?
)


enum class MeetingRequestStatusTypes(val value: String){
    NOTES_UPDATED("0"),
    MEETING_REQUESTED("1"),
    MEETING_ACCEPTED("2"),
    MEETING_REJECTED("3"),
    MEETING_CANCELED("4"),
    MEETING_DELETED("5"),
    MEETING_SELF_CANCELED("6"),
}