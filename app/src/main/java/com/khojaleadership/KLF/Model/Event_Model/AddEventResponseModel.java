package com.khojaleadership.KLF.Model.Event_Model;

public class AddEventResponseModel {
    String status,message;

    public AddEventResponseModel() {
    }

    public AddEventResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
