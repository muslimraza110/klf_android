package com.khojaleadership.KLF.Model.Event_Model;

public class DeleteEventRequestModel {
    String event_id;

    public DeleteEventRequestModel() {
    }

    public DeleteEventRequestModel(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
