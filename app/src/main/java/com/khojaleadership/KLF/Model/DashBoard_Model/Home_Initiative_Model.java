package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_Initiative_Model {
    String Initiatives__id,Initiatives__name,Amount;

    public Home_Initiative_Model() {
    }

    public Home_Initiative_Model(String initiatives__id, String initiatives__name, String amount) {
        Initiatives__id = initiatives__id;
        Initiatives__name = initiatives__name;
        Amount = amount;
    }

    public String getInitiatives__id() {
        return Initiatives__id;
    }

    public void setInitiatives__id(String initiatives__id) {
        Initiatives__id = initiatives__id;
    }

    public String getInitiatives__name() {
        return Initiatives__name;
    }

    public void setInitiatives__name(String initiatives__name) {
        Initiatives__name = initiatives__name;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
