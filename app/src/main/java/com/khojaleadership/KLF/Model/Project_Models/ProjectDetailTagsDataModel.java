package com.khojaleadership.KLF.Model.Project_Models;

public class ProjectDetailTagsDataModel {

    String ProjectsTags__tag_id,ProjectsTags__id,ProjectsTags__project_id,Tags__id,Tags__name;

    public ProjectDetailTagsDataModel() {
    }

    public ProjectDetailTagsDataModel(String projectsTags__tag_id, String projectsTags__id, String projectsTags__project_id, String tags__id, String tags__name) {
        ProjectsTags__tag_id = projectsTags__tag_id;
        ProjectsTags__id = projectsTags__id;
        ProjectsTags__project_id = projectsTags__project_id;
        Tags__id = tags__id;
        Tags__name = tags__name;
    }

    public String getProjectsTags__tag_id() {
        return ProjectsTags__tag_id;
    }

    public void setProjectsTags__tag_id(String projectsTags__tag_id) {
        ProjectsTags__tag_id = projectsTags__tag_id;
    }

    public String getProjectsTags__id() {
        return ProjectsTags__id;
    }

    public void setProjectsTags__id(String projectsTags__id) {
        ProjectsTags__id = projectsTags__id;
    }

    public String getProjectsTags__project_id() {
        return ProjectsTags__project_id;
    }

    public void setProjectsTags__project_id(String projectsTags__project_id) {
        ProjectsTags__project_id = projectsTags__project_id;
    }

    public String getTags__id() {
        return Tags__id;
    }

    public void setTags__id(String tags__id) {
        Tags__id = tags__id;
    }

    public String getTags__name() {
        return Tags__name;
    }

    public void setTags__name(String tags__name) {
        Tags__name = tags__name;
    }
}
