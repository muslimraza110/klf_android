package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailForumPostsDataModel {
    String ForumPostsGroups__forum_post_id,ForumPostsGroups__id,ForumPostsGroups__group_id,ForumPosts__id;
    String ForumPosts__user_id,ForumPosts__subforum_id,ForumPosts__topic,ForumPosts__content;

    public GroupDetailForumPostsDataModel() {
    }

    public GroupDetailForumPostsDataModel(String forumPostsGroups__forum_post_id, String forumPostsGroups__id, String forumPostsGroups__group_id, String forumPosts__id, String forumPosts__user_id, String forumPosts__subforum_id, String forumPosts__topic, String forumPosts__content) {
        ForumPostsGroups__forum_post_id = forumPostsGroups__forum_post_id;
        ForumPostsGroups__id = forumPostsGroups__id;
        ForumPostsGroups__group_id = forumPostsGroups__group_id;
        ForumPosts__id = forumPosts__id;
        ForumPosts__user_id = forumPosts__user_id;
        ForumPosts__subforum_id = forumPosts__subforum_id;
        ForumPosts__topic = forumPosts__topic;
        ForumPosts__content = forumPosts__content;
    }

    public String getForumPostsGroups__forum_post_id() {
        return ForumPostsGroups__forum_post_id;
    }

    public void setForumPostsGroups__forum_post_id(String forumPostsGroups__forum_post_id) {
        ForumPostsGroups__forum_post_id = forumPostsGroups__forum_post_id;
    }

    public String getForumPostsGroups__id() {
        return ForumPostsGroups__id;
    }

    public void setForumPostsGroups__id(String forumPostsGroups__id) {
        ForumPostsGroups__id = forumPostsGroups__id;
    }

    public String getForumPostsGroups__group_id() {
        return ForumPostsGroups__group_id;
    }

    public void setForumPostsGroups__group_id(String forumPostsGroups__group_id) {
        ForumPostsGroups__group_id = forumPostsGroups__group_id;
    }

    public String getForumPosts__id() {
        return ForumPosts__id;
    }

    public void setForumPosts__id(String forumPosts__id) {
        ForumPosts__id = forumPosts__id;
    }

    public String getForumPosts__user_id() {
        return ForumPosts__user_id;
    }

    public void setForumPosts__user_id(String forumPosts__user_id) {
        ForumPosts__user_id = forumPosts__user_id;
    }

    public String getForumPosts__subforum_id() {
        return ForumPosts__subforum_id;
    }

    public void setForumPosts__subforum_id(String forumPosts__subforum_id) {
        ForumPosts__subforum_id = forumPosts__subforum_id;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }

    public String getForumPosts__content() {
        return ForumPosts__content;
    }

    public void setForumPosts__content(String forumPosts__content) {
        ForumPosts__content = forumPosts__content;
    }
}
