package com.khojaleadership.KLF.Model.Forum_Models.TopicModels;

import android.os.Parcel;
import android.os.Parcelable;

public class GetTopicDataModel implements Parcelable {
    String Subforums__id,Subforums__user_id,Subforums__model,Subforums__title,Subforums__description,Subforums__status,Subforums__created,Subforums__modified;

    public GetTopicDataModel() {
    }

    public GetTopicDataModel(String subforums__id, String subforums__user_id, String subforums__model, String subforums__title, String subforums__description, String subforums__status, String subforums__created, String subforums__modified) {
        Subforums__id = subforums__id;
        Subforums__user_id = subforums__user_id;
        Subforums__model = subforums__model;
        Subforums__title = subforums__title;
        Subforums__description = subforums__description;
        Subforums__status = subforums__status;
        Subforums__created = subforums__created;
        Subforums__modified = subforums__modified;
    }

    protected GetTopicDataModel(Parcel in) {
        Subforums__id = in.readString();
        Subforums__user_id = in.readString();
        Subforums__model = in.readString();
        Subforums__title = in.readString();
        Subforums__description = in.readString();
        Subforums__status = in.readString();
        Subforums__created = in.readString();
        Subforums__modified = in.readString();
    }

    public static final Creator<GetTopicDataModel> CREATOR = new Creator<GetTopicDataModel>() {
        @Override
        public GetTopicDataModel createFromParcel(Parcel in) {
            return new GetTopicDataModel(in);
        }

        @Override
        public GetTopicDataModel[] newArray(int size) {
            return new GetTopicDataModel[size];
        }
    };

    public String getSubforums__id() {
        return Subforums__id;
    }

    public void setSubforums__id(String subforums__id) {
        Subforums__id = subforums__id;
    }

    public String getSubforums__user_id() {
        return Subforums__user_id;
    }

    public void setSubforums__user_id(String subforums__user_id) {
        Subforums__user_id = subforums__user_id;
    }

    public String getSubforums__model() {
        return Subforums__model;
    }

    public void setSubforums__model(String subforums__model) {
        Subforums__model = subforums__model;
    }

    public String getSubforums__title() {
        return Subforums__title;
    }

    public void setSubforums__title(String subforums__title) {
        Subforums__title = subforums__title;
    }

    public String getSubforums__description() {
        return Subforums__description;
    }

    public void setSubforums__description(String subforums__description) {
        Subforums__description = subforums__description;
    }

    public String getSubforums__status() {
        return Subforums__status;
    }

    public void setSubforums__status(String subforums__status) {
        Subforums__status = subforums__status;
    }

    public String getSubforums__created() {
        return Subforums__created;
    }

    public void setSubforums__created(String subforums__created) {
        Subforums__created = subforums__created;
    }

    public String getSubforums__modified() {
        return Subforums__modified;
    }

    public void setSubforums__modified(String subforums__modified) {
        Subforums__modified = subforums__modified;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Subforums__id);
        dest.writeString(Subforums__user_id);
        dest.writeString(Subforums__model);
        dest.writeString(Subforums__title);
        dest.writeString(Subforums__description);
        dest.writeString(Subforums__status);
        dest.writeString(Subforums__created);
        dest.writeString(Subforums__modified);
    }
}
