package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

import android.os.Parcel;
import android.os.Parcelable;

public class GetSubTopicDataModel implements Parcelable {
    //all parameters arangment is according to screen
    String id,ForumPosts__topic,Users__role,ForumPosts__created,likes,dislike,comments,pendingcomments,particptatinguser,ForumPosts__eblast;
    String ForumPosts__moderated,ForumPosts__pinned,ForumPosts__sod,content,ForumPosts__post_visibility;
    String post_approved_post,ForumPosts__user_id;
    String ForumPosts__visibility;
    public GetSubTopicDataModel() {
    }



    public GetSubTopicDataModel(String id, String forumPosts__topic, String users__role, String forumPosts__created, String likes, String dislike, String comments, String pendingcomments, String particptatinguser, String forumPosts__eblast, String forumPosts__moderated, String forumPosts__pinned, String forumPosts__sod, String content, String forumPosts__post_visibility, String post_approved_post, String forumPosts__user_id, String forumPosts__visibility) {
        this.id = id;
        ForumPosts__topic = forumPosts__topic;
        Users__role = users__role;
        ForumPosts__created = forumPosts__created;
        this.likes = likes;
        this.dislike = dislike;
        this.comments = comments;
        this.pendingcomments = pendingcomments;
        this.particptatinguser = particptatinguser;
        ForumPosts__eblast = forumPosts__eblast;
        ForumPosts__moderated = forumPosts__moderated;
        ForumPosts__pinned = forumPosts__pinned;
        ForumPosts__sod = forumPosts__sod;
        this.content = content;
        ForumPosts__post_visibility = forumPosts__post_visibility;
        this.post_approved_post = post_approved_post;
        ForumPosts__user_id = forumPosts__user_id;
        ForumPosts__visibility = forumPosts__visibility;
    }

    protected GetSubTopicDataModel(Parcel in) {
        id = in.readString();
        ForumPosts__topic = in.readString();
        Users__role = in.readString();
        ForumPosts__created = in.readString();
        likes = in.readString();
        dislike = in.readString();
        comments = in.readString();
        pendingcomments = in.readString();
        particptatinguser = in.readString();
        ForumPosts__eblast = in.readString();
        ForumPosts__moderated = in.readString();
        ForumPosts__pinned = in.readString();
        ForumPosts__sod = in.readString();
        content = in.readString();
        ForumPosts__post_visibility = in.readString();
        post_approved_post = in.readString();
        ForumPosts__user_id = in.readString();
        ForumPosts__visibility = in.readString();
    }

    public static final Creator<GetSubTopicDataModel> CREATOR = new Creator<GetSubTopicDataModel>() {
        @Override
        public GetSubTopicDataModel createFromParcel(Parcel in) {
            return new GetSubTopicDataModel(in);
        }

        @Override
        public GetSubTopicDataModel[] newArray(int size) {
            return new GetSubTopicDataModel[size];
        }
    };

    public String getForumPosts__visibility() {
        return ForumPosts__visibility;
    }

    public void setForumPosts__visibility(String forumPosts__visibility) {
        ForumPosts__visibility = forumPosts__visibility;
    }

    public String getForumPosts__user_id() {
        return ForumPosts__user_id;
    }

    public void setForumPosts__user_id(String forumPosts__user_id) {
        ForumPosts__user_id = forumPosts__user_id;
    }

    public String getPost_approved_post() {
        return post_approved_post;
    }

    public void setPost_approved_post(String post_approved_post) {
        this.post_approved_post = post_approved_post;
    }

    public String getForumPosts__post_visibility() {
        return ForumPosts__post_visibility;
    }

    public void setForumPosts__post_visibility(String forumPosts__post_visibility) {
        ForumPosts__post_visibility = forumPosts__post_visibility;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getForumPosts__moderated() {
        return ForumPosts__moderated;
    }

    public void setForumPosts__moderated(String forumPosts__moderated) {
        ForumPosts__moderated = forumPosts__moderated;
    }

    public String getForumPosts__pinned() {
        return ForumPosts__pinned;
    }

    public void setForumPosts__pinned(String forumPosts__pinned) {
        ForumPosts__pinned = forumPosts__pinned;
    }

    public String getForumPosts__sod() {
        return ForumPosts__sod;
    }

    public void setForumPosts__sod(String forumPosts__sod) {
        ForumPosts__sod = forumPosts__sod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForumPosts__topic() {
        return ForumPosts__topic;
    }

    public void setForumPosts__topic(String forumPosts__topic) {
        ForumPosts__topic = forumPosts__topic;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getForumPosts__created() {
        return ForumPosts__created;
    }

    public void setForumPosts__created(String forumPosts__created) {
        ForumPosts__created = forumPosts__created;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDislike() {
        return dislike;
    }

    public void setDislike(String dislike) {
        this.dislike = dislike;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPendingcomments() {
        return pendingcomments;
    }

    public void setPendingcomments(String pendingcomments) {
        this.pendingcomments = pendingcomments;
    }

    public String getParticptatinguser() {
        return particptatinguser;
    }

    public void setParticptatinguser(String particptatinguser) {
        this.particptatinguser = particptatinguser;
    }

    public String getForumPosts__eblast() {
        return ForumPosts__eblast;
    }

    public void setForumPosts__eblast(String forumPosts__eblast) {
        ForumPosts__eblast = forumPosts__eblast;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(ForumPosts__topic);
        dest.writeString(Users__role);
        dest.writeString(ForumPosts__created);
        dest.writeString(likes);
        dest.writeString(dislike);
        dest.writeString(comments);
        dest.writeString(pendingcomments);
        dest.writeString(particptatinguser);
        dest.writeString(ForumPosts__eblast);
        dest.writeString(ForumPosts__moderated);
        dest.writeString(ForumPosts__pinned);
        dest.writeString(ForumPosts__sod);
        dest.writeString(content);
        dest.writeString(ForumPosts__post_visibility);
        dest.writeString(post_approved_post);
        dest.writeString(ForumPosts__user_id);
        dest.writeString(ForumPosts__visibility);
    }
}
