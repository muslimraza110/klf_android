package com.khojaleadership.KLF.Model.DashBoard_Model;

public class Home_Child_Model {
    String child_name;

    public Home_Child_Model() {
    }

    public Home_Child_Model(String child_name) {
        this.child_name = child_name;
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }
}
