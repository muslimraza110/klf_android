package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class SummitEventsRegistrationRequest(
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_event_id") val summitEventId: String
)