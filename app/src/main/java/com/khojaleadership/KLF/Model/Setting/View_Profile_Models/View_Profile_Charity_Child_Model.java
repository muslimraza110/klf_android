package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

public class View_Profile_Charity_Child_Model {

    String id,name;

    public View_Profile_Charity_Child_Model() {
    }

    public View_Profile_Charity_Child_Model(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
