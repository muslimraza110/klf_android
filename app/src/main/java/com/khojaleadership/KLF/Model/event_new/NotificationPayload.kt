package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class NotificationPayload(
        @SerializedName("identifier") val identifier: String?,
        @SerializedName("details") val details: NotificationData?
)


data class NotificationData(
        @SerializedName("user_id") val userId: String?,
        @SerializedName("event_id") val eventId: String?,
        @SerializedName("summit_events_faculty_id") val summitEventsFacultyId: String?,
        @SerializedName("meeting_parent_id") val meetingParentId: String?
)

enum class SummitEventNotificationType(val value: String){
    MEETING_REQUEST_SENT("PushNotification_SummitEvents_MeetingRequest_Sent"),
    MEETING_REQUEST_UPDATED("PushNotification_SummitEvents_MeetingRequest_Updated"),
    MEETING_REQUEST_REJECTED("PushNotification_SummitEvents_MeetingRequest_Rejected"),
    MEETING_REQUEST_ACCEPTED("PushNotification_SummitEvents_MeetingRequest_Accepted"),
    MEETING_REQUEST_CANCELLED("PushNotification_SummitEvents_MeetingRequest_Cancelled"),
    MEETING_REQUEST_DELETED("PushNotification_SummitEvents_MeetingRequest_Deleted")
}

data class NotificationBody(
        @SerializedName("body") val body: String?,
        @SerializedName("title") val title: String?
)