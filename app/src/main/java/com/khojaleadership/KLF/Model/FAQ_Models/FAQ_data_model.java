package com.khojaleadership.KLF.Model.FAQ_Models;

public class FAQ_data_model {
    String id, user_id, parent_id, level, title, description, status;

    public FAQ_data_model() {
    }

    public FAQ_data_model(String id, String user_id, String parent_id, String level, String title, String description, String status) {
        this.id = id;
        this.user_id = user_id;
        this.parent_id = parent_id;
        this.level = level;
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
