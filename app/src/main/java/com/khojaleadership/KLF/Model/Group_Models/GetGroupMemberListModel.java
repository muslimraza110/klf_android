package com.khojaleadership.KLF.Model.Group_Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetGroupMemberListModel implements Parcelable {
    String status,message;
    ArrayList<GetGroupMemberListDataModel> data;

    public GetGroupMemberListModel(String status, String message, ArrayList<GetGroupMemberListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public GetGroupMemberListModel() {
    }

    protected GetGroupMemberListModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetGroupMemberListModel> CREATOR = new Creator<GetGroupMemberListModel>() {
        @Override
        public GetGroupMemberListModel createFromParcel(Parcel in) {
            return new GetGroupMemberListModel(in);
        }

        @Override
        public GetGroupMemberListModel[] newArray(int size) {
            return new GetGroupMemberListModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetGroupMemberListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetGroupMemberListDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
