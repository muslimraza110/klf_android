package com.khojaleadership.KLF.Model.Contact;

import android.os.Parcel;
import android.os.Parcelable;

public class GetContactDataListModel implements Parcelable {

    private String letters;//Display the initials of the pinyin
    String Users__title,Users__id,Users__role,Users__email,Users__first_name,Users__last_name,Users__gender,Users__status;
    String UserDetails__title,UserDetails__description,UserDetails__address,UserDetails__address2;
    String UserDetails__phone,UserDetails__phone2,UserDetails__email2,UserDetails__website,UserDetails__facebook;
    String profileimg;

    public GetContactDataListModel() {
    }

    public GetContactDataListModel(String letters, String users__title, String users__id, String users__role, String users__email, String users__first_name, String users__last_name, String users__gender, String users__status, String userDetails__title, String userDetails__description, String userDetails__address, String userDetails__address2, String userDetails__phone, String userDetails__phone2, String userDetails__email2, String userDetails__website, String userDetails__facebook, String profileimg) {
        this.letters = letters;
        Users__title = users__title;
        Users__id = users__id;
        Users__role = users__role;
        Users__email = users__email;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
        Users__gender = users__gender;
        Users__status = users__status;
        UserDetails__title = userDetails__title;
        UserDetails__description = userDetails__description;
        UserDetails__address = userDetails__address;
        UserDetails__address2 = userDetails__address2;
        UserDetails__phone = userDetails__phone;
        UserDetails__phone2 = userDetails__phone2;
        UserDetails__email2 = userDetails__email2;
        UserDetails__website = userDetails__website;
        UserDetails__facebook = userDetails__facebook;
        this.profileimg = profileimg;
    }


    protected GetContactDataListModel(Parcel in) {
        letters = in.readString();
        Users__title = in.readString();
        Users__id = in.readString();
        Users__role = in.readString();
        Users__email = in.readString();
        Users__first_name = in.readString();
        Users__last_name = in.readString();
        Users__gender = in.readString();
        Users__status = in.readString();
        UserDetails__title = in.readString();
        UserDetails__description = in.readString();
        UserDetails__address = in.readString();
        UserDetails__address2 = in.readString();
        UserDetails__phone = in.readString();
        UserDetails__phone2 = in.readString();
        UserDetails__email2 = in.readString();
        UserDetails__website = in.readString();
        UserDetails__facebook = in.readString();
        profileimg = in.readString();
    }

    public static final Creator<GetContactDataListModel> CREATOR = new Creator<GetContactDataListModel>() {
        @Override
        public GetContactDataListModel createFromParcel(Parcel in) {
            return new GetContactDataListModel(in);
        }

        @Override
        public GetContactDataListModel[] newArray(int size) {
            return new GetContactDataListModel[size];
        }
    };

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getProfileimg() {
        return profileimg;
    }

    public void setProfileimg(String profileimg) {
        this.profileimg = profileimg;
    }

    public String getUserDetails__phone() {
        return UserDetails__phone;
    }

    public void setUserDetails__phone(String userDetails__phone) {
        UserDetails__phone = userDetails__phone;
    }

    public String getUserDetails__phone2() {
        return UserDetails__phone2;
    }

    public void setUserDetails__phone2(String userDetails__phone2) {
        UserDetails__phone2 = userDetails__phone2;
    }

    public String getUserDetails__email2() {
        return UserDetails__email2;
    }

    public void setUserDetails__email2(String userDetails__email2) {
        UserDetails__email2 = userDetails__email2;
    }

    public String getUserDetails__website() {
        return UserDetails__website;
    }

    public void setUserDetails__website(String userDetails__website) {
        UserDetails__website = userDetails__website;
    }

    public String getUserDetails__facebook() {
        return UserDetails__facebook;
    }

    public void setUserDetails__facebook(String userDetails__facebook) {
        UserDetails__facebook = userDetails__facebook;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }

    public String getUsers__gender() {
        return Users__gender;
    }

    public void setUsers__gender(String users__gender) {
        Users__gender = users__gender;
    }

    public String getUsers__status() {
        return Users__status;
    }

    public void setUsers__status(String users__status) {
        Users__status = users__status;
    }

    public String getUserDetails__title() {
        return UserDetails__title;
    }

    public void setUserDetails__title(String userDetails__title) {
        UserDetails__title = userDetails__title;
    }

    public String getUserDetails__description() {
        return UserDetails__description;
    }

    public void setUserDetails__description(String userDetails__description) {
        UserDetails__description = userDetails__description;
    }

    public String getUserDetails__address() {
        return UserDetails__address;
    }

    public void setUserDetails__address(String userDetails__address) {
        UserDetails__address = userDetails__address;
    }

    public String getUserDetails__address2() {
        return UserDetails__address2;
    }

    public void setUserDetails__address2(String userDetails__address2) {
        UserDetails__address2 = userDetails__address2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(letters);
        dest.writeString(Users__title);
        dest.writeString(Users__id);
        dest.writeString(Users__role);
        dest.writeString(Users__email);
        dest.writeString(Users__first_name);
        dest.writeString(Users__last_name);
        dest.writeString(Users__gender);
        dest.writeString(Users__status);
        dest.writeString(UserDetails__title);
        dest.writeString(UserDetails__description);
        dest.writeString(UserDetails__address);
        dest.writeString(UserDetails__address2);
        dest.writeString(UserDetails__phone);
        dest.writeString(UserDetails__phone2);
        dest.writeString(UserDetails__email2);
        dest.writeString(UserDetails__website);
        dest.writeString(UserDetails__facebook);
        dest.writeString(profileimg);
    }
}
