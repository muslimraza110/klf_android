package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class RemoveModeratorResponseModel {
    String status,message;

    public RemoveModeratorResponseModel() {
    }

    public RemoveModeratorResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
