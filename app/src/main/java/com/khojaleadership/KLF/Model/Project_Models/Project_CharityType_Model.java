package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class Project_CharityType_Model {
    String status,message;
    ArrayList<Project_CharityType_Data_Model> data;

    public Project_CharityType_Model() {
    }

    public Project_CharityType_Model(String status, String message, ArrayList<Project_CharityType_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Project_CharityType_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<Project_CharityType_Data_Model> data) {
        this.data = data;
    }
}
