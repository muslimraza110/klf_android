package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class SetMeetingRequest(
        @SerializedName("requested_by") val requestedById: String,
        @SerializedName("requested_to") val requestedToIdList: String,
        @SerializedName("summit_events_id") val eventId: String,
        @SerializedName("meeting_date") val meetingDate: String,
        @SerializedName("meeting_start_time") val meetingStartTime: String,
        @SerializedName("meeting_end_time") val meetingEndTime: String,
        @SerializedName("meeting_venue") val meetingVenue: String,
        @SerializedName("notes") val meetingNotes: String,
        @SerializedName("user_id") val userId: String
)

data class UpdateMeetingRequest(
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_events_meeting_request_child_id") val summitEventsMeetingRequestChildId: String,
        @SerializedName("new_status") val newStatus: String
)

data class UpdateMeetingNotesRequest(
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_events_meeting_request_parent_id") val summitEventsMeetingRequestParentId: String,
        @SerializedName("notes") val notes: String
)

data class CancelMeetingRequest(
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_events_meeting_request_parent_id") val summitEventsMeetingRequestParentId: String
)
