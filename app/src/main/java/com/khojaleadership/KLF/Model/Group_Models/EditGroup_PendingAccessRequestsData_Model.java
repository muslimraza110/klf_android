package com.khojaleadership.KLF.Model.Group_Models;

public class EditGroup_PendingAccessRequestsData_Model {
    String GroupRequests__id,GroupRequests__group_id,GroupRequests__user_id,GroupRequests__status,Users__id;
    String Users__role,Users__email,Users__title,Users__first_name,Users__last_name;

    public EditGroup_PendingAccessRequestsData_Model() {
    }

    public EditGroup_PendingAccessRequestsData_Model(String groupRequests__id, String groupRequests__group_id, String groupRequests__user_id, String groupRequests__status, String users__id, String users__role, String users__email, String users__title, String users__first_name, String users__last_name) {
        GroupRequests__id = groupRequests__id;
        GroupRequests__group_id = groupRequests__group_id;
        GroupRequests__user_id = groupRequests__user_id;
        GroupRequests__status = groupRequests__status;
        Users__id = users__id;
        Users__role = users__role;
        Users__email = users__email;
        Users__title = users__title;
        Users__first_name = users__first_name;
        Users__last_name = users__last_name;
    }

    public String getGroupRequests__id() {
        return GroupRequests__id;
    }

    public void setGroupRequests__id(String groupRequests__id) {
        GroupRequests__id = groupRequests__id;
    }

    public String getGroupRequests__group_id() {
        return GroupRequests__group_id;
    }

    public void setGroupRequests__group_id(String groupRequests__group_id) {
        GroupRequests__group_id = groupRequests__group_id;
    }

    public String getGroupRequests__user_id() {
        return GroupRequests__user_id;
    }

    public void setGroupRequests__user_id(String groupRequests__user_id) {
        GroupRequests__user_id = groupRequests__user_id;
    }

    public String getGroupRequests__status() {
        return GroupRequests__status;
    }

    public void setGroupRequests__status(String groupRequests__status) {
        GroupRequests__status = groupRequests__status;
    }

    public String getUsers__id() {
        return Users__id;
    }

    public void setUsers__id(String users__id) {
        Users__id = users__id;
    }

    public String getUsers__role() {
        return Users__role;
    }

    public void setUsers__role(String users__role) {
        Users__role = users__role;
    }

    public String getUsers__email() {
        return Users__email;
    }

    public void setUsers__email(String users__email) {
        Users__email = users__email;
    }

    public String getUsers__title() {
        return Users__title;
    }

    public void setUsers__title(String users__title) {
        Users__title = users__title;
    }

    public String getUsers__first_name() {
        return Users__first_name;
    }

    public void setUsers__first_name(String users__first_name) {
        Users__first_name = users__first_name;
    }

    public String getUsers__last_name() {
        return Users__last_name;
    }

    public void setUsers__last_name(String users__last_name) {
        Users__last_name = users__last_name;
    }
}
