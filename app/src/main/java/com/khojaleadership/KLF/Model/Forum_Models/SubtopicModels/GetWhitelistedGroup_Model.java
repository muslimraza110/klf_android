package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

import java.util.ArrayList;

public class GetWhitelistedGroup_Model {
    String status,message;
    ArrayList<GetWhitelistedGroup_Data_Model> data;

    public GetWhitelistedGroup_Model() {
    }

    public GetWhitelistedGroup_Model(String status, String message, ArrayList<GetWhitelistedGroup_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetWhitelistedGroup_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<GetWhitelistedGroup_Data_Model> data) {
        this.data = data;
    }
}
