package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class MeetingsResponse(
        @SerializedName("host_details") val hostDetails: HostDetails?,
        @SerializedName("meeting_details") val meetingDetails: List<MeetingDetailsItem?>?
)

data class HostDetails(
        @SerializedName("id") val meetingId: String?,
        @SerializedName("summit_events_id") val summitEventsId: String?,
        @SerializedName("requested_by") val requestedById: String?,
        @SerializedName("meeting_date") val meetingDate: String?,
        @SerializedName("meeting_start_time") val meetingStartTime: String?,
        @SerializedName("meeting_end_time") val meetingEndTime: String?,
        @SerializedName("status") val meetingStatus: String?,
        @SerializedName("meeting_time") val meetingStartEndTime: String?,
        @SerializedName("notes") val notes: String?,
        @SerializedName("i_am_host") val iAmHost: Boolean?,
        @SerializedName("requested_by_details") val requestedByDetails: PersonDetailResponse?
)

data class MeetingDetailsItem(
        @SerializedName("id") val meetingIdForDelegate: String?,
        @SerializedName("parent_id") val parentId: String?,
        @SerializedName("requested_to") val requestedTo: String?,
        @SerializedName("status") val meetingStatus: String?,
        @SerializedName("requested_to_details") val requestedToDetails: PersonDetailResponse?
)