package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class ChangeAvailabilityRequest(
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_events_summit_events_program_details_id") val programDetailsId: String,
        @SerializedName("is_available") val isAvailable: String
)