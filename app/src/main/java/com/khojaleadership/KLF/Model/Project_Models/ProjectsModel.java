package com.khojaleadership.KLF.Model.Project_Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProjectsModel implements Parcelable {
    String status,message;
    ArrayList<ProjectsDataModel> data;

    public ProjectsModel() {
    }

    public ProjectsModel(String status, String message, ArrayList<ProjectsDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }


    protected ProjectsModel(Parcel in) {
        status = in.readString();
        message = in.readString();
        data = in.createTypedArrayList(ProjectsDataModel.CREATOR);
    }

    public static final Creator<ProjectsModel> CREATOR = new Creator<ProjectsModel>() {
        @Override
        public ProjectsModel createFromParcel(Parcel in) {
            return new ProjectsModel(in);
        }

        @Override
        public ProjectsModel[] newArray(int size) {
            return new ProjectsModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ProjectsDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<ProjectsDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
