package com.khojaleadership.KLF.Model.FAQ_Models;

import java.util.ArrayList;

public class FAQ_Model {
    String status,message;
    ArrayList<FAQ_data_model> data;

    public FAQ_Model() {
    }

    public FAQ_Model(String status, String message, ArrayList<FAQ_data_model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<FAQ_data_model> getData() {
        return data;
    }

    public void setData(ArrayList<FAQ_data_model> data) {
        this.data = data;
    }
}
