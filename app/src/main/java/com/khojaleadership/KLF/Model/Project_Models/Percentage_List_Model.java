package com.khojaleadership.KLF.Model.Project_Models;

import java.util.ArrayList;

public class Percentage_List_Model {
    String status,message;
    ArrayList<Percentage_List_Data_Model> data;

    public Percentage_List_Model() {
    }

    public Percentage_List_Model(String status, String message, ArrayList<Percentage_List_Data_Model> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Percentage_List_Data_Model> getData() {
        return data;
    }

    public void setData(ArrayList<Percentage_List_Data_Model> data) {
        this.data = data;
    }
}
