package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class UpdateWholeMeetingRequest(
        @SerializedName("requested_by") val requestedById: String,
        @SerializedName("requested_to") val requestedToIdList: String,
        @SerializedName("summit_events_id") val eventId: String,
        @SerializedName("meeting_date") val meetingDate: String,
        @SerializedName("meeting_start_time") val meetingStartTime: String,
        @SerializedName("meeting_end_time") val meetingEndTime: String,
        @SerializedName("meeting_venue") val meetingVenue: String,
        @SerializedName("notes") val meetingNotes: String,
        @SerializedName("user_id") val userId: String,
        @SerializedName("event_meeting_id") val eventMeetingId: String
)