package com.khojaleadership.KLF.Model.Khoja_Care_Models;

import java.util.List;

public class Khoja_care_parent_model {



    public String Title;
    public List<Khoja_care_child_model> model_child;


    public Khoja_care_parent_model( String title, List<Khoja_care_child_model> model_child) {

        Title = title;
        this.model_child = model_child;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public List<Khoja_care_child_model> getModel_child() {
        return model_child;
    }

    public void setModel_child(List<Khoja_care_child_model> model_child) {
        this.model_child = model_child;
    }
}
