package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

public class GetPostListModel_Updated {
    String status,message;
    GetPostListDataModel_Updated data;

    public GetPostListModel_Updated() {
    }

    public GetPostListModel_Updated(String status, String message, GetPostListDataModel_Updated data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetPostListDataModel_Updated getData() {
        return data;
    }

    public void setData(GetPostListDataModel_Updated data) {
        this.data = data;
    }
}
