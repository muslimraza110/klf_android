package com.khojaleadership.KLF.Model.Group_Models;

public class ListCatagoryDataModel {
    String id,model,name,description;

    public ListCatagoryDataModel() {
    }

    public ListCatagoryDataModel(String id, String model, String name, String description) {
        this.id = id;
        this.model = model;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
