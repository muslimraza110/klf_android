package com.khojaleadership.KLF.Model.Feedback_Models;

public class FeedbackResponseModel {
    String status,message;

    public FeedbackResponseModel() {
    }

    public FeedbackResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
