package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Encrypt_Response_Model {
    String status,message,data;

    public Intiative_Detail_Encrypt_Response_Model() {
    }

    public Intiative_Detail_Encrypt_Response_Model(String status, String message, String data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
