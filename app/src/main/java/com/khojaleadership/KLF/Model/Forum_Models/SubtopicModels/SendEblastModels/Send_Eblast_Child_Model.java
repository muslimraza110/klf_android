package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Send_Eblast_Child_Model implements Parcelable {
    String child_name;
    ArrayList<Send_Eblast_Child_Data_Model> child_data;

    public Send_Eblast_Child_Model() {
    }

    public Send_Eblast_Child_Model(String child_name, ArrayList<Send_Eblast_Child_Data_Model> child_data) {
        this.child_name = child_name;
        this.child_data = child_data;
    }


    protected Send_Eblast_Child_Model(Parcel in) {
        child_name = in.readString();
        child_data = in.createTypedArrayList(Send_Eblast_Child_Data_Model.CREATOR);
    }

    public static final Creator<Send_Eblast_Child_Model> CREATOR = new Creator<Send_Eblast_Child_Model>() {
        @Override
        public Send_Eblast_Child_Model createFromParcel(Parcel in) {
            return new Send_Eblast_Child_Model(in);
        }

        @Override
        public Send_Eblast_Child_Model[] newArray(int size) {
            return new Send_Eblast_Child_Model[size];
        }
    };

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public ArrayList<Send_Eblast_Child_Data_Model> getChild_data() {
        return child_data;
    }

    public void setChild_data(ArrayList<Send_Eblast_Child_Data_Model> child_data) {
        this.child_data = child_data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(child_name);
        dest.writeTypedList(child_data);
    }
}
