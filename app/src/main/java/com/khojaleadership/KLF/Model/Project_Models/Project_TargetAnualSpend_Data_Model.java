package com.khojaleadership.KLF.Model.Project_Models;

public class Project_TargetAnualSpend_Data_Model {

    String Tags__id,Tags__parent_id,Tags__name;

    public Project_TargetAnualSpend_Data_Model() {
    }

    public Project_TargetAnualSpend_Data_Model(String tags__id, String tags__parent_id, String tags__name) {
        Tags__id = tags__id;
        Tags__parent_id = tags__parent_id;
        Tags__name = tags__name;
    }

    public String getTags__id() {
        return Tags__id;
    }

    public void setTags__id(String tags__id) {
        Tags__id = tags__id;
    }

    public String getTags__parent_id() {
        return Tags__parent_id;
    }

    public void setTags__parent_id(String tags__parent_id) {
        Tags__parent_id = tags__parent_id;
    }

    public String getTags__name() {
        return Tags__name;
    }

    public void setTags__name(String tags__name) {
        Tags__name = tags__name;
    }
}
