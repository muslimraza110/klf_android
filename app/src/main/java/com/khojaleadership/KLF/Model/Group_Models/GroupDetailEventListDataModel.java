package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailEventListDataModel {
    String Events__id,Events__user_id,Events__name,Events__start_date,Events__end_date,Events__all_day;

    public GroupDetailEventListDataModel() {
    }

    public GroupDetailEventListDataModel(String events__id, String events__user_id, String events__name, String events__start_date, String events__end_date, String events__all_day) {
        Events__id = events__id;
        Events__user_id = events__user_id;
        Events__name = events__name;
        Events__start_date = events__start_date;
        Events__end_date = events__end_date;
        Events__all_day = events__all_day;
    }

    public String getEvents__id() {
        return Events__id;
    }

    public void setEvents__id(String events__id) {
        Events__id = events__id;
    }

    public String getEvents__user_id() {
        return Events__user_id;
    }

    public void setEvents__user_id(String events__user_id) {
        Events__user_id = events__user_id;
    }

    public String getEvents__name() {
        return Events__name;
    }

    public void setEvents__name(String events__name) {
        Events__name = events__name;
    }

    public String getEvents__start_date() {
        return Events__start_date;
    }

    public void setEvents__start_date(String events__start_date) {
        Events__start_date = events__start_date;
    }

    public String getEvents__end_date() {
        return Events__end_date;
    }

    public void setEvents__end_date(String events__end_date) {
        Events__end_date = events__end_date;
    }

    public String getEvents__all_day() {
        return Events__all_day;
    }

    public void setEvents__all_day(String events__all_day) {
        Events__all_day = events__all_day;
    }
}
