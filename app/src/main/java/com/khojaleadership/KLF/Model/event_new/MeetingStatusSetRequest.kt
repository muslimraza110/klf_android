package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class MeetingStatusSetRequest(
        @SerializedName("requested_by") val requestedById: String,
        @SerializedName("requested_to") val requestedToId: String,
        @SerializedName("user_id") val userId: String,
        @SerializedName("summit_events_program_details_id") val programDetailsId: String,
        @SerializedName("note") val meetingNotes: String,
        @SerializedName("status") val meetingStatus: String,
        @SerializedName("is_update") val isUpdate: Boolean
)
