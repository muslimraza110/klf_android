package com.khojaleadership.KLF.Model.Group_Models;

public class GroupDetailProjectDetailModel {

    String ProjectsGroups__project_id,ProjectsGroups__id,ProjectsGroups__group_id,ProjectsGroups__percent,Projects__id;
    String Projects__creator_id,Projects__khoja_care_category_id,Projects__name,Projects__website,Projects__description;

    public GroupDetailProjectDetailModel() {
    }

    public GroupDetailProjectDetailModel(String projectsGroups__project_id, String projectsGroups__id, String projectsGroups__group_id, String projectsGroups__percent, String projects__id, String projects__creator_id, String projects__khoja_care_category_id, String projects__name, String projects__website, String projects__description) {
        ProjectsGroups__project_id = projectsGroups__project_id;
        ProjectsGroups__id = projectsGroups__id;
        ProjectsGroups__group_id = projectsGroups__group_id;
        ProjectsGroups__percent = projectsGroups__percent;
        Projects__id = projects__id;
        Projects__creator_id = projects__creator_id;
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
        Projects__name = projects__name;
        Projects__website = projects__website;
        Projects__description = projects__description;
    }

    public String getProjectsGroups__project_id() {
        return ProjectsGroups__project_id;
    }

    public void setProjectsGroups__project_id(String projectsGroups__project_id) {
        ProjectsGroups__project_id = projectsGroups__project_id;
    }

    public String getProjectsGroups__id() {
        return ProjectsGroups__id;
    }

    public void setProjectsGroups__id(String projectsGroups__id) {
        ProjectsGroups__id = projectsGroups__id;
    }

    public String getProjectsGroups__group_id() {
        return ProjectsGroups__group_id;
    }

    public void setProjectsGroups__group_id(String projectsGroups__group_id) {
        ProjectsGroups__group_id = projectsGroups__group_id;
    }

    public String getProjectsGroups__percent() {
        return ProjectsGroups__percent;
    }

    public void setProjectsGroups__percent(String projectsGroups__percent) {
        ProjectsGroups__percent = projectsGroups__percent;
    }

    public String getProjects__id() {
        return Projects__id;
    }

    public void setProjects__id(String projects__id) {
        Projects__id = projects__id;
    }

    public String getProjects__creator_id() {
        return Projects__creator_id;
    }

    public void setProjects__creator_id(String projects__creator_id) {
        Projects__creator_id = projects__creator_id;
    }

    public String getProjects__khoja_care_category_id() {
        return Projects__khoja_care_category_id;
    }

    public void setProjects__khoja_care_category_id(String projects__khoja_care_category_id) {
        Projects__khoja_care_category_id = projects__khoja_care_category_id;
    }

    public String getProjects__name() {
        return Projects__name;
    }

    public void setProjects__name(String projects__name) {
        Projects__name = projects__name;
    }

    public String getProjects__website() {
        return Projects__website;
    }

    public void setProjects__website(String projects__website) {
        Projects__website = projects__website;
    }

    public String getProjects__description() {
        return Projects__description;
    }

    public void setProjects__description(String projects__description) {
        Projects__description = projects__description;
    }
}
