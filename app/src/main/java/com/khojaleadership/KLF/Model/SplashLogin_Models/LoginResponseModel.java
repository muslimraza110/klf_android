package com.khojaleadership.KLF.Model.SplashLogin_Models;

public class LoginResponseModel {
    String status,message;
    LoginResponseDataModel data;

    public LoginResponseModel() {
    }

    public LoginResponseModel(String status, String message, LoginResponseDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseDataModel getData() {
        return data;
    }

    public void setData(LoginResponseDataModel data) {
        this.data = data;
    }
}
