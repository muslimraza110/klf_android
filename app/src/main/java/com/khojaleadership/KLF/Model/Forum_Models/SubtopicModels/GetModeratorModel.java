package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

import java.util.ArrayList;

public class GetModeratorModel {

    String status,message;
    ArrayList<GetModeratorDataModel> data;

    public GetModeratorModel() {
    }

    public GetModeratorModel(String status, String message, ArrayList<GetModeratorDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetModeratorDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetModeratorDataModel> data) {
        this.data = data;
    }
}
