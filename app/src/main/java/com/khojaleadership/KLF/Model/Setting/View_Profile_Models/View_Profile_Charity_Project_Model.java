package com.khojaleadership.KLF.Model.Setting.View_Profile_Models;

import android.os.Parcel;
import android.os.Parcelable;

public class View_Profile_Charity_Project_Model implements Parcelable {

    String Groups__id, Groups__category_id, Groups__name, Groups__description, Groups__email,groupimage;
    String Groups__phone,Groups__address,Groups__address2,Groups__bio;
    String GroupsUsers__id,Groups__city,Groups__country;

    public View_Profile_Charity_Project_Model() {
    }

    public View_Profile_Charity_Project_Model(String groups__id, String groups__category_id, String groups__name, String groups__description, String groups__email, String groupimage, String groups__phone, String groups__address, String groups__address2, String groups__bio, String groupsUsers__id, String groups__city, String groups__country) {
        Groups__id = groups__id;
        Groups__category_id = groups__category_id;
        Groups__name = groups__name;
        Groups__description = groups__description;
        Groups__email = groups__email;
        this.groupimage = groupimage;
        Groups__phone = groups__phone;
        Groups__address = groups__address;
        Groups__address2 = groups__address2;
        Groups__bio = groups__bio;
        GroupsUsers__id = groupsUsers__id;
        Groups__city = groups__city;
        Groups__country = groups__country;
    }

    protected View_Profile_Charity_Project_Model(Parcel in) {
        Groups__id = in.readString();
        Groups__category_id = in.readString();
        Groups__name = in.readString();
        Groups__description = in.readString();
        Groups__email = in.readString();
        groupimage = in.readString();
        Groups__phone = in.readString();
        Groups__address = in.readString();
        Groups__address2 = in.readString();
        Groups__bio = in.readString();
        GroupsUsers__id = in.readString();
        Groups__city = in.readString();
        Groups__country = in.readString();
    }

    public static final Creator<View_Profile_Charity_Project_Model> CREATOR = new Creator<View_Profile_Charity_Project_Model>() {
        @Override
        public View_Profile_Charity_Project_Model createFromParcel(Parcel in) {
            return new View_Profile_Charity_Project_Model(in);
        }

        @Override
        public View_Profile_Charity_Project_Model[] newArray(int size) {
            return new View_Profile_Charity_Project_Model[size];
        }
    };

    public String getGroups__city() {
        return Groups__city;
    }

    public void setGroups__city(String groups__city) {
        Groups__city = groups__city;
    }

    public String getGroups__country() {
        return Groups__country;
    }

    public void setGroups__country(String groups__country) {
        Groups__country = groups__country;
    }

    public String getGroupsUsers__id() {
        return GroupsUsers__id;
    }

    public void setGroupsUsers__id(String groupsUsers__id) {
        GroupsUsers__id = groupsUsers__id;
    }

    public String getGroups__phone() {
        return Groups__phone;
    }

    public void setGroups__phone(String groups__phone) {
        Groups__phone = groups__phone;
    }

    public String getGroups__address() {
        return Groups__address;
    }

    public void setGroups__address(String groups__address) {
        Groups__address = groups__address;
    }

    public String getGroups__address2() {
        return Groups__address2;
    }

    public void setGroups__address2(String groups__address2) {
        Groups__address2 = groups__address2;
    }

    public String getGroups__bio() {
        return Groups__bio;
    }

    public void setGroups__bio(String groups__bio) {
        Groups__bio = groups__bio;
    }

    public String getGroupimage() {
        return groupimage;
    }

    public void setGroupimage(String groupimage) {
        this.groupimage = groupimage;
    }

    public String getGroups__id() {
        return Groups__id;
    }

    public void setGroups__id(String groups__id) {
        Groups__id = groups__id;
    }

    public String getGroups__category_id() {
        return Groups__category_id;
    }

    public void setGroups__category_id(String groups__category_id) {
        Groups__category_id = groups__category_id;
    }

    public String getGroups__name() {
        return Groups__name;
    }

    public void setGroups__name(String groups__name) {
        Groups__name = groups__name;
    }

    public String getGroups__description() {
        return Groups__description;
    }

    public void setGroups__description(String groups__description) {
        Groups__description = groups__description;
    }

    public String getGroups__email() {
        return Groups__email;
    }

    public void setGroups__email(String groups__email) {
        Groups__email = groups__email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Groups__id);
        dest.writeString(Groups__category_id);
        dest.writeString(Groups__name);
        dest.writeString(Groups__description);
        dest.writeString(Groups__email);
        dest.writeString(groupimage);
        dest.writeString(Groups__phone);
        dest.writeString(Groups__address);
        dest.writeString(Groups__address2);
        dest.writeString(Groups__bio);
        dest.writeString(GroupsUsers__id);
        dest.writeString(Groups__city);
        dest.writeString(Groups__country);
    }
}
