package com.khojaleadership.KLF.Model.Forum_Models.PostModels;

import java.util.ArrayList;

public class A_GetPostListModel {
    String status,message,totalcount,totalpages;
    ArrayList<A_GetPostListDataModel> data;

    public A_GetPostListModel() {
    }

    public A_GetPostListModel(String status, String message, String totalcount, String totalpages, ArrayList<A_GetPostListDataModel> data) {
        this.status = status;
        this.message = message;
        this.totalcount = totalcount;
        this.totalpages = totalpages;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }

    public String getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(String totalpages) {
        this.totalpages = totalpages;
    }

    public ArrayList<A_GetPostListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<A_GetPostListDataModel> data) {
        this.data = data;
    }
}
