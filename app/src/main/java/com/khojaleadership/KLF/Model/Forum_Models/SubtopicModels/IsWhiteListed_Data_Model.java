package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class IsWhiteListed_Data_Model {
    private String letters;//Display the initials of the pinyin
    String role,id,title,first_name,middle_name,last_name,email,forum_post_id;
    int iswhitelisted;

    public IsWhiteListed_Data_Model() {
    }

    public IsWhiteListed_Data_Model(String letters, String role, String id, String title, String first_name, String middle_name, String last_name, String email, String forum_post_id, int iswhitelisted) {
        this.letters = letters;
        this.role = role;
        this.id = id;
        this.title = title;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.email = email;
        this.forum_post_id = forum_post_id;
        this.iswhitelisted = iswhitelisted;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForum_post_id() {
        return forum_post_id;
    }

    public void setForum_post_id(String forum_post_id) {
        this.forum_post_id = forum_post_id;
    }

    public int getIswhitelisted() {
        return iswhitelisted;
    }

    public void setIswhitelisted(int iswhitelisted) {
        this.iswhitelisted = iswhitelisted;
    }
}
