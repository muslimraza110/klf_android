package com.khojaleadership.KLF.Model.Feedback_Models;

public class FeedbackRequestModel {
    String user_id,name,description;

    public FeedbackRequestModel() {
    }

    public FeedbackRequestModel(String user_id, String name, String description) {
        this.user_id = user_id;
        this.name = name;
        this.description = description;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
