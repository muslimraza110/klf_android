package com.khojaleadership.KLF.Model.Resource;

public class ResourcesCategoreyListDataModel {
    String Categories__id,Categories__model,Categories__name,Categories__description;

    public ResourcesCategoreyListDataModel() {
    }

    public ResourcesCategoreyListDataModel(String categories__id, String categories__model, String categories__name, String categories__description) {
        Categories__id = categories__id;
        Categories__model = categories__model;
        Categories__name = categories__name;
        Categories__description = categories__description;
    }

    public String getCategories__id() {
        return Categories__id;
    }

    public void setCategories__id(String categories__id) {
        Categories__id = categories__id;
    }

    public String getCategories__model() {
        return Categories__model;
    }

    public void setCategories__model(String categories__model) {
        Categories__model = categories__model;
    }

    public String getCategories__name() {
        return Categories__name;
    }

    public void setCategories__name(String categories__name) {
        Categories__name = categories__name;
    }

    public String getCategories__description() {
        return Categories__description;
    }

    public void setCategories__description(String categories__description) {
        Categories__description = categories__description;
    }
}
