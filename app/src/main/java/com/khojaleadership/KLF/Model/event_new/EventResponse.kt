package com.khojaleadership.KLF.Model.event_new

import com.google.gson.annotations.SerializedName

data class SingleEventResponse(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("venue_address") val address: String?,
        @SerializedName("short_description") val shortDescription: String?,
        @SerializedName("long_description") val longDescription: String?,
        @SerializedName("start_date") val startDate: String?,
        @SerializedName("end_date") val endDate: String?,
        @SerializedName("price_range_start") val priceRangeStart: String?,
        @SerializedName("price_range_end") val priceRangeEnd: String?,
        @SerializedName("venue_city") val city: String?,

        @SerializedName("registration_link") val registrationLink: String?,

        @SerializedName("event_images") val images: List<String?>?,
        @SerializedName("is_userRegistered") var isUserRegistered: Boolean?,
        @SerializedName("summit_events_faculty_id") var summitEventsFacultyId: String?,
        @SerializedName("meeting_slots") val meetingSlots: List<SummitEventDayAvailability>?


)