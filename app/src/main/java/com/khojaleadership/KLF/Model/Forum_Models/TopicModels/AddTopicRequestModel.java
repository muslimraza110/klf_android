package com.khojaleadership.KLF.Model.Forum_Models.TopicModels;

public class AddTopicRequestModel {
    String user_id,category_name,subtopic_title,subtopic_description;

    public AddTopicRequestModel() {
    }

    public AddTopicRequestModel(String user_id, String category_name, String subtopic_title, String subtopic_description) {
        this.user_id = user_id;
        this.category_name = category_name;
        this.subtopic_title = subtopic_title;
        this.subtopic_description = subtopic_description;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getSubtopic_title() {
        return subtopic_title;
    }

    public void setSubtopic_title(String subtopic_title) {
        this.subtopic_title = subtopic_title;
    }

    public String getSubtopic_description() {
        return subtopic_description;
    }

    public void setSubtopic_description(String subtopic_description) {
        this.subtopic_description = subtopic_description;
    }
}
