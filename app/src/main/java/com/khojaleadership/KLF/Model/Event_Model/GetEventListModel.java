package com.khojaleadership.KLF.Model.Event_Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetEventListModel implements Parcelable {

    String status,message;
    ArrayList<GetEventsListDataModel> data;

    public GetEventListModel() {
    }

    public GetEventListModel(String status, String message, ArrayList<GetEventsListDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    protected GetEventListModel(Parcel in) {
        status = in.readString();
        message = in.readString();
    }

    public static final Creator<GetEventListModel> CREATOR = new Creator<GetEventListModel>() {
        @Override
        public GetEventListModel createFromParcel(Parcel in) {
            return new GetEventListModel(in);
        }

        @Override
        public GetEventListModel[] newArray(int size) {
            return new GetEventListModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GetEventsListDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetEventsListDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
    }
}
