package com.khojaleadership.KLF.Model.Mission;

public class GetMissionDataModel {

    String Pages__id,Pages__title,Pages__content,Pages__created,Pages__modified;

    public GetMissionDataModel() {
    }

    public GetMissionDataModel(String pages__id, String pages__title, String pages__content, String pages__created, String pages__modified) {
        Pages__id = pages__id;
        Pages__title = pages__title;
        Pages__content = pages__content;
        Pages__created = pages__created;
        Pages__modified = pages__modified;
    }

    public String getPages__id() {
        return Pages__id;
    }

    public void setPages__id(String pages__id) {
        Pages__id = pages__id;
    }

    public String getPages__title() {
        return Pages__title;
    }

    public void setPages__title(String pages__title) {
        Pages__title = pages__title;
    }

    public String getPages__content() {
        return Pages__content;
    }

    public void setPages__content(String pages__content) {
        Pages__content = pages__content;
    }

    public String getPages__created() {
        return Pages__created;
    }

    public void setPages__created(String pages__created) {
        Pages__created = pages__created;
    }

    public String getPages__modified() {
        return Pages__modified;
    }

    public void setPages__modified(String pages__modified) {
        Pages__modified = pages__modified;
    }
}
