package com.khojaleadership.KLF.Model.Forum_Models.TopicModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetTopicModel implements Parcelable {
    String status,message,totalcount,totalpages;
    ArrayList<GetTopicDataModel> data;

    public GetTopicModel() {
    }

    public GetTopicModel(String status, String message, String totalcount, String totalpages, ArrayList<GetTopicDataModel> data) {
        this.status = status;
        this.message = message;
        this.totalcount = totalcount;
        this.totalpages = totalpages;
        this.data = data;
    }

    protected GetTopicModel(Parcel in) {
        status = in.readString();
        message = in.readString();
        totalcount = in.readString();
        totalpages = in.readString();
    }

    public static final Creator<GetTopicModel> CREATOR = new Creator<GetTopicModel>() {
        @Override
        public GetTopicModel createFromParcel(Parcel in) {
            return new GetTopicModel(in);
        }

        @Override
        public GetTopicModel[] newArray(int size) {
            return new GetTopicModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }

    public String getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(String totalpages) {
        this.totalpages = totalpages;
    }

    public ArrayList<GetTopicDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<GetTopicDataModel> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
        dest.writeString(totalcount);
        dest.writeString(totalpages);
    }
}
