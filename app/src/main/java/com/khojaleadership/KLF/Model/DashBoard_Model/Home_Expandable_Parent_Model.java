package com.khojaleadership.KLF.Model.DashBoard_Model;

import java.util.List;

public class Home_Expandable_Parent_Model {


    public int images;
    public String Title;
    public List<Home_Expandable_Child_Model> model_child;


    public Home_Expandable_Parent_Model(int images, String title, List<Home_Expandable_Child_Model> model_child) {
        this.images = images;
        Title = title;
        this.model_child = model_child;
    }

    public Home_Expandable_Parent_Model() {
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public List<Home_Expandable_Child_Model> getModel_child() {
        return model_child;
    }

    public void setModel_child(List<Home_Expandable_Child_Model> model_child) {
        this.model_child = model_child;
    }
}
