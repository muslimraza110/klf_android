package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class GetWhitelistedGroup_Data_Model {
    private String letters;//Display the initials of the pinyin
    String id,name,email,phone;
    int iswhitelisted;

    public GetWhitelistedGroup_Data_Model() {
    }

    public GetWhitelistedGroup_Data_Model(String letters, String id, String name, String email, String phone, int iswhitelisted) {
        this.letters = letters;
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.iswhitelisted = iswhitelisted;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIswhitelisted() {
        return iswhitelisted;
    }

    public void setIswhitelisted(int iswhitelisted) {
        this.iswhitelisted = iswhitelisted;
    }
}
