package com.khojaleadership.KLF.Model.Intiative_Models;

public class Intiative_Detail_Model {
    String status,message;
    Intiative_Detail_Data_Model data;

    public Intiative_Detail_Model() {
    }

    public Intiative_Detail_Model(String status, String message, Intiative_Detail_Data_Model data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Intiative_Detail_Data_Model getData() {
        return data;
    }

    public void setData(Intiative_Detail_Data_Model data) {
        this.data = data;
    }
}
