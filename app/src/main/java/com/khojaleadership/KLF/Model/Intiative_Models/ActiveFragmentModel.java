package com.khojaleadership.KLF.Model;

import java.util.ArrayList;

public class ActiveFragmentModel {
    String status,message;
    ArrayList<ActiveFragmentDataModel> data;

    public ActiveFragmentModel() {
    }

    public ActiveFragmentModel(String status, String message, ArrayList<ActiveFragmentDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ActiveFragmentDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<ActiveFragmentDataModel> data) {
        this.data = data;
    }
}

