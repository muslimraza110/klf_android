package com.khojaleadership.KLF.Model.Project_Models;

public class DeleteProjectRequestModel {
    String project_id;

    public DeleteProjectRequestModel() {
    }

    public DeleteProjectRequestModel(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }
}
