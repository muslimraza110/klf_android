package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Send_Eblast_Parent_Model implements Parcelable {
    String Parent_name;
    ArrayList<Send_Eblast_Child_Model> child_model;

    public Send_Eblast_Parent_Model() {
    }

    public Send_Eblast_Parent_Model(String parent_name, ArrayList<Send_Eblast_Child_Model> child_model) {
        Parent_name = parent_name;
        this.child_model = child_model;
    }

    protected Send_Eblast_Parent_Model(Parcel in) {
        Parent_name = in.readString();
        child_model = in.createTypedArrayList(Send_Eblast_Child_Model.CREATOR);
    }

    public static final Creator<Send_Eblast_Parent_Model> CREATOR = new Creator<Send_Eblast_Parent_Model>() {
        @Override
        public Send_Eblast_Parent_Model createFromParcel(Parcel in) {
            return new Send_Eblast_Parent_Model(in);
        }

        @Override
        public Send_Eblast_Parent_Model[] newArray(int size) {
            return new Send_Eblast_Parent_Model[size];
        }
    };

    public String getParent_name() {
        return Parent_name;
    }

    public void setParent_name(String parent_name) {
        Parent_name = parent_name;
    }

    public ArrayList<Send_Eblast_Child_Model> getChild_model() {
        return child_model;
    }

    public void setChild_model(ArrayList<Send_Eblast_Child_Model> child_model) {
        this.child_model = child_model;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Parent_name);
        dest.writeTypedList(child_model);
    }
}
