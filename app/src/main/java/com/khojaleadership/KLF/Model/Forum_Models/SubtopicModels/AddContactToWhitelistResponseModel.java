package com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels;

public class AddContactToWhitelistResponseModel {
    String status,message;

    public AddContactToWhitelistResponseModel() {
    }

    public AddContactToWhitelistResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

