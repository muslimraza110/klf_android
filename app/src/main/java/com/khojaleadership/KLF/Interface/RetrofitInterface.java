package com.khojaleadership.KLF.Interface;


import com.khojaleadership.KLF.Model.ActiveFragmentModel;
import com.khojaleadership.KLF.Model.Contact.GetContactListModel;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_ForumsMasterSearch_Model;
import com.khojaleadership.KLF.Model.Event_Model.AddEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.AddEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.EditEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.EditEventResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveGroupFromWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveGroupFromWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddGroupToWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddGroupToWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.DeleteGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.DeleteGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.ListCatagoryModel;
import com.khojaleadership.KLF.Model.Intiative_Models.SubmitInitiativeFundraisingRequestModel;
import com.khojaleadership.KLF.Model.Intiative_Models.SubmitInitiativeFundraisingResponseModel;
import com.khojaleadership.KLF.Model.Resource.AddResourcePostRequestModel;
import com.khojaleadership.KLF.Model.Resource.AddResourcePostResponseModel;
import com.khojaleadership.KLF.Model.Resource.Add_Active_News_ToArchieve_RequestModel;
import com.khojaleadership.KLF.Model.Resource.Add_Active_News_ToArchieve_ResponseModel;
import com.khojaleadership.KLF.Model.Resource.Add_Archieve_News_ToActive_RequestModel;
import com.khojaleadership.KLF.Model.Resource.Add_Archieve_News_ToActive_ResponseModel;
import com.khojaleadership.KLF.Model.Resource.DeleteResourcePostRequestModel;
import com.khojaleadership.KLF.Model.Resource.DeleteResourcePostResponseModel;
import com.khojaleadership.KLF.Model.Resource.EditResourcePostRequestModel;
import com.khojaleadership.KLF.Model.Resource.EditResourcePostResponseModel;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Model;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Model;
import com.khojaleadership.KLF.Model.Resource.Publish_News_Request_Model;
import com.khojaleadership.KLF.Model.Resource.Publish_News_Response_Model;
import com.khojaleadership.KLF.Model.Resource.ResourcesCategoreyListModel;
import com.khojaleadership.KLF.Model.Resource.Unpublish_News_Request_Model;
import com.khojaleadership.KLF.Model.Resource.Unpublish_News_Response_Model;
import com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models.Edit_ProfileRequest_Model;
import com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models.Edit_ProfileResponse_Model;
import com.khojaleadership.KLF.Model.Event_Model.GetEventListModel;
import com.khojaleadership.KLF.Model.FAQ_Models.FAQ_Model;
import com.khojaleadership.KLF.Model.Feedback_Models.FeedbackRequestModel;
import com.khojaleadership.KLF.Model.Feedback_Models.FeedbackResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddQouteRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddQouteResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddContactToWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddContactToWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddModeratorRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddModeratorResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.EditSubTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.EditSubTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetModeratorModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveContactFromWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveContactFromWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveModeratorRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveModeratorResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.AddTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.AddTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetWhitelistedGroup_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.IsWhiteListed_Model;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.A_GetPostListModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddPostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddPostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.ApproveCommentRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.ApproveCommentResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.EditPostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.EditPostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.LikeDislikeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.LikeDislikeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.SubcribeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.SubcribeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.UnSubscribeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.UnSubscribeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddSubTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddSubTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.DeleteTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.DeleteTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicModel;
import com.khojaleadership.KLF.Model.Group_Models.AddGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.AddGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.AddMemberToGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.AddMemberToGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.ApproveGroupJoinRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.ApproveGroupJoinResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.EditGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.EditGroup_PendingAccessRequests_Model;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailMembersListModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailForumPostsModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailEventListModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailPendingAccessRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailRelatedGroupModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailProjectModel;
import com.khojaleadership.KLF.Model.Group_Models.JoinGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.JoinGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveGroupJoinRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveGroupJoinResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupResponseMOdel;
import com.khojaleadership.KLF.Model.Mission.GetMissionModel;
import com.khojaleadership.KLF.Model.Project_Models.Accept_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Accept_Project_Access_Response_Model;
import com.khojaleadership.KLF.Model.Project_Models.AddMemberToProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddMemberToProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.AddPercentageRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddPercentageResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.AddProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Cancel_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Cancel_Project_Access_Response_Model;
import com.khojaleadership.KLF.Model.Project_Models.DeleteProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.DeleteProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.EditProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.EditProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectModel;
import com.khojaleadership.KLF.Model.Project_Models.Percentage_List_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectDetailModel;
import com.khojaleadership.KLF.Model.Project_Models.Project_Access_Request_List_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_CharityType_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_Member_List;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Request_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Response_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Add_Plege_Request_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Add_Plege_Response_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Encrypt_Request_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Encrypt_Response_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Group_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelGroupJoinRequest_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelGroupJoinResponse_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelProjectJoinRequest_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelProjectJoinResponse_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_TargetAnualSpend_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsModel;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Send_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Send_Project_Access_Response_Model;
import com.khojaleadership.KLF.Model.Project_Models.TargetAudienceModel;
import com.khojaleadership.KLF.Model.Setting.ChangePasswordRequestModel;
import com.khojaleadership.KLF.Model.Setting.ChangePasswordResponseModel;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Model;
import com.khojaleadership.KLF.Model.SplashLogin_Models.ForgetPasswordRequestModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.ForgetPasswordResponseModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginRequestModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseModel;
import com.khojaleadership.KLF.Model.logout.LogoutRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitInterface {


    @POST("users/login.json")
    Call<LoginResponseModel> getLoginAccess(@Header("Content-Type") String header,
                                            @Body LoginRequestModel request);



    @POST("users/logout.json")
    Call<Object> logout(@Header("Content-Type") String header,
                        @Header("auth-token") String token,
                        @Body LogoutRequest request);

    @POST("users/forgotpassword.json")
    Call<ForgetPasswordResponseModel> getForgetPasswordAccess(@Header("Content-Type") String header,
                                                              @Header("auth-token") String token,
                                                              @Body ForgetPasswordRequestModel request);


    //...............HOME DATA........
    //get already groups in project
    @GET("home/homecontent.json")
    Call<Home_Model> GetHomeData(@Header("Content-Type") String header,
                                 @Header("auth-token") String token,
                                 @Query("user_id") String user_id);

    @GET("forums/forumsearch.json")
    Call<Home_ForumsMasterSearch_Model> MainForumsMasterSearch(@Header("Content-Type") String header,
                                                               @Header("auth-token") String token,
                                                               @Query("q") String q);

    @POST("groups/cancelgroupjoinrequest.json")
    Call<Home_CancelGroupJoinResponse_Model> Home_Cancel_GroupJoinRequest(@Header("Content-Type") String header,
                                                                          @Header("auth-token") String token,
                                                                          @Body Home_CancelGroupJoinRequest_Model request);

    @POST("projects/cancelprojectaccessrequest.json")
    Call<Home_CancelProjectJoinResponse_Model> Home_Cancel_ProjectJoinRequest(@Header("Content-Type") String header,
                                                                              @Header("auth-token") String token,
                                                                              @Body Home_CancelProjectJoinRequest_Model request);


    @GET("forums/gettopics.json")
    Call<GetTopicModel> getTopic(@Header("Content-Type") String header,
                                 @Header("auth-token") String token,
                                 @Query("searchcatagory") String searchcatagory,
                                 @Query("page_no") int page_no,
                                 @Query("pagesize") int pagesize);

     //updated
    @GET("forums/ getsubtopicsdetailsv2.json")
    Call<GetSubTopicModel> getSubTopic(@Header("Content-Type") String header,
                                       @Header("auth-token") String token,
                                       @Query("sub_forum_id") int sub_forum_id,
                                       @Query("page_no") int page_no,
                                       @Query("pagesize") int pagesize,
                                       @Query("user_id") int user_id);


    @GET("forums/subtopicspostlist.json")
    Call<A_GetPostListModel> getPostList(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Query("forum_post_id") int forum_post_id,
                                         @Query("pagesize") int pagesize,
                                         @Query("page_no") int page_no,
                                         @Query("user_id") int user_id);


    @GET("forums /subtopicspostlistv2.json")
    Call<GetPostListModel_Updated> getPostListUpdated(@Header("Content-Type") String header,
                                                      @Header("auth-token") String token,
                                                      @Query("forum_post_id") int forum_post_id,
                                                      @Query("pagesize") int pagesize,
                                                      @Query("page_no") int page_no,
                                                      @Query("user_id") int user_id);



    @POST("forums/addsubtopics.json")
    Call<AddTopicResponseModel> addTopic(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Body AddTopicRequestModel request);

    @POST("forums/addsubtopicsposts.json")
    Call<AddSubTopicResponseModel> addSubTopic(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body AddSubTopicRequestModel request);

    @POST("forums/addforumpostreply.json")
    Call<AddPostResponseModel> addSubTopicPost(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body AddPostRequestModel request);

    //...........send eblast........................

    @POST("forums/sendeblast.json")
    Call<Send_Eblast_Response_Model> SendEblastFunction(@Header("Content-Type") String header,
                                                        @Header("auth-token") String token,
                                                        @Body Send_Eblast_Request_Model request);

    //...........send eblast........................


    @POST("forums/editsubtopicsposts.json")
    Call<EditSubTopicResponseModel> editSubTopic(@Header("Content-Type") String header,
                                                 @Header("auth-token") String token,
                                                 @Body EditSubTopicRequestModel request);

    @POST("forums/editforumpost.json")
    Call<EditPostResponseModel> editSubTopicPost(@Header("Content-Type") String header,
                                                 @Header("auth-token") String token,
                                                 @Body EditPostRequestModel request);

    @POST("forums/subscribeforumpost.json")
    Call<SubcribeResponseModel> SubcribePost(@Header("Content-Type") String header,
                                             @Header("auth-token") String token,
                                             @Body SubcribeRequestModel request);

    @POST("forums/unsubscribeforumpost.json")
    Call<UnSubscribeResponseModel> UnSubcribePost(@Header("Content-Type") String header,
                                                  @Header("auth-token") String token,
                                                  @Body UnSubscribeRequestModel request);

    @POST("forums/addquote.json")
    Call<AddQouteResponseModel> AddQoute(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Body AddQouteRequestModel request);

    @POST("forums/approvereply.json")
    Call<ApproveCommentResponseModel> ApproveComment(@Header("Content-Type") String header,
                                                     @Header("auth-token") String token,
                                                     @Body ApproveCommentRequestModel request);


    @GET("groups/grouplistbytype.json")
    Call<GetGroupListModel> getGroups(@Header("Content-Type") String header,
                                      @Header("auth-token") String token,
                                      @Query("type") String type,
                                      @Query("page_no") int page_no,
                                      @Query("pagesize") int pagesize,
                                      @Query("user_id") String user_id);


    @POST("groups/addgroup.json")
    Call<AddGroupResponseModel> AddGroup(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Body AddGroupRequestModel request);

    @POST("groups/editgroup.json")
    Call<AddGroupResponseModel> EditGroup(@Header("Content-Type") String header,
                                          @Header("auth-token") String token,
                                          @Body EditGroupRequestModel request);

    @POST("groups/deletegroup.json")
    Call<DeleteGroupResponseModel> DeleteGroup(@Header("Content-Type") String header, @Header("auth-token") String token,
                                               @Body DeleteGroupRequestModel request);


    @POST("forums/forumpostslikesdislikes.json")
    Call<LikeDislikeResponseModel> LikeDislike(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body LikeDislikeRequestModel request);


    @POST("forums/deletesubforum.json")
    Call<DeleteTopicResponseModel> DeleteTopic(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body DeleteTopicRequestModel request);


    @POST("forums/deleteforumpost.json")
    Call<DeletePostResponseModel> DeletePost(@Header("Content-Type") String header,
                                             @Header("auth-token") String token,
                                             @Body DeletePostRequestModel request);


    @GET("forums/postmoderator.json")
    Call<GetModeratorModel> getModerator(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Query("post_id") int post_id);

    //add moderator
    @POST("forums/addpostmoderator.json")
    Call<AddModeratorResponseModel> AddModerator(@Header("Content-Type") String header,
                                                 @Header("auth-token") String token,
                                                 @Body AddModeratorRequestModel request);

    //Remove moderator
    @POST("forums/removepostmoderator.json")
    Call<RemoveModeratorResponseModel> RemoveModerator(@Header("Content-Type") String header,
                                                       @Header("auth-token") String token,
                                                       @Body RemoveModeratorRequestModel request);

    @POST("forums/addgrouptowhitelist.json")
    Call<AddGroupToWhitelistResponseModel> AddGroupToWhiteList(@Header("Content-Type") String header,
                                                               @Header("auth-token") String token,
                                                               @Body AddGroupToWhitelistRequestModel request);

    @POST("forums/removegrouptowhitelist.json")
    Call<RemoveGroupFromWhitelistResponseModel> RemoveGroupFromWhiteList(@Header("Content-Type") String header,
                                                                         @Header("auth-token") String token,
                                                                         @Body RemoveGroupFromWhitelistRequestModel request);


    @POST("forums/addcontacttowhitelist.json")
    Call<AddContactToWhitelistResponseModel> AddContactToWhiteList(@Header("Content-Type") String header,
                                                                   @Header("auth-token") String token,
                                                                   @Body AddContactToWhitelistRequestModel request);

    @POST("forums/removecontacttowhitelist.json")
    Call<RemoveContactFromWhitelistResponseModel> RemoveContactFromWhiteList(@Header("Content-Type") String header,
                                                                             @Header("auth-token") String token,
                                                                             @Body RemoveContactFromWhitelistRequestModel request);


    //already white listed contacts
    @GET("forums/iswhitelistusers.json")
    Call<IsWhiteListed_Model> getIsWhitelistedContacts(@Header("Content-Type") String header,
                                                       @Header("auth-token") String token,
                                                       @Query("post_id") String post_id);


    //already white listed Groups
    @GET("forums/iswhitelistgroups.json")
    Call<GetWhitelistedGroup_Model> getWhitelistedGroupsUpdated(@Header("Content-Type") String header,
                                                                @Header("auth-token") String token,
                                                                @Query("post_id") String user_id);



    //....................................Project Section.................
    //get Projects List
    @GET("projects/listallprojects.json")
    Call<ProjectsModel> getProjectsList(@Header("Content-Type") String header, @Header("auth-token") String token);

    //get project details
    @GET("projects/detailproject.json")
    Call<ProjectDetailModel> getProjectsDetail(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Query("project_id") String project_id,
                                               @Query("page_no") String page_no,
                                               @Query("pagesize") String pagesize,
                                               @Query("UserId") String UserId);


    //get project access request list
    @GET("projects/projectaccessrequest.json")
    Call<Project_Access_Request_List_Model> getProjectAccessRequestList(@Header("Content-Type") String header,
                                                                        @Header("auth-token") String token,
                                                                        @Query("project_id") String project_id);

    //Send project access request
    @POST("projects/submitprojectaccessrequest.json")
    Call<Send_Project_Access_Response_Model> SendProjectAccessRequest(@Header("Content-Type") String header,
                                                                      @Header("auth-token") String token,
                                                                      @Body Send_Project_Access_Request_Model requestModel);


    //Accespt project access request
    @POST("projects/acceptprojectrequest.json")
    Call<Accept_Project_Access_Response_Model> AcceptProjectAccessRequest(@Header("Content-Type") String header,
                                                                          @Header("auth-token") String token,
                                                                          @Body Accept_Project_Access_Request_Model requestModel);

    //Cancel project access request
    @POST("projects/cancelprojectaccessrequest.json")
    Call<Cancel_Project_Access_Response_Model> CancelProjectAccessRequest(@Header("Content-Type") String header,
                                                                          @Header("auth-token") String token,
                                                                          @Body Cancel_Project_Access_Request_Model requestModel);

    //add project
    @POST("projects/addproject.json")
    Call<AddProjectResponseModel> AddProject(@Header("Content-Type") String header,
                                             @Header("auth-token") String token,
                                             @Body AddProjectRequestModel requestModel);

    //edit project
    @POST("projects/updateprojectinfo.json")
    Call<EditProjectResponseModel> EditProject(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body EditProjectRequestModel requestModel);

    //Get Percentage list
    @GET("projects/projectsgroups.json")
    Call<Percentage_List_Model> GetPercentageList(@Header("Content-Type") String header,
                                                  @Header("auth-token") String token,
                                                  @Query("project_id") String project_id);

    //Add percentage
    @POST("projects/addpercentage.json")
    Call<AddPercentageResponseModel> AddPercentage(@Header("Content-Type") String header,
                                                   @Header("auth-token") String token,
                                                   @Body AddPercentageRequestModel requestModel);


    //Delete project
    @POST("projects/deleteproject.json")
    Call<DeleteProjectResponseModel> DeleteProject(@Header("Content-Type") String header,
                                                   @Header("auth-token") String token,
                                                   @Body DeleteProjectRequestModel requestModel);


    //get project access request list
    @GET("projects/projectmemberlist.json")
    Call<Project_Member_List> GetProjectMemberList(@Header("Content-Type") String header,
                                                   @Header("auth-token") String token,
                                                   @Query("project_id") String project_id);

    //Add member to project
    @POST("projects/addmembertoproject.json")
    Call<AddMemberToProjectResponseModel> AddMemberToProject(@Header("Content-Type") String header,
                                                             @Header("auth-token") String token,
                                                             @Body AddMemberToProjectRequestModel requestModel);

    //Remove member from project
    @POST("projects/removememberfromproject.json")
    Call<RemoveMemberFromProjectResponseModel> RemoveMemberFromProject(@Header("Content-Type") String header,
                                                                       @Header("auth-token") String token,
                                                                       @Body RemoveMemberFromProjectRequestModel requestModel);






    //get charity type
    @GET("khojacare/charitytype.json")
    Call<Project_CharityType_Model> getCharityType(@Header("Content-Type") String header, @Header("auth-token") String token);


    //get target AUdience
    @GET("khojacare/targetaudience.json")
    Call<TargetAudienceModel> getTargetAudience(@Header("Content-Type") String header, @Header("auth-token") String token);

    //get target anual spend
    @GET("khojacare/targetanualspent.json")
    Call<Project_TargetAnualSpend_Model> getTargetAnualSpend(@Header("Content-Type") String header, @Header("auth-token") String token);

    //......................................................................................End......................................//


    //get Catagory List
    @GET("groups/listcatagory.json")
    Call<ListCatagoryModel> getCatagoryList(@Header("Content-Type") String header, @Header("auth-token") String token);


    //get businuess and charity group List
    @GET("groups/listofallgroups.json")
    Call<GetAllGroup_ProjectListModel> getProjectAllGroupList(@Header("Content-Type") String header, @Header("auth-token") String token);


    //get already groups in project
    @GET("projects/listgroups.json")
    Call<GetAlreadyGroup_ProjectModel> getAlreadyGroupsInProject(@Header("Content-Type") String header,
                                                                 @Header("auth-token") String token,
                                                                 @Query("project_id") String project_id);


    //Add groups To project
    @POST("projects/ addgrouptoproject.json")
    Call<Add_group_To_ProjectResponseModel> AddGroupToProject(@Header("Content-Type") String header,
                                                              @Header("auth-token") String token,
                                                              @Body Add_group_To_ProjectRequestModel requestModel);


    //Remove group from project
    @POST("projects/removegroupfromproject.json")
    Call<Remove_Group_From_ProjectResponseModel> RemoveGroupFromProject(@Header("Content-Type") String header,
                                                                        @Header("auth-token") String token,
                                                                        @Body Remove_Group_From_ProjectRequestModel requestModel);


    //group detail related tasks

    /////////////////////////////member section...............................
    //get members list  Done
    @GET("groups/listgroupsmembers.json")
    Call<GroupDetailMembersListModel> getGroupMembersList(@Header("Content-Type") String header,
                                                          @Header("auth-token") String token,
                                                          @Query("group_id") String group_id);

    //.................here is the issue......................
    //join group request Done
    @POST("groups/joingroup.json")
    Call<JoinGroupResponseModel> JoinGroupRrequest(@Header("Content-Type") String header,
                                                   @Header("auth-token") String token,
                                                   @Body JoinGroupRequestModel requestModel);


    //remove user from group  Done
    @POST("groups/removeuserfromgroup.json")
    Call<RemoveMemberFromGroupResponseMOdel> RemoveMemberFromGroup(@Header("Content-Type") String header,
                                                                   @Header("auth-token") String token,
                                                                   @Body RemoveMemberFromGroupRequestModel requestModel);

    /////////////////////////////member section end...............................


    /////////////////////////////Event section...............................
    //get event list
    @GET("groups/groupeventlist.json")
    Call<GroupDetailEventListModel> getGroupEventList_New(@Header("Content-Type") String header,
                                                          @Header("auth-token") String token,
                                                          @Query("group_id") String group_id);

    //get related group list  Done
    @GET("groups/relatedgroups.json")
    Call<GroupDetailRelatedGroupModel> getGroupRelatedGroupList(@Header("Content-Type") String header,
                                                                @Header("auth-token") String token,
                                                                @Query("user_id") String user_id,
                                                                @Query("group_id") String group_id);

    //get group project list  Done
    @GET("groups/grouprojectlist.json")
    Call<GroupDetailProjectModel> getGroupGroupProjectList(@Header("Content-Type") String header,
                                                           @Header("auth-token") String token,
                                                           @Query("group_id") String group_id);


    ///////////////////////////last section......................................done
    //get pending access requests list done
    @GET("groups/groupjoiningrequests.json")
    Call<GroupDetailPendingAccessRequestModel> getPendingAccessRequests(@Header("Content-Type") String header,
                                                                        @Header("auth-token") String token,
                                                                        @Query("group_id") String group_id);

    //approve group join request done
    @POST("groups/approvegrouprequest.json")
    Call<ApproveGroupJoinResponseModel> ApproveGroupJoinRequest(@Header("Content-Type") String header,
                                                                @Header("auth-token") String token,
                                                                @Body ApproveGroupJoinRequestModel requestModel);

    //Reject group join request done
    @POST("groups/removependingrequests.json")
    Call<RemoveGroupJoinResponseModel> RemoveGroupJoinRequest(@Header("Content-Type") String header,
                                                              @Header("auth-token") String token,
                                                              @Body RemoveGroupJoinRequestModel requestModel);


//.................end......................................................................


    //in edit group section
    //get member list
    @GET("groups/isgroupmembers.json")
    Call<GetGroupMemberListModel> getMemberListInEditGroup(@Header("Content-Type") String header,
                                                           @Header("auth-token") String token,
                                                           @Query("group_id") String group_id);


    //Add Member in Group
    @POST("groups/addmememberinagroup.json")
    Call<AddMemberToGroupResponseModel> AddMemberInGroup(@Header("Content-Type") String header,
                                                         @Header("auth-token") String token,
                                                         @Body AddMemberToGroupRequestModel requestModel);

    //Remove Member in Group
    @POST("groups/removeuserfromgroup.json")
    Call<RemoveMemberFromGroupResponseMOdel> RemoveMemberInGroup(@Header("Content-Type") String header,
                                                                 @Header("auth-token") String token,
                                                                 @Body RemoveMemberFromGroupRequestModel requestModel);


    //get pending access request list
    @GET("groups/groupjoiningrequests.json")
    Call<EditGroup_PendingAccessRequests_Model> EditGroupPendingAccessRequests(@Header("Content-Type") String header,
                                                                               @Header("auth-token") String token,
                                                                               @Query("group_id") String group_id);


    //get group forum post list
    @GET("groups/listforumspostsgroups.json")
    Call<GroupDetailForumPostsModel> getGroupForumPostList(@Header("Content-Type") String header,
                                                           @Header("auth-token") String token,
                                                           @Query("group_id") String group_id);


    //.......................FAQ Section................
    //get FAQ List
    @GET("faq/listfaq.json")
    Call<FAQ_Model> getFAQList(@Header("Content-Type") String header, @Header("auth-token") String token);

    //.......................FAQ Section END................
    //Change Password
    @POST("contact/changepassword.json")
    Call<ChangePasswordResponseModel> ChangePassword(@Header("Content-Type") String header,
                                                     @Header("auth-token") String token,
                                                     @Body ChangePasswordRequestModel requestModel);

    //get Contact List
    @GET("contact/listcontacts.json")
    Call<GetContactListModel> getContactList(@Header("Content-Type") String header, @Header("auth-token") String token);


    //...................................Event Section..........................

    //get Events List
    @GET("events/listevents.json")
    Call<GetEventListModel> getAllEventList(@Header("Content-Type") String header, @Header("auth-token") String token);


    //get Events List per month
    @GET("events/listevents.json")
    Call<GetEventListModel> getEventsByDate(@Header("Content-Type") String header,
                                            @Header("auth-token") String token,
                                            @Query("year") String year,
                                            @Query("month") String month);

    //Add Event
    @POST("events/addevents.json")
    Call<AddEventResponseModel> AddEvent(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Body AddEventRequestModel requestModel);

    //Edit Event
    @POST("events/editevent.json")
    Call<EditEventResponseModel> EditEvent(@Header("Content-Type") String header,
                                           @Header("auth-token") String token,
                                           @Body EditEventRequestModel requestModel);

    //Delete Event
    @POST("events/deletevent.json")
    Call<DeleteEventResponseModel> DeleteEvent(@Header("Content-Type") String header,
                                               @Header("auth-token") String token,
                                               @Body DeleteEventRequestModel requestModel);


    //...................................Event Section End..........................


    //Post Feedback
    @POST("feedback/addfeedback.json")
    Call<FeedbackResponseModel> Feedback(@Header("Content-Type") String header,
                                         @Header("auth-token") String token,
                                         @Body FeedbackRequestModel requestModel);


    //................................................Intiative Section.......................

    //active initiative list
    @GET("initiatives/listinitives.json")
    Call<ActiveFragmentModel> getActiveInitiativeList(@Header("Content-Type") String header,
                                                      @Header("auth-token") String token);

    //archieve initiative list
//    @GET("initiatives/listintiativearchive.json")
//    Call<ArchieveFragmentModel> getArchieveInitiativeList(@Header("Content-Type") String header,@Header("auth-token") String token);


    //get intiative detail
    @GET("initiatives/initiativedetails.json")
    Call<Intiative_Detail_Model> IntiativeDetail(@Header("Content-Type") String header,
                                                 @Header("auth-token") String token,
                                                 @Query("initiative_id") String initiative_id,
                                                 @Query("user_id") String user_id);


    //get intiative detail group list
    @GET("initiatives/initiativegroups.json")
    Call<Intiative_Detail_Group_Model> IntiativeDetailGroupList(@Header("Content-Type") String header,
                                                                @Header("auth-token") String token,
                                                                @Query("initiative_id") String initiative_id);

    //submit_initiative_fundraising
    @POST("initiatives/submit_initiative_fundraising.json")
    Call<SubmitInitiativeFundraisingResponseModel> submitInitiativeFundraising(@Header("Content-Type") String header,
                                                                               @Header("auth-token") String token,
                                                                               @Body SubmitInitiativeFundraisingRequestModel submitInitiativeFundraisingRequestModel
    );
    //Add  Plege
    @POST("initiatives/initiateplege.json")
    Call<Add_Plege_Response_Model> AddPlege(@Header("Content-Type") String header,
                                            @Body Add_Plege_Request_Model requestModel);

    //Add  Plege
    @POST("users/encrypData.json")
    Call<Intiative_Detail_Encrypt_Response_Model> EncriptIntiativeData(@Header("Content-Type") String header,
                                                                       @Header("auth-token") String token,
                                                                       @Body Intiative_Detail_Encrypt_Request_Model requestModel);

    //................................................Intiative Section End.......................

    ////////////////Mission///////////
    //get mission List
    @GET("mission/missionstatement.json")
    Call<GetMissionModel> getMissionList(@Header("Content-Type") String header, @Header("auth-token") String token);


    //...................................Resource section.........................

    //get khoja summit List
    @GET("resources/viewlistsummit.json")
    Call<Khoja_Summit_Model> getKhojaSummitList(@Header("Content-Type") String header, @Header("auth-token") String token);

    //get khoja summit Resources Categorey List
    @GET("resources/listcatagory.json")
    Call<ResourcesCategoreyListModel> getResourcesCatagoreyList(@Header("Content-Type") String header, @Header("auth-token") String token);

    //Add  Resource Post
    @POST("resources/addposts.json")
    Call<AddResourcePostResponseModel> AddResourcePost(@Header("Content-Type") String header,
                                                       @Header("auth-token") String token,
                                                       @Body AddResourcePostRequestModel requestModel);

    //Edit Resource Post
    @POST("resources/editposts.json")
    Call<EditResourcePostResponseModel> EditResourcePost(@Header("Content-Type") String header,
                                                         @Header("auth-token") String token,
                                                         @Body EditResourcePostRequestModel requestModel);


    //get news List
//    @GET("resources/newslist.json")
//    Call<A_News_Model> getNewsList(@Header("Content-Type") String header,@Header("auth-token") String token);

    //get News List updated
    @GET("resources/newslist.json")
    Call<News_Fragments_Model> getUpdatedNewsList(@Header("Content-Type") String header, @Header("auth-token") String token);

    //Add To Archieve
    @POST("resources/ addtoarchivenews.json")
    Call<Add_Active_News_ToArchieve_ResponseModel> AddNewsToArchieve(@Header("Content-Type") String header,
                                                                     @Header("auth-token") String token,
                                                                     @Body Add_Active_News_ToArchieve_RequestModel requestModel);

    //Add To Active
    @POST("resources/backtoactiveafterarchived.json")
    Call<Add_Archieve_News_ToActive_ResponseModel> AddNewsToActive(@Header("Content-Type") String header,
                                                                   @Header("auth-token") String token,
                                                                   @Body Add_Archieve_News_ToActive_RequestModel requestModel);


    //Publish News
    @POST("resources/publishedpost.json")
    Call<Publish_News_Response_Model> PublishNews(@Header("Content-Type") String header,
                                                  @Header("auth-token") String token,
                                                  @Body Publish_News_Request_Model requestModel);

    //Publish News
    @POST("resources/unpublishedpost.json")
    Call<Unpublish_News_Response_Model> UnPublishNews(@Header("Content-Type") String header,
                                                      @Header("auth-token") String token,
                                                      @Body Unpublish_News_Request_Model requestModel);


    //Delete Resource Post
    @POST("resources/deleteposts.json")
    Call<DeleteResourcePostResponseModel> DeleteResourcePost(@Header("Content-Type") String header,
                                                             @Header("auth-token") String token,
                                                             @Body DeleteResourcePostRequestModel requestModel);

    //.....................View Profile...............................

    //get group project list  Done
    @GET("users/completeuserbio.json")
    Call<View_Profile_Model> GetProfileData(@Header("Content-Type") String header,
                                            @Header("auth-token") String token,
                                            @Query("user_id") String user_id);


    //Edit Profile
    @POST("users/editprofile.json")
    Call<Edit_ProfileResponse_Model> EditProfile(@Header("Content-Type") String header,
                                                 @Header("auth-token") String token,
                                                 @Body Edit_ProfileRequest_Model requestModel);

    //.....................View Profile END...............................


    //....................Khoja care.....................

    //get already groups in project
    @GET("Khojacare/khojacarelist.json")
    Call<String> GetKhojaCareList(@Header("Content-Type") String header,@Header("auth-token") String token);


    @GET("forums/eblastuserlist.json")
    Call<String> GetSendEblastList(@Header("Content-Type") String header,@Header("auth-token") String token);

    //....................Khoja care end.....................


    //.....................New Events Section..........................

    // Check class EventsApi

    //.....................New Events Section end..........................




}
