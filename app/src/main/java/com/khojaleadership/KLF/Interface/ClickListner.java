package com.khojaleadership.KLF.Interface;

import android.view.View;

public interface ClickListner {
    void onGroupClick(int GroupPosition);
    void onRowClick(int GroupPosition,int ChildPosition, View v);
}
