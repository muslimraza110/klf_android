package com.khojaleadership.KLF.Activities.DashBoard_New

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Activities.event_new.eventSections.EventSectionsActivity
import com.khojaleadership.KLF.Activities.event_new.events.EventsActivity
import com.khojaleadership.KLF.Activities.event_new.hotelRegistration.HotelRegistrationActivity
import com.khojaleadership.KLF.Adapter.DashBoard_New.EventRecyclerViewAdapter
import com.khojaleadership.KLF.Adapter.DashBoard_New.HomeItemsRecyclerViewAdapter
import com.khojaleadership.KLF.databinding.ActivityMainNewBinding

class NewMainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainNewBinding
    private val viewModel: MainActivityViewModel by viewModels()

    private lateinit var eventsAdapter: EventRecyclerViewAdapter
    private lateinit var navigationItemsAdapter: HomeItemsRecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {

        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityMainNewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setUpObservers()
        viewModel.fetchData()

    }

    private fun setUpObservers() {
        viewModel.eventsData.observe(this, Observer { list ->
            eventsAdapter.submitList(list)
        })

        viewModel.navigationData.observe(this, Observer { list ->
            if (list.size % 2 != 0) {
                val navigationHomeUiModel: NavigationHomeUiModel = list[list.size - 1]
                val templist = list.toMutableList()
                templist.removeAt(templist.size - 1)
                navigationItemsAdapter.submitList(templist)
                binding.oddItem.visibility = View.VISIBLE
                binding.oddItemName.text = navigationHomeUiModel.name
            } else {
                navigationItemsAdapter.submitList(list)
                binding.oddItem.visibility = View.GONE
            }
        })
    }

    private fun setUpViews() {

        eventsAdapter = EventRecyclerViewAdapter(
                onRegsiterClick = {eventId->
                    startActivity(Intent(this, HotelRegistrationActivity::class.java))
                },
                onEventClick = {eventId ->
                    startActivity(Intent(this, EventSectionsActivity::class.java))

                },
                onDonateClick = {},
                onSupportClick = {}
        )
        eventsAdapter.setHasStableIds(true)
        binding.rvEvent.apply {
            adapter = eventsAdapter
            layoutManager = LinearLayoutManager(this@NewMainActivity)
        }


        navigationItemsAdapter = HomeItemsRecyclerViewAdapter()
        navigationItemsAdapter.setHasStableIds(true)

        binding.rvList.apply {
            adapter = navigationItemsAdapter
            layoutManager = GridLayoutManager(this@NewMainActivity, 2)
            addItemDecoration(GridSpacingItemDecoration(this@NewMainActivity, 2, 16, true))
        }

        binding.imgMenu.setOnClickListener {

        }

        binding.imgRefresh.setOnClickListener {
            startActivity(Intent(this, EventsActivity::class.java))
        }



    }
}
