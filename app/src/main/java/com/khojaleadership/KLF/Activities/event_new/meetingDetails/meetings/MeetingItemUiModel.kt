package com.khojaleadership.KLF.Activities.event_new.meetingDetails.meetings

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Model.event_new.MeetingRequestStatusTypes
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemMeetingHeaderBinding
import com.khojaleadership.KLF.databinding.ItemMeetingRow2Binding
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractExpandableHeaderItem
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.ExpandableViewHolder
import java.util.*

class MeetingItemUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        var meetingChildId: String,
        var personDetail: PersonDetailUiModel,
        var isLast: Boolean,
        val requestedBy: String,
        val requestedTo: String,
        var meetingStatus: String,
        val iAmHost: Boolean,
        val myFacultyId: String,
        val header: MeetingHeaderItem,
        val context: Context,
        val onAcceptClick: (MeetingItemUiModel) -> Unit,
        val onRejectClick: (MeetingItemUiModel) -> Unit,
        val onCancelClick: (MeetingItemUiModel) -> Unit
) : AbstractSectionableItem<MeetingItemUiModel.ViewHolder, MeetingHeaderItem>(header) {

    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_meeting_row_2
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                binding = ItemMeetingRow2Binding.bind(view),
                context = context
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }


    class ViewHolder(
            val binding: ItemMeetingRow2Binding,
            var context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MeetingItemUiModel) {
            item.apply {
                binding.tvName.text = personDetail.name
                binding.tvDesignation.text = personDetail.designation

                when {
                    requestedBy == requestedTo -> {
                        // Host row
                        binding.tvMeetingStatus.text = "Meeting Host"
                        binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appCyan))
                        binding.imgMeetingStatus.setImageResource(R.drawable.user_2)
                        binding.layoutAcceptReject.visibility = View.GONE
                        binding.buttonCancelMeeting.visibility = View.GONE
                    }
                    iAmHost -> {
                        // Other than host row, but I am host, host can remove attendee
                        setMeetingStatusHeader(binding, meetingStatus, context)
//                        binding.tvName.text = "Host Can Cancel $name"
                        binding.layoutAcceptReject.visibility = View.GONE
                        binding.buttonCancelMeeting.visibility = View.GONE
                        binding.buttonCancelMeeting.text = "Remove Attendee"
                        if (meetingStatus != MeetingRequestStatusTypes.MEETING_CANCELED.value &&
                                meetingStatus != MeetingRequestStatusTypes.MEETING_REJECTED.value) {
                            binding.buttonCancelMeeting.visibility = View.VISIBLE
                        }
                    }
                    requestedTo == myFacultyId -> {
                        // its me
                        setMeetingStatusHeader(binding, meetingStatus, context)
//                        binding.tvName.text = "Me, I am not host $name"
                        binding.buttonCancelMeeting.visibility = View.GONE
                        binding.layoutAcceptReject.visibility = View.GONE
                        // cancel button
                        binding.buttonCancelMeeting.text = "Cancel"
                        if(meetingStatus == MeetingRequestStatusTypes.MEETING_REQUESTED.value){
                            binding.layoutAcceptReject.visibility = View.VISIBLE
                        }else if(meetingStatus == MeetingRequestStatusTypes.MEETING_ACCEPTED.value){
                            binding.buttonCancelMeeting.visibility = View.VISIBLE
                        }
                    }
                    else -> {
                        // its not me
                        setMeetingStatusHeader(binding, meetingStatus, context)
//                        binding.tvName.text = "Not me, neither host $name"
                        binding.buttonCancelMeeting.visibility = View.GONE
                        binding.layoutAcceptReject.visibility = View.GONE
                    }
                }


                if (isLast) {
                    binding.root.background = ContextCompat.getDrawable(context, R.drawable.blue_bg_round_bottom)
                } else {
                    binding.root.background = ContextCompat.getDrawable(context, R.drawable.blue_bg)
                }

                binding.buttonCancelMeeting.setOnClickListener {
                    onCancelClick.invoke(this)
                }
                binding.layoutAccept.setOnClickListener {
                    onAcceptClick.invoke(this)
                }
                binding.layoutReject.setOnClickListener {
                    onRejectClick.invoke(this)
                }

            }
        }

        private fun setMeetingStatusHeader(binding: ItemMeetingRow2Binding, meetingStatus: String, context: Context) {
            if (meetingStatus == MeetingRequestStatusTypes.MEETING_REQUESTED.value) {
                binding.tvMeetingStatus.text = "Awaiting Response"
                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appOrange))
                binding.imgMeetingStatus.setImageResource(R.drawable.hourglass)
            }
            if (meetingStatus == MeetingRequestStatusTypes.MEETING_ACCEPTED.value) {
                binding.tvMeetingStatus.text = "Meeting Accepted"
                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appDullGreen))
                binding.imgMeetingStatus.setImageResource(R.drawable.calendar)
            }
            if (meetingStatus == MeetingRequestStatusTypes.MEETING_REJECTED.value) {
//                binding.tvMeetingStatus.text = "Remove Attendee"
                binding.tvMeetingStatus.text = "Meeting Rejected"
                //meeting rejected
//                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appLightGrey2))
                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appRed))
                binding.imgMeetingStatus.setImageResource(R.drawable.check_cross_white)
            }
            if (meetingStatus == MeetingRequestStatusTypes.MEETING_CANCELED.value) {
                binding.tvMeetingStatus.text = "Attendee Removed"
                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appRed))
                binding.imgMeetingStatus.setImageResource(R.drawable.check_cross_white)
            }

//            if (meetingStatus == MeetingRequestStatusTypes.MEETING_SELF_CANCELED.value) {
//                binding.tvMeetingStatus.text = "You have Canceled your Metting"
//                binding.layoutMeeting.setBackgroundColor(ContextCompat.getColor(context, R.color.appRed))
//                binding.imgMeetingStatus.setImageResource(R.drawable.check_cross_white)
//            }
        }

    }

}

class MeetingHeaderItem(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val meetingId: String,
        var date: String,
        var startEndTime: String,
        var startTime: String,
        var endTime: String,
        var meetingNotes: String,
        var iAmHost: Boolean,
        var onMeetingNotesClick: (MeetingHeaderItem) -> Unit,
        var onDeleteClick: (MeetingHeaderItem) -> Unit,
        var onEditClick: (MeetingHeaderItem) -> Unit,
        var isOpened: Boolean,
        var context: Context
) : AbstractExpandableHeaderItem<MeetingHeaderItem.ViewHolder, MeetingItemUiModel>() {


    override fun isExpanded(): Boolean {
        return isOpened
    }


    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_meeting_header
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                ItemMeetingHeaderBinding.bind(view), adapter
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }

    class ViewHolder(
            val binding: ItemMeetingHeaderBinding,
            var adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ) : ExpandableViewHolder(binding.root, adapter, true) {

        fun bind(item: MeetingHeaderItem) {

            if(item.iAmHost){
                binding.imageEdit.visibility = View.VISIBLE
                binding.imageDelete.visibility = View.VISIBLE
            }else{
                binding.imageEdit.visibility = View.GONE
                binding.imageDelete.visibility = View.GONE
            }

            binding.tvDateTime.text = "${item.date}," + " ${item.startEndTime}"

            binding.imageList.setOnClickListener {
                item.onMeetingNotesClick.invoke(item)
            }

            binding.imageDelete.setOnClickListener {
                item.onDeleteClick.invoke(item)
            }

            binding.imageEdit.setOnClickListener {
                item.onEditClick.invoke(item)
            }

            binding.imageDropdown.setOnClickListener {
                toggleExpansion()
                item.isOpened = !item.isOpened
                expandCollapse(item.isOpened, binding, item.context)
            }
            expandCollapse(item.isOpened, binding, item.context)
        }

        private fun expandCollapse(isOpened: Boolean, binding: ItemMeetingHeaderBinding, context: Context) {
            if (isOpened) {
                binding.imageDropdown.setImageResource(R.drawable.action_drop_up)
                binding.view1.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round_top)
            } else {
                binding.imageDropdown.setImageResource(R.drawable.action_drop_down)
                binding.view1.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round)
            }
        }
    }
}