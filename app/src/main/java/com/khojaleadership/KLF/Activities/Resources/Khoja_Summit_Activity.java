package com.khojaleadership.KLF.Activities.Resources;

import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.Resource.Khoja_Summit_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Data_Model;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Khoja_Summit_Activity extends AppCompatActivity implements RecyclerViewClickListner {

    RecyclerView recyclerView;
    Khoja_Summit_Adapter adapter;
    //ArrayList<Khoja_Summit_Data_Model> list;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView header_title;
    LinearLayout c_add_btn;

    ArrayList<Khoja_Summit_Data_Model> khoja_summit_list = new ArrayList<>();
    Khoja_Summit_Data_Model khoja_summit_list_data=new Khoja_Summit_Data_Model();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khoja__summit_);

        header_title=(TextView)findViewById(R.id.c_header_title);
        header_title.setText("Khoja Summits");

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Khoja_Summit_Activity.this);

                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        intialize_view();
        getKhojaSummitFunction();

        //intialize_view();


        c_add_btn=findViewById(R.id.c_add_btn);
        if (Common.is_Admin_flag==false){
            c_add_btn.setVisibility(View.GONE);
        }

        c_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(Khoja_Summit_Activity.this, Add_Khoja_Summit_Post.class);
                startActivity(add);
            }
        });
    }

    public void intialize_view() {

        recyclerView = findViewById(R.id.khoja_summit_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);


        adapter = new Khoja_Summit_Adapter(this, khoja_summit_list, this);
        recyclerView.setAdapter(adapter);
    }

    private void getKhojaSummitFunction() {

        Call<Khoja_Summit_Model> responseCall = retrofitInterface.getKhojaSummitList("application/json",Common.auth_token);
        responseCall.enqueue(new Callback<Khoja_Summit_Model>() {
            @Override
            public void onResponse(Call<Khoja_Summit_Model> call, Response<Khoja_Summit_Model> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {

                    loading_dialog.dismiss();
                    String status, message;

                    Khoja_Summit_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<Khoja_Summit_Data_Model> list = new ArrayList<>();
                    list = responseModel.getData();

                    if (list != null) {
                        //Common.khoja_summit_list = list;
                        khoja_summit_list=list;
                        adapter.updateList(khoja_summit_list);
                        // intialize_view();
                    }

                } else {
                }


            }

            @Override
            public void onFailure(Call<Khoja_Summit_Model> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }
    @Override
    public void onRowClick(int position) {
        khoja_summit_list_data=khoja_summit_list.get(position);
        //Common.is_news_falg=false;
        //Intent detail = new Intent(Khoja_Summit_Activity.this, A_Khoja_Summit_Detail_Activity.class);
        Intent detail = new Intent(Khoja_Summit_Activity.this, News_Detail_Activity.class);
        detail.putExtra("KhojaData",khoja_summit_list_data);
        detail.putExtra("isNewsFlag","0");
        startActivity(detail);
    }

    @Override
    public void onViewClcik(int position, View v) {


    }

//    public void Confirm_Dialog(String title, String message, String btn) {
//
//
//        TextView title_text, message_text, btn_text;
//        RelativeLayout close_btn;
//
//        // custom dialog
//        final Dialog dialog = new Dialog(Khoja_Summit_Activity.this);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.aa_confirm_dialog);
//        dialog.setCanceledOnTouchOutside(false);
//        // dialog.getWindow().setLayout(275, 350);
//
//
//        title_text = (TextView) dialog.findViewById(R.id.custom_title);
//        message_text = (TextView) dialog.findViewById(R.id.custom_message);
//        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
//        close_btn=(RelativeLayout)dialog.findViewById(R.id.confirm_custom_close_btn);
//
//        title_text.setText(title);
//        message_text.setText(message);
//        btn_text.setText(btn);
//
//
//
//        btn_text.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                loading_dialog.show();
//                DeleteProjectFunction();
//
//            }
//        });
//
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//    }
//    private void DeleteProjectFunction() {
//
//        DeleteResourcePostRequestModel request=new DeleteResourcePostRequestModel();
//        request.setPost_id(Common.khoja_summit_list_data.getPosts__id());
//
//
//        Call<DeleteResourcePostResponseModel> responseCall = retrofitInterface.DeleteResourcePost("application/json",Common.auth_token,request);
//
//        responseCall.enqueue(new Callback<DeleteResourcePostResponseModel>() {
//            @Override
//            public void onResponse(Call<DeleteResourcePostResponseModel> call, Response<DeleteResourcePostResponseModel> response) {
//
//                if (response.isSuccessful()) {
//                    //loading_dialog.dismiss();
//
//                    String status, message;
//
//                    DeleteResourcePostResponseModel responseModel = response.body();
//                    status = responseModel.getStatus();
//                    message = responseModel.getMessage();
//
//                    if (status.equals("error")){
//                        //data is null
//                    }else if (status.equals("success")){
//                        Toast.makeText(Khoja_Summit_Activity.this,"Khoja summit deleted Successfully.",Toast.LENGTH_SHORT).show();
//
//                        Intent summit=new Intent(Khoja_Summit_Activity.this,Khoja_Summit_Activity.class);
//                        startActivity(summit);
//                        finish();
//                    }
//
//
//                } else {
//                    loading_dialog.dismiss();
//                 }
//
//            }
//
//            @Override
//            public void onFailure(Call<DeleteResourcePostResponseModel> call, Throwable t) {
//                loading_dialog.dismiss();
//             }
//        });
//
//
//    }



    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
