package com.khojaleadership.KLF.Activities.event_new.planner.myItinerary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel

class MyItineraryViewModel : ViewModel() {

    private val _daysData: MutableLiveData<List<EventDaysUiModel>> = MutableLiveData()
    val daysData: LiveData<List<EventDaysUiModel>> = _daysData

    private val _myItineraryItemDetails: MutableLiveData<List<MyItineraryItemUiModel>> = MutableLiveData()
    val myItineraryItemDetails: LiveData<List<MyItineraryItemUiModel>> = _myItineraryItemDetails

    private val _checkAvailabilityDetails: MutableLiveData<List<CheckAvailabilityUiModel>> = MutableLiveData()
    val checkAvailabilityDetails: LiveData<List<CheckAvailabilityUiModel>> = _checkAvailabilityDetails

    private val _checkAvailabilityToggle: MutableLiveData<Boolean> = MutableLiveData(false)
    val checkAvailabilityToggle: LiveData<Boolean> = _checkAvailabilityToggle



    fun toggleCheckAvailability() {
        _checkAvailabilityToggle.value = _checkAvailabilityToggle.value?.not()
    }


}
