package com.khojaleadership.KLF.Activities.event_new.events

import com.khojaleadership.KLF.Model.event_new.SummitEventDayAvailability
import java.util.*

data class EventCardUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val eventId: String,
        val name: String,
        val date: String,
        val price: String,
        val priceRangeStart: String,
        val priceRangeEnd: String,
        val address: String,
        val city: String,

        val registration_link: String,

        val meetingSlots: List<SummitEventDayAvailability>?,
        val eventImage: String,
        val isUserRegistered: Boolean,
        val facultyId: String?,
        val startDate: String,
        val endDate: String
)