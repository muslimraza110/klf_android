package com.khojaleadership.KLF.Activities.Forum_Section;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.Forum.SendEblast_Screen3_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class SendEblastScreen3 extends AppCompatActivity implements RecyclerViewClickListner {

    SendEblast_Screen3_Adapter adapter;
    RecyclerView recyclerView;


    TextView header_title;

    ArrayList<Send_Eblast_Child_Model> send_eblast_child_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_eblast_screen3);


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, SendEblastScreen3.this);
                finish();
            }
        });

        //getting data
        send_eblast_child_list=getIntent().getParcelableArrayListExtra("send_eblast_child_list");

        try {
            header_title = findViewById(R.id.c_header_title);
            header_title.setText("Recipients Detail");


            adapter = new SendEblast_Screen3_Adapter(send_eblast_child_list, this);
            recyclerView = findViewById(R.id.recyclerview);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    @Override
    public void onRowClick(int position) {
        //Common.contact_name = Common.send_eblast_child_list.get(position).getChild_name();
        if (Common.send_eblast_email_list.size() > 0) {
            Common.send_eblast_email_list_copy = Common.send_eblast_email_list;

            for (int i = 0; i < Common.send_eblast_email_list.size(); i++) {
            }
        }

        Common.send_eblast_child_data_list = send_eblast_child_list.get(position).getChild_data();
        Common.send_eblast_child_data_list_copy = send_eblast_child_list.get(position).getChild_data();


        for (int i = 0; i < Common.send_eblast_child_data_list.size(); i++) {

            String user_id = Common.send_eblast_child_data_list.get(i).getUserID();
            if (Common.send_eblast_email_list.size() > 0) {

                for (int j = 0; j < Common.send_eblast_email_list.size(); j++) {


                    if (user_id.equals(Common.send_eblast_email_list.get(j).getId())) {

                        String UserID, FirstName, LastName, Email, ProfileURL;
                        String is_checked;

                        UserID = Common.send_eblast_child_data_list.get(i).getUserID();
                        FirstName = Common.send_eblast_child_data_list.get(i).getFirstName();
                        LastName = Common.send_eblast_child_data_list.get(i).getLastName();
                        Email = Common.send_eblast_child_data_list.get(i).getEmail();
                        ProfileURL = Common.send_eblast_child_data_list.get(i).getProfileURL();
                        is_checked = "1";

                        Send_Eblast_Child_Data_Model item = new Send_Eblast_Child_Data_Model("", UserID, FirstName, LastName, Email, ProfileURL, is_checked);


                        Common.send_eblast_child_data_list.set(i, item);

                    } else {


                    }


                }
            } else {

                //email list empty
                String UserID, FirstName, LastName, Email, ProfileURL;
                String is_checked;

                UserID = Common.send_eblast_child_data_list.get(i).getUserID();
                FirstName = Common.send_eblast_child_data_list.get(i).getFirstName();
                LastName = Common.send_eblast_child_data_list.get(i).getLastName();
                Email = Common.send_eblast_child_data_list.get(i).getEmail();
                ProfileURL = Common.send_eblast_child_data_list.get(i).getProfileURL();
                is_checked = "0";

                Send_Eblast_Child_Data_Model item = new Send_Eblast_Child_Data_Model("", UserID, FirstName, LastName, Email, ProfileURL, is_checked);

                Common.send_eblast_child_data_list.set(i, item);
            }

        }


        Intent screen4 = new Intent(SendEblastScreen3.this, SendEblastScreen4.class);
        screen4.putExtra("ContactName",send_eblast_child_list.get(position).getChild_name());
        startActivity(screen4);

    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
