package com.khojaleadership.KLF.Activities.event_new.events

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.khojaleadership.KLF.Activities.event_new.eventRegistration.EventRegistrationActivity
import com.khojaleadership.KLF.Activities.event_new.eventSections.EventSectionsActivity
import com.khojaleadership.KLF.Adapter.event_new.previousEvents.PreviousEventsAdapter
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Common.summitEventsRegistrationLink
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.databinding.ActivityPreviousEventsBinding
//import com.khojaleadership.KLF.databinding.ActivityPreviousEventsBinding
import timber.log.Timber


class EventsActivity : AppCompatActivity() {


    lateinit var binding: ActivityPreviousEventsBinding

    private val viewModel: EventsViewModel by viewModels()

    lateinit var eventsAdapter: PreviousEventsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityPreviousEventsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.e("----------EventsActivity")
        Timber.e("Logout Common.login_data.getData().getAuth_token())" + Common.login_data.data.auth_token)
        Timber.e(
            "Logout Common.login_data.getData().getId()" + Common.login_data.data.id)

        Timber.e(Common.auth_token.toString())
        Timber.e(
            Common.login_data?.data?.id
            ?: "")
        Timber.e( viewModel.authStore.event?.eventId ?: "")
        Timber.e( "eventId-------")
        if (intent.hasExtra(KEY_IS_UPCOMING_EVENT)) {

            viewModel.isUpcomingEvent = intent.getBooleanExtra(KEY_IS_UPCOMING_EVENT, true)
        }

        setUpViews()
        setUpObservers()
        viewModel.fetchData()
    }

    private fun setUpObservers() {

        viewModel.eventsData.observe(this, Observer {
            it?.let {
                if (it.isNotEmpty()) {
                    binding.viewpager.visibility = View.VISIBLE
                    binding.tvNoEventsFound.visibility = View.GONE
                } else {
                    binding.viewpager.visibility = View.GONE
                    binding.tvNoEventsFound.visibility = View.VISIBLE
                }
                eventsAdapter.updateList(it.toMutableList())
                binding.pageIndicatorView.count = it.size

            }
        })

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.viewpager.visibility = View.GONE
                    binding.tvUpcomingEvent.visibility = View.GONE
                    binding.tvNoEventsFound.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.tvUpcomingEvent.visibility = View.VISIBLE
                    binding.tvNoEventsFound.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.tvUpcomingEvent.visibility = View.VISIBLE
                    binding.tvNoEventsFound.text = it.message
                    binding.tvNoEventsFound.visibility = View.INVISIBLE
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })


        viewModel.setSummitEventsRegistration.observe(this, Observer {
            when (it)
            {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                }
            }
        })

    }

    private fun setUpViews() {

        if (viewModel.isUpcomingEvent) {
            binding.tvPreviousEvents.text = "Upcoming Event"
            binding.tvUpcomingEvent.text = "<< Previous Event"
            binding.frameLayoutRegisterBtn.visibility = View.VISIBLE
        } else {
            binding.tvPreviousEvents.text = "Previous Events"
            binding.tvUpcomingEvent.text = "<< Upcoming Event"
            binding.frameLayoutRegisterBtn.visibility = View.GONE
        }

        binding.pageIndicatorView.count = 0

        eventsAdapter = PreviousEventsAdapter(
                context = this,
                isUpcomingEvent = viewModel.isUpcomingEvent,
                onViewDetailsClick = {
                    viewModel.authStore.event = it
                    startActivity(Intent(this, EventSectionsActivity::class.java))
                }
        )

        binding.viewpager.adapter = eventsAdapter

        binding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { /*empty*/
            }

            override fun onPageSelected(position: Int) {
                binding.pageIndicatorView.selection = position
            }

            override fun onPageScrollStateChanged(state: Int) { /*empty*/
            }
        })


        binding.imageBack.setOnClickListener {
            finish()
        }

        binding.tvUpcomingEvent.setOnClickListener {
            if (viewModel.isUpcomingEvent) {
                val intent = Intent(this, EventsActivity::class.java)
                intent.putExtra(KEY_IS_UPCOMING_EVENT, false)
                startActivity(intent)
            } else {
                finish()
            }
        }

        binding.layoutRegistration.setOnClickListener {




            viewModel.setSummitEventsRegistration {  mySuccessResponse ->

                print(mySuccessResponse)
                Timber.e(mySuccessResponse)



                viewModel.eventsData.value?.getOrNull(binding.viewpager.currentItem)?.let {


                    val _registrationLink = it.registration_link

                    if (_registrationLink != "") {

                        //redirect to new browser..

                        val openURL = Intent(Intent.ACTION_VIEW)
                        openURL.data = Uri.parse( _registrationLink )

                        startActivity(openURL)

                    } else {

                        Toast.makeText(this, Common.noRegistrationLinkFound,Toast.LENGTH_SHORT).show()

                    }
                }

            }

        }

    }


    companion object {
        const val KEY_IS_UPCOMING_EVENT = "keyUpcomingEvent"
    }
}
