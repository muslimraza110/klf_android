package com.khojaleadership.KLF.Activities.Project;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Adapter.Project.IsProjectMemberListAdapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForIsProjectMember;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.AddMemberToProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddMemberToProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Project_Member_Data_List;
import com.khojaleadership.KLF.Model.Project_Models.Project_Member_List;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectResponseModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Member_In_Project extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private IsProjectMemberListAdapter adapter;
    private List<Project_Member_Data_List> sourceDataList;
    List<Project_Member_Data_List> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;

    private PinyinComparatorForIsProjectMember pinyinComparator;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;

    TextView contact_list_title;

    String isProjectDetail = "-1";
    String ProjectId="";

    ArrayList<Project_Member_Data_List> projects_member_data_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whitelist_contacts);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Add Member");


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isProjectDetail = bundle.getString("isProjectDetail");
            ProjectId=bundle.getString("ProjectId");
        }

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Member_In_Project.this);

                Intent detail = new Intent(Add_Member_In_Project.this, View_Projects_Detail_Updated.class);
                detail.putExtra("ProjectId",ProjectId);
                detail.putExtra("isProjectDetail","0");
                detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(detail);
                finish();
            }
        });

        findViewById(R.id.whitelist_contact_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detail = new Intent(Add_Member_In_Project.this, View_Projects_Detail_Updated.class);
                detail.putExtra("ProjectId",ProjectId);
                detail.putExtra("isProjectDetail","0");
                detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(detail);
                finish();
            }
        });

        listner = this;

        //initViews();

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        GetProjectMemberList(ProjectId, true);

    }


    private void initViews(Boolean is_first_time) {

//        loading_dialog.dismiss();

        pinyinComparator = new PinyinComparatorForIsProjectMember();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        sourceDataList = filledData(projects_member_data_list);
        //......................................................................................................................
        Collections.sort(sourceDataList, pinyinComparator);

        manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        if (is_first_time == true) {
            adapter = new IsProjectMemberListAdapter(getApplicationContext(), sourceDataList, listner);
        } else if (is_first_time == false) {
            adapter.updateList(sourceDataList);
        }
        //......................................................................................................................
        mRecyclerView.setAdapter(adapter);


        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);

        mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private List<Project_Member_Data_List> filledData(List<Project_Member_Data_List> data) {
        List<Project_Member_Data_List> mSortList = new ArrayList<>();


        for (int i = 0; i < data.size(); i++) {
            Project_Member_Data_List sortModel = new Project_Member_Data_List();

            sortModel.setRole(data.get(i).getRole());
            sortModel.setTitle(data.get(i).getTitle());
            sortModel.setFirst_name(data.get(i).getFirst_name());
            sortModel.setMiddle_name(data.get(i).getMiddle_name());
            sortModel.setLast_name(data.get(i).getLast_name());
            sortModel.setGender(data.get(i).getGender());
            sortModel.setChair_person(data.get(i).getChair_person());
            sortModel.setId(data.get(i).getId());
            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setProject_id(data.get(i).getProject_id());
            sortModel.setProfile_image(data.get(i).getProfile_image());
            sortModel.setIsmember(data.get(i).getIsmember());

            String pinyin_english = data.get(i).getFirst_name();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {
                sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();

                if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }


        return mSortList;
    }

    private void filterData(String filterStr) {

        filter_flag = true;

        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(projects_member_data_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (Project_Member_Data_List sortModel : sourceDataList) {
                String name = sortModel.getFirst_name();

                if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().startsWith(filterStr)
                        || name.toUpperCase().startsWith(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
        Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }

    private void GetProjectMemberList(String project_id, final Boolean is_first_time) {

        Call<Project_Member_List> call = retrofitInterface.GetProjectMemberList("application/json", Common.auth_token, project_id);


        call.enqueue(new Callback<Project_Member_List>() {
            @Override
            public void onResponse(Call<Project_Member_List> call, Response<Project_Member_List> response) {
                 if (response.isSuccessful()) {
                    String status, message;


                    Project_Member_List responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<Project_Member_Data_List> list = new ArrayList<>();
                    list = responseModel.getData();


                    if (list != null) {
                        loading_dialog.dismiss();
                        projects_member_data_list = list;
                        initViews(is_first_time);
                    }
                }
            }

            @Override
            public void onFailure(Call<Project_Member_List> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRowClick(int position) {

    }

    int flag;
    String member_id;
    @Override
    public void onViewClcik(int position, View v) {

        try {
            switch (v.getId()) {
                case R.id.whitelist_contact_checkbox:
                    if (filter_flag == true) {
                        if (filterDataList != null) {
                            //Common.projects_member_data_detail = filterDataList.get(position);
                            member_id=filterDataList.get(position).getId();
                            flag=filterDataList.get(position).getIsmember();
                        }
                    } else {
                        if (sourceDataList != null) {
                            //Common.projects_member_data_detail = sourceDataList.get(position);
                            member_id=sourceDataList.get(position).getId();
                            flag=sourceDataList.get(position).getIsmember();
                        }
                    }


                    if (flag == 0) {

                        loading_dialog.show();

                        AddMember(member_id, ProjectId);
                    } else if (flag == 1) {

                        loading_dialog.show();
                        RemoveMember(member_id, ProjectId);
                    }


                    break;
                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void AddMember(String user_id, String project_id) {


        AddMemberToProjectRequestModel requestModel = new AddMemberToProjectRequestModel();

        requestModel.setUser_id(user_id);
        requestModel.setProject_id(project_id);  //default


        Call<AddMemberToProjectResponseModel> responseCall = retrofitInterface.AddMemberToProject("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<AddMemberToProjectResponseModel>() {
            @Override
            public void onResponse(Call<AddMemberToProjectResponseModel> call, Response<AddMemberToProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    // loading_dialog.dismiss();

                    AddMemberToProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    GetProjectMemberList(ProjectId, false);

                    Toast.makeText(Add_Member_In_Project.this, "Member added to project successfully", Toast.LENGTH_SHORT).show();

                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Add_Member_In_Project.this, "Member added to project failed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddMemberToProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Add_Member_In_Project.this, "Member added to project failed", Toast.LENGTH_LONG).show();
             }
        });

    }

    private void RemoveMember(String user_id, String project_id) {


        RemoveMemberFromProjectRequestModel requestModel = new RemoveMemberFromProjectRequestModel();

        requestModel.setUser_id(user_id);
        requestModel.setProject_id(project_id);  //default


        Call<RemoveMemberFromProjectResponseModel> responseCall = retrofitInterface.RemoveMemberFromProject("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<RemoveMemberFromProjectResponseModel>() {
            @Override
            public void onResponse(Call<RemoveMemberFromProjectResponseModel> call, Response<RemoveMemberFromProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    RemoveMemberFromProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    GetProjectMemberList(ProjectId, false);

                    Toast.makeText(Add_Member_In_Project.this, "Member remove from project successfully", Toast.LENGTH_LONG).show();

                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Add_Member_In_Project.this, "Member remove from project failed", Toast.LENGTH_LONG).show();

                 }

            }

            @Override
            public void onFailure(Call<RemoveMemberFromProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Add_Member_In_Project.this, "Member remove from project failed", Toast.LENGTH_LONG).show();
             }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent detail = new Intent(Add_Member_In_Project.this, View_Projects_Detail_Updated.class);
        detail.putExtra("ProjectId",ProjectId);
        detail.putExtra("isProjectDetail","0");
        detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(detail);
        finish();
    }
}
