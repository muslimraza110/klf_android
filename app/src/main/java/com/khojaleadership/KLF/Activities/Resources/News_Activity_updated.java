package com.khojaleadership.KLF.Activities.Resources;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Fragments.News_Active_Fragment;
import com.khojaleadership.KLF.Fragments.News_Archieve_Fragment;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.R;

public class News_Activity_updated extends AppCompatActivity {
    Button active, archive;
    String current = "active";
    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView header_title;

    LinearLayout c_add_btn;

//    ArrayList<News_Fragments_Data_Model> active_list = new ArrayList<>();
//    ArrayList<News_Fragments_Data_Model> archieve_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_activity_updated);

        header_title = findViewById(R.id.c_header_title);
        header_title.setText("News");

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, News_Activity_updated.this);

                finish();
            }
        });

        //add news btn
        c_add_btn = findViewById(R.id.c_add_btn);
        if (Common.is_Admin_flag == false) {
            c_add_btn.setVisibility(View.GONE);
        }
        c_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(News_Activity_updated.this, Add_News_Post.class);
                startActivity(i);
            }
        });


        active = findViewById(R.id.active);
        archive = findViewById(R.id.archive);


        loading_dialog = Common.LoadingDilaog(this);
        //loading_dialog.show();
        AttachActiveFragment();

        //getNewsListFunction();

        active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //change btn clr
                changeTabColor(active, archive);

                DetachArchiveFragment();
                AttachActiveFragment();
            }
        });

        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //change btn clr
                changeTabColor(archive, active);

                DeattachActiveFragment();
                AttachArchiveFragment();


            }
        });


    }

    public void changeTabColor(Button b1, Button b2) {
        b1.setBackgroundColor(getResources().getColor(R.color.blue));
        b1.setTextColor(getResources().getColor(R.color.white));

        b2.setBackgroundColor(getResources().getColor(R.color.dark_grey));
        b2.setTextColor(getResources().getColor(R.color.blue));
    }


    void AttachActiveFragment() {
        //because aik hi adapter use kia for active and archieve
        //Common.is_active_news = true;


        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        //for attaching fragment add this in fragment   import android.support.v4.app.Fragment;
        News_Active_Fragment fb = (News_Active_Fragment) fm.findFragmentByTag("BGF");

        if (fb == null) {
            fb = new News_Active_Fragment();
            ft.add(R.id.frame, fb, "BGF");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //ft.commit();
            ft.commitAllowingStateLoss();
        }
    }

    void DeattachActiveFragment() {
        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        News_Active_Fragment fb = (News_Active_Fragment) fm.findFragmentByTag("BGF");

        if (fb != null) {
            ft.remove(fb);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            //ft.commit();
            ft.commitAllowingStateLoss();

        }
    }

    void AttachArchiveFragment() {
        //Common.is_active_news = false;
        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        //for attaching fragment add this in fragment   import android.support.v4.app.Fragment;
        News_Archieve_Fragment fb = (News_Archieve_Fragment) fm.findFragmentByTag("CF");

        if (fb == null) {
            fb = new News_Archieve_Fragment();
            ft.add(R.id.frame, fb, "CF");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //ft.commit();
            ft.commitAllowingStateLoss();
        }
    }

    void DetachArchiveFragment() {
        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        News_Archieve_Fragment fb = (News_Archieve_Fragment) fm.findFragmentByTag("CF");

        if (fb != null) {
            ft.remove(fb);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            //ft.commit();
            ft.commitAllowingStateLoss();
        }
    }

    private void getNewsListFunction() {
//        if (Common.is_active_news == true) {
//
//            //change btn clr
//            changeTabColor(active, archive);
//            DetachArchiveFragment();
//            AttachActiveFragment();
//        } else {
//
//            //change btn clr
//            changeTabColor(archive, active);
//            DeattachActiveFragment();
//            AttachArchiveFragment();
//        }

//        Call<News_Fragments_Model> responseCall = retrofitInterface.getUpdatedNewsList("application/json", Common.auth_token);
//
//        responseCall.enqueue(new Callback<News_Fragments_Model>() {
//            @Override
//            public void onResponse(Call<News_Fragments_Model> call, Response<News_Fragments_Model> response) {
//
//               // loading_dialog.dismiss();
//                if (response.isSuccessful()) {
//
//                    //loading_dialog.dismiss();
//                    String status, message;
//
//                    News_Fragments_Model responseModel = response.body();
//                    status = responseModel.getStatus();
//                    message = responseModel.getMessage();
//                    ArrayList<News_Fragments_Data_Model> list = new ArrayList<>();
//                    list = responseModel.getData();
//
//                    if (list != null) {
//
//
////                        Common.news_active_fragment_list.clear();
////                        Common.news_archieve_fragment_list.clear();
//
////                        for (int i=0;i<list.size();i++){
////                            if (list.get(i).getPosts__archived().equals("0")){
////                                 if (Common.is_Admin_flag==true) {
////                                     Common.news_active_fragment_list.add(list.get(i));
////                                }else if (Common.is_Admin_flag==false) {
////                                     if (list.get(i).getIs_published().equals("1")) {
////                                         Common.news_active_fragment_list.add(list.get(i));
////                                    }
////                                }
////                            }else {
////                                //
////                                if (Common.is_Admin_flag==true) {
////                                    Common.news_archieve_fragment_list.add(list.get(i));
////                                }else if (Common.is_Admin_flag==false) {
////                                    if (list.get(i).getIs_published().equals("1")) {
////                                        Common.news_archieve_fragment_list.add(list.get(i));
////                                    }
////                                }
////                            }
////
////                        }
//
////                        if (Common.is_active_news == true) {
////
////                            //change btn clr
////                            changeTabColor(active, archive);
////                            DetachArchiveFragment();
////                            AttachActiveFragment();
////                        } else {
////
////                            //change btn clr
////                            changeTabColor(archive, active);
////                            DeattachActiveFragment();
////                            AttachArchiveFragment();
////                        }
//                    }
//
//
//                } else {
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<News_Fragments_Model> call, Throwable t) {
//                loading_dialog.dismiss();
//            }
//        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}

