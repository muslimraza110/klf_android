package com.khojaleadership.KLF.Activities.event_new.speakers

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.userProfile.UserProfileActivity
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.EditTextBackEvent
import com.khojaleadership.KLF.Helper.EditTextImeBackListener
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import com.khojaleadership.KLF.databinding.ActivityEventSpeakersBinding
import eu.davidea.flexibleadapter.FlexibleAdapter
import timber.log.Timber
import java.util.*

class EventSpeakersActivity : AppCompatActivity() {

    lateinit var binding: ActivityEventSpeakersBinding

    private val viewModel: EventSpeakersViewModel by viewModels()

    private val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }


    //    private lateinit var speakersAdapter: SpeakersExpandableListViewAdapter
    private lateinit var speakersAdapter: FlexibleAdapter<SpeakerHeaderItem>


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        Timber.e("onCreate----------EventSpeakersActivity")

        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityEventSpeakersBinding.inflate(layoutInflater)

        setContentView(binding.root)
        authStore.meetingRequestSent = false

        setUpViews()
        setUpObservers()

    }

    override fun onResume() {
        super.onResume()
        Timber.e("onResume----------EventSpeakersActivity")
        viewModel.userClicked = false

        if (authStore.meetingRequestSent) {
            viewModel.getSpeakers("")
            binding.editSearch.setText("")
            authStore.meetingRequestSent = false
        }

    }

    private fun setUpObservers() {
        viewModel.speakers.observe(this, Observer {
            it?.let {

                speakersAdapter.updateDataSet(it)

                if (it.isNotEmpty()) {
                    binding.rvList.scrollToPosition(0)
                    binding.tvNoRecordsFound.visibility = View.GONE
                } else {
                    binding.tvNoRecordsFound.visibility = View.VISIBLE
                }

            }
        })



        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    speakersAdapter.updateDataSet(listOf())
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.rvList.visibility = View.GONE
                    binding.tvNoRecordsFound.visibility = View.GONE

                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.tvNoRecordsFound.text = it.message
                    binding.rvList.visibility = View.VISIBLE

                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvList.visibility = View.GONE
                    binding.tvNoRecordsFound.text = it.message
                    speakersAdapter.updateDataSet(listOf())
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.messageEvent.observe(this, Observer {
            it?.consume()?.let { message ->
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.socialClickEvent.observe(this, Observer {
            val url = it?.consume()
            Common.loadUrlInBrowser(url, this)
        })


//        viewModel.twitterClickEvent.observe(this, Observer {
//            val url = it?.consume()
//            loadUrlInBrowser(url)
//        })


        viewModel.speakerClickEvent.observe(this, Observer {
            it?.consume()?.let { speaker ->
                if (!viewModel.userClicked) {
                    viewModel.userClicked = true
//                    authStore.lastSelectedPersonAvailability = speaker.availableSlotData
                    val intent = Intent(this, UserProfileActivity::class.java)
                    intent.putExtra(UserProfileActivity.KEY_FACULTY_ID, speaker.summitEventsFacultyId)
                    intent.putExtra(UserProfileActivity.KEY_USER_ID, speaker.userId)
                    intent.putExtra(UserProfileActivity.KEY_IS_DELEGATE, speaker.isDelegate)
                    intent.putExtra(UserProfileActivity.KEY_USER_FB, speaker.facebook)
                    intent.putExtra(UserProfileActivity.KEY_USER_TWITTER, speaker.twitter)
                    intent.putExtra(UserProfileActivity.KEY_USER_LINKED_IN, speaker.linkedIn)
                    startActivity(intent)
                }

            }
        })
    }


    private fun setUpViews() {

        speakersAdapter = FlexibleAdapter(listOf())
        speakersAdapter.setDisplayHeadersAtStartUp(true)
        speakersAdapter.setStickyHeaders(true)


        binding.rvList.apply {
            adapter = speakersAdapter
            layoutManager = LinearLayoutManager(this@EventSpeakersActivity)
        }

        binding.imageBack.setOnClickListener {
            finish()
        }


        binding.editSearch.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                Common.hideKeyboard(v, this)
                viewModel.searchTextHasChanged = true
                v?.clearFocus()
            }
            return@setOnKeyListener true
        }

        binding.editSearch.doOnTextChanged { text, start, count, after ->
            viewModel.searchTextHasChanged = true
        }

        binding.editSearch.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && viewModel.searchTextHasChanged) {
                viewModel.getSpeakers(binding.editSearch.text?.toString() ?: "")
                viewModel.searchTextHasChanged = false
            }
        }

        binding.editSearch.setOnEditTextImeBackListener(object : EditTextImeBackListener {
            override fun onImeBack(ctrl: EditTextBackEvent, text: String) {
                Common.hideKeyboard(binding.editSearch, this@EventSpeakersActivity)
                binding.editSearch.clearFocus()
            }
        })

        binding.imageSearch.setOnClickListener {
            Common.hideKeyboard(binding.editSearch, this)
            viewModel.searchTextHasChanged = true
            binding.editSearch.clearFocus()
        }
    }


    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}
