package com.khojaleadership.KLF.Activities.Resources;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Resource.EditResourcePostRequestModel;
import com.khojaleadership.KLF.Model.Resource.EditResourcePostResponseModel;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Data_Model;
import com.khojaleadership.KLF.Model.Resource.ResourcesCategoreyListDataModel;
import com.khojaleadership.KLF.Model.Resource.ResourcesCategoreyListModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Khoja_Summit_Activity extends AppCompatActivity {

    RichEditor mEditor;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    String cat_id = "-1", pinned_post_text = "0";

    TextView name, topic;
    TextView date;
    CheckBox pinned_post;
    LinearLayout save_btn;

    DatePickerDialog.OnDateSetListener mDateSetListener1;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    String time;

    Khoja_Summit_Data_Model KhojaData = new Khoja_Summit_Data_Model();

    ArrayList<String> resources_cat_list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__khoja__summit_);


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Edit Post");
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Edit_Khoja_Summit_Activity.this);

                finish();
            }
        });


        Intent intent = getIntent();
        KhojaData = intent.getParcelableExtra("KhojaData");


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();


        getResourcesCatagoryList();

        findViewById(R.id.edit_summit_save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                loading_dialog.show();
//                EditResourcePost();

                if (name.getText().toString() == null || name.getText().toString().isEmpty()) {
                    name.setError("Name should not be empty.");
                } else if (topic.getText().toString() == null || topic.getText().toString().isEmpty()) {
                    topic.setError("Topic should not be empty.");
                } else if (date.getText().toString() == null || date.getText().toString().isEmpty()) {
                    date.setError("Date should not be empty.");
                } else if (cat_id.equals("-1")) {
                    Common.Toast_Message(Edit_Khoja_Summit_Activity.this, "Please select category");
                } else if (mEditor.getHtml() == null || mEditor.getHtml().equals("") || mEditor.getHtml().equalsIgnoreCase("")) {
                    Common.Toast_Message(Edit_Khoja_Summit_Activity.this, "Description should not be empty.");
                } else {
                    loading_dialog.show();
                    EditResourcePost();
                }

            }
        });

    }

    private void getResourcesCatagoryList() {

        Call<ResourcesCategoreyListModel> responseCall = retrofitInterface.getResourcesCatagoreyList("application/json", Common.auth_token);

        responseCall.enqueue(new Callback<ResourcesCategoreyListModel>() {
            @Override
            public void onResponse(Call<ResourcesCategoreyListModel> call, Response<ResourcesCategoreyListModel> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {

                    loading_dialog.dismiss();
                    String status, message;

                    ResourcesCategoreyListModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<ResourcesCategoreyListDataModel> list = new ArrayList<>();
                    list = responseModel.getData();

                    if (list != null) {
//                        Common.resources_catagory_list = list;

                        resources_cat_list.add("Category");
                        for (int i = 0; i < list.size(); i++) {
                            resources_cat_list.add(list.get(i).getCategories__name());
                        }

                        intialize_view();
                    }


                } else {
                }


            }

            @Override
            public void onFailure(Call<ResourcesCategoreyListModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }

    public void intialize_view() {

//        Khoja_Summit_Data_Model model = Common.khoja_summit_list_data;

        Khoja_Summit_Data_Model model = KhojaData;

        name = (TextView) findViewById(R.id.edit_summit_name);
        topic = (TextView) findViewById(R.id.edit_summit_topic);
        date = findViewById(R.id.edit_summit_date);

        date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                date.setError(null);//removes error
                date.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getTimeFunction();
                Calendar cal1 = Calendar.getInstance();
                int year = cal1.get(Calendar.YEAR);
                int month = cal1.get(Calendar.MONTH);
                int day = cal1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog1 = new DatePickerDialog(
                        Edit_Khoja_Summit_Activity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                // Toast.makeText(Edit_Khoja_Summit_Activity.this, "onDateSet: dd//mm//yyyy:" + month + "/" + day + "/" + year, Toast.LENGTH_SHORT).show();

                String date_text = year + "-" + day + "-" + month + " " + time;
                date.setText(date_text);
            }
        };


        mEditor = (RichEditor) findViewById(R.id.editor);

        name.setText(model.getPosts__name());
        topic.setText(model.getPosts__topic());
        date.setText(model.getPosts__created());
        mEditor.setHtml(model.getPosts__content());

        //catagorey list
        Spinner catagorey_spinner = (Spinner) findViewById(R.id.edit_summit_category_spinner);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, resources_cat_list) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);
        catagorey_spinner.setAdapter(adapter);
        catagorey_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //category = parent.getItemAtPosition(position).toString();

                if (position == 1) {
                    //for news
                    cat_id = "1";
                } else if (position == 2) {
                    //for khoja summit
                    cat_id = "4";
                } else {
                    cat_id = "-1";
                }

                ((TextView) view).setTextColor(getResources().getColor(R.color.pure_grey));
                ((TextView) view).setTextSize(12);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (model.getPosts__category_id().equals("1")) {
            catagorey_spinner.setSelection(1);
        } else if (model.getPosts__category_id().equals("4")) {
            catagorey_spinner.setSelection(2);
        }


        intialize_webview();
    }

    public void getTimeFunction() {

        try {

            final Calendar calendar = Calendar.getInstance();
            final int[] hours = {calendar.get(Calendar.HOUR_OF_DAY)};
            final int[] minuts = {calendar.get(Calendar.MINUTE)};

            int sec = calendar.get(Calendar.SECOND);

            time = hours[0] + ":" + minuts[0] + ":" + sec;
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void intialize_webview() {

        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(22);
        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);
        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        mEditor.setPlaceholder("Description");

        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);


                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag = false;
                            action_right_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            action_left_flag = false;
                            action_right_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag = false;
                            action_left_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });


    }

    private void EditResourcePost() {


        EditResourcePostRequestModel requestModel = new EditResourcePostRequestModel();

        requestModel.setUser_id(Common.login_data.getData().getId());
//        requestModel.setPost_id(Common.khoja_summit_list_data.getPosts__id());

        requestModel.setPost_id(KhojaData.getPosts__id());
        requestModel.setName(name.getText().toString());
        requestModel.setTopic(topic.getText().toString());
//        requestModel.setPublished("2018-05-24 22:41:54");  //date format
        requestModel.setPublished(date.getText().toString());  //date format
        requestModel.setCategory_id(cat_id);
        requestModel.setPost_theme_id("1");  //static for now
        requestModel.setContent(mEditor.getHtml());

        //pinned post not available


        Call<EditResourcePostResponseModel> responseCall = retrofitInterface.EditResourcePost("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<EditResourcePostResponseModel>() {
            @Override
            public void onResponse(Call<EditResourcePostResponseModel> call, Response<EditResourcePostResponseModel> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {

                    loading_dialog.dismiss();
                    String status, message;

                    EditResourcePostResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("success")) {
                        Toast.makeText(Edit_Khoja_Summit_Activity.this, "Khoja Summit Post edited successfully", Toast.LENGTH_SHORT).show();

                        Intent khoja_summit = new Intent(Edit_Khoja_Summit_Activity.this, Khoja_Summit_Activity.class);
                        khoja_summit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(khoja_summit);
                        finish();
                    }

                } else {
                }


            }

            @Override
            public void onFailure(Call<EditResourcePostResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
