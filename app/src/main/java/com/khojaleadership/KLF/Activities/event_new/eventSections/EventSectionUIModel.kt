package com.khojaleadership.KLF.Activities.event_new.eventSections

data class EventSectionUIModel(
        val tag: String,
        val name: String,
        val description: String
//        val backgroundImage: String
)

enum class EventSectionTag(val value: String){
    PLANNER("myplanner"),
    PROGRAMME("programe"),
    DELEGATES("delegates"),
    SPEAKERS("speakers"),
    MEETING_DETAILS("meetingdetails"),
    INTIATIVE_DETAILS("initiative"),}