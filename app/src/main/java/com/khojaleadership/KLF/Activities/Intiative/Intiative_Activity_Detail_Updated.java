package com.khojaleadership.KLF.Activities.Intiative;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Adapter.Intiative.IntiativeFundraising_Adapter;
import com.khojaleadership.KLF.Adapter.Intiative.IntiativeMatcher_Adapter;
import com.khojaleadership.KLF.Adapter.Intiative.IntiativeProject_Adapter;
import com.khojaleadership.KLF.Adapter.Intiative.Intiative_Detail_RecyclerView_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Basic_Info_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Fundraising_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Pledgers_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Projects_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Encrypt_Request_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Encrypt_Response_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Group_Data_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Group_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Model;
import com.khojaleadership.KLF.Model.Intiative_Models.SubmitInitiativeFundraisingRequestModel;
import com.khojaleadership.KLF.Model.Intiative_Models.SubmitInitiativeFundraisingResponseModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Intiative_Activity_Detail_Updated extends AppCompatActivity {
    RecyclerView recyclerView;
    Intiative_Detail_RecyclerView_Adapter adapter;


    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView top_progress_ammount, bottom_progress_ammount;
    LinearLayout bottom_progress_ammount_layout;
    ProgressBar top_progressBar, bottom_progressBar;

    TextView line1_block, line2_block, line3_block;

    TextView block_value;

    Intiative_Detail_Data_Fundraising_Model fund_model;
    Intiative_Detail_Data_Projects_Model proj_model;
    Intiative_Detail_Data_Basic_Info_Model basic_model;

    Spinner group_spinner;
    String group_id = "-1";

    Spinner block_spinner;
    String block_id = "-1";
    String block_byType;
    SharedPreferences sp;
    SharedPreferences.Editor Ed;

    Spinner amount_spinner;
    String amount_spinner_id = "0", pledge_blocks = null;
    LinearLayout et_intiative_amount_layout, intiative_amount_spinner_layout;
    int fundraisings_max_blocks_value_position = 0;
    EditText et_intiative_amount;
    CheckBox anonymous_checkbox, hideAmountCheckbox;
    LinearLayout hideAmount_checkbox_layout, anonymous_checkbox_layout;
    public String anonymous_value, hideAmount_value;

    public String fundraisings_id, fundraisings_max_blocks_value = "";
    Button add_plege_btn;

    ArrayList<String> fundrasing_name_list = new ArrayList<>();
    ArrayList<String> fundrasing_id_list = new ArrayList<>();

    ArrayList<String> fundrasing_amount_list = new ArrayList<>();
    LinearLayout block_layout;
    LinearLayout pledge_text_layout;
    String fp;
    String fm;
    RecyclerView fundraising_recyclerview;
    IntiativeFundraising_Adapter fundraising_adapter;

    String project_ammount = "";
    RecyclerView project_recyclerview;
    IntiativeProject_Adapter project_adapter;

    RecyclerView matcher_recyclerview;
    IntiativeMatcher_Adapter matcher_adapter;

    LinearLayout matcher_layout;


    TextView intiative_name;
    TextView fundraising_no_found, project_no_found;

    String InitiativeId = "0";

    ArrayList<Intiative_Detail_Data_Basic_Info_Model> basicinfo = new ArrayList<>();
    ArrayList<Intiative_Detail_Data_Projects_Model> projects = new ArrayList<>();


    ArrayList<Intiative_Detail_Data_Fundraising_Model> fundraising = new ArrayList<>();
    ArrayList<Intiative_Detail_Data_Pledgers_Model> Pledgers = new ArrayList<>();
    ArrayList<Intiative_Detail_Data_Fundraising_Model> amount = new ArrayList<>();


    ArrayList<Intiative_Detail_Group_Data_Model> initiative_group_list = new ArrayList<>();

    ArrayList<String> initiative_group_string_list = new ArrayList<>();
    ArrayList<String> initiative_block_string_list = new ArrayList<>();

    ArrayList<String> initiative_amount_string_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intiative___detail__updated);
        Timber.e("onCreate----------Intiative_Activity_Detail_Updated---------------");
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Initiative Detail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            InitiativeId = bundle.getString("InitiativeId");
            Timber.e("InitiativeId============" + InitiativeId);
        }

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Intiative_Activity_Detail_Updated.this);

                finish();
            }
        });



        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        loading_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        loading_dialog.show();

        //.............................getting intiative detail........
        Timber.e("Call----------getInitiativeDetailFunction---------------");

        getInitiativeDetailFunction(InitiativeId);

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.activityResumed();
        loading_dialog.show();
        getInitiativeDetailFunction(InitiativeId);
    }

    public void intialize_view() {

        intiative_name = findViewById(R.id.intiative_name);
        //progress bar and value
        top_progress_ammount = (TextView) findViewById(R.id.top_progress_ammount);
        bottom_progress_ammount = (TextView) findViewById(R.id.bottom_progress_ammount);

        bottom_progress_ammount_layout = (LinearLayout) findViewById(R.id.bottom_progress_ammount_layout);
        bottom_progress_ammount_layout.setVisibility(View.GONE);

        et_intiative_amount = (EditText) findViewById(R.id.et_intiative_amount);
        String mPAmount = et_intiative_amount.getText().toString();

        et_intiative_amount_layout = (LinearLayout) findViewById(R.id.et_intiative_amount_layout);
        intiative_amount_spinner_layout = (LinearLayout) findViewById(R.id.intiative_amount_spinner_layout);
        top_progressBar = (ProgressBar) findViewById(R.id.top_progressBar);
        bottom_progressBar = (ProgressBar) findViewById(R.id.bottom_progressBar);

        block_layout = findViewById(R.id.block_layout);
        block_layout.setVisibility(View.GONE);
        pledge_text_layout = findViewById(R.id.pledge_text_layout);
        pledge_text_layout.setVisibility(View.GONE);
        project_no_found = findViewById(R.id.project_no_found);
        fundraising_no_found = findViewById(R.id.fundraising_no_found);

        //fundraising
        if (fundraising.size() > 0) {
            fundraising_no_found.setVisibility(View.GONE);
            fundraising_recyclerview = findViewById(R.id.fundraising_recyclerview);
            fundraising_recyclerview.setHasFixedSize(true);
            fundraising_recyclerview.setLayoutManager(new LinearLayoutManager(this));

            fundraising_adapter = new IntiativeFundraising_Adapter(Intiative_Activity_Detail_Updated.this, fundraising);
            fundraising_recyclerview.setAdapter(fundraising_adapter);
        } else {
            fundraising_no_found.setVisibility(View.VISIBLE);
        }

        //block text views
        line1_block = (TextView) findViewById(R.id.line1_block);
        line2_block = (TextView) findViewById(R.id.line2_block);
        line3_block = (TextView) findViewById(R.id.line3_block);


        block_value = (TextView) findViewById(R.id.block_value);

        add_plege_btn = (Button) findViewById(R.id.add_plege_btn);

        anonymous_checkbox = (CheckBox) findViewById(R.id.anonymous_checkbox);
        hideAmountCheckbox = (CheckBox) findViewById(R.id.hideAmount_checkbox);
        hideAmountCheckbox.setVisibility(View.GONE);
        anonymous_checkbox.setVisibility(View.GONE);

        anonymous_checkbox_layout = (LinearLayout) findViewById(R.id.anonymous_checkbox_layout);
        hideAmount_checkbox_layout = (LinearLayout) findViewById(R.id.hideAmount_checkbox_layout);


        anonymous_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    anonymous_value = "1";
                    Timber.e("anonymous_checkbox: " + anonymous_value);
                } else if (b == false) {
                    anonymous_value = "0";
                    Timber.e("anonymous_checkbox: " + anonymous_value);

                }

            }
        });
        hideAmountCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    hideAmount_value = "1";
                    Timber.e("hideAmountCheckbox: " + hideAmount_value);

                } else if (b == false) {
                    hideAmount_value = "0";
                    Timber.e("hideAmountCheckbox: " + hideAmount_value);

                }

            }
        });

        //matcher recycler view
        matcher_layout = findViewById(R.id.matcher_layout);
        matcher_recyclerview = findViewById(R.id.matcher_recyclerview);
        matcher_recyclerview.setHasFixedSize(true);
        matcher_recyclerview.setLayoutManager(new LinearLayoutManager(this));

        try {


            if (fundraising != null) {
                if (fundraising.size() != 0) {
                    fund_model = fundraising.get(0);

                    //for matcher layout
                    if (fund_model.getMatchers() != null) {
                        if (fund_model.getMatchers().size() > 0) {

                            matcher_layout.setVisibility(View.VISIBLE);
                            matcher_adapter = new IntiativeMatcher_Adapter(Intiative_Activity_Detail_Updated.this, fund_model.getMatchers());
                            matcher_recyclerview.setAdapter(matcher_adapter);
                        } else {
                            matcher_layout.setVisibility(View.GONE);
                        }
                    } else {
                        matcher_layout.setVisibility(View.GONE);
                    }


                    //aggr fundraising type B h tu block show krwana
                    if (fundraising.get(0).getFundraisings__type() != null) {
                        if (fundraising.get(0).getFundraisings__type().equals("B")) {
                            block_layout.setVisibility(View.GONE);
                            pledge_text_layout.setVisibility(View.GONE);
                        } else {

                            block_layout.setVisibility(View.GONE);
                            pledge_text_layout.setVisibility(View.GONE);
//                            block_layout.setVisibility(View.GONE);
//                            pledge_text_layout.setVisibility(View.GONE);
                        }
                    }

                    //setting fundraising data
                    bottom_progress_ammount.setText(" USD " + fund_model.getTotalfundraising() + " USD");  //by shaheer

                    Double progress_value = 0.0;
                    if (Double.valueOf(fund_model.getFundraisings__amount()) == 0) {

                    } else {
                        progress_value = (Double.valueOf(fund_model.getFundrasingcollectedamount()) / Double.valueOf(fund_model.getFundraisings__amount())) * 100;
                    }

                    //int value=Integer.valueOf(String.valueOf(progress_value));
                    int value = Integer.valueOf(progress_value.intValue());

                    ObjectAnimator.ofInt(bottom_progressBar, "progress", value)
                            .setDuration(2500)
                            .start();
                    block_value.setText("( = USD " + String.valueOf(fund_model.getPledge_1_Block()) + ")");

                    //if fundraising type is block then add pledge button is visible and invisible according to condition
                    if (fund_model.getFundraisings__type().equals("B")) {
                        if (fund_model.getIs_plege() == 0) {
                            add_plege_btn.setVisibility(View.VISIBLE);
                            add_plege_btn.setBackground(null);
                            add_plege_btn.setEnabled(true);
                            add_plege_btn.setBackgroundResource(R.drawable.btn_bg);

                        } else if (fund_model.getIs_plege() == 1) {
                            //add_plege_btn.setVisibility(View.GONE);
                            add_plege_btn.setBackground(null);
                            add_plege_btn.setEnabled(false);
                            add_plege_btn.setBackgroundResource(R.drawable.grey_btn_bg);
//                            Toast.makeText(Intiative_Activity_Detail_Updated.this, "You have already pledged.", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        add_plege_btn.setVisibility(View.VISIBLE);
                        add_plege_btn.setBackground(null);
                        add_plege_btn.setEnabled(true);
                        add_plege_btn.setBackgroundResource(R.drawable.btn_bg);
                    }


                }
            } else {
                block_layout.setVisibility(View.GONE);
                pledge_text_layout.setVisibility(View.GONE);
//
//                block_layout.setVisibility(View.GONE);
//                pledge_text_layout.setVisibility(View.GONE);
            }

            if (projects.size() != 0) {
                proj_model = projects.get(0);
            }

            if (basicinfo.size() != 0) {
                basic_model = basicinfo.get(0);

                intiative_name.setText(basic_model.getInitiatives__name());

                //setting top and bottom progressbar and value
                top_progress_ammount.setText("Total Intiative : " + " USD " + basic_model.getAmount() + " USD");  //by shaheer
                bottom_progress_ammount.setText(" USD " + fund_model.getTotalfundraising() + " USD");  //by shaheer

                project_ammount = basic_model.getFundraisings__amount();
//                proj_ammount.setText(basic_model.getFundraisings__amount());   //basic info me se value leni....shaheer


                Double progress_value = 0.0;
                if (Double.valueOf(basic_model.getFundraisings__amount()) == 0) {

                } else {
                    progress_value = (Double.valueOf(basic_model.getAmount_collected()) / Double.valueOf(basic_model.getFundraisings__amount())) * 100;
                }

                //int value=Integer.valueOf(String.valueOf(progress_value));
                int value = Integer.valueOf(progress_value.intValue());

                ObjectAnimator.ofInt(top_progressBar, "progress", value)
                        .setDuration(2500)
                        .start();


            }


            //setting block data
            getBlockData(0);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


        //add plege
        add_plege_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Timber.e("user_id" + Common.login_data.getData().getId());
                Timber.e("anonymous false");
                Timber.e("hide_amount false");
                Timber.e("group_id Charity " + group_id);
                Timber.e("fundraising_id " + fundraisings_id);
                Timber.e("pledge_blocks " + pledge_blocks);
                String result = et_intiative_amount.getText().toString();
                Timber.e("MATCH || PLEDGE " + result);
                Timber.e("MATCH || PLEDGE " + et_intiative_amount.getText().toString());

                //AddPlegeFunction();
                if (block_id.equals("-1") && block_id.equals("0")) {
                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Please select fundraising.", Toast.LENGTH_SHORT).show();


                }
                else if (group_id.equals("-1") || group_id.equals("0")) {
                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Please select charity.", Toast.LENGTH_SHORT).show();

                }


                if (amount_spinner.getSelectedItem().toString().trim().equals("-- Block --")) {

//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Please Fill Required Fields", Toast.LENGTH_SHORT).show();


                } else {

                    if(group_id != "0" ){
                        loading_dialog.show();
                        postSubmitInitiativeFundraising();
                        Encrypt_Data();

                    }

                }

                if (TextUtils.isEmpty(result)) {

                    return;
                } else {


                    if(group_id != "0" ){
                        loading_dialog.show();
                        postSubmitInitiativeFundraising();
                        Encrypt_Data();

                    }
                }


//               else if (et_intiative_amount.toString().trim().length() == 0) {
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Amount Required Field", Toast.LENGTH_SHORT).show();
//                }


//                else if (et_intiative_amount.toString().trim().length() == 0 && et_intiative_amount.toString().matches("")) {
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Please Fill Required Field", Toast.LENGTH_SHORT).show();
//
//                }
//                else {
////                    loading_dialog.show();
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "now hit", Toast.LENGTH_SHORT).show();
////                    postSubmitInitiativeFundraising();
////                    Encrypt_Data();
//                }

            }
        });


        //project list
        if (projects.size() > 0) {

            project_no_found.setVisibility(View.GONE);
            project_recyclerview = findViewById(R.id.intiative_project_recyclerview);
            project_recyclerview.setHasFixedSize(true);
            project_recyclerview.setLayoutManager(new LinearLayoutManager(this));
            project_adapter = new IntiativeProject_Adapter(Intiative_Activity_Detail_Updated.this, project_ammount, projects);
            project_recyclerview.setAdapter(project_adapter);
        } else {
            project_no_found.setVisibility(View.VISIBLE);
        }

        //plegede list
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        for (int i = 0; i < Pledgers.size(); i++) {

            Log.d("PledgeIssue1", "fundraising :" + Pledgers.get(i).getFundraising());
            Log.d("PledgeIssue1", "Charity :" + Pledgers.get(i).getCharity());
            Log.d("PledgeIssue1", "hide ammount :" + Pledgers.get(i).getFundraisingPledges__hide_amount());
            Log.d("PledgeIssue1", "anonymous :" + Pledgers.get(i).getFundraisingPledges__anonymous());
            Log.d("PledgeIssue1", "....................................................................");
        }

        adapter = new Intiative_Detail_RecyclerView_Adapter(Pledgers);
        recyclerView.setAdapter(adapter);
    }

    public void getBlockData(int position) {

        try {

            if (fundraising.size() != 0) {
//                Intiative_Detail_Data_Fundraising_Model fund = fundraising.get(0);

                Intiative_Detail_Data_Fundraising_Model fund = fundraising.get(position);

                Log.d("InitiativeIssue", "Total value :" + fund.getTotalfundraising());


//                a=fundrasingcollectedamount  b=Fundraisings__amount;

                //String[] split = fund.getTotalfundraising().split("of");

                Double a_value = Double.valueOf(fund.getFundrasingcollectedamount());
                Double b_value = Double.valueOf(fund.getFundraisings__amount());

                Double plege_block_size = fund.getPledge_1_Block();


                Double A = a_value / plege_block_size;
                Double B = b_value / plege_block_size;
                Double C = B - A;


                line1_block.setText(Integer.valueOf(A.intValue()) + " blocks ");
                line2_block.setText(Integer.valueOf(C.intValue()) + " blocks ");
                line3_block.setText(fund.getFundraisings__max_blocks());
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void getInitiativeDetailFunction(String id) {
        Timber.e("GET----------getInitiativeDetailFunction---------------");
        Timber.e("auth_token" + Common.auth_token);
        Timber.e("login_data" + Common.login_data.getData().getId());
//        Call<Intiative_Detail_Model> responseCall = retrofitInterface.IntiativeDetail("application/json",Common.intiative_data.getInitiatives__id());

        Call<Intiative_Detail_Model> responseCall = retrofitInterface.IntiativeDetail("application/json", Common.auth_token, id, Common.login_data.getData().getId());


        responseCall.enqueue(new Callback<Intiative_Detail_Model>() {
            @Override
            public void onResponse(Call<Intiative_Detail_Model> call, Response<Intiative_Detail_Model> response) {

                //loading_dialog.dismiss();
                if (response.isSuccessful()) {

                    Timber.e("----------jksahdjkahsdjkas");
                    Timber.e(response.body().getData().toString());
                    Timber.e(String.valueOf(response.body()));


                    Timber.e("Size of time slots ${response.isSuccessful()}");
                    Timber.e("Size of time slots ${response.isSuccessful())}");
                    Timber.e("----------getInitiativeDetailFunction" + response.isSuccessful());
                    Timber.e("----------getInitiativeDetailFunction" + response.body().toString());
                    Timber.e("----------getInitiativeDetailFunction" + response.toString());

                    String status, message;

                    Intiative_Detail_Model responseModel = response.body();


                    Timber.e("----------getInitiativeDetail_responseModel" + responseModel.toString());
                    Timber.e("----------getInitiativeDetail_responseModel" + responseModel.getData().getBasicinfo().toString());
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();


                    Intiative_Detail_Data_Model data = responseModel.getData();
                    Timber.e("----------getInitiativeDetail_responseModel" + responseModel.getData().toString());
                    Timber.e("----------getInitiativeDetail_responseModel" + data.toString());
                    if (data != null) {

                        if (basicinfo != null) {
                            basicinfo.clear();
                        }

                        if (projects != null) {
                            projects.clear();
                        }

                        if (fundraising != null) {
                            fundraising.clear();
                        }

                        if (Pledgers != null) {
                            Pledgers.clear();
                        }

                        basicinfo = data.getBasicinfo();
                        projects = data.getProjects();
                        fundraising = data.getFundraising();
                        Pledgers = data.getPledgers();

                        Timber.e("projects90909091" + data.getProjects());
//                        Timber.e("projects90909092" + projects.get(0).getProjectFundraisings__amount());
//                        Timber.e("projects90909093" + projects.get(0).getGroups_details());
                    }


                    getInitiativeDetailGroupListFunction();

                } else {
                    Timber.e("Fails");
                    Timber.e("Else----------getInitiativeDetailFunction");

                }


            }

            @Override
            public void onFailure(Call<Intiative_Detail_Model> call, Throwable t) {

                getInitiativeDetailGroupListFunction();
                loading_dialog.dismiss();
            }
        });
    }

    private void getInitiativeDetailGroupListFunction() {
        Timber.e("auth_token_GroupList" + Common.auth_token.toString());
        Timber.e("InitiativeId_GroupList" + InitiativeId);
        Call<Intiative_Detail_Group_Model> responseCall = retrofitInterface.IntiativeDetailGroupList("application/json", Common.auth_token, InitiativeId);

        responseCall.enqueue(new Callback<Intiative_Detail_Group_Model>() {
            @Override
            public void onResponse(Call<Intiative_Detail_Group_Model> call, Response<Intiative_Detail_Group_Model> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {
                    Timber.e("IntiativeDetailGroupList" + response.body().toString());
                    Timber.e("IntiativeDetailGroupList" + response.body().getData().toString());

                    String status, message;

                    Intiative_Detail_Group_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    ArrayList<Intiative_Detail_Group_Data_Model> list = new ArrayList<>();

                    list = responseModel.getData();

                    if (list != null) {
                        initiative_group_list = list;

                        ArrayList<String> string_list = new ArrayList<>();
                        string_list.add("-- Charity --");
                        //for group spinner
                        for (int i = 0; i < list.size(); i++) {
                            string_list.add(list.get(i).getGroups__name());
                        }

                        initiative_group_string_list = string_list;
                        Timber.e("initiative_group_string_list" + initiative_group_string_list.toString());
                    }


                    GroupSpinnerView();
                    BlockSpinnerView();
                    AmountSpinnerView();
                    intialize_view();

                } else {
                    Timber.e("isfail");
                }


            }

            @Override
            public void onFailure(Call<Intiative_Detail_Group_Model> call, Throwable t) {
                Timber.e("onfailure");
                Log.d("FaizanTesting", "onfailure :" + t.getMessage());
                GroupSpinnerView();
                BlockSpinnerView();
                AmountSpinnerView();
                intialize_view();

                loading_dialog.dismiss();
            }
        });
    }


    private void postSubmitInitiativeFundraising() {
        Timber.e("insideFunctionSubmitInitiativeFundraising");
        Timber.e("required parameter");
        Timber.e("user_id" + Common.login_data.getData().getId());
        Timber.e("anonymous false" + anonymous_value);
        Timber.e("hide_amount " + hideAmount_value);
        Timber.e("group_id Charity " + group_id);
        Timber.e("fundraising_id " + fundraisings_id);
        Timber.e("block_id " + block_id);
        Timber.e("pledge_blocks " + pledge_blocks);
        String result = et_intiative_amount.getText().toString();
        Timber.e("MATCH || PLEDGE " + result);
        Timber.e("MATCH || PLEDGE " + et_intiative_amount.getText().toString());

        if (fundraisings_id == null) {
            fundraisings_id = block_id;
        }
        SubmitInitiativeFundraisingRequestModel requestModel = new SubmitInitiativeFundraisingRequestModel();
        requestModel.setUser_id(Common.login_data.getData().getId());
        requestModel.setGroup_id(group_id);
        requestModel.setFundraising_id(fundraisings_id);
        requestModel.setAnonymous(anonymous_value);
        requestModel.setHide_amount(hideAmount_value);
        requestModel.setPledge_blocks(pledge_blocks);
        requestModel.setAmount(result);

        Call<SubmitInitiativeFundraisingResponseModel> responseCall = retrofitInterface.submitInitiativeFundraising("application/json", Common.auth_token, requestModel);


        responseCall.enqueue(new Callback<SubmitInitiativeFundraisingResponseModel>() {
            @Override
            public void onResponse(Call<SubmitInitiativeFundraisingResponseModel> call, Response<SubmitInitiativeFundraisingResponseModel> response) {
                Timber.e("insideFunctionSubmitInitiativeFundraising_onResponse");
                loading_dialog.dismiss();


                if (response.isSuccessful()) {
                    //Fundraising Amount is too low
                    Timber.e("insideFunctionSubmitInitiativeFundraising_isSuccessful");
                    loading_dialog.dismiss();
                    String status, message;

                    SubmitInitiativeFundraisingResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("success")) {
                        Toast.makeText(Intiative_Activity_Detail_Updated.this, message.toString(), Toast.LENGTH_LONG);

                        Intent i = new Intent(Intiative_Activity_Detail_Updated.this, Intiative_Activity_Updated.class);
                        startActivity(i);
                        finish();

//                        finish();
//                        startActivity(getIntent());


                    }
                } else {
                    String status, message;
                    SubmitInitiativeFundraisingResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    Timber.e("" + status);
                    Timber.e("" + message);
                    Timber.e("insideFunctionSubmitInitiativeFundraising_isfailure" + response.body().getData().toString());
                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "Something went wrong", Toast.LENGTH_SHORT);
                }

            }

            @Override
            public void onFailure(Call<SubmitInitiativeFundraisingResponseModel> call, Throwable t) {
                Timber.e("insideFunctionSubmitInitiativeFundraising_onFailure");

                loading_dialog.dismiss();
                Toast.makeText(Intiative_Activity_Detail_Updated.this, "Something went wrong", Toast.LENGTH_SHORT);
            }
        });
    }


    public void GroupSpinnerView() {

        try {


            group_spinner = (Spinner) findViewById(R.id.intiative_group_spinner);

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> group_spinnerArrayAdapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, initiative_group_string_list) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // tv.setTextColor(Color.BLACK);

                    } else {

                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };
            group_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            group_spinner.setAdapter(group_spinnerArrayAdapter);
            group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "group_spinner_selectedItemText" + selectedItemText, Toast.LENGTH_SHORT).show();

                    if (position > 0) {


                        group_id = initiative_group_list.get(position - 1).getGroups__id();
                        Timber.e("GroupSpinnerView_onItemSelected" + group_id);

                    } else {
                        group_id = "0";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    public void AmountSpinnerView() {
        try {

            Intiative_amount_list();
            Timber.e("callIntiative_amount_list");
//            group_spinner = (Spinner) findViewById(R.id.intiative_group_spinner);

            amount_spinner = (Spinner) findViewById(R.id.intiative_amount_spinner);
            // Initializing an ArrayAdapter

            final ArrayAdapter<String> group_spinnerArrayAdapter = new ArrayAdapter<String>(
                    //initiative_amount_string_list
                    // fundrasing_amount_list
                    this, R.layout.spinner_item, fundrasing_amount_list) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);


                    TextView tv = (TextView) view;

                    if (position == 0) {
//                         tv.setTextColor(Color.BLACK);
                    } else {

                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };
            group_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            amount_spinner.setAdapter(group_spinnerArrayAdapter);
            amount_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "amount_spinner_selectedItemText" + selectedItemText, Toast.LENGTH_SHORT).show();

                    Timber.e("selectedPledge_blocks" + selectedItemText);
                    pledge_blocks = selectedItemText;
//                    if (position > 0) {
//
//                        amount_spinner_id = fundraising.get(position).getFundraisings__max_blocks();
//                        Timber.e("AmountSpinnerView_onItemSelected"  + amount_spinner_id);
//
//                    } else {
//                        amount_spinner_id = "0";
//                    }
//                    if (position > 0) {
//
////                        group_id = initiative_group_list.get(position - 1).getGroups__id();
//                        amount_spinner_id = amount.get(position - 1).getFundraisings__max_blocks();
//                        String mamount_spinner_id;
//                        mamount_spinner_id = amount.get(position).getFundraisings__max_blocks();
//
//                        Toast.makeText(Intiative_Activity_Detail_Updated.this, "amount_spinner_id" + amount_spinner_id, Toast.LENGTH_SHORT).show();
//                        Timber.e("amount_spinner_id" + amount_spinner_id);
//                        Timber.e("mamount_spinner_id" + mamount_spinner_id);
//
//
//                    } else {
//                        amount_spinner_id = "0";
//                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    public void BlockValue() {
        int block_value = Integer.valueOf(fundraising.get(0).getFundraisings__blocks());
        ArrayList<String> block_list = new ArrayList<>();
        block_list.add("Select block");
        for (int i = 1; i <= block_value; i++) {
            block_list.add(String.valueOf(i));
        }

        initiative_block_string_list = block_list;
    }

    public void Intiative_list() {
//        Timber.e("98789789787897897");
//        Timber.e(String.valueOf(fundraising.size()));
//        Timber.e(String.valueOf(fundraising.get(0).getFundraisings__name()));
        try {


            fundrasing_name_list.clear();

//            fundrasing_name_list.add("-- Fundraising --");
//            fundrasing_name_list.add(fundraising.get(0).getFundraisings__name());
            fundrasing_name_list.add(0,"-- Fundraising --");

            if (fundraising != null) {
                if (fundraising.size() > 0) {
                    for (int i = 0; i < fundraising.size(); i++) {

                        fundrasing_name_list.add(fundraising.get(i).getFundraisings__name());
                    }
                }
            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void Intiative_amount_list() {

        Timber.e("98789789787897897");
        Timber.e(String.valueOf(fundraising.size()));
        Timber.e(String.valueOf(fundraising.get(0).getFundraisings__name()));
        Timber.e(String.valueOf(fundraising.get(0).getFundraisings__type()));
        Timber.e(String.valueOf(fundraising.get(0).getFundraisings__max_blocks()));




        try {

            amount = fundraising;


//            fundrasing_amount_list.clear();
//            initiative_amount_string_list.clear();
            ArrayList<String> string_list = new ArrayList<>();
            fundrasing_amount_list.clear();
            fundrasing_amount_list.add("-- Block --");
            sp = getSharedPreferences("fundraisings_max_blocks_value", 0);
            String  fundraisings_max_blocks_values = sp.getString("fundraisings_max_blocks_value", "0");
            Timber.e("getFundraisings__max_blocks----" + fundraising.get(fundraisings_max_blocks_value_position).getFundraisings__max_blocks());
            Timber.e("fundraisings_max_blocks_value_position----" + fundraisings_max_blocks_value_position);
            //for group spinner
//            fundraisings_max_blocks_value
//            fundraisings_max_blocks_value_position
            Timber.e("beforeForLOOP" + fundraisings_max_blocks_values.length());
//            int l = fundraisings_max_blocks_value.length();
            int l = Integer.parseInt( fundraising.get(fundraisings_max_blocks_value_position).getFundraisings__max_blocks());
            Timber.e("1----" + String.valueOf(l));
            for (int i = 1; i <= l; i++) {

                Timber.e("insideLOOP" + i);
                fundrasing_amount_list.add(String.valueOf(i));
            }

            Timber.e("AfterForLOOP" + fundraisings_max_blocks_values.length());
            Ed.remove("fundraisings_max_blocks_value");
            Ed.apply();
            Timber.e("afterclearIntiative_amount_list----" + fundraisings_max_blocks_values);
//            initiative_amount_string_list = string_list;
            Timber.e("initiative_amount_string_list" + initiative_amount_string_list.toString());

//            fundrasing_amount_list.clear();
//
//
//            fundrasing_amount_list.add("--amount--");
//            if (amount != null) {
//                if (amount.size() > 0) {
//                    for (int i = 0; i < amount.size(); i++) {
//
//                        fundrasing_amount_list.add(amount.get(i).getFundraisings__max_blocks());
//                    }
//                }
//            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void BlockSpinnerView() {


        try {

            Intiative_list();

            block_spinner = (Spinner) findViewById(R.id.intiative_block_spinner);

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> block_spinnerArrayAdapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, fundrasing_name_list) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray

//                        tv.setTextColor(Color.BLACK);

                    } else {

                        //tv.setTextSize(22);
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };
            block_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            block_spinner.setAdapter(block_spinnerArrayAdapter);
            block_spinner.setSelection(1,true);
//            block_spinner.setSelection(block_spinnerArrayAdapter.getPosition("1"),true);
            block_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String selectedItemText = (String) parent.getItemAtPosition(position);
//                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "block_spinner_selectedItemText" + selectedItemText, Toast.LENGTH_SHORT).show();

//                    if(selectedItemText == fundraising.get(0).getFundraisings__name()){
//                        block_layout.setVisibility(View.VISIBLE);
//                        pledge_text_layout.setVisibility(View.VISIBLE);
//                        bottom_progress_ammount_layout.setVisibility(View.VISIBLE);
//                        intiative_amount_spinner_layout.setVisibility(View.VISIBLE);
//                    }

//                    if (selectedItemText !=  -1 ){
//                        image.setImageResource(tempCountry.getCountryImage());
//                        image.setVisibility(View.VISIBLE);
//                    }else{
//                        image.setVisibility(View.GONE);
//                    }

                    if (position > 0) {

                        //target_audience_string=selectedItemText;
//                        Toast.makeText(Intiative_Activity_Detail_Updated.this, "block_spinner_selectedItemText" +fundraising.get(position - 1).getFundraisings__type(), Toast.LENGTH_SHORT).show();

                        block_id = fundraising.get(position - 1).getFundraisings__id();
                        block_byType = fundraising.get(position - 1).getFundraisings__type();
                        Timber.e("BlockSpinnerView_onItemSelected" + block_id);
                        Timber.e("BlockSpinnerView_onItemSelectedISPladge" + fundraising.get(position - 1).getIs_plege());
                        Timber.e("block_idfundraising" + fundraising.get(position - 1).getFundraisings__id());
                        Timber.e("block_id" + block_id);
                        Timber.e("Fundraisings__allow_hidden_amount" + fundraising.get(position - 1).getFundraisings__allow_hidden_amount());
                        Timber.e("Fundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
                        bottom_progress_ammount.setText(" USD " + fundraising.get(position - 1).getTotalfundraising() + " USD");


                        /* hide check box Fundraisings__allow_hidden_amount */
                        if (fundraising.get(position - 1).getFundraisings__allow_hidden_amount().equals("1")) {
                            hideAmountCheckbox.setVisibility(View.VISIBLE);

                        } else {
                            hideAmountCheckbox.setVisibility(View.GONE);

                        }
                        /* hide check box Fundraisings__allow_anonymous */
                        if (fundraising.get(position - 1).getFundraisings__allow_anonymous().equals("1")) {
                            anonymous_checkbox.setVisibility(View.VISIBLE);

                        } else {
                            anonymous_checkbox.setVisibility(View.GONE);

                        }
                        //by shaheer


                        Double progress_value = 0.0;
                        if (Double.valueOf(fundraising.get(position - 1).getFundraisings__amount()) == 0) {

                        } else {
                            progress_value = (Double.valueOf(fundraising.get(position - 1).getFundrasingcollectedamount()) / Double.valueOf(fundraising.get(position - 1).getFundraisings__amount())) * 100;
                        }

                        //int value=Integer.valueOf(String.valueOf(progress_value));
                        int value = Integer.valueOf(progress_value.intValue());

                        ObjectAnimator.ofInt(bottom_progressBar, "progress", value)
                                .setDuration(2500)
                                .start();

                        //update block value
                        getBlockData(position - 1);

                        if (fundraising.get(position - 1).getFundraisings__type() != null) {
                            if (fundraising.get(position - 1).getFundraisings__type().equals("B")) {
                                Log.d("BlockIssue", "Block type");

                                block_byType = fundraising.get(position - 1).getFundraisings__type();
                                //if fundraising type is block then add pledge button is visible and invisible according to condition
                                if ((fundraising.get(position - 1).getIs_plege()) == 0) {

                                    Log.d("BlockIssue", "is Pledge 0");
                                    //add_plege_btn.setVisibility(View.VISIBLE);
                                    block_layout.setVisibility(View.VISIBLE);
                                    pledge_text_layout.setVisibility(View.VISIBLE);
                                    bottom_progress_ammount_layout.setVisibility(View.VISIBLE);

                                    add_plege_btn.setBackground(null);
                                    add_plege_btn.setEnabled(true);
                                    add_plege_btn.setBackgroundResource(R.drawable.btn_bg);
                                    et_intiative_amount_layout.setVisibility(View.GONE);
                                    intiative_amount_spinner_layout.setVisibility(View.VISIBLE);

                                    String fundraisings_max_blocks_value1 = fundraising.get(position - 1).getFundraisings__max_blocks();
                                    fundraisings_max_blocks_value_position = position;

                                    fundraisings_max_blocks_value = fundraisings_max_blocks_value1;
                                    sp = getSharedPreferences("fundraisings_max_blocks_value", 0);
                                    Ed = sp.edit();
                                    Ed.putString("fundraisings_max_blocks_value", fundraisings_max_blocks_value1);
                                    Ed.commit();


//                                             fundrasing_amount_list.clear();
//                                    Intiative_amount_list();


                                    fundraisings_id = fundraising.get(position - 1).getFundraisings__id();


                                    if (fundraising.get(position - 1).getFundraisings__allow_anonymous().equals(String.valueOf(1))) {
                                        anonymous_checkbox.setVisibility(View.VISIBLE);
                                        Timber.e("getFundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
                                    } else {
                                        anonymous_checkbox.setVisibility(View.GONE);
                                        Timber.e("getFundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
                                    }

                                    if (fundraising.get(position - 1).getFundraisings__allow_hidden_amount().equals(String.valueOf(1))) {
                                        hideAmountCheckbox.setVisibility(View.VISIBLE);
                                        Timber.e("getFundraisings__allow_hidden_amount" + fundraising.get(position - 1).getFundraisings__allow_hidden_amount());
                                    } else {
                                        hideAmountCheckbox.setVisibility(View.GONE);
                                        Timber.e("getFundraisings__allow_hidden_amount" + fundraising.get(position - 1).getFundraisings__allow_hidden_amount());
                                    }


                                } else if ((fundraising.get(position - 1).getIs_plege()) == 1) {

                                    Log.e("BlockIssue", "is Pledge 1");

//                                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "You have already pledged.", Toast.LENGTH_SHORT).show();
                                    Common.Toast_Message(Intiative_Activity_Detail_Updated.this,"You have already pledged.--");
//                                    add_plege_btn.setVisibility(View.VISIBLE);
                                    add_plege_btn.setBackground(null);
                                    add_plege_btn.setEnabled(false);
                                    add_plege_btn.setBackgroundResource(R.drawable.grey_btn_bg);
                                    block_layout.setVisibility(View.VISIBLE);
                                    pledge_text_layout.setVisibility(View.VISIBLE);
                                    bottom_progress_ammount_layout.setVisibility(View.VISIBLE);

//                                    add_plege_btn.setVisibility(View.GONE);
//                                    block_layout.setVisibility(View.VISIBLE);
//                                    pledge_text_layout.setVisibility(View.VISIBLE);
////                                    add_plege_btn.setEnabled(true);
//                                    add_plege_btn.setBackground(null);
//                                    add_plege_btn.setBackgroundResource(R.drawable.grey_btn_bg);


                                    intiative_amount_spinner_layout.setVisibility(View.GONE);
                                }


                            } else {
                                Log.d("BlockIssue case 2", "not Block type ");
//                                block_layout.setVisibility(View.GONE);
//                                pledge_text_layout.setVisibility(View.GONE);

                                block_layout.setVisibility(View.VISIBLE);
                                pledge_text_layout.setVisibility(View.VISIBLE);
                                bottom_progress_ammount_layout.setVisibility(View.VISIBLE);
                                //if type is other then block then add pledge button is visible
                                add_plege_btn.setBackground(null);
                                add_plege_btn.setEnabled(true);
                                add_plege_btn.setBackgroundResource(R.drawable.btn_bg);
                            }
                        }

                        //case 2: M --> Match and P --> Pledge

                        if (fundraising.get(position - 1).getFundraisings__type() != null) {
                            if (fundraising.get(position - 1).getFundraisings__type().equals("P") || fundraising.get(position - 1).getFundraisings__type().equals("M")) {
                                Log.e("BlockIssue", "Block type");
                                block_layout.setVisibility(View.GONE);
                                pledge_text_layout.setVisibility(View.GONE);
                                intiative_amount_spinner_layout.setVisibility(View.GONE);

                                fp = fundraising.get(position - 1).getFundraisings__type();
                                fm = fundraising.get(position - 1).getFundraisings__type();
                                Timber.e("fp" + fp);
                                Timber.e("fm" + fm);
                                //if fundraising type is block then add pledge button is visible and invisible according to condition
                                if ((fundraising.get(position - 1).getIs_plege()) == 0) {
                                    Log.d("BlockIssue", "is Pledge 0");
                                    add_plege_btn.setVisibility(View.VISIBLE);
                                    add_plege_btn.setBackground(null);
                                    add_plege_btn.setEnabled(true);
                                    add_plege_btn.setBackgroundResource(R.drawable.btn_bg);

                                    et_intiative_amount_layout.setVisibility(View.VISIBLE);
                                    intiative_amount_spinner_layout.setVisibility(View.GONE);
                                    Timber.e("Fundraisings__type" + fundraising.get(position - 1).getFundraisings__type());
                                    Timber.e("getFundraisings__max_blocks" + fundraising.get(position - 1).getFundraisings__max_blocks());
                                    Timber.e("case 2getFundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
                                    Timber.e("case 2getFundraisings__allow_hidden_amount" + fundraising.get(position - 1).getFundraisings__allow_hidden_amount());


//                                    if(fundraising.get(position - 1).getFundraisings__allow_anonymous() == "1") {
//                                        Timber.e("getFundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
//                                    }
//                                    else {
//                                        Timber.e("getFundraisings__allow_anonymous" + fundraising.get(position - 1).getFundraisings__allow_anonymous());
//                                    }
//                                    if(Pledgers.get(position - 1).getFundraisingPledges__anonymous().equals("1")) {
//                                        Timber.e("getFundraisings__allow_hidden_amount" + fundraising.get(position).getFundraisings__allow_hidden_amount());
//                                    }
//                                    else {
//                                        Timber.e("getFundraisings__allow_hidden_amount" + fundraising.get(position).getFundraisings__allow_hidden_amount());
//                                    }


                                } else if ((fundraising.get(position - 1).getIs_plege()) == 1) {

                                    Log.e("BlockIssue", "is Pledge 1");
//                                    add_plege_btn.setVisibility(View.GONE);
//                                    add_plege_btn.setEnabled(false);
//                                    add_plege_btn.setBackground(null);
//                                    add_plege_btn.setBackgroundResource(R.drawable.grey_btn_bg);

                                    add_plege_btn.setVisibility(View.VISIBLE);
                                    add_plege_btn.setBackground(null);
                                    add_plege_btn.setEnabled(true);
                                    add_plege_btn.setBackgroundResource(R.drawable.btn_bg);

                                    et_intiative_amount_layout.setVisibility(View.VISIBLE);

//                                    Toast.makeText(Intiative_Activity_Detail_Updated.this, "You have already pledged", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Log.d("BlockIssue", "not Block type ");
//                                block_layout.setVisibility(View.GONE);
//                                pledge_text_layout.setVisibility(View.GONE);

//                                block_layout.setVisibility(View.GONE);
//                                pledge_text_layout.setVisibility(View.GONE);

                                //if type is other then block then add pledge button is visible
//                                add_plege_btn.setBackground(null);
//                                add_plege_btn.setEnabled(true);
//                                add_plege_btn.setBackgroundResource(R.drawable.btn_bg);
                            }
                        }


//                        if (fundraising.get(position - 1).getIs_plege() == 0) {
//                            //add_plege_btn.setVisibility(View.VISIBLE);
//                            add_plege_btn.setBackground(null);
//                            add_plege_btn.setEnabled(true);
//                            add_plege_btn.setBackgroundResource(R.drawable.btn_bg);
//                        } else if (fundraising.get(position - 1).getIs_plege() == 1) {
//                            //add_plege_btn.setVisibility(View.GONE);
//                            add_plege_btn.setEnabled(false);
//                            add_plege_btn.setBackground(null);
//                            add_plege_btn.setBackgroundResource(R.drawable.grey_btn_bg);
//                        }

                        if (fundraising.get(position - 1).getMatchers() != null) {
                            if (fundraising.get(position - 1).getMatchers().size() > 0) {

                                matcher_layout.setVisibility(View.VISIBLE);
                                matcher_adapter = new IntiativeMatcher_Adapter(Intiative_Activity_Detail_Updated.this, fundraising.get(position - 1).getMatchers());
                                matcher_recyclerview.setAdapter(matcher_adapter);
                            } else {
                                matcher_layout.setVisibility(View.GONE);
                            }
                        } else {
                            matcher_layout.setVisibility(View.GONE);
                        }


                    }

                    else if (position == 0) {
                        block_id = fundraising.get(position).getFundraisings__id();
                        fundraisings_id = fundraising.get(position).getFundraisings__id();

                    }
                    else {
                        block_id = "0";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void Encrypt_Data() {

        try {

            Intiative_Detail_Encrypt_Request_Model request_model = new Intiative_Detail_Encrypt_Request_Model();

            request_model.setFundraising_id(block_id);
            request_model.setGroup_id(group_id);
            request_model.setInitative_id(InitiativeId);
            request_model.setUser_id(Common.login_data.getData().getId());

            Call<Intiative_Detail_Encrypt_Response_Model> responseCall = retrofitInterface.EncriptIntiativeData("application/json", Common.auth_token, request_model);

            responseCall.enqueue(new Callback<Intiative_Detail_Encrypt_Response_Model>() {
                @Override
                public void onResponse(Call<Intiative_Detail_Encrypt_Response_Model> call, Response<Intiative_Detail_Encrypt_Response_Model> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {


                        String status, message;

                        Intiative_Detail_Encrypt_Response_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        if (status.equals("success")) {
                            String url = Common.LIVE_BASE_URL + "pledgenow?ref=" + responseModel.getData() + "&refby=1";
                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(url));
//                            startActivity(i);
                        }

                    } else {

                    }


                }

                @Override
                public void onFailure(Call<Intiative_Detail_Encrypt_Response_Model> call, Throwable t) {

                    loading_dialog.dismiss();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

            loading_dialog.dismiss();
        }


    }

}
