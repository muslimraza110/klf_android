package com.khojaleadership.KLF.Activities.Splash_Login;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Helper.ConnectionDetector;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.SplashLogin_Models.ForgetPasswordRequestModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.ForgetPasswordResponseModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginRequestModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseDataModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseModel;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore;

import java.util.concurrent.atomic.AtomicReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {
    private RelativeLayout tv_forgotPassword;
    private TextView btn_login;

    TextView user_name_mendatory, password_mendatory;
    LinearLayout user_box, password_box;

    TextView no_internet;
    EditText email, password;

    EditText dialog_email;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    SharedPreferences sp1;
    SharedPreferences.Editor Ed;

    Dialog loading_dialog;

    CheckBox remeber_me;
    Boolean is_Checked_flag = false;

    String unm, pass, role,remeber_flag;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.scrollview_login_activity);

        //pd = Common.progressDialog(this, "Loading");
        loading_dialog = Common.LoadingDilaog(this);


        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        remeber_me = findViewById(R.id.remeber_me);
        remeber_me.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    is_Checked_flag = true;
                } else {
                    is_Checked_flag = false;
                }
            }
        });

        user_name_mendatory = (TextView) findViewById(R.id.user_name_mendatory);
        password_mendatory = (TextView) findViewById(R.id.password_mendatory);

        user_box = (LinearLayout) findViewById(R.id.l1);
        password_box = (LinearLayout) findViewById(R.id.l2);

        btn_login = findViewById(R.id.logIn_button);


        no_internet = (TextView) findViewById(R.id.no_internet);
        if (Common.internetConnection == false) {
            no_internet.setVisibility(View.VISIBLE);
        }

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //white box
                emailWhiteBox();

                //check internet connectivity on text change
                if (new ConnectionDetector(LoginActivity.this).isConnectingToInternet()) {
                    no_internet.setVisibility(View.GONE);
                    Common.internetConnection = true;
                } else {
                    no_internet.setVisibility(View.VISIBLE);
                    Common.internetConnection = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //password box
                passwordWhiteBox();

                //check internet connectivity on text change
                if (new ConnectionDetector(LoginActivity.this).isConnectingToInternet()) {
                    no_internet.setVisibility(View.GONE);
                    Common.internetConnection = true;
                } else {
                    no_internet.setVisibility(View.VISIBLE);
                    Common.internetConnection = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //init shared preferences
        sp1 = this.getSharedPreferences("Login", 0);

        //getting email and password
        unm = sp1.getString("Unm", null);
        pass = sp1.getString("Psw", null);
        role = sp1.getString("role", null);
        remeber_flag=sp1.getString("RemeberFlag",null);
        Ed = sp1.edit();



        if (unm!=null && pass!=null && remeber_flag!=null){
            if (remeber_flag.equals("1")) {
                email.setText(unm);
                password.setText(pass);
                remeber_me.setChecked(true);
            }else if (remeber_flag.equals("0")){
                email.setText("");
                password.setText("");
                remeber_me.setChecked(false);
            }
        }else {
            remeber_me.setChecked(false);
        }


        tv_forgotPassword = (RelativeLayout) findViewById(R.id.forget_password);
        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "Enter your email address to receive an email containing the link to reset your password. It can take the couple of minutes for the email to arrive. If your doesn't exist see in your inbox, verify your junk mail folder.";
                Wellcome_Dialog("Forgot your Password?", message, "Submit", false);
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_btn_functionality();
            }
        });


    }

    public void login_btn_functionality() {
        //check internet connectivity on login click
        if (new ConnectionDetector(LoginActivity.this).isConnectingToInternet()) {
            no_internet.setVisibility(View.GONE);
            Common.internetConnection = true;
        } else {
            no_internet.setVisibility(View.VISIBLE);
            Common.internetConnection = false;
        }


        //email validation.......................
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        if (emailText.length() < 1 && passwordText.length() < 1) {
            Log.d("emailValidation", "both red boxes.");
            //red alert boxs
            emailRedBox();

            passwordRedBox();
        } else {
            if (emailText.length() < 1 && passwordText.length() > 0) {
                 //email red alert box
                emailRedBox();
            } else {

                if (passwordText.length() < 1 && emailText.length() > 0) {
                     //password red alert box
                    passwordRedBox();
                } else {

                     //char checking must contain @ and atleast 2 digits after dot

                    Boolean containsAlpha = emailText.contains("@");
                    Boolean containsDot = emailText.contains(".");

                    String afterDotString = "";
                    if (containsDot) {
                        afterDotString = emailText.substring(emailText.lastIndexOf("."));
                    }


                    if (containsAlpha == true && containsDot == true && afterDotString.length() > 2) {


                        if (Common.internetConnection == false) {
//
                        } else {
                            loading_dialog.show();
                            SignInApiCall();
                        }
                    } else {

                        user_box.setBackground(getResources().getDrawable(R.drawable.red_text_box));
                        user_name_mendatory.setText("Enter valid email address");
                        user_name_mendatory.setVisibility(View.VISIBLE);
                    }

                }

            }

        }
    }

    void Wellcome_Dialog(String Title, String Message, String strBtn, final Boolean hide) {

        // custom dialog
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.extra_page_2);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);

        TextView title_txt = (TextView) dialog.findViewById(R.id.custom_title);
        title_txt.setText(Title);

        TextView message_txt = (TextView) dialog.findViewById(R.id.custom_message);
        message_txt.setText(Message);


        TextView submit_btn = (TextView) dialog.findViewById(R.id.btn_fogotPassword_submit);
        submit_btn.setText(strBtn);

        final TextView email_mendatory = (TextView) dialog.findViewById(R.id.custom_email_mendatory);
        dialog_email = (EditText) dialog.findViewById(R.id.custom_email);
        //on Text change change the color of red box.
        dialog_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //remove red alert boxs
                dialog_email.setBackground(getResources().getDrawable(R.drawable.edittext_round));
                email_mendatory.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        RelativeLayout close_btn = (RelativeLayout) dialog.findViewById(R.id.custom_close_btn);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        if (hide == true) {
            dialog_email.setVisibility(View.INVISIBLE);
        }


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (hide == true) {
                    //when email successfully sent.
                    dialog.dismiss();

                } else {


                    if (dialog_email.getText().toString().equals("")) {
                        //red alert boxs
                        dialog_email.setBackground(getResources().getDrawable(R.drawable.red_text_box));
                        email_mendatory.setText("This filed is required.");
                        email_mendatory.setVisibility(View.VISIBLE);
                    } else {


                         //char checking must contain @ and atleast 2 digits after dot

                        String emailText = dialog_email.getText().toString();

                        Boolean containsAlpha = emailText.contains("@");
                        Boolean containsDot = emailText.contains(".");

                        String afterDotString = "";
                        if (containsDot) {
                            afterDotString = emailText.substring(emailText.lastIndexOf("."));
                        }

                        if (containsAlpha == true && containsDot == true && afterDotString.length() > 2) {
                            dialog.dismiss();
                            loading_dialog.show();
                            ForgetApiCall();
                        } else {
                            //red alert boxs
                            dialog_email.setBackground(getResources().getDrawable(R.drawable.red_text_box));
                            email_mendatory.setText("Email is invalid.");
                            email_mendatory.setVisibility(View.VISIBLE);
                        }

                    }


                }

            }
        });


        dialog.show();
    }

    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        RelativeLayout confirm_custom_close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        confirm_custom_close_btn = dialog.findViewById(R.id.confirm_custom_close_btn);
        confirm_custom_close_btn.setVisibility(View.GONE);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public void SignInApiCall() {

        LoginRequestModel request = new LoginRequestModel();
        final String emailText = email.getText().toString();
        final String passwordText = password.getText().toString();

        AtomicReference<String> fcmToken = new AtomicReference<>("");
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(this, token -> {
            if(token != null){
                fcmToken.set(token);
            }
            Timber.e(token);

            request.setPlatform("2");//for android devices
            request.setEmail(emailText);
            request.setPassword(passwordText);
            request.setFcm_token(fcmToken.get());

            Call<LoginResponseModel> responseCall = retrofitInterface.getLoginAccess("application/json", request);

            responseCall.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                    int statusCode = response.code();
                    if (response.isSuccessful()) {
                        if (statusCode == 200) {
                            Ed.putString("logout", "0");
                            //email and password saved.
//                        if (is_Checked_flag == true) {
                            Ed.putString("Unm", emailText);
                            Ed.putString("Psw", passwordText);
                            //}

                            if (is_Checked_flag==true){
                                Ed.putString("RemeberFlag","1");
                            }else {
                                Ed.putString("RemeberFlag","0");
                            }

                            LoginResponseModel responseModel = response.body();
                            Common.login_data = responseModel;
                            new SharedPreferencesAuthStore(
                                    LoginActivity.this,
                                    new Gson()
                            ).setLoginData(responseModel.getData());


                            if (responseModel.getStatus().equals("success")) {
                                LoginResponseDataModel data = responseModel.getData();
                                Common.auth_token = data.getAuth_token();


                                if (data.getRole().equals("A")) {
                                    loading_dialog.dismiss();
                                    Common.is_Admin_flag = true;

//                            //saving role
                                    //if (is_Checked_flag == true) {
                                    Ed.putString("role", "Admin");
                                    Ed.commit();
//                                } else if (is_Checked_flag == false) {
//                                    Ed.clear();
//                                    Ed.commit();
//                                }

                                    //Toast.makeText(LoginActivity.this, "Successfully login.", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                } else if (data.getRole().equals("C")) {


                                    loading_dialog.dismiss();
                                    Common.is_Admin_flag = false;

                                    //saving role
                                    //if (is_Checked_flag == true) {
                                    Ed.putString("role", "Member");
                                    Ed.commit();
//                                } else if (is_Checked_flag == false) {
//                                    Ed.clear();
//                                    Ed.commit();
//                                }

                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }


                            } else if (responseModel.getStatus().equals("error")) {
                                Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_LONG).show();
                            }


                        } else {
                            loading_dialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_LONG).show();
                }
            });
        });

    }

    private void ForgetApiCall() {
        ForgetPasswordRequestModel request = new ForgetPasswordRequestModel();
        request.setEmail(dialog_email.getText().toString());

        Call<ForgetPasswordResponseModel> responseCall = retrofitInterface.getForgetPasswordAccess("application/json", Common.auth_token, request);
        responseCall.enqueue(new Callback<ForgetPasswordResponseModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponseModel> call, Response<ForgetPasswordResponseModel> response) {
                int statusCode = response.code();


                if (response.isSuccessful()) {
                    loading_dialog.dismiss();

                    ForgetPasswordResponseModel responseModel = response.body();

                    if (responseModel.getStatus().equals("success")) {
                        Confirm_Dialog("Email Sent", "Email has been sent Successfully.", "OK");
                    } else if (responseModel.getStatus().equals("error")) {
                        Confirm_Dialog("Email Sending Failed", "You Entered an invalid Email.", "OK");
                    }

                } else {
                    loading_dialog.dismiss();
                    Confirm_Dialog("Email Sending Failed", "You Entered an invalid Email.", "OK");
                }


            }

            @Override
            public void onFailure(Call<ForgetPasswordResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Confirm_Dialog("Email Sending Failed", "You Entered an invalid Email.", "OK");
            }
        });
    }

    public void emailRedBox() {
        //red alert boxs
        user_box.setBackground(getResources().getDrawable(R.drawable.red_text_box));
        user_name_mendatory.setVisibility(View.VISIBLE);
    }

    public void passwordRedBox() {
        password_box.setBackground(getResources().getDrawable(R.drawable.red_text_box));
        password_mendatory.setVisibility(View.VISIBLE);
    }

    public void emailWhiteBox() {
        //red alert boxs
        user_box.setBackground(getResources().getDrawable(R.drawable.edittext_round));
        user_name_mendatory.setVisibility(View.INVISIBLE);
    }

    public void passwordWhiteBox() {
        password_box.setBackground(getResources().getDrawable(R.drawable.edittext_round));
        password_mendatory.setVisibility(View.INVISIBLE);
    }

}
