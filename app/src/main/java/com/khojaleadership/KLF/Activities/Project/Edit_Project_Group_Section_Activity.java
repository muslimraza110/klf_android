package com.khojaleadership.KLF.Activities.Project;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Project.EditProjectGroupAdapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForEditProjectGroupSection;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectDataModel;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectResponseModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Project_Group_Section_Activity extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private EditProjectGroupAdapter adapter;
    private List<GetAlreadyGroup_ProjectDataModel> sourceDataList=new ArrayList<>();
    List<GetAlreadyGroup_ProjectDataModel> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;

    /**
     * 根据拼音来排列RecyclerView里面的数据类
     */
    private PinyinComparatorForEditProjectGroupSection pinyinComparator;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;

    ProjectsDataModel projects_data_detail=new ProjectsDataModel();
    ArrayList<GetAlreadyGroup_ProjectDataModel> already_project_group_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whitelist_contacts);


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Add Group");

        Intent intent = getIntent();
        projects_data_detail = intent.getParcelableExtra("ProjectData");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Edit_Project_Group_Section_Activity.this);

                Intent i=new Intent(Edit_Project_Group_Section_Activity.this,Edit_Project_Activity.class);
                i.putExtra("ProjectData",projects_data_detail);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        findViewById(R.id.whitelist_contact_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Edit_Project_Group_Section_Activity.this,Edit_Project_Activity.class);
                i.putExtra("ProjectData",projects_data_detail);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        listner = this;

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        getAlreadyGroupsInProject(projects_data_detail.getProjects__id());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i=new Intent(Edit_Project_Group_Section_Activity.this,Edit_Project_Activity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    private void initViews() {

        loading_dialog.dismiss();

        pinyinComparator = new PinyinComparatorForEditProjectGroupSection();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

         sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                 int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        sourceDataList = filledData(already_project_group_list);
         Collections.sort(sourceDataList, pinyinComparator);

         manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        adapter = new EditProjectGroupAdapter(getApplicationContext(), sourceDataList, listner);
        mRecyclerView.setAdapter(adapter);


        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);

         mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }



    private List<GetAlreadyGroup_ProjectDataModel> filledData(List<GetAlreadyGroup_ProjectDataModel> data) {
        List<GetAlreadyGroup_ProjectDataModel> mSortList = new ArrayList<>();


        for (int i = 0; i < data.size(); i++) {
            GetAlreadyGroup_ProjectDataModel sortModel = new GetAlreadyGroup_ProjectDataModel();

            String name,project_id,group_id,IsSelected;

            sortModel.setName(data.get(i).getName());
            sortModel.setProject_id(data.get(i).getProject_id());
            sortModel.setGroup_id(data.get(i).getGroup_id());
            sortModel.setIsSelected(data.get(i).getIsSelected());
            sortModel.setProfileimg(data.get(i).getProfileimg());



            String pinyin_english = data.get(i).getName();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {

                 sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();
            if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }


        return mSortList;
    }

    private void filterData(String filterStr) {

        filter_flag = true;

        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(already_project_group_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (GetAlreadyGroup_ProjectDataModel sortModel : sourceDataList) {
                String name = sortModel.getName();
             if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().startsWith(filterStr)
                        || name.toUpperCase().startsWith(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
         Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }


    private void getAlreadyGroupsInProject(String project_id) {

        Call<GetAlreadyGroup_ProjectModel> call = retrofitInterface.getAlreadyGroupsInProject("application/json",Common.auth_token,
                project_id);


        call.enqueue(new Callback<GetAlreadyGroup_ProjectModel>() {
            @Override
            public void onResponse(Call<GetAlreadyGroup_ProjectModel> call, Response<GetAlreadyGroup_ProjectModel> response) {
                 loading_dialog.dismiss();
                if (response.isSuccessful()) {
                    String status, message;


                    GetAlreadyGroup_ProjectModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<GetAlreadyGroup_ProjectDataModel> list = new ArrayList<>();
                    list = responseModel.getData();

                    if (list != null) {
                        already_project_group_list=list;
                        initViews();
                    }

                }
            }

            @Override
            public void onFailure(Call<GetAlreadyGroup_ProjectModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }

    //..........................................................................

    @Override
    public void onRowClick(int position) {


    }

    @Override
    public void onViewClcik(int position, View v) {

        GetAlreadyGroup_ProjectDataModel edit_already_project_group_data_detail=new GetAlreadyGroup_ProjectDataModel();
        String flag;
        switch (v.getId()) {
            case R.id.group_checkbox:
                if (filter_flag == true) {
                    edit_already_project_group_data_detail = filterDataList.get(position);
                } else {
                    edit_already_project_group_data_detail = sourceDataList.get(position);
                }

                flag=edit_already_project_group_data_detail.getIsSelected();

//
                    if (flag.equals("0")) {
                        //now add to white list
                         loading_dialog.show();
                        onAddGroupToProject(edit_already_project_group_data_detail.getGroup_id(),projects_data_detail.getProjects__id());
                     } else if (flag.equals("1")) {
                        //now remove to white list
                         loading_dialog.show();
                        onRemoveGroupFromProject(edit_already_project_group_data_detail.getGroup_id(),projects_data_detail.getProjects__id());
                     }

                break;
            default:
                break;
        }
    }

    private void onAddGroupToProject(String group_id, String project_id) {


        Add_group_To_ProjectRequestModel requestModel = new Add_group_To_ProjectRequestModel();

        requestModel.setGroup_id(group_id);
        requestModel.setProject_id(project_id);


        Call<Add_group_To_ProjectResponseModel> responseCall = retrofitInterface.AddGroupToProject("application/json",Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<Add_group_To_ProjectResponseModel>() {
            @Override
            public void onResponse(Call<Add_group_To_ProjectResponseModel> call, Response<Add_group_To_ProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    loading_dialog.dismiss();

                    Add_group_To_ProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                     Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group added to Project successfully", Toast.LENGTH_SHORT).show();
                    getAlreadyGroupsInProject(projects_data_detail.getProjects__id());
                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group added to Project Failed", Toast.LENGTH_SHORT).show();
                 }

            }

            @Override
            public void onFailure(Call<Add_group_To_ProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group added to Project Failed", Toast.LENGTH_SHORT).show();
             }
        });

    }

    private void onRemoveGroupFromProject(String group_id, String project_id) {


        Remove_Group_From_ProjectRequestModel requestModel = new Remove_Group_From_ProjectRequestModel();

        requestModel.setGroup_id(group_id);
        requestModel.setProject_id(project_id);


        Call<Remove_Group_From_ProjectResponseModel> responseCall = retrofitInterface.RemoveGroupFromProject("application/json",Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<Remove_Group_From_ProjectResponseModel>() {
            @Override
            public void onResponse(Call<Remove_Group_From_ProjectResponseModel> call, Response<Remove_Group_From_ProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    loading_dialog.dismiss();

                    Remove_Group_From_ProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group remove from Project successfully", Toast.LENGTH_SHORT).show();
                    getAlreadyGroupsInProject(projects_data_detail.getProjects__id());
                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group remove from Project Failed", Toast.LENGTH_SHORT).show();
                 }

            }

            @Override
            public void onFailure(Call<Remove_Group_From_ProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Edit_Project_Group_Section_Activity.this, "Group remove from Project Failed", Toast.LENGTH_SHORT).show();
             }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
