package com.khojaleadership.KLF.Activities.event_new.planner.requestMeeting

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.delegates.DelegateHeaderItem
import com.khojaleadership.KLF.Activities.event_new.delegates.DelegateUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.PersonDetailResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import kotlinx.coroutines.Job
import timber.log.Timber

class RequestMeetingViewModel(val app: Application) : AndroidViewModel(app) {

    var searchTextHasChanged = false

    private val _delegates: MutableLiveData<List<DelegateHeaderItem>> = MutableLiveData()
    val delegates: LiveData<List<DelegateHeaderItem>> = _delegates

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _socialClickEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val socialClickEvent: LiveData<Event<String>> = _socialClickEvent


    private val _delegateClickEvent: MutableLiveData<Event<DelegateUiModel>> = MutableLiveData()
    val delegateClickEvent: LiveData<Event<DelegateUiModel>> = _delegateClickEvent

    var onSocialClick: ((String) -> Unit) = {
        _socialClickEvent.value = Event(it)
    }


    var onDelegateClick: ((DelegateUiModel) -> Unit) = {
        _delegateClickEvent.value = Event(it)
    }

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    private val recentlyAdded = "Recently Added"
    private val allCountries = "All Countries"
    private val allIndustries = "All Industries"

    private var delegatesFetchJob: Job? = null


    fun resetUsers() {
        _delegates.value = listOf()
    }


    fun getDelegates(timeSlotModel: EventTimeSlotUiModel, searchText: String) {

        _eventState.value = Result.Loading

        delegatesFetchJob?.cancel()

        Common.login_data?.data?.id?.let { userId ->
            delegatesFetchJob = viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getSpeakersAndDelegatesForProgramDetailsId(
                                searchText = searchText,
                                attendeeAvailabilityStatus = "1",
                                eventId = authStore.event?.eventId ?: "",
                                programDetailsId = timeSlotModel.programDetailsId ?: "",
                                userId = userId
                        )

                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->
                                val tempList = mutableListOf<DelegateHeaderItem>()

                                val headerRecentlyAdded = DelegateHeaderItem(
                                        isOpened = false,
                                        title = recentlyAdded
                                )

                                setDelegateListWithHeader(
                                        header = headerRecentlyAdded,
                                        list = data.recentlyAdded
                                )


                                val headerAllCountries = DelegateHeaderItem(
                                        isOpened = false,
                                        title = allCountries
                                )

                                setDelegateListWithHeader(
                                        header = headerAllCountries,
                                        list = data.allCountry
                                )

                                val headerAllIndustries = DelegateHeaderItem(
                                        isOpened = false,
                                        title = allIndustries
                                )

                                setDelegateListWithHeader(
                                        header = headerAllIndustries,
                                        list = data.allIndustry
                                )


                                if (headerRecentlyAdded.subItemsCount > 0) {
                                    tempList.add(headerRecentlyAdded)
                                }
                                if (headerAllCountries.subItemsCount > 0) {
                                    tempList.add(headerAllCountries)
                                }
                                if (headerAllIndustries.subItemsCount > 0) {
                                    tempList.add(headerAllIndustries)
                                }

                                tempList.firstOrNull()?.isOpened = true

                                _delegates.value = tempList

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
                                )

                            } ?: run {
                                Timber.e(response.message)
                                _delegates.value = listOf()

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
                                )
                                searchTextHasChanged = true
                            }


                        } else {
                            Timber.e(response.message)

                            _eventState.value = Result.Error(
                                    message = if (response.message.isNullOrEmpty()) Common.somethingWentWrongMessage else response.message
                            )
                            searchTextHasChanged = true
                        }
                    },
                    error = {
                        Timber.e(it)
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        searchTextHasChanged = true
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            searchTextHasChanged = true
        }


    }


    private fun setDelegateListWithHeader(header: DelegateHeaderItem, list: List<PersonDetailResponse?>?) {

        //filter{} condition is necessary to remove the persons. These persons already have a meeting at this particular slot

        list?.filterNotNull()
//                ?.filter {
//                    it.meetingSlots != null
//                }
                ?.map { person ->
                    mapToDelegatesUiModel(
                            responseData = person,
                            headerItem = header
                    )
                } ?: mutableListOf()

    }


    private fun mapToDelegatesUiModel(responseData: PersonDetailResponse, headerItem: DelegateHeaderItem): DelegateUiModel {
        val delegate = DelegateUiModel(
                name = responseData.name ?: "",
                description = responseData.shortDescription ?: "",
                userId = responseData.id ?: "",
                summitEventsFacultyId = responseData.summitEventsFacultyId ?: "",
                designation = responseData.designation ?: "",
                facebook = responseData.facebookLink ?: "",
                linkedIn = responseData.linkedInLink ?: "",
                photoUrl = responseData.userImage ?: "",
                twitter = responseData.twitterLink ?: "",
//                availableSlotData = responseData.meetingSlots,
                onClick = onDelegateClick,
                onSocialClick = onSocialClick,
                context = app,
                header = headerItem,
                isDelegate = responseData.isDelegate == "1",
                isSpeaker = responseData.isSpeaker == "1"
        )
        headerItem.addSubItem(delegate)
        return delegate
    }

}