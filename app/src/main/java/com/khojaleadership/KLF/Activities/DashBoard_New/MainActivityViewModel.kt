package com.khojaleadership.KLF.Activities.DashBoard_New


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel() : ViewModel(){

    private val _eventsData : MutableLiveData<List<EventHomeUiModel>> = MutableLiveData()
    val eventsData: LiveData<List<EventHomeUiModel>> =_eventsData

    private val _navigationData : MutableLiveData<List<NavigationHomeUiModel>> = MutableLiveData()
    val navigationData: LiveData<List<NavigationHomeUiModel>> =_navigationData

    fun fetchData(){
        val tempList = listOf<EventHomeUiModel>(
                EventHomeUiModel(name = "Event", itemType = HomeEventsItemType.EVENT),
                EventHomeUiModel(name = "Project", itemType = HomeEventsItemType.PROJECT),
                EventHomeUiModel(name = "Funding", itemType = HomeEventsItemType.FUNDING)
        )

        val tempList2 = listOf<NavigationHomeUiModel>(
                NavigationHomeUiModel(name = "Groups"),
                NavigationHomeUiModel(name = "Projects"),
                NavigationHomeUiModel(name = "Funds"),
                NavigationHomeUiModel(name = "Suggestions"),
                NavigationHomeUiModel(name = "News")
        )
        _eventsData.postValue(tempList)
        _navigationData.postValue(tempList2)
    }
}