package com.khojaleadership.KLF.Activities.DashBoard_New

import java.util.*

data class EventHomeUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val name: String,
        val itemType: HomeEventsItemType
)

enum class HomeEventsItemType {
    EVENT,
    PROJECT,
    FUNDING
}