package com.khojaleadership.KLF.Activities.Project;

import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Project.Projects_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.DeleteProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.DeleteProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Projects_Activity extends AppCompatActivity implements RecyclerViewClickListner {

    RecyclerView recyclerView;
    Projects_Adapter adapter;
    ArrayList<ProjectsDataModel> list;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    ArrayList<ProjectsDataModel> projects_data_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_);

        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Projects");


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Projects_Activity.this);
                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);

        loading_dialog.show();
        onGetProjectListData();

   //     getData();

        findViewById(R.id.c_add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Common.add_project_in_group_flag=false;
                Intent add=new Intent(Projects_Activity.this, Add_Project_Activity.class);
                add.putExtra("AddProjectInGroupFlag","0");
                startActivity(add);
            }
        });


    }

//    public void getData(){
//        list=new ArrayList<>();
//    }

    private void onGetProjectListData() {


        Call<ProjectsModel> responseCall = retrofitInterface.getProjectsList("application/json",Common.auth_token);
        responseCall.enqueue(new Callback<ProjectsModel>() {
            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {

                if (response.isSuccessful()) {

                    String status, message;

                    ProjectsModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    ArrayList<ProjectsDataModel> project_data=responseModel.getData();

                    if (project_data!=null){
                        loading_dialog.dismiss();
                        projects_data_list=project_data;
                        intialize_view();
                    }

                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                loading_dialog.dismiss();
                 //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    public void intialize_view(){
        recyclerView =  (RecyclerView)findViewById(R.id.recyclerview);


        adapter= new Projects_Adapter(this,projects_data_list,this);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onRowClick(int position) {
       // Common.is_project_detail=true;
//        Common.projects_data_detail=projects_data_list.get(position);

        ProjectsDataModel projects_data_detail=new ProjectsDataModel();
        projects_data_detail=projects_data_list.get(position);

        //Common.projects_data_detail_id_new=projects_data_list.get(position).getProjects__id();
         Intent i=new Intent(Projects_Activity.this, View_Projects_Detail_Updated.class);
         i.putExtra("ProjectData",projects_data_detail);
         i.putExtra("isProjectDetail","1");
        startActivity(i);
    }

    @Override
    public void onViewClcik(int position, View v) {
//        Common.projects_data_detail=projects_data_list.get(position);

        ProjectsDataModel projects_data_detail=new ProjectsDataModel();
        projects_data_detail=projects_data_list.get(position);

        if (v.getId()==R.id.edit_project_layout){
            //edit here
            Intent i=new Intent(Projects_Activity.this, Edit_Project_Activity.class);
            i.putExtra("ProjectData",projects_data_detail);
            startActivity(i);
        }else if (v.getId()==R.id.delete_project_layout){
            //delete here

            Confirm_Dialog("Delete Event","Are you sure to delete this project?","Yes",projects_data_detail.getProjects__id());

        }


    }

    public void Confirm_Dialog(String title, String message, String btn, final String project_id) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(Projects_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn=(RelativeLayout)dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);



        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                DeleteProjectFunction(project_id);

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void DeleteProjectFunction(String project_id) {

        DeleteProjectRequestModel request=new DeleteProjectRequestModel();
        request.setProject_id(project_id);


        Call<DeleteProjectResponseModel> responseCall = retrofitInterface.DeleteProject("application/json",Common.auth_token,request);

        responseCall.enqueue(new Callback<DeleteProjectResponseModel>() {
            @Override
            public void onResponse(Call<DeleteProjectResponseModel> call, Response<DeleteProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    DeleteProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")){
                        //data is null
                    }else if (status.equals("success")){
                        Toast.makeText(Projects_Activity.this,"Project deleted Successfully.",Toast.LENGTH_SHORT).show();

                        onGetProjectListData();
                    }


                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<DeleteProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
             }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }


}
