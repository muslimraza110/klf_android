package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.EditText;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.AddTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.AddTopicResponseModel;
import com.khojaleadership.KLF.R;


import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Topic extends AppCompatActivity {

    EditText title;
    RichEditor mEditor;
    ScrollView scrollView;

    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog loading_dialog;

    Boolean flag = false;

    TextView header_title;

    ImageButton  action_align_left, action_align_center, action_align_right;
    Boolean  action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    String Category;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_topic);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        findViewById(R.id.container_execpt_toolbar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Topic.this);
            }
        });



        Bundle bundle=getIntent().getExtras();
        if (bundle!=null) {
            Category = bundle.getString("Category");
        }


        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Topic.this);
                Common.hideKeyboard(v, getApplicationContext());
                Intent i = new Intent(Add_Topic.this, Topic.class);
                i.putExtra("Category",Category);
                startActivity(i);
                finish();
            }
        });
//        try {


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Add Topic");




        loading_dialog = Common.LoadingDilaog(this);

        title = (EditText) findViewById(R.id.add_topic_title);

        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(22);
        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);

        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Description");
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);


        scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);



                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag=false;
                            action_right_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);



                            action_left_flag=false;
                            action_right_flag=false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag=false;
                            action_left_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });


        findViewById(R.id.publish_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String title1 = title.getText().toString();
                String description = mEditor.getHtml();

                if ((title1 == null || title1 == "" || title1.equalsIgnoreCase(""))) {
                    title.setError("Title should not be empty.");
                } else if (title1.replace(" ", "").equals("")) {
                    title.setError("Title should not contain only space.");
                } else {

                    //aik dafa button press py add kre
                    if (flag == false) {
                        flag = true;
                        loading_dialog.show();
                        onAddTopicBtnClick(title1, description);
                    }
                }


            }


        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(Add_Topic.this, Topic.class);
        i.putExtra("Category",Category);
        startActivity(i);
        finish();
    }

    private void onAddTopicBtnClick(String title, String desc) {

        try {

            AddTopicRequestModel requestModel = new AddTopicRequestModel();

            requestModel.setUser_id(Common.login_data.getData().getId());

            requestModel.setCategory_name(Category);

            requestModel.setSubtopic_title(title);
            requestModel.setSubtopic_description(desc);

            Call<AddTopicResponseModel> responseCall = retrofitInterface.addTopic("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddTopicResponseModel>() {
                @Override
                public void onResponse(Call<AddTopicResponseModel> call, Response<AddTopicResponseModel> response) {

                    if (response.isSuccessful()) {
                        //loading_dialog.dismiss();

                        String status, message;

                        AddTopicResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        onMainTopicBtnClick(Category);


                    }
                }

                @Override
                public void onFailure(Call<AddTopicResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Confirm_Dialog("Topic", "Process Failed!!!", "Retry");
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
            Log.d("NullExceptionIssue", "onRowClick: " + e.getMessage());
        }


    }

    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        // custom dialog
        final Dialog dialog = new Dialog(Add_Topic.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                loading_dialog.show();
                onMainTopicBtnClick(Category);
            }
        });


        dialog.show();
    }


    private void onMainTopicBtnClick(String category) {

        Intent admin_forum_intent = new Intent(Add_Topic.this, Topic.class);
        admin_forum_intent.putExtra("Category", category);
        startActivity(admin_forum_intent);
        finish();

//        try {
//
//            final ArrayList<GetTopicDataModel> list = new ArrayList<>();
//
//            Call<GetTopicModel> call = retrofitInterface.getTopic("application/json", Common.auth_token,
//                    admin, 1, 1000);
//            call.enqueue(new Callback<GetTopicModel>() {
//                @Override
//                public void onResponse(Call<GetTopicModel> call, Response<GetTopicModel> response) {
//                    ArrayList<GetTopicDataModel> topic_data_list = new ArrayList<>();
//
//                    if (response.isSuccessful()) {
//                        String status, message, totalcount, totalpages;
//                        GetTopicModel responseModel = response.body();
//
//                        status = responseModel.getStatus();
//                        message = responseModel.getMessage();
//                        totalcount = responseModel.getTotalcount();
//                        totalpages = responseModel.getTotalpages();
//
//                        topic_data_list = responseModel.getData();
//                        for (GetTopicDataModel data : topic_data_list) {
//                            String Subforums__id, Subforums__user_id, Subforums__model, Subforums__title, Subforums__description, Subforums__status, Subforums__created, Subforums__modified;
//
//                            Subforums__id = data.getSubforums__id();
//                            Subforums__user_id = data.getSubforums__user_id();
//                            Subforums__model = data.getSubforums__model();
//                            Subforums__title = data.getSubforums__title();
//                            Subforums__description = data.getSubforums__description();
//                            Subforums__status = data.getSubforums__status();
//                            Subforums__created = data.getSubforums__created();
//                            Subforums__modified = data.getSubforums__modified();
//
//
//                            list.add(new GetTopicDataModel(Subforums__id, Subforums__user_id, Subforums__model, Subforums__title, Subforums__description, Subforums__status, Subforums__created, Subforums__modified));
//                        }
//
//                        ArrayList<Integer> ids = new ArrayList<>();
//
//                        for (int i = 0; i < list.size(); i++) {
//                            ids.add(Integer.valueOf(list.get(i).getSubforums__id()));
//                        }
//
//                        if (Common.ids != null) {
//                            Common.ids.clear();
//                        }
//
//                        Common.ids = ids;
//
//                        if (list != null) {
//                            loading_dialog.dismiss();
//
//                            if (Common.topic_data != null) {
//                                Common.topic_data.clear();
//                            }
//                            Common.topic_data = list;
//                            Intent admin_forum_intent = new Intent(Add_Topic.this, Topic.class);
//                            startActivity(admin_forum_intent);
//                            finish();
//                        }
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<GetTopicModel> call, Throwable t) {
//
//                }
//            });
//
//            throw new RuntimeException("Run Time exception");
//        } catch (Exception e) {
//         }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
