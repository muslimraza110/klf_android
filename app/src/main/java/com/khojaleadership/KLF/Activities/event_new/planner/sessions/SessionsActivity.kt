package com.khojaleadership.KLF.Activities.event_new.planner.sessions

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Activities.event_new.userProfile.UserProfileActivity
import com.khojaleadership.KLF.Adapter.event_new.planner.breakoutSessions.SessionsAdapter
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivitySessionsBinding


/**
 * This Activity is presented as a dialog in the app.
 */
class SessionsActivity : AppCompatActivity() {

    private val viewModel: SessionsViewModel by viewModels()

    private lateinit var binding: ActivitySessionsBinding

    private lateinit var sessionsAdapter: SessionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // To show this activity as a dialog, also theme is applied in manifest in the activity Tag
        setTheme(R.style.AppTheme_Dialog)
        setFinishOnTouchOutside(false)

        super.onCreate(savedInstanceState)
        binding = ActivitySessionsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
//                    binding.rvList.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
//                    binding.rvList.visibility = View.VISIBLE
//                    binding.tvNoRecordsFound.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
//                    binding.rvList.visibility = View.VISIBLE
//                    binding.tvNoRecordsFound.text = it.message
//                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })


        viewModel.messageEvent.observe(this, Observer {
            it?.consume()?.let { message ->
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.sessions.observe(this, Observer {
            it?.let {
                sessionsAdapter.submitList(it)
                sessionsAdapter.notifyDataSetChanged()
            } ?: run {

            }
        })
    }


    private fun setUpViews() {
        Common.setSizeOfActivityAsDialog(
                windowManager,
                this,
                window
        )

        binding.tvSession.text = viewModel.authStore.sessionsModel?.title

        sessionsAdapter = SessionsAdapter(
                context = this,
                onJoinLeaveBtnClick = {
                    if (it.iAmEnrolled) {
                        showDialog(it)
                    } else {
                        if (it.roomLimit - it.roomOccupied > 0) {
                            showDialog(it)
                        } else {
                            Toast.makeText(this, "This session has no seats available", Toast.LENGTH_SHORT).show()
                        }
                    }
                },
                onHyperLinkClick = {
//                    viewModel.authStore.lastSelectedPersonAvailability = it.availableSlotData
                    val intent = Intent(this, UserProfileActivity::class.java)
                    intent.putExtra(UserProfileActivity.KEY_FACULTY_ID, it.summitEventsFacultyId)
                    intent.putExtra(UserProfileActivity.KEY_USER_ID, it.userId)
                    intent.putExtra(UserProfileActivity.KEY_IS_DELEGATE, it.isDelegate)
                    intent.putExtra(UserProfileActivity.KEY_USER_FB, it.facebook)
                    intent.putExtra(UserProfileActivity.KEY_USER_TWITTER, it.twitter)
                    intent.putExtra(UserProfileActivity.KEY_USER_LINKED_IN, it.linkedIn)
                    startActivity(intent)
                },
                sessionType = viewModel.authStore.sessionsModel?.sessionType ?: "",
                onMeetingLinkClick = {
                    Common.loadUrlInBrowser(it, this)
                },
                onMeetingLinkLongClick = {
                    copyTextToClipBoard(it)
                    Toast.makeText(this, "Link copied!", Toast.LENGTH_SHORT).show()
                }
        )

        binding.rvList.apply {
            adapter = sessionsAdapter
            layoutManager = LinearLayoutManager(this@SessionsActivity)
        }
        binding.imageClose.setOnClickListener {
            finish()
        }
    }


    private fun copyTextToClipBoard(text: String) {
        val clipboard: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("link", text)
        clipboard.setPrimaryClip(clip)
    }


    private fun showDialog(item: SessionItemUiModel) {
        val enroll = !item.iAmEnrolled
        val previouslyJoinedSession = viewModel.sessions.value?.firstOrNull {
            it.iAmEnrolled
        }

        val message = if (enroll) {
            previouslyJoinedSession?.let {
                "You will be removed from \"${it.title}\" if you join this session. Are you sure you want to join this session?"
            } ?: run {
                "Are you sure you want to join this session?"
            }
        } else "Are you sure you want to leave this session?"

        val dialog = AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("YES") { dialog, which ->
                    viewModel.joinOrLeaveBreakoutSession(item)
                }
                .setNegativeButton("NO") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.appBlue))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.appBlue))
        }
        dialog.show()
    }


}