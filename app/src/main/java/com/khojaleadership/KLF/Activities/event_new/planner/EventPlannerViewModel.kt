package com.khojaleadership.KLF.Activities.event_new.planner

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.*
import com.khojaleadership.KLF.Activities.event_new.planner.sessions.SessionItemUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.*
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class EventPlannerViewModel(application: Application) : AndroidViewModel(application) {

    var scrollToSpecificPosition = false

    var clickedSessionPosition = -1

    var userClicked = false
    var lastSelectedDayIndex = 0

    var isDataUpdated = false

    var allDelegates: List<PersonDetailUiModel>? = null

    private val _selectedDelegates: MutableLiveData<List<PersonDetailUiModel>> = MutableLiveData()
    val selectedDelegates: LiveData<List<PersonDetailUiModel>> = _selectedDelegates

    private val _meetingSlotClickEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val meetingSlotClickEvent: LiveData<Event<String>> = _meetingSlotClickEvent

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    var clickedRequestMeetingProgramDetailsId: String? = null

    private val _enableSpinnersEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val enableSpinnersEvent: LiveData<Event<Boolean>> = _enableSpinnersEvent

    private val _dayClickEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val dayClickEvent: LiveData<Event<String>> = _dayClickEvent

    private val _myAvailability: MutableLiveData<List<CheckAvailabilityUiModel>> = MutableLiveData()
    val myAvailability: LiveData<List<CheckAvailabilityUiModel>> = _myAvailability

//    private val _myAvailability: MutableLiveData<LinkedHashMap<EventDaysUiModel, List<CheckAvailabilityItemUiModel>>> = MutableLiveData()
//    val myAvailability: LiveData<LinkedHashMap<EventDaysUiModel, List<CheckAvailabilityItemUiModel>>> = _myAvailability

    private val _daysData: MutableLiveData<List<EventDaysUiModel>> = MutableLiveData()
    val daysData: LiveData<List<EventDaysUiModel>> = _daysData

    private val _spinnerDaysData: MutableLiveData<List<EventDaysUiModel>> = MutableLiveData()
    val spinnerDaysData: LiveData<List<EventDaysUiModel>> = _spinnerDaysData


    private val _spinnerTimeData: MutableLiveData<List<List<EventTimeSlotUiModel>>> = MutableLiveData()
    val spinnerTimeData: LiveData<List<List<EventTimeSlotUiModel>>> = _spinnerTimeData

    var selectedTimeSlot: EventTimeSlotUiModel? = null
    var selectedDaySlotIndex: Int? = null

    var isCheckAvailabilitySelected = false

    private val _checkAvailabilityToggle: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val checkAvailabilityToggle: LiveData<Event<Boolean>> = _checkAvailabilityToggle


    private val _myItineraryList: MutableLiveData<List<MyItineraryUiModel>> = MutableLiveData()
//    val myItineraryList: LiveData<List<MyItineraryUiModel>> = _myItineraryList

    private val _myItinerary: MutableLiveData<MyItineraryUiModel> = MutableLiveData()
    val myItinerary: LiveData<MyItineraryUiModel> = _myItinerary

    private val _meetingRequestSentEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val meetingRequestSentEvent: LiveData<Event<Boolean>> = _meetingRequestSentEvent

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _sendNotesEvent: MutableLiveData<Result<Unit>> = MutableLiveData()
    val sendNotesEvent: LiveData<Result<Unit>> = _sendNotesEvent

    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    private val _viewPagerVisibilityEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val viewPagerVisibilityEvent: LiveData<Event<Boolean>> = _viewPagerVisibilityEvent

    private val _multipleMeetingSentErrorEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val multipleMeetingSentErrorEvent: LiveData<Event<String>> = _multipleMeetingSentErrorEvent

    private val _showItineraryFragmentEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val showItineraryFragmentEvent: LiveData<Event<Boolean>> = _showItineraryFragmentEvent

    init {
        setDayTimeSpinnerData()
    }

    fun getEventProgramDetails() {
        Timber.e("----------getEventProgramDetails")
        Timber.e("----------"+ Common.login_data?.data?.id)
        Timber.e("----------"+authStore.event?.eventId)
        _eventState.value = Result.Loading

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getMyItineraryData(
                                eventId = authStore.event?.eventId ?: "",
                                userId = userId
                        )
//                        Timber.e("----------response" + response.data.toString())
//                        Timber.e("----------response" + response.status.toString())
                         if (response.status == ResponseStatus.SUCCESS) {

                            response.data?.let { programmeDetailResponse ->

                                val tempItineraryDetails = mutableListOf<MyItineraryUiModel>()
                                val tempAvailabilityItems = mutableListOf<CheckAvailabilityUiModel>()

//                                val tempSpinnerDaysData = mutableListOf<EventDaysUiModel>()
//                                val tempSpinnerTimeData: MutableList<MutableList<EventTimeSlotUiModel>> = mutableListOf()

                                programmeDetailResponse.programmeDetails?.forEach {
                                    it?.let { programDay ->
                                        tempItineraryDetails.add(mapToMyItineraryUiModel(programDay))
                                        val daysUiModel = EventDaysUiModel(
                                                date = programDay.date ?: "",
                                                day = programDay.day ?: "",
                                                isSelected = false
                                        )
                                        tempAvailabilityItems.add(
                                                CheckAvailabilityDateUiModel(
                                                        dayModel = daysUiModel
                                                )
                                        )
                                        tempAvailabilityItems.addAll(
                                                mapToMyAvailabilityUiModel(programDay.programmeItems
                                                        ?: listOf(), daysUiModel)
                                        )

//                                        Now we get this data on current_summit_event api

//                                        if (programDay.programmeItems?.isNotEmpty() == true) {
//                                            val timeSlotsForSingleDay = programDay.programmeItems
//                                                    .filterNotNull()
//                                                    .filter { it.canRequestMeeting == RequestMeeting.YES.value }
//                                                    .map {
//                                                        EventTimeSlotUiModel(
//                                                                programDetailsId = it.id ?: "",
//                                                                time = it.startEndTime ?: ""
//                                                        )
//                                                    }.toMutableList()
//
//                                            tempSpinnerTimeData.add(timeSlotsForSingleDay)
//                                            tempSpinnerDaysData.add(daysUiModel)
//                                        }

                                    }

                                }

//                                _spinnerTimeData.value = tempSpinnerTimeData
//                                _spinnerDaysData.value = tempSpinnerDaysData


                                val tempDays = tempItineraryDetails.map {
                                    it.dayData
                                }

                                isDataUpdated = true

                                tempDays.getOrNull(lastSelectedDayIndex)?.isSelected = true

//                                setDayTimeSpinnersData(tempAvailabilityItems)


                                Timber.e("Size of time slots ${_spinnerTimeData.value?.size}")
                                _daysData.value = tempDays

                                _myItineraryList.value = tempItineraryDetails
                                _myItinerary.value = _myItineraryList.value?.getOrNull(lastSelectedDayIndex)
                                _myAvailability.value = tempAvailabilityItems

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message
                                )

                            } ?: run {
                                _daysData.value = listOf()
                                _eventState.value = Result.Error(
                                        message = response.message ?: Common.noRecordFoundMessage
                                )
//                                _messageEvent.value = Event(response.message ?: Common.noRecordFoundMessage)
                            }
                            getAllDelegates()

                        } else {
//                            _daysData.value = listOf()
                            Timber.e("----------somethingWentWrongMessageELSE")
                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
//                        _daysData.value = listOf()
                        Timber.e("----------somethingWentWrongMessageerror")
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    private fun getAllDelegates() {
        _eventState.value = Result.Loading
        allDelegates = listOf()
        _selectedDelegates.value = listOf()

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getDelegates(
                                searchText = "",
                                eventId = authStore.event?.eventId ?: "",
                                userId = userId
                        )

                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->

                                allDelegates = mapToPersonDetailUiModel(data.all)

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
                                )

                            } ?: run {
                                Timber.e(response.message)
                                allDelegates = listOf()
                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
                                )
                            }


                        } else {
                            Timber.e(response.message)
                            _eventState.value = Result.Error(
                                    message = if (response.message.isNullOrEmpty()) Common.somethingWentWrongMessage else response.message
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        Timber.e(it)
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }


//        viewModelScope.launchSafely(
//                block = {
//                    val tempList = mutableListOf<PersonDetailUiModel>()
//                    for (i in 0..30) {
//                        tempList.add(PersonDetailUiModel(
//                                name = "Sibtein $i",
//                                designation = "Founder",
//                                twitter = "",
//                                photoUrl = "",
//                                linkedIn = "",
////                                availableSlotData = listOf(),
//                                facebook = "",
//                                userId = i.toString(),
//                                company = "",
//                                country = "",
//                                longDescription = "",
//                                shortDescription = "",
//                                isSelected = false,
//                                summitEventsFacultyId = ""
//                        ))
//                    }
//
//                    allPersons = tempList
//                },
//                error = {
//
//                }
//        )
    }

    fun updateAllPersonComingInEvent() {
        allDelegates = authStore.delegatesToSelect
        _selectedDelegates.value = allDelegates?.filter {
            it.isSelected
        }
    }

    private fun setDayTimeSpinnerData() {
        selectedTimeSlot = null
        val meetingSlotData = authStore.event?.meetingSlots
        val tempListDay = mutableListOf<EventDaysUiModel>()
        val tempListTime = mutableListOf<List<EventTimeSlotUiModel>>()

        meetingSlotData?.forEach { item ->

            if (item.slots?.isEmpty() == false) {
                tempListDay.add(
                        EventDaysUiModel(
                                isSelected = false,
                                date = item.programDate ?: "",
                                day = item.programDay ?: ""
                        )
                )
                val timeSlots = item.slots.map { slot ->
                    EventTimeSlotUiModel(
                            programDetailsId = slot.programId ?: "",
                            time = slot.time ?: ""
                    )
                }

                tempListTime.add(timeSlots)
            }

        }

        _spinnerTimeData.value = tempListTime
        _spinnerDaysData.value = tempListDay
    }


    private fun setDayTimeSpinnersData(checkAvailabilityItems: List<CheckAvailabilityUiModel>) {
        selectedTimeSlot = null
        val tempSpinnerTimeSlots = mutableListOf<List<EventTimeSlotUiModel>>()
        val tempSpinnerDaySlots = mutableListOf<EventDaysUiModel>()

        val daysList = checkAvailabilityItems.filterIsInstance<CheckAvailabilityDateUiModel>()

        daysList.forEach { headerModel ->
            val availabilityItemUiModelList = checkAvailabilityItems.filterIsInstance<CheckAvailabilityItemUiModel>().filter {
                it.dayModel.day == headerModel.dayModel.day && it.status == CheckAvailabilityStatus.YES.value
            }
            if (availabilityItemUiModelList.isNotEmpty()) {
                tempSpinnerTimeSlots.add(availabilityItemUiModelList.map { item ->
                    EventTimeSlotUiModel(
                            programDetailsId = item.programDetailsId,
                            time = item.time
                    )
                })
                tempSpinnerDaySlots.add(headerModel.dayModel)
            }
        }

        _spinnerTimeData.value = tempSpinnerTimeSlots
        _spinnerDaysData.value = tempSpinnerDaySlots

    }


    fun acceptOrRejectRequest(accept: Boolean, model: MyItineraryRequestItemUiModel) {

        if (accept) {
            updateMeetingStatus(
                    item = model,
                    meetingNotes = model.meetingNotes,
                    meetingStatus = MeetingRequestStatusTypes.MEETING_ACCEPTED.value
            )

        } else {
            updateMeetingStatus(
                    item = model,
                    meetingNotes = model.meetingNotes,
                    meetingStatus = MeetingRequestStatusTypes.MEETING_REJECTED.value
            )
        }

    }

    private fun updateMeetingStatus(item: MyItineraryRequestItemUiModel, meetingStatus: String, meetingNotes: String) {
        _eventState.value = Result.Loading

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.setSummitEventMeetingRequestStatus(
                                requestedToId = item.requestedTo,
                                requestedById = item.requestedBy,
                                userId = userId,
                                meetingStatus = meetingStatus,
                                meetingNotes = meetingNotes,
                                programDetailsId = item.programDetailsId,
                                isUpdate = true
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            clickedSessionPosition = -1
                            response.data?.let { data ->
                                if (data.isError == false) {
                                    val tempList = _myItinerary.value?.myItineraryItems?.toMutableList()

                                    tempList?.let {
                                        val index = tempList.indexOf(item)
                                        if (index != -1) {
                                            tempList.removeAt(index)
                                            val newModel = getUpdatedMeetingRequestModel(item, data.updatedStatus, item.meetingNotes)

                                            newModel?.let {
                                                tempList.add(index, newModel)
                                            }

                                            val sessionModelIndex = tempList.indexOfFirst {
                                                it.programDetailsId == item.programDetailsId && it.type == MyItineraryItemType.SESSION
                                            }
                                            if (sessionModelIndex != -1) {
                                                val count = tempList.filterIsInstance<MyItineraryRequestItemUiModel>().count {
                                                    it.programDetailsId == item.programDetailsId
                                                }

                                                val model = tempList.removeAt(sessionModelIndex) as MyItinerarySessionUiModel
                                                tempList.add(
                                                        sessionModelIndex,
                                                        model.copy(
                                                                meetingRequestCount = count
                                                        )
                                                )

                                            }

                                            val itineraryListIndex = _myItineraryList.value?.indexOfFirst {
                                                it.dayData.day == _myItinerary.value?.dayData?.day
                                            }

                                            itineraryListIndex?.let {

                                                _myItinerary.value = _myItinerary.value?.copy(
                                                        myItineraryItems = tempList
                                                )

                                                val allDaysItineraryList = _myItineraryList.value?.toMutableList()
                                                allDaysItineraryList?.removeAt(itineraryListIndex)
                                                allDaysItineraryList?.add(itineraryListIndex, _myItinerary.value!!)
                                                _myItineraryList.value = allDaysItineraryList
                                            }

                                        }
                                    }

                                    val tempAvailability = _myAvailability.value

                                    //Update Availability Item
                                    tempAvailability?.forEach {
                                        if (it is CheckAvailabilityItemUiModel) {
                                            if (item.programDetailsId == it.programDetailsId) {
                                                it.lastMeetingStatus = data.updatedStatus
                                            }
                                        }
                                    }
                                    _myAvailability.value = tempAvailability

//                                _myAvailability.value?.let {
//                                    setDayTimeSpinnersData(it)
//                                }
                                    _eventState.value = Result.Success(message = "", data = Unit)


                                } else {
                                    _messageEvent.value = Event(response.message ?: "")
                                    _eventState.value = Result.Error(message = "")
                                }

                            } ?: run {
                                _messageEvent.value = Event(response.message
                                        ?: Common.somethingWentWrongMessage)
                                _eventState.value = Result.Error(message = "")
                            }


                        } else {
                            _eventState.value = Result.Error(message = "")
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }

                    },
                    error = {
                        _eventState.value = Result.Error(message = "")
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error("")
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    fun updateMeetingNotes(item: MyItineraryRequestItemUiModel, meetingNotes: String) {
        _sendNotesEvent.value = Result.Loading

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.setSummitEventMeetingRequestStatus(
                                requestedToId = item.requestedTo,
                                requestedById = item.requestedBy,
                                meetingStatus = item.meetingStatus,
                                meetingNotes = meetingNotes,
                                programDetailsId = item.programDetailsId,
                                userId = userId,
                                isUpdate = true
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            clickedSessionPosition = -1
                            response.data?.let { data ->
                                if (data.isError == false) {
                                    val tempList = _myItinerary.value?.myItineraryItems?.toMutableList()

                                    tempList?.let {
                                        val index = tempList.indexOf(item)
                                        if (index != -1) {
                                            tempList.removeAt(index)
                                            val newModel = getUpdatedMeetingRequestModel(item, data.updatedStatus, meetingNotes)

                                            newModel?.let {
                                                tempList.add(index, newModel)
                                            }

                                            val itineraryListIndex = _myItineraryList.value?.indexOfFirst {
                                                it.dayData.day == _myItinerary.value?.dayData?.day
                                            }

                                            itineraryListIndex?.let {

                                                _myItinerary.value = _myItinerary.value?.copy(
                                                        myItineraryItems = tempList
                                                )

                                                val allDaysItineraryList = _myItineraryList.value?.toMutableList()
                                                allDaysItineraryList?.removeAt(itineraryListIndex)
                                                allDaysItineraryList?.add(itineraryListIndex, _myItinerary.value!!)
                                                _myItineraryList.value = allDaysItineraryList
                                            }

                                        }
                                    }
                                    _sendNotesEvent.value = Result.Success(message = "", data = Unit)
                                    _messageEvent.value = Event(response.message ?: "")

                                } else {
                                    _sendNotesEvent.value = Result.Error(message = "")
                                    _messageEvent.value = Event(response.message ?: "")
                                }

                            } ?: run {
                                _sendNotesEvent.value = Result.Error(message = "")
                                _messageEvent.value = Event(response.message
                                        ?: Common.somethingWentWrongMessage)
                            }


                        } else {
                            _sendNotesEvent.value = Result.Error(message = "")
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }

                    },
                    error = {
                        _sendNotesEvent.value = Result.Error(message = "")
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _sendNotesEvent.value = Result.Error(message = "")
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }

    fun sendMultipleRequests(meetingNotes: String) {
        _eventState.value = Result.Loading
        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.setSummitEventMeetingRequestStatus(
                                requestedToId = _selectedDelegates.value?.joinToString(separator = ",") { it.summitEventsFacultyId }
                                        ?: "",
                                requestedById = authStore.event?.facultyId ?: "",
                                meetingStatus = MeetingRequestStatusTypes.MEETING_REQUESTED.value,
                                meetingNotes = meetingNotes,
                                programDetailsId = selectedTimeSlot?.programDetailsId ?: "",
                                userId = userId,
                                isUpdate = false
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->


                            }
                            _eventState.value = Result.Success(data = Unit, message = "")
                            _messageEvent.value = Event(response.message ?: "")
                            getEventProgramDetails()
                            _selectedDelegates.value = listOf()
                            _showItineraryFragmentEvent.value = Event(true)

                        } else {
                            _eventState.value = Result.Error(message = "")
                            _multipleMeetingSentErrorEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)

//                            _messageEvent.value = Event(response.message
//                                    ?: Common.somethingWentWrongMessage)
                        }

                    },
                    error = {
                        _eventState.value = Result.Error(message = "")
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(message = "")
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }
    }


    private fun getUpdatedMeetingRequestModel(item: MyItineraryRequestItemUiModel, updatedStatus: String?, meetingNotes: String): MyItineraryItemUiModel? {

        val type = getViewTypeForMeetingRequest(updatedStatus, item.isReceived)
        return if (type == MyItineraryItemType.SESSION) {
            null
//            MyItinerarySessionUiModel(
//                    programDetailsId = item.programDetailsId,
//                    isChecked = false,
//                    sessionType = item.sessionType,
//                    time = item.time,
//                    title = item.title,
//                    checkMyAvailabilityIsAvailable = item.checkMyAvailabilityIsAvailable,
//                    canRequestMeeting = item.canRequestMeeting,
//                    sessionsList = item.sessionsList,
//                    isExpanded = true
//            )
        } else {
            item.copy(
                    type = type,
                    meetingNotes = meetingNotes,
                    meetingStatus = updatedStatus ?: item.meetingStatus
            )
        }

    }

    private fun getViewTypeForMeetingRequest(status: String?, isReceived: Boolean?): MyItineraryItemType {

        return if (status == MeetingRequestStatusTypes.MEETING_ACCEPTED.value) {
            MyItineraryItemType.REQUEST_ACCEPTED
        } else if (status == MeetingRequestStatusTypes.MEETING_REQUESTED.value && isReceived == true) {
            MyItineraryItemType.REQUEST_RECEIVED
        } else if (status == MeetingRequestStatusTypes.MEETING_REQUESTED.value && isReceived == false) {
            MyItineraryItemType.REQUEST_SENT
        } else {
            MyItineraryItemType.SESSION
        }
    }

    private fun mapToMyAvailabilityUiModel(list: List<ProgrammeItemResponse?>, dayModel: EventDaysUiModel): List<CheckAvailabilityItemUiModel> {
        return list.mapNotNull {
            it?.let {
                CheckAvailabilityItemUiModel(
                        time = it.startEndTime ?: "",
                        status = if (it.canRequestMeeting == RequestMeeting.NO.value) CheckAvailabilityStatus.NOT_AVAILABLE.value
                        else it.checkMyAvailabilityIsAvailable,
                        programDetailsId = it.id ?: "",
                        dayModel = dayModel,
//                        lastMeetingStatus = it.meetingRequestDetails?.meetingRequestStatus?.firstOrNull()?.status,
//                        Randomly setting request meeting status
                        lastMeetingStatus = MeetingRequestStatusTypes.MEETING_CANCELED.value,
                        sessionType = it.sessionType ?: "",
                        hasJoinedABreakoutSession = it.sessionDetails?.firstOrNull { it?.amIEnrolled == true }?.amIEnrolled
                                ?: false
                )
            }
        }
    }

    private fun mapToMyItineraryUiModel(responseData: ProgrammeDayResponse): MyItineraryUiModel {
        val eventDay = EventDaysUiModel(
                date = responseData.date ?: "",
                isSelected = false,
                day = responseData.day ?: ""
        )

        val myItineraryItems = mutableListOf<MyItineraryItemUiModel>()
        responseData.programmeItems?.mapNotNull {
            it?.let {
                myItineraryItems.addAll(mapToMyItineraryItemUiModelList(data = it))
//                mapToMyItineraryItemUiModel(it)

            }
        }

        return MyItineraryUiModel(
                dayData = eventDay,
                myItineraryItems = myItineraryItems
        )
    }

    private fun mapToMyItineraryItemUiModelList(data: ProgrammeItemResponse): List<MyItineraryItemUiModel> {
        val list = mutableListOf<MyItineraryItemUiModel>()
        val sessionItem = MyItinerarySessionUiModel(
                time = data.startEndTime ?: "",
                title = data.title ?: "",
                isChecked = false,
                sessionType = data.sessionType ?: "",
                programDetailsId = data.id ?: "",
                checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
                canRequestMeeting = data.canRequestMeeting ?: "",
                sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id),
                isExpanded = false,
                meetingRequestCount = data.meetingRequestDetails?.filter {
                    it?.meetingRequestStatus?.firstOrNull()?.status == MeetingRequestStatusTypes.MEETING_REQUESTED.value ||
                            it?.meetingRequestStatus?.firstOrNull()?.status == MeetingRequestStatusTypes.MEETING_ACCEPTED.value
                }?.size ?: 0
        )
        list.add(sessionItem)

        data.meetingRequestDetails?.filterNotNull()?.forEach {
            if (
                    it.meetingRequestStatus?.firstOrNull()?.status != null &&
                    it.meetingRequestStatus.firstOrNull()?.status != MeetingRequestStatusTypes.MEETING_REJECTED.value &&
                    it.meetingRequestStatus.firstOrNull()?.status != MeetingRequestStatusTypes.MEETING_CANCELED.value
            ) {
                list.add(
                        MyItineraryRequestItemUiModel(
                                title = sessionItem.title,
                                sessionType = sessionItem.sessionType,
                                time = sessionItem.time,
                                programDetailsId = sessionItem.programDetailsId,
                                designation = if (authStore.event?.facultyId == it.requestedToId)
                                    (it.requestedByDetails?.designation
                                            ?: "") else
                                    (it.requestedToDetails?.designation ?: ""),
                                name = if (authStore.event?.facultyId == it.requestedToId)
                                    (it.requestedByDetails?.name ?: "") else
                                    (it.requestedToDetails?.name ?: ""),
                                sessionsList = sessionItem.sessionsList,
                                canRequestMeeting = sessionItem.canRequestMeeting,
                                meetingNotes = it.meetingNotes ?: "",
                                meetingStatus = it.meetingRequestStatus.firstOrNull()?.status ?: "",
                                requestedBy = it.requestedById ?: "",
                                requestedTo = it.requestedToId ?: "",
                                checkMyAvailabilityIsAvailable = sessionItem.checkMyAvailabilityIsAvailable,
                                isReceived = it.isReceived ?: false,
                                type = getViewTypeForMeetingRequest(
                                        status = it.meetingRequestStatus.firstOrNull()?.status
                                                ?: "",
                                        isReceived = it.isReceived ?: false
                                )
                        )
                )
            }
        }
        return list
    }

//    private fun mapToMyItineraryItemUiModel(data: ProgrammeItemResponse): MyItineraryItemUiModel {
////        return if (data.sessionType == SessionType.PLENARY.value) {
//        return if (false) {
//            if (data.canRequestMeeting == RequestMeeting.NO.value) {
//                MyItineraryPlenarySessionItemUiModel(
//                        programDetailsId = data.id ?: "",
//                        title = data.sessionDetails?.firstOrNull()?.title ?: "",
//                        speakers = mapToPersonDetailUiModel(data.speakers),
//                        delegates = mapToPersonDetailUiModel(data.delegates),
//                        description = data.sessionDetails?.firstOrNull()?.description ?: "",
//                        isExpanded = false,
//                        time = data.startEndTime ?: ""
//                )
//
//            } else if (data.checkMyAvailabilityIsAvailable != CheckAvailabilityStatus.YES.value) {
//                MyItinerarySessionUiModel(
//                        time = data.startEndTime ?: "",
//                        title = data.title ?: "",
//                        isChecked = false,
//                        sessionType = data.sessionType ?: "",
//                        programDetailsId = data.id ?: "",
//                        checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                        canRequestMeeting = data.canRequestMeeting ?: "",
//                        sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id)
//                )
//            }
////        else if (data.meetingRequestDetails == null ||
////                data.meetingRequestDetails.meetingRequestStatus?.firstOrNull()?.status == MeetingRequestStatusTypes.MEETING_CANCELED.value ||
////                data.meetingRequestDetails.meetingRequestStatus?.firstOrNull()?.status == MeetingRequestStatusTypes.MEETING_REJECTED.value
////         ){
////             MyItineraryMeetingSlotItemUiModel(
////                     time = data.startEndTime ?: "",
////                     isChecked = false,
////                     programDetailsId = data.id ?: "",
////                     checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable
////             )
////
////
////        }
//            else {
//                val type = getViewTypeForMeetingRequest(
//                        status = data.meetingRequestDetails?.meetingRequestStatus?.firstOrNull()?.status,
//                        isReceived = data.meetingRequestDetails?.isReceived
//                )
//
//                if (type == MyItineraryItemType.MEETING_SLOT) {
//                    MyItinerarySessionUiModel(
//                            time = data.startEndTime ?: "",
//                            title = data.title ?: "",
//                            isChecked = false,
//                            programDetailsId = data.id ?: "",
//                            sessionType = data.sessionType ?: "",
//                            checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                            sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id),
//                            canRequestMeeting = data.canRequestMeeting ?: ""
//                    )
//                } else {
//                    MyItineraryRequestItemUiModel(
//                            programDetailsId = data.id ?: "",
//                            time = data.startEndTime ?: "",
//                            designation = if (Common.login_data?.data?.id == data.meetingRequestDetails?.requestedToId)
//                                (data.meetingRequestDetails?.requestedByDetails?.designation
//                                        ?: "") else
//                                (data.meetingRequestDetails?.requestedToDetails?.designation ?: ""),
//                            name = if (Common.login_data?.data?.id == data.meetingRequestDetails?.requestedToId)
//                                (data.meetingRequestDetails?.requestedByDetails?.name ?: "") else
//                                (data.meetingRequestDetails?.requestedToDetails?.name ?: ""),
//                            meetingNotes = data.meetingRequestDetails?.meetingNotes ?: "",
//                            type = type,
//                            isReceived = data.meetingRequestDetails?.isReceived ?: false,
//                            checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                            requestedBy = data.meetingRequestDetails?.requestedById ?: "",
//                            requestedTo = data.meetingRequestDetails?.requestedToId ?: "",
//                            meetingStatus = data.meetingRequestDetails?.meetingRequestStatus?.firstOrNull()?.status
//                                    ?: "",
//                            canRequestMeeting = data.canRequestMeeting ?: "",
//                            sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id),
//                            sessionType = data.sessionType ?: "",
//                            title = data.title ?: ""
//
//                    )
//                }
//
//            }
//        } else {
//            if (data.checkMyAvailabilityIsAvailable != CheckAvailabilityStatus.YES.value) {
//                MyItinerarySessionUiModel(
//                        time = data.startEndTime ?: "",
//                        title = data.title ?: "",
//                        isChecked = false,
//                        sessionType = data.sessionType ?: "",
//                        programDetailsId = data.id ?: "",
//                        canRequestMeeting = data.canRequestMeeting ?: "",
//                        checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                        sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id)
//                )
//            } else {
//                val type = getViewTypeForMeetingRequest(
//                        status = data.meetingRequestDetails?.meetingRequestStatus?.firstOrNull()?.status,
//                        isReceived = data.meetingRequestDetails?.isReceived
//                )
//
//                if (type == MyItineraryItemType.MEETING_SLOT) {
//                    MyItinerarySessionUiModel(
//                            time = data.startEndTime ?: "",
//                            title = data.title ?: "",
//                            isChecked = false,
//                            programDetailsId = data.id ?: "",
//                            sessionType = data.sessionType ?: "",
//                            checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                            canRequestMeeting = data.canRequestMeeting ?: "",
//                            sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id)
//
//                    )
//                } else {
//                    MyItineraryRequestItemUiModel(
//                            programDetailsId = data.id ?: "",
//                            time = data.startEndTime ?: "",
//                            designation = if (Common.login_data?.data?.id == data.meetingRequestDetails?.requestedToId)
//                                (data.meetingRequestDetails?.requestedByDetails?.designation
//                                        ?: "") else
//                                (data.meetingRequestDetails?.requestedToDetails?.designation ?: ""),
//                            name = if (Common.login_data?.data?.id == data.meetingRequestDetails?.requestedToId)
//                                (data.meetingRequestDetails?.requestedByDetails?.name ?: "") else
//                                (data.meetingRequestDetails?.requestedToDetails?.name ?: ""),
//                            meetingNotes = data.meetingRequestDetails?.meetingNotes ?: "",
//                            type = type,
//                            isReceived = data.meetingRequestDetails?.isReceived ?: false,
//                            checkMyAvailabilityIsAvailable = data.checkMyAvailabilityIsAvailable,
//                            requestedBy = data.meetingRequestDetails?.requestedById ?: "",
//                            requestedTo = data.meetingRequestDetails?.requestedToId ?: "",
//                            meetingStatus = data.meetingRequestDetails?.meetingRequestStatus?.firstOrNull()?.status
//                                    ?: "",
//                            sessionsList = mapToBreakoutSessionUiModel(data.sessionDetails, data.id),
//                            sessionType = data.sessionType ?: "",
//                            canRequestMeeting = data.canRequestMeeting ?: "",
//                            title = data.title ?: ""
//
//                    )
//                }
//
//            }
//        }
//
//    }


    fun updateAvailability(item: CheckAvailabilityItemUiModel) {

        Common.login_data.data.id?.let { userId ->
            _eventState.value = Result.Loading
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.setSummitEventProgramAvailability(
                                userId = userId,
                                isAvailable = if (item.status == CheckAvailabilityStatus.NO.value || item.status == null)
                                    CheckAvailabilityStatus.YES.value
                                else CheckAvailabilityStatus.NO.value,
                                programDetailsId = item.programDetailsId
                        )

                        if (response.status == ResponseStatus.SUCCESS) {

                            val tempList = _myAvailability.value?.toMutableList()
                            tempList?.let {

                                val index = tempList.indexOf(item)
                                if (index != -1) {

                                    var newStatus: String? = null
                                    when (item.status) {
                                        CheckAvailabilityStatus.YES.value -> {
                                            newStatus = CheckAvailabilityStatus.NO.value
                                        }
                                        CheckAvailabilityStatus.NO.value -> {
                                            newStatus = CheckAvailabilityStatus.YES.value
                                        }
                                        null -> {
                                            newStatus = CheckAvailabilityStatus.YES.value
                                        }
                                    }
                                    val newModel = item.copy(
                                            status = newStatus
                                    )
                                    tempList.removeAt(index)
                                    tempList.add(index, newModel)


                                    _myAvailability.value = tempList

//                                    setDayTimeSpinnersData(tempList)


                                    _eventState.value = Result.Success(
                                            message = "",
                                            data = Unit
                                    )
                                    _messageEvent.value = Event(response.message ?: "")
                                }

                            }


                        } else {
                            _eventState.value = Result.Error(
                                    message = ""
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }


                    },
                    error = {
                        _eventState.value = Result.Error(
                                message = ""
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }
    }

    private fun mapToPersonDetailUiModel(delegates: List<PersonDetailResponse?>?): List<PersonDetailUiModel> {
        return delegates?.mapNotNull {
            it?.let {
                PersonDetailUiModel(
                        userId = it.id ?: "",
                        summitEventsFacultyId = it.summitEventsFacultyId ?: "",
                        name = it.name ?: "",
                        shortDescription = it.shortDescription ?: "",
                        longDescription = it.longDescription ?: "",
                        twitter = it.twitterLink ?: "",
                        linkedIn = it.linkedInLink ?: "",
                        facebook = it.facebookLink ?: "",
                        photoUrl = it.userImage ?: "",
                        designation = it.designation ?: "",
                        company = "",
                        country = "",
//                        availableSlotData = it.meetingSlots,
                        isSelected = false,
                        isSpeaker = it.isSpeaker == "1",
                        isDelegate = it.isDelegate == "1"
                )
            }

        } ?: listOf()
    }

    fun toggleIsExpanded(item: MyItinerarySessionUiModel) {
        val tempList = _myItinerary.value?.myItineraryItems?.toMutableList()

        tempList?.let {
            val index = tempList.indexOf(item)
            if (index != -1) {
                clickedSessionPosition = index
                tempList.removeAt(index)
                val newModel = item.copy(
                        isExpanded = !item.isExpanded
                )

                tempList.add(index, newModel)

                val itineraryListIndex = _myItineraryList.value?.indexOfFirst {
                    it.dayData.day == _myItinerary.value?.dayData?.day
                }

                itineraryListIndex?.let {

                    _myItinerary.value = _myItinerary.value?.copy(
                            myItineraryItems = tempList
                    )

                    val allDaysItineraryList = _myItineraryList.value?.toMutableList()
                    allDaysItineraryList?.removeAt(itineraryListIndex)
                    allDaysItineraryList?.add(itineraryListIndex, _myItinerary.value!!)
                    _myItineraryList.value = allDaysItineraryList
                }

            }
        }
    }

    private fun mapToBreakoutSessionUiModel(list: List<SessionDetailResponse?>?, programDetailsId: String?): List<SessionItemUiModel> {

        return list?.filterNotNull()?.map {
            SessionItemUiModel(
                    sessionId = it.id ?: "",
                    programDetailsId = programDetailsId ?: "",
                    title = it.title ?: "",
                    description = it.description ?: "",
                    iAmEnrolled = it.amIEnrolled ?: false,
                    delegatesEnrolled = mapToPersonDetailUiModel(it.enrolledDelegateSpeakers),
                    roomLimit = it.roomLimit ?: 0,
                    zoomMeetingUrl = it.zoomLink
                            ?: "https://stackoverflow.com/questions/10979821/how-to-make-part-of-the-text-bold-in-android-at-runtime",
                    roomOccupied = it.roomOccupied ?: 0,
                    speakers = mapToPersonDetailUiModel(it.sessionFaculties?.speakers?.map { it.facultyDetails }),
                    delegates = mapToPersonDetailUiModel(it.sessionFaculties?.delegates?.map { it.facultyDetails })
            )
        } ?: listOf()
    }

    fun changeSelectedDay(day: String, position: Int) {
        clickedSessionPosition = -1
        lastSelectedDayIndex = position
        val tempList = _daysData.value
        tempList?.forEach {
            it.isSelected = false
        }
        tempList?.get(position)?.isSelected = true

        _daysData.value = tempList

        _myItinerary.value = _myItineraryList.value?.getOrNull(position)

        _dayClickEvent.value = Event(day)

    }

    fun requestMeetingCheckboxClick(programDetailsId: String) {
        clickedRequestMeetingProgramDetailsId = programDetailsId
        _meetingSlotClickEvent.value = Event(programDetailsId)
    }

    fun meetingRequestSent() {
        _meetingRequestSentEvent.value = Event(true)
    }

    fun enableSpinners() {
        _enableSpinnersEvent.value = Event(true)
    }

    fun cancelMeeting(item: MyItineraryRequestItemUiModel) {
        updateMeetingStatus(item, MeetingRequestStatusTypes.MEETING_CANCELED.value, item.meetingNotes)
    }

    fun toggleCheckAvailability() {
        _checkAvailabilityToggle.value = Event(true)
    }

    fun changeViewPagerVisibility(visible: Boolean) {
        _viewPagerVisibilityEvent.value = Event(visible)
    }

}