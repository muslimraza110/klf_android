package com.khojaleadership.KLF.Activities.Feedback;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Feedback_Models.FeedbackRequestModel;
import com.khojaleadership.KLF.Model.Feedback_Models.FeedbackResponseModel;
import com.khojaleadership.KLF.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Feedback_Activity extends AppCompatActivity {

    EditText title,feedback;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Feedback");



        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v,Feedback_Activity.this);

                finish();
            }
        });

        loading_dialog = Common.LoadingDilaog(this);

        title=(EditText)findViewById(R.id.feedback_title);
        feedback=(EditText)findViewById(R.id.feedback_detail);

        //submit feedback
        findViewById(R.id.submit_feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                loading_dialog.show();
//                FeedbackFunction();

                if (title.getText().toString().isEmpty() ||
                        title.getText().toString().equalsIgnoreCase("")) {
                    title.setError("This field should not be empty.");
                } else if (feedback.getText().toString().isEmpty() ||
                        feedback.getText().toString().equalsIgnoreCase("")) {
                    feedback.setError("This field should not be empty.");
                } else {
                    loading_dialog.show();
                    FeedbackFunction();
                }


            }
        });

    }

    private void FeedbackFunction() {

        FeedbackRequestModel request=new FeedbackRequestModel();
        request.setUser_id(Common.login_data.getData().getId());
        request.setName(title.getText().toString());
        request.setDescription(feedback.getText().toString());


        Call<FeedbackResponseModel> responseCall = retrofitInterface.Feedback("application/json",Common.auth_token,request);

        responseCall.enqueue(new Callback<FeedbackResponseModel>() {
            @Override
            public void onResponse(Call<FeedbackResponseModel> call, Response<FeedbackResponseModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    FeedbackResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    loading_dialog.dismiss();
                    if (status.equals("error")){
                        //data is null
                    }else if (status.equals("success")){
                        title.getText().clear();
                        feedback.getText().clear();
                        Toast.makeText(Feedback_Activity.this,"Feedback sent successfully.",Toast.LENGTH_SHORT).show();
                    }


                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<FeedbackResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Log.d("AddProject", "failure :" + t.getMessage());
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
