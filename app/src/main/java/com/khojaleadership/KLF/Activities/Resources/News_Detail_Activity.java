package com.khojaleadership.KLF.Activities.Resources;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Resource.DeleteResourcePostRequestModel;
import com.khojaleadership.KLF.Model.Resource.DeleteResourcePostResponseModel;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Data_Model;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Data_Model;
import com.khojaleadership.KLF.Model.Resource.Publish_News_Request_Model;
import com.khojaleadership.KLF.Model.Resource.Publish_News_Response_Model;
import com.khojaleadership.KLF.Model.Resource.Unpublish_News_Request_Model;
import com.khojaleadership.KLF.Model.Resource.Unpublish_News_Response_Model;
import com.khojaleadership.KLF.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class News_Detail_Activity extends AppCompatActivity {

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    TextView publish_btn, click_text;

    Boolean is_publish;

    ImageView news_img;
    TextView news_name;
    WebView news_webview;

    TextView header_title;

    LinearLayout publish_layout, news_detail_edit_btn, news_detail_delete_btn;

    Khoja_Summit_Data_Model KhojaData=new Khoja_Summit_Data_Model();
    News_Fragments_Data_Model NewsData=new News_Fragments_Data_Model();
    String isNewsFlag="-1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news__detail_);

        loading_dialog = Common.LoadingDilaog(this);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, News_Detail_Activity.this);
                finish();
            }
        });


        Intent intent= getIntent();
        KhojaData=intent.getParcelableExtra("KhojaData");
        NewsData=intent.getParcelableExtra("NewsData");

        Bundle bundle = getIntent().getExtras();

        if (bundle!=null) {
            isNewsFlag = bundle.getString("isNewsFlag");
        }else {
            isNewsFlag="-1";
        }



        loading_dialog.show();
        setting_data();

        publish_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loading_dialog.show();

                if (is_publish == false) {
                    PublishNews();
                    is_publish = true;

                } else if (is_publish == true) {
                    UnPublishNews();
                    //publish_btn.setText("Publish");
                    is_publish = false;
                }


            }
        });

        publish_layout = findViewById(R.id.publish_layout);
        news_detail_edit_btn = findViewById(R.id.news_detail_edit_btn);
        news_detail_delete_btn = findViewById(R.id.news_detail_delete_btn);

        //member ko show ni krwany
        if (Common.is_Admin_flag == false) {
            publish_layout.setVisibility(View.GONE);
            news_detail_delete_btn.setVisibility(View.GONE);
            news_detail_edit_btn.setVisibility(View.GONE);
        }

        news_detail_edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit;
                if (isNewsFlag.equals("1")) {
                    edit = new Intent(News_Detail_Activity.this, Edit_News_Activity.class);
                    edit.putExtra("NewsData", NewsData);
                    startActivity(edit);
                } else if (isNewsFlag.equals("0")) {
                    edit = new Intent(News_Detail_Activity.this, Edit_Khoja_Summit_Activity.class);
                    edit.putExtra("KhojaData",KhojaData);
                    startActivity(edit);
                }

            }
        });

        //delete button
        news_detail_delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isNewsFlag.equals("1")) {
                    //delete work here
                    Confirm_Dialog("Delete News", "Are you sure to delete this News?", "Yes");

                } else if (isNewsFlag.equals("0")) {
                    //delete work here
                    Confirm_Dialog("Delete Khoja Summit", "Are you sure to delete this Khoja summit?", "Yes");

                }

            }
        });

    }

    public void setting_data() {

        header_title = findViewById(R.id.c_header_title);
        news_img = findViewById(R.id.news_detail_img);
        news_name = findViewById(R.id.news_detail_name);
        news_webview = findViewById(R.id.news_detail_webview);

        publish_btn = (TextView) findViewById(R.id.news_publish_btn);
        click_text = findViewById(R.id.click_text);


//        if (Common.is_news_falg == true) {
        if (isNewsFlag.equals("1")) {
            //............for news detail.......................

            header_title.setText("News Detail");

//            if (Common.news_list_data.getIs_published().equals("0")) {
//                is_publish = false;
//                publish_btn.setText("Publish");
//                click_text.setText("Click here to publish");
//            } else if (Common.news_list_data.getIs_published().equals("1")) {
//                is_publish = true;
//                publish_btn.setText("Unpublish");
//                click_text.setText("Click here to publish");
//            }
//
//            //setting data
//            news_img.setBackground(null);
//            Glide.with(getApplicationContext()).load(Common.news_list_data.getProfileimage()).into(news_img);
//            news_name.setText(Common.news_list_data.getPosts__name());
//
//
//            WebSettings webSettings = news_webview.getSettings();
//            webSettings.setJavaScriptEnabled(true);
//            news_webview.loadData(Common.news_list_data.getPosts__content(), "text/html; charset=utf-8", "UTF-8");


            if (NewsData.getIs_published().equals("0")) {
                is_publish = false;
                publish_btn.setText("Publish");
                click_text.setText("Click here to publish");
            } else if (NewsData.getIs_published().equals("1")) {
                is_publish = true;
                publish_btn.setText("Unpublish");
                click_text.setText("Click here to publish");
            }

            //setting data
            news_img.setBackground(null);
            Glide.with(getApplicationContext()).load(NewsData.getProfileimage()).into(news_img);
            news_name.setText(NewsData.getPosts__name());


            WebSettings webSettings = news_webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            news_webview.loadData(NewsData.getPosts__content(), "text/html; charset=utf-8", "UTF-8");


//        } else if (Common.is_news_falg == false) {
        } else if (isNewsFlag.equals("0")) {
            //............for khoja summit detail.......................

            header_title.setText("Khoja Summit Detail");

//            if (Common.khoja_summit_list_data.getIs_published().equals("0")) {
//                is_publish = false;
//                publish_btn.setText("Publish");
//                click_text.setText("Click here to publish");
//            } else if (Common.khoja_summit_list_data.getIs_published().equals("1")) {
//                is_publish = true;
//                publish_btn.setText("Unpublish");
//                click_text.setText("Click here to publish");
//            }
//            news_name.setText(Common.khoja_summit_list_data.getPosts__name());
//
//
//            WebSettings webSettings = news_webview.getSettings();
//            webSettings.setJavaScriptEnabled(true);
//            news_webview.loadData(Common.khoja_summit_list_data.getPosts__content(), "text/html; charset=utf-8", "UTF-8");


            if (KhojaData.getIs_published().equals("0")) {
                is_publish = false;
                publish_btn.setText("Publish");
                click_text.setText("Click here to publish");
            } else if (KhojaData.getIs_published().equals("1")) {
                is_publish = true;
                publish_btn.setText("Unpublish");
                click_text.setText("Click here to publish");
            }
            news_name.setText(KhojaData.getPosts__name());

            WebSettings webSettings = news_webview.getSettings();
            webSettings.setJavaScriptEnabled(true);

            news_webview.loadData(KhojaData.getPosts__content(), "text/html; charset=utf-8", "UTF-8");
            news_webview.setVerticalScrollBarEnabled(true);
            news_webview.setHorizontalScrollBarEnabled(true);
            news_webview.setWebViewClient(new WebViewClient());

            Log.d("WebContentIssue","Content :"+KhojaData.getPosts__content());
            //news_webview.loadData(KhojaData.getPosts__content(), "text/html; charset=utf-8", "UTF-8");
            //news_webview.setH
        }


        loading_dialog.dismiss();
    }

    public void Confirm_Dialog(String title, String message, String btn) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(News_Detail_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                DeleteResourceFunction();

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void DeleteResourceFunction() {

        DeleteResourcePostRequestModel request = new DeleteResourcePostRequestModel();


        if (isNewsFlag.equals("1")) {
            request.setPost_id(NewsData.getPosts__id());
        } else if (isNewsFlag.equals("0")) {
            request.setPost_id(KhojaData.getPosts__id());
        }


        Call<DeleteResourcePostResponseModel> responseCall = retrofitInterface.DeleteResourcePost("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<DeleteResourcePostResponseModel>() {
            @Override
            public void onResponse(Call<DeleteResourcePostResponseModel> call, Response<DeleteResourcePostResponseModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    DeleteResourcePostResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")) {
                        //data is null
                    } else if (status.equals("success")) {

                        if (isNewsFlag.equals("1")) {
                            Toast.makeText(News_Detail_Activity.this, "News deleted Successfully.", Toast.LENGTH_SHORT).show();

                            Intent news = new Intent(News_Detail_Activity.this, News_Activity_updated.class);
                            news.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(news);
                            finish();
                        } else if (isNewsFlag.equals("0")) {
                            Toast.makeText(News_Detail_Activity.this, "Khoja Summit deleted Successfully.", Toast.LENGTH_SHORT).show();

                            Intent news = new Intent(News_Detail_Activity.this, Khoja_Summit_Activity.class);
                            news.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(news);
                            finish();
                        }


                    }


                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<DeleteResourcePostResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    private void PublishNews() {

        Publish_News_Request_Model request = new Publish_News_Request_Model();

        if (isNewsFlag.equals("1")) {
            request.setPost_id(NewsData.getPosts__id());
        } else if (isNewsFlag.equals("0")) {
            request.setPost_id(KhojaData.getPosts__id());
        }
        //request.setPost_id(Common.news_list_data.getPosts__id());

        Call<Publish_News_Response_Model> responseCall = retrofitInterface.PublishNews("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<Publish_News_Response_Model>() {
            @Override
            public void onResponse(Call<Publish_News_Response_Model> call, Response<Publish_News_Response_Model> response) {

                if (response.isSuccessful()) {
                    loading_dialog.dismiss();

                    String status, message;

                    Publish_News_Response_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(News_Detail_Activity.this, "News publish failed.", Toast.LENGTH_SHORT).show();

                    } else if (status.equals("success")) {

                        if (isNewsFlag.equals("1")) {
                            Toast.makeText(News_Detail_Activity.this, "News publish Successfully.", Toast.LENGTH_SHORT).show();
                            publish_btn.setText("Unpublish");

//                            Intent news = new Intent(News_Detail_Activity.this, News_Activity_updated.class);
//                            news.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(news);
//                            finish();
                        } else if (isNewsFlag.equals("0")) {
                            Toast.makeText(News_Detail_Activity.this, "Khoja summit publish Successfully.", Toast.LENGTH_SHORT).show();
                            publish_btn.setText("Unpublish");

//                            Intent news = new Intent(News_Detail_Activity.this, Khoja_Summit_Activity.class);
//                            news.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(news);
//                            finish();
                        }

                    }


                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(News_Detail_Activity.this, "News publish failed.", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<Publish_News_Response_Model> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(News_Detail_Activity.this, "News publish failed.", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void UnPublishNews() {

        Unpublish_News_Request_Model request = new Unpublish_News_Request_Model();
//        request.setPost_id(NewsData.getPosts__id());
        if (isNewsFlag.equals("1")) {
            request.setPost_id(NewsData.getPosts__id());
        } else if (isNewsFlag.equals("0")) {
            request.setPost_id(KhojaData.getPosts__id());
        }


        Call<Unpublish_News_Response_Model> responseCall = retrofitInterface.UnPublishNews("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<Unpublish_News_Response_Model>() {
            @Override
            public void onResponse(Call<Unpublish_News_Response_Model> call, Response<Unpublish_News_Response_Model> response) {

                if (response.isSuccessful()) {
                    loading_dialog.dismiss();

                    String status, message;

                    Unpublish_News_Response_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(News_Detail_Activity.this, "News unpublish failed.", Toast.LENGTH_SHORT).show();

                    } else if (status.equals("success")) {

                        if (isNewsFlag.equals("1")) {
                            Toast.makeText(News_Detail_Activity.this, "News unpublish Successfully.", Toast.LENGTH_SHORT).show();

                            publish_btn.setText("Publish");
//                            Intent news = new Intent(News_Detail_Activity.this, News_Activity_updated.class);
//                            startActivity(news);
//                            finish();

                        } else if (isNewsFlag.equals("0")) {
                            Toast.makeText(News_Detail_Activity.this, "Khoja summit unpublish Successfully.", Toast.LENGTH_SHORT).show();
                            publish_btn.setText("Publish");

//                            Intent news = new Intent(News_Detail_Activity.this, Khoja_Summit_Activity.class);
//                            startActivity(news);
//                            finish();
                        }


                    }


                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(News_Detail_Activity.this, "News unpublish failed.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Unpublish_News_Response_Model> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(News_Detail_Activity.this, "News unpublish failed.", Toast.LENGTH_SHORT).show();
 }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
