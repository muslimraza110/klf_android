package com.khojaleadership.KLF.Activities.event_new.personSelection

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore

class PersonSelectionViewModel(application: Application):  AndroidViewModel(application) {


    var allPersons: List<PersonDetailUiModel>? = null

    var searchText = ""

    private val _personSuggestions: MutableLiveData<List<PersonDetailUiModel>> = MutableLiveData()
    val personSuggestions: LiveData<List<PersonDetailUiModel>> = _personSuggestions

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    init {
        allPersons = authStore.delegatesToSelect
        showSuggestions("")
    }

    fun showSuggestions(searchString: String) {
        if(searchString.trim().isEmpty() || searchString.trim().length < 2){
            _personSuggestions.value = allPersons
            return
        }

        _personSuggestions.value = allPersons?.filter {
            it.name.toLowerCase().contains(searchString.trim().toLowerCase())
        }
    }

    fun toggleSelectionForItem(item: PersonDetailUiModel) {
        val tempList = allPersons?.toMutableList() ?: mutableListOf()
        val index = tempList.indexOf(item)

        if (index != -1){
            tempList.removeAt(index)
            tempList.add(index, item.copy(
                    isSelected = !item.isSelected
            ))
            allPersons = tempList.toList()
        }

        showSuggestions(searchText)
    }


}