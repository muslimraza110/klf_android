package com.khojaleadership.KLF.Activities.event_new.programme

import java.util.*

data class ProgrammeDetailsItemUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val programDetailsId: String,
        val time: String,
        val title: String,
        val description: String,
        var isExpanded: Boolean,
        val speakers: List<PersonDetailUiModel>,
        val delegates: List<PersonDetailUiModel>
)