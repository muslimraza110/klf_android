package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Adapter.Forum.Sub_Topic_Posts_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.ApproveCommentRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.ApproveCommentResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListDataCommentsModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListDataModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.LikeDislikeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.LikeDislikeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.SubcribeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.SubcribeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.UnSubscribeRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.UnSubscribeResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sub_Topic_Posts extends AppCompatActivity implements RecyclerViewClickListner {

    //Boolean like_flag=false,dislike_flag=false;


    String frequency = "";
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    Sub_Topic_Posts_Adapter adapter;

    ImageView subscribe_btn, post_reply_btn;
    RelativeLayout add_post_btn;

    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog subcribe_dialog;
    Dialog loading_dialog;

    TextView title;
    TextView header_title;
    RecyclerViewClickListner listner;

    Boolean subscribed_flag = false;
    Boolean Like_Flag = false, UnLike_Flag = false;


    //for pagination
    private int page_number = 1;
    private int item_count = 25;
    int total_api_item_count = 0;
    int final_id;


    Boolean is_post_deleted, is_post_approved;


    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();

    String Subtopic_Id, Category, IsFromDashBoard;

    GetPostListDataCommentsModel_Updated sub_topic_post_detail_updated=new GetPostListDataCommentsModel_Updated();

    ArrayList<GetPostListDataCommentsModel_Updated> post_list_comment_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__topic__posts);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        //getting data
        topic_data_detail = getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
            Subtopic_Id = bundle.getString("SubtopicId");

            IsFromDashBoard = bundle.getString("IsFromDashBoard");
            if (IsFromDashBoard == null) {
                IsFromDashBoard = "0"; // when data is not returning from dash board....default value is 0
            }

        } else {
            IsFromDashBoard = "0";
        }


//        ------------Exception handling------------------
//        try {


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Forum Post");

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Sub_Topic_Posts.this);

                if (IsFromDashBoard.equals("1")) {
                    finish();
                } else if (IsFromDashBoard.equals("0")) {
                    //Common.post_back_flag = true;
                    Intent i = new Intent(Sub_Topic_Posts.this, Sub_Topics.class);
                    i.putExtra("TopicDataDetail", topic_data_detail);
                    i.putExtra("Category", Category);
                    startActivity(i);
                    finish();
                }

            }
        });


        //setting title
        title = findViewById(R.id.sub_topic_post_title_text);

        recyclerView = (RecyclerView) findViewById(R.id.sub_topics_posts_recyclerview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        final_id = Integer.valueOf(Subtopic_Id);
        is_post_deleted = false;
        is_post_approved = false;
        onPostListBtnClick_Updated(final_id, false, false);


        // for pagination
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int pos = layoutManager.findLastCompletelyVisibleItemPosition();
                int numItems = adapter.getItemCount();

                if (total_api_item_count == item_count) {
                    if ((pos >= numItems - 1)) {

                        page_number++;
                        loading_dialog.show();
                        PerformPagination(final_id, false, false);

                    }
                }

            }
        });


        //floating action btn
        findViewById(R.id.floating_action_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Sub_Topic_Posts.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        subscribe_btn = (ImageView) findViewById(R.id.post_subscribe_btn);
        post_reply_btn = (ImageView) findViewById(R.id.post_reply_btn);
        add_post_btn = findViewById(R.id.add_post_btn);


        subscribe_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subscribed_flag == false) {
                    Subscribe_Dialog();
                } else if (subscribed_flag == true) {
                    loading_dialog.show();
                    onUnSubcribeBtnClick();
                }
            }
        });


        add_post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.addPost = true;
                Common.editPost = false;

                Intent edit_post = new Intent(Sub_Topic_Posts.this, Add_Post.class);
                edit_post.putExtra("SubtopicId", String.valueOf(final_id));
                edit_post.putExtra("TopicDataDetail", topic_data_detail);
                edit_post.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                edit_post.putExtra("IsFromDashBoard", "1");
                edit_post.putExtra("Category", Category);
                edit_post.putExtra("ModeratorFlag",post_list_comment_data.get(0).getForumPosts__moderated());
                startActivity(edit_post);
                finish();
            }
        });


        post_reply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.addPost = false;
                Common.editPost = false;


                Intent edit_post = new Intent(Sub_Topic_Posts.this, Add_Post.class);
                edit_post.putExtra("SubtopicId", String.valueOf(final_id));
                edit_post.putExtra("TopicDataDetail", topic_data_detail);
                edit_post.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                edit_post.putExtra("IsFromDashBoard", "1");
                edit_post.putExtra("Category", Category);
                edit_post.putExtra("ModeratorFlag",post_list_comment_data.get(0).getForumPosts__moderated());
                startActivity(edit_post);
                finish();
            }
        });


//            throw new RuntimeException("Run Time exception");
//        } catch (Exception e) {
//        }

    }

    public void intialize_view(final int id, final Boolean is_deleted, final boolean is_approved) {

//        ------------Exception handling------------------
        try {


            if (is_deleted == true || is_approved == true) {
                adapter.updateList(post_list_comment_data);
            } else if (is_deleted == false && is_approved == false) {
                listner = this;
                adapter = new Sub_Topic_Posts_Adapter(this, post_list_comment_data, listner);
                recyclerView.setAdapter(adapter);
                ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            }

            loading_dialog.dismiss();


            if (post_list_comment_data != null) {

                if (post_list_comment_data.get(0).getFrequency() != null) {
                    if (post_list_comment_data.get(0).getFrequency().equals("null")) {
                        //subscribe functionality here
                        subscribed_flag = false;
                    } else {
                        //unsubscribe functionality here
                        subscribed_flag = true;
                        subscribe_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_subscribe_green));
                    }
                }

                //agr visibilty A h orr user member h tu......
                if (post_list_comment_data.get(0).getForumPosts__visibility() != null) {
                    if (post_list_comment_data.get(0).getForumPosts__visibility().equals("A")) {
                        if (Common.is_Admin_flag == false) {
                            post_reply_btn.setVisibility(View.GONE);
                            add_post_btn.setVisibility(View.GONE);
                        }
                    }
                }

                title.setText(post_list_comment_data.get(0).getForumPosts__topic());

            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }


    private void PerformPagination(int id, final Boolean isPostDelete, final Boolean isPostApproved) {

        try {
            Call<GetPostListModel_Updated> call = retrofitInterface.getPostListUpdated("application/json", Common.auth_token, id, item_count, page_number, Integer.valueOf(Common.login_data.getData().getId()));

            call.enqueue(new Callback<GetPostListModel_Updated>() {
                @Override
                public void onResponse(Call<GetPostListModel_Updated> call, Response<GetPostListModel_Updated> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GetPostListDataModel_Updated postListData = new GetPostListDataModel_Updated();
                        GetPostListModel_Updated responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (responseModel.getMessage().equals("No more Page to be found")) {
                            Toast.makeText(Sub_Topic_Posts.this, "No more page found", Toast.LENGTH_SHORT).show();

                        } else {


                            postListData = responseModel.getData();
                            if (postListData != null) {

                                //static makes first post from API and added in comment List....jugaaar
                                GetPostListDataCommentsModel_Updated post1 = new GetPostListDataCommentsModel_Updated();

                                post1.setForumPosts__id(postListData.getForumPosts__id());
                                post1.setForumPosts__user_id(postListData.getForumPosts__user_id());
                                post1.setForumPosts__subforum_id(postListData.getForumPosts__subforum_id());
                                post1.setForumPosts__topic(postListData.getForumPosts__topic());
                                post1.setForumPosts__content(postListData.getForumPosts__content());
                                post1.setForumPosts__pending(postListData.getForumPosts__pending());
                                post1.setForumPosts__moderated(postListData.getForumPosts__moderated());
                                post1.setForumPosts__pinned(postListData.getForumPosts__pinned());
                                post1.setForumPosts__sod(postListData.getForumPosts__sod());
                                post1.setForumPosts__notify_users(postListData.getForumPosts__notify_users());
                                post1.setForumPosts__visibility(postListData.getForumPosts__visibility());
                                post1.setForumPosts__post_visibility(postListData.getForumPosts__post_visibility());
                                post1.setForumPosts__status(postListData.getForumPosts__status());
                                post1.setForumPosts__created(postListData.getForumPosts__created());
                                post1.setUsers__first_name(postListData.getUsers__first_name());
                                post1.setUsers__last_name(postListData.getUsers__last_name());
                                post1.setPosts(postListData.getPosts());
                                post1.setLikescount(postListData.getLikescount());
                                post1.setDislikescount(postListData.getDislikescount());
                                post1.setIslike(postListData.getIslike());
                                post1.setIsdislike(postListData.getIsdislike());
                                post1.setFrequency(postListData.getFrequency());
                                post1.setUsers__role(postListData.getUsers__role());
                                post1.setIs_moderator(postListData.getIs_moderator());
                                post1.setProfileImg__name(postListData.getProfileImg__name());
                                //...................................................

//                                if (responseModel.getMessage().equals("No more Page to be found")) {
//
//                                    if (topic_data_detail != null) {
//
//                                        if (topic_data_detail.getSubforums__id() != null && Common.login_data.getData().getId() != null) {
//                                            int form_id = Integer.valueOf(topic_data_detail.getSubforums__id());
//                                            int user_id = Integer.valueOf(Common.login_data.getData().getId());
//                                            onSubTopicBtnClick(form_id, user_id);
//                                        }
//                                    }
//
//                                } else {
                                //pd.dismiss();

                               // Common.post_data = postListData;

                                ArrayList<GetPostListDataCommentsModel_Updated> comment_list = postListData.getComments();
                                ArrayList<GetPostListDataCommentsModel_Updated> new_list = new ArrayList<>();
//                                    new_list.add(post1);
//................................................................visibilty not null.................................................
                                if (post1.getForumPosts__visibility() != null) {

                                    //....................agr admin h tu sbb show ho.......................................
                                    if (Common.is_Admin_flag == true) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }

                                    } else {
                                        //......................................S..............................................
                                        if (post1.getForumPosts__visibility().equals("S")) {


                                            if (post1.getForumPosts__visibility().equals("S") && post1.getIs_moderator() == 1) {

                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }


                                                //aggr member h or visibilty S h tu sirf post or admin k comments nzr aye gi
                                                //select admin comments
                                            } else if (post1.getForumPosts__visibility().equals("S") && post1.getIs_moderator() == 0) {

                                                for (int i = 0; i < comment_list.size(); i++) {

                                                    if (comment_list.get(i).getUsers__role().equals("C")) {
                                                        //rejecting all comments for member which is to be approved
                                                    } else if (comment_list.get(i).getUsers__role().equals("A")) {
                                                        //adding all approved comments
                                                        new_list.add(comment_list.get(i));
                                                    }

                                                }
                                            }


                                            //......................................P..............................................
                                            //aggr member h or visibilty P h tu sbb kujj nzr aye ga and agr post approve post 1 h tu approved or pending dono nzr aye gy
                                        } else if (post1.getForumPosts__visibility().equals("P")) {


                                            //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                            if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                                for (int i = 0; i < comment_list.size(); i++) {

                                                    if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                        //rejecting all comments for member which is to be approved
                                                    } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                        //adding all approved comments
                                                        new_list.add(comment_list.get(i));
                                                    }

                                                }

                                                //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                            } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            }

                                        } else {

                                            //...........now adding first post in comment..........
                                            //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                            if (post1.getForumPosts__moderated() != null) {
                                                if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                                    for (int i = 0; i < comment_list.size(); i++) {

                                                        if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                            //rejecting all comments for member which is to be approved
                                                        } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                            //adding all approved comments
                                                            new_list.add(comment_list.get(i));
                                                        }

                                                    }

                                                    //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                                } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                }


                                            }
                                        }

                                    }

                                } else {
                                    //................................................................visibilty  null.................................................
                                    //agr visibility null h tu by default public me show krwany by sir asif and hamza

                                    //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                    if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                        for (int i = 0; i < comment_list.size(); i++) {

                                            if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                //rejecting all comments for member which is to be approved
                                            } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                //adding all approved comments
                                                new_list.add(comment_list.get(i));
                                            }

                                        }

                                        //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                    } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    }

                                }


                                //.....................................................


                                if (isPostDelete == true && isPostApproved == false) {
                                    Toast.makeText(Sub_Topic_Posts.this, "Post deleted successfully", Toast.LENGTH_LONG).show();
                                } else if (isPostDelete == false && isPostApproved == true) {
                                    Toast.makeText(Sub_Topic_Posts.this, "Post Approved successfully", Toast.LENGTH_LONG).show();
                                }


                                if (postListData.getComments().size() == 0) {

                                    total_api_item_count = response.body().getData().getComments().size();
                                    //no_page = true;
                                    loading_dialog.dismiss();
                                    Toast.makeText(Sub_Topic_Posts.this, "No more page found", Toast.LENGTH_LONG).show();
                                } else {


                                    post_list_comment_data.addAll(new_list);

                                    total_api_item_count = response.body().getData().getComments().size();
                                    adapter.AddData(new_list);

                                    loading_dialog.dismiss();
                                }
                                //            }


                            } else {
                                //post lis is null
                            }
                        }
                    } else {
                        //response failure
                    }


                }

                @Override
                public void onFailure(Call<GetPostListModel_Updated> call, Throwable t) {
                    loading_dialog.dismiss();
                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {

        //        ------------Exception handling------------------
        try {

            switch (v.getId()) {
                case R.id.like_btn:
                    if (Like_Flag == false) {

                        if (post_list_comment_data.get(position).getIslike() == 0) {
                            loading_dialog.show();
                            onLikeDislikeBtnClick(position, "1", "0");
                        }
                    }
                    break;
                case R.id.dislike_btn:
                    if (UnLike_Flag == false) {
                        if (post_list_comment_data.get(position).getIsdislike() == 0) {
                            loading_dialog.show();
                            onLikeDislikeBtnClick(position, "0", "1");
                        }
                    }
                    break;
                case R.id.post_edit_icon:

                    //............................................................
                    Common.addPost = false;
                    Common.editPost = true;

                    if (post_list_comment_data.get(position) != null) {
                        sub_topic_post_detail_updated = post_list_comment_data.get(position);
                    }
                    Intent edit_post = new Intent(Sub_Topic_Posts.this, Add_Post.class);
                    edit_post.putExtra("SubtopicId", String.valueOf(final_id));
                    edit_post.putExtra("TopicDataDetail", topic_data_detail);
                    edit_post.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                    edit_post.putExtra("SubtopicPostDataDetail",sub_topic_post_detail_updated);
                    edit_post.putExtra("IsFromDashBoard", "1");
                    edit_post.putExtra("Category", Category);
                    startActivity(edit_post);
                    finish();
                    // }
                    break;


                case R.id.post_add_qoute_icon:

                    if (post_list_comment_data.get(position) != null) {
                        sub_topic_post_detail_updated = post_list_comment_data.get(position);
                    }


                    Intent i = new Intent(Sub_Topic_Posts.this, AddQoute.class);
                    i.putExtra("SubtopicId", String.valueOf(final_id));
                    i.putExtra("TopicDataDetail", topic_data_detail);
                    i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                    i.putExtra("SubtopicPostDataDetail",sub_topic_post_detail_updated);
                    i.putExtra("IsFromDashBoard", "1");
                    i.putExtra("Category", Category);
                    i.putExtra("ModeratorFlag",post_list_comment_data.get(0).getForumPosts__moderated());
                    startActivity(i);
                    finish();

                    break;

                case R.id.post_delete_icon:
//                    loading_dialog.show();

                    if (post_list_comment_data.get(position) != null) {
                        sub_topic_post_detail_updated = post_list_comment_data.get(position);
                    }

                    Confirm_Dialog_Delete("Delete Subtopic Post", "Are you sure to delete this subtopic post?", "Yes",sub_topic_post_detail_updated.getForumPosts__id());
                    //onDeleteTopicBtnClick();
                    break;

                case R.id.post_approve_icon:
                    loading_dialog.show();
                    if (post_list_comment_data.get(position) != null) {
                        sub_topic_post_detail_updated = post_list_comment_data.get(position);
                    }
                    ApproveComment(sub_topic_post_detail_updated.getForumPosts__id());

                    break;
                case R.id.send_eblast_layout:
                    Common.send_eblast_post = true;

                    Common.eblast_subject = "";
                    Common.eblast_label = "";
                    Common.eblast_message = "";
                    Common.send_eblast_list = "";

                    Common.Subtopic_Id=Subtopic_Id;
                    Common.Category=Category;
                    Common.topic_data_detail=topic_data_detail;
                    Common.sub_topic_data_detail=sub_topic_data_detail;
                    Common.eblast_message=post_list_comment_data.get(position).getForumPosts__content();
                    Intent eblast = new Intent(Sub_Topic_Posts.this, SendEblastScreen1.class);
//                eblast.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(eblast);

                    break;
                default:
                    break;
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }


    private void onDeleteTopicBtnClick(String post_id) {

        //        ------------Exception handling------------------
        try {


            DeletePostRequestModel requestModel = new DeletePostRequestModel();
            requestModel.setForum_post_id(post_id);

            Call<DeletePostResponseModel> responseCall = retrofitInterface.DeletePost("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<DeletePostResponseModel>() {
                @Override
                public void onResponse(Call<DeletePostResponseModel> call, Response<DeletePostResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        DeletePostResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        int id = Integer.valueOf(final_id);
                        final_id = Integer.valueOf(id);
                        is_post_deleted = true;
                        is_post_approved = false;

                        page_number = 1;
                        item_count = 25;
                        total_api_item_count = 0;
                        onPostListBtnClick_Updated(id, true, false);

                    } else {
                    }

                }

                @Override
                public void onFailure(Call<DeletePostResponseModel> call, Throwable t) {
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    private void ApproveComment(String post_id) {


        //        ------------Exception handling------------------
        try {

            ApproveCommentRequestModel requestModel = new ApproveCommentRequestModel();


            requestModel.setReply_id(post_id);

            Call<ApproveCommentResponseModel> responseCall = retrofitInterface.ApproveComment("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<ApproveCommentResponseModel>() {
                @Override
                public void onResponse(Call<ApproveCommentResponseModel> call, Response<ApproveCommentResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        ApproveCommentResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        if (status.equals("success")) {
                            //int id = Integer.valueOf(final_id);

                            //final_id = Integer.valueOf(sub_topic_data_detail.getId());
                            is_post_deleted = false;
                            is_post_approved = true;

                            page_number = 1;
                            item_count = 25;

                            total_api_item_count = 0;
                            onPostListBtnClick_Updated(final_id, false, true);
                        }


                    } else {
                    }

                }

                @Override
                public void onFailure(Call<ApproveCommentResponseModel> call, Throwable t) {

                    // int id = Integer.valueOf(sub_topic_data_detail.getId());
                    intialize_view(final_id, false, true);
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    private void onPostListBtnClick_Updated(final int id, final Boolean isPostDelete, final Boolean isPostApproved) {
        //        ------------Exception handling------------------
        try {

            Call<GetPostListModel_Updated> call = retrofitInterface.getPostListUpdated("application/json", Common.auth_token, id, 25, 1, Integer.valueOf(Common.login_data.getData().getId()));

            call.enqueue(new Callback<GetPostListModel_Updated>() {
                @Override
                public void onResponse(Call<GetPostListModel_Updated> call, Response<GetPostListModel_Updated> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GetPostListDataModel_Updated postListData = new GetPostListDataModel_Updated();
                        GetPostListModel_Updated responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (responseModel.getMessage().equals("No more Page to be found")) {
                            if (post_list_comment_data != null) {
                                post_list_comment_data.clear();
                            }
                            intialize_view(id, isPostDelete, isPostApproved);

                        } else {


                            postListData = responseModel.getData();

                            if (postListData != null) {

                                //static makes first post from API and added in comment List....jugaaar
                                GetPostListDataCommentsModel_Updated post1 = new GetPostListDataCommentsModel_Updated();

                                post1.setForumPosts__id(postListData.getForumPosts__id());
                                post1.setForumPosts__user_id(postListData.getForumPosts__user_id());
                                post1.setForumPosts__subforum_id(postListData.getForumPosts__subforum_id());
                                post1.setForumPosts__topic(postListData.getForumPosts__topic());
                                post1.setForumPosts__content(postListData.getForumPosts__content());
                                post1.setForumPosts__pending(postListData.getForumPosts__pending());
                                post1.setForumPosts__moderated(postListData.getForumPosts__moderated());
                                post1.setForumPosts__pinned(postListData.getForumPosts__pinned());
                                post1.setForumPosts__sod(postListData.getForumPosts__sod());
                                post1.setForumPosts__notify_users(postListData.getForumPosts__notify_users());
                                post1.setForumPosts__visibility(postListData.getForumPosts__visibility());
                                post1.setForumPosts__post_visibility(postListData.getForumPosts__post_visibility());
                                post1.setForumPosts__status(postListData.getForumPosts__status());
                                post1.setForumPosts__created(postListData.getForumPosts__created());
                                post1.setUsers__first_name(postListData.getUsers__first_name());
                                post1.setUsers__last_name(postListData.getUsers__last_name());
                                post1.setPosts(postListData.getPosts());
                                post1.setLikescount(postListData.getLikescount());
                                post1.setDislikescount(postListData.getDislikescount());
                                post1.setIslike(postListData.getIslike());
                                post1.setIsdislike(postListData.getIsdislike());
                                post1.setFrequency(postListData.getFrequency());
                                post1.setUsers__role(postListData.getUsers__role());
                                post1.setIs_moderator(postListData.getIs_moderator());
                                post1.setProfileImg__name(postListData.getProfileImg__name());
                                post1.setUsers__last_login(postListData.getUsers__last_login());
                                //...................................................


//                                if (responseModel.getMessage().equals("No more Page to be found")) {
//
//                                    if (Common.topic_data_detail != null) {
//
//                                        if (Common.topic_data_detail.getSubforums__id() != null && Common.login_data.getData().getId() != null) {
//                                            int form_id = Integer.valueOf(Common.topic_data_detail.getSubforums__id());
//                                            int user_id = Integer.valueOf(Common.login_data.getData().getId());
//                                            onSubTopicBtnClick(form_id, user_id);
//                                        }
//                                    }
//
//                                } else {
                                //pd.dismiss();

                                //Common.post_data = postListData;

                                ArrayList<GetPostListDataCommentsModel_Updated> comment_list = postListData.getComments();


                                ArrayList<GetPostListDataCommentsModel_Updated> new_list = new ArrayList<>();
                                new_list.add(post1);
//................................................................visibilty not null.................................................
                                if (post1.getForumPosts__visibility() != null) {

                                    //....................agr admin h tu sbb show ho.......................................
                                    if (Common.is_Admin_flag == true) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }

                                    } else {
                                        //......................................S..............................................
                                        if (post1.getForumPosts__visibility().equals("S")) {


                                            if (post1.getForumPosts__visibility().equals("S") && post1.getIs_moderator() == 1) {

                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }


                                                //aggr member h or visibilty S h tu sirf post or admin k comments nzr aye gi
                                                //select admin comments
                                            } else if (post1.getForumPosts__visibility().equals("S") && post1.getIs_moderator() == 0) {

                                                for (int i = 0; i < comment_list.size(); i++) {

                                                    if (comment_list.get(i).getUsers__role().equals("C")) {
                                                        //rejecting all comments for member which is to be approved
                                                    } else if (comment_list.get(i).getUsers__role().equals("A")) {
                                                        //adding all approved comments
                                                        new_list.add(comment_list.get(i));
                                                    }

                                                }
                                            }


                                            //......................................P..............................................
                                            //aggr member h or visibilty P h tu sbb kujj nzr aye ga and agr post approve post 1 h tu approved or pending dono nzr aye gy
                                        } else if (post1.getForumPosts__visibility().equals("P")) {


                                            //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                            if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                                for (int i = 0; i < comment_list.size(); i++) {

                                                    if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                        //rejecting all comments for member which is to be approved
                                                    } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                        //adding all approved comments
                                                        new_list.add(comment_list.get(i));
                                                    }

                                                }

                                                //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                            } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                                for (int i = 0; i < comment_list.size(); i++) {
                                                    new_list.add(comment_list.get(i));
                                                }
                                            }

                                        } else {

                                            //...........now adding first post in comment..........
                                            //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                            if (post1.getForumPosts__moderated() != null) {
                                                if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                                    for (int i = 0; i < comment_list.size(); i++) {

                                                        if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                            //rejecting all comments for member which is to be approved
                                                        } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                            //adding all approved comments
                                                            new_list.add(comment_list.get(i));
                                                        }

                                                    }

                                                    //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                                } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                                    for (int i = 0; i < comment_list.size(); i++) {
                                                        new_list.add(comment_list.get(i));
                                                    }
                                                }


                                            }
                                        }

                                    }

                                } else {
                                    //................................................................visibilty  null.................................................
                                    //agr visibility null h tu by default public me show krwany by sir asif and hamza

                                    //aggr post approve post 1 and is moderator 0 h tu pending post show ni krwani
                                    if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 0) {

                                        for (int i = 0; i < comment_list.size(); i++) {

                                            if (comment_list.get(i).getForumPosts__status().equals("P")) {
                                                //rejecting all comments for member which is to be approved
                                            } else if (comment_list.get(i).getForumPosts__status().equals("A")) {
                                                //adding all approved comments
                                                new_list.add(comment_list.get(i));
                                            }

                                        }

                                        //aggr post approve post 1 and is moderator 1 h tu all post show krwani
                                    } else if (post1.getForumPosts__moderated().equals("1") && post1.getIs_moderator() == 1) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 1) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    } else if (post1.getForumPosts__moderated().equals("0") && post1.getIs_moderator() == 0) {
                                        for (int i = 0; i < comment_list.size(); i++) {
                                            new_list.add(comment_list.get(i));
                                        }
                                    }

                                }


                                //.....................................................

                                if (new_list != null) {
                                    if (post_list_comment_data != null) {
                                        post_list_comment_data.clear();
                                    }
                                    post_list_comment_data = new_list;
                                }


                                if (isPostDelete == true && isPostApproved == false) {
                                    Toast.makeText(Sub_Topic_Posts.this, "Post deleted successfully", Toast.LENGTH_LONG).show();
                                } else if (isPostDelete == false && isPostApproved == true) {
                                    Toast.makeText(Sub_Topic_Posts.this, "Post Approved successfully", Toast.LENGTH_LONG).show();
                                }


                                //for pagination use

                                total_api_item_count = response.body().getData().getComments().size();

                                intialize_view(id, isPostDelete, isPostApproved);


                                //  }


                            } else {
                                //post lis is null
                            }
                        }
                    } else {
                    }


                }

                @Override
                public void onFailure(Call<GetPostListModel_Updated> call, Throwable t) {
                    loading_dialog.dismiss();
                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }


//    private void onSubTopicBtnClick(int form_id, int user_id) {
//
//        Intent sub_topic_intent = new Intent(Sub_Topic_Posts.this, Sub_Topics.class);
//        startActivity(sub_topic_intent);
//        finish();
//
////        //        ------------Exception handling------------------
////        try {
////            Call<GetSubTopicModel> call = retrofitInterface.getSubTopic("application/json", Common.auth_token, form_id, 1, 1000, user_id);
////            call.enqueue(new Callback<GetSubTopicModel>() {
////                @Override
////                public void onResponse(Call<GetSubTopicModel> call, Response<GetSubTopicModel> response) {
////
////                    int status_code = response.code();
////
////                    ArrayList<GetSubTopicDataModel> main_list = new ArrayList<>();
////
////                    ArrayList<GetSubTopicDataModel> subTopicListData = new ArrayList<>();
////
////                    if (response.isSuccessful()) {
////                        String status, message;
////
////
////                        GetSubTopicModel responseModel = response.body();
////
////                        //main_list.add(responseModel);
////
////
////                        status = responseModel.getStatus();
////                        message = responseModel.getMessage();
////
////                        subTopicListData = responseModel.getData();
////
////                        if (subTopicListData != null) {
////                            for (GetSubTopicDataModel item : subTopicListData) {
////                                if (item != null) {
////                                    main_list.add(item);
////                                }
////                            }
////                        }
////
////
////                    }
////
////                    if (main_list != null) {
////                        //pd.dismiss();
////                        loading_dialog.dismiss();
////
////                        if (Common.sub_topic_data != null) {
////                            Common.sub_topic_data.clear();
////                        }
////                        Common.sub_topic_data = main_list;
////                        Intent sub_topic_intent = new Intent(Sub_Topic_Posts.this, Sub_Topics.class);
////                        startActivity(sub_topic_intent);
////                        finish();
////                    }
////
////
////                }
////
////                @Override
////                public void onFailure(Call<GetSubTopicModel> call, Throwable t) {
////                 }
////            });
////            throw new RuntimeException("Run Time exception");
////        } catch (Exception e) {
////         }
//    }


    private void onLikeDislikeBtnClick(final int position, final String isLike, final String isDislike) {


        //        ------------Exception handling------------------
        try {

            LikeDislikeRequestModel likeRequest = new LikeDislikeRequestModel();
            likeRequest.setUser_id(Common.login_data.getData().getId());
            likeRequest.setLikes(isLike);
            likeRequest.setDislikes(isDislike);
            //likeRequest.setForum_post_id(Common.post_list_data.get(position).getForumPosts__id());
            likeRequest.setForum_post_id(post_list_comment_data.get(position).getForumPosts__id());


            Call<LikeDislikeResponseModel> responseCall = retrofitInterface.LikeDislike("application/json", Common.auth_token, likeRequest);
            responseCall.enqueue(new Callback<LikeDislikeResponseModel>() {
                @Override
                public void onResponse(Call<LikeDislikeResponseModel> call, Response<LikeDislikeResponseModel> response) {
                    //loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        String status, message;

                        LikeDislikeResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {

                            if (isLike.equals("1") && isDislike.equals("0")) {
                                Like_Flag = true;
                                UnLike_Flag = false;
                            } else if (isLike.equals("0") && isDislike.equals("1")) {
                                Like_Flag = false;
                                UnLike_Flag = true;
                            }

                            //int id = Integer.valueOf(sub_topic_data_detail.getId());
                            //onPostListBtnClick(id,false);

                            //final_id = Integer.valueOf(sub_topic_data_detail.getId());
                            is_post_deleted = false;
                            is_post_approved = false;
                            onPostListBtnClick_Updated(final_id, false, false);
                        }

                    } else {
                    }
                }

                @Override
                public void onFailure(Call<LikeDislikeResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (IsFromDashBoard.equals("1")) {
            finish();
        } else if (IsFromDashBoard.equals("0")) {
            //Common.post_back_flag = true;
            Intent i = new Intent(Sub_Topic_Posts.this, Sub_Topics.class);
            i.putExtra("TopicDataDetail", topic_data_detail);
            i.putExtra("Category", Category);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }


    void Subscribe_Dialog() {

        //        ------------Exception handling------------------
        try {
            // custom dialog
            subcribe_dialog = new Dialog(Sub_Topic_Posts.this);
            subcribe_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            subcribe_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            subcribe_dialog.setContentView(R.layout.subscribe_dialog);
            subcribe_dialog.setCanceledOnTouchOutside(false);
            // dialog.getWindow().setLayout(275, 350);


            TextView dialog_subcribe_button;
            final RadioButton immediately, once_day, once_week;

            dialog_subcribe_button = (TextView) subcribe_dialog.findViewById(R.id.subscribe_btn);
            immediately = (RadioButton) subcribe_dialog.findViewById(R.id.radio_Immediately);
            once_day = (RadioButton) subcribe_dialog.findViewById(R.id.radio_Day);
            once_week = (RadioButton) subcribe_dialog.findViewById(R.id.radio_Week);

            //by default
            immediately.setChecked(true);
            frequency = "I";

            immediately.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    immediately.setChecked(true);
                    once_day.setChecked(false);
                    once_week.setChecked(false);

                    frequency = "I";
                }
            });

            once_day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    immediately.setChecked(false);
                    once_day.setChecked(true);
                    once_week.setChecked(false);

                    frequency = "D";
                }
            });


            once_week.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    immediately.setChecked(false);
                    once_day.setChecked(false);
                    once_week.setChecked(true);

                    frequency = "W";
                }
            });

            RelativeLayout close = (RelativeLayout) subcribe_dialog.findViewById(R.id.subscribe_close_btn);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subcribe_dialog.dismiss();
                }
            });


            dialog_subcribe_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (frequency == "") {
                        Toast.makeText(Sub_Topic_Posts.this, "Select Frequency", Toast.LENGTH_SHORT).show();
                    } else {
                        subcribe_dialog.dismiss();
                        loading_dialog.show();
                        onSubcribeBtnClick(frequency);
                    }
                }
            });


            subcribe_dialog.show();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    public void Confirm_Dialog_Delete(String title, String message, String btn, final String post_id) {


        //        ------------Exception handling------------------
        try {

            TextView title_text, message_text, btn_text;
            RelativeLayout close_btn;

            // custom dialog
            final Dialog dialog = new Dialog(Sub_Topic_Posts.this);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.aa_confirm_dialog);
            dialog.setCanceledOnTouchOutside(false);
            // dialog.getWindow().setLayout(275, 350);


            title_text = (TextView) dialog.findViewById(R.id.custom_title);
            message_text = (TextView) dialog.findViewById(R.id.custom_message);
            btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
            close_btn = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);

            title_text.setText(title);
            message_text.setText(message);
            btn_text.setText(btn);


            btn_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    loading_dialog.show();
                    onDeleteTopicBtnClick(post_id);

                }
            });

            close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    void Confirm_Dialog(String title, String message, String btn, final Boolean Subscribe_flag) {

        //        ------------Exception handling------------------
        try {
            TextView title_text, message_text, btn_text;

            // custom dialog
            final Dialog dialog = new Dialog(Sub_Topic_Posts.this);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.aa_confirm_dialog);
            dialog.setCanceledOnTouchOutside(false);
            // dialog.getWindow().setLayout(275, 350);


            title_text = (TextView) dialog.findViewById(R.id.custom_title);
            message_text = (TextView) dialog.findViewById(R.id.custom_message);
            btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

            title_text.setText(title);
            message_text.setText(message);
            btn_text.setText(btn);

            btn_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);
            close.setVisibility(View.GONE);

            dialog.show();


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onSubcribeBtnClick(final String frequency_value) {

//        ------------Exception handling------------------
        try {


            SubcribeRequestModel requestModel = new SubcribeRequestModel();

            requestModel.setForum_post_id(String.valueOf(final_id));
            requestModel.setUser_id(Common.login_data.getData().getId());
            requestModel.setFrequency(frequency_value);


            Call<SubcribeResponseModel> responseCall = retrofitInterface.SubcribePost("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<SubcribeResponseModel>() {
                @Override
                public void onResponse(Call<SubcribeResponseModel> call, Response<SubcribeResponseModel> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        String status, message;

                        SubcribeResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        subscribed_flag = true;
                        subscribe_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_subscribe_green));

                        //once frequency done
                        frequency = "";
                        //Confirm_Dialog("Subscribed", "Sub-topic is subscribed successfully", "OK", true);

                    } else {
                    }
                }

                @Override
                public void onFailure(Call<SubcribeResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onUnSubcribeBtnClick() {

//        ------------Exception handling------------------
        try {


            UnSubscribeRequestModel requestModel = new UnSubscribeRequestModel();

            requestModel.setForum_post_id(String.valueOf(final_id));
            requestModel.setUser_id(Common.login_data.getData().getId());


            Call<UnSubscribeResponseModel> responseCall = retrofitInterface.UnSubcribePost("application/json", Common.auth_token, requestModel);

            responseCall.enqueue(new Callback<UnSubscribeResponseModel>() {
                @Override
                public void onResponse(Call<UnSubscribeResponseModel> call, Response<UnSubscribeResponseModel> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        String status, message;

                        UnSubscribeResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        subscribed_flag = false;
                        subscribe_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_subscribe));

                        Confirm_Dialog("UnSubscribed", "Sub-topic is UnSubscribed successfully", "OK", false);


                    } else {
                    }
                }

                @Override
                public void onFailure(Call<UnSubscribeResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }
}
