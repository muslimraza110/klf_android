package com.khojaleadership.KLF.Activities.event_new.test

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.Model.event_new.SummitEventDayAvailability
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemProfile2Binding
import com.khojaleadership.KLF.databinding.LayoutListGroupHeaderBinding
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractExpandableHeaderItem
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.ExpandableViewHolder
import java.util.*

class Item(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val userId: String,
        val name: String,
        val designation: String,
        val description: String,
        val facebook: String,
        val twitter: String,
        val linkedIn: String,
        val photoUrl: String,
        val availableSlotData: List<SummitEventDayAvailability>?,
        val onClick: (Item) -> Unit,
        val onFacebookClick: (String) -> Unit,
        val onTwitterClick: (String) -> Unit,
        var header: HeaderItem,
        var context: Context
) : AbstractSectionableItem<Item.ViewHolder, HeaderItem>(
        header
) {


    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_profile_2
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                binding = ItemProfile2Binding.bind(view),
                onClick = onClick,
                onFacebookClick = onFacebookClick,
                onTwitterClick = onTwitterClick,
                context = context
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }


    class ViewHolder(
            val binding: ItemProfile2Binding,
            var context: Context,
            private val onClick: (Item) -> Unit,
            private val onFacebookClick: (String) -> Unit,
            private val onTwitterClick: (String) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Item) {
            item.apply {
                binding.tvName.text = name

                binding.tvDesignation.text = designation
                binding.tvSummary.text = description

                Glide.with(context)
                        .load(photoUrl)
                        .centerCrop()
                        .into(binding.userImage)
                binding.root.setOnClickListener {
                    onClick.invoke(this)
                }
                binding.linkedInIcon.setOnClickListener {
                }
                binding.fbIcon.setOnClickListener {
                    onFacebookClick.invoke(facebook)
                }
            }
        }

    }

}


class HeaderItem(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        var title: String,
        var isOpened: Boolean
) : AbstractExpandableHeaderItem<HeaderItem.ViewHolder, Item>() {


    override fun isExpanded(): Boolean {
        return isOpened
    }

    init {
//        isExpanded = isOpened
//        setHidden(false);
//        setExpanded(true);
//        setSelectable(false);
    }

    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.layout_list_group_header
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                LayoutListGroupHeaderBinding.bind(view), adapter
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }

    class ViewHolder(
            val binding: LayoutListGroupHeaderBinding,
            var adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ) : ExpandableViewHolder(binding.root, adapter, true) {

        fun bind(item: HeaderItem) {
            binding.tvGroupName.text = item.title
            binding.groupIndicator.setOnClickListener {
                toggleExpansion()
                item.isOpened = !item.isOpened
                if (item.isOpened) {
                    binding.groupIndicator.setImageResource(R.drawable.action_drop_up)

                } else {
                    binding.groupIndicator.setImageResource(R.drawable.action_drop_down)
                }
            }

            if (item.isOpened) {
                binding.groupIndicator.setImageResource(R.drawable.action_drop_up)

            } else {
                binding.groupIndicator.setImageResource(R.drawable.action_drop_down)
            }


        }

    }
}