package com.khojaleadership.KLF.Activities.Mission;

import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Model.Mission.GetMissionDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import jp.wasabeef.richeditor.RichEditor;

public class Edit_Mission extends AppCompatActivity {

    RichEditor mEditor;

    ArrayList<GetMissionDataModel> mission_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__mission);

        //back btn
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(22);

//        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setScrollbarFadingEnabled(false);
        //mEditor.setVerticalScrollbarOverlay(true);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
//        mEditor.isScrollContainer();
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);




        mEditor.setEditorFontColor(Color.BLACK);
        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Insert text here...");
        //mEditor.setInputEnabled(false);





        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setBold();
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setItalic();
            }
        });



        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });


        //setting data for editing purpose
        intialize_view();
    }

    public void intialize_view(){
        WebSettings webSettings = mEditor.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mEditor.loadData(mission_list.get(0).getPages__content(), "text/html; charset=utf-8", "UTF-8");

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
