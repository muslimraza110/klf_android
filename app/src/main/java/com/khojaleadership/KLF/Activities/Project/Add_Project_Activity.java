package com.khojaleadership.KLF.Activities.Project;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.Model.Project_Models.AddProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Add_group_To_ProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Project_CharityType_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_CharityType_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_TargetAnualSpend_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_TargetAnualSpend_Model;
import com.khojaleadership.KLF.Model.Project_Models.TargetAudienceDataModel;
import com.khojaleadership.KLF.Model.Project_Models.TargetAudienceModel;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Project_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Project_Activity extends AppCompatActivity {

    private TextView tv_startDate;
    TextView tv_Est_End_Date;

    DatePickerDialog.OnDateSetListener mDateSetListener;
    DatePickerDialog.OnDateSetListener mDateSetListener1;

    EditText name, website, description, city, country;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    Spinner target_audience_spinner;
    String target_audience_string;

    Spinner charity_type_spinner;
    String charity_type_string;

    Spinner target_anualspend_spinner;
    String target_anualspend_string;


    ArrayList<Project_CharityType_Data_Model> projects_charity_type_list = new ArrayList<>();
    ArrayList<String> project_charity_type_string = new ArrayList<>();

    ArrayList<TargetAudienceDataModel> projects_target_audience_list = new ArrayList<>();
    ArrayList<String> project_target_audience_string = new ArrayList<>();

    ArrayList<Project_TargetAnualSpend_Data_Model> projects_target_anualspend_list = new ArrayList<>();
    ArrayList<String> project_target_anualspend_string = new ArrayList<>();

    String Group_Id="0",AddProjectInGroupFlag="-1";


    GetGroupListDataModel group_data_detail=new GetGroupListDataModel();
    GetAllGroup_ProjectListDataModel all_group_data_detail=new GetAllGroup_ProjectListDataModel();
    View_Profile_Bussiness_Project_Model user_profile_businessandprojects_data=new View_Profile_Bussiness_Project_Model();
    View_Profile_Charity_Project_Model user_profile_charitiesandprojects_data=new View_Profile_Charity_Project_Model();

    String isAllGroupDetail="-1",ViewProfileDetail="-1",isProfileBusinessData="-1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__project_);


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Add Project");


        //getting data
        Intent intent= getIntent();
        group_data_detail=intent.getParcelableExtra("group_data_detail");
        all_group_data_detail=intent.getParcelableExtra("all_group_data_detail");
        user_profile_businessandprojects_data=intent.getParcelableExtra("ViewProfileBussinessData");
        user_profile_charitiesandprojects_data=intent.getParcelableExtra("ViewProfileCharityData");


        Bundle bundle=intent.getExtras();
        if (bundle!=null) {
            isAllGroupDetail = bundle.getString("isAllGroupDetail");
            ViewProfileDetail=bundle.getString("ViewProfileDetail");
            isProfileBusinessData=bundle.getString("isProfileBusinessData");

            Group_Id = bundle.getString("Group_Id");
            AddProjectInGroupFlag = bundle.getString("AddProjectInGroupFlag");
        }
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Project_Activity.this);

                finish();
            }
        });

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        GetCharityType();

        findViewById(R.id.add_project_save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddBtnClick();
            }
        });
    }

    private void GetCharityType() {


        Call<Project_CharityType_Model> responseCall = retrofitInterface.getCharityType("application/json", Common.auth_token);

        responseCall.enqueue(new Callback<Project_CharityType_Model>() {
            @Override
            public void onResponse(Call<Project_CharityType_Model> call, Response<Project_CharityType_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Project_CharityType_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    //loading_dialog.dismiss();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Add_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<Project_CharityType_Data_Model> list = responseModel.getData();
                        projects_charity_type_list = list;

                        project_charity_type_string.clear();
                        project_charity_type_string.add("Charity Type");
                        for (int i = 0; i < projects_charity_type_list.size(); i++) {
                            project_charity_type_string.add(projects_charity_type_list.get(i).getTags__name());
                        }

                        GetTargetAudience();
                        //getAlreadyGroupsInProject(Common.projects_data_detail.getProjects__id());
                        //intialize_view();

                    }


                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<Project_CharityType_Model> call, Throwable t) {
                loading_dialog.dismiss();
                }

        });


    }

    private void GetTargetAudience() {


        Call<TargetAudienceModel> responseCall = retrofitInterface.getTargetAudience("application/json", Common.auth_token);


        responseCall.enqueue(new Callback<TargetAudienceModel>() {
            @Override
            public void onResponse(Call<TargetAudienceModel> call, Response<TargetAudienceModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    TargetAudienceModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Add_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<TargetAudienceDataModel> list = responseModel.getData();
                        projects_target_audience_list = list;

                        project_target_audience_string.clear();
                        project_target_audience_string.add("Target Audience People");
                        for (int i = 0; i < projects_target_audience_list.size(); i++) {
                            project_target_audience_string.add(projects_target_audience_list.get(i).getTags__name());
                        }

                        GetTargetAnualSpend();


                    }


                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<TargetAudienceModel> call, Throwable t) {
                loading_dialog.dismiss();
             }
        });


    }

    private void GetTargetAnualSpend() {


        Call<Project_TargetAnualSpend_Model> responseCall = retrofitInterface.getTargetAnualSpend("application/json", Common.auth_token);


        responseCall.enqueue(new Callback<Project_TargetAnualSpend_Model>() {
            @Override
            public void onResponse(Call<Project_TargetAnualSpend_Model> call, Response<Project_TargetAnualSpend_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Project_TargetAnualSpend_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Add_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<Project_TargetAnualSpend_Data_Model> list = responseModel.getData();
                        // Common.projects_target_audience_list.add(new TargetAudienceDataModel("0","","","","Target Audience People",""));
                        projects_target_anualspend_list = list;

                        project_target_anualspend_string.clear();
                        project_target_anualspend_string.add("Target Audience People");
                        for (int i = 0; i < projects_target_anualspend_list.size(); i++) {
                            project_target_anualspend_string.add(projects_target_anualspend_list.get(i).getTags__name());
                        }


                        loading_dialog.dismiss();
                        intialize_view();

                    }


                } else {
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<Project_TargetAnualSpend_Model> call, Throwable t) {
                loading_dialog.dismiss();
             }
        });


    }


    public void intialize_view() {
        name = (EditText) findViewById(R.id.add_project_name);
        website = (EditText) findViewById(R.id.add_project_website);
        description = (EditText) findViewById(R.id.add_project_description);
        city = (EditText) findViewById(R.id.add_project_city);
        country = (EditText) findViewById(R.id.add_project_country);


        tv_startDate = findViewById(R.id.add_project_startDate);
        tv_startDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_startDate.setError(null);//removes error
                tv_startDate.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        tv_Est_End_Date = findViewById(R.id.add_project_endDate);
        tv_Est_End_Date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_Est_End_Date.setError(null);
                tv_Est_End_Date.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //this is for the start date


        tv_startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal1 = Calendar.getInstance();
                int year = cal1.get(Calendar.YEAR);
                int month = cal1.get(Calendar.MONTH);
                int day = cal1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog1 = new DatePickerDialog(
                        Add_Project_Activity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = year + "-" + month + "-" + day;
                tv_startDate.setText(date);
            }
        };


//this is for the est end date

        tv_Est_End_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        Add_Project_Activity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                String date = year + "-" + month + "-" + day;
                tv_Est_End_Date.setText(date);
            }
        };


        //this is for the Target Audience Spinner
        CharityTypeSpinnerView();
        //this is for the Target Audience Spinner
        TargetAudienceSpinnerView();
        //this is for the Target Anual Spend Spinner
        TargetAnualSpendSpinnerView();


    }


    public void CharityTypeSpinnerView() {

        charity_type_spinner = (Spinner) findViewById(R.id.spinner_Charity_type);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_charity_type_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {


                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    if (projects_charity_type_list.get(position - 1).getTags__id().equals("9")) {
                        tv.setTextSize(14);
                    } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("13")) {
                        tv.setTextSize(14);
                    } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("14")) {
                        tv.setTextSize(14);
                    } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("15")) {
                        tv.setTextSize(14);
                    } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("31")) {
                        tv.setTextSize(14);
                    } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("18")) {
                        tv.setTextSize(14);
                    } else {
                        tv.setTextSize(12);
                    }

                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        charity_type_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        charity_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {


                    charity_type_string = projects_charity_type_list.get(position - 1).getTags__id();



                } else {
                    charity_type_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void TargetAudienceSpinnerView() {

        target_audience_spinner = (Spinner) findViewById(R.id.target_audience_spinner);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_target_audience_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {


                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    //tv.setTextSize(22);
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        target_audience_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        target_audience_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {


                    target_audience_string = projects_target_audience_list.get(position - 1).getTags__id();



                } else {
                    target_audience_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void TargetAnualSpendSpinnerView() {

        target_anualspend_spinner = (Spinner) findViewById(R.id.target_anualspend_spinner);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_target_anualspend_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {

                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    //tv.setTextSize(22);
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        target_anualspend_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        target_anualspend_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {


                    target_anualspend_string = projects_target_anualspend_list.get(position - 1).getTags__id();


                } else {
                    target_anualspend_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void onAddBtnClick() {


        if (TextUtils.isEmpty(name.getText().toString())) {
            name.setError("Name should not be empty");
        } else if (TextUtils.isEmpty(website.getText().toString())) {
            website.setError("Website should not be empty");
        } else if (TextUtils.isEmpty(description.getText().toString())) {
            description.setError("Description should not be empty");
        } else if (TextUtils.isEmpty(tv_startDate.getText().toString())) {
            tv_startDate.setError("Description should not be empty");
        } else if (TextUtils.isEmpty(tv_Est_End_Date.getText().toString())) {
            tv_Est_End_Date.setError("Description should not be empty");
        } else if (TextUtils.isEmpty(city.getText().toString())) {
            city.setError("Description should not be empty");
        } else if (TextUtils.isEmpty(country.getText().toString())) {
            country.setError("Description should not be empty");
        } else if (charity_type_string.equals("0")) {
            Common.Toast_Message(Add_Project_Activity.this, "Please select Charity type");
        } else if (target_audience_string.equals("0")) {
            Common.Toast_Message(Add_Project_Activity.this, "Please select Target Audience");
        } else if (target_anualspend_string.equals("0")) {
            Common.Toast_Message(Add_Project_Activity.this, "Please select Target Anual Spend");
        } else {
            loading_dialog.show();
            AddProjectFunction();
        }

    }

    private void AddProjectFunction() {

        final AddProjectRequestModel request = new AddProjectRequestModel();
        request.setCreator_id(Common.login_data.getData().getId()); //user id
        request.setKhoja_care_category_id("1");  //static for now...will be change

        request.setName(name.getText().toString());
        request.setWebsite(website.getText().toString());
        request.setDescription(description.getText().toString());
        request.setStart_date(tv_startDate.getText().toString());
        request.setEst_completion_date(tv_Est_End_Date.getText().toString());
        request.setCity(city.getText().toString());
        request.setCountry(country.getText().toString());
        request.setStatus("a"); //not know y

        request.setCharity_type(charity_type_string);
        request.setTargetaudience(target_audience_string);
        request.setTargetanualspent(target_anualspend_string);

        Call<AddProjectResponseModel> responseCall = retrofitInterface.AddProject("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<AddProjectResponseModel>() {
            @Override
            public void onResponse(Call<AddProjectResponseModel> call, Response<AddProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    AddProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                      if (status.equals("error")) {
                        //data is null
                        loading_dialog.dismiss();
                        Toast.makeText(Add_Project_Activity.this, "Project added failed.", Toast.LENGTH_SHORT).show();

                    } else if (status.equals("success")) {
                        if (AddProjectInGroupFlag.equals("1")) {
                            Log.d("AddProjectIssue","add group to project");
                            AddGroupToProject(Group_Id, responseModel.getData().getProject_id());
                        } else if (AddProjectInGroupFlag.equals("0")) {
                            loading_dialog.dismiss();
                            Log.d("AddProjectIssue","add project");
                            Toast.makeText(Add_Project_Activity.this, "Project added Successfully.", Toast.LENGTH_SHORT).show();
                            Intent list = new Intent(Add_Project_Activity.this, Projects_Activity.class);
                            list.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(list);
                            finish();
                        }


                    }


                } else {
                    loading_dialog.dismiss();
                    Log.d("AddProjectIssue","failure code:"+response.code());
                }

            }

            @Override
            public void onFailure(Call<AddProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Log.d("AddProjectIssue","on failure :"+t.getMessage());
             }
        });


    }

    private void AddGroupToProject(String group_id, String project_id) {


        Add_group_To_ProjectRequestModel requestModel = new Add_group_To_ProjectRequestModel();

        requestModel.setGroup_id(group_id);
        requestModel.setProject_id(project_id);


        Call<Add_group_To_ProjectResponseModel> responseCall = retrofitInterface.AddGroupToProject("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<Add_group_To_ProjectResponseModel>() {
            @Override
            public void onResponse(Call<Add_group_To_ProjectResponseModel> call, Response<Add_group_To_ProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    loading_dialog.dismiss();

                    Add_group_To_ProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                      Toast.makeText(Add_Project_Activity.this, "Project added to Group successfully", Toast.LENGTH_LONG).show();
                    Intent add_project = new Intent(Add_Project_Activity.this, GroupsDetail.class);

                    add_project.putExtra("group_data_detail",group_data_detail);
                    add_project.putExtra("all_group_data_detail",all_group_data_detail);
                    add_project.putExtra("ViewProfileBussinessData",user_profile_businessandprojects_data);
                    add_project.putExtra("ViewProfileCharityData",user_profile_charitiesandprojects_data);

                    add_project.putExtra("isAllGroupDetail",isAllGroupDetail);
                    add_project.putExtra("ViewProfileDetail",ViewProfileDetail);
                    add_project.putExtra("isProfileBusinessData",isProfileBusinessData);


                    add_project.putExtra("Group_Id",Group_Id);
                    add_project.putExtra("AddProjectInGroupFlag","1");


                    add_project.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(add_project);
                    finish();
                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Add_Project_Activity.this, "Project added to Group Failed", Toast.LENGTH_LONG).show();
                 }

            }

            @Override
            public void onFailure(Call<Add_group_To_ProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Add_Project_Activity.this, "Project added to Group Failed", Toast.LENGTH_LONG).show();
             }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
