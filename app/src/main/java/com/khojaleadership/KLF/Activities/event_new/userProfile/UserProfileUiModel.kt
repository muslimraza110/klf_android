package com.khojaleadership.KLF.Activities.event_new.userProfile

data class UserProfileUiModel(
        val name: String,
        val facebook: String,
        val twitter: String,
        val linkedIn: String
)