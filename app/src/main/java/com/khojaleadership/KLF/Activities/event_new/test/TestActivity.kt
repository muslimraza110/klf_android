package com.khojaleadership.KLF.Activities.event_new.test

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.R
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import kotlinx.android.synthetic.main.activity_test.*


class TestActivity : AppCompatActivity() {


    // Initialize the Adapter
    var flexibleAdapter = FlexibleAdapter(listOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        rv_list?.apply {
            adapter = flexibleAdapter
            layoutManager = LinearLayoutManager(this@TestActivity, LinearLayoutManager.VERTICAL, false)
        }

        var items = mutableListOf<IFlexible<*>>()


        for (j in 0..5) {

            var headerItem = HeaderItem(
                    title = "$j",
                    isOpened = false
            )

            items.add(headerItem)
            for (i in 0..1000) {

                var item = Item(
                        name = "Sibtein Asaria$i",
                        userId = "",
                        description = "Sibtein Asaria is the Founder of Grupo Alfa Corporation," +
                                " a leading business conglomerate based in the Republic of Costa Rica" +
                                " and with interest on three $i more continents",
                        facebook = "https://www.facebook.com/racerrhell",
                        twitter = "https://www.twitter.com/osama_muhammad0",
                        linkedIn = "https://www.linkedin.com/",
                        photoUrl = "https://picsum.photos/20${i % 10}",
                        designation = "Founder, Grupo Alfa corporation$i",
                        onClick = {
                            Toast.makeText(this, it.name, Toast.LENGTH_SHORT).show()
                        },
                        onTwitterClick = {},
                        onFacebookClick = {},
                        availableSlotData = null,
                        header = headerItem,
                        context = this
                )

                headerItem.addSubItem(item)
            }
        }


        flexibleAdapter.setDisplayHeadersAtStartUp(true)
        flexibleAdapter.setStickyHeaders(true)
        flexibleAdapter.updateDataSet(items)

    }
}