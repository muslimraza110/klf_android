package com.khojaleadership.KLF.Activities.event_new.hotelRegistration

import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.khojaleadership.KLF.databinding.ActivityHotelRegistrationBinding

class HotelRegistrationActivity : AppCompatActivity() {

    private val viewModel: HotelRegistrationViewModel by viewModels()

    lateinit var binding: ActivityHotelRegistrationBinding

//    private lateinit var hotelPackagesAdapter: HotelPackagesAdapter
//    private lateinit var orderSummaryAdapter: OrderSummaryAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityHotelRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setUpObservers()
        viewModel.fetchData()
    }

    private fun setUpObservers() {
//        viewModel.hotelPackages.observe(this, Observer {
//            it?.let {
//                hotelPackagesAdapter.submitList(it)
//            }
//        })
//
//        viewModel.orderSummary.observe(this, Observer {
//            it?.let {
//                orderSummaryAdapter.submitList(it)
//            }
//        })

    }

    private fun setUpViews() {
        binding.imageBack.setOnClickListener {
            finish()
        }

//        hotelPackagesAdapter = HotelPackagesAdapter()
//        hotelPackagesAdapter.setHasStableIds(true)
//
//        binding.rvHotelPkgs.apply {
//            adapter = hotelPackagesAdapter
//            layoutManager = LinearLayoutManager(this@HotelRegistrationActivity)
//        }
//
//        orderSummaryAdapter = OrderSummaryAdapter()
//        orderSummaryAdapter.setHasStableIds(true)
//
//        binding.rvOrderSummary.apply {
//            adapter = orderSummaryAdapter
//            layoutManager = LinearLayoutManager(this@HotelRegistrationActivity)
//        }
    }
}
