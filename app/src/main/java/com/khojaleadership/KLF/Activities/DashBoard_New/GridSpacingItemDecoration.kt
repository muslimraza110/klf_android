package com.khojaleadership.KLF.Activities.DashBoard_New

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridSpacingItemDecoration(
        private val context: Context,
        private val spanCount: Int,
        private val spaceInDp: Int,
        private val includeEdge: Boolean)
    : RecyclerView.ItemDecoration() {
    var spaceInPx: Int = 0

    init {
        spaceInPx = Math.round(spaceInDp * context.resources.displayMetrics.density)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view) // item position

        val column = position % spanCount // item column


        if (includeEdge) {
            outRect.left = spaceInPx - column * spaceInPx / spanCount // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * spaceInPx / spanCount // (column + 1) * ((1f / spanCount) * spacing)
            if (position < spanCount) { // top edge
                outRect.top = spaceInPx
            }
            outRect.bottom = spaceInPx // item bottom
        } else {
            outRect.left = column * spaceInPx / spanCount // column * ((1f / spanCount) * spacing)
            outRect.right = spaceInPx - (column + 1) * spaceInPx / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spaceInPx // item top
            }
        }
    }
}