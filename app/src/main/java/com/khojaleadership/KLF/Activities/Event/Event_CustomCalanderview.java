package com.khojaleadership.KLF.Activities.Event;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Adapter.Event.Event_Dialoge_Adapter;
import com.khojaleadership.KLF.Adapter.Event.Event_CustomCalanderview_Adapter;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Event_CustomCalanderview extends LinearLayout implements RecyclerViewClickListner {
    ImageView NextButton, PreviousButton;
    TextView CurrentDate;
    GridView gridView;
    private static final int MAX_CALENDAR_DAYS = 42;
    Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
    Context context;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    SimpleDateFormat month_numeric = new SimpleDateFormat("MM", Locale.ENGLISH);
    SimpleDateFormat yaer_numeric = new SimpleDateFormat("yyyy", Locale.ENGLISH);


    Event_CustomCalanderview_Adapter myGridAdapter;
    AlertDialog alertDialog;
    List<Date> dates = new ArrayList<>();

    Dialog loading_dialog;
    String month, year;

    ArrayList<GetEventsListDataModel> all_event_list=new ArrayList<>();
    ArrayList<GetEventsListDataModel> events_by_month=new ArrayList<>();
    ArrayList<GetEventsListDataModel> event_by_day_list=new ArrayList<>();


    public Event_CustomCalanderview(Context context) {
        super(context);
    }

    public Event_CustomCalanderview(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        IntializeLayout();
        SetUpCalendar();

        PreviousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, -1);
                SetUpCalendar();
            }
        });

        NextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, 1);
                SetUpCalendar();
            }
        });


    }

    public void PassData(ArrayList<GetEventsListDataModel> parsed_all_event_list){
        all_event_list=parsed_all_event_list;
    }

//    public void PassEventsByDay(ArrayList<GetEventsListDataModel> events_by_day){
//        event_by_day_list=events_by_day;
//    }

    public Event_CustomCalanderview(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    void IntializeLayout() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.event_calander_layout, this);
        NextButton = view.findViewById(R.id.btn_next);
        PreviousButton = view.findViewById(R.id.btn_pre);
        CurrentDate = view.findViewById(R.id.txt_currentDate);
        gridView = view.findViewById(R.id.gridview);

        loading_dialog = Common.LoadingDilaog(getContext());
    }

    void SetUpCalendar() {

        try {

            String currwntDate = dateFormat.format(calendar.getTime());

            //setting for API use
            month = month_numeric.format(calendar.getTime());
            year = yaer_numeric.format(calendar.getTime());


            CurrentDate.setText(currwntDate);
            dates.clear();
            Calendar monthCalendar = (Calendar) calendar.clone();
            monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
            int FirstDayofMonth = monthCalendar.get(Calendar.DAY_OF_WEEK) - 1;
            monthCalendar.add(Calendar.DAY_OF_MONTH, -FirstDayofMonth);

            while (dates.size() < MAX_CALENDAR_DAYS) {
                dates.add(monthCalendar.getTime());
                monthCalendar.add(Calendar.DAY_OF_MONTH, 1);

            }

            loading_dialog.show();
            CollectEventsPerMonthManually(year, month);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }

    public void CollectEventsPerMonthManually(String year, String month) {

        //ArrayList<GetEventsListDataModel> list = Common.all_event_list;

        ArrayList<GetEventsListDataModel> list = all_event_list;

        ArrayList<GetEventsListDataModel> Collected_list = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getStart_date() != null) {
                String[] space_split = list.get(i).getStart_date().split(" ");
                if (space_split != null) {
                    if (space_split.length > 0) {
                        String date = space_split[0];

                        String[] split_date = date.split("-");

                        if (split_date != null) {
                            if (split_date.length > 0) {
                                String y = split_date[0];
                                String m = split_date[1];

                                if (y.equals(year) && m.equals(month)) {
                                     Collected_list.add(list.get(i));
                                }
                            }
                        }
                    }
                }
            }
        }

        //Common.events_by_month = Collected_list;

        events_by_month=Collected_list;
        intialize_grid_view();
        loading_dialog.dismiss();
    }

    public void intialize_grid_view() {

        try {

            myGridAdapter = new Event_CustomCalanderview_Adapter(context, dates, calendar, events_by_month, this);
            gridView.setAdapter(myGridAdapter);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }

    public void custom_view(int position, View parent) {

        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(true);

            View showView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_custom_dialog_updated, null);

            RelativeLayout close = showView.findViewById(R.id.custom_close_btn);
            close.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            RecyclerView recyclerView = showView.findViewById(R.id.EventsRV);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(showView.getContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);

            Event_Dialoge_Adapter eventRecyclerAdapter = new Event_Dialoge_Adapter(showView.getContext(), Common.events_by_day);

            //Event_Dialoge_Adapter eventRecyclerAdapter = new Event_Dialoge_Adapter(showView.getContext(), event_by_day_list);
            recyclerView.setAdapter(eventRecyclerAdapter);
            eventRecyclerAdapter.notifyDataSetChanged();

            builder.setView(showView);
            alertDialog = builder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }


    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {

        try {

            if (Common.events_by_day.size() > 0) {
                custom_view(position, v);
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }
    }

}
