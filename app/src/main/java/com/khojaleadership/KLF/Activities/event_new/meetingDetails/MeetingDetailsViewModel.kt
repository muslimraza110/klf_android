package com.khojaleadership.KLF.Activities.event_new.meetingDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.meetings.MeetingHeaderItem
import com.khojaleadership.KLF.Helper.Event
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore

class MeetingDetailsViewModel(val app: Application) : AndroidViewModel(app)  {

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _updateDelegateSelectionEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val updateDelegateSelectionEvent : LiveData<Event<Boolean>> = _updateDelegateSelectionEvent

    private val _refreshMeetingDataEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val refreshMeetingDataEvent : LiveData<Event<Boolean>> = _refreshMeetingDataEvent

    private val _refreshRequestMeetingEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val refreshRequestMeetingEvent: LiveData<Event<Boolean>> = _refreshRequestMeetingEvent

    private val _showMeetingsFragmentEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val showMeetingsFragmentEvent: LiveData<Event<Boolean>> = _showMeetingsFragmentEvent

    private val _showRequestMeetingFragmentEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val showRequestMeetingFragmentEvent: LiveData<Event<Boolean>> = _showRequestMeetingFragmentEvent

    private val _editMeetingEvent: MutableLiveData<Event<MeetingHeaderItem>> = MutableLiveData()
    val editMeetingEvent: LiveData<Event<MeetingHeaderItem>> = _editMeetingEvent

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    fun setEventState(result: Result<Unit>){
        _eventState.value  = result
    }

    fun updateDelegateSelection() {
        _updateDelegateSelectionEvent.value = Event(true)
    }

    fun refresh(){
        _refreshMeetingDataEvent.value = Event(true)
        _refreshRequestMeetingEvent.value = Event(true)
    }

    fun refreshRequestMeeting(){
        _refreshRequestMeetingEvent.value = Event(true)
    }

    fun showMeetingsFragment(){
        _showMeetingsFragmentEvent.value = Event(true)
    }


    fun showRequestMeetingFragment(){
        _showRequestMeetingFragmentEvent.value = Event(true)
    }


    fun editMeeting(meetingHeaderItem: MeetingHeaderItem) {
        _editMeetingEvent.value = Event(meetingHeaderItem)
        showRequestMeetingFragment()
    }

}