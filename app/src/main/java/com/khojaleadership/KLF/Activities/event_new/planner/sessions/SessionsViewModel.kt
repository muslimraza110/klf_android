package com.khojaleadership.KLF.Activities.event_new.planner.sessions

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class SessionsViewModel(application: Application) : AndroidViewModel(application) {

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    private val _sessions: MutableLiveData<List<SessionItemUiModel>> = MutableLiveData()
    val sessions: LiveData<List<SessionItemUiModel>> = _sessions

    init {
        getSessions()
    }

    private fun getSessions() {
        _sessions.value = authStore.sessionsModel?.sessionsList ?: listOf()
    }

    fun joinOrLeaveBreakoutSession(item: SessionItemUiModel) {

        _eventState.value = Result.Loading

        val enroll = !item.iAmEnrolled

        viewModelScope.launchSafely(
                block = {
                    val response = eventsRepository.joinBreakoutSession(
                            facultyId = authStore.event?.facultyId ?: "",
                            sessionId = item.sessionId,
                            enroll = enroll
                    )

                    if (response.status == ResponseStatus.SUCCESS) {

                        authStore.meetingRequestSent = true

                        val previouslyJoinedSessionId = _sessions.value?.findLast {
                            it.iAmEnrolled
                        }?.sessionId

                        previouslyJoinedSessionId?.let {
                            if (previouslyJoinedSessionId == item.sessionId) {
//                                  Leaving a joined session
                                _sessions.value?.forEach {
                                    if (it.sessionId == item.sessionId) {
                                        it.roomOccupied -= 1
                                    }
                                }
                            } else {
//                                  Leaving and Joining a new Session
                                _sessions.value?.forEach {
                                    if (it.sessionId == item.sessionId) {
                                        it.roomOccupied += 1
                                    } else if (it.sessionId == previouslyJoinedSessionId) {
                                        it.roomOccupied -= 1
                                    }
                                }
                            }
                        } ?: run {
//                                  Joining a new session
                            _sessions.value?.forEach {
                                if (it.sessionId == item.sessionId) {
                                    it.roomOccupied += 1
                                }
                            }
                        }



                        val tempList = _sessions.value
                        tempList?.forEach {
                            if (it.sessionId == item.sessionId) {
                                it.iAmEnrolled = !item.iAmEnrolled
                            } else {
                                it.iAmEnrolled = false
                            }
                        }

                        _sessions.value = tempList

                        _eventState.value = Result.Success(
                                message = response.message,
                                data = Unit
                        )

                        _messageEvent.value = Event(response.message ?: "")
                    } else {
                        _eventState.value = Result.Error(
                                message = response.message ?: Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(response.message
                                ?: Common.somethingWentWrongMessage)
                    }

                },
                error = {
                    Timber.e(it)
                    _eventState.value = Result.Error(
                            message = Common.somethingWentWrongMessage
                    )
                    _messageEvent.value = Event(Common.somethingWentWrongMessage)

                }
        )
    }
}
