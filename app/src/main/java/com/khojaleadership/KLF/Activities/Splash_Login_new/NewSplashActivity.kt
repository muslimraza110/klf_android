package com.khojaleadership.KLF.Activities.Splash_Login_new

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.khojaleadership.KLF.R

class NewSplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_splash)
        Handler().postDelayed(
                Runnable {
                    startActivity(Intent(this, NewLoginActivity::class.java))
                    finish()
                },
                2000
        )
    }
}
