package com.khojaleadership.KLF.Activities.event_new.timePicker

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.khojaleadership.KLF.R
import java.util.*

class TimePickerDialogFragment: DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current time as the default values for the picker
        val c = Calendar.getInstance()
        if(arguments?.containsKey(TIMESTAMP) == true){
            c.timeInMillis = arguments?.getLong(TIMESTAMP) ?: c.timeInMillis
        }
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(requireActivity(), R.style.DateTimePickerDialogTheme, targetFragment as TimePickerDialog.OnTimeSetListener, hour, minute, false)
    }

    companion object{
        const val TIMESTAMP = "timeStamp"
    }
}