package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddSubTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddSubTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Sub_Topic extends AppCompatActivity {

    ScrollView scrollView;
    private RadioGroup mFirstGroup;
    private RadioGroup mSecondGroup;

    CheckBox pinned_post, show_on_dashboarrd, add_recipient, post_approve_post;
    String pinned_post_Text = "0", sod_text = "0", add_recipient_text = "0", post_approved_text = "0";

    private boolean isChecking = true;
    private int mCheckedId = R.id.radio_public;
    String post_visibility = "P";


    EditText title;
    RichEditor mEditor;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog loading_dialog;


    Boolean flag = false;
    TextView header_title;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    String Category;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__sub__topic);

        loading_dialog = Common.LoadingDilaog(this);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        //getting data
        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null) {
            Category = bundle.getString("Category");
        }

        //view intialization
        intialize_view();

    }

    public void intialize_view(){

        try {

            header_title = findViewById(R.id.c_header_title);
            header_title.setText("Add Subtopic");

            findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.hideKeyboard(v, Add_Sub_Topic.this);


                    Common.hideKeyboard(v, getApplicationContext());
                    Intent i = new Intent(Add_Sub_Topic.this, Sub_Topics.class);
                    i.putExtra("TopicDataDetail",topic_data_detail);
                    i.putExtra("Category", Category);
                    startActivity(i);
                    finish();
                }
            });

            LinearLayout top_check_box_layout, visibilty_layout, moderator_layout;
            top_check_box_layout = findViewById(R.id.top_check_box_layout);
            visibilty_layout = findViewById(R.id.visibilty_layout);
            moderator_layout = findViewById(R.id.moderator_layout);

            if (Common.is_Admin_flag == false) {
                top_check_box_layout.setVisibility(View.GONE);
                visibilty_layout.setVisibility(View.GONE);
                moderator_layout.setVisibility(View.GONE);

            }

            add_recipient = (CheckBox) findViewById(R.id.add_recipient_checkbox);
            add_recipient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        closeKeyboard();
                    }
                }
            });

            show_on_dashboarrd = (CheckBox) findViewById(R.id.add_dashboard_checkbox);
            show_on_dashboarrd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        sod_text = "1";

                        //closeKeyboard();
                    } else if (isChecked == false) {
                        sod_text = "0";
                    }
                }
            });


            pinned_post = (CheckBox) findViewById(R.id.add_pinned_post_checkbox);
            pinned_post.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        pinned_post_Text = "1";
                    } else if (isChecked == false) {
                        pinned_post_Text = "0";
                    }
                }
            });


            post_approve_post = (CheckBox) findViewById(R.id.add_post_approved);
            post_approve_post.setChecked(false);
            post_approve_post.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {

                        post_approve_post.setChecked(true);
                        post_approved_text = "1";

                        closeKeyboard();

                    } else {
                        post_approve_post.setChecked(false);
                        post_approved_text = "0";
                    }
                }
            });


            title = (EditText) findViewById(R.id.add_sub_topic_title);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(title, InputMethodManager.SHOW_IMPLICIT);
            mEditor = (RichEditor) findViewById(R.id.editor);
            mEditor.setEditorHeight(250);
            mEditor.setEditorFontSize(22);
            closeKeyboard();

//        mEditor.setVerticalScrollBarEnabled(true);
            mEditor.setScrollbarFadingEnabled(false);
            //mEditor.setVerticalScrollbarOverlay(true);
            mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
            mEditor.setScrollBarSize(5);
//        mEditor.isScrollContainer();
            mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
            mEditor.canScrollVertically(0);
            mEditor.setVerticalScrollBarEnabled(true);
            mEditor.setMotionEventSplittingEnabled(true);
            mEditor.setFontSize(12);
            mEditor.setEditorFontSize(12);
            mEditor.setEditorFontColor(Color.BLACK);
            mEditor.setPadding(10, 10, 10, 10);

            //for smooth scrooling
            scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
            scrollView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                    //  We will have to follow above for all scrollable contents
                    return false;
                }
            });

            mEditor.setPlaceholder("Description");

            mEditor.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View p_v, MotionEvent p_event) {
                    // this will disallow the touch request for parent scroll on touch of child view
                    p_v.getParent().requestDisallowInterceptTouchEvent(true);

                    action_align_left = findViewById(R.id.action_align_left);
                    action_align_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_left_flag == false) {
                                action_left_flag = true;
                                action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                                action_centr_flag = false;
                                action_right_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                                action_align_right.setBackgroundResource(R.drawable.ic_right);

                                mEditor.setAlignLeft();
                            } else if (action_left_flag == true) {
                                action_left_flag = false;
                                action_align_left.setBackgroundResource(R.drawable.ic_left);
                            }

                        }
                    });

                    action_align_center = findViewById(R.id.action_align_center);
                    action_align_center.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_centr_flag == false) {
                                action_centr_flag = true;
                                action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                                action_left_flag = false;
                                action_right_flag = false;
                                action_align_left.setBackgroundResource(R.drawable.ic_left);
                                action_align_right.setBackgroundResource(R.drawable.ic_right);

                                mEditor.setAlignCenter();
                            } else if (action_centr_flag == true) {
                                action_centr_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                            }
                        }
                    });

                    action_align_right = findViewById(R.id.action_align_right);
                    action_align_right.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_right_flag == false) {
                                action_right_flag = true;
                                action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                                action_centr_flag = false;
                                action_left_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                                action_align_left.setBackgroundResource(R.drawable.ic_left);

                                mEditor.setAlignRight();
                            } else if (action_right_flag == true) {
                                action_right_flag = false;
                                action_align_right.setBackgroundResource(R.drawable.ic_right);
                            }

                        }
                    });


                    return false;
                }
            });


            mFirstGroup = (RadioGroup) findViewById(R.id.first_group);
            mSecondGroup = (RadioGroup) findViewById(R.id.second_group);


            mFirstGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId != -1 && isChecking) {
                        isChecking = false;
                        mSecondGroup.clearCheck();

                        mCheckedId = checkedId;
                        if (mCheckedId == R.id.radio_public) {
                            post_visibility = "P";
                        } else if (mCheckedId == R.id.radio_Sami_private) {
                            post_visibility = "S";
                        }


                    }
                    isChecking = true;
                }
            });


            mSecondGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId != -1 && isChecking) {
                        isChecking = false;
                        mFirstGroup.clearCheck();
                        mCheckedId = checkedId;

                        if (mCheckedId == R.id.radio_Restricted) {
                            post_visibility = "W";
                        } else if (mCheckedId == R.id.radio_admin) {
                            post_visibility = "A";
                        }

                    }
                    isChecking = true;
                }
            });


            findViewById(R.id.publish_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (title.getText().toString().equalsIgnoreCase("") ||
                            title.getText().toString().isEmpty()) {
                        title.setError("Title should not be empty.");
                    } else if (title.getText().toString().replace(" ", "").equals("")) {
                        title.setError("Title should not contain only space.");
                    } else if (mEditor.getHtml() == null) {
                        Toast.makeText(Add_Sub_Topic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (mEditor.getHtml().equals("")) {
                        Toast.makeText(Add_Sub_Topic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (mEditor.getHtml().equalsIgnoreCase("")) {
                        Toast.makeText(Add_Sub_Topic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else {


//                    aik dafa button press py add kre
                        if (flag == false) {
                            flag = true;

                            loading_dialog.show();
                            onAddSubTopicBtnClick();
                        }
                    }
                }


            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }
    }


    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        Intent i = new Intent(Add_Sub_Topic.this, Sub_Topics.class);
        i.putExtra("TopicDataDetail",topic_data_detail);
        i.putExtra("Category", Category);
        startActivity(i);
        finish();
    }

    private void onAddSubTopicBtnClick() {

        try {

            AddSubTopicRequestModel requestModel = new AddSubTopicRequestModel();

            requestModel.setUser_id(Common.login_data.getData().getId());
            requestModel.setSubforum_id(topic_data_detail.getSubforums__id());
            requestModel.setTopic(title.getText().toString());
            requestModel.setContent(mEditor.getHtml());
            requestModel.setPinned(pinned_post_Text);
            requestModel.setSod(sod_text);
            requestModel.setEblast("0");
            requestModel.setPost_visibility(post_visibility);
            requestModel.setPost_approved_post(post_approved_text);


            Call<AddSubTopicResponseModel> responseCall = retrofitInterface.addSubTopic("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddSubTopicResponseModel>() {
                @Override
                public void onResponse(Call<AddSubTopicResponseModel> call, Response<AddSubTopicResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        AddSubTopicResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        //finish();
                        loading_dialog.dismiss();
                        if (status.equals("success")) {
                            Intent sub_topic_intent = new Intent(Add_Sub_Topic.this, Sub_Topics.class);
                            sub_topic_intent.putExtra("TopicDataDetail",topic_data_detail);
                            sub_topic_intent.putExtra("Category", Category);
                            startActivity(sub_topic_intent);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddSubTopicResponseModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}



