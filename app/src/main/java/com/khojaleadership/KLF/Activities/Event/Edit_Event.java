package com.khojaleadership.KLF.Activities.Event;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Event_Model.AddEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.AddEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.EditEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.EditEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Group_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Project_Model;
import com.khojaleadership.KLF.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Event extends AppCompatActivity {

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    EditText event_name, city, country;
    TextView start_date, end_date, start_time, end_time;
    Button delete_btn;
    LinearLayout delete_event_layout;
    RichEditor mEditor;

    DatePickerDialog.OnDateSetListener mDateSetListener;
    DatePickerDialog.OnDateSetListener mDateSetListener1;


    Spinner project_spinner;
    String project_string;

    Spinner group_spinner;
    String group_string = "0";


    TextView header_title;
    String FromDate = "05/01/2018", ToDate = "07/01/2018";
    SimpleDateFormat dateFormat;

    Calendar dateSelected = Calendar.getInstance();
    String time;
    String AM_PM_text;
    private DatePickerDialog datePickerDialog;


    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    GetEventsListDataModel event_data_detail;

    String is_event_edit="0";
    ArrayList<String> project_name_list = new ArrayList<>();
    ArrayList<String> project_id_list = new ArrayList<>();

    ArrayList<View_Profile_Group_Model> user_profile_groups = new ArrayList<>();
    ArrayList<View_Profile_Project_Model> user_profile_groupsproject = new ArrayList<>();

    ArrayList<String> all_group_name_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__event);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        header_title = findViewById(R.id.c_header_title);

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Edit_Event.this);
                finish();
            }
        });


        //getting Object data
        Intent intent= getIntent();
        event_data_detail=intent.getParcelableExtra("EventData");

        //getting string data
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null) {
            is_event_edit = bundle.getString("is_event_edit");
        }else {
            is_event_edit="0";
        }
        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        getProfileData();

        findViewById(R.id.save_event_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(event_name.getText().toString())) {
                    event_name.setError("field should not be empty.");
                } else if (group_string.equals("0")) {
                    Common.Toast_Message(Edit_Event.this, "Please select --Category--");
                } else if (TextUtils.isEmpty(city.getText().toString())) {
                    city.setError("field should not be empty.");
                } else if (TextUtils.isEmpty(country.getText().toString())) {
                    country.setError("field should not be empty.");
                } else if (TextUtils.isEmpty(start_date.getText().toString())) {
                    start_date.setError("field should not be empty.");
                } else if (TextUtils.isEmpty(end_date.getText().toString())) {
                    end_date.setError("field should not be empty.");
                } else if (TextUtils.isEmpty(start_time.getText().toString())) {
                    start_time.setError("field should not be empty.");
                } else if (TextUtils.isEmpty(end_time.getText().toString())) {
                    end_time.setError("field should not be empty.");
                } else {
                    loading_dialog.show();

                    if (is_event_edit.equals("0")) {
                        AddEventFunction();
                    } else if (is_event_edit.equals("1")) {
                        EditEventFunction();
                    }

                }
            }
        });


        delete_btn = findViewById(R.id.delete_event);
        delete_event_layout = findViewById(R.id.delete_event_layout);
        if (is_event_edit.equals("0")) {
            delete_event_layout.setVisibility(View.GONE);
        }
        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Confirm_Dialog("Delete Event", "Are you sure to delete this event ?", "Yes");

            }
        });


    }

    private void getProfileData() {


        try {

            Call<View_Profile_Model> responseCall = retrofitInterface.GetProfileData("application/json", Common.auth_token, Common.login_data.getData().getId());

            responseCall.enqueue(new Callback<View_Profile_Model>() {
                @Override
                public void onResponse(Call<View_Profile_Model> call, Response<View_Profile_Model> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        View_Profile_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                          //saving data

                        if (user_profile_groups != null) {
                            user_profile_groups.clear();
                        }

                        if (user_profile_groupsproject != null) {
                            user_profile_groupsproject.clear();
                        }

                        user_profile_groups = responseModel.getGroups();
                        user_profile_groupsproject = responseModel.getGroupsproject();


                        ArrayList<String> group_name_list = new ArrayList<>();
                        group_name_list.add("Business & Charities");

                        if (responseModel.getGroups() != null) {
                            if (responseModel.getGroups().size() > 0) {
                                for (int i = 0; i < responseModel.getGroups().size(); i++) {
                                    group_name_list.add(responseModel.getGroups().get(i).getGroups__name());
                                }
                            }
                        }

                        if (all_group_name_list != null) {
                            all_group_name_list.clear();
                        }

                        all_group_name_list = group_name_list;

                        //in case of not selected any group
                        ArrayList<String> project_name_list = new ArrayList<>();
                        project_name_list.add("Project");

                        if (project_name_list != null) {
                            project_name_list.clear();
                        }

                        //Common.project_name_list = project_name_list;
                        project_name_list = project_name_list;

                        intialize_view();

                    } else {

                        loading_dialog.dismiss();
                         intialize_view();
                    }


                }

                @Override
                public void onFailure(Call<View_Profile_Model> call, Throwable t) {
                    //loading_dialog.dismiss();
                    loading_dialog.dismiss();
                    intialize_view();
                 }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    public void GroupSpinnerView() {


        try {

             group_spinner = findViewById(R.id.event_group_spinner);

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, all_group_name_list) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    tv.setTextColor(Color.BLACK);
                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.spinner_item);

            group_spinner.setAdapter(adapter);
            group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
                    if (position > 0) {
                        group_string = user_profile_groups.get(position - 1).getGroup_id();

//                        if (Common.project_name_list != null) {
//                            Common.project_name_list.clear();
//                        }

//                        if (Common.project_id_list != null) {
//                            Common.project_id_list.clear();
//                        }

                        ArrayList<String> local_project_name_list = new ArrayList<>();
                        local_project_name_list.add("Select Project");
                        ArrayList<String> local_project_id_list = new ArrayList<>();

                        for (int i = 0; i < user_profile_groupsproject.size(); i++) {

                            if (user_profile_groupsproject.get(i).getGroup_id() != null) {
                                if (group_string.equals(user_profile_groupsproject.get(i).getGroup_id())) {
                                    local_project_name_list.add(user_profile_groupsproject.get(i).getProjects__name());
                                    local_project_id_list.add(user_profile_groupsproject.get(i).getProject_id());
                                  }
                            }

                        }

//
//                        if (Common.project_name_list != null) {
//                            Common.project_name_list.clear();
//                        }

//                        if (Common.project_id_list != null) {
//                            Common.project_id_list.clear();
//                        }
//                        Common.project_name_list = project_name_list;

                        project_id_list = local_project_id_list;
                        project_name_list = local_project_name_list;

                        ProjectSpinnerView();
                    } else {
                        group_string = "0";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            //edit case.................................
            int group_list_index = 0;
            if (is_event_edit.equals("0")) {
                //ignore
            } else {
                if (user_profile_groups != null) {
                    for (int i = 0; i < user_profile_groups.size(); i++) {

                        if (event_data_detail.getGroup_id() != null && user_profile_groups.get(i).getGroup_id() != null) {
                            if (event_data_detail.getGroup_id().equals(user_profile_groups.get(i).getGroup_id())) {
                                 group_list_index = i + 1;
                            }
                        }
                    }
                }

            }

            group_spinner.setSelection(group_list_index);


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    public void ProjectSpinnerView() {

        try {

             project_spinner = findViewById(R.id.event_project_spinner);

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, project_name_list) {
                @Override
                public boolean isEnabled(int position) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    tv.setTextColor(Color.BLACK);

                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.spinner_item);

            project_spinner.setAdapter(adapter);
            project_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);

                    if (position > 0) {
                        if (project_id_list != null) {
                            if (project_id_list.size() >= position) {
                                try {
                                    project_string = project_id_list.get(position - 1);
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    throw e;
                                }
                            }
                        }
                    } else {
                        project_string = "0";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            int project_list_index = 0;
            if (is_event_edit.equals("0")) {
                //ignore
            } else {
                if (project_id_list != null) {
                    for (int i = 0; i < project_id_list.size(); i++) {

                        if (project_id_list != null) {
                            if (project_id_list.size() != 0) {

                                if (event_data_detail.getProject_id() != null && project_id_list.get(i) != null) {
                                    if (event_data_detail.getProject_id().equals(project_id_list.get(i))) {
                                        project_string = project_id_list.get(i);
                                        project_list_index = (i + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            project_spinner.setSelection(project_list_index);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private void AddEventFunction() {

        try {

            AddEventRequestModel request = new AddEventRequestModel();
            request.setGroup_id(group_string);  //static
            request.setUser_id(Common.login_data.getData().getId());   //static
            request.setProject_id(project_string);  //static
            request.setName(event_name.getText().toString());
            request.setStart_date(start_date.getText().toString() + " " + start_time.getText().toString());
            request.setEnd_date(end_date.getText().toString() + " " + end_time.getText().toString());
            request.setAll_day("1");   //static for now
            request.setDescription(mEditor.getHtml());
            request.setCity(city.getText().toString());
            request.setCountry(country.getText().toString());


            Call<AddEventResponseModel> call = retrofitInterface.AddEvent("application/json", Common.auth_token, request);

            call.enqueue(new Callback<AddEventResponseModel>() {
                @Override
                public void onResponse(Call<AddEventResponseModel> call, Response<AddEventResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                         if (status.equals("success")) {
                            loading_dialog.dismiss();
                            Toast.makeText(Edit_Event.this, "Event added successfully.", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Edit_Event.this, EventActivity_Updated.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        } else {
                            loading_dialog.dismiss();
                         }

                    } else {
                        loading_dialog.dismiss();
                     }
                }

                @Override
                public void onFailure(Call<AddEventResponseModel> call, Throwable t) {
                     loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private void EditEventFunction() {

        try {

            EditEventRequestModel request = new EditEventRequestModel();

            request.setGroup_id(group_string);

            request.setUser_id(Common.login_data.getData().getId());

            request.setProject_id(project_string);

            request.setName(event_name.getText().toString());


            request.setStart_date(start_date.getText().toString() + " " + start_time.getText().toString());

            request.setEnd_date(end_date.getText().toString() + " " + end_time.getText().toString());


            request.setAll_day("1");   //static for now
            request.setDescription(mEditor.getHtml());
             request.setCity(city.getText().toString());
             request.setCountry(country.getText().toString());
             request.setEvent_id(event_data_detail.getId());


            Call<EditEventResponseModel> call = retrofitInterface.EditEvent("application/json", Common.auth_token, request);

            call.enqueue(new Callback<EditEventResponseModel>() {
                @Override
                public void onResponse(Call<EditEventResponseModel> call, Response<EditEventResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                         if (status.equals("success")) {
                            loading_dialog.dismiss();
                            Toast.makeText(Edit_Event.this, "Event edited successfully.", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Edit_Event.this, EventActivity_Updated.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        } else {
                            loading_dialog.dismiss();
                         }

                    } else {
                        loading_dialog.dismiss();
                     }
                }

                @Override
                public void onFailure(Call<EditEventResponseModel> call, Throwable t) {
                     loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private void DeleteEventFunction() {


        try {

            DeleteEventRequestModel request = new DeleteEventRequestModel();
            request.setEvent_id(event_data_detail.getId());

            Call<DeleteEventResponseModel> call = retrofitInterface.DeleteEvent("application/json", Common.auth_token, request);

            call.enqueue(new Callback<DeleteEventResponseModel>() {
                @Override
                public void onResponse(Call<DeleteEventResponseModel> call, Response<DeleteEventResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                         if (status.equals("success")) {

                            Toast.makeText(Edit_Event.this, "Event Deleted successfully.", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Edit_Event.this, EventActivity_Updated.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                            loading_dialog.dismiss();
                        } else {
                            loading_dialog.dismiss();
                         }

                    } else {
                        loading_dialog.dismiss();
                     }
                }

                @Override
                public void onFailure(Call<DeleteEventResponseModel> call, Throwable t) {
                     loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    public void intialize_view() {
//
//        try {

        mEditor = findViewById(R.id.event_description);
        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(12);
        mEditor.setFontSize(12);
        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(5);
        mEditor.setOverScrollMode(View.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);
        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Description");


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag = false;
                            action_right_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            action_left_flag = false;
                            action_right_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag = false;
                            action_left_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });


        event_name = (EditText) findViewById(R.id.event_name);
        city = findViewById(R.id.event_city_name);
        country = findViewById(R.id.event_country_name);


        start_date = findViewById(R.id.event_start_date);
        end_date = findViewById(R.id.event_end_date);

        start_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                start_date.setError(null);//removes error
                start_date.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        end_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                end_date.setError(null);//removes error
                end_date.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        start_time = findViewById(R.id.event_start_time);
        end_time = findViewById(R.id.event_end_time);

        start_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                start_time.setError(null);//removes error
                start_time.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        end_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                end_time.setError(null);//removes error
                end_time.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeFunction(true);
            }
        });

        end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeFunction(false);
            }
        });


        if (is_event_edit.equals("1")) {

            header_title.setText("Edit Event");

            event_name.setText(event_data_detail.getName());
            city.setText(event_data_detail.getCity());
            country.setText(event_data_detail.getCountry());


            String start_d = event_data_detail.getStart_date();
            if (start_d != null) {
                String[] split_start = start_d.split("\\s+");
                if (split_start.length >= 2) {
                    start_date.setText(split_start[0]);
                    start_time.setText(split_start[1]);
                }
            }

            String end_d = event_data_detail.getEnd_date();
            if (end_d != null) {
                String[] split_end = end_d.split("\\s+");
                if (split_end.length >= 2) {
                    end_date.setText(split_end[0]);
                    end_time.setText(split_end[1]);
                }
            }

            mEditor.setHtml(event_data_detail.getDescription());
        } else {
            header_title.setText("Add Event");
        }

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal1 = Calendar.getInstance();
                int year = cal1.get(Calendar.YEAR);
                int month = cal1.get(Calendar.MONTH);
                int day = cal1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog1 = new DatePickerDialog(
                        Edit_Event.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = year + "-" + month + "-" + day;
                start_date.setText(date);
            }
        };

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal1 = Calendar.getInstance();
                int year = cal1.get(Calendar.YEAR);
                int month = cal1.get(Calendar.MONTH);
                int day = cal1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog1 = new DatePickerDialog(
                        Edit_Event.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
//
                String date = year + "-" + month + "-" + day;
                end_date.setText(date);
            }
        };


        //.......................project spinner...........
        loading_dialog.dismiss();
        GroupSpinnerView();
         }

    public void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(Edit_Event.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = dialog.findViewById(R.id.custom_title);
        message_text = dialog.findViewById(R.id.custom_message);
        btn_text = dialog.findViewById(R.id.custom_btn);
        close_btn = dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                DeleteEventFunction();

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }



    public void getTimeFunction(final Boolean start) {

        try {

            final Calendar calendar = Calendar.getInstance();
            final int[] hours = {calendar.get(Calendar.HOUR_OF_DAY)};
            final int[] minuts = {calendar.get(Calendar.MINUTE)};

       TimePickerDialog.OnTimeSetListener onTimeSetListener =
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int i, int i1) {
                            if (timePicker.isShown()) {

                                calendar.set(Calendar.HOUR_OF_DAY, i);
                                calendar.set(Calendar.MINUTE, i1);
                                calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND));
                                calendar.set(Calendar.AM_PM, calendar.get(Calendar.AM_PM));

                                if (calendar.get(Calendar.AM_PM) == 0) {
                                    AM_PM_text = "AM";
                                } else if (calendar.get(Calendar.AM_PM) == 1) {
                                    AM_PM_text = "PM";
                                } else {
                                    AM_PM_text = "";
                                }

                                hours[0] = calendar.get(Calendar.HOUR_OF_DAY);
                                minuts[0] = calendar.get(Calendar.MINUTE);

                                time = hours[0] + ":" + minuts[0] + ":" + calendar.get(Calendar.SECOND);

                                if (start == true) {
                                    start_time.setText(time);
                                } else if (start == false) {
                                    end_time.setText(time);
                                }


                             }

                        }
                    };

            TimePickerDialog timePickerDialog =
                    new TimePickerDialog(Edit_Event.this,
                            android.R.style.Theme_Holo_Light_Dialog_NoActionBar, onTimeSetListener, hours[0], minuts[0], false);
            timePickerDialog.setTitle("Set Time");
            timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            timePickerDialog.show();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
