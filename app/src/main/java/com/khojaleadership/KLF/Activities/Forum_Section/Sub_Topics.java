package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Adapter.Forum.Sub_Topic_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.DeletePostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sub_Topics extends AppCompatActivity implements RecyclerViewClickListner {

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    Sub_Topic_Adapter adapter;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    TextView title, header_title;

    RecyclerViewClickListner listner;
    LinearLayout add_sub_topic_btn;


    //for pagination
    int page_num = 1;
    int item_count = 10;
    int total_api_item_count = 0;

    String Category;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail=new GetSubTopicDataModel();
    ArrayList<GetSubTopicDataModel> sub_topic_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub__topics);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        listner = this;

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Sub_Topics.this);
                finish();
            }
        });

        //getting data
        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");

        Bundle bundle=getIntent().getExtras();
        if (bundle!=null) {
            Category = bundle.getString("Category");
        }

        try {

            title = (TextView) findViewById(R.id.sub_topic_title_text);
            if (topic_data_detail != null) {
                if (topic_data_detail.getSubforums__title() != null) {
                    title.setText(topic_data_detail.getSubforums__title());
                } else {
                    title.setText("No title found");
                }
            }


            header_title = findViewById(R.id.c_header_title);
            header_title.setText("SubTopic List");


            loading_dialog = Common.LoadingDilaog(this);

            recyclerView = (RecyclerView) findViewById(R.id.sub_topics_recyclerview);
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);


            loading_dialog.show();
            final int user_id = Integer.valueOf(Common.login_data.getData().getId());
            if (topic_data_detail!=null) {
                onSubTopicBtnClick(Integer.valueOf(topic_data_detail.getSubforums__id()), user_id, false);
            } else {
                recyclerView.setVisibility(View.GONE);
                findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
            }

            //floating action btn
            findViewById(R.id.floating_action_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Sub_Topics.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            });


            //add sub topic
            add_sub_topic_btn = findViewById(R.id.c_add_btn);

            if (Common.is_Admin_flag == false) {
                if (Category.equals("Admin")) {
                    add_sub_topic_btn.setVisibility(View.GONE);
                } else {
                    add_sub_topic_btn.setVisibility(View.VISIBLE);
                }
            }

            add_sub_topic_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Sub_Topics.this, Add_Sub_Topic.class);
                    i.putExtra("TopicDataDetail",topic_data_detail);
                    i.putExtra("Category", Category);
                    startActivity(i);
                    finish();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


//    private void Pagination(int form_id, int user_id) {
//        try {
//
//            Call<GetSubTopicModel> call = retrofitInterface.getSubTopic("application/json", Common.auth_token, form_id, page_num, item_count, user_id);
//            call.enqueue(new Callback<GetSubTopicModel>() {
//                @Override
//                public void onResponse(Call<GetSubTopicModel> call, Response<GetSubTopicModel> response) {
//
//                    int status_code = response.code();
//                    ArrayList<GetSubTopicDataModel> main_list = new ArrayList<>();
//
//                    if (response.isSuccessful()) {
//                        String status, message;
//                        GetSubTopicModel responseModel = response.body();
//
//                        status = responseModel.getStatus();
//                        message = responseModel.getMessage();
//
//                        main_list = responseModel.getData();
//
//                        if (main_list != null) {
//                            //pd.dismiss();
//                            loading_dialog.dismiss();
//
//                            if (main_list.size() == 0) {
////
//                                Toast.makeText(Sub_Topics.this, "list size 0", Toast.LENGTH_SHORT).show();
//
//                            } else {
//
//                                total_api_item_count = main_list.size();
//
//                                adapter.AddItem(main_list);
//                                loading_dialog.dismiss();
//                                Toast.makeText(Sub_Topics.this, "Next shown items :" + main_list.size(), Toast.LENGTH_SHORT).show();
//                            }
//                        } else {
//                            loading_dialog.dismiss();
//                            Toast.makeText(Sub_Topics.this, "list is null", Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//
//                }
//
//                @Override
//                public void onFailure(Call<GetSubTopicModel> call, Throwable t) {
//                    }
//            });
//
//            throw new RuntimeException("Run Time exception");
//        } catch (Exception e) {
//         }
//
//    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onRowClick(int position) {

        try {

            if (sub_topic_data != null) {
                if (sub_topic_data.get(position).getId() != null) {
//                    int id = Integer.valueOf(sub_topic_data.get(position).getId());
//                    Common.sub_topic_edit_form_id = id;
                    //Common.sub_topic_data_detail = sub_topic_data.get(position);
                    sub_topic_data_detail = sub_topic_data.get(position);
                }
            }

            Intent post_intent = new Intent(Sub_Topics.this, Sub_Topic_Posts.class);
            post_intent.putExtra("SubtopicDataDetail",sub_topic_data.get(position));
            post_intent.putExtra("SubtopicId",sub_topic_data.get(position).getId());
            //this is required when returning back to subtopic
            post_intent.putExtra("TopicDataDetail", topic_data_detail);
            post_intent.putExtra("Category", Category);
            startActivity(post_intent);
            finish();
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    @Override
    public void onViewClcik(int position, View v) {

        try {

            if (sub_topic_data != null) {
                if (sub_topic_data.get(position).getId() != null) {
//                    int id = Integer.valueOf(sub_topic_data.get(position).getId());
//                    Common.sub_topic_edit_form_id = id;
                    //Common.sub_topic_data_detail = sub_topic_data.get(position);

                    sub_topic_data_detail = sub_topic_data.get(position);
                }
            }


            switch (v.getId()) {


                case R.id.sub_topic_edit_layout:

                    //aggr phly send eblast flag true hua h tu ussy false krna h
                    Common.send_eblast_falg = false;
                    Intent i = new Intent(Sub_Topics.this, EditSubTopic.class);
                    i.putExtra("TopicDataDetail",topic_data_detail);
                    i.putExtra("SubtopicDataDetail",sub_topic_data.get(position));
                    i.putExtra("Category", Category);
                    startActivity(i);
                    break;

                case R.id.sub_topic_delete_layout:
                    //for delete functionality
                    Confirm_Dialog("Delete Subtopic", "Are you sure to delete this subtopic?", "Yes");
                    break;
                default:
                    break;
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }


    public void Confirm_Dialog(String title, String message, String btn) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(Sub_Topics.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                onDeletePostBtnClick();

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void onSubTopicBtnClick(int form_id, int user_id, final Boolean is_deleted) {


        try {

            Call<GetSubTopicModel> call = retrofitInterface.getSubTopic("application/json", Common.auth_token, form_id, 1, 1000, user_id);
            call.enqueue(new Callback<GetSubTopicModel>() {
                @Override
                public void onResponse(Call<GetSubTopicModel> call, Response<GetSubTopicModel> response) {

                    int status_code = response.code();
                     ArrayList<GetSubTopicDataModel> main_list = new ArrayList<>();

                    if (response.isSuccessful()) {
                        String status, message;


                        GetSubTopicModel responseModel = response.body();

                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        main_list = responseModel.getData();


                        if (main_list != null) {
                            //pd.dismiss();
                            loading_dialog.dismiss();

                            if (sub_topic_data != null) {
                                sub_topic_data.clear();
                            }

                            sub_topic_data = main_list;
                            if (sub_topic_data.size() == 0) {
                                recyclerView.setVisibility(View.GONE);
                                findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);

                            } else {

                                if (is_deleted == false) {
                                    total_api_item_count = main_list.size();
                                    adapter = new Sub_Topic_Adapter(getApplicationContext(), main_list, listner);
                                    recyclerView.setAdapter(adapter);
                                } else if (is_deleted == true) {
                                    total_api_item_count = main_list.size();
                                    adapter.updateList(main_list);
                                }
                            }
                        } else {
                            loading_dialog.dismiss();
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<GetSubTopicModel> call, Throwable t) {

                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }

    private void onDeletePostBtnClick() {

        try {
            DeletePostRequestModel requestModel = new DeletePostRequestModel();


            requestModel.setForum_post_id(sub_topic_data_detail.getId());

            Call<DeletePostResponseModel> responseCall = retrofitInterface.DeletePost("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<DeletePostResponseModel>() {
                @Override
                public void onResponse(Call<DeletePostResponseModel> call, Response<DeletePostResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        DeletePostResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {

                            int user_id = Integer.valueOf(Common.login_data.getData().getId());
                            onSubTopicBtnClick(Integer.valueOf(topic_data_detail.getSubforums__id()), user_id, true);
                            Toast.makeText(Sub_Topics.this, "Subtopic deleted successfully.", Toast.LENGTH_LONG).show();
                        }


                    } else {
                     }

                }

                @Override
                public void onFailure(Call<DeletePostResponseModel> call, Throwable t) {
                    }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }


}
