package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Forum.WhitelistContactAdapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForWhitelistContact;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddContactToWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddContactToWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.IsWhiteListed_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.IsWhiteListed_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveContactFromWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveContactFromWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WhitelistContactsScreen extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private WhitelistContactAdapter adapter;
    private List<IsWhiteListed_Data_Model> sourceDataList;
    List<IsWhiteListed_Data_Model> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;
    private PinyinComparatorForWhitelistContact pinyinComparator;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;


    String Category,IsWhitelistContacts;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();

    List<IsWhiteListed_Data_Model> is_whitelisted_contact_list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whitelist_contacts);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Whitelist contacts");

        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
            //IsWhitelistContacts=bundle.getString("IsWhitelistContacts");
        }


        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, WhitelistContactsScreen.this);

                finish();
            }
        });

        findViewById(R.id.whitelist_contact_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listner = this;

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        GetIsWhitelistedContactFunction();
    }


    private void initViews() {

        pinyinComparator = new PinyinComparatorForWhitelistContact();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        //设置右侧SideBar触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        sourceDataList = filledData(is_whitelisted_contact_list);
         Collections.sort(sourceDataList, pinyinComparator);

         manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        adapter = new WhitelistContactAdapter(getApplicationContext(), sourceDataList, listner);
        mRecyclerView.setAdapter(adapter);


        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);
        mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        loading_dialog.dismiss();
    }



    private List<IsWhiteListed_Data_Model> filledData(List<IsWhiteListed_Data_Model> data) {
        List<IsWhiteListed_Data_Model> mSortList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            IsWhiteListed_Data_Model sortModel = new IsWhiteListed_Data_Model();

            String role,id,title,first_name,middle_name,last_name,email,forum_post_id;
            int iswhitelisted;

            sortModel.setRole(data.get(i).getRole());
            sortModel.setId(data.get(i).getId());
            sortModel.setTitle(data.get(i).getTitle());
            sortModel.setFirst_name(data.get(i).getFirst_name());
            sortModel.setMiddle_name(data.get(i).getMiddle_name());
            sortModel.setLast_name(data.get(i).getLast_name());
            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setForum_post_id(data.get(i).getForum_post_id());
            sortModel.setIswhitelisted(data.get(i).getIswhitelisted());


            String pinyin_english = data.get(i).getFirst_name();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {
                 sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();

                 if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }


        return mSortList;
    }

    private void filterData(String filterStr) {

        filter_flag = true;

        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(is_whitelisted_contact_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (IsWhiteListed_Data_Model sortModel : sourceDataList) {
                String name = sortModel.getFirst_name();


                if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().startsWith(filterStr)
                        || name.toUpperCase().startsWith(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
         Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }


    private void GetIsWhitelistedContactFunction() {

        try {

            Call<IsWhiteListed_Model> call = retrofitInterface.getIsWhitelistedContacts("application/json",Common.auth_token,
                    sub_topic_data_detail.getId());


            call.enqueue(new Callback<IsWhiteListed_Model>() {
                @Override
                public void onResponse(Call<IsWhiteListed_Model> call, Response<IsWhiteListed_Model> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        IsWhiteListed_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        ArrayList<IsWhiteListed_Data_Model> list = new ArrayList<>();
                        list = responseModel.getData();


                        for (int i=0;i<list.size();i++){
                         }

                        if (list != null) {
                            //pd.dismiss();
                            //loading_dialog.dismiss();

                            if (is_whitelisted_contact_list!=null){
                                is_whitelisted_contact_list.clear();
                            }
                            is_whitelisted_contact_list = list;

                            //initViews();

                            initViews();
                        }


                        //Common.ids=IDS;
                    }
                }

                @Override
                public void onFailure(Call<IsWhiteListed_Model> call, Throwable t) {

                    initViews();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }
    @Override
    public void onRowClick(int position) {
     }

    @Override
    public void onViewClcik(int position, View v) {

        IsWhiteListed_Data_Model is_whitelisted_contact_data_detail=new IsWhiteListed_Data_Model();

        try {
            switch (v.getId()) {
                case R.id.whitelist_contact_checkbox:
                    if (filter_flag == true) {
                        is_whitelisted_contact_data_detail = filterDataList.get(position);
                    } else {
                        is_whitelisted_contact_data_detail = sourceDataList.get(position);
                    }

                    String contact_id = is_whitelisted_contact_data_detail.getId();
                    String post_id = sub_topic_data_detail.getId();
                    int flag=is_whitelisted_contact_data_detail.getIswhitelisted();


                    if (flag == 0) {
                        //now add to white list
                         loading_dialog.show();
                        onAddContactToWhitelistBtnClick(contact_id, post_id);
                    } else if (flag == 1) {
                        //now remove to white list
                         loading_dialog.show();
                        onRemoveContactFromWhitelistBtnClick(contact_id, post_id);
                    }
                    //    }

                    break;
                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    private void onAddContactToWhitelistBtnClick(String user_id, String post_id) {

        try {

            AddContactToWhitelistRequestModel requestModel = new AddContactToWhitelistRequestModel();

            requestModel.setUser_id(user_id);
            requestModel.setPost_id(post_id);  //default


            Call<AddContactToWhitelistResponseModel> responseCall = retrofitInterface.AddContactToWhiteList("application/json",Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddContactToWhitelistResponseModel>() {
                @Override
                public void onResponse(Call<AddContactToWhitelistResponseModel> call, Response<AddContactToWhitelistResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        loading_dialog.dismiss();

                        AddContactToWhitelistResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        GetIsWhitelistedContactFunction();
                        //onAlreadyWhitelistedContactFunction();
                        Toast.makeText(WhitelistContactsScreen.this, "Contact added to Whitelist successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        loading_dialog.dismiss();
                        Toast.makeText(WhitelistContactsScreen.this, "Contact added to Whitelist Failed", Toast.LENGTH_SHORT).show();
                     }

                }

                @Override
                public void onFailure(Call<AddContactToWhitelistResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(WhitelistContactsScreen.this, "Contact added to Whitelist Failed", Toast.LENGTH_SHORT).show();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



    }

    private void onRemoveContactFromWhitelistBtnClick(String user_id, String post_id) {

        try {

            RemoveContactFromWhitelistRequestModel requestModel = new RemoveContactFromWhitelistRequestModel();

            requestModel.setUser_id(user_id);
            requestModel.setPost_id(post_id);  //default


            Call<RemoveContactFromWhitelistResponseModel> responseCall = retrofitInterface.RemoveContactFromWhiteList("application/json",Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<RemoveContactFromWhitelistResponseModel>() {
                @Override
                public void onResponse(Call<RemoveContactFromWhitelistResponseModel> call, Response<RemoveContactFromWhitelistResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        loading_dialog.dismiss();

                        RemoveContactFromWhitelistResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        GetIsWhitelistedContactFunction();
                        Toast.makeText(WhitelistContactsScreen.this, "Contact Remove to Whitelist successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        loading_dialog.dismiss();
                        Toast.makeText(WhitelistContactsScreen.this, "Contact Remove to Whitelist Failed", Toast.LENGTH_SHORT).show();
                     }

                }

                @Override
                public void onFailure(Call<RemoveContactFromWhitelistResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(WhitelistContactsScreen.this, "Contact Remove to Whitelist Failed", Toast.LENGTH_SHORT).show();
                 }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
