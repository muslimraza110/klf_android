package com.khojaleadership.KLF.Activities.event_new.programme

data class ProgramDetailsUiModel(
        val dayData: EventDaysUiModel,
        val programmeDetailItems: List<ProgrammeDetailsItemUiModel>
)