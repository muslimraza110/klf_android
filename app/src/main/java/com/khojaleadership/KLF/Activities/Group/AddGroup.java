package com.khojaleadership.KLF.Activities.Group;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.CheckBox;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Group_Models.AddGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.AddGroupResponseModel;
import com.khojaleadership.KLF.R;

import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGroup extends AppCompatActivity {

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    String category;
    CheckBox visible, accept_donation, pending, passive;
    String visible_status = "0", accept_status = "0", pending_status = "0", passive_status = "0";
    Boolean flag=false;
    RichEditor mEditor;
    TextView name, description, email, website, tags, phone, address1, address2, city, postal_code, province, country;
    ScrollView scrollView;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean  action_left_flag = false, action_right_flag = false, action_centr_flag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("New Group");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, AddGroup.this);

                finish();
            }
        });

        loading_dialog = Common.LoadingDilaog(this);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //intialize rich editor box
        initialize_Rich_editor();

        name = (TextView) findViewById(R.id.g_name);
        description = (TextView) findViewById(R.id.g_description);
        email = (TextView) findViewById(R.id.g_main_email);
        website = (TextView) findViewById(R.id.g_website);
        phone = (TextView) findViewById(R.id.g_phone);
        address1 = (TextView) findViewById(R.id.g_address1);
        address2 = (TextView) findViewById(R.id.g_address2);
        city = (TextView) findViewById(R.id.g_city);
        postal_code = (TextView) findViewById(R.id.g_postal_code);
        province = (TextView) findViewById(R.id.g_province);
        country = (TextView) findViewById(R.id.g_country);


        visible = (CheckBox) findViewById(R.id.g_visible_radiobtn);
        accept_donation = (CheckBox) findViewById(R.id.g_accept_donation_radiobtn);
        pending = (CheckBox) findViewById(R.id.g_pending_radiobbtn);
        passive = (CheckBox) findViewById(R.id.g_passive_radiobbtn);

        if (Common.is_Admin_flag==false){
            accept_donation.setVisibility(View.GONE);
            pending.setVisibility(View.GONE);
            passive.setVisibility(View.GONE);
        }

        visible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    visible_status="1";
                }else {
                    visible_status="0";
                }
            }
        });

        accept_donation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    accept_status="1";
                }else {
                    accept_status="0";
                }
            }
        });

        pending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    pending_status="1";
                }else {
                    pending_status="0";
                }
            }
        });

        passive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    passive_status="1";
                }else {
                    passive_status="0";
                }
            }
        });


        Spinner spinner = (Spinner) findViewById(R.id.add_spinner);
        ArrayList<String> category_title = new ArrayList<>();
        category_title.add("--Groups--");
        category_title.add("Business");
        category_title.add("Charity");

        //create a list of items for the spinner.
        final ArrayAdapter<String> block_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, category_title) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    //tv.setTextColor(Color.BLACK);

                } else {

                    //tv.setTextSize(22);
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        block_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(block_spinnerArrayAdapter);
        //Set the listener for when each option is clicked.
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Change the selected item's text color
                category = parent.getItemAtPosition(position).toString();
                if (category.equals("Business")) {
                    category = "2";
                } else if (category.equals("Charity")) {
                    category = "3";
                }


                ((TextView) view).setTextColor(getResources().getColor(R.color.pure_grey));
                ((TextView) view).setTextSize(12);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        findViewById(R.id.add_group_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category.equals("--Groups--")) {
                    Toast.makeText(AddGroup.this, "Please select Category.", Toast.LENGTH_SHORT).show();
                } else {


                    if (name.getText().toString().equalsIgnoreCase("") || name.getText().toString().isEmpty()) {
                        name.setError("Name should not be empty.");
                    }else if(name.getText().toString().replace(" ", "").equals("")) {
                        name.setError("Name should not contain only space.");
                    }else if (description.getText().toString().equalsIgnoreCase("") || description.getText().toString().isEmpty()) {
                        description.setError("Description should not be empty.");
                    }else if(description.getText().toString().replace(" ", "").equals("")) {
                        description.setError("Description should not contain only space.");
                    } else if (email.getText().toString().equalsIgnoreCase("") || email.getText().toString().isEmpty()) {
                        email.setError("Email should not be empty.");
                    }else if(email.getText().toString().replace(" ", "").equals("")) {
                        email.setError("Email should not contain only space.");
                    }else if(!isValidEmail(email.getText().toString())) {
                        email.setError("Email is not valid.");
                    }else if (phone.getText().toString().equalsIgnoreCase("") || phone.getText().toString().isEmpty()) {
                        phone.setError("phone # should not be empty.");
                    }else if(phone.getText().toString().replace(" ", "").equals("")) {
                        phone.setError("phone # should not contain only space.");
                    } else if (address1.getText().toString().equalsIgnoreCase("") || address1.getText().toString().isEmpty()) {
                        address1.setError("Address should not be empty.");
                    }else if(address1.getText().toString().replace(" ", "").equals("")) {
                        address1.setError("Address should not contain only space.");
                    } else {


                        if (flag==false){
                            flag=true;
                            //now add group
                            loading_dialog.show();
                            onAddGroupBtnClick();
                        }


                    }


                }



            }
        });


    }


    public void initialize_Rich_editor(){
        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(22);

//        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setScrollbarFadingEnabled(false);
        //mEditor.setVerticalScrollbarOverlay(true);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
//        mEditor.isScrollContainer();
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);


        //mEditor.setEditorFontColor(Color.BLACK);
        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Description");
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        //mEditor.setInputEnabled(false);


        scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag=false;
                            action_right_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);



                            action_left_flag=false;
                            action_right_flag=false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag=false;
                            action_left_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });

    }


    public static boolean isValidEmail(String emailText) {
        Boolean flag=false;

        Boolean containsAlpha = emailText.contains("@");
        Boolean containsDot = emailText.contains(".");

        String afterDotString = "";
        if (containsDot) {
            afterDotString = emailText.substring(emailText.lastIndexOf("."));
        }


        if (containsAlpha == true && containsDot == true && afterDotString.length() > 2) {
            flag=true;
        }else {
            flag=false;
        }

        return flag;
    }


    private void onAddGroupBtnClick() {

        try {
            AddGroupRequestModel requestModel = new AddGroupRequestModel();


            if (category.equalsIgnoreCase("") || category.isEmpty()) {

            } else {
                requestModel.setCategory_id(category);
            }

            requestModel.setName(name.getText().toString());
            requestModel.setDescription(description.getText().toString());
            requestModel.setEmail(email.getText().toString());
            requestModel.setPhone(phone.getText().toString());
            requestModel.setAddress(address1.getText().toString());
            requestModel.setAddress2(address2.getText().toString());
            requestModel.setWebsite("https:" + website.getText().toString());
            requestModel.setCity(city.getText().toString());
            requestModel.setRegion(province.getText().toString());
            requestModel.setCountry(country.getText().toString());
            requestModel.setPostal_code(postal_code.getText().toString());

            requestModel.setBio(mEditor.getHtml());
            requestModel.setHidden(visible_status);
            requestModel.setPending(pending_status);
            requestModel.setVote_decision("");
            requestModel.setDonations(accept_status);
            requestModel.setPassive(passive_status);
            requestModel.setUser_id(Common.login_data.getData().getId());

//            String is_admin;
//            if (Common.Role .equals("Admin")) {
//                is_admin = "1";
//            } else {
//                is_admin = "0";
//            }

            requestModel.setIs_admin("1");


            Call<AddGroupResponseModel> call = retrofitInterface.AddGroup("application/json",Common.auth_token,
                    requestModel);
            call.enqueue(new Callback<AddGroupResponseModel>() {
                @Override
                public void onResponse(Call<AddGroupResponseModel> call, Response<AddGroupResponseModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        AddGroupResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                            loading_dialog.dismiss();
                            Common.Toast_Message(AddGroup.this,"Group Added Successfully.");
                            Intent i=new Intent(AddGroup.this,GroupsMainScreen.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                         }
                    }
                }

                @Override
                public void onFailure(Call<AddGroupResponseModel> call, Throwable t) {
                    Confirm_Dialog("Group", "Group Added Failed.", "OK");
                }

            });


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        // custom dialog
        final Dialog dialog = new Dialog(AddGroup.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(AddGroup.this, GroupsMainScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });


        dialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
