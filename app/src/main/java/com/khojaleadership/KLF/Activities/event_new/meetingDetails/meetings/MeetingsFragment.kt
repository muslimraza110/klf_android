package com.khojaleadership.KLF.Activities.event_new.meetingDetails.meetings

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsViewModel
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Model.event_new.MeetingRequestStatusTypes
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.FragmentMeetingsBinding
import com.khojaleadership.KLF.databinding.LayoutMeetingNotesBinding
import eu.davidea.flexibleadapter.FlexibleAdapter

class MeetingsFragment : Fragment() {

    private val viewModel: MeetingsViewModel by viewModels()
    private val parentViewModel: MeetingDetailsViewModel by activityViewModels()
    private lateinit var binding: FragmentMeetingsBinding

    private var alertDialog: AlertDialog? = null
    lateinit var meetingNotesBinding: LayoutMeetingNotesBinding

    private lateinit var meetingsAdapter: FlexibleAdapter<MeetingHeaderItem>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMeetingsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.getDataFromIntent(requireActivity().intent)
        viewModel.getMeetings()
        setupViews()
        setupObservers()
    }


    private fun setupObservers() {
        viewModel.eventState.observe(viewLifecycleOwner) {
            when (it) {
                is Result.Loading -> {
                    parentViewModel.setEventState(it)
                }
                is Result.Success -> {
                    binding.tvError.text = it.message
                    parentViewModel.setEventState(it)
                }
                is Result.Error -> {
                    binding.tvError.text = it.message
                    parentViewModel.setEventState(it)
                }
            }
        }

        viewModel.messageEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.meetingItems.observe(viewLifecycleOwner, Observer {
            it?.let {
                meetingsAdapter.updateDataSet(it)
                if (it.isNotEmpty()) {
                    binding.tvError.visibility = View.GONE
                } else {
                    binding.tvError.visibility = View.VISIBLE
                }

                viewModel.meetingParentId?.let inner@{ meetingParentId ->
                    it.forEachIndexed() { index, headerItem ->
                        if (headerItem.subItems.any { item -> item.meetingChildId == meetingParentId }) {
                            meetingsAdapter.smoothScrollToPosition(index)
                            return@inner
                        }
                    }
                }
                viewModel.meetingParentId = null
            }
        })

        viewModel.showMeetingsDialogEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                openMeetingNotesDialog(it)
            }
        })

        viewModel.showDeleteDialogEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                openCancelDialog(it)
            }
        })

        viewModel.editMeetingEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                parentViewModel.editMeeting(it)
            }
        })

        viewModel.sendNotesEvent.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    meetingNotesBinding.progressBar.visibility = View.VISIBLE
                    meetingNotesBinding.image.visibility = View.INVISIBLE
                    meetingNotesBinding.name.visibility = View.INVISIBLE
                    meetingNotesBinding.editText.isEnabled = false
                    meetingNotesBinding.imageClose.isEnabled = false
                }
                is Result.Success -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                    alertDialog?.dismiss()
                    alertDialog = null
                }
                is Result.Error -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                }
            }
        })

        parentViewModel.refreshMeetingDataEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                viewModel.getMeetings()
            }
        })

        viewModel.showAcceptDialogEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                showDialog(
                        itemUiModel = it,
                        isAccept = true
                )
            }
        })

        viewModel.showCancelDialogEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                showDialog(
                        itemUiModel = it,
                        isCancel = true
                )
            }
        })

        viewModel.showRejectDialogEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                showDialog(
                        itemUiModel = it,
                        isReject = true
                )
            }
        })
    }

    /**
     * Only one parameter should be true from (isAccept, isReject, isCancel)
     * for this method to work properly
     */
    private fun showDialog(
            itemUiModel: MeetingItemUiModel,
            isAccept: Boolean = false,
            isReject: Boolean = false,
            isCancel: Boolean = false
    ) {

        var message = ""
        when {
            isAccept -> {
                message = "Are you sure you want to accept this meeting invitation?"
            }
            isReject -> {
                message = "Are you sure you want to reject this meeting invitation?"
            }
            isCancel -> {
                message = if (itemUiModel.iAmHost) {
                    "Are you sure want to cancel the meeting invitation for ${itemUiModel.personDetail.name}?"
                } else {
                    "Are you sure you want to cancel this meeting invitation?"
                }
            }
        }

        val dialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setPositiveButton("YES") { dialog, which ->
                    when {
                        isAccept -> viewModel.updateMeetingStatus(MeetingRequestStatusTypes.MEETING_ACCEPTED.value, itemUiModel)
                        isReject -> viewModel.updateMeetingStatus(MeetingRequestStatusTypes.MEETING_REJECTED.value, itemUiModel)
                        isCancel -> viewModel.updateMeetingStatus(MeetingRequestStatusTypes.MEETING_CANCELED.value, itemUiModel)
                    }
                }
                .setNegativeButton("NO") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
        dialog.setOnShowListener {
            dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
            dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
        }
        dialog.show()
    }

    private fun setupViews() {
        meetingsAdapter = FlexibleAdapter(listOf())
        meetingsAdapter.setDisplayHeadersAtStartUp(true)

        binding.rvMeetings.apply {
            adapter = meetingsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }


    private fun openCancelDialog(item: MeetingHeaderItem) {
        val dialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to cancel this meeting?")
                .setPositiveButton("YES") { dialog, which ->
                    viewModel.cancelMeetingRequest(item)
                }
                .setNegativeButton("NO") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
        dialog.setOnShowListener {
            dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
            dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
        }
        dialog.show()
    }

    private fun openMeetingNotesDialog(item: MeetingHeaderItem) {

        meetingNotesBinding = LayoutMeetingNotesBinding.inflate(layoutInflater)
        meetingNotesBinding.spinnerTime.visibility = View.GONE
        meetingNotesBinding.spinnerDate.visibility = View.GONE
        meetingNotesBinding.editText.setText(item.meetingNotes)
        meetingNotesBinding.name.text = "Update Notes"
        alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog?.setView(meetingNotesBinding.root)
        alertDialog?.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        alertDialog?.show()
        alertDialog?.setCanceledOnTouchOutside(false)


        meetingNotesBinding.imageClose.setOnClickListener {
            alertDialog?.dismiss()
        }

        meetingNotesBinding.layoutSendNotes.setOnClickListener {
            viewModel.updateMeetingNotes(meetingNotesBinding.editText.text?.toString() ?: "", item)
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                MeetingsFragment()
    }
}