package com.khojaleadership.KLF.Activities.event_new.events

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.ResponseStatus
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Helper.launchSafely
import com.khojaleadership.KLF.Model.event_new.SingleEventResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class EventsViewModel(val app: Application) : AndroidViewModel(app) {



    private val _eventsData: MutableLiveData<List<EventCardUiModel>> = MutableLiveData()
    val eventsData: LiveData<List<EventCardUiModel>> = _eventsData




    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState


    private val _setSummitEventsRegistration: MutableLiveData<Result<Unit>> = MutableLiveData()
    val setSummitEventsRegistration: LiveData<Result<Unit>> = _setSummitEventsRegistration


    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
            context = app,
            gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")



    var isUpcomingEvent = true


    fun fetchData() {
        if (isUpcomingEvent) {
            getCurrentEvents()
        } else {
            getPreviousEvents()
        }
    }


     fun setSummitEventsRegistration(onSuccess: (String?) -> Unit) {
        _setSummitEventsRegistration.value = Result.Loading

        viewModelScope.launchSafely(
            block = {

                val response = eventsRepository.setSummitEventsRegistration(
                    Common.login_data?.data?.id ?: "", authStore.event?.eventId ?: ""
                )


                if (response.status == ResponseStatus.SUCCESS) {

                    Timber.d(response.toString())


                    _setSummitEventsRegistration.value = Result.Success(
                        message = response.message ?: "",
                        data = Unit
                    )

                    onSuccess.invoke(response.toString())

                } else {
                    _setSummitEventsRegistration.value = Result.Error(
                        message = response.message ?: Common.somethingWentWrongMessage
                    )
                }
            },
            error = {
                Timber.e(it)

                _setSummitEventsRegistration.value = Result.Error(
                    message = Common.somethingWentWrongMessage
                )
            }
        )
    }



    private fun getCurrentEvents() {
        _eventState.value = Result.Loading

        viewModelScope.launchSafely(
            block = {
                val response = eventsRepository.getCurrentEvents(
                    Common.login_data?.data?.id
                        ?: ""
                )

                if (response.status == ResponseStatus.SUCCESS) {
                    Timber.d(response.toString())
                    response.data?.let {
                        val tempList = mapToEventCardModel(it)
                        if (tempList.isNotEmpty()) {

                            val list = listOf(tempList.firstOrNull())
                            _eventsData.value = list.filterNotNull()
                        } else {
                            _eventsData.value = listOf()
                        }
                        Timber.e("----------Timber.e(Unit.toString())")
                        Timber.e(Unit.toString())
                        _eventState.value = Result.Success(

                            data = Unit,
                            message = response.message
                        )
                    } ?: run {
                        _eventsData.value = listOf()
                        _eventState.value = Result.Success(
                            message = response.message ?: Common.noRecordFoundMessage,
                            data = Unit
                        )
                    }
                } else {
                    _eventState.value = Result.Error(
                        message = response.message ?: Common.somethingWentWrongMessage
                    )
                }
            },
            error = {
                Timber.e(it)

                _eventState.value = Result.Error(
                    message = Common.somethingWentWrongMessage
                )
            }
        )
    }

    private fun getPreviousEvents() {

        _eventState.value = Result.Loading

        viewModelScope.launchSafely(
            block = {
                val response = eventsRepository.getPreviousEvents(
                    Common.login_data?.data?.id
                        ?: ""
                )

                if (response.status == ResponseStatus.SUCCESS) {
                    Timber.d(response.toString())
                    response.data?.let {


                            _eventsData.value = mapToEventCardModel(it)

                        _eventState.value = Result.Success(
                            data = Unit,
                            message = response.message
                        )
                    } ?: run {
                        _eventsData.value = listOf()
                        _eventState.value = Result.Success(
                            message = response.message ?: Common.noRecordFoundMessage,
                            data = Unit
                        )
                    }
                } else {
                    _eventState.value = Result.Error(
                        message = response.message ?: Common.somethingWentWrongMessage
                    )
                }
            },
            error = {
                Timber.e(it)
                _eventState.value = Result.Error(
                    message = Common.somethingWentWrongMessage
                )
            }
        )

    }

    private fun mapToEventCardModel(responseData: List<SingleEventResponse>): List<EventCardUiModel> {
        Timber.e("mapToEventCardModel")
        val responseModel: List<SingleEventResponse> = responseData


        return responseData.map {


            EventCardUiModel(
                date = it.startDate ?: "",
                address = it.address ?: "",
                city = it.city ?: "",
                eventImage = it.images?.firstOrNull() ?: "",
                eventId = it.id ?: "",

                price = "$${it.priceRangeStart ?: ""} - $${it.priceRangeEnd ?: ""}",
                priceRangeEnd = "${it.priceRangeEnd ?: "0"}",
                priceRangeStart = "${it.priceRangeStart ?: "0"}",

                registration_link = it.registrationLink ?: "",

                isUserRegistered = it.isUserRegistered ?: false,
                name = it.name ?: "",
                facultyId = it.summitEventsFacultyId,
                meetingSlots = it.meetingSlots,
                startDate = it.startDate ?: "",
                endDate = it.endDate ?: ""
            )

        }

    }
}