package com.khojaleadership.KLF.Activities.Forum_Section;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Request_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Response_Model;
import com.khojaleadership.KLF.R;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendEblastScreen1 extends AppCompatActivity {

    TextView textView;
    String Url = "http://portal.khojaleadershipforum.org/-/forum/view/263";
    RichEditor mEditor;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;


    EditText subject, button_label;
    Button send_eblast_btn;
    TextView header_title;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_eblast_screen1);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        loading_dialog = Common.LoadingDilaog(this);

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, SendEblastScreen1.this);

                if (Common.send_eblast_list != null) {
                    if (!(Common.send_eblast_list.equals(""))) {
                        Common.send_eblast_falg = false;
                    }
                }

                finish();
            }
        });


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Eblast");

        subject = (EditText) findViewById(R.id.subject);
        button_label = (EditText) findViewById(R.id.button_label);

        findViewById(R.id.send_eblast_add_recipient).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.eblast_subject = subject.getText().toString();
                Common.eblast_label = button_label.getText().toString();
                Common.eblast_message = mEditor.getHtml();

                Intent recipient = new Intent(SendEblastScreen1.this, SendEblastScreen2.class);
                startActivity(recipient);
            }
        });


        intialize_view();


        send_eblast_btn = (Button) findViewById(R.id.send_eblast_btn);
        send_eblast_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Common.send_eblast_email_list != null) {

                    if (button_label.getText().toString() == null || button_label.getText().toString().isEmpty()) {
                        Toast.makeText(SendEblastScreen1.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (subject.getText().toString() == null || subject.getText().toString().isEmpty()) {
                        Toast.makeText(SendEblastScreen1.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (mEditor.getHtml() == null || mEditor.getHtml().isEmpty()) {
                        Toast.makeText(SendEblastScreen1.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (Common.send_eblast_email_list == null) {
                        Toast.makeText(SendEblastScreen1.this, "Please select Recipients.", Toast.LENGTH_SHORT).show();
                    } else if (Common.send_eblast_email_list.size() == 0) {
                        Toast.makeText(SendEblastScreen1.this, "Please select Recipients.", Toast.LENGTH_SHORT).show();
                    } else {
                        loading_dialog.show();

                        String email_array = "";

                        for (int i = 0; i < Common.send_eblast_email_list.size(); i++) {

                            String email = "," + Common.send_eblast_email_list.get(i).getEmail();
                            String email1 = Common.send_eblast_email_list.get(i).getEmail();

                            if (i == 0) {
                                email_array = email1;
                            } else {
                                email_array = email_array + email;
                            }
                        }
                        SendEblast(email_array);
                    }

                } else {
                    Toast.makeText(SendEblastScreen1.this, "Please select Recipients", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void SendEblast(String email_array) {

        try {

            Send_Eblast_Request_Model request_model = new Send_Eblast_Request_Model();
            request_model.setButtonlink(textView.getText().toString());
            request_model.setButtonlabel(button_label.getText().toString());
            request_model.setSubject(subject.getText().toString());
            request_model.setContent(mEditor.getHtml());
            request_model.setEmails(email_array);


            Call<Send_Eblast_Response_Model> call = retrofitInterface.SendEblastFunction("application/json", Common.auth_token, request_model);
            call.enqueue(new Callback<Send_Eblast_Response_Model>() {
                @Override
                public void onResponse(Call<Send_Eblast_Response_Model> call, Response<Send_Eblast_Response_Model> response) {


                    loading_dialog.dismiss();

                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;
                        Send_Eblast_Response_Model responseModel = response.body();

                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        Log.d("EblastIssue", "Status :" + status);

                        if (status.equals("success")) {
                            if (Common.send_eblast_post == false) {
                                Toast.makeText(SendEblastScreen1.this, "Eblast sent successfully", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(SendEblastScreen1.this, Sub_Topics.class);
                                i.putExtra("TopicDataDetail", Common.topic_data_detail);
                                i.putExtra("Category", Common.Category);
                                startActivity(i);
                                finish();

                            } else if (Common.send_eblast_post == true) {
                                Toast.makeText(SendEblastScreen1.this, "Eblast sent successfully", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(SendEblastScreen1.this, Sub_Topic_Posts.class);
                                i.putExtra("TopicDataDetail", Common.topic_data_detail);
                                i.putExtra("Category", Common.Category);
                                i.putExtra("SubtopicDataDetail", Common.sub_topic_data_detail);
                                i.putExtra("SubtopicId", Common.Subtopic_Id);

                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            }

                        } else {
                            //failure
                        }

                    } else {
                        Log.d("EblastIssue", "response code :" + response.code());
                    }
                }

                @Override
                public void onFailure(Call<Send_Eblast_Response_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                    Log.d("EblastIssue", "on failure :" + t.getMessage());
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void intialize_view() {

        textView = findViewById(R.id.tv_link);
        textView.setText(HtmlCompat.fromHtml(Url, 0));


        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);


        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Content*");


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag = false;
                            action_right_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            action_left_flag = false;
                            action_right_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag = false;
                            action_left_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });

//        mEditor.loadData(Common.eblast_message, "text/html; charset=utf-8", "UTF-8");
//        mEditor.setVerticalScrollBarEnabled(true);
//        mEditor.setHorizontalScrollBarEnabled(true);
//        mEditor.setWebViewClient(new WebViewClient());

        mEditor.setHtml(Common.eblast_message);
        button_label.setText(Common.eblast_label);
        subject.setText(Common.eblast_subject);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Common.send_eblast_list != null) {
            if (!(Common.send_eblast_list.equals(""))) {
                Common.send_eblast_falg = false;
            }
        }
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
