package com.khojaleadership.KLF.Activities.Group;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.khojaleadership.KLF.Fragments.Businuess_Group_Fragment;
import com.khojaleadership.KLF.Fragments.Charitie_Group_Fragment;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.R;


public class GroupsMainScreen extends AppCompatActivity {
    Button businuess_btn, charitie_btn;
    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groups_main_screen_activity);

        loading_dialog = Common.LoadingDilaog(this);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Groups");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, GroupsMainScreen.this);

                finish();
            }
        });

        //add btn
        findViewById(R.id.c_add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupsMainScreen.this, AddGroup.class);
                startActivity(i);
            }
        });

        //first Call API for first time as a default
        //loading_dialog.show();
        attachFragmentB();


//        if (Common.is_bussinues_group == true) {
//            onGroupBtnClick("B");
//        } else {
//            onGroupBtnClick("C");
//        }

        businuess_btn = (Button) findViewById(R.id.business_btn);
        businuess_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading_dialog.show();
                //onGroupBtnClick("B");

                //change btn clr
                businuess_btn.setBackgroundColor(getResources().getColor(R.color.blue));
                businuess_btn.setTextColor(getResources().getColor(R.color.white));

                charitie_btn.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                charitie_btn.setTextColor(getResources().getColor(R.color.blue));


                detachFragmentC();
                attachFragmentB();


            }
        });

        charitie_btn = (Button) findViewById(R.id.charitie_btn);
        charitie_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading_dialog.show();
                //onGroupBtnClick("C");

                //change btn clr
                charitie_btn.setBackgroundColor(getResources().getColor(R.color.blue));
                charitie_btn.setTextColor(getResources().getColor(R.color.white));

                businuess_btn.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                businuess_btn.setTextColor(getResources().getColor(R.color.blue));

                detachFragmentB();
                attachFragmentC();
            }
        });


        //initViews();
    }

    void attachFragmentB() {
//        Common.is_bussinues_group = true;
        try {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            //for attaching fragment add this in fragment   import android.support.v4.app.Fragment;
            Businuess_Group_Fragment fb = (Businuess_Group_Fragment) fm.findFragmentByTag("BGF");

//            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("Foo", (ArrayList<? extends Parcelable>) Common.group_list);
//            fb.setArguments(bundle);

            if (fb == null) {
                fb = new Businuess_Group_Fragment();
                ft.add(R.id.FragmentContainer, fb, "BGF");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                //ft.commit();
                ft.commitAllowingStateLoss();
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    void detachFragmentB() {
        try {
            FragmentManager fm = getFragmentManager();

            FragmentTransaction ft = fm.beginTransaction();

            Businuess_Group_Fragment fb = (Businuess_Group_Fragment) fm.findFragmentByTag("BGF");

            if (fb != null) {
                ft.remove(fb);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                //ft.commit();
                ft.commitAllowingStateLoss();
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    void attachFragmentC() {
//        Common.is_bussinues_group = false;

        try {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            //for attaching fragment add this in fragment   import android.support.v4.app.Fragment;
            Charitie_Group_Fragment fb = (Charitie_Group_Fragment) fm.findFragmentByTag("CF");

            if (fb == null) {
                fb = new Charitie_Group_Fragment();
                ft.add(R.id.FragmentContainer, fb, "CF");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                //ft.commit();
                ft.commitAllowingStateLoss();
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    void detachFragmentC() {
        try {

            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            Charitie_Group_Fragment fb = (Charitie_Group_Fragment) fm.findFragmentByTag("CF");

            if (fb != null) {
                ft.remove(fb);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                //ft.commit();
                ft.commitAllowingStateLoss();
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


//    private void onGroupBtnClick(final String type) {
//
//        try {
//
//            Call<GetGroupListModel> call = retrofitInterface.getGroups("application/json", Common.auth_token,
//                    type, 1, 1000, Common.login_data.getData().getId());
//            call.enqueue(new Callback<GetGroupListModel>() {
//                @Override
//                public void onResponse(Call<GetGroupListModel> call, Response<GetGroupListModel> response) {
//                    if (response.isSuccessful()) {
//                        String status, message;
//
//
//                        GetGroupListModel responseModel = response.body();
//                        status = responseModel.getStatus();
//                        message = responseModel.getMessage();
//
//                        ArrayList<GetGroupListDataModel> list = new ArrayList<>();
//                        list = responseModel.getData();
//
//                        if (list != null) {
//                            //pd.dismiss();
//                            loading_dialog.dismiss();
//
//                            //attaching fragment
//                            if (type.equals("C")) {
//
//                                //change btn clr
//                                charitie_btn.setBackgroundColor(getResources().getColor(R.color.blue));
//                                charitie_btn.setTextColor(getResources().getColor(R.color.white));
//
//                                businuess_btn.setBackgroundColor(getResources().getColor(R.color.dark_grey));
//                                businuess_btn.setTextColor(getResources().getColor(R.color.blue));
//
//                                if (Common.C_group_list != null) {
//                                    Common.C_group_list.clear();
//                                }
//                                Common.C_group_list = list;
//
//                                detachFragmentB();
//                                attachFragmentC();
//                            } else if (type == "B") {
//
//                                //change btn clr
//                                businuess_btn.setBackgroundColor(getResources().getColor(R.color.blue));
//                                businuess_btn.setTextColor(getResources().getColor(R.color.white));
//
//                                charitie_btn.setBackgroundColor(getResources().getColor(R.color.dark_grey));
//                                charitie_btn.setTextColor(getResources().getColor(R.color.blue));
//
//                                if (Common.group_list != null) {
//                                    Common.group_list.clear();
//                                }
//                                //attaching fragment
//                                Common.group_list = list;
//                                detachFragmentC();
//                                attachFragmentB();
//                            }
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<GetGroupListModel> call, Throwable t) {
//
//                }
//            });
//
//            throw new RuntimeException("Run Time exception");
//        } catch (Exception e) {
//         }
//
//    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
