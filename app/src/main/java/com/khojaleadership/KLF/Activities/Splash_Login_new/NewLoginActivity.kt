package com.khojaleadership.KLF.Activities.Splash_Login_new

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.khojaleadership.KLF.Activities.DashBoard_New.NewMainActivity
import com.khojaleadership.KLF.databinding.ActivityNewLoginBinding

class NewLoginActivity : AppCompatActivity() {


    lateinit var binding:ActivityNewLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityNewLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
    }

    private fun setUpViews() {
        binding.loginLayout.setOnClickListener {
            startActivity(Intent(this,NewMainActivity::class.java))
            finish()
        }
    }
}
