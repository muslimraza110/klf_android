package com.khojaleadership.KLF.Activities.event_new.datePicker

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.gson.Gson
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import java.text.SimpleDateFormat
import java.util.*

class DatePickerDialogFragment : DialogFragment() {

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = requireContext().applicationContext,
                gson = Gson()
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current time as the default values for the picker
        val c = Calendar.getInstance()
        if(arguments?.containsKey(TIMESTAMP) == true){
            c.timeInMillis = arguments?.getLong(TIMESTAMP) ?: c.timeInMillis
        }
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return try {
            val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
            val parsedStartDate = dateFormat.parse(authStore.event?.startDate ?: "")
            val parsedEndDate = dateFormat.parse(authStore.event?.endDate ?: "")
            DatePickerDialog(requireActivity(), R.style.DateTimePickerDialogTheme, targetFragment as DatePickerDialog.OnDateSetListener, year, month, day).apply {
                datePicker.minDate = parsedStartDate.time
                datePicker.maxDate = parsedEndDate.time
            }

        } catch (e: Exception) { //this generic but you can control another types of exception
            DatePickerDialog(requireActivity(), R.style.DateTimePickerDialogTheme, targetFragment as DatePickerDialog.OnDateSetListener, year, month, day).apply {
                datePicker.minDate = c.timeInMillis
            }
        }


    }

    companion object{
        const val TIMESTAMP = "timeStamp"
    }
}