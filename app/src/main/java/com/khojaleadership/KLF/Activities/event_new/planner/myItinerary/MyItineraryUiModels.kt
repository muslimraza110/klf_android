package com.khojaleadership.KLF.Activities.event_new.planner.myItinerary

import com.khojaleadership.KLF.Activities.event_new.planner.sessions.SessionItemUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import java.util.*


data class MyItineraryUiModel(
        val myItineraryItems: List<MyItineraryItemUiModel>,
        val dayData: EventDaysUiModel
)

abstract class MyItineraryItemUiModel(
        open val id: Long = UUID.randomUUID().mostSignificantBits,
        open val type: MyItineraryItemType
) {
    abstract val programDetailsId: String
    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

data class MyItineraryPlenarySessionItemUiModel(
        override val programDetailsId: String,
        val time: String,
        val title: String,
        val description: String,
        var isExpanded: Boolean,
        val speakers: List<PersonDetailUiModel>,
        val delegates: List<PersonDetailUiModel>
) : MyItineraryItemUiModel(type = MyItineraryItemType.SESSION_DETAILS)


data class MyItinerarySessionUiModel(
        override val programDetailsId: String,
        val time: String,
        val title: String,
        var isChecked: Boolean,
        val sessionType: String,
        val isExpanded: Boolean,
        val canRequestMeeting: String,
        val checkMyAvailabilityIsAvailable: String?,
        val sessionsList: List<SessionItemUiModel>,
        val meetingRequestCount: Int
) : MyItineraryItemUiModel(type = MyItineraryItemType.SESSION)

data class MyItineraryRequestItemUiModel(
        override val id: Long = UUID.randomUUID().mostSignificantBits,
        override val type: MyItineraryItemType,
        override val programDetailsId: String,
        val name: String,
        val title: String,
        val designation: String,
        val requestedBy: String,
        val requestedTo: String,
        val time: String,
        val sessionType: String,
        val canRequestMeeting: String,
        val meetingNotes: String,
        val isReceived: Boolean,
        val checkMyAvailabilityIsAvailable: String?,
        val meetingStatus: String,
        val sessionsList: List<SessionItemUiModel>
) : MyItineraryItemUiModel(type = type)

enum class MyItineraryItemType {
    SESSION_DETAILS,
    SESSION,
    REQUEST_SENT,
    REQUEST_RECEIVED,
    REQUEST_ACCEPTED
}

enum class RequestMeeting(val value: String) {
    YES("1"),
    NO("0")
}