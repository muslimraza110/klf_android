package com.khojaleadership.KLF.Activities.event_new.eventSections

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.Intiative.Intiative_Activity_Updated
import com.khojaleadership.KLF.Activities.event_new.delegates.EventDelegatesActivity
import com.khojaleadership.KLF.Activities.event_new.eventRegistration.EventRegistrationActivity
import com.khojaleadership.KLF.Activities.event_new.events.EventsViewModel
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsActivity
import com.khojaleadership.KLF.Activities.event_new.notification.PlannerNotificationsActivity
import com.khojaleadership.KLF.Activities.event_new.planner.EventPlannerActivity
import com.khojaleadership.KLF.Activities.event_new.programme.EventProgrammeActivity
import com.khojaleadership.KLF.Activities.event_new.speakers.EventSpeakersActivity
import com.khojaleadership.KLF.Adapter.event_new.eventSections.EventSectionsAdapter
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import com.khojaleadership.KLF.databinding.ActivityEventSectionsBinding
import timber.log.Timber


class EventSectionsActivity : AppCompatActivity() {

    private val eventsViewModel: EventsViewModel by viewModels()
    private val viewModel by viewModels<EventSectionsViewModel>()
    lateinit var binding: ActivityEventSectionsBinding
    lateinit var eventSectionAdapter: EventSectionsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityEventSectionsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.e("----------EventSectionsActivity")

        setUpViews()
        setUpObservers()
    }

    override fun onResume() {
        super.onResume()
        if(viewModel.authStore.event?.isUserRegistered == true){
            viewModel.getNotifications()
        }

    }

    private fun setUpObservers() {

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.rvSection.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvSection.visibility = View.VISIBLE
                    binding.tvError.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvSection.visibility = View.VISIBLE
                    binding.tvError.text = it.message
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.eventSectionList.observe(this, Observer {
            it?.let {
                if (it.isEmpty()) {
                    binding.tvError.visibility = View.VISIBLE
                } else {
                    binding.tvError.visibility = View.GONE
                }
                eventSectionAdapter.eventSectionList = it
                eventSectionAdapter.notifyDataSetChanged()
            }
        })

        viewModel.notificationCount.observe(this, Observer {
            it?.let { count ->
                if (count > 0) {
                    binding.tvNotificationCount.text = count.toString()
                    binding.tvNotificationCount.visibility = View.VISIBLE
                } else {
                    binding.tvNotificationCount.visibility = View.GONE
                }
            } ?: run {
                binding.tvNotificationCount.visibility = View.GONE
            }
        })

        eventsViewModel.setSummitEventsRegistration.observe(this, Observer {
            when (it)
            {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                }
            }
        })

    }

    private fun setUpViews() {

        if (viewModel.authStore.event?.isUserRegistered != true) {
            binding.layoutNotification.visibility = View.GONE
        }


        binding.imageBack.setOnClickListener {
            finish()
        }
        binding.layoutRegistration.setOnClickListener {
//            startActivity(Intent(this, EventRegistrationActivity::class.java))

            eventsViewModel.setSummitEventsRegistration {  mySuccessResponse ->

                print(mySuccessResponse)
                Timber.e(mySuccessResponse)


                val _registrationLink = viewModel.authStore.event?.registration_link

                if (_registrationLink != "") {

                    //redirect to new browser..

                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse( _registrationLink )

                    startActivity(openURL)

                } else {

                    Toast.makeText(this, Common.noRegistrationLinkFound,Toast.LENGTH_SHORT).show()

                }


//                eventsViewModel.eventsData.value?.getOrNull(0)?.let {
//                }


            }

        }

        binding.layoutNotification.setOnClickListener {
            startActivity(Intent(this, PlannerNotificationsActivity::class.java))
        }

        eventSectionAdapter = EventSectionsAdapter(
            eventSectionList = listOf(),
            context = this,
            onClick = { tag ->
                startAppropriateActivity(tag)
            }
        )

        binding.rvSection.apply {
            adapter = eventSectionAdapter
            layoutManager = LinearLayoutManager(this@EventSectionsActivity)
        }


    }

    private fun startAppropriateActivity(tag: String) {
        when (tag) {
            EventSectionTag.SPEAKERS.value -> {
                startActivity(Intent(this, EventSpeakersActivity::class.java))
            }
            EventSectionTag.DELEGATES.value -> {
                startActivity(Intent(this, EventDelegatesActivity::class.java))
            }
            EventSectionTag.PLANNER.value -> {
                if (viewModel.authStore.event?.isUserRegistered == true) {
                    startActivity(Intent(this, EventPlannerActivity::class.java))
                } else {
                    val dialog = AlertDialog.Builder(this)
                        .setMessage("You are not registered in this Event. Please get yourself registered to access this section.")
                        .setPositiveButton("OK") { dialog, which ->
                            dialog.dismiss()
                        }
                        .create()
                    dialog.setOnShowListener {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                            ContextCompat.getColor(
                                this,
                                R.color.appBlue
                            )
                        )
                    }
                    dialog.show()
                }
            }
            EventSectionTag.PROGRAMME.value -> {
                startActivity(Intent(this, EventProgrammeActivity::class.java))
            }
            EventSectionTag.MEETING_DETAILS.value -> {
//                startActivity(Intent(this, MeetingDetailsActivity::class.java))
                if (viewModel.authStore.event?.isUserRegistered == true) {

                    startActivity(Intent(this, MeetingDetailsActivity::class.java))

                } else {
                    val dialog = AlertDialog.Builder(this)
                        .setMessage("You are not registered in this Event. Please get yourself registered to access this section.")
                        .setPositiveButton("OK") { dialog, which ->
                            dialog.dismiss()
                        }
                        .create()
                    dialog.setOnShowListener {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                            ContextCompat.getColor(
                                this,
                                R.color.appBlue
                            )
                        )
                    }
                    dialog.show()
                }
            }
            EventSectionTag.INTIATIVE_DETAILS.value -> {
                if (viewModel.authStore.event?.isUserRegistered == true) {
                    startActivity(Intent(this, Intiative_Activity_Updated::class.java))
                } else {
                    val dialog = AlertDialog.Builder(this)
                        .setMessage("You are not registered in this Event. Please get yourself registered to access this section.")
                        .setPositiveButton("OK") { dialog, which ->
                            dialog.dismiss()
                        }
                        .create()
                    dialog.setOnShowListener {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                            ContextCompat.getColor(
                                this,
                                R.color.appBlue
                            )
                        )
                    }
                    dialog.show()
                }

//                if (tag == "initiative") {
//                    for (i in  eventSectionAdapter.eventSectionList) {
//                        println(i)
//                    }
//                    //                    eventSectionAdapter.eventSectionList.description
////                    binding.tvDescription.text = ""
//                }
//                val initiativeId: String = intent.getStringExtra("InitiativeId").toString()

//                Timber.e(eventSectionAdapter.eventSectionList[1].description)

//                Timber.e(eventSectionAdapter.eventSectionList[position].description)

//                Timber.e(eventSectionList[1].description)

//                intent.putExtra("InitiativeId",eventSectionAdapter.eventSectionList[1].description)
//                intent.putExtra("InitiativeId", eventSectionAdapter.eventSectionList[1].description.toInt())
//                startActivity(Intent(this, Intiative_Activity_Detail_Updated::class.java))

//                val intent = Intent(this, Intiative_Activity_Updated::class.java)
////                intent.putExtra("InitiativeId", eventSectionAdapter.eventSectionList[1].description)
//                startActivity(intent)
            }
        }
    }


}
