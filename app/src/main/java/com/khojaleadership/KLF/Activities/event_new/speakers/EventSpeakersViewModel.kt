package com.khojaleadership.KLF.Activities.event_new.speakers

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.PersonDetailResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class EventSpeakersViewModel(var app: Application) : AndroidViewModel(app) {

    var searchTextHasChanged = false

    var userClicked = false


    private val _speakers: MutableLiveData<List<SpeakerHeaderItem>> = MutableLiveData()
    val speakers: LiveData<List<SpeakerHeaderItem>> = _speakers

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _socialClickEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val socialClickEvent: LiveData<Event<String>> = _socialClickEvent

//    private val _twitterClickEvent: MutableLiveData<Event<String>> = MutableLiveData()
//    val twitterClickEvent: LiveData<Event<String>> = _twitterClickEvent

    private val _speakerClickEvent: MutableLiveData<Event<SpeakersUiModel>> = MutableLiveData()
    val speakerClickEvent: LiveData<Event<SpeakersUiModel>> = _speakerClickEvent

    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    var onSocialIconClick: ((String) -> Unit) = {
        _socialClickEvent.value = Event(it)
    }


    var onSpeakerClick: ((SpeakersUiModel) -> Unit) = {
        _speakerClickEvent.value = Event(it)
    }

    private val recentlyAdded = "Recently Added"
    private val allCountries = "All Countries"
    private val allIndustries = "All Industries"


    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    init {
        fetchData()
    }

    private fun fetchData() {
        getSpeakers("")
    }


    fun getSpeakers(searchText: String) {

        _eventState.value = Result.Loading

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getSpeakers(
                                eventId = authStore.event?.eventId ?: "",
                                userId = userId,
                                searchText = searchText
                        )

                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->
                                val tempList = mutableListOf<SpeakerHeaderItem>()

                                val headerRecentlyAdded = SpeakerHeaderItem(
                                        isOpened = false,
                                        title = recentlyAdded
                                )

                                setSpeakerListWithHeader(
                                        header = headerRecentlyAdded,
                                        list = data.recentlyAdded
                                )


                                val headerAllCountries = SpeakerHeaderItem(
                                        isOpened = false,
                                        title = allCountries
                                )

                                setSpeakerListWithHeader(
                                        header = headerAllCountries,
                                        list = data.allCountry
                                )

                                val headerAllIndustries = SpeakerHeaderItem(
                                        isOpened = false,
                                        title = allIndustries
                                )

                                setSpeakerListWithHeader(
                                        header = headerAllIndustries,
                                        list = data.allIndustry
                                )


                                if (headerRecentlyAdded.subItemsCount > 0) {
                                    tempList.add(headerRecentlyAdded)
                                }
                                if (headerAllCountries.subItemsCount > 0) {
                                    tempList.add(headerAllCountries)
                                }
                                if (headerAllIndustries.subItemsCount > 0) {
                                    tempList.add(headerAllIndustries)
                                }

                                tempList.firstOrNull()?.isOpened = true

                                _speakers.value = tempList

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message
                                )
                            } ?: run {
                                Timber.e(response.message)
                                _speakers.value = listOf()

                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message ?: Common.noRecordFoundMessage
                                )
                                searchTextHasChanged = true
                            }

                        } else {
                            Timber.e(response.message)

                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                            searchTextHasChanged = true
                        }
                    },
                    error = {
                        Timber.e(it)
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        searchTextHasChanged = true
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            searchTextHasChanged = true
        }

    }


    private fun setSpeakerListWithHeader(header: SpeakerHeaderItem, list: List<PersonDetailResponse?>?) {

        list?.filterNotNull()?.map { person ->
            mapToSpeakerUiModel(
                    responseData = person,
                    headerItem = header
            )
        } ?: mutableListOf()

    }


    private fun mapToSpeakerUiModel(responseData: PersonDetailResponse, headerItem: SpeakerHeaderItem): SpeakersUiModel {
        val speaker = SpeakersUiModel(
                name = responseData.name ?: "",
                description = responseData.shortDescription ?: "",
                userId = responseData.id ?: "",
                summitEventsFacultyId = responseData.summitEventsFacultyId ?: "",
                designation = responseData.designation ?: "",
                facebook = responseData.facebookLink ?: "",
                linkedIn = responseData.linkedInLink ?: "",
                photoUrl = responseData.userImage ?: "",
                twitter = responseData.twitterLink ?: "",
                date = responseData.speechDate ?: "",
                time = responseData.speechTime ?: "",
//                availableSlotData = responseData.meetingSlots,
                onClick = onSpeakerClick,
                onSocialIconClick = onSocialIconClick,
                context = app,
                header = headerItem,
                isDelegate = responseData.isDelegate == "1",
                isSpeaker = responseData.isSpeaker == "1"
        )
        headerItem.addSubItem(speaker)
        return speaker
    }

    companion object {
        const val msgSpeakerNotFound = "No speakers found."
    }

}