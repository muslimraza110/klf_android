package com.khojaleadership.KLF.Activities.Dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.khojaleadership.KLF.Activities.Splash_Login.LoginActivity;
import com.khojaleadership.KLF.Helper.App;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.DashBoard.OfflineCustomExpandableListAdapter;
import com.khojaleadership.KLF.Adapter.DashBoard.OfflineExpandableAdapter;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.ClickListner;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Child_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.OfflineExpandableParentModel;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Parent_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.OfflineExpandableChildModel;
import com.khojaleadership.KLF.R;


import java.util.ArrayList;
import java.util.List;


public class OfflineDashBoad extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ClickListner {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter adapter;

    ArrayList<Home_Parent_Model> pModel = new ArrayList<>();

    int[] menu_icons = new int[]{R.drawable.menu1_offline, R.drawable.menu2_offline, R.drawable.menu3_offline, R.drawable.menu4_offline
            , R.drawable.menu5_offline, R.drawable.menu6_offline, R.drawable.menu7_offline
            , R.drawable.menu8_offline, R.drawable.menu9_offline, R.drawable.menu10_offline
            , R.drawable.menu11_offline, R.drawable.menu12_offline, R.drawable.menu13_offline, R.drawable.menu12_offline
    };

    private ExpandableListView ContentexpandableListView;
    private ArrayList<OfflineExpandableParentModel> brand_ExpandableModels = new ArrayList<OfflineExpandableParentModel>();

    private OfflineExpandableAdapter mExpandableListAdapter;

    SharedPreferences sp;
    SharedPreferences.Editor Ed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Common.is_Online = false;
        //Common.isOffline = true;

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //added

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableListView = (ExpandableListView) findViewById(R.id.navList);


        //adding header view
        View listHeaderView = getLayoutInflater().inflate(R.layout.aa_dashboard_nav_header, null, false);
        listHeaderView.setMinimumHeight(200);
        listHeaderView.setMinimumWidth(310);

        expandableListView.addHeaderView(listHeaderView);

        TextView header_username, header_email;
        RelativeLayout header_dropdown_icon;

        header_username = (TextView) listHeaderView.findViewById(R.id.header_user_name);
        header_username.setText("Khoja_App");

        header_email = (TextView) listHeaderView.findViewById(R.id.header_user_email);
        header_email.setText("Khoja_App@inovedia.com");


        //adding footer view
        View footerView = getLayoutInflater().inflate(R.layout.aa_dashboard_nav_footer, null, false);
        expandableListView.addFooterView(footerView);

        footerView.findViewById(R.id.logout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(OfflineDashBoad.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });


        addDrawersItem();
        setupDrawer();


        //default open drawer layout
        drawerLayout.openDrawer(GravityCompat.START);

        findViewById(R.id.drawer_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If the navigation drawer is not open then open it, if its already open then close it.
                if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.openDrawer(GravityCompat.START);
                else drawerLayout.closeDrawer(GravityCompat.END);

            }
        });

        mainContentData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

    public void mainContentData() {
        ContentexpandableListView = (ExpandableListView) findViewById(R.id.list_brands);

        data();

        mExpandableListAdapter = new OfflineExpandableAdapter(this, brand_ExpandableModels);
        ContentexpandableListView.setAdapter(mExpandableListAdapter);

        ContentexpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    expandableListView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });
    }

    private void data() {


        List<OfflineExpandableChildModel> initiative = new ArrayList<OfflineExpandableChildModel>();
        initiative.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        initiative.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        initiative.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        initiative.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        OfflineExpandableParentModel Brand_initiative = new OfflineExpandableParentModel(R.drawable.icon4, "Initiative", initiative);

        List<OfflineExpandableChildModel> ic_forum = new ArrayList<OfflineExpandableChildModel>();
        ic_forum.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        ic_forum.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        ic_forum.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        OfflineExpandableParentModel Brand_forum = new OfflineExpandableParentModel(R.drawable.ic_forum_icon, "Forums", ic_forum);

        List<OfflineExpandableChildModel> subscription = new ArrayList<OfflineExpandableChildModel>();
        subscription.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        subscription.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        subscription.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        subscription.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        OfflineExpandableParentModel Brand_subscription = new OfflineExpandableParentModel(R.drawable.icon3, "Forums Subscriptions", subscription);

        List<OfflineExpandableChildModel> pinpost = new ArrayList<OfflineExpandableChildModel>();
        pinpost.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect post"));
        pinpost.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        pinpost.add(new OfflineExpandableChildModel(R.drawable.ic_view_icon, "This is my gtect-test post"));
        OfflineExpandableParentModel Brand_pinpost = new OfflineExpandableParentModel(R.drawable.icon2, "Pin Post", pinpost);

        brand_ExpandableModels.add(Brand_initiative);
        brand_ExpandableModels.add(Brand_forum);
        brand_ExpandableModels.add(Brand_subscription);
        brand_ExpandableModels.add(Brand_pinpost);
    }

    private void AdminData() {

        ArrayList<Home_Child_Model> cModel = new ArrayList<>();
        cModel.add(new Home_Child_Model("Admin"));
        cModel.add(new Home_Child_Model("Charities"));
        cModel.add(new Home_Child_Model("Business"));
        cModel.add(new Home_Child_Model("Other"));

        pModel.add(new Home_Parent_Model("Forums", cModel));


        ArrayList<Home_Child_Model> c2Model = new ArrayList<>();
        c2Model.add(new Home_Child_Model("News"));
        c2Model.add(new Home_Child_Model("Khoja Summits"));

        pModel.add(new Home_Parent_Model("Resources", c2Model));


        ArrayList<Home_Child_Model> c3Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Mission", c3Model));

        ArrayList<Home_Child_Model> c4Model = new ArrayList<>();
//        c4Model.add(new Home_Child_Model("Business"));
//        c4Model.add(new Home_Child_Model("Charities"));
        pModel.add(new Home_Parent_Model("Groups", c4Model));


        ArrayList<Home_Child_Model> c5Model = new ArrayList<>();
        c5Model.add(new Home_Child_Model("People"));
        c5Model.add(new Home_Child_Model("Recommendations"));
        c5Model.add(new Home_Child_Model("Governing Board of Trustees"));
        pModel.add(new Home_Parent_Model("Contacts", c5Model));

        ArrayList<Home_Child_Model> c6Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Projects", c6Model));


        ArrayList<Home_Child_Model> c7Model = new ArrayList<>();
        c7Model.add(new Home_Child_Model("Initiatives"));
        c7Model.add(new Home_Child_Model("Fundraising"));
        c7Model.add(new Home_Child_Model("Review Pledge Requests"));
        pModel.add(new Home_Parent_Model("Initiatives", c7Model));


        ArrayList<Home_Child_Model> c8Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Khoja Care", c8Model));


        ArrayList<Home_Child_Model> c9Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Events", c9Model));

        ArrayList<Home_Child_Model> c10Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Feedbacks", c10Model));

        ArrayList<Home_Child_Model> c11Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("FAQs", c11Model));


        ArrayList<Home_Child_Model> c12Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Administrations", c12Model));


        ArrayList<Home_Child_Model> c13Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Bugs", c13Model));

        ArrayList<Home_Child_Model> c14Model = new ArrayList<>();
        c14Model.add(new Home_Child_Model("View Profile"));
        c14Model.add(new Home_Child_Model("Edit Profile"));
        c14Model.add(new Home_Child_Model("Change Password"));
        pModel.add(new Home_Parent_Model("Settings", c14Model));


    }

    private void MemberData() {


        ArrayList<Home_Child_Model> cModel = new ArrayList<>();
        cModel.add(new Home_Child_Model("Admin"));
        cModel.add(new Home_Child_Model("Charities"));
        cModel.add(new Home_Child_Model("Business"));
        cModel.add(new Home_Child_Model("Other"));

        pModel.add(new Home_Parent_Model("Forums", cModel));


        ArrayList<Home_Child_Model> c2Model = new ArrayList<>();
        c2Model.add(new Home_Child_Model("News"));
        c2Model.add(new Home_Child_Model("Khoja Summits"));

        pModel.add(new Home_Parent_Model("Resources", c2Model));

        ArrayList<Home_Child_Model> c3Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Mission", c3Model));

        ArrayList<Home_Child_Model> c4Model = new ArrayList<>();
//        c4Model.add(new Home_Child_Model("Business"));
//        c4Model.add(new Home_Child_Model("Charities"));
        pModel.add(new Home_Parent_Model("Groups", c4Model));


        ArrayList<Home_Child_Model> c5Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Contacts", c5Model));

        ArrayList<Home_Child_Model> c6Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Projects", c6Model));


        ArrayList<Home_Child_Model> c7Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Initiatives", c7Model));


        ArrayList<Home_Child_Model> c8Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Khoja Care", c8Model));


        ArrayList<Home_Child_Model> c9Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Events", c9Model));

        ArrayList<Home_Child_Model> c10Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Feedback", c10Model));

        ArrayList<Home_Child_Model> c11Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("FAQs", c11Model));


        ArrayList<Home_Child_Model> c12Model = new ArrayList<>();
        c12Model.add(new Home_Child_Model("View Profile"));
        c12Model.add(new Home_Child_Model("Edit Profile"));
        c12Model.add(new Home_Child_Model("Change Password"));
        pModel.add(new Home_Parent_Model("Settings", c12Model));


    }


    private void addDrawersItem() {
        if (Common.is_Admin_flag==true) {
            AdminData();
        } else if (Common.is_Admin_flag==false) {
            MemberData();
        }


        adapter = new OfflineCustomExpandableListAdapter(this, pModel, menu_icons, this);
        expandableListView.setAdapter(adapter);

        //default first group expand
        expandableListView.expandGroup(0);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                //setting title text
                //getSupportActionBar().setTitle(lstTitle.get(groupPosition).toString());
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                //getSupportActionBar().setTitle("Faizan Shoukat");
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                String selectedItems = ((List) (lstChild.get(lstTitle.get(groupPosition))))
//                        .get(childPosition).toString();

                // getSupportActionBar().setTitle(selectedItems);

//                if (items[0].equals(lstTitle.get(groupPosition)))
//                    navigationManager.showFragment(selectedItems);
//                else
//                    throw new IllegalArgumentException("Not Supported Fragment");

                drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void setupDrawer() {
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getSupportActionBar().setTitle("Faizan Shoukat");
//                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
//                getSupportActionBar().setTitle(activityTitle);
//                invalidateOptionsMenu();
            }
        };

        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(toggle);

    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onGroupClick(int GroupPosition) {

    }

    @Override
    public void onRowClick(int GroupPosition, int ChildPosition, View v) {
    }

//
//    public void toOfflineMode(){
//        Intent offlineActivity=new Intent(OfflineDashBoad.this,MainActivity.class);
//        startActivity(offlineActivity);
//    }
}
