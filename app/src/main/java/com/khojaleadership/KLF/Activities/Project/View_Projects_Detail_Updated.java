package com.khojaleadership.KLF.Activities.Project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Adapter.Project.ProjectAccessRequestAdapter;
import com.khojaleadership.KLF.Adapter.Project.View_Projects_group_Adapter;
import com.khojaleadership.KLF.Adapter.Project.View_Projects_members_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.Accept_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Accept_Project_Access_Response_Model;
import com.khojaleadership.KLF.Model.Project_Models.Cancel_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Cancel_Project_Access_Response_Model;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectDetailModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectDetailTagsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.Project_Access_Request_List_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_Access_Request_List_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDetailGroupDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDetailUsersDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsModel;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.RemoveMemberFromProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Send_Project_Access_Request_Model;
import com.khojaleadership.KLF.Model.Project_Models.Send_Project_Access_Response_Model;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class View_Projects_Detail_Updated extends AppCompatActivity implements RecyclerViewClickListner {

    TextView p_name, p_person_name, p_date, p_location, p_description;

    TextView p_charity_type, p_target_audience, p_target_annual_spend;

    RecyclerView members_recyclerView;
    View_Projects_members_Adapter members_adapter;

    RecyclerView group_recyclerView;
    View_Projects_group_Adapter group_adapter;

    RecyclerView pending_access_recyclerView;
    ProjectAccessRequestAdapter pending_access_adapter;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    RelativeLayout add_member_btn, join_member_btn, project_leave_layout, project_final_leave_layout;

    TextView member_not_found, related_group_not_found, pending_access_request_not_found;

    LinearLayout project_access_requests_layout;

    ProjectsDataModel projects_data_detail = new ProjectsDataModel();
    String isProjectDetail = "-1";
    String ProjectId="";

    ArrayList<ProjectsDetailUsersDataModel> projects_detail_user_data_list = new ArrayList<>();
    ArrayList<ProjectsDetailGroupDataModel> projects_detail_group_data_list = new ArrayList<>();
    ArrayList<ProjectDetailTagsDataModel> projects_detail_tags_data_list = new ArrayList<>();

    ArrayList<Project_Access_Request_List_Data_Model> projects_detail_access_request_list = new ArrayList<>();

    ArrayList<GetAllGroup_ProjectListDataModel> project_group_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__projects__detail__updated);

        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("View Project");

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, View_Projects_Detail_Updated.this);

                finish();
            }
        });


        Intent intent = getIntent();
        projects_data_detail = intent.getParcelableExtra("ProjectData");


        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            isProjectDetail = bundle.getString("isProjectDetail");
            ProjectId=bundle.getString("ProjectId");
            Log.d("ProjectId","Project id when bundle not null :"+ProjectId);
            Log.d("ProjectId","isProjectDetail :"+isProjectDetail);
        }else {
            //for oncreate functionality we required project id
            //ProjectId=projects_data_detail.getProjects__id();
            Log.d("ProjectId","Project id when bundle null :"+ProjectId);
        }

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        if (isProjectDetail.equals("1")) {
            ProjectId=projects_data_detail.getProjects__id();
            onGetProjectDetailData(projects_data_detail.getProjects__id());
        } else if (isProjectDetail.equals("0")) {
            onGetProjectListData(ProjectId);
        }

        add_member_btn = findViewById(R.id.add_member_btn);
        join_member_btn = findViewById(R.id.join_request_btn);
        project_leave_layout = findViewById(R.id.project_leave_layout);
        project_final_leave_layout = findViewById(R.id.project_final_leave_layout);


        add_member_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("ProjectId","Project id in add member :"+ProjectId);
                //Common.is_for_whitelist_contacts = false;
                Intent contact_list = new Intent(View_Projects_Detail_Updated.this, Add_Member_In_Project.class);
                contact_list.putExtra("ProjectId",ProjectId);
                contact_list.putExtra("isProjectDetail","1");
                startActivity(contact_list);
            }
        });

        join_member_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading_dialog.show();
                SendAccessRequest(ProjectId);
            }
        });

        project_leave_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading_dialog.show();

                String toast_msg = "Project Access Request Cancel Successfully.";
                CancelAccessRequest(Common.login_data.getData().getId(), ProjectId, toast_msg);
                //RemoveMember(Common.projects_data_detail.getCreators__id(),Common.projects_data_detail_id_new,toast_msg);
            }
        });

        project_final_leave_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading_dialog.show();
                String toast_msg = "Project Leave Successfully.";
                RemoveMember(Common.login_data.getData().getId(), ProjectId, toast_msg);
                //CancelAccessRequest(Common.projects_data_detail.getCreators__id(),Common.projects_data_detail_id_new,toast_msg);
            }
        });


        findViewById(R.id.floating_action_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(View_Projects_Detail_Updated.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

    }

    private void onGetProjectListData(final String project_id) {


        Call<ProjectsModel> responseCall = retrofitInterface.getProjectsList("application/json", Common.auth_token);
        responseCall.enqueue(new Callback<ProjectsModel>() {
            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    ProjectsModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    ArrayList<ProjectsDataModel> projects_data_list = responseModel.getData();

                    if (projects_data_list != null) {
                        // loading_dialog.dismiss();
                        //Common.projects_data_list = project_data;


                        String final_project_id = project_id;
                        for (int i = 0; i < projects_data_list.size(); i++) {

                            if (projects_data_list.get(i).getProjects__id().equals(final_project_id)) {

                                // Common.projects_data_detail = Common.projects_data_list.get(i);
                                //Common.projects_data_detail_id_new=Common.projects_data_list.get(i).getProjects__id();

                                projects_data_detail = projects_data_list.get(i);
                                final_project_id= projects_data_list.get(i).getProjects__id();
                                i = projects_data_list.size();
                            }

                        }


                        onGetProjectDetailData(project_id);
                    }

                } else {
                    //onGetProjectDetailData(Common.projects_data_detail_id_new);
                }

            }

            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                //loading_dialog.dismiss();
                //onGetProjectDetailData(Common.projects_data_detail_id_new);
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void onGetProjectDetailData(final String project_id) {


        Call<ProjectDetailModel> responseCall = retrofitInterface.getProjectsDetail("application/json", Common.auth_token, project_id, "1", "1000", Common.login_data.getData().getId());

        responseCall.enqueue(new Callback<ProjectDetailModel>() {
            @Override
            public void onResponse(Call<ProjectDetailModel> call, Response<ProjectDetailModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    ProjectDetailModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")) {
                        //data is null
                    } else {

                        ArrayList<ProjectsDetailUsersDataModel> users_data = responseModel.getProject_users();

                        ArrayList<ProjectsDetailGroupDataModel> group_data = responseModel.getProject_group();

                        ArrayList<ProjectDetailTagsDataModel> tags_data = responseModel.getProject_tags();

                        //loading_dialog.dismiss();
                       // Common.projects_detail_user_data_list = users_data;
                        projects_detail_user_data_list = users_data;
                        projects_detail_group_data_list = group_data;
                        projects_detail_tags_data_list = tags_data;

                    }


                    GetProjectAccessRequestList(project_id);

                } else {
//                    loading_dialog.dismiss();
                    Log.d("ProjectDetailIssue","response code :"+response.code());
                    GetProjectAccessRequestList(project_id);
                }

            }

            @Override
            public void onFailure(Call<ProjectDetailModel> call, Throwable t) {
                //loading_dialog.dismiss();
                Log.d("ProjectDetailIssue","on failure :"+t.getMessage());
                GetProjectAccessRequestList(project_id);
            }
        });


    }

    private void GetProjectAccessRequestList(String project_id) {


        Call<Project_Access_Request_List_Model> responseCall = retrofitInterface.getProjectAccessRequestList("application/json", Common.auth_token, project_id);

        responseCall.enqueue(new Callback<Project_Access_Request_List_Model>() {
            @Override
            public void onResponse(Call<Project_Access_Request_List_Model> call, Response<Project_Access_Request_List_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Project_Access_Request_List_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                    } else {

                        ArrayList<Project_Access_Request_List_Data_Model> list = responseModel.getData();
                        projects_detail_access_request_list = list;

                    }


                    intialize_view();
                } else {
                    intialize_view();
                }

            }

            @Override
            public void onFailure(Call<Project_Access_Request_List_Model> call, Throwable t) {
                // loading_dialog.dismiss();
                intialize_view();
            }
        });


    }

    private void SendAccessRequest(final String project_id) {

        Send_Project_Access_Request_Model request_model = new Send_Project_Access_Request_Model();
        request_model.setProject_id(project_id);
        request_model.setUser_id(Common.login_data.getData().getId());

        Call<Send_Project_Access_Response_Model> responseCall = retrofitInterface.SendProjectAccessRequest("application/json", Common.auth_token, request_model);

        responseCall.enqueue(new Callback<Send_Project_Access_Response_Model>() {
            @Override
            public void onResponse(Call<Send_Project_Access_Response_Model> call, Response<Send_Project_Access_Response_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Send_Project_Access_Response_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                    } else if (status.equals("success")) {
                        Toast.makeText(View_Projects_Detail_Updated.this, "Project access request has been sent.", Toast.LENGTH_SHORT).show();
                        onGetProjectDetailData(project_id);
                    }

                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Send_Project_Access_Response_Model> call, Throwable t) {
                loading_dialog.dismiss();
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    private void AcceptAccessRequest(final String user_id, final String project_id) {

        Accept_Project_Access_Request_Model request_model = new Accept_Project_Access_Request_Model();
        request_model.setProject_id(project_id);
        request_model.setUser_id(user_id);

        Call<Accept_Project_Access_Response_Model> responseCall = retrofitInterface.AcceptProjectAccessRequest("application/json", Common.auth_token, request_model);

        responseCall.enqueue(new Callback<Accept_Project_Access_Response_Model>() {
            @Override
            public void onResponse(Call<Accept_Project_Access_Response_Model> call, Response<Accept_Project_Access_Response_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Accept_Project_Access_Response_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                    } else if (status.equals("success")) {
                        Toast.makeText(View_Projects_Detail_Updated.this, "Project access request has been accepted.", Toast.LENGTH_SHORT).show();
                        onGetProjectDetailData(project_id);
                    }

                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Accept_Project_Access_Response_Model> call, Throwable t) {
                loading_dialog.dismiss();
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    private void CancelAccessRequest(final String user_id, final String project_id, final String toast_message) {

        Cancel_Project_Access_Request_Model request_model = new Cancel_Project_Access_Request_Model();
        request_model.setProject_id(project_id);
        request_model.setUser_id(user_id);

        Call<Cancel_Project_Access_Response_Model> responseCall = retrofitInterface.CancelProjectAccessRequest("application/json", Common.auth_token, request_model);

        responseCall.enqueue(new Callback<Cancel_Project_Access_Response_Model>() {
            @Override
            public void onResponse(Call<Cancel_Project_Access_Response_Model> call, Response<Cancel_Project_Access_Response_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Cancel_Project_Access_Response_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")) {
                        //data is null
                    } else if (status.equals("success")) {
                        Toast.makeText(View_Projects_Detail_Updated.this, toast_message, Toast.LENGTH_SHORT).show();
                        onGetProjectDetailData(project_id);
                    }

                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Cancel_Project_Access_Response_Model> call, Throwable t) {
                loading_dialog.dismiss();
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    private void RemoveMember(String user_id, String project_id, final String toast_Message) {


        RemoveMemberFromProjectRequestModel requestModel = new RemoveMemberFromProjectRequestModel();

        requestModel.setUser_id(user_id);
        requestModel.setProject_id(project_id);  //default


        Call<RemoveMemberFromProjectResponseModel> responseCall = retrofitInterface.RemoveMemberFromProject("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<RemoveMemberFromProjectResponseModel>() {
            @Override
            public void onResponse(Call<RemoveMemberFromProjectResponseModel> call, Response<RemoveMemberFromProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;


                    RemoveMemberFromProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    onGetProjectDetailData(projects_data_detail.getProjects__id());
                    //onAlreadyWhitelistedContactFunction();
                    Toast.makeText(View_Projects_Detail_Updated.this, toast_Message, Toast.LENGTH_LONG).show();

                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(View_Projects_Detail_Updated.this, "Member Removed failed", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<RemoveMemberFromProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(View_Projects_Detail_Updated.this, "Member removed failed", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void intialize_view() {


        try {

            p_name = findViewById(R.id.p_name);
            p_person_name = findViewById(R.id.p_person_name);
            p_date = findViewById(R.id.p_date);
            p_location = findViewById(R.id.p_location);
            p_description = findViewById(R.id.p_description);

            p_charity_type = findViewById(R.id.p_charity_type);
            p_target_audience = findViewById(R.id.p_target_audience);
            p_target_annual_spend = findViewById(R.id.p_target_audience);


            member_not_found = findViewById(R.id.member__text);
            related_group_not_found = findViewById(R.id.related_group__text);
            pending_access_request_not_found = findViewById(R.id.pending_access_text);

            //first all these texts are not visible
            member_not_found.setVisibility(View.GONE);
            related_group_not_found.setVisibility(View.GONE);
            pending_access_request_not_found.setVisibility(View.GONE);


            project_access_requests_layout = findViewById(R.id.project_access_requests_layout);
            project_access_requests_layout.setVisibility(View.GONE);


            //.........for admin...project access request section visible...............
            if (Common.is_Admin_flag == true) {
                project_access_requests_layout.setVisibility(View.VISIBLE);
                join_member_btn.setVisibility(View.VISIBLE);
                add_member_btn.setVisibility(View.VISIBLE);
            } else if (Common.is_Admin_flag == false) {
                project_access_requests_layout.setVisibility(View.GONE);
                //member cannot add members in project
                join_member_btn.setVisibility(View.VISIBLE);
                add_member_btn.setVisibility(View.GONE);
            }


            if (projects_data_detail != null) {
                //member ko b visible ni ho ga agr admin h phir b.....adina
//            if (Common.projects_data_detail.getCreators__id().equals(Common.login_data.getData().getId())) {
//                //if project created by this user
//                project_access_requests_layout.setVisibility(View.VISIBLE);
//            }

                p_name.setText(projects_data_detail.getProjects__name());
                p_person_name.setText(projects_data_detail.getProjects__name());
                //p_person_name.setText(Common.projects_data_detail.getCreators__first_name() + " " + Common.projects_data_detail.getCreators__last_name());
                p_date.setText(projects_data_detail.getProjects__start_date() + " " + projects_data_detail.getProjects__created());
                p_location.setText(projects_data_detail.getProjects__city() + "," + projects_data_detail.getProjects__country());
                p_description.setText(projects_data_detail.getProjects__description());

                if (projects_data_detail.getCharity_type()!=null) {
                    Log.d("SpinnerIssue","charity type :"+projects_data_detail.getCharity_type().getName());
                    p_charity_type.setText(projects_data_detail.getCharity_type().getName());
                }else {
                    Log.d("SpinnerIssue","charity type null");
                }
                if (projects_data_detail.getTarget_audience()!=null) {
                    p_target_audience.setText(projects_data_detail.getTarget_audience().getName());
                }
                if (projects_data_detail.getTarget_annual_spend()!=null) {
                    p_target_annual_spend.setText(projects_data_detail.getTarget_annual_spend().getName());
                }
            }


            //..................................this is for join,leave and cancel group btn................................
            Boolean is_member_flag = false, is_requested_flag = false;


            //if user is already member then join btn visibilty gone
            if (projects_detail_user_data_list != null) {
                if (projects_detail_user_data_list.size() == 0) {
                    //for use
//                is_member_flag=true;
                } else {

                    for (int i = 0; i < projects_detail_user_data_list.size(); i++) {
                        if (projects_detail_user_data_list.get(i).getUsers__id().equals(Common.login_data.getData().getId())) {
                            join_member_btn.setVisibility(View.GONE);
                            project_leave_layout.setVisibility(View.GONE);
                            project_final_leave_layout.setVisibility(View.VISIBLE);

//                        is_member_flag=false;
                            is_member_flag = true;

                        } else {
                            //for use
//                        is_member_flag=true;
                        }
                    }
                }

            }


            //if user already sent join request then join btn visibilty gone
            if (projects_detail_access_request_list != null) {
                if (projects_detail_access_request_list.size() == 0) {
                } else {
                    for (int i = 0; i < projects_detail_access_request_list.size(); i++) {
                        if (projects_detail_access_request_list.get(i).getUser_id().equals(Common.login_data.getData().getId())) {
                            join_member_btn.setVisibility(View.GONE);
                            project_leave_layout.setVisibility(View.VISIBLE);
                            project_final_leave_layout.setVisibility(View.GONE);

                            is_requested_flag = true;
                        } else {
                        }
                    }
                }
            }


            //if user is already member or sent request button
            if (projects_detail_user_data_list == null && projects_detail_access_request_list == null) {
                if (projects_detail_user_data_list.size() == 0 && projects_detail_access_request_list.size() == 0) {
                    join_member_btn.setVisibility(View.VISIBLE);
                    project_leave_layout.setVisibility(View.GONE);
                    project_final_leave_layout.setVisibility(View.GONE);
                }
            } else {
                if (projects_detail_user_data_list.size() == 0 && projects_detail_access_request_list.size() == 0) {
                    join_member_btn.setVisibility(View.VISIBLE);
                    project_leave_layout.setVisibility(View.GONE);
                    project_final_leave_layout.setVisibility(View.GONE);
                }
            }

            if (is_member_flag == true && is_requested_flag == false) {
                join_member_btn.setVisibility(View.GONE);
                project_leave_layout.setVisibility(View.GONE);
                project_final_leave_layout.setVisibility(View.VISIBLE);
            }

            if (is_member_flag == false && is_requested_flag == true) {
                join_member_btn.setVisibility(View.GONE);
                project_leave_layout.setVisibility(View.VISIBLE);
                project_final_leave_layout.setVisibility(View.GONE);
            }

            if (is_member_flag == false && is_requested_flag == false) {
                join_member_btn.setVisibility(View.VISIBLE);
                project_leave_layout.setVisibility(View.GONE);
                project_final_leave_layout.setVisibility(View.GONE);
            }

            //..................................this is for join,leave and cancel group btn end................................
            //member data
            if (projects_detail_user_data_list != null) {
                Log.d("ProjectDetailIssue", "user list :" + projects_detail_user_data_list .size());
                members_recyclerView = (RecyclerView) findViewById(R.id.p_member_reccyclerview);
                members_recyclerView.setHasFixedSize(false);
                members_recyclerView.setLayoutManager(new LinearLayoutManager(this));

                if (projects_detail_user_data_list.size() == 0) {
                    member_not_found.setVisibility(View.VISIBLE);

                    members_adapter = new View_Projects_members_Adapter(this, projects_detail_user_data_list, this);
                    members_recyclerView.setAdapter(members_adapter);
                } else {
                    members_adapter = new View_Projects_members_Adapter(this, projects_detail_user_data_list, this);
                    members_recyclerView.setAdapter(members_adapter);
                }
            } else {
                member_not_found.setVisibility(View.VISIBLE);
            }


            //group data
            if (projects_detail_group_data_list != null) {

                Log.d("ProjectDetailIssue", "group list :" + projects_detail_group_data_list.size());
                group_recyclerView = (RecyclerView) findViewById(R.id.p_related_reccyclerview);
                group_recyclerView.setHasFixedSize(false);
                group_recyclerView.setLayoutManager(new LinearLayoutManager(this));

                if (projects_detail_group_data_list.size() == 0) {
                    related_group_not_found.setVisibility(View.VISIBLE);
                    group_adapter = new View_Projects_group_Adapter(this, projects_detail_group_data_list, this);
                    group_recyclerView.setAdapter(group_adapter);
                } else {
                    group_adapter = new View_Projects_group_Adapter(this, projects_detail_group_data_list, this);
                    group_recyclerView.setAdapter(group_adapter);

                }
            } else {
                Log.d("ProjectDetailIssue", "group list null");
                related_group_not_found.setVisibility(View.VISIBLE);
            }

            //pending access request data
            if (projects_detail_access_request_list != null) {
                Log.d("ProjectDetailIssue", "access request list :" + projects_detail_access_request_list .size());

                pending_access_recyclerView = (RecyclerView) findViewById(R.id.p_pending_requests_reccyclerview);
                pending_access_recyclerView.setHasFixedSize(false);
                pending_access_recyclerView.setLayoutManager(new LinearLayoutManager(this));


                if (projects_detail_access_request_list.size() == 0) {
                    project_access_requests_layout.setVisibility(View.GONE);
                } else {
                    pending_access_adapter = new ProjectAccessRequestAdapter(this, projects_detail_access_request_list, this);
                    pending_access_recyclerView.setAdapter(pending_access_adapter);
                }
            } else {
                Log.d("ProjectDetailIssue", "group access request list null");
                project_access_requests_layout.setVisibility(View.GONE);
            }
            //tags data

            loading_dialog.dismiss();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

            loading_dialog.dismiss();
        }


    }

    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {
        switch (v.getId()) {
            case R.id.accept_member:
                loading_dialog.show();
                AcceptAccessRequest(projects_detail_access_request_list.get(position).getUser_id(), projects_data_detail.getProjects__id());
                break;
            case R.id.reject_member:
                loading_dialog.show();
                String toast_msg = "Project access request has been denied.";
                CancelAccessRequest(projects_detail_access_request_list.get(position).getUser_id(), projects_data_detail.getProjects__id(), toast_msg);
                break;
            case R.id.remove_member:
                loading_dialog.show();
                String toast_message = "Member Removed successfully";
                RemoveMember(projects_detail_user_data_list.get(position).getUsers__id(), projects_data_detail.getProjects__id(), toast_message);
                break;

            case R.id.project_member_name:
                String member_id = projects_detail_user_data_list.get(position).getUsers__id();

//                Common.master_search_view_profile = true;
//                Common.view_profile_id = member_id;
                Intent detail=new Intent(View_Projects_Detail_Updated.this, View_Profile_Activity_Updated.class);
                detail.putExtra("view_profile_id",member_id);
                detail.putExtra("isMasterSearch","1");
                startActivity(detail);
                break;
            case R.id.project_group_row:
                String group_id = projects_detail_group_data_list.get(position).getGroups__id();
                loading_dialog.show();
                getAllGroups(group_id);
                break;
            default:
                break;

        }

    }

    private void getAllGroups(final String group_id) {

        Call<GetAllGroup_ProjectListModel> call = retrofitInterface.getProjectAllGroupList("application/json", Common.auth_token);

        call.enqueue(new Callback<GetAllGroup_ProjectListModel>() {
            @Override
            public void onResponse(Call<GetAllGroup_ProjectListModel> call, Response<GetAllGroup_ProjectListModel> response) {
                if (response.isSuccessful()) {
                    String status, message;


                    GetAllGroup_ProjectListModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<GetAllGroup_ProjectListDataModel> list = new ArrayList<>();
                    list = responseModel.getData();


                    GetAllGroup_ProjectListDataModel all_group_data_detail=new GetAllGroup_ProjectListDataModel();
                    if (list != null) {
                        loading_dialog.dismiss();
                        project_group_list = list;

                        for (int i = 0; i < project_group_list.size(); i++) {

                            if (project_group_list.get(i).getGroup_id().equals(group_id)) {

                                //Common.all_group_data_detail = project_group_list.get(i);
                                all_group_data_detail = project_group_list.get(i);

                                i = project_group_list.size();
                            }
                        }

//                        Common.is_all_group_detail = true;
//                        Common.viewprofile_group_detail = false;
                        Intent view_event = new Intent(View_Projects_Detail_Updated.this, GroupsDetail.class);
                        view_event.putExtra("all_group_data_detail",all_group_data_detail);
                        view_event.putExtra("isAllGroupDetail","1");
                        view_event.putExtra("ViewProfileDetail","0");
                        //view_event.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(view_event);
                        //finish();

                    }


                }
            }

            @Override
            public void onFailure(Call<GetAllGroup_ProjectListModel> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
