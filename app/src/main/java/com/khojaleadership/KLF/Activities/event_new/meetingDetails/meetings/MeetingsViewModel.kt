package com.khojaleadership.KLF.Activities.event_new.meetingDetails.meetings

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsActivity
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.MeetingRequestStatusTypes
import com.khojaleadership.KLF.Model.event_new.MeetingsResponse
import com.khojaleadership.KLF.Model.event_new.PersonDetailResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class MeetingsViewModel(val app: Application) : AndroidViewModel(app) {

    private var summitEventFacultyId: String? = null
    var meetingParentId: String? = null
    private var eventId: String? = null
    private var userId: String? = null

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _meetingItems: MutableLiveData<List<MeetingHeaderItem>> = MutableLiveData()
    val meetingItems: LiveData<List<MeetingHeaderItem>> = _meetingItems

    private val _showMeetingsDialogEvent: MutableLiveData<Event<MeetingHeaderItem>> = MutableLiveData()
    val showMeetingsDialogEvent: LiveData<Event<MeetingHeaderItem>> = _showMeetingsDialogEvent

    private val _showRejectDialogEvent: MutableLiveData<Event<MeetingItemUiModel>> = MutableLiveData()
    val showRejectDialogEvent: LiveData<Event<MeetingItemUiModel>> = _showRejectDialogEvent

    private val _showAcceptDialogEvent: MutableLiveData<Event<MeetingItemUiModel>> = MutableLiveData()
    val showAcceptDialogEvent: LiveData<Event<MeetingItemUiModel>> = _showAcceptDialogEvent

    private val _showCancelDialogEvent: MutableLiveData<Event<MeetingItemUiModel>> = MutableLiveData()
    val showCancelDialogEvent: LiveData<Event<MeetingItemUiModel>> = _showCancelDialogEvent

    private val _showDeleteDialogEvent: MutableLiveData<Event<MeetingHeaderItem>> = MutableLiveData()
    val showDeleteDialogEvent: LiveData<Event<MeetingHeaderItem>> = _showDeleteDialogEvent

    private val _editMeetingEvent: MutableLiveData<Event<MeetingHeaderItem>> = MutableLiveData()
    val editMeetingEvent: LiveData<Event<MeetingHeaderItem>> = _editMeetingEvent


    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    private val _sendNotesEvent: MutableLiveData<Result<Unit>> = MutableLiveData()
    val sendNotesEvent: LiveData<Result<Unit>> = _sendNotesEvent

    private val onCancelClick: (MeetingItemUiModel) -> Unit = {
        _showCancelDialogEvent.value = Event(it)
    }

    private val onAcceptClick: (MeetingItemUiModel) -> Unit = {
        _showAcceptDialogEvent.value = Event(it)
    }

    private val onRejectClick: (MeetingItemUiModel) -> Unit = {
        _showRejectDialogEvent.value = Event(it)
    }

    private val onMeetingNotesCLick: (MeetingHeaderItem) -> Unit = {
        _showMeetingsDialogEvent.value = Event(it)
    }

    private val onDeleteClick: (MeetingHeaderItem) -> Unit = {
        _showDeleteDialogEvent.value = Event(it)
    }

    private val onEditClick: (MeetingHeaderItem) -> Unit = {
        _editMeetingEvent.value = Event(it)
    }

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")


    fun getDataFromIntent(intent: Intent?) {
        userId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_USER_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_USER_ID)
        } else {
            authStore.loginData?.id
        }

        eventId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_EVENT_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_EVENT_ID)
        } else {
            authStore.event?.eventId
        }

        summitEventFacultyId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID)
        } else {
            authStore.event?.facultyId
        }

        if (intent?.hasExtra(MeetingDetailsActivity.KEY_MEETING_PARENT_ID) == true) {
            meetingParentId = intent.getStringExtra(MeetingDetailsActivity.KEY_MEETING_PARENT_ID)
        }

    }


    fun getMeetings() {
        _eventState.value = Result.Loading

        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getMeetings(
                                eventId = eventId ?: "",
                                userId = userId,
                                facultyId = summitEventFacultyId ?: ""
                        )
                        if (response.status == ResponseStatus.SUCCESS) {

                            response.data?.let { data ->

                                val tempList = data.map {
                                    mapToMeetingHeaderItem(it)
                                }

                                meetingParentId?.let {
                                    tempList.forEach {
//                                        if (it.meetingId == meetingParentId) {
                                        if (it.subItems.any { item -> item.meetingChildId ==  meetingParentId}) {
                                            it.isOpened = true
                                        }
                                    }
                                }


                                _meetingItems.value = tempList
                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message
                                )

                            } ?: run {
                                _eventState.value = Result.Success(
                                        message = response.message ?: Common.noRecordFoundMessage,
                                        data = Unit
                                )
                            }
                        } else {
                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }
    }



    fun updateMeetingStatus(newStatus: String, item: MeetingItemUiModel) {
        var newStatuss = newStatus
        _eventState.value = Result.Loading
        if (!item.iAmHost && newStatuss == MeetingRequestStatusTypes.MEETING_CANCELED.value) {
            Timber.e("launchSafelyIF" + item.iAmHost)
            newStatuss = MeetingRequestStatusTypes.MEETING_SELF_CANCELED.value
            Timber.e("" + newStatus)
        } else {
//            Timber.e("" + item.iAmHost)
//            newStatuss = MeetingRequestStatusTypes.MEETING_SELF_CANCELED.value
//            Timber.e("launchSafelyELSE" + newStatus)
        }
        userId?.let { userId ->
            viewModelScope.launchSafely(

                block = {

                    val response = eventsRepository.updateSummitEventMeetingRequest(
                        userId = userId,
                        newStatus = newStatuss,
                        meetingChildId = item.meetingChildId
                    )
                    if (response.status == ResponseStatus.SUCCESS) {

                        //newStatus == MeetingRequestStatusTypes.MEETING_CANCELED.value
                        if (newStatus == MeetingRequestStatusTypes.MEETING_CANCELED.value) {
                            val tempList = _meetingItems.value
                            _meetingItems.value = tempList

                            _eventState.value = Result.Success(
                                message = response.message ?: Common.noRecordFoundMessage,
                                data = Unit
                            )
                            _messageEvent.value = Event(
                                response.message
                                    ?: ""
                            )
                        } else {

                        }



                        item.meetingStatus = newStatuss

                        var tempList = _meetingItems.value

                        if (newStatuss == MeetingRequestStatusTypes.MEETING_CANCELED.value || newStatuss == MeetingRequestStatusTypes.MEETING_SELF_CANCELED.value) {
                            tempList?.forEach {
                                it.subItems.removeAll { subItem ->
                                    subItem.header.isOpened = false
                                    subItem.meetingChildId == item.meetingChildId

                                }
                            }

                        }
                        tempList = tempList?.filter {
                            it.subItems.size > 1
                        }

                        tempList?.forEach {
                            it.subItems.forEach { subitem ->
                                subitem.isLast = false
                            }
                            it.subItems.last()?.isLast = true
                        }

                        _meetingItems.value = tempList

                        _eventState.value = Result.Success(
                            message = response.message ?: Common.noRecordFoundMessage,
                            data = Unit
                        )
                        _messageEvent.value = Event(
                            response.message
                                ?: ""
                        )

                    } else {
                        _eventState.value = Result.Error(
                            message = response.message ?: Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(
                            response.message
                                ?: Common.somethingWentWrongMessage
                        )
                    }
                },
                error = {
                    _eventState.value = Result.Error(
                        message = Common.somethingWentWrongMessage
                    )
                    _messageEvent.value = Event(Common.somethingWentWrongMessage)
                }
            )
        } ?: run {
            _eventState.value = Result.Error(
                message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    fun updateMeetingNotes(notes: String, item: MeetingHeaderItem) {
        _sendNotesEvent.value = Result.Loading

        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.updateSummitEventMeetingNotes(
                                userId = userId,
                                notes = notes,
                                meetingParentId = item.meetingId
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            item.meetingNotes = notes

                            val tempList = _meetingItems.value
                            _meetingItems.value = tempList

                            _sendNotesEvent.value = Result.Success(
                                    message = response.message ?: Common.noRecordFoundMessage,
                                    data = Unit
                            )
                            _messageEvent.value = Event(response.message
                                    ?: "")

                        } else {
                            _sendNotesEvent.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        _sendNotesEvent.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _sendNotesEvent.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    private fun mapToMeetingHeaderItem(meetingData: MeetingsResponse): MeetingHeaderItem {
        val meetingHeaderItem = MeetingHeaderItem(
                meetingNotes = meetingData.hostDetails?.notes ?: "",
                onMeetingNotesClick = onMeetingNotesCLick,
                context = app,
                startEndTime = meetingData.hostDetails?.meetingStartEndTime ?: "",
                date = meetingData.hostDetails?.meetingDate ?: "",
                endTime = meetingData.hostDetails?.meetingEndTime ?: "",
                startTime = meetingData.hostDetails?.meetingStartTime ?: "",
                isOpened = false,
                meetingId = meetingData.hostDetails?.meetingId ?: "",
                onDeleteClick = onDeleteClick,
                onEditClick = onEditClick,
                iAmHost = meetingData.hostDetails?.iAmHost ?: false
        )

        val itemList = mutableListOf<MeetingItemUiModel>()

        itemList.add(
                MeetingItemUiModel(
                        context = app,
                        onRejectClick = onRejectClick,
                        onCancelClick = onCancelClick,
                        onAcceptClick = onAcceptClick,
                        isLast = false,
                        requestedBy = meetingData.hostDetails?.requestedById ?: "",
                        requestedTo = meetingData.hostDetails?.requestedById ?: "",
                        header = meetingHeaderItem,
                        meetingStatus = meetingData.hostDetails?.meetingStatus ?: "",
                        iAmHost = meetingData.hostDetails?.iAmHost ?: false,
                        meetingChildId = meetingData.hostDetails?.meetingId ?: "",
                        myFacultyId = summitEventFacultyId ?: "",
                        personDetail = mapToPersonDetailUiModel(meetingData.hostDetails?.requestedByDetails)
                )
        )

        val nonNullList = meetingData.meetingDetails?.filterNotNull() ?: listOf()

        nonNullList.forEachIndexed { index, element ->
            itemList.add(
                    MeetingItemUiModel(
                            context = app,
                            onRejectClick = onRejectClick,
                            onCancelClick = onCancelClick,
                            onAcceptClick = onAcceptClick,
                            isLast = index == nonNullList.size - 1,
                            requestedBy = meetingData.hostDetails?.requestedById ?: "",
                            requestedTo = element.requestedTo ?: "",
                            header = meetingHeaderItem,
                            meetingStatus = element.meetingStatus ?: "",
                            iAmHost = meetingData.hostDetails?.iAmHost ?: false,
                            meetingChildId = element.meetingIdForDelegate ?: "",
                            myFacultyId = summitEventFacultyId ?: "",
                            personDetail = mapToPersonDetailUiModel(element.requestedToDetails)
                    )
            )
        }

        meetingHeaderItem.addSubItems(0, itemList)
        return meetingHeaderItem
    }

    fun cancelMeetingRequest(item: MeetingHeaderItem) {
        _eventState.value = Result.Loading

        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.cancelSummitEventMeetingRequest(
                                userId = userId,
                                meetingParentId = item.meetingId
                        )
                        if (response.status == ResponseStatus.SUCCESS) {


                            val tempList = _meetingItems.value
                            _meetingItems.value = tempList?.filter {
                                it.meetingId != item.meetingId
                            }

                            _eventState.value = Result.Success(
                                    message = response.message ?: Common.noRecordFoundMessage,
                                    data = Unit
                            )
                            _messageEvent.value = Event(response.message
                                    ?: "")
                        } else {
                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    private fun mapToPersonDetailUiModel(person: PersonDetailResponse?): PersonDetailUiModel {
        return PersonDetailUiModel(
                userId = person?.id ?: "",
                summitEventsFacultyId = person?.summitEventsFacultyId ?: "",
                name = person?.name ?: "",
                shortDescription = person?.shortDescription ?: "",
                longDescription = person?.longDescription ?: "",
                twitter = person?.twitterLink ?: "",
                linkedIn = person?.linkedInLink ?: "",
                facebook = person?.facebookLink ?: "",
                photoUrl = person?.userImage ?: "",
                designation = person?.designation ?: "",
                company = "",
                country = "",
//                        availableSlotData = it.meetingSlots,
                isSelected = false,
                isSpeaker = person?.isSpeaker == "1",
                isDelegate = person?.isDelegate == "1"
        )

    }

}