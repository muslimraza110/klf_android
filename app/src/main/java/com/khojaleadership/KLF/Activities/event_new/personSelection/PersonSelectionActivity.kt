package com.khojaleadership.KLF.Activities.event_new.personSelection

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Adapter.event_new.personSelection.PersonSelectionAdapter
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivityPersonSelectionBinding

class PersonSelectionActivity : AppCompatActivity() {

    private val viewModel: PersonSelectionViewModel by viewModels()

    private lateinit var binding: ActivityPersonSelectionBinding

    private lateinit var personSelectionAdapter: PersonSelectionAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // To show this activity as a dialog, also theme is applied in manifest in the activity Tag
        setTheme(R.style.AppTheme_Dialog)
        setFinishOnTouchOutside(false)

        super.onCreate(savedInstanceState)
        binding = ActivityPersonSelectionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {

        viewModel.personSuggestions.observe(this, Observer {
            it?.let {
                personSelectionAdapter.submitList(it)
                if(it.isNotEmpty()){
                    binding.tvNoRecordsFound.visibility = View.GONE
                    binding.rvList.visibility = View.VISIBLE
                }else{
                    binding.tvNoRecordsFound.visibility = View.VISIBLE
                    binding.rvList.visibility = View.GONE
                }

            }?: run {
                binding.tvNoRecordsFound.visibility = View.VISIBLE
                binding.rvList.visibility = View.GONE
            }
        })

    }

    private fun setUpViews() {
        Common.setSizeOfActivityAsDialog(
                windowManager,
                this,
                window
        )

        personSelectionAdapter = PersonSelectionAdapter(
                context = this,
                onPersonClick = {
                    viewModel.toggleSelectionForItem(it)
                }
        )
        binding.rvList.apply {
            adapter = personSelectionAdapter
            layoutManager = LinearLayoutManager(this@PersonSelectionActivity)
        }

        binding.editSearch.doOnTextChanged { text, start, count, after ->
            viewModel.searchText = text?.toString() ?: ""
            viewModel.showSuggestions(text?.toString() ?: "")
        }

        binding.tvOk.setOnClickListener {
            viewModel.authStore.selectDelegateOkClicked = true
            viewModel.authStore.delegatesToSelect = viewModel.allPersons
            finish()
        }

        binding.tvCancel.setOnClickListener {
            finish()
        }

    }
}