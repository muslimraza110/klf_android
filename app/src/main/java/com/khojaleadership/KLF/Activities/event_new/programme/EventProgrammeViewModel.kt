package com.khojaleadership.KLF.Activities.event_new.programme

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.ResponseStatus
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Helper.launchSafely
import com.khojaleadership.KLF.Model.event_new.PersonDetailResponse
import com.khojaleadership.KLF.Model.event_new.ProgrammeDayResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore

class EventProgrammeViewModel(application: Application) : AndroidViewModel(application) {


    private val _daysData: MutableLiveData<List<EventDaysUiModel>> = MutableLiveData()
    val daysData: LiveData<List<EventDaysUiModel>> = _daysData

    private val _programmeDetailsList: MutableLiveData<List<ProgramDetailsUiModel>> = MutableLiveData()

    private val _programmeDetails: MutableLiveData<ProgramDetailsUiModel> = MutableLiveData()
    val programmeDetails: LiveData<ProgramDetailsUiModel> = _programmeDetails


    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")


    init {
        fetchData()
    }


    private fun fetchData() {

        _eventState.value = Result.Loading

        Common.login_data?.data?.id?.let {userId->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getProgrammeDetails(
                                eventId = authStore.event?.eventId ?: "",
                                userId = userId
                                )
                        if(response.status == ResponseStatus.SUCCESS){

                            response.data?.let {

                                val tempProgrammeDetails = mutableListOf<ProgramDetailsUiModel>()

                                it.programmeDetails?.forEach {programDay->
                                    programDay?.let {
                                        tempProgrammeDetails.add(mapToProgrammeDetailsUiModel(it))
                                    }
                                }

                                val tempDays = tempProgrammeDetails.map {
                                    it.dayData
                                }
                                tempDays.firstOrNull()?.isSelected  = true

                                _daysData.value = tempDays
                                _programmeDetailsList.value = tempProgrammeDetails
                                _programmeDetails.value = _programmeDetailsList.value?.getOrNull(0)
                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message
                                )

                            } ?: run {
                                _daysData.value = listOf()
                                _eventState.value = Result.Success(
                                        message = response.message ?: Common.noRecordFoundMessage,
                                        data = Unit
                                )

                            }
                        }
                        else{
//                            _daysData.value = listOf()
                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                        }
                    },
                    error = {
//                        _daysData.value = listOf()
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
        }
    }

    private fun mapToProgrammeDetailsUiModel(responseData: ProgrammeDayResponse): ProgramDetailsUiModel {
        val eventDay =   EventDaysUiModel(
                date = responseData.date ?: "",
                isSelected = false,
                day = responseData.day ?: ""
        )

        val programmeItems = responseData.programmeItems?.map {

            it?.let {
                ProgrammeDetailsItemUiModel(
                        time = it.startEndTime ?: "",
                        description = it.description ?: "",
                        isExpanded = false,
                        delegates = mapToPersonDetailUiModel(it.delegates),
                        speakers = mapToPersonDetailUiModel(it.speakers),
                        title = it.title ?: "",
                        programDetailsId = it.id ?: ""
                )
            }

        }?.filterNotNull()

        return ProgramDetailsUiModel(
                dayData = eventDay,
                programmeDetailItems = programmeItems ?: listOf()
        )
    }

    private fun mapToPersonDetailUiModel(delegates: List<PersonDetailResponse?>?): List<PersonDetailUiModel> {
        return delegates?.map {
            it?.let {
                PersonDetailUiModel(
                        userId = it.id ?: "",
                        summitEventsFacultyId = it.summitEventsFacultyId ?: "",
                        name =  it.name ?: "",
                        shortDescription = it.shortDescription ?: "",
                        longDescription = it.longDescription ?: "",
                        twitter = it.twitterLink ?: "",
                        linkedIn = it.linkedInLink ?: "",
                        facebook = it.facebookLink ?: "",
                        photoUrl = it.userImage ?: "",
                        designation = it.designation ?: "",
                        company = "",
                        country =  "",
//                        availableSlotData = it.meetingSlots,
                        isSelected = false,
                        isSpeaker = it.isSpeaker == "1",
                        isDelegate = it.isDelegate == "1"
                )
            }

        }?.filterNotNull() ?: listOf()
    }

    fun changeSelectedDay(position: Int) {
        val tempList = _daysData.value
        tempList?.forEach {
            it.isSelected = false
        }
        tempList?.get(position)?.isSelected = true

        _daysData.value = tempList

        _programmeDetails.value = _programmeDetailsList.value?.get(position)

    }

}