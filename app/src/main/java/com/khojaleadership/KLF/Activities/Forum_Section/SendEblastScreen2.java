package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Adapter.Forum.SendEblast_Screen2_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Parent_Model;
import com.khojaleadership.KLF.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SendEblastScreen2 extends AppCompatActivity implements RecyclerViewClickListner {

    SendEblast_Screen2_Adapter adapter;
    RecyclerView recyclerView;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    RetrofitInterface string_retrofitInterface;
    Dialog loading_dialog;

    TextView header_title;


    ArrayList<Send_Eblast_Parent_Model> send_eblast_parent_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_eblast_screen2);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, SendEblastScreen2.this);
                finish();
            }
        });


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Recipients");

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        loading_dialog = Common.LoadingDilaog(this);


        try {

            //first clear email list
            if (Common.send_eblast_list != null) {
                Common.send_eblast_list = null;
            }

            loading_dialog.show();
            GetSendEblastString();
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public RetrofitInterface initRetrofit() {

        String Base_Url = Common.LIVE_BASE_URL;   //live url

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Base_Url)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        return retrofitInterface;
    }

    public void JsonToModel(String response) {

        //Object json = null;
        try {
            //response me complete string h json ki

            ArrayList<Send_Eblast_Parent_Model> parent_list = new ArrayList<>();

            JSONObject obj = new JSONObject(response);
            JSONObject data = obj.getJSONObject("data");
            String data_string = data.toString();
            Iterator<String> Keys = data.keys();
            while (Keys.hasNext()) {
                String key_name = Keys.next();

                ArrayList<Send_Eblast_Child_Model> child_list = new ArrayList<>();

                JSONObject data_obj = data.getJSONObject(key_name);
                Iterator<String> in_keys = data_obj.keys();
                while (in_keys.hasNext()) {
                    String in_key_name = in_keys.next();

                    JSONArray array = data_obj.getJSONArray(in_key_name);

                    ArrayList<Send_Eblast_Child_Data_Model> child_data_list = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject value = array.getJSONObject(i);

                        Iterator<String> array_keys = value.keys();

                        String UserID = "", FirstName = "", LastName = "", Email = "", ProfileURL = "";
                        while (array_keys.hasNext()) {
                            String array_key_name = array_keys.next();
                            String array_key_value = value.getString(array_key_name);
                            if (array_key_name.equals("UserID")) {
                                UserID = value.getString(array_key_name);
                            }
                            if (array_key_name.equals("Email")) {
                                Email = value.getString(array_key_name);
                            }
                            if (array_key_name.equals("FirstName")) {
                                FirstName = value.getString(array_key_name);
                            }
                            if (array_key_name.equals("LastName")) {
                                LastName = value.getString(array_key_name);
                            }

                            if (array_key_name.equals("ProfileURL")) {
                                ProfileURL = value.getString(array_key_name);
                            }
                        }

                        child_data_list.add(new Send_Eblast_Child_Data_Model("", UserID, FirstName, LastName, Email, ProfileURL, "0"));

                    }

                    child_list.add(new Send_Eblast_Child_Model(in_key_name, child_data_list));
                }

                parent_list.add(new Send_Eblast_Parent_Model(key_name, child_list));
            }

            if (send_eblast_parent_list != null && send_eblast_parent_list.size() > 0) {
                send_eblast_parent_list.clear();
            }

            send_eblast_parent_list = parent_list;
            loading_dialog.dismiss();
            intialize_view();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void GetSendEblastString() {

        string_retrofitInterface = initRetrofit();
        Call<String> call = string_retrofitInterface.GetSendEblastList("application/json", Common.auth_token);


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String json_response = response.body().toString();
                        JsonToModel(json_response);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void intialize_view() {


        adapter = new SendEblast_Screen2_Adapter(send_eblast_parent_list, this);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRowClick(int position) {

//        for (int i=0;i<send_eblast_parent_list.get(position).getChild_model().size();i++){
//         }

        ArrayList<Send_Eblast_Child_Model> send_eblast_child_list = new ArrayList<>();
        //Common.send_eblast_child_list = send_eblast_parent_list.get(position).getChild_model();

        send_eblast_child_list = send_eblast_parent_list.get(position).getChild_model();
        Intent screen3 = new Intent(SendEblastScreen2.this, SendEblastScreen3.class);
        screen3.putParcelableArrayListExtra("send_eblast_child_list", send_eblast_child_list);
        startActivity(screen3);

    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
