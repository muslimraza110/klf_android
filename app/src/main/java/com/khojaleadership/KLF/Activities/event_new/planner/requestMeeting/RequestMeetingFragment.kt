package com.khojaleadership.KLF.Activities.event_new.planner.requestMeeting

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Activities.event_new.delegates.DelegateHeaderItem
import com.khojaleadership.KLF.Activities.event_new.planner.EventPlannerViewModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Activities.event_new.userProfile.UserProfileActivity
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.EditTextBackEvent
import com.khojaleadership.KLF.Helper.EditTextImeBackListener
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.RequestMeetingFragmentBinding
import eu.davidea.flexibleadapter.FlexibleAdapter
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL

class RequestMeetingFragment : Fragment() {

    companion object {
        fun newInstance() = RequestMeetingFragment()
    }

    private val viewModel: RequestMeetingViewModel by viewModels()
    private val parentViewModel: EventPlannerViewModel by activityViewModels()

    private lateinit var delegatesAdapter: FlexibleAdapter<DelegateHeaderItem>


    lateinit var binding: RequestMeetingFragmentBinding

    private lateinit var timeSlotSpinnerAdapter: ArrayAdapter<EventTimeSlotUiModel>
    private lateinit var daysSpinnerAdapter: ArrayAdapter<EventDaysUiModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = RequestMeetingFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {

        viewModel.eventState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    delegatesAdapter.updateDataSet(listOf())
                    binding.progressBar.visibility = View.VISIBLE
                    binding.rvList.visibility = View.GONE
                    binding.tvNoRecordsFound.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.rvList.visibility = View.VISIBLE
                    binding.tvNoRecordsFound.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.rvList.visibility = View.VISIBLE
                    binding.tvNoRecordsFound.text = it.message
                    delegatesAdapter.updateDataSet(listOf())
                    Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.delegates.observe(viewLifecycleOwner, Observer {
            it?.let {
                delegatesAdapter.updateDataSet(it)
                if(it.isNotEmpty()){
                    binding.rvList.scrollToPosition(0)
                    binding.tvNoRecordsFound.visibility = View.GONE
                }else{
                    binding.tvNoRecordsFound.visibility = View.VISIBLE
                }

            }
        })

        parentViewModel.spinnerDaysData.observe(viewLifecycleOwner, Observer {
            it?.let {

                if(it.isEmpty()){
                    viewModel.resetUsers()
                }
                Timber.e("spinnerDate, ${it.size}")
                showHideSpinners(show = it.isNotEmpty())
                daysSpinnerAdapter.clear()
                daysSpinnerAdapter.addAll(it)
                binding.spinnerDate.adapter = daysSpinnerAdapter

            }
        })

        parentViewModel.enableSpinnersEvent.observe(viewLifecycleOwner, Observer {
            binding.spinnerDate.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_dropdown_icon)
            binding.spinnerTime.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_dropdown_icon)
            binding.spinnerDate.isEnabled = true
            binding.spinnerTime.isEnabled = true
        })

        parentViewModel.meetingSlotClickEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let { programDetailsId ->

                parentViewModel.spinnerDaysData.value?.let {
                    val dayIndex = it.indexOfFirst { eventUiModel ->
                        eventUiModel.day == parentViewModel.myItinerary.value?.dayData?.day
                    }

                    if (dayIndex != -1) {
                        binding.spinnerDate.adapter = daysSpinnerAdapter
                        binding.spinnerDate.setSelection(dayIndex)
                    }

                    binding.spinnerDate.background = null
                    binding.spinnerTime.background = null
                    binding.spinnerDate.isEnabled = false
                    binding.spinnerTime.isEnabled = false

                }
            }
        })

        parentViewModel.dayClickEvent.observe(viewLifecycleOwner, Observer {

            it?.consume()?.let { day ->
                parentViewModel.spinnerDaysData.value?.let {
                    val dayIndex = it.indexOfFirst { eventUiModel ->
//                    eventUiModel.dateId == parentViewModel.myItinerary.value?.dayData?.dateId
                        eventUiModel.day == day
                    }

                    Timber.e("Found Index $dayIndex  $day")
                    if (dayIndex != -1) {
                        binding.spinnerDate.setSelection(dayIndex)
                    }

                }
            }


        })

        viewModel.socialClickEvent.observe(viewLifecycleOwner, Observer {
            val url = it?.consume()
            loadUrl(url)
        })


//        viewModel.twitterClickEvent.observe(viewLifecycleOwner, Observer {
//            val url = it?.consume()
//            loadUrl(url)
//        })


        viewModel.delegateClickEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let { delegate ->
                if(!parentViewModel.userClicked){
                    parentViewModel.userClicked = true
                    val intent = Intent(requireContext(), UserProfileActivity::class.java)
                    intent.putExtra(UserProfileActivity.KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID, parentViewModel.selectedTimeSlot?.programDetailsId)
                    intent.putExtra(UserProfileActivity.KEY_FACULTY_ID, delegate.summitEventsFacultyId)
                    intent.putExtra(UserProfileActivity.KEY_USER_ID, delegate.userId)
                    intent.putExtra(UserProfileActivity.KEY_IS_DELEGATE, delegate.isDelegate)
                    intent.putExtra(UserProfileActivity.KEY_USER_FB, delegate.facebook)
                    intent.putExtra(UserProfileActivity.KEY_USER_TWITTER, delegate.twitter)
                    intent.putExtra(UserProfileActivity.KEY_USER_LINKED_IN, delegate.linkedIn)
                    startActivity(intent)
                }
           }
        })
    }

    fun loadUrl(url: String?) {

        try {
            URL(url)
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (urlMalformedExceptionL: MalformedURLException) {
            Timber.e("Invalid Url: $url")
            Toast.makeText(requireContext(),Common.invalidUrlMessage, Toast.LENGTH_SHORT).show()
        }
    }



    private fun showHideSpinners(show: Boolean) {
        if (show) {
            binding.spinnerDate.visibility = View.VISIBLE
            binding.spinnerTime.visibility = View.VISIBLE
        } else {
            binding.spinnerDate.visibility = View.GONE
            binding.spinnerTime.visibility = View.GONE
        }
    }

    private fun setUpViews() {

        delegatesAdapter = FlexibleAdapter(listOf())
        delegatesAdapter.setDisplayHeadersAtStartUp(true)
        delegatesAdapter.setStickyHeaders(true)


        binding.rvList.apply {
            adapter = delegatesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }


        timeSlotSpinnerAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner_simple)


        binding.spinnerTime.adapter = timeSlotSpinnerAdapter
        binding.spinnerTime.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Timber.e("OnItemSelected, $position")
                parentViewModel.selectedDaySlotIndex?.let { dayIndex ->
                    val timeSlotModel= parentViewModel.spinnerTimeData.value?.getOrNull(dayIndex)?.getOrNull(position)
                    if(parentViewModel.selectedTimeSlot?.programDetailsId != timeSlotModel?.programDetailsId){

                        parentViewModel.selectedTimeSlot = timeSlotModel
                        binding.editSearch.setText("")
                        parentViewModel.selectedTimeSlot?.let {
                            viewModel.getDelegates(it, "")
                        }
                    }
                }
            }
        }


        daysSpinnerAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner_simple_2)
        binding.spinnerDate.adapter = daysSpinnerAdapter

        binding.spinnerDate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Timber.e("$position")
                parentViewModel.selectedDaySlotIndex = position
                parentViewModel.spinnerTimeData.value?.let {
                    it.getOrNull(position)?.let { list ->
                        timeSlotSpinnerAdapter.clear()
                        timeSlotSpinnerAdapter.addAll(list)
                        if (list.isNotEmpty()) {
                            binding.spinnerTime.adapter = timeSlotSpinnerAdapter
                            parentViewModel.clickedRequestMeetingProgramDetailsId?.let { programDetailsId ->
                                val timeIndex = parentViewModel.spinnerTimeData.value?.get(position)?.indexOfFirst {
                                    it.programDetailsId == programDetailsId
                                }
                                parentViewModel.clickedRequestMeetingProgramDetailsId = null
                                timeIndex?.let {
                                    if (timeIndex != -1) {
                                        binding.spinnerTime.setSelection(timeIndex)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Timber.e("spinnerDate, Nothing")
            }
        }

        binding.editSearch.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                Common.hideKeyboard(v, requireContext())
                viewModel.searchTextHasChanged = true
                v?.clearFocus()
            }
            return@setOnKeyListener true
        }

        binding.editSearch.doOnTextChanged { text, start, count, after ->
            viewModel.searchTextHasChanged = true
        }

        binding.editSearch.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus && viewModel.searchTextHasChanged) {
                parentViewModel.selectedTimeSlot?.let {
                    viewModel.getDelegates(it,binding.editSearch.text?.toString() ?: "")
                }
                viewModel.searchTextHasChanged = false
            }
        }

        binding.editSearch.setOnEditTextImeBackListener(object : EditTextImeBackListener{
            override fun onImeBack(ctrl: EditTextBackEvent, text: String) {
                Common.hideKeyboard(binding.editSearch, requireContext())
                binding.editSearch.clearFocus()
            }
        })

        binding.imageSearch.setOnClickListener {
            Common.hideKeyboard(binding.editSearch, requireContext())
            viewModel.searchTextHasChanged = true
            binding.editSearch.clearFocus()
        }

        val event = parentViewModel.authStore.event
        event?.apply {
            binding.tvAddress.text = address.replace("\r\n", "").replace("\n", "")
        }


    }


}
