package com.khojaleadership.KLF.Activities.Setting;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Adapter.Setting.Edit_Profile_Family_RecyclerviewAdapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models.Edit_ProfileRequest_Model;
import com.khojaleadership.KLF.Model.Setting.Edit_Proflie_Models.Edit_ProfileResponse_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Comments_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Family_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_profile_User_Info_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Profile_Activity extends AppCompatActivity {


    RecyclerView recyclerView;
    Edit_Profile_Family_RecyclerviewAdapter adapter;


    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView first_name, middle_name, last_name, primary_email, secondry_email, website, facebook, linkdin, bussiness_title, description;
    TextView address, city, postal_code, province, country, primary_phone, secondry_phone;

    RichEditor bio, about_you;

    EditText comment;

    String title_string = "", gender_string = "";

    String id_array = "";
    String relation_array = "";

    TextView edit_profile_header_name;
    CircleImageView img_profile;

    ImageButton a_action_align_left, a_action_align_center, a_action_align_right;
    Boolean a_action_left_flag = false, a_action_right_flag = false, a_action_centr_flag = false;

    ImageButton b_action_align_left, b_action_align_center, b_action_align_right;
    Boolean b_action_left_flag = false, b_action_right_flag = false, b_action_centr_flag = false;

    ArrayList<View_profile_User_Info_Model> user_profile_information_list = new ArrayList<>();
    ArrayList<View_Profile_Family_Model> user_prfile_family_list = new ArrayList<>();

    ArrayList<View_Profile_Comments_Model> user_profile_comment_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__profile_);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        loading_dialog = Common.LoadingDilaog(this);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Edit Profile");

        loading_dialog.show();
        getProfileData();

        //save btn
        findViewById(R.id.p_save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //loading_dialog.show();
                EditProfile();
            }
        });


        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onBackPressed();
                Common.hideKeyboard(v, Edit_Profile_Activity.this);
                finish();
            }
        });

    }


    public void Title_Spinner() {
        ArrayList<String> title = new ArrayList<>();
        title.add("Mr");
        title.add("Dr");
        title.add("Mrs.");
        title.add("Ms");

        Spinner title_spinner = (Spinner) findViewById(R.id.title_spinner);
        // ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, title);


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, title) {
            @Override
            public boolean isEnabled(int position) {
                return true;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);

        title_spinner.setAdapter(adapter);
        title_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                title_string = parent.getItemAtPosition(position).toString();
                ((TextView) view).setTextColor(getResources().getColor(R.color.pure_grey));
                ((TextView) view).setTextSize(12);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //setting data
        if (user_profile_information_list != null) {
            if (user_profile_information_list.size() > 0) {
                View_profile_User_Info_Model userinfo1 = user_profile_information_list.get(0);
                if (userinfo1.getUsers__title().equals("Mr")) {
                    title_spinner.setSelection(0);
                } else if (userinfo1.getUsers__title().equals("Dr")) {
                    title_spinner.setSelection(1);
                } else if (userinfo1.getUsers__title().equals("Mrs.")) {
                    title_spinner.setSelection(2);
                } else if (userinfo1.getUsers__title().equals("Ms")) {
                    title_spinner.setSelection(3);
                }
            }
        }

    }

    public void Gender_Spinner() {
        ArrayList<String> gender = new ArrayList<>();

        gender.add("Male");
        gender.add("Female");

        Spinner gender_spinner = (Spinner) findViewById(R.id.gender_spinner);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, gender) {
            @Override
            public boolean isEnabled(int position) {
                return true;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTextColor(Color.BLACK);

                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_item);

        gender_spinner.setAdapter(adapter);
        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender_string = parent.getItemAtPosition(position).toString().substring(0, 1);
                ((TextView) view).setTextColor(getResources().getColor(R.color.pure_grey));
                ((TextView) view).setTextSize(12);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (user_profile_information_list != null) {
            if (user_profile_information_list.size() > 0) {
                View_profile_User_Info_Model userinfo1 = user_profile_information_list.get(0);
                if (userinfo1.getUsers__gender().equals("M")) {
                    gender_spinner.setSelection(0);
                } else if (userinfo1.getUsers__gender().equals("F")) {
                    gender_spinner.setSelection(1);
                }

            }
        }
    }

    private void getProfileData() {

        Call<View_Profile_Model> responseCall = retrofitInterface.GetProfileData("application/json", Common.auth_token, Common.login_data.getData().getId());


        responseCall.enqueue(new Callback<View_Profile_Model>() {
            @Override
            public void onResponse(Call<View_Profile_Model> call, Response<View_Profile_Model> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {

                    String status, message;

                    View_Profile_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    //saving data
                    user_profile_information_list = responseModel.getUserinfo1();

                    user_prfile_family_list = responseModel.getFamilies();
                    //Common.user_profile_businessandprojects = responseModel.getBusinessandprojects();
                    //Common.user_profile_charitiesandprojects = responseModel.getCharitiesandprojects();
                    //Common.user_profile_groups = responseModel.getGroups();
                   // Common.user_profile_groupsproject = responseModel.getGroupsproject();
                    user_profile_comment_list = responseModel.getUsercomments();

                    intialize_view();


                } else {
                }


            }

            @Override
            public void onFailure(Call<View_Profile_Model> call, Throwable t) {
                loading_dialog.dismiss();
                intialize_view();
            }
        });
    }

    public void intialize_richeditor_bio() {
        bio = (RichEditor) findViewById(R.id.p_bio_editor);
        bio.setEditorHeight(250);
        bio.setEditorFontSize(22);
        bio.setScrollbarFadingEnabled(false);
        bio.setScrollBarStyle(bio.SCROLLBARS_OUTSIDE_OVERLAY);
        bio.setScrollBarSize(8);
        bio.setOverScrollMode(bio.OVER_SCROLL_IF_CONTENT_SCROLLS);
        bio.canScrollVertically(0);
        bio.setVerticalScrollBarEnabled(true);
        bio.setMotionEventSplittingEnabled(true);

        bio.setPadding(10, 10, 10, 10);
        bio.setPlaceholder("Bio");
        bio.setFontSize(12);
        bio.setEditorFontSize(12);


        bio.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                b_action_align_left = findViewById(R.id.action_align_left);
                b_action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (b_action_left_flag == false) {
                            b_action_left_flag = true;
                            b_action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            b_action_centr_flag = false;
                            b_action_right_flag = false;
                            b_action_align_center.setBackgroundResource(R.drawable.ic_center);
                            b_action_align_right.setBackgroundResource(R.drawable.ic_right);

                            bio.setAlignLeft();
                        } else if (b_action_left_flag == true) {
                            b_action_left_flag = false;
                            b_action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                b_action_align_center = findViewById(R.id.action_align_center);
                b_action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (b_action_centr_flag == false) {
                            b_action_centr_flag = true;
                            b_action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            b_action_left_flag = false;
                            b_action_right_flag = false;
                            b_action_align_left.setBackgroundResource(R.drawable.ic_left);
                            b_action_align_right.setBackgroundResource(R.drawable.ic_right);

                            bio.setAlignCenter();
                        } else if (b_action_centr_flag == true) {
                            b_action_centr_flag = false;
                            b_action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                b_action_align_right = findViewById(R.id.action_align_right);
                b_action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (b_action_right_flag == false) {
                            b_action_right_flag = true;
                            b_action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            b_action_centr_flag = false;
                            b_action_left_flag = false;
                            b_action_align_center.setBackgroundResource(R.drawable.ic_center);
                            b_action_align_left.setBackgroundResource(R.drawable.ic_left);

                            bio.setAlignRight();
                        } else if (b_action_right_flag == true) {
                            b_action_right_flag = false;
                            b_action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });


    }

    public void intialize_richeditor_about_you() {
        about_you = (RichEditor) findViewById(R.id.p_about_you_editor);
        about_you.setEditorHeight(250);
        about_you.setEditorFontSize(22);
        about_you.setScrollbarFadingEnabled(false);
        about_you.setScrollBarStyle(bio.SCROLLBARS_OUTSIDE_OVERLAY);
        about_you.setScrollBarSize(8);
        about_you.setOverScrollMode(bio.OVER_SCROLL_IF_CONTENT_SCROLLS);
        about_you.canScrollVertically(0);
        about_you.setVerticalScrollBarEnabled(true);
        about_you.setMotionEventSplittingEnabled(true);

        about_you.setPadding(10, 10, 10, 10);
        about_you.setPlaceholder("About you");
        about_you.setFontSize(12);
        about_you.setEditorFontSize(12);


        about_you.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                a_action_align_left = findViewById(R.id.a_action_align_left);
                a_action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (a_action_left_flag == false) {
                            a_action_left_flag = true;
                            a_action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            a_action_centr_flag = false;
                            a_action_right_flag = false;
                            a_action_align_center.setBackgroundResource(R.drawable.ic_center);
                            a_action_align_right.setBackgroundResource(R.drawable.ic_right);

                            about_you.setAlignLeft();
                        } else if (a_action_left_flag == true) {
                            a_action_left_flag = false;
                            a_action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                a_action_align_center = findViewById(R.id.a_action_align_center);
                a_action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (a_action_centr_flag == false) {
                            a_action_centr_flag = true;
                            a_action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            a_action_left_flag = false;
                            a_action_right_flag = false;
                            a_action_align_left.setBackgroundResource(R.drawable.ic_left);
                            a_action_align_right.setBackgroundResource(R.drawable.ic_right);

                            about_you.setAlignCenter();
                        } else if (a_action_centr_flag == true) {
                            a_action_centr_flag = false;
                            a_action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                a_action_align_right = findViewById(R.id.a_action_align_right);
                a_action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (a_action_right_flag == false) {
                            a_action_right_flag = true;
                            a_action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            a_action_centr_flag = false;
                            a_action_left_flag = false;
                            a_action_align_center.setBackgroundResource(R.drawable.ic_center);
                            a_action_align_left.setBackgroundResource(R.drawable.ic_left);

                            about_you.setAlignRight();
                        } else if (a_action_right_flag == true) {
                            a_action_right_flag = false;
                            a_action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });

    }

    public void intialize_view() {
        Title_Spinner();
        Gender_Spinner();

        intialize_richeditor_bio();
        intialize_richeditor_about_you();


        edit_profile_header_name = findViewById(R.id.edit_profile_header_name);
        img_profile = findViewById(R.id.img_profile);

        first_name = findViewById(R.id.p_first_name);
        middle_name = findViewById(R.id.p_middle_name);
        last_name = findViewById(R.id.p_last_name);
        primary_email = findViewById(R.id.p_primary_email);
        secondry_email = findViewById(R.id.p_secondry_email);

        website = findViewById(R.id.p_website);
        facebook = findViewById(R.id.p_facebook);
        linkdin = findViewById(R.id.p_linkdin);
        bussiness_title = findViewById(R.id.p_bussiness_title);
        description = findViewById(R.id.p_description);

        address = findViewById(R.id.p_address);
        city = findViewById(R.id.p_city);
        postal_code = findViewById(R.id.p_postal_code);
        province = findViewById(R.id.p_province);
        country = findViewById(R.id.p_country);

        primary_phone = findViewById(R.id.p_primary_phone);
        secondry_phone = findViewById(R.id.p_secondary_phone);

        comment = findViewById(R.id.p_comment);

        //setting data
        if (user_profile_information_list != null) {
            if (user_profile_information_list.size() > 0) {
                View_profile_User_Info_Model userinfo1 = user_profile_information_list.get(0);

                edit_profile_header_name.setText(userinfo1.getUsers__first_name() + " " + userinfo1.getUsers__last_name());
                Glide.with(getApplicationContext()).load(userinfo1.getProfileImg__name()).into(img_profile);
                first_name.setText(userinfo1.getUsers__first_name());
                middle_name.setText(userinfo1.getUsers__middle_name());
                last_name.setText(userinfo1.getUsers__last_name());

                primary_email.setText(userinfo1.getUsers__email());
                secondry_email.setText(userinfo1.getUserDetails__email2());

                website.setText(userinfo1.getUserDetails__website());
                facebook.setText(userinfo1.getUserDetails__facebook());
                linkdin.setText(userinfo1.getUserDetails__linkedin());

                bussiness_title.setText(userinfo1.getUserDetails__title());
                description.setText(userinfo1.getUserDetails__description());

                address.setText(userinfo1.getUserDetails__address());
                city.setText(userinfo1.getUserDetails__city());
                postal_code.setText(userinfo1.getUserDetails__postal_code());
                province.setText(userinfo1.getUserDetails__region());
                country.setText(userinfo1.getUserDetails__country());

                primary_phone.setText(userinfo1.getUserDetails__phone());
                secondry_phone.setText(userinfo1.getUserDetails__phone2());

                //comment.setText(userinfo1.get);
                bio.setHtml(userinfo1.getUserDetails__private_bio());
                about_you.setHtml(userinfo1.getUserDetails__bio());
            }
        }

        if (user_profile_comment_list != null) {
            if (user_profile_comment_list.size() > 0) {
                comment.setText(user_profile_comment_list.get(0).getUserComments__comment());
            }
        }

        if (user_prfile_family_list != null) {

            recyclerView = findViewById(R.id.p_family_recyclerview);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));


            for (int i = 0; i < user_prfile_family_list.size(); i++) {

                String ids = "," + user_prfile_family_list.get(i).getRelatedUsers__id();
                String ids_first = user_prfile_family_list.get(i).getRelatedUsers__id();

                String relation = "," + user_prfile_family_list.get(i).getRelatedUsers__id();
                String rerlation_first =user_prfile_family_list.get(i).getRelatedUsers__id();

                if (i == 0) {
                    id_array = ids_first;
                    relation_array = rerlation_first;
                } else {
                    id_array = id_array + ids;
                    relation_array = relation_array + relation;
                }
            }
            adapter = new Edit_Profile_Family_RecyclerviewAdapter(this, user_prfile_family_list);
            recyclerView.setAdapter(adapter);
        } else {

        }


    }


    private void EditProfile() {
    try {
            Edit_ProfileRequest_Model request_model = new Edit_ProfileRequest_Model();

            if (Common.login_data.getData().getId() != null) {
                request_model.setUser_id(Common.login_data.getData().getId());
            }

            if (about_you.getHtml() == null || about_you.getHtml().equalsIgnoreCase("") ||
                    about_you.getHtml().isEmpty()) {
                request_model.setBio("");
            } else {
                request_model.setBio(about_you.getHtml().toString());
            }

            if (bio.getHtml().toString() == null || bio.getHtml().toString().equalsIgnoreCase("") ||
                    bio.getHtml().toString().isEmpty()) {
                request_model.setPrivate_bio("");
            } else {
                request_model.setPrivate_bio(bio.getHtml().toString());
            }

            request_model.setAddress2("Testing Address 2");

            if (comment.getText().toString() == null || comment.getText().toString().equalsIgnoreCase("") ||
                    comment.getText().toString().isEmpty()) {
                request_model.setComment("");
            } else {
                request_model.setComment(comment.getText().toString());
            }


            request_model.setStatus("A");

            request_model.setRelated_user_id(id_array);
            request_model.setRelation(relation_array);
            request_model.setCopywriter_user_id("1");

            request_model.setTitle(title_string);
            request_model.setGender(gender_string);

            if (first_name.getText().toString() == null || first_name.getText().toString().equalsIgnoreCase("") ||
                    first_name.getText().toString().isEmpty()) {
                request_model.setFirst_name("");
            } else {
                request_model.setFirst_name(first_name.getText().toString());
            }


            if (middle_name.getText().toString() == null || middle_name.getText().toString().equalsIgnoreCase("") ||
                    middle_name.getText().toString().isEmpty()) {
                request_model.setMiddle_name("");
            } else {
                request_model.setMiddle_name(middle_name.getText().toString());
            }


            if (last_name.getText().toString() == null || last_name.getText().toString().equalsIgnoreCase("") ||
                    last_name.getText().toString().isEmpty()) {
                request_model.setLast_name("");
            } else {
                request_model.setLast_name(last_name.getText().toString());
            }

            if (primary_email.getText().toString() == null || primary_email.getText().toString().equalsIgnoreCase("") ||
                    primary_email.getText().toString().isEmpty()) {
                request_model.setEmail("");
                Common.Toast_Message(Edit_Profile_Activity.this, "Email should be empty.");
            } else {
                request_model.setEmail(primary_email.getText().toString());
            }

            if (secondry_email.getText().toString() == null || secondry_email.getText().toString().equalsIgnoreCase("") ||
                    secondry_email.getText().toString().isEmpty()) {
                request_model.setEmail2("");
            } else {
                request_model.setEmail2(secondry_email.getText().toString());
            }


            if (website.getText().toString() == null || website.getText().toString().equalsIgnoreCase("") ||
                    website.getText().toString().isEmpty()) {
                request_model.setWebsite("");
            } else {
                request_model.setWebsite(website.getText().toString());
            }
            if (facebook.getText().toString() == null || facebook.getText().toString().equalsIgnoreCase("") ||
                    facebook.getText().toString().isEmpty()) {
                request_model.setFacebook("");
            } else {
                request_model.setFacebook(facebook.getText().toString());
            }

            if (linkdin.getText().toString() == null || linkdin.getText().toString().equalsIgnoreCase("") ||
                    linkdin.getText().toString().isEmpty()) {
                request_model.setLinkedin("");
            } else {
                request_model.setLinkedin(linkdin.getText().toString());
            }


            if (bussiness_title.getText().toString() == null || bussiness_title.getText().toString().equalsIgnoreCase("") ||
                    bussiness_title.getText().toString().isEmpty()) {
                request_model.setBusiness_title("");
            } else {
                request_model.setBusiness_title(bussiness_title.getText().toString());
            }


            if (address.getText().toString() == null || address.getText().toString().equalsIgnoreCase("") ||
                    address.getText().toString().isEmpty()) {
                request_model.setAddress("");
            } else {
                request_model.setAddress(address.getText().toString());
            }


            if (city.getText().toString() == null || city.getText().toString().equalsIgnoreCase("") ||
                    city.getText().toString().toString().isEmpty()) {
                request_model.setCity("");
            } else {
                request_model.setCity(city.getText().toString());
            }


            if (postal_code.getText().toString() == null || postal_code.getText().toString().equalsIgnoreCase("") ||
                    postal_code.getText().toString().isEmpty()) {
                request_model.setPostal_code("");
            } else {
                request_model.setPostal_code(postal_code.getText().toString());
            }


            if (province.getText().toString() == null || province.getText().toString().equalsIgnoreCase("") ||
                    province.getText().toString().isEmpty()) {
                request_model.setRegion("");
            } else {
                request_model.setRegion(province.getText().toString());
            }


            if (country.getText().toString() == null || country.getText().toString().equalsIgnoreCase("") ||
                    country.getText().toString().isEmpty()) {
                request_model.setCountry("");
            } else {
                request_model.setCountry(country.getText().toString());
            }


            if (primary_phone.getText().toString() == null || primary_phone.getText().toString().equalsIgnoreCase("") ||
                    primary_phone.getText().toString().isEmpty()) {
                request_model.setPhone("");
            } else {
                request_model.setPhone(primary_phone.getText().toString());
            }


            if (secondry_phone.getText().toString() == null || secondry_phone.getText().toString().equalsIgnoreCase("") ||
                    secondry_phone.getText().toString().isEmpty()) {
                request_model.setPhone2("");
            } else {
                request_model.setPhone2(secondry_phone.getText().toString());
            }


            Call<Edit_ProfileResponse_Model> responseCall = retrofitInterface.EditProfile("application/json", Common.auth_token, request_model);

            responseCall.enqueue(new Callback<Edit_ProfileResponse_Model>() {
                @Override
                public void onResponse(Call<Edit_ProfileResponse_Model> call, Response<Edit_ProfileResponse_Model> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {

                        String status, message;

                        Edit_ProfileResponse_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                            Toast.makeText(Edit_Profile_Activity.this, "Profile edited successfully", Toast.LENGTH_SHORT).show();
                            closeKeyboard();
                            finish();
                        } else {
                         }

                    } else {
                     }
                }

                @Override
                public void onFailure(Call<Edit_ProfileResponse_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                 }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
