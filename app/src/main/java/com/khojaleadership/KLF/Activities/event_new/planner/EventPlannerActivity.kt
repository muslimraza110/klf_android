package com.khojaleadership.KLF.Activities.event_new.planner

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.notification.PlannerNotificationsActivity
import com.khojaleadership.KLF.Adapter.event_new.planner.PlannerViewPagerAdapter
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import com.khojaleadership.KLF.databinding.ActivityEventPlannerBinding
import timber.log.Timber


class EventPlannerActivity : AppCompatActivity() {

    private val viewModel: EventPlannerViewModel by viewModels()

    private lateinit var pagerAdapter: PlannerViewPagerAdapter
    private lateinit var binding: ActivityEventPlannerBinding

    var imageViewAnimator: ObjectAnimator? = null

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityEventPlannerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.e("----------EventPlannerActivity")
        authStore.meetingRequestSent = false
        authStore.selectDelegateOkClicked = false

        if (intent?.hasExtra(PlannerNotificationsActivity.KEY_EVENT_DAY_INDEX) == true) {
            viewModel.lastSelectedDayIndex = intent?.getIntExtra(PlannerNotificationsActivity.KEY_EVENT_DAY_INDEX, 0)
                    ?: 0
        }
//        if(intent?.hasExtra(PlannerNotificationsActivity.KEY_EVENT_SLOT_ID) == true){
//            viewModel.scrollToPositionProgramDetailsId = intent?.getStringExtra(PlannerNotificationsActivity.KEY_EVENT_SLOT_ID) ?: ""
//            viewModel.scrollToSpecificPosition = true
//        }

        setUpViews()
        setUpObservers()
    }

    override fun onResume() {
        super.onResume()
        viewModel.userClicked = false
        if (authStore.meetingRequestSent) {
            viewModel.meetingRequestSent()
            authStore.meetingRequestSent = false
        }
        if (authStore.selectDelegateOkClicked) {
            viewModel.updateAllPersonComingInEvent()
            authStore.selectDelegateOkClicked = false
        }
    }

    private fun setUpObservers() {
        viewModel.meetingSlotClickEvent.observe(this, Observer {
            binding.viewpager.setCurrentItem(1, true)
        })

        viewModel.meetingRequestSentEvent.observe(this, Observer {
            binding.viewpager.setCurrentItem(0, true)
            viewModel.getEventProgramDetails()
            viewModel.enableSpinners()
        })

        viewModel.viewPagerVisibilityEvent.observe(this, Observer {
            it.consume()?.let { visible ->
                if (visible) {
                    binding.viewpager.visibility = View.VISIBLE
                    binding.tvError.visibility = View.GONE
                } else {
                    binding.viewpager.visibility = View.INVISIBLE
                    binding.tvError.visibility = View.VISIBLE
                }
            }
        })

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
//                    binding.viewpager.visibility = View.GONE

                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.viewpager.visibility = View.VISIBLE
                    stopRotation()

                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.tvError.text = it.message
//                    binding.viewpager.visibility = View.GONE

                    stopRotation()
                }
            }
        })


        viewModel.showItineraryFragmentEvent.observe(this, Observer {
            it?.consume()?.let {
                binding.viewpager.setCurrentItem(0, true)
            }
        })
    }

    private fun setUpViews() {
        binding.viewpager.visibility = View.INVISIBLE
        pagerAdapter = PlannerViewPagerAdapter(supportFragmentManager, lifecycle)

        changeSelectedTab(0)

        binding.viewpager.apply {
            adapter = pagerAdapter
            offscreenPageLimit = 1
            registerOnPageChangeCallback(object : OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    changeSelectedTab(position)
                }
            })
            isUserInputEnabled = false
        }

        binding.buttonMyItinerary.setOnClickListener {
            binding.viewpager.setCurrentItem(0, true)
            viewModel.enableSpinners()

        }

        binding.buttonRequestMeeting.setOnClickListener {
            binding.viewpager.setCurrentItem(1, true)
        }

        binding.imageBack.setOnClickListener {

            if (binding.viewpager.currentItem == 0) {
                if (viewModel.isCheckAvailabilitySelected) {
                    viewModel.toggleCheckAvailability()
                } else {
                    stopRotation()
                    finish()
                }
            } else {
                binding.viewpager.setCurrentItem(0, true)
                viewModel.enableSpinners()
            }
        }

        binding.imageRefresh.setOnClickListener {
            startRotation(binding.imageRefresh)
            viewModel.getEventProgramDetails()
        }

    }

    override fun onBackPressed() {
        stopRotation()
        super.onBackPressed()
    }

    private fun startRotation(imageview: ImageView) {

        val imageViewObjectAnimator = ObjectAnimator.ofFloat(imageview,
                "rotation", 0f, 360f)
        imageViewObjectAnimator.repeatCount = ObjectAnimator.INFINITE
        imageViewObjectAnimator.repeatMode = ObjectAnimator.RESTART
        imageViewObjectAnimator.duration = 1000
        imageViewObjectAnimator.interpolator = LinearInterpolator()
        imageViewObjectAnimator.start()
        imageViewAnimator = imageViewObjectAnimator
    }

    private fun stopRotation() {
        binding.imageRefresh.rotation = 0f
        imageViewAnimator?.cancel()
        imageViewAnimator = null
    }

    private fun changeSelectedTab(position: Int) {
        if (position == 0) {
            binding.buttonMyItinerary.background = ContextCompat.getDrawable(this, R.drawable.linear_gradient_blue_cyan_round)
            binding.buttonMyItinerary.setTextColor(ContextCompat.getColor(this, R.color.appWhite))

            binding.buttonRequestMeeting.background = ContextCompat.getDrawable(this, R.drawable.grey_outlined_bg)
            binding.buttonRequestMeeting.setTextColor(ContextCompat.getColor(this, R.color.appGrey))
        } else {

            binding.buttonRequestMeeting.background = ContextCompat.getDrawable(this, R.drawable.linear_gradient_blue_cyan_round)
            binding.buttonRequestMeeting.setTextColor(ContextCompat.getColor(this, R.color.appWhite))

            binding.buttonMyItinerary.background = ContextCompat.getDrawable(this, R.drawable.grey_outlined_bg)
            binding.buttonMyItinerary.setTextColor(ContextCompat.getColor(this, R.color.appGrey))
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}
