package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.Forum.Topic_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.DeleteTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.DeleteTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Topic extends AppCompatActivity implements RecyclerViewClickListner {
    RecyclerView recyclerView;
    Topic_Adapter adminForumRecyclerView_adapter;

    ArrayList<GetSubTopicDataModel> list;

    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog loading_dialog;
    LinearLayout add_topic_btn;
    TextView main_topic_title;
    RecyclerViewClickListner listner;
    TextView header_title;

    String Category;

    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    ArrayList<GetTopicDataModel> topic_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        listner = this;


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
        }

        //intialize view
        intialize_view();

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        onMainTopicBtnClick(Category, false);

    }

    public void intialize_view() {
//        try {

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Topic.this);
                finish();
            }
        });

        header_title = (TextView) findViewById(R.id.c_header_title);
        header_title.setText("Forum Topic");

        main_topic_title = (TextView) findViewById(R.id.main_topic_title);
        main_topic_title.setText(Category + " Forum");


        add_topic_btn = findViewById(R.id.c_add_btn);
        //.......................................................................
        //for user and also admin topic section....add functionality is gone
        if (Common.is_Admin_flag == false) {
            if (Category.equals("Admin")) {
                add_topic_btn.setVisibility(View.GONE);
            } else {
                add_topic_btn.setVisibility(View.VISIBLE);
            }
        }

        //..................................END..................................

        add_topic_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Common.addPost=false;
                Intent i = new Intent(Topic.this, Add_Topic.class);
                i.putExtra("Category", Category);
                startActivity(i);
                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.admin_forum_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adminForumRecyclerView_adapter = new Topic_Adapter(this, this, topic_data);
        recyclerView.setAdapter(adminForumRecyclerView_adapter);


//            if (topic_data == null) {
//                //do nothing
//                findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
//            } else {
//                if (topic_data.size() == 0) {
//                    findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
//                }else {
//
//                    adminForumRecyclerView_adapter = new Topic_Adapter(this, this, topic_data);
//                    recyclerView.setAdapter(adminForumRecyclerView_adapter);
//
//                }
//            }


//            throw new RuntimeException("Run Time exception");
//        }catch (Exception e){
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {


        try {

            switch (v.getId()) {

                case R.id.delete_topic_btn:
                    //loading_dialog.show();

                    if (topic_data != null) {
                        if (topic_data.size() > 0) {
                            //Common.topic_data_detail = topic_data.get(position);
                            topic_data_detail = topic_data.get(position);
                            Confirm_Dialog_Delete("Delete Forum Topic", "Are you sure to delete this forum topic?", "Yes");
                        } else {
                            Common.Toast_Message(this, "You can not delete this.Due to data is empty");
                        }
                    }


                    break;
                case R.id.linearLayout:
                    if (topic_data != null) {
                        if (topic_data.size() > 0) {
//                            Common.topic_data_detail = topic_data.get(position);
                            topic_data_detail = topic_data.get(position);
                        }
                    }

                    Intent sub_topic_intent = new Intent(Topic.this, Sub_Topics.class);
                    sub_topic_intent.putExtra("TopicDataDetail",topic_data_detail);
                    sub_topic_intent.putExtra("Category", Category);
                    startActivity(sub_topic_intent);
                    break;
                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void Confirm_Dialog_Delete(String title, String message, String btn) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(Topic.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn = (RelativeLayout) dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                onDeleteTopicBtnClick();

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void onDeleteTopicBtnClick() {

        try {
            DeleteTopicRequestModel requestModel = new DeleteTopicRequestModel();
            requestModel.setSubforum_id(topic_data_detail.getSubforums__id());

            Call<DeleteTopicResponseModel> responseCall = retrofitInterface.DeleteTopic("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<DeleteTopicResponseModel>() {
                @Override
                public void onResponse(Call<DeleteTopicResponseModel> call, Response<DeleteTopicResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        DeleteTopicResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        onMainTopicBtnClick(Category, true);

                    } else {
                    }

                }

                @Override
                public void onFailure(Call<DeleteTopicResponseModel> call, Throwable t) {
                    Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        // custom dialog
        final Dialog dialog = new Dialog(Topic.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);

        RelativeLayout confirm_custom_close_btn = dialog.findViewById(R.id.confirm_custom_close_btn);
        confirm_custom_close_btn.setVisibility(View.GONE);

        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    private void onMainTopicBtnClick(String admin, final Boolean isDelete) {

        try {


            Call<GetTopicModel> call = retrofitInterface.getTopic("application/json", Common.auth_token,
                    admin, 1, 1000);
            call.enqueue(new Callback<GetTopicModel>() {
                @Override
                public void onResponse(Call<GetTopicModel> call, Response<GetTopicModel> response) {
                    ArrayList<GetTopicDataModel> list = new ArrayList<>();
                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;
                        GetTopicModel responseModel = response.body();

                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        totalcount = responseModel.getTotalcount();
                        totalpages = responseModel.getTotalpages();

                        list = response.body().getData();

                        if (list != null) {
                            loading_dialog.dismiss();

                            topic_data = list;

                            Log.d("TopicList", "Topic list :" + topic_data.size());
                            if (isDelete == true) {
                                Common.Toast_Message(Topic.this, "Topic deleted successfully");
                            }

                            if (topic_data == null) {
                                //do nothing
                                recyclerView.setVisibility(View.GONE);
                                findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
                            } else {
                                if (topic_data.size() == 0) {
                                    recyclerView.setVisibility(View.GONE);
                                    findViewById(R.id.no_record_found).setVisibility(View.VISIBLE);
                                } else {
                                    adminForumRecyclerView_adapter.updateList(topic_data);

                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetTopicModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

}
