package com.khojaleadership.KLF.Activities.event_new.notification

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.khojaleadership.KLF.Activities.event_new.planner.EventPlannerActivity
import com.khojaleadership.KLF.Adapter.event_new.notification.PlannerNotificationAdapter
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivityPlannerNotificationsBinding


/**
 * This Activity is presented as a dialog in the app.
 */

class PlannerNotificationsActivity : AppCompatActivity() {

    private val viewModel: PlannerNotificationsViewModel by viewModels()

    private lateinit var notificationsAdapter: PlannerNotificationAdapter

    private lateinit var binding: ActivityPlannerNotificationsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // To show this activity as a dialog, also theme is applied in manifest in the activity Tag
        setTheme(R.style.AppTheme_Dialog)
        setFinishOnTouchOutside(false)

        super.onCreate(savedInstanceState)
        binding = ActivityPlannerNotificationsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.rvList.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvList.visibility = View.VISIBLE
                    binding.tvNoRecordsFound.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvList.visibility = View.VISIBLE
                    binding.tvNoRecordsFound.text = it.message
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.notifications.observe(this, Observer {
            it?.let {
                if (it.isEmpty()) {
                    binding.tvNoRecordsFound.visibility = View.VISIBLE
                } else {
                    binding.tvNoRecordsFound.visibility = View.GONE
                }
                notificationsAdapter.submitList(it)
            }
        })
    }


    private fun setUpViews() {

        Common.setSizeOfActivityAsDialog(
                windowManager,
                this,
                window
        )


        notificationsAdapter = PlannerNotificationAdapter(
                context = this,
                onNotificationClick = {
                    val intent = Intent(this, EventPlannerActivity::class.java)
                    intent.putExtra(KEY_EVENT_DAY_INDEX, it.eventDay)
//                    intent.putExtra(KEY_EVENT_SLOT_ID, it.eventProgramDetailsId)
                    startActivity(intent)
                    finish()
                }
        )

        binding.rvList.apply {
            adapter = notificationsAdapter
            layoutManager = LinearLayoutManager(this@PlannerNotificationsActivity)
        }

        binding.imageBack.setOnClickListener {
            finish()
        }

    }

    companion object{
        const val KEY_EVENT_DAY_INDEX = "eventDayIndexKey"
        const val KEY_EVENT_PROGRAM_DETAILS_ID = "eventProgramDetailsIdKey"
    }

}
