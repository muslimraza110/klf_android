package com.khojaleadership.KLF.Activities.event_new.planner.sessions

import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import java.util.*

data class SessionItemUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val sessionId: String,
        val programDetailsId: String,
        val title: String,
        val description: String,
        val roomLimit: Int,
        val zoomMeetingUrl: String,
        var roomOccupied: Int,
        var iAmEnrolled: Boolean,
        val delegatesEnrolled: List<PersonDetailUiModel>,
        val speakers: List<PersonDetailUiModel>,
        val delegates: List<PersonDetailUiModel>
)