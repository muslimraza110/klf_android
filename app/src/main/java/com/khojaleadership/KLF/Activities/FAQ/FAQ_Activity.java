package com.khojaleadership.KLF.Activities.FAQ;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.FAQ.FAQ_Expandable_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.FAQ_Models.FAQ_Model;
import com.khojaleadership.KLF.Model.FAQ_Models.FAQ_data_model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FAQ_Activity extends AppCompatActivity {

    private ExpandableListView faq_expandable_list_view;
    private FAQ_Expandable_Adapter faq_expandable_adapter;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;


    ImageView add_faq;
    ImageView edit_faq;

    ArrayList<FAQ_data_model> faq_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("FAQs");



        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v,FAQ_Activity.this);
                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);

        loading_dialog.show();
        getFAQList();
     }

    private void getFAQList() {

        Call<FAQ_Model> call = retrofitInterface.getFAQList("application/json",Common.auth_token);

        call.enqueue(new Callback<FAQ_Model>() {
            @Override
            public void onResponse(Call<FAQ_Model> call, Response<FAQ_Model> response) {

                if (response.isSuccessful()) {

                    String status,message;

                    status=response.body().getStatus();
                    message=response.body().getMessage();

                    if (status.equals("success")){

                        if (faq_list!=null){
                            faq_list.clear();
                        }

                        faq_list=response.body().getData();
                        mainContentData();

                        loading_dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<FAQ_Model> call, Throwable t) {

            }
        });
    }

    public void mainContentData() {

        faq_expandable_list_view= (ExpandableListView) findViewById(R.id.faq_expandable_list);

        //data();

        faq_expandable_adapter = new FAQ_Expandable_Adapter(this, faq_list);
        faq_expandable_list_view.setAdapter(faq_expandable_adapter);

        faq_expandable_list_view.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    faq_expandable_list_view.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
