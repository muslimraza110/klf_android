package com.khojaleadership.KLF.Activities.event_new.upcomingEvent_not_used

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannedString
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.Activities.event_new.eventRegistration.EventRegistrationActivity
import com.khojaleadership.KLF.Activities.event_new.eventSections.EventSectionsActivity
import com.khojaleadership.KLF.Activities.event_new.events.EventsActivity
import com.khojaleadership.KLF.Helper.CustomTypefaceSpan
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivityUpcomingEventBinding
import timber.log.Timber

class UpcomingEventActivity : AppCompatActivity() {

    lateinit var binding: ActivityUpcomingEventBinding
    private val viewModel: UpcomingEventViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityUpcomingEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.eventData.observe(this, Observer {
            it?.let {
                it.apply {
                    Glide.with(this@UpcomingEventActivity)
                            .load(eventImage)
                            .placeholder(R.drawable.building)
//                            .placeholder(R.drawable.placeholder_image)
                            .into(binding.eventCardLayout.imageView)
                    binding.eventCardLayout.tvAddress2.text = city
                    binding.eventCardLayout.tvDate.text = date
                    Timber.e( "price----" +price)
                    Timber.e( "price----")
                    binding.eventCardLayout.tvPrice.text = price

                    val completeString: SpannedString

                    val eventName = SpannableString(name)
                    eventName.setSpan(
                            CustomTypefaceSpan(
                                    "",
                                    ResourcesCompat.getFont(this@UpcomingEventActivity, R.font.poppins_semibold)!!
                            ),
                            0,
                            name.length,
                            0
                    )

                    eventName.setSpan(
                            RelativeSizeSpan(1.1f),0,name.length,0
                    )

                    val eventAddress = SpannableString(address)
                    eventAddress.setSpan(
                            CustomTypefaceSpan(
                                    "",
                                    ResourcesCompat.getFont(this@UpcomingEventActivity, R.font.poppins_regular)!!
                            ),
                            0,
                            address.length,
                            0
                    )

                    completeString = TextUtils.concat(eventName, "\n", eventAddress) as SpannedString

                    binding.eventCardLayout.tvAddress.text = completeString

                }

            }
        })

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.tvNoEventsFound.visibility = View.GONE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.eventCardLayout.root.visibility = View.GONE
                    binding.tvPreviousEvent.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.tvPreviousEvent.visibility = View.VISIBLE
                    if (viewModel.eventData.value == null) {
                        binding.tvNoEventsFound.text = it.message
                        binding.tvNoEventsFound.visibility = View.VISIBLE
                        binding.eventCardLayout.root.visibility = View.GONE
                    } else {
                        binding.tvNoEventsFound.visibility = View.GONE
                        binding.eventCardLayout.root.visibility = View.VISIBLE
                    }
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.eventCardLayout.root.visibility = View.GONE
                    binding.tvPreviousEvent.visibility = View.VISIBLE
                    binding.tvNoEventsFound.visibility = View.INVISIBLE
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }

            }
        })

    }

    private fun setUpViews() {
        binding.imageBack.setOnClickListener {
            finish()
        }

        binding.eventCardLayout.tvViewDetails.setOnClickListener {
            viewModel.authStore.event = viewModel.eventData.value
            startActivity(Intent(this, EventSectionsActivity::class.java))
        }

        binding.tvPreviousEvent.setOnClickListener {
            startActivity(Intent(this, EventsActivity::class.java))
        }

        binding.layoutRegistration.setOnClickListener {
            if (viewModel.eventData.value != null) {
                viewModel.authStore.event = viewModel.eventData.value
                startActivity(Intent(this, EventRegistrationActivity::class.java))
            }
        }
    }
}
