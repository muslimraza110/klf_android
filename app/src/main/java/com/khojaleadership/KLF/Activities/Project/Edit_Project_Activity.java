package com.khojaleadership.KLF.Activities.Project;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Adapter.Project.EditProjectAdapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.EditProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.EditProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectDataModel;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectModel;
import com.khojaleadership.KLF.Model.Project_Models.Project_CharityType_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_CharityType_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_TargetAnualSpend_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Project_TargetAnualSpend_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.Remove_Group_From_ProjectResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.TargetAudienceDataModel;
import com.khojaleadership.KLF.Model.Project_Models.TargetAudienceModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Project_Activity extends AppCompatActivity implements RecyclerViewClickListner {

    EditProjectAdapter adapter;

    LinearLayout add_group_in_project_layout;

    private TextView tv_startDate;
    TextView tv_Est_End_Date;
    DatePickerDialog.OnDateSetListener mDateSetListener;
    DatePickerDialog.OnDateSetListener mDateSetListener1;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    EditText project_name, project_website, project_description, project_city, project_country;

    CheckBox stamp_chackbox, read_only_checkbox;

    String khoja_care_catagorey_id = "0";
    String stamp_text = "0", read_only_text = "A";

    Spinner target_audience_spinner;
    String target_audience_string;

    Spinner charity_type_spinner;
    String charity_type_string;

    Spinner target_anualspend_spinner;
    String target_anualspend_string;

    ProjectsDataModel model = new ProjectsDataModel();

    ArrayList<Project_CharityType_Data_Model> projects_charity_type_list = new ArrayList<>();
    ArrayList<String> project_charity_type_string = new ArrayList<>();

    ArrayList<TargetAudienceDataModel> projects_target_audience_list = new ArrayList<>();
    ArrayList<String> project_target_audience_string = new ArrayList<>();

    ArrayList<Project_TargetAnualSpend_Data_Model> projects_target_anualspend_list = new ArrayList<>();
    ArrayList<String> project_target_anualspend_string = new ArrayList<>();

    ArrayList<GetAlreadyGroup_ProjectDataModel> edit_already_project_group_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__project);

        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Edit Project");

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        model = intent.getParcelableExtra("ProjectData");


        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Edit_Project_Activity.this);

                finish();
            }
        });

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        GetCharityType();


        findViewById(R.id.save_project_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stamp_text.equals("0")) {
                    loading_dialog.show();
                    EditProjectFunction();
                } else {
                    if (khoja_care_catagorey_id.equals("0")) {
                        Common.Toast_Message(Edit_Project_Activity.this, "Please select Khoja Care Category");
                    } else {
                        loading_dialog.show();
                        EditProjectFunction();
                    }
                }
            }
        });


        //...................Group Section....................
        add_group_in_project_layout = findViewById(R.id.add_group_in_project_layout);
        add_group_in_project_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent group_section = new Intent(Edit_Project_Activity.this, Edit_Project_Group_Section_Activity.class);
                group_section.putExtra("ProjectData", model);
                startActivity(group_section);
            }
        });


        //percentage btn
        findViewById(R.id.percentage_project_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //percentage activity
                Intent percentage = new Intent(Edit_Project_Activity.this, Add_Percentage_Activity.class);
                percentage.putExtra("ProjectData", model);
                startActivity(percentage);

            }
        });


    }

    public void intialize_view() {

        project_name = (EditText) findViewById(R.id.project_name);
        project_website = (EditText) findViewById(R.id.project_website);
        project_description = (EditText) findViewById(R.id.project_description);
        project_city = (EditText) findViewById(R.id.project_city);
        project_country = (EditText) findViewById(R.id.project_country);
        tv_startDate = findViewById(R.id.project_start_date);
        tv_Est_End_Date = findViewById(R.id.project_est_completion_date);
        stamp_chackbox = (CheckBox) findViewById(R.id.stamp_checkbox);
        read_only_checkbox = (CheckBox) findViewById(R.id.read_only_checkbox);

        //for setting data
        if (model != null) {
            //model = Common.projects_data_detail;
            project_name.setText(model.getProjects__name());
            project_website.setText(model.getProjects__website());
            project_description.setText(model.getProjects__description());
            project_city.setText(model.getProjects__city());
            project_country.setText(model.getProjects__country());
            tv_startDate.setText(model.getProjects__start_date());
            tv_Est_End_Date.setText(model.getProjects__est_completion_date());

            //stamp check box
            if (model.getProjects__approval_stamp().equals("1")) {
                stamp_chackbox.setChecked(true);
            }

            //read only check box
            if (model.getProjects__status().equalsIgnoreCase("r")) {
                read_only_checkbox.setChecked(true);
                add_group_in_project_layout.setVisibility(View.GONE);
            }

        }

        stamp_chackbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    stamp_text = "1";

                    //closeKeyboard();
                } else if (isChecked == false) {
                    stamp_text = "0";
                }
            }
        });

        read_only_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    read_only_text = "R";
                    //if readonly checkbox is checked then user can't add groups in projects
                    add_group_in_project_layout.setVisibility(View.GONE);
                    //closeKeyboard();
                } else if (isChecked == false) {
                    read_only_text = "A";
                    add_group_in_project_layout.setVisibility(View.VISIBLE);
                }
            }
        });


        //this is for the start date
        tv_startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal1 = Calendar.getInstance();
                int year = cal1.get(Calendar.YEAR);
                int month = cal1.get(Calendar.MONTH);
                int day = cal1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog1 = new DatePickerDialog(
                        Edit_Project_Activity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                //               Toast.makeText(Edit_Project_Activity.this, "onDateSet: dd//mm//yyyy:" + month + "/" + day + "/" + year, Toast.LENGTH_SHORT).show();
                String date = year + "-" + day + "-" + month;
                tv_startDate.setText(date);
            }
        };


        //this is for the est end date
        tv_Est_End_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        Edit_Project_Activity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                //Toast.makeText(Edit_Project_Activity.this, "onDateSet: dd//mm//yyyy:" + month + "/" + day + "/" + year, Toast.LENGTH_SHORT).show();
                String date = year + "-" + day + "-" + month;
                tv_Est_End_Date.setText(date);
            }
        };


        //        this is a reccylerView Part start
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        TextView group_record = findViewById(R.id.group_record);

        if (edit_already_project_group_list != null) {
            if (edit_already_project_group_list.size() > 0) {
                adapter = new EditProjectAdapter(this, edit_already_project_group_list, this);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(adapter);

            } else {
                group_record.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            group_record.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        //        this is for the Khoja Care Category Spinner
        KhojaCareCatagoreySpinnerView();
        //        this is for the Target Audience Spinner
        CharityTypeSpinnerView();
        //        this is for the Target Audience Spinner
        TargetAudienceSpinnerView();

        //        this is for the Target Anual Spend Spinner
        TargetAnualSpendSpinnerView();
    }

    public void KhojaCareCatagoreySpinnerView() {

        final Spinner spinner = (Spinner) findViewById(R.id.Khoja_Care_Category_spinner);

        // Initializing a String Array
        final String[] plants = new String[]{
                "--Category--",
                "Eduaction for All",
                "Eradication of Poverty",
                "Humanitariant Relief",
                "Other"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {

                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray


                    tv.setTextColor(Color.BLACK);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {
                    // Notify the selected item text

                    if (position == 1) {
                        khoja_care_catagorey_id = "1";
                    } else if (position == 2) {
                        khoja_care_catagorey_id = "2";
                    } else if (position == 3) {
                        khoja_care_catagorey_id = "3";
                    } else if (position == 2) {
                        khoja_care_catagorey_id = "4";
                    }


                } else {
                    khoja_care_catagorey_id = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //setting data
        if (model != null) {
            if (model.getProjects__khoja_care_category_id().equals("1")) {
                spinner.setSelection(1);
            } else if (model.getProjects__khoja_care_category_id().equals("2")) {
                spinner.setSelection(2);
            } else if (model.getProjects__khoja_care_category_id().equals("3")) {
                spinner.setSelection(3);
            } else if (model.getProjects__khoja_care_category_id().equals("4")) {
                spinner.setSelection(4);
            } else {
                spinner.setSelection(0);
            }
        }

    }

    public void CharityTypeSpinnerView() {

        charity_type_spinner = (Spinner) findViewById(R.id.spinner_Charity_type);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_charity_type_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {

                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    if (projects_charity_type_list != null) {
                        if (projects_charity_type_list.get(position - 1).getTags__id().equals("9")) {
                            tv.setTextSize(20);
                        } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("13")) {
                            tv.setTextSize(20);
                        } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("14")) {
                            tv.setTextSize(20);
                        } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("15")) {
                            tv.setTextSize(20);
                        } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("31")) {
                            tv.setTextSize(20);
                        } else if (projects_charity_type_list.get(position - 1).getTags__id().equals("18")) {
                            tv.setTextSize(20);
                        } else {
                            tv.setTextSize(14);
                        }
                    }

                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        charity_type_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        charity_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {

                    if (projects_charity_type_list != null) {
                        charity_type_string = projects_charity_type_list.get(position - 1).getTags__id();
                    }

                } else {
                    charity_type_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (model.getCharity_type() != null) {
            Log.d("SpinnerIssue", "Charity type "+model.getCharity_type().getName());
        } else {
            Log.d("SpinnerIssue", "Charity type null");
        }
        int charity_list_index = 0;
        if (projects_charity_type_list != null) {
            //setting data
            for (int i = 0; i < projects_charity_type_list.size(); i++) {
                if (model != null) {
                    if (model.getCharity_type() != null) {
                        if (model.getCharity_type().getId().equals(projects_charity_type_list.get(i).getTags__id())) {
                            charity_list_index = i + 1;
                        }
                    }
                }
            }
        }

        charity_type_spinner.setSelection(charity_list_index);


    }

    public void TargetAudienceSpinnerView() {

        target_audience_spinner = (Spinner) findViewById(R.id.target_audience_spinner);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_target_audience_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    //tv.setTextSize(22);
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        target_audience_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        target_audience_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {

                    if (projects_target_audience_list != null) {
                        target_audience_string = projects_target_audience_list.get(position - 1).getTags__id();
                    }

                } else {
                    target_audience_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        int target_list_index = 0;
        //setting data
        if (projects_target_audience_list != null) {
            for (int i = 0; i < projects_target_audience_list.size(); i++) {
                if (model != null) {
                    if (model.getTarget_audience() != null) {
                        if (model.getTarget_audience().getId().equals(projects_target_audience_list.get(i).getTags__id())) {
                            target_list_index = i + 1;
                        }
                    }
                }
            }
        }

        target_audience_spinner.setSelection(target_list_index);


    }

    public void TargetAnualSpendSpinnerView() {

        target_anualspend_spinner = (Spinner) findViewById(R.id.target_anualspend_spinner);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> Target_audience_spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, project_target_anualspend_string) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(Color.BLACK);

                } else {

                    //tv.setTextSize(22);
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        Target_audience_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        target_anualspend_spinner.setAdapter(Target_audience_spinnerArrayAdapter);
        target_anualspend_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {

                    if (projects_target_anualspend_list != null) {
                        target_anualspend_string = projects_target_anualspend_list.get(position - 1).getTags__id();
                    }

                } else {
                    target_anualspend_string = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        int target_list_index = 0;
        //setting data
        if (projects_target_anualspend_list != null) {
            for (int i = 0; i < projects_target_anualspend_list.size(); i++) {
                if (model != null) {
                    if (model.getTarget_annual_spend() != null) {
                        if (model.getTarget_annual_spend().getId().equals(projects_target_anualspend_list.get(i).getTags__id())) {
                            target_list_index = i + 1;
                        }
                    }
                }
            }
        }
        target_anualspend_spinner.setSelection(target_list_index);


    }


    private void getAlreadyGroupsInProject(String project_id) {

        Call<GetAlreadyGroup_ProjectModel> call = retrofitInterface.getAlreadyGroupsInProject("application/json", Common.auth_token,
                project_id);


        call.enqueue(new Callback<GetAlreadyGroup_ProjectModel>() {
            @Override
            public void onResponse(Call<GetAlreadyGroup_ProjectModel> call, Response<GetAlreadyGroup_ProjectModel> response) {
                loading_dialog.dismiss();
                if (response.isSuccessful()) {
                    String status, message;

                    GetAlreadyGroup_ProjectModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<GetAlreadyGroup_ProjectDataModel> list = new ArrayList<>();
                    list = responseModel.getData();


                    if (list != null) {


                        //Common.already_project_group_list = list;

                        ArrayList<GetAlreadyGroup_ProjectDataModel> already_in_list = new ArrayList<>();

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getIsSelected().equals("1")) {
                                already_in_list.add(list.get(i));
                            }
                        }


                        edit_already_project_group_list = already_in_list;
                        intialize_view();
                    }


                    //Common.ids=IDS;
                }
            }

            @Override
            public void onFailure(Call<GetAlreadyGroup_ProjectModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }


    private void GetCharityType() {


        Call<Project_CharityType_Model> responseCall = retrofitInterface.getCharityType("application/json", Common.auth_token);

        responseCall.enqueue(new Callback<Project_CharityType_Model>() {
            @Override
            public void onResponse(Call<Project_CharityType_Model> call, Response<Project_CharityType_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Project_CharityType_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    //loading_dialog.dismiss();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Edit_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<Project_CharityType_Data_Model> list = responseModel.getData();

                        if (projects_charity_type_list != null) {
                            projects_charity_type_list.clear();
                        }
                        projects_charity_type_list = list;

                        if (project_charity_type_string != null) {
                            project_charity_type_string.clear();
                        }
                        project_charity_type_string.add("Charity Type");
                        for (int i = 0; i < projects_charity_type_list.size(); i++) {
                            project_charity_type_string.add(projects_charity_type_list.get(i).getTags__name());
                        }

                        GetTargetAudience();
                        //getAlreadyGroupsInProject(Common.projects_data_detail.getProjects__id());
                        //intialize_view();

                    }


                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Project_CharityType_Model> call, Throwable t) {
                loading_dialog.dismiss();
            }

        });


    }

    private void GetTargetAudience() {


        Call<TargetAudienceModel> responseCall = retrofitInterface.getTargetAudience("application/json", Common.auth_token);


        responseCall.enqueue(new Callback<TargetAudienceModel>() {
            @Override
            public void onResponse(Call<TargetAudienceModel> call, Response<TargetAudienceModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    TargetAudienceModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Edit_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<TargetAudienceDataModel> list = responseModel.getData();
                        if (projects_target_audience_list != null) {
                            projects_target_audience_list.clear();
                        }
                        projects_target_audience_list = list;

                        if (project_target_audience_string != null) {
                            project_target_audience_string.clear();
                        }

                        project_target_audience_string.add("Target Audience People");
                        for (int i = 0; i < projects_target_audience_list.size(); i++) {
                            project_target_audience_string.add(projects_target_audience_list.get(i).getTags__name());
                        }

                        GetTargetAnualSpend();
                        //getAlreadyGroupsInProject(Common.projects_data_detail.getProjects__id());
                        //intialize_view();

                    }


                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<TargetAudienceModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });


    }

    private void GetTargetAnualSpend() {


        Call<Project_TargetAnualSpend_Model> responseCall = retrofitInterface.getTargetAnualSpend("application/json", Common.auth_token);


        responseCall.enqueue(new Callback<Project_TargetAnualSpend_Model>() {
            @Override
            public void onResponse(Call<Project_TargetAnualSpend_Model> call, Response<Project_TargetAnualSpend_Model> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Project_TargetAnualSpend_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    //loading_dialog.dismiss();
                    if (status.equals("error")) {
                        //data is null
                        Toast.makeText(Edit_Project_Activity.this, "Server is not responding at the moment", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("success")) {

                        ArrayList<Project_TargetAnualSpend_Data_Model> list = responseModel.getData();
                        if (projects_target_anualspend_list != null) {
                            projects_target_anualspend_list.clear();
                        }
                        projects_target_anualspend_list = list;

                        if (project_target_anualspend_string != null) {
                            project_target_anualspend_string.clear();
                        }
                        project_target_anualspend_string.clear();
                        project_target_anualspend_string.add("Target Audience People");
                        for (int i = 0; i < projects_target_anualspend_list.size(); i++) {
                            project_target_anualspend_string.add(projects_target_anualspend_list.get(i).getTags__name());
                        }


                        getAlreadyGroupsInProject(model.getProjects__id());
                        //intialize_view();

                    }


                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Project_TargetAnualSpend_Model> call, Throwable t) {
                loading_dialog.dismiss();
                //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    //jis project ko edit krna uss ka data kaha se save krna
    private void EditProjectFunction() {

        EditProjectRequestModel request = new EditProjectRequestModel();
        request.setCreator_id(Common.login_data.getData().getId()); //static for now
        request.setKhoja_care_category_id(khoja_care_catagorey_id);  //static for now...will be change
        request.setName(project_name.getText().toString());
        request.setWebsite(project_website.getText().toString());
        request.setDescription(project_description.getText().toString());
        request.setStart_date(tv_startDate.getText().toString());
        request.setEst_completion_date(tv_Est_End_Date.getText().toString());
        request.setCity(project_city.getText().toString());
        request.setCountry(project_country.getText().toString());
        request.setStatus(read_only_text); //not know y
        request.setProject_id(model.getProjects__id());
        request.setApproval_stamp(stamp_text);
        request.setCharity_type(charity_type_string);
        request.setTargetaudience(target_audience_string);
        request.setTargetanualspent(target_anualspend_string);

        Call<EditProjectResponseModel> responseCall = retrofitInterface.EditProject("application/json", Common.auth_token, request);

        responseCall.enqueue(new Callback<EditProjectResponseModel>() {
            @Override
            public void onResponse(Call<EditProjectResponseModel> call, Response<EditProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    EditProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    loading_dialog.dismiss();
                    if (status.equals("error")) {
                        //data is null
                    } else if (status.equals("success")) {
                        Intent i = new Intent(Edit_Project_Activity.this, Projects_Activity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();

                        Toast.makeText(Edit_Project_Activity.this, "Project edited Successfully.", Toast.LENGTH_SHORT).show();

                    }


                } else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<EditProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });


    }


    @Override
    public void onRowClick(int position) {
        loading_dialog.show();
        if (edit_already_project_group_list != null) {
            onRemoveGroupFromProject(edit_already_project_group_list.get(position).getGroup_id(), model.getProjects__id());
        }

        //onRemoveGroupFromProject(Common.already_project_group_list.get(position).getGroup_id(),Common.projects_data_detail.getProjects__id());
    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    private void onRemoveGroupFromProject(String group_id, String project_id) {


        Remove_Group_From_ProjectRequestModel requestModel = new Remove_Group_From_ProjectRequestModel();

        requestModel.setGroup_id(group_id);
        requestModel.setProject_id(project_id);


        Call<Remove_Group_From_ProjectResponseModel> responseCall = retrofitInterface.RemoveGroupFromProject("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<Remove_Group_From_ProjectResponseModel>() {
            @Override
            public void onResponse(Call<Remove_Group_From_ProjectResponseModel> call, Response<Remove_Group_From_ProjectResponseModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    loading_dialog.dismiss();

                    Remove_Group_From_ProjectResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    Toast.makeText(Edit_Project_Activity.this, "Group remove from Project successfully", Toast.LENGTH_LONG).show();
                    getAlreadyGroupsInProject(model.getProjects__id());
                } else {
                    loading_dialog.dismiss();
                    Toast.makeText(Edit_Project_Activity.this, "Group remove from Project Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Remove_Group_From_ProjectResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                Toast.makeText(Edit_Project_Activity.this, "Group remove from Project Failed", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}

