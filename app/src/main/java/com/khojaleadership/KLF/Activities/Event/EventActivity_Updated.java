package com.khojaleadership.KLF.Activities.Event;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Event.Event_Dialoge_Adapter;
import com.khojaleadership.KLF.Adapter.Event.Event_List_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventRequestModel;
import com.khojaleadership.KLF.Model.Event_Model.DeleteEventResponseModel;
import com.khojaleadership.KLF.Model.Event_Model.GetEventListModel;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity_Updated extends AppCompatActivity implements RecyclerViewClickListner {

    Event_CustomCalanderview customCalanderview;

    RecyclerView recyclerView;
    Event_List_Adapter adapter;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    AlertDialog alertDialog;

    public ArrayList<GetEventsListDataModel> all_event_list = new ArrayList<>();
    String isMasterSearch="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event__updated);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Events");

        LinearLayout add_btn=findViewById(R.id.c_add_btn);
        ImageView add_icon=findViewById(R.id.c_add_icon);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Common.event_edit_flag=false;
                Intent edit_event = new Intent(EventActivity_Updated.this, Edit_Event.class);
                edit_event.putExtra("is_event_edit","0");
                startActivity(edit_event);
            }
        });

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v,EventActivity_Updated.this);
                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);

        Intent intent= getIntent();
        GetEventsListDataModel EventData=intent.getParcelableExtra("EventData");

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            isMasterSearch = bundle.getString("isMasterSearch");
        }else {
            isMasterSearch="0";
        }

        if (isMasterSearch.equals("1")){
            custom_view(EventData);
        }else if (isMasterSearch.equals("0")) {
            loading_dialog.show();
            getEventsList();
        }

//        if (isMasterSearch.equals("1")){
//            custom_view(EventData);
//        }else if (Common.master_search_is_event==false) {
//
//            loading_dialog.show();
//            getEventsList();
//        }
    }


    public void intialize_view(){

        try {


            recyclerView =  (RecyclerView)findViewById(R.id.events_recyclerview);

            //adapter= new Event_List_Adapter(this, Common.all_event_list,this);
            adapter= new Event_List_Adapter(this, all_event_list,this);
            recyclerView.setHasFixedSize(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);


            customCalanderview = (Event_CustomCalanderview) findViewById(R.id.custom_calender_view);
            customCalanderview.PassData(all_event_list);
            customCalanderview.intialize_grid_view();
            customCalanderview.SetUpCalendar();

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }
    }
    private void getEventsList() {

        try {
            Call<GetEventListModel> call = retrofitInterface.getAllEventList("application/json",Common.auth_token);

            call.enqueue(new Callback<GetEventListModel>() {
                @Override
                public void onResponse(Call<GetEventListModel> call, Response<GetEventListModel> response) {

                    if (response.isSuccessful()) {

                        String status,message;

                        status=response.body().getStatus();
                        message=response.body().getMessage();

                        if (status.equals("success")){

//                            if (Common.all_event_list!=null){
//                                Common.all_event_list.clear();
//                            }

                            all_event_list=response.body().getData();
                            //Common.all_event_list=response.body().getData();


                            intialize_view();

                            loading_dialog.dismiss();
                        }
                    }else {
                     }

                }

                @Override
                public void onFailure(Call<GetEventListModel> call, Throwable t) {
                    loading_dialog.dismiss();

                }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }
    public void custom_view(GetEventsListDataModel data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        View showView = LayoutInflater.from(this).inflate(R.layout.event_custom_dialog_updated, null);

        RelativeLayout close=showView.findViewById(R.id.custom_close_btn);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (isMasterSearch.equals("1")){
                    loading_dialog.show();
                    isMasterSearch="0";
                    getEventsList();
                }
            }
        });


        RecyclerView recyclerView = showView.findViewById(R.id.EventsRV);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(showView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        ArrayList<GetEventsListDataModel> list=new ArrayList<>();
        //list.add(Common.event_data_detail);
        list.add(data);

        Event_Dialoge_Adapter eventRecyclerAdapter = new Event_Dialoge_Adapter(showView.getContext()
                , list);

        recyclerView.setAdapter(eventRecyclerAdapter);
        eventRecyclerAdapter.notifyDataSetChanged();

        builder.setView(showView);
        alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }
    @Override
    public void onRowClick(int position) {

        try {

//            if (Common.all_event_list.get(position)!=null) {
//                Common.event_data_detail = Common.all_event_list.get(position);
//            }

            GetEventsListDataModel data=new GetEventsListDataModel();
            if (all_event_list.get(position)!=null) {
                data=all_event_list.get(position);
            }
            custom_view(data);
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    @Override
    public void onViewClcik(int position, View v) {

        try {

           // Common.event_edit_flag=true;
            //Common.event_data_detail=Common.all_event_list.get(position);

            GetEventsListDataModel event_data=new GetEventsListDataModel();
            event_data=all_event_list.get(position);

            if (v.getId()==R.id.edit_event) {
                Intent edit_event = new Intent(EventActivity_Updated.this, Edit_Event.class);
                edit_event.putExtra("EventData",event_data);
                edit_event.putExtra("is_event_edit","1");
                startActivity(edit_event);
            }else if (v.getId()==R.id.delete_event){
                 Confirm_Dialog("Delete Event","Are you sure to delete this event ?","Yes",event_data.getId());
             }
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }
    public void Confirm_Dialog(String title, String message, String btn, final String event_id) {


        TextView title_text, message_text, btn_text;
        RelativeLayout close_btn;

        // custom dialog
        final Dialog dialog = new Dialog(EventActivity_Updated.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);
        close_btn=(RelativeLayout)dialog.findViewById(R.id.confirm_custom_close_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);



        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                loading_dialog.show();
                DeleteEventFunction(event_id);

            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }
    private void DeleteEventFunction(String event_id) {


        try {

            DeleteEventRequestModel request=new DeleteEventRequestModel();
//            request.setEvent_id(Common.event_data_detail.getId());
            request.setEvent_id(event_id);

            Call<DeleteEventResponseModel> call = retrofitInterface.DeleteEvent("application/json",Common.auth_token,request);

            call.enqueue(new Callback<DeleteEventResponseModel>() {
                @Override
                public void onResponse(Call<DeleteEventResponseModel> call, Response<DeleteEventResponseModel> response) {

                    if (response.isSuccessful()) {

                        String status,message;

                        status=response.body().getStatus();
                        message=response.body().getMessage();

                         if (status.equals("success")){

                            Toast.makeText(EventActivity_Updated.this,"Event Deleted successfully.",Toast.LENGTH_SHORT).show();
                            getEventsList();
                        }else {
                            loading_dialog.dismiss();
                         }

                    }else {
                        loading_dialog.dismiss();
                     }
                }

                @Override
                public void onFailure(Call<DeleteEventResponseModel> call, Throwable t) {
                     loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isMasterSearch.equals("1")){
            loading_dialog.show();
            isMasterSearch="0";
            getEventsList();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();

        customCalanderview = (Event_CustomCalanderview) findViewById(R.id.custom_calender_view);
    }
}
