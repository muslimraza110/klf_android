package com.khojaleadership.KLF.Activities.event_new.eventSections

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.ResponseStatus
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Helper.launchSafely
import com.khojaleadership.KLF.Model.event_new.EventSectionsResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class EventSectionsViewModel(app: Application) : AndroidViewModel(app) {


    private val _eventSectionList: MutableLiveData<List<EventSectionUIModel>> = MutableLiveData()
     val eventSectionList: LiveData<List<EventSectionUIModel>> = _eventSectionList

    private val _notificationCount: MutableLiveData<Int> = MutableLiveData()
    val notificationCount: LiveData<Int> = _notificationCount



    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    init {
        fetchData()
    }

    private fun fetchData() {
//        Timber.e("----------fetchData")
//        Timber.e("Logout Common.login_data.getData().getAuth_token())" + Common.login_data.data.auth_token)
//        Timber.e(
//            "Logout Common.login_data.getData().getId()" + Common.login_data.data.id)
//        Timber.e(Common.auth_token.toString())
//        Timber.e(Common.login_data?.data?.id
//            ?: "")
//
//        Timber.e("----------EventSectionsViewModel"+authStore.event?.eventId)
        _eventState.value =Result.Loading

        viewModelScope.launchSafely(
                block = {
                    val response =eventsRepository.getEventSections(authStore.event?.eventId ?: "")
                    if(response.status == ResponseStatus.SUCCESS){
                        response.data?.let { list->
                            val tempList = list.map {
                                mapToEventSectionUiModel(it)
                            }.toMutableList()

                            _eventSectionList.value =tempList
                            _eventState.value = Result.Success(
                                    message = response.message,
                                    data = Unit
                            )
                        } ?:run {
                            _eventSectionList.value = listOf()
                            _eventState.value = Result.Success(
                                    message = response.message ?: Common.noRecordFoundMessage,
                                    data = Unit
                            )
                        }
                    }
                    else{
                        _eventState.value = Result.Error(
                                message = response.message ?: Common.somethingWentWrongMessage
                        )
                    }
                },
                error = {
                    Timber.e(it)
                    _eventState.value = Result.Error(
                            message = Common.somethingWentWrongMessage
                    )
                }
        )
    }

    private fun mapToEventSectionUiModel(responseData: EventSectionsResponse) : EventSectionUIModel {
        return EventSectionUIModel(
                name = responseData.title ?: "",
                tag = responseData.tag ?: "",
                description = responseData.shortDescription ?: ""
//                backgroundImage = responseData.backgroundImage
        )

    }

    fun getNotifications() {

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {

                        val response = eventsRepository.getPlannerNotifications(
                                userId = userId,
                                eventId = authStore.event?.eventId ?: ""
                        )

                        if(response.status == ResponseStatus.SUCCESS){
                            response.data?.let { list->

                                _notificationCount.value = list.size

                            } ?:run {
                                _notificationCount.value = 0
                            }
                        }
                        else{
                            _notificationCount.value = 0
                        }

                    },
                    error = {
                        Timber.e(it)
                        _notificationCount.value = 0
                    }
            )
        } ?: run {
            _notificationCount.value = 0
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
        }

    }


}