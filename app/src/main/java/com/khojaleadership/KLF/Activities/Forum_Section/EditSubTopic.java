package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.EditSubTopicRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.EditSubTopicResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSubTopic extends AppCompatActivity {

    ScrollView scrollView;
    private RadioGroup mFirstGroup;
    private RadioGroup mSecondGroup;

    CheckBox pinned_post, show_on_dashboarrd, send_eblast, post_approved;
    String pinned_post_Text = "0", sod_text = "0", post_approved_text = "0";

    private boolean isChecking = true;
    private int mCheckedId = R.id.radio_public;
    String post_visibility = "P";


    EditText title;
    RichEditor mEditor;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog loading_dialog;
    Boolean flag = false;


    LinearLayout restricted_layout;
    TextView whitelist_contact_btn, whitelist_group_btn;
    LinearLayout check_box, tv_Moderator, visibilty_layout;
    TextView header_title;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    String Category;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sub_topic);
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Edit SubTopic");

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
        }

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, EditSubTopic.this);
                Intent i = new Intent(EditSubTopic.this, Sub_Topics.class);
                i.putExtra("TopicDataDetail",topic_data_detail);
                i.putExtra("SubtopicDataDetail",sub_topic_data_detail);
                i.putExtra("Category", Category);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });


        check_box = findViewById(R.id.check_box);
        tv_Moderator = findViewById(R.id.tv_Moderator);
        visibilty_layout = findViewById(R.id.visibilty_layout);

        if (Common.is_Admin_flag == false) {
            check_box.setVisibility(View.GONE);
            tv_Moderator.setVisibility(View.GONE);
            visibilty_layout.setVisibility(View.GONE);
        }

        intialize_view();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(EditSubTopic.this, Sub_Topics.class);
        i.putExtra("TopicDataDetail",topic_data_detail);
        i.putExtra("SubtopicDataDetail",sub_topic_data_detail);
        i.putExtra("Category", Category);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    public void intialize_view() {
        loading_dialog = Common.LoadingDilaog(this);

        findViewById(R.id.add_moderator_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditSubTopic.this, AddModerator.class);
                i.putExtra("TopicDataDetail",topic_data_detail);
                i.putExtra("SubtopicDataDetail",sub_topic_data_detail);
                i.putExtra("Category", Category);
                startActivity(i);
            }
        });


        restricted_layout = (LinearLayout) findViewById(R.id.restricted_layout);
        //calling white list function
        whitelist_contact_btn = (TextView) findViewById(R.id.whitelist_contact);
        whitelist_contact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Common.is_for_whitelist_contacts = true;

                Intent i = new Intent(EditSubTopic.this, WhitelistContactsScreen.class);
                i.putExtra("TopicDataDetail",topic_data_detail);
                i.putExtra("SubtopicDataDetail",sub_topic_data_detail);
                i.putExtra("Category", Category);
                //i.putExtra("IsWhitelistContacts","1");
                startActivity(i);
            }
        });

        //calling white list function
        whitelist_group_btn = (TextView) findViewById(R.id.whitelist_group);
        whitelist_group_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditSubTopic.this, WhitelistGroupScreenUpdated.class);
                i.putExtra("TopicDataDetail",topic_data_detail);
                i.putExtra("SubtopicDataDetail",sub_topic_data_detail);
                i.putExtra("Category", Category);
                startActivity(i);
            }
        });


        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        send_eblast = (CheckBox) findViewById(R.id.edit_send_eblast_checkbox);
        send_eblast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    send_eblast.setChecked(true);
                    Common.send_eblast_falg = true;
                } else {
                    Common.send_eblast_falg = false;
                    send_eblast.setChecked(false);
                }
            }
        });

        post_approved = (CheckBox) findViewById(R.id.edit_post_approved);
        //first chechbox should be false
        post_approved.setChecked(false);
        post_approved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    post_approved.setChecked(true);
                    post_approved_text = "1";

                    closeKeyboard();

                } else {
                    post_approved.setChecked(false);
                    post_approved_text = "0";
                }
            }
        });

        show_on_dashboarrd = (CheckBox) findViewById(R.id.edit_sod_checkbox);
        show_on_dashboarrd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sod_text = "1";
                    show_on_dashboarrd.setChecked(true);
                    closeKeyboard();
                } else {
                    sod_text = "0";
                    show_on_dashboarrd.setChecked(false);
                }
            }
        });


        pinned_post = (CheckBox) findViewById(R.id.edit_pined_post_checkbox);
        pinned_post.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    pinned_post.setChecked(true);
                    pinned_post_Text = "1";
                    closeKeyboard();
                } else {
                    pinned_post.setChecked(false);
                    pinned_post_Text = "0";
                }
            }
        });


        title = (EditText) findViewById(R.id.edit_sub_topic_title);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(title, InputMethodManager.SHOW_IMPLICIT);
        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);

        closeKeyboard();

        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(5);
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setPadding(10, 10, 10, 10);

        scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);


                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag = false;
                            action_right_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);

                            action_left_flag = false;
                            action_right_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag = false;
                            action_left_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });

        title.setText(sub_topic_data_detail.getForumPosts__topic());
        mEditor.setHtml(sub_topic_data_detail.getContent());

        if (sub_topic_data_detail.getForumPosts__sod().equals("1")) {
            show_on_dashboarrd.setChecked(true);
        }

        if (sub_topic_data_detail.getForumPosts__pinned().equals("1")) {
            pinned_post.setChecked(true);
        }


        if (sub_topic_data_detail.getPost_approved_post().equals("1")) {
            post_approved.setChecked(true);
        }


        mFirstGroup = (RadioGroup) findViewById(R.id.first_group);
        mSecondGroup = (RadioGroup) findViewById(R.id.second_group);


         mFirstGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                 if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    mSecondGroup.clearCheck();
                    mCheckedId = checkedId;

                    if (mCheckedId == R.id.radio_public) {
                         post_visibility = "P";
                        restricted_layout.setVisibility(View.GONE);
                    } else if (mCheckedId == R.id.radio_Sami_private) {
                        post_visibility = "S";
                         restricted_layout.setVisibility(View.GONE);
                    }


                }
                isChecking = true;
            }
        });

        if (sub_topic_data_detail.getForumPosts__visibility() != null) {
            if (sub_topic_data_detail.getForumPosts__visibility().equals("P")) {
                //i checked it manually in xml file.
                 mFirstGroup.check(R.id.radio_public);
            } else if (sub_topic_data_detail.getForumPosts__visibility().equals("S")) {
                mFirstGroup.check(R.id.radio_Sami_private);
             }
        }


        mSecondGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                 if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    mFirstGroup.clearCheck();
                    mCheckedId = checkedId;

                    if (mCheckedId == R.id.radio_Restricted) {
                         post_visibility = "W";
                        restricted_layout.setVisibility(View.VISIBLE);
                    } else if (mCheckedId == R.id.radio_admin) {
                         post_visibility = "A";
                        restricted_layout.setVisibility(View.GONE);
                    }

                }
                isChecking = true;
            }
        });


        if (sub_topic_data_detail.getForumPosts__visibility() != null) {
            if (sub_topic_data_detail.getForumPosts__visibility().equals("W")) {
                //i checked it manually in xml file.
                 mSecondGroup.check(R.id.radio_Restricted);
            } else if (sub_topic_data_detail.getForumPosts__visibility().equals("A")) {
                 mSecondGroup.check(R.id.radio_admin);
            }
        }


        findViewById(R.id.publish_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (title.getText().toString().equalsIgnoreCase("") ||
                        title.getText().toString().isEmpty()) {

                    title.setError("Title should not be empty.");
                } else if (title.getText().toString().replace(" ", "").equals("")) {
                    title.setError("Title should not contain only space.");
                } else if (mEditor.getHtml() == null) {
                    Toast.makeText(EditSubTopic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (mEditor.getHtml().equals("")) {
                    Toast.makeText(EditSubTopic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (mEditor.getHtml().equalsIgnoreCase("")) {
                    Toast.makeText(EditSubTopic.this, "Content should not be empty.", Toast.LENGTH_SHORT).show();
                } else {


                    //aik dafa button press py add kre
                    if (flag == false) {
                        flag = true;

                        if (Common.send_eblast_falg == true) {
                             Common.send_eblast_post = false;
                            Common.send_eblast_list = "";

                            //for use when recipient added
                            Common.eblast_subject = "";
                            Common.eblast_label = "";
                            Common.eblast_message = "";

                            //clear email list
                            if (Common.send_eblast_email_list != null) {
                                if (Common.send_eblast_email_list.size() > 0) {
                                    Common.send_eblast_email_list.clear();
                                }
                            }

                            //saving category and data
                            Common.Category=Category;
                            Common.topic_data_detail=topic_data_detail;
                            Common.eblast_message=sub_topic_data_detail.getContent();
                            Intent i = new Intent(EditSubTopic.this, SendEblastScreen1.class);
                            startActivity(i);
                        } else if (Common.send_eblast_falg == false) {

                            loading_dialog.show();
                            onEditSubTopicBtnClick();
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.activityResumed();
        //aggr send eblast ki screen se wapis ata h tu btn enable ho
        flag = false;

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void onEditSubTopicBtnClick() {

        try {


            EditSubTopicRequestModel requestModel = new EditSubTopicRequestModel();
            requestModel.setUser_id(Common.login_data.getData().getId());
            requestModel.setPost_id(sub_topic_data_detail.getId());
            requestModel.setTopic(title.getText().toString());
            requestModel.setContent(mEditor.getHtml());
            requestModel.setPost_visibility(post_visibility);
            requestModel.setEblast("0");
            requestModel.setPinned(pinned_post_Text);
            requestModel.setSod(sod_text);
            requestModel.setPost_approved_post(post_approved_text);


            Call<EditSubTopicResponseModel> responseCall = retrofitInterface.editSubTopic("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<EditSubTopicResponseModel>() {
                @Override
                public void onResponse(Call<EditSubTopicResponseModel> call, Response<EditSubTopicResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        EditSubTopicResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        loading_dialog.dismiss();

                        if (status.equals("success")) {
                            Intent sub_topic_intent = new Intent(EditSubTopic.this, Sub_Topics.class);
                            sub_topic_intent.putExtra("TopicDataDetail",topic_data_detail);
                            sub_topic_intent.putExtra("SubtopicDataDetail",sub_topic_data_detail);
                            sub_topic_intent.putExtra("Category", Category);
                            sub_topic_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(sub_topic_intent);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EditSubTopicResponseModel> call, Throwable t) {

                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }
    }
}
