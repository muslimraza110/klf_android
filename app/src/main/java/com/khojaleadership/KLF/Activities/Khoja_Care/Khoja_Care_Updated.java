package com.khojaleadership.KLF.Activities.Khoja_Care;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.khojaleadership.KLF.Adapter.KhojaCare.Khoja_care_expandable_adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Khoja_Care_Models.Khoja_care_child_model;
import com.khojaleadership.KLF.Model.Khoja_Care_Models.Khoja_care_parent_model;
import com.khojaleadership.KLF.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Khoja_Care_Updated extends AppCompatActivity {


    ExpandableListView expandableListView;
    ArrayList<Khoja_care_parent_model> expandableModel_parents = new ArrayList<Khoja_care_parent_model>();
    Khoja_care_expandable_adapter adapter_parent;


    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = initRetrofit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khoja__care__updated);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Khoja Care");


        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Khoja_Care_Updated.this);

                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        GetKhojaCareList();


    }


    private void GetKhojaCareList() {

        Call<String> call = retrofitInterface.GetKhojaCareList("application/json", Common.auth_token);


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String json_response = response.body().toString();
                        JsonToModel(json_response);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public RetrofitInterface initRetrofit() {
        String Base_Url = Common.LIVE_BASE_URL;   //live url

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Base_Url)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        return retrofitInterface;
    }

    public void JsonToModel(String response) {

        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.optString("status");

            if (status.equals("success")) {
                JSONObject data = obj.getJSONObject("data");

                Iterator<String> keys = data.keys();

                ArrayList<Khoja_care_parent_model> parent_list = new ArrayList<>();
                while (keys.hasNext()) {
                    String key = keys.next();
                    //this is key value "Education for All"

                    JSONArray array = data.getJSONArray(key);
                    ArrayList<Khoja_care_child_model> child_list = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject value = array.getJSONObject(i);
                        String Projects__id, Projects__name, Projects__description, Creators__first_name, Creators__last_name, Projects__city, Projects__country, charity;

                        Projects__id = value.getString("Projects__id");
                        Projects__name = value.getString("Projects__name");
                        Projects__description = value.getString("Projects__description");
                        Creators__first_name = value.getString("Creators__first_name");
                        Creators__last_name = value.getString("Creators__last_name");
                        Projects__city = value.getString("Projects__city");
                        Projects__country = value.getString("Projects__country");
                        charity = value.getString("charity");

                        Khoja_care_child_model child = new Khoja_care_child_model();
                        child.setProjects__id(Projects__id);
                        child.setProjects__name(Projects__name);
                        child.setProjects__description(Projects__description);
                        child.setCreators__first_name(Creators__first_name);
                        child.setCreators__last_name(Creators__last_name);
                        child.setProjects__city(Projects__city);
                        child.setProjects__country(Projects__country);
                        child.setCharity(charity);

                        //child list
                        child_list.add(child);
                    }

                    parent_list.add(new Khoja_care_parent_model(key, child_list));

                }

                if (parent_list.size() > 0) {
                    loading_dialog.dismiss();
                    expandableModel_parents = parent_list;

                    for (int i = 0; i < expandableModel_parents.size(); i++) {
                    }
                    intialize_view();
                }


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void intialize_view() {
        expandableListView = findViewById(R.id.list_parent);
        //data();
        adapter_parent = new Khoja_care_expandable_adapter(this, expandableModel_parents);
        expandableListView.setAdapter(adapter_parent);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {

                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    expandableListView.collapseGroup(previousGroup);
                }

                previousGroup = groupPosition;

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}

