package com.khojaleadership.KLF.Activities.event_new.userProfile

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.MeetingRequestStatusTypes
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore

class UserProfileViewModel(application: Application) : AndroidViewModel(application) {


    var isDelegate = false
    var facebookUrl = ""
    var twitterUrl = ""
    var linkedInUrl = ""
    var requestedToId = ""
    var userId = ""
    var programDetailsId: String? = null

    var dayIndex = 0

    var meetingRequestSent = false

    private val _spinnerDaysData: MutableLiveData<List<EventDaysUiModel>> = MutableLiveData()
    val spinnerDaysData: LiveData<List<EventDaysUiModel>> = _spinnerDaysData


    private val _spinnerTimeData: MutableLiveData<List<List<EventTimeSlotUiModel>>> = MutableLiveData()
    val spinnerTimeData: LiveData<List<List<EventTimeSlotUiModel>>> = _spinnerTimeData

    private val _sendMeetingRequestEvent: MutableLiveData<Result<Unit>> = MutableLiveData()
    val sendMeetingRequestEvent: LiveData<Result<Unit>> = _sendMeetingRequestEvent

    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")


    fun sendMeetingRequest(meetingNotes: String){

        _sendMeetingRequestEvent.value = Result.Loading

        Common.login_data?.data?.id?.let {userId->
            programDetailsId?.let { programDetailsId->

                viewModelScope.launchSafely(
                        block = {
                            val response = eventsRepository.setSummitEventMeetingRequestStatus(
                                    meetingStatus = MeetingRequestStatusTypes.MEETING_REQUESTED.value,
                                    meetingNotes = meetingNotes,
                                    programDetailsId = programDetailsId,
                                    requestedById = authStore.event?.facultyId ?: "",
                                    requestedToId = requestedToId,
                                    userId = userId,
                                    isUpdate = false
                            )

                            if (response.status == ResponseStatus.SUCCESS) {
                                response.data?.let {data->
                                    if(data.isError == false && data.updatedStatus == MeetingRequestStatusTypes.MEETING_REQUESTED.value){

                                        authStore.meetingRequestSent = true
//                                        meetingRequestSent = true
//                                        if(_spinnerDaysData.value?.isNotEmpty() == true){
//
//                                            val tempMeetingSlots = authStore.lastSelectedPersonAvailability
//                                            tempMeetingSlots?.forEach {
//                                                it.slots?.removeAll { item ->
//                                                    item.programId == programDetailsId
//                                                }
//                                            }
//                                            authStore.lastSelectedPersonAvailability = tempMeetingSlots
//                                            loadAvailableMeetingSlots()
//                                            this.programDetailsId = null
//                                        }

                                        _sendMeetingRequestEvent.value = Result.Success(message = "" ,data = Unit)
                                        _messageEvent.value = Event(response.message ?: requestSentMessage)

                                    } else{

                                        _sendMeetingRequestEvent.value = Result.Error(message = "")
                                        _messageEvent.value = Event(response.message ?: "")

                                    }
                                }

                            }else{
                                _sendMeetingRequestEvent.value = Result.Error(message = "")
                                _messageEvent.value = Event(response.message ?: Common.somethingWentWrongMessage)
                            }

                        },
                        error = {
                            _sendMeetingRequestEvent.value = Result.Error(message = "")
                            _messageEvent.value = Event(Common.somethingWentWrongMessage)
                        }
                )


            } ?: run {
                _sendMeetingRequestEvent.value = Result.Error(Common.somethingWentWrongMessage)
            }

        } ?: run{
            _sendMeetingRequestEvent.value = Result.Error(Common.idNotFoundMessage)
        }

    }


    fun loadAvailableMeetingSlots() {

//        val meetingSlotData = authStore.lastSelectedPersonAvailability
        val meetingSlotData = authStore.event?.meetingSlots

        val tempListDay = mutableListOf<EventDaysUiModel>()
        val tempListTime = mutableListOf<List<EventTimeSlotUiModel>>()

        meetingSlotData?.forEach { item ->

            if (item.slots?.isEmpty() == false){
                tempListDay.add(
                        EventDaysUiModel(
                                isSelected = false,
                                date = item.programDate ?: "",
                                day = item.programDay ?: ""
                        )
                )
                val timeSlots = item.slots.map { slot ->
                    EventTimeSlotUiModel(
                            programDetailsId = slot.programId ?: "",
                            time = slot.time ?: ""
                    )
                }

                tempListTime.add(timeSlots)
            }

        }

        _spinnerTimeData.value = tempListTime
        _spinnerDaysData.value = tempListDay
        
    }

    fun areMeetingSlotsAvailable(): Boolean {
        return !_spinnerDaysData.value.isNullOrEmpty()
    }


    companion object{
        const val requestSentMessage = "Meeting Request sent successfully."
    }
}