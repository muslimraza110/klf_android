package com.khojaleadership.KLF.Activities.event_new.upcomingEvent_not_used

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.events.EventCardUiModel
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.ResponseStatus
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Helper.launchSafely
import com.khojaleadership.KLF.Model.event_new.SingleEventResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class UpcomingEventViewModel(val app: Application) : AndroidViewModel(app) {


    private val _eventData: MutableLiveData<EventCardUiModel> = MutableLiveData()
    val eventData: LiveData<EventCardUiModel> = _eventData

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = app,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")


    init {
        fetchData()
    }

    fun fetchData() {

        _eventState.value = Result.Loading

        viewModelScope.launchSafely(
                block = {
                    val response = eventsRepository.getCurrentEvents(Common.login_data?.data?.id
                            ?: "")

                    if (response.status == ResponseStatus.SUCCESS) {
                        response.data?.let { data ->
                            Timber.d(response.toString())
                            data.firstOrNull()?.let {
                                _eventData.value = mapToEventCardModel(it)
                                _eventState.value = Result.Success(
                                        data = Unit,
                                        message = response.message
                                )
                            }
                        } ?: run {
                            _eventState.value = Result.Success(
                                    message = response.message ?: Common.noRecordFoundMessage,
                                    data = Unit
                            )

                        }

                    } else {
                        _eventState.value = Result.Error(
                                message = response.message ?: Common.somethingWentWrongMessage
                        )
                    }

                },
                error = {
                    Timber.e(it)
                    _eventState.value = Result.Error(
                            message = Common.somethingWentWrongMessage
                    )
                }
        )

    }

    private fun mapToEventCardModel(responseData: SingleEventResponse): EventCardUiModel {
        return EventCardUiModel(
                date = responseData.startDate ?: "",
                address = responseData.address ?: "",
                city = responseData.city ?: "",
                eventImage = responseData.images?.firstOrNull() ?: "",
                eventId = responseData.id.toString(),
                price = "${responseData.priceRangeStart ?: ""} - $${responseData.priceRangeEnd ?: ""}",

                priceRangeEnd = "${responseData.priceRangeEnd ?: "0"}",
                priceRangeStart = "${responseData.priceRangeStart ?: "0"}",
                registration_link = responseData.registrationLink ?: "",


                isUserRegistered = responseData.isUserRegistered ?: false,
                name = responseData.name ?: "",
                facultyId = responseData.summitEventsFacultyId,
                meetingSlots = responseData.meetingSlots,
                startDate = responseData.startDate ?: "",
                endDate = responseData.endDate ?: ""
        )
    }
}