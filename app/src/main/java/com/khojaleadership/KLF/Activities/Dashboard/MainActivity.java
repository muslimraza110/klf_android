package com.khojaleadership.KLF.Activities.Dashboard;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.khojaleadership.KLF.Activities.Contact.ContactActivity;
import com.khojaleadership.KLF.Activities.event_new.events.EventsActivity;
import com.khojaleadership.KLF.Activities.FAQ.FAQ_Activity;
import com.khojaleadership.KLF.Activities.Feedback.Feedback_Activity;
import com.khojaleadership.KLF.Activities.Forum_Section.Topic;
import com.khojaleadership.KLF.Activities.Forum_Section.Sub_Topic_Posts;
import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Activities.Group.GroupsMainScreen;
import com.khojaleadership.KLF.Activities.Mission.Mission_Activity;
import com.khojaleadership.KLF.Activities.Project.Projects_Activity;
import com.khojaleadership.KLF.Activities.Resources.Khoja_Summit_Activity;
import com.khojaleadership.KLF.Activities.Resources.News_Activity_updated;
import com.khojaleadership.KLF.Activities.Resources.News_Detail_Activity;
import com.khojaleadership.KLF.Activities.Setting.Change_Password;
import com.khojaleadership.KLF.Activities.Splash_Login.LoginActivity;
import com.khojaleadership.KLF.Adapter.DashBoard.Home_DrawerExpandableList_Adapter;
import com.khojaleadership.KLF.Adapter.DashBoard.Home_Search_Adapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Activities.Setting.Edit_Profile_Activity;
import com.khojaleadership.KLF.Activities.Event.EventActivity_Updated;
import com.khojaleadership.KLF.Adapter.DashBoard.Home_Expandable_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Activities.Intiative.Intiative_Activity_Detail_Updated;
import com.khojaleadership.KLF.Activities.Intiative.Intiative_Activity_Updated;
import com.khojaleadership.KLF.Activities.Khoja_Care.Khoja_Care_Updated;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Expandable_Child_Model;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListModel;
import com.khojaleadership.KLF.Model.Contact.GetContactListModel;
import com.khojaleadership.KLF.Model.Event_Model.GetEventListModel;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Child_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Initiative_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_ForumsMasterSearch_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Main_Search_Model;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Data_Model;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsModel;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelGroupJoinRequest_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelGroupJoinResponse_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelProjectJoinRequest_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_CancelProjectJoinResponse_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_ForumSubscriptions_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_ForumsJointheDiscussion_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_PendingGroupAccessRequests_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_PendingProjectAccessRequests_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Expandable_Parent_Model;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;
import com.khojaleadership.KLF.Activities.Project.View_Projects_Detail_Updated;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.ClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Parent_Model;
import com.khojaleadership.KLF.Model.logout.LogoutRequest;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ClickListner, RecyclerViewClickListner {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter drawer_adapter;

    String fcmToken_id;
    ArrayList<Home_Parent_Model> pModel = new ArrayList<>();
    int[] menu_icons = new int[]{R.drawable.menu_1, R.drawable.menu_2, R.drawable.menu_3, R.drawable.menu_4
            , R.drawable.menu_5, R.drawable.menu_6, R.drawable.menu_7
            , R.drawable.menu_8, R.drawable.menu_9, R.drawable.menu_10
            , R.drawable.menu_11, R.drawable.menu_12, R.drawable.menu_9, R.drawable.menu_13, R.drawable.menu_12,
    };


    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    SharedPreferences sp;
    SharedPreferences.Editor Ed;

    public Context mctx;
    ImageView notification_icon;

    ExpandableListView dashboard_expandableListView;
    ArrayList<Home_Expandable_Parent_Model> expandableModel_parents = new ArrayList<Home_Expandable_Parent_Model>();
    Home_Expandable_Adapter adapter_parent;


    List<Home_Expandable_Child_Model> home_data_list1 = new ArrayList<Home_Expandable_Child_Model>();
    List<Home_Expandable_Child_Model> home_data_list2 = new ArrayList<Home_Expandable_Child_Model>();
    List<Home_Expandable_Child_Model> home_data_list3 = new ArrayList<Home_Expandable_Child_Model>();
    List<Home_Expandable_Child_Model> home_data_list4 = new ArrayList<Home_Expandable_Child_Model>();
    List<Home_Expandable_Child_Model> home_data_list5 = new ArrayList<Home_Expandable_Child_Model>();

    RelativeLayout main_search_layout;
    RecyclerView recyclerView;
    TextView master_search_no_found;
    Home_Search_Adapter main_search_adapter;
    RelativeLayout home_search_btn;

    private ClearEditText mClearEditText;
    int spinner_item_position = 0;
    public ArrayList<Home_Main_Search_Model> main_list;
    public ArrayList<Home_Main_Search_Model> filtered_list = new ArrayList<>();
    public Boolean filter_flag = false;

    //for for internet conectivity
    BroadcastReceiver broadcastReceiver;
    final IntentFilter intentFilter = new IntentFilter();


    public ArrayList<GetEventsListDataModel> all_event_list = new ArrayList<>();
    ArrayList<News_Fragments_Data_Model> all_news_list = new ArrayList<>();

    ArrayList<GetAllGroup_ProjectListDataModel> project_group_list = new ArrayList<>();

    ArrayList<Home_Main_Search_Model> main_search_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        findViewById(R.id.main_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, MainActivity.this);
            }
        });


        findViewById(R.id.main_layout1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, MainActivity.this);
            }
        });


        try {

            onCreateData();
            //intialize Internet connectivity Broadcast
            Common.is_Online = true;
            installListener();
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    Boolean is_online = true;

    private void installListener() {

        if (broadcastReceiver == null) {

            broadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {


                    boolean isVisible = App.isActivityVisible();

                    if (isVisible == true) {

                        is_online = true;
                        ConnectivityManager conn = (ConnectivityManager)
                                context.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo networkInfo = conn.getActiveNetworkInfo();
                        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
                            Intent connected = new Intent("connected");
                            context.sendBroadcast(connected);

                            if (Common.is_Online == false) {
                                startActivity(new Intent(MainActivity.this, MainActivity.class));
                            }

                        } else {
                            Intent connected = new Intent("Connection Lost");
                            context.sendBroadcast(connected);

                            if (Common.is_Online == true) {
                                startActivity(new Intent(MainActivity.this, OfflineDashBoad.class));
                            }
                        }

                    }
                }
            };


            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.activityResumed();
        //from returning to groups loader was always loading
        loading_dialog.dismiss();
    }

    public void onCreateData() {

        //Common.isOffline = false;
        mctx = this;


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        GetHomeData();

        notification_icon = (ImageView) findViewById(R.id.notification_icon);
        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading_dialog.show();
                GetHomeData();
            }
        });


        //added
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableListView = (ExpandableListView) findViewById(R.id.navList);


        drawerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, MainActivity.this);
            }
        });


        //adding header view
        View listHeaderView = getLayoutInflater().inflate(R.layout.aa_dashboard_nav_header, null, false);
        listHeaderView.setMinimumHeight(200);
        listHeaderView.setMinimumWidth(310);
        expandableListView.addHeaderView(listHeaderView);

        TextView header_username, header_email;
        ImageView header_img;

        header_username = (TextView) listHeaderView.findViewById(R.id.header_user_name);
        header_email = (TextView) listHeaderView.findViewById(R.id.header_user_email);
        header_img = listHeaderView.findViewById(R.id.header_img);

        if (Common.login_data != null) {
            header_username.setText(Common.login_data.getData().getFirst_name() + "_" + Common.login_data.getData().getLast_name());
            header_email.setText(Common.login_data.getData().getEmail());

            Glide.with(getApplicationContext()).load(Common.login_data.getData().getProfile_img()).into(header_img);
        }


        //adding footer view
        View footerView = getLayoutInflater().inflate(R.layout.aa_dashboard_nav_footer, null, false);
        expandableListView.addFooterView(footerView);

        footerView.findViewById(R.id.logout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AtomicReference<String> fcmToken = new AtomicReference<>("");


                Timber.d("Logout Common.login_data.getData().getAuth_token())" + Common.login_data.getData().getAuth_token());
                Timber.d("Logout Common.login_data.getData().getId()" + Common.login_data.getData().getId());
                Timber.d("Logout  fcmToken.get()" + fcmToken.get());
                FirebaseMessaging.getInstance().getToken().addOnSuccessListener(new OnSuccessListener<String>() {
                    @Override
                    public void onSuccess(String s) {
                        if (s != null) {
                            fcmToken.set(s);
//                            fcmToken_id = s;
                            new SharedPreferencesAuthStore(
                                    MainActivity.this,
                                    new Gson()
                            ).setLoginData(null);
                            Timber.d("Logout  fcmToken.get()" + fcmToken.get());
                            if(fcmToken.get().isEmpty()){
                                fcmToken.set(fcmToken.get());
                                Timber.d("elseasdsaddsaseee " +fcmToken_id);
                            }
                            else{
                                Timber.d("elseeee " + fcmToken.get());
                                Timber.d("elseasdsaddsaseee " +fcmToken_id);
                            }
                            retrofitInterface
                                    .logout("application/json", Common.login_data.getData().getAuth_token(), new LogoutRequest(
                                            Common.login_data.getData().getId(),
                                            fcmToken.get()

                                    )).enqueue(new Callback<Object>() {

                                @Override
                                public void onResponse(@NotNull Call<Object> call, @NotNull Response<Object> response) {
                                    if (response.isSuccessful()) {
                                        Timber.d("Logout success");
                                        Timber.d("Logout Common.login_data.getData().getId()" + Common.login_data.getData().getId());
                                        Timber.d("Logout  fcmToken.get()" + fcmToken.get());
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull Call<Object> call, @NotNull Throwable t) {
                                    Timber.d("Logout onFailure");
                                }
                            });
                        }
                        Timber.e(s);
                    }
                });



                Common.hideKeyboard(v, mctx);
                sp = getSharedPreferences("Login", 0);
                Ed = sp.edit();
                Ed.putString("logout", "1");
                Ed.commit();

                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });



        addDrawersItem();
        setupDrawer();

        findViewById(R.id.drawer_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.hideKeyboard(v, mctx);
                Common.hideKeyboard(v, mctx);
                // If the navigation drawer is not open then open it, if its already open then close it.
                if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.openDrawer(GravityCompat.START);
                else drawerLayout.closeDrawer(GravityCompat.END);

            }
        });


        //dashboard spinner
        Main_Spinner();

    }

    public void Main_Spinner() {

        try {
            main_search_layout = findViewById(R.id.main_search_layout);
            recyclerView = (RecyclerView) findViewById(R.id.main_recyclerView);
            master_search_no_found = findViewById(R.id.master_search_no_found);
            mClearEditText = (ClearEditText) findViewById(R.id.main_filter_edittext);
            mClearEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (count == 0) {
                        main_search_layout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    main_search_layout.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            home_search_btn = findViewById(R.id.home_search_btn);
            home_search_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.d("MasterSearchIssue", "Keyword :" + mClearEditText.getText().toString());
                    main_search_layout.setVisibility(View.GONE);
                    main_search_list.clear();

                    if (spinner_item_position == 0) {
                        Common.Toast_Message(MainActivity.this, "Select the category first");
                    } else if (mClearEditText.getText().toString() == null || mClearEditText.getText().toString().isEmpty() ||
                            mClearEditText.getText().toString().equalsIgnoreCase("")) {
                        Common.Toast_Message(MainActivity.this, "Search keyword missing");
                    } else if (spinner_item_position == 1) {
                        loading_dialog.show();
                        getContactListFunction(mClearEditText.getText().toString());
                    } else if (spinner_item_position == 2) {
                        loading_dialog.show();
                        getEventsList(mClearEditText.getText().toString());
                    } else if (spinner_item_position == 3) {
                        //..........for forum search
                        loading_dialog.show();
                        MainForumsSearch(mClearEditText.getText().toString());
                    } else if (spinner_item_position == 4) {
                        loading_dialog.show();
                        getAllGroups(mClearEditText.getText().toString(), true);
                    } else if (spinner_item_position == 5) {
                        loading_dialog.show();
                        getNewsListFunction(mClearEditText.getText().toString());
                    } else if (spinner_item_position == 6) {
                        loading_dialog.show();
                        onGetProjectListData(mClearEditText.getText().toString());

                    } else {
                        //this is for spinner position other than 0-6
                    }


                }
            });


            //catagorey list
            ArrayList<String> main_spinner_items = new ArrayList<>();
            main_spinner_items.add("Category");
            main_spinner_items.add("Contact");
            main_spinner_items.add("Event");
            main_spinner_items.add("Forum");
            main_spinner_items.add("Groups");
            main_spinner_items.add("News");
            main_spinner_items.add("Projects");
            Spinner catagorey_spinner = (Spinner) findViewById(R.id.Main_Category_spinner);

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, main_spinner_items) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    tv.setTextColor(Color.BLACK);
                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.spinner_item);
            catagorey_spinner.setAdapter(adapter);
            catagorey_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //category = parent.getItemAtPosition(position).toString();

                    TextView tv = (TextView) view;
                    tv.setTextColor(Color.WHITE);
                    Common.hideKeyboard(view, mctx);
                    Common.hideKeyboard(view, mctx);
                    if (position == 0) {
                        spinner_item_position = 0;

                    } else if (position == 1) {
                        spinner_item_position = 1;
                    } else if (position == 2) {
                        spinner_item_position = 2;
                    } else if (position == 3) {
                        spinner_item_position = 3;
                    } else if (position == 4) {
                        spinner_item_position = 4;
                    } else if (position == 5) {
                        spinner_item_position = 5;
                    } else if (position == 6) {
                        spinner_item_position = 6;
                    }

                    ((TextView) view).setTextSize(12);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    public void intialize_view() {

        try {
            //  if (main_search_list != null) {
            //    if (main_search_list.size() > 0) {
            main_search_layout.setVisibility(View.VISIBLE);
            if (filter_flag == true) {
                Log.d("MasterSearchIssue", "filter flag true");
                if (filtered_list.size() > 0) {

                    main_search_adapter = new Home_Search_Adapter(this, filtered_list, this);
                } else {
                    main_search_layout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    master_search_no_found.setVisibility(View.VISIBLE);
                }
            } else if (filter_flag == false) {
                Log.d("MasterSearchIssue", "filter flag false");
                main_search_adapter = new Home_Search_Adapter(this, main_search_list, this);
            }

            recyclerView.setHasFixedSize(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(main_search_adapter);

            //                } else {
//                    main_search_layout.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.GONE);
//                    master_search_no_found.setVisibility(View.VISIBLE);
//                }

//            } else {
//             }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void filterData(String filterStr) {

        try {
            filter_flag = true;
            ArrayList<Home_Main_Search_Model> filterDataList = new ArrayList<>();

            if (TextUtils.isEmpty(filterStr)) {
            } else {

                if (main_search_list != null) {
                    for (Home_Main_Search_Model sortModel : main_search_list) {
                        String name = sortModel.getName();
                        if (name.indexOf(filterStr.toString()) != -1
                                || name.toLowerCase().startsWith(filterStr)
                                || name.toUpperCase().startsWith(filterStr)) {

                            filterDataList.add(sortModel);
                        }
                    }
                }
            }

            filtered_list = filterDataList;
            Log.d("MasterSearchIssue", "filtered list :" + filtered_list.size());
            intialize_view();

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onRowClick(int position) {

        try {
            ArrayList<Home_Main_Search_Model> list = new ArrayList<>();
            if (filter_flag == true) {
                list = filtered_list;
            } else {
                list = main_search_list;
            }
            String id = list.get(position).getId();
            if (spinner_item_position == 1) {

//                Common.master_search_view_profile = true;
//                Common.view_profile_id = id;

                //....................contact screen changed
                Intent detail = new Intent(MainActivity.this, View_Profile_Activity_Updated.class);
                detail.putExtra("view_profile_id", id);
                detail.putExtra("isMasterSearch", "1");
                startActivity(detail);

            } else if (spinner_item_position == 2) {

//                for (int i = 0; i < Common.all_event_list.size(); i++) {
//                    if (Common.all_event_list.get(i).getId().equals(id)) {
//                        Common.event_data_detail = Common.all_event_list.get(i);
//                        i = Common.all_event_list.size();
//                    }
//                }

                GetEventsListDataModel event_data = new GetEventsListDataModel();
                for (int i = 0; i < all_event_list.size(); i++) {
                    if (all_event_list.get(i).getId().equals(id)) {
//                        Common.event_data_detail = all_event_list.get(i);
                        event_data = all_event_list.get(i);
                        i = all_event_list.size();
                    }
                }

                //Common.master_search_is_event = true;
                Intent view_event = new Intent(MainActivity.this, EventActivity_Updated.class);
                view_event.putExtra("EventData", event_data);
                view_event.putExtra("isMasterSearch", "1");
                startActivity(view_event);
            } else if (spinner_item_position == 3) {
                //Common.For_Home_forum_flag = true;
                //Common.sub_topic_edit_form_id = Integer.valueOf(main_search_list.get(position).getId());
                Intent view_event = new Intent(MainActivity.this, Sub_Topic_Posts.class);
                view_event.putExtra("IsFromDashBoard", "1");
                view_event.putExtra("SubtopicId", Integer.valueOf(main_search_list.get(position).getId()));
                startActivity(view_event);
            } else if (spinner_item_position == 4) {

                GetAllGroup_ProjectListDataModel all_group_data_detail = new GetAllGroup_ProjectListDataModel();

                for (int i = 0; i < project_group_list.size(); i++) {
                    if (project_group_list.get(i).getGroup_id().equals(id)) {
                        all_group_data_detail = project_group_list.get(i);
                        i = project_group_list.size();
                    }

                }

//                Common.is_all_group_detail = true;
//                Common.viewprofile_group_detail = false;
                Intent view_event = new Intent(MainActivity.this, GroupsDetail.class);
                view_event.putExtra("all_group_data_detail", all_group_data_detail);
                view_event.putExtra("isAllGroupDetail", "1");
                view_event.putExtra("ViewProfileDetail", "0");
                startActivity(view_event);

            } else if (spinner_item_position == 5) {
                News_Fragments_Data_Model news_list_data = new News_Fragments_Data_Model();

                for (int i = 0; i < all_news_list.size(); i++) {
                    if (all_news_list.get(i).getPosts__id().equals(id)) {
                        news_list_data = all_news_list.get(i);
                        i = all_news_list.size();
                    }
                }
//                Common.is_news_falg = true;
                Intent view_event = new Intent(MainActivity.this, News_Detail_Activity.class);
                view_event.putExtra("NewsData", news_list_data);
                view_event.putExtra("isNewsFlag", "1");
                startActivity(view_event);
            } else if (spinner_item_position == 6) {
                // Common.is_project_detail = false;
                //Common.project_id_for_detail = id;
                Intent view_event = new Intent(MainActivity.this, View_Projects_Detail_Updated.class);
                view_event.putExtra("ProjectId", id);
                view_event.putExtra("isProjectDetail", "0");
                startActivity(view_event);
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    private void MainForumsSearch(String q) {
        try {

            Call<Home_ForumsMasterSearch_Model> call = retrofitInterface.MainForumsMasterSearch("application/json", Common.auth_token, q);

            call.enqueue(new Callback<Home_ForumsMasterSearch_Model>() {
                @Override
                public void onResponse(Call<Home_ForumsMasterSearch_Model> call, Response<Home_ForumsMasterSearch_Model> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                        Log.d("MasterSearchIssue", "forum list:" + response.body().getData().size());
                        if (status.equals("success")) {


                            ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Home_Main_Search_Model item = new Home_Main_Search_Model();
                                item.setName(response.body().getData().get(i).getForum__topic());
                                item.setId(response.body().getData().get(i).getForum__id());

                                main_event_list.add(item);
                            }

                            loading_dialog.dismiss();
                            if (main_event_list != null) {
                                main_search_list = main_event_list;
                                main_list = main_event_list;

                                filter_flag = false;
                                intialize_view();
                            }


                        } else {
                            loading_dialog.dismiss();
                            filter_flag = false;
                            intialize_view();
                        }


                    } else {
                        loading_dialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<Home_ForumsMasterSearch_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getEventsList(final String keyword) {

        try {

            Call<GetEventListModel> call = retrofitInterface.getAllEventList("application/json", Common.auth_token);

            call.enqueue(new Callback<GetEventListModel>() {
                @Override
                public void onResponse(Call<GetEventListModel> call, Response<GetEventListModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                        if (status.equals("success")) {


                            //Common.all_event_list = response.body().getData();
                            all_event_list = response.body().getData();

                            ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Home_Main_Search_Model item = new Home_Main_Search_Model();
                                item.setName(response.body().getData().get(i).getName());
                                item.setId(response.body().getData().get(i).getId());

                                main_event_list.add(item);
                            }

                            main_search_list = main_event_list;
                            main_list = main_event_list;

                            //intialize_view();
                            filterData(keyword);


                            loading_dialog.dismiss();
                        }
                    } else {
                    }

                }

                @Override
                public void onFailure(Call<GetEventListModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getContactListFunction(final String keyword) {

        try {

            Call<GetContactListModel> responseCall = retrofitInterface.getContactList("application/json", Common.auth_token);
            responseCall.enqueue(new Callback<GetContactListModel>() {
                @Override
                public void onResponse(Call<GetContactListModel> call, Response<GetContactListModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        GetContactListModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                            loading_dialog.dismiss();

                            //Common.main_contact_list = responseModel.getData();

                            Log.d("MasterSearchIssue", "contact list :" + response.body().getData().size());

                            ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Home_Main_Search_Model item = new Home_Main_Search_Model();
                                item.setName(response.body().getData().get(i).getUsers__first_name() + " " + response.body().getData().get(i).getUsers__last_name());
                                item.setId(response.body().getData().get(i).getUsers__id());

                                main_event_list.add(item);
                            }

                            main_search_list = main_event_list;
                            main_list = main_event_list;

                            filterData(keyword);
                        }


                    } else {
                        loading_dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetContactListModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getAllGroups(final String keyword, final Boolean is_search) {

        try {

            Call<GetAllGroup_ProjectListModel> call = retrofitInterface.getProjectAllGroupList("application/json", Common.auth_token);

            call.enqueue(new Callback<GetAllGroup_ProjectListModel>() {
                @Override
                public void onResponse(Call<GetAllGroup_ProjectListModel> call, Response<GetAllGroup_ProjectListModel> response) {

                    if (response.isSuccessful()) {

                        GetAllGroup_ProjectListModel responseModel = response.body();

                        ArrayList<GetAllGroup_ProjectListDataModel> list = new ArrayList<>();
                        list = responseModel.getData();


                        if (list != null) {
                            loading_dialog.dismiss();
                            project_group_list = list;

                            if (is_search == true) {
                                ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    Home_Main_Search_Model item = new Home_Main_Search_Model();
                                    item.setName(response.body().getData().get(i).getName());
                                    item.setId(response.body().getData().get(i).getGroup_id());

                                    main_event_list.add(item);
                                }

                                main_search_list = main_event_list;
                                main_list = main_event_list;

                                //intialize_view();
                                filterData(keyword);
                            } else if (is_search == false) {
                                //this for main expandable pending project access request
                                String id = Common.Home_MainContent_data.getId();

                                GetAllGroup_ProjectListDataModel all_group_data_detail = new GetAllGroup_ProjectListDataModel();

                                for (int i = 0; i < project_group_list.size(); i++) {

                                    if (project_group_list.get(i).getGroup_id().equals(id)) {
                                        all_group_data_detail = project_group_list.get(i);
                                        i = project_group_list.size();
                                    }

                                }

//                                Common.is_all_group_detail = true;
//                                Common.viewprofile_group_detail = false;
                                Intent view_event = new Intent(MainActivity.this, GroupsDetail.class);
                                view_event.putExtra("all_group_data_detail", all_group_data_detail);
                                view_event.putExtra("isAllGroupDetail", "1");
                                view_event.putExtra("ViewProfileDetail", "0");
                                startActivity(view_event);
                            }
                        }


                    }
                }

                @Override
                public void onFailure(Call<GetAllGroup_ProjectListModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getNewsListFunction(final String keyword) {

        try {

            Call<News_Fragments_Model> responseCall = retrofitInterface.getUpdatedNewsList("application/json", Common.auth_token);

            responseCall.enqueue(new Callback<News_Fragments_Model>() {
                @Override
                public void onResponse(Call<News_Fragments_Model> call, Response<News_Fragments_Model> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {


                        String status, message;

                        News_Fragments_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        ;

                        ArrayList<News_Fragments_Data_Model> list = new ArrayList<>();
                        list = responseModel.getData();

                        // Common.all_news_list = list;
                        all_news_list = list;
                        if (list != null) {

                            ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Home_Main_Search_Model item = new Home_Main_Search_Model();
                                item.setName(response.body().getData().get(i).getPosts__name());
                                item.setId(response.body().getData().get(i).getPosts__id());

                                main_event_list.add(item);
                            }

                            main_search_list = main_event_list;
                            main_list = main_event_list;

                            filterData(keyword);
                        }


                    } else {
                    }


                }

                @Override
                public void onFailure(Call<News_Fragments_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onGetProjectListData(final String keyword) {

        try {

            Call<ProjectsModel> responseCall = retrofitInterface.getProjectsList("application/json", Common.auth_token);
            responseCall.enqueue(new Callback<ProjectsModel>() {
                @Override
                public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        ProjectsModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<ProjectsDataModel> project_data = responseModel.getData();
                        //Common.projects_data_list = project_data;

                        Log.d("ProjectIssue", "Project list :" + project_data.size());
                        if (project_data != null) {
                            loading_dialog.dismiss();

                            ArrayList<Home_Main_Search_Model> main_event_list = new ArrayList<>();

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Home_Main_Search_Model item = new Home_Main_Search_Model();
                                item.setName(response.body().getData().get(i).getProjects__name());
                                item.setId(response.body().getData().get(i).getProjects__id());

                                main_event_list.add(item);
                            }

                            main_search_list = main_event_list;
                            main_list = main_event_list;
                            filterData(keyword);

                        }

                    } else {
                        loading_dialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<ProjectsModel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public void updated_mainContentData() {

        dashboard_expandableListView = (ExpandableListView) findViewById(R.id.list_brands);
        updated_data();

        adapter_parent = new Home_Expandable_Adapter(this, expandableModel_parents, this);
        dashboard_expandableListView.setAdapter(adapter_parent);

        dashboard_expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                closeKeyboard();
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    dashboard_expandableListView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });

        dashboard_expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                closeKeyboard();
            }
        });

        loading_dialog.dismiss();
    }


    private void AdminData() {
        ArrayList<Home_Child_Model> cModel = new ArrayList<>();
        cModel.add(new Home_Child_Model("Admin"));
        cModel.add(new Home_Child_Model("Charity"));
        cModel.add(new Home_Child_Model("Business"));
        cModel.add(new Home_Child_Model("Other"));

        pModel.add(new Home_Parent_Model("Forums", cModel));


        ArrayList<Home_Child_Model> c2Model = new ArrayList<>();
        c2Model.add(new Home_Child_Model("News"));
        c2Model.add(new Home_Child_Model("Khoja Summits"));

        pModel.add(new Home_Parent_Model("Resources", c2Model));


        ArrayList<Home_Child_Model> c3Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Mission", c3Model));

        ArrayList<Home_Child_Model> c4Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Groups", c4Model));


        ArrayList<Home_Child_Model> c5Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Contacts", c5Model));

        ArrayList<Home_Child_Model> c6Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Projects", c6Model));


        ArrayList<Home_Child_Model> c7Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Initiatives", c7Model));


        ArrayList<Home_Child_Model> c8Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Khoja Care", c8Model));


        ArrayList<Home_Child_Model> c9Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Events", c9Model));


        ArrayList<Home_Child_Model> c10Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Feedbacks", c10Model));

        ArrayList<Home_Child_Model> c11Model = new ArrayList<>();
        pModel.add(new Home_Parent_Model("FAQs", c11Model));

        ArrayList<Home_Child_Model> c14Model = new ArrayList<>();
        c14Model.add(new Home_Child_Model("View Profile"));
        c14Model.add(new Home_Child_Model("Edit Profile"));
        c14Model.add(new Home_Child_Model("Change Password"));
        pModel.add(new Home_Parent_Model("Settings", c14Model));

        ArrayList<Home_Child_Model> c9BModel = new ArrayList<>();
        pModel.add(new Home_Parent_Model("Summit", c9BModel));


    }


    private void addDrawersItem() {
        AdminData();

        drawer_adapter = new Home_DrawerExpandableList_Adapter(this, pModel, menu_icons, this);
        expandableListView.setAdapter(drawer_adapter);

        //default first group expand
        expandableListView.expandGroup(0);


        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int GroupPosition, long l) {

                Common.hideKeyboard(view, mctx);

                if (GroupPosition == 0) {
                    //forum section
                } else if (GroupPosition == 1) {
                    //Resource section
                } else if (GroupPosition == 2) {
                    //Mission section
                    Intent moderator = new Intent(MainActivity.this, Mission_Activity.class);
                    startActivity(moderator);
                } else if (GroupPosition == 3) {
                    //Group section
                    Intent contact = new Intent(MainActivity.this, GroupsMainScreen.class);
                    startActivity(contact);
                } else if (GroupPosition == 4) {
                    //Contact section
                    Intent contact = new Intent(MainActivity.this, ContactActivity.class);
                    startActivity(contact);
                } else if (GroupPosition == 6) {
                    //Intiative section
                    Intent intiative = new Intent(MainActivity.this, Intiative_Activity_Updated.class);
                    startActivity(intiative);
                } else if (GroupPosition == 7) {
                    //Khoja care section
                    Intent khoja_care = new Intent(MainActivity.this, Khoja_Care_Updated.class);
                    startActivity(khoja_care);
                } else if (GroupPosition == 5) {
                    //Project section
                    Intent projects = new Intent(MainActivity.this, Projects_Activity.class);
                    startActivity(projects);
                } else if (GroupPosition == 8) {
                    //Event section
                    Intent events = new Intent(MainActivity.this, EventActivity_Updated.class);
                    startActivity(events);
                } else if (GroupPosition == 9) {
                    //Feedback section
                    Intent Feedback = new Intent(MainActivity.this, Feedback_Activity.class);
                    startActivity(Feedback);
                } else if (GroupPosition == 10) {
                    //FAQ section
                    Intent FAQ = new Intent(MainActivity.this, FAQ_Activity.class);
                    startActivity(FAQ);
                } else if (GroupPosition == 11) {
                    //do nothing
                } else if (GroupPosition == 12) {
//                    Timber.e("GroupPositionEventsActivity");
                    Intent intent = new Intent(MainActivity.this, EventsActivity.class);
                    startActivity(intent);
                } else {
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });

    }

    private void setupDrawer() {
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Common.hideKeyboard(drawerView, mctx);
            }
        };

        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(toggle);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void Confirm_Dialog(String title, String message, String btn) {
        TextView title_text, message_text, btn_text;
        // custom dialog
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);

        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    @Override
    public void onGroupClick(int GroupPosition) {


    }

    @Override
    public void onRowClick(int GroupPosition, int ChildPosition, View v) {
        Common.hideKeyboard(v, mctx);

        try {

            if (v.getId() == R.id.main_content_child_name) {
                if (Common.Home_MainContent_data.getParent_number() == 5) {

                    // Common.intiative_id = Common.Home_MainContent_data.getId();
                    Intent i = new Intent(MainActivity.this, Intiative_Activity_Detail_Updated.class);
                    i.putExtra("InitiativeId", Common.Home_MainContent_data.getId());
                    startActivity(i);

                } else if (Common.Home_MainContent_data.getParent_number() == 4) {
                    //Common.For_Home_forum_flag = true;

//                    int id = Integer.valueOf(Common.Home_MainContent_data.getId());
                    //Common.sub_topic_edit_form_id = id;
                    Intent post_intent = new Intent(MainActivity.this, Sub_Topic_Posts.class);
                    post_intent.putExtra("IsFromDashBoard", "1");
                    post_intent.putExtra("SubtopicId", Common.Home_MainContent_data.getId());
                    startActivity(post_intent);

                } else if (Common.Home_MainContent_data.getParent_number() == 3) {
//                    Common.For_Home_forum_flag = true;
//                    int id = Integer.valueOf(Common.Home_MainContent_data.getId());
//                    Common.sub_topic_edit_form_id = id;
//

                    Intent post_intent = new Intent(MainActivity.this, Sub_Topic_Posts.class);
                    post_intent.putExtra("IsFromDashBoard", "1");
                    post_intent.putExtra("SubtopicId", Common.Home_MainContent_data.getId());
                    startActivity(post_intent);

                } else if (Common.Home_MainContent_data.getParent_number() == 2) {
                    //Common.is_project_detail = false;
                    //Common.project_id_for_detail = Common.Home_MainContent_data.getId();

                    String id = Common.Home_MainContent_data.getId();
                    Intent view_event = new Intent(MainActivity.this, View_Projects_Detail_Updated.class);
                    view_event.putExtra("ProjectId", id);
                    view_event.putExtra("isProjectDetail", "0");
                    startActivity(view_event);

                } else if (Common.Home_MainContent_data.getParent_number() == 1) {
                    loading_dialog.show();
                    getAllGroups("", false);
                }

            } else if (v.getId() == R.id.Cancel_btn) {
                //this is for main home page content
                loading_dialog.show();
                if (Common.Home_MainContent_data.getIs_group_access_request() == true && Common.Home_MainContent_data.getIs_project_access_request() == false) {
                    Cancel_GroupJoinRequest(Common.Home_MainContent_data.getId());
                } else if (Common.Home_MainContent_data.getIs_group_access_request() == false && Common.Home_MainContent_data.getIs_project_access_request() == true) {
                    Cancel_ProjectJoinRequest(Common.Home_MainContent_data.getId());
                }

            } else {
                //this is for main drawer expandable layout
                if (GroupPosition == 0) {
                    //pd.show();
                    if (ChildPosition == 0) {
                        // loading_dialog.show();
                        //Common.Forum_cat_name = "Admin";
                        onMainTopicBtnClick("Admin");
                    }

                    if (ChildPosition == 1) {
                        //loading_dialog.show();
                        //Common.Forum_cat_name = "Charity";
                        onMainTopicBtnClick("Charity");

                    }

                    if (ChildPosition == 2) {
                        //loading_dialog.show();
                        //Common.Forum_cat_name = "Business";
                        onMainTopicBtnClick("Business");

                    }

                    if (ChildPosition == 3) {
                        //loading_dialog.show();
                        //Common.Forum_cat_name = "Other";
                        onMainTopicBtnClick("Other");

                    }


                } else if (GroupPosition == 1) {
                    if (ChildPosition == 0) {
                        Intent news = new Intent(MainActivity.this, News_Activity_updated.class);
                        startActivity(news);
                    } else if (ChildPosition == 1) {
                        Intent khoja_summit = new Intent(MainActivity.this, Khoja_Summit_Activity.class);
                        startActivity(khoja_summit);
                    }
                } else if (GroupPosition == 11) {
                    if (ChildPosition == 0) {
                        //Common.master_search_view_profile = false;
                        Intent view_profile = new Intent(MainActivity.this, View_Profile_Activity_Updated.class);
                        view_profile.putExtra("isMasterSearch", "0");
                        startActivity(view_profile);
                    } else if (ChildPosition == 1) {
                        Intent edit_Profile = new Intent(MainActivity.this, Edit_Profile_Activity.class);
                        startActivity(edit_Profile);
                    } else if (ChildPosition == 2) {
                        Intent change_password = new Intent(MainActivity.this, Change_Password.class);
                        startActivity(change_password);
                    }
                }
            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }


    private void onMainTopicBtnClick(String admin) {

        Intent admin_forum_intent = new Intent(MainActivity.this, Topic.class);
        admin_forum_intent.putExtra("Category", admin);
        startActivity(admin_forum_intent);

//        try {
//
//            final ArrayList<GetTopicDataModel> list = new ArrayList<>();
//
//            Call<GetTopicModel> call = retrofitInterface.getTopic("application/json", Common.auth_token,
//                    admin, 1, 1000);
//
//            call.enqueue(new Callback<GetTopicModel>() {
//                @Override
//                public void onResponse(Call<GetTopicModel> call, Response<GetTopicModel> response) {
//                    ArrayList<GetTopicDataModel> topic_data_list = new ArrayList<>();
//
//                    if (response.isSuccessful()) {
//                        GetTopicModel responseModel = response.body();
//                        topic_data_list = responseModel.getData();
//                        for (GetTopicDataModel data : topic_data_list) {
//                            String Subforums__id, Subforums__user_id, Subforums__model, Subforums__title, Subforums__description, Subforums__status, Subforums__created, Subforums__modified;
//
//                            Subforums__id = data.getSubforums__id();
//                            Subforums__user_id = data.getSubforums__user_id();
//                            Subforums__model = data.getSubforums__model();
//                            Subforums__title = data.getSubforums__title();
//                            Subforums__description = data.getSubforums__description();
//                            Subforums__status = data.getSubforums__status();
//                            Subforums__created = data.getSubforums__created();
//                            Subforums__modified = data.getSubforums__modified();
//
//
//                            list.add(new GetTopicDataModel(Subforums__id, Subforums__user_id, Subforums__model, Subforums__title, Subforums__description, Subforums__status, Subforums__created, Subforums__modified));
//                        }
//
//
//                        ArrayList<Integer> ids = new ArrayList<>();
//
//                        for (int i = 0; i < list.size(); i++) {
//                            ids.add(Integer.valueOf(list.get(i).getSubforums__id()));
//                        }
//
//                        if (Common.ids != null) {
//                            Common.ids.clear();
//                        }
//                        Common.ids = ids;
//
//                        if (list != null) {
//                            //pd.dismiss();
//                            loading_dialog.dismiss();
//
//                            if (Common.topic_data != null) {
//                                Common.topic_data.clear();
//                            }
//                            Common.topic_data = list;
//                            Intent admin_forum_intent = new Intent(MainActivity.this, Topic.class);
//                            startActivity(admin_forum_intent);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<GetTopicModel> call, Throwable t) {
//                }
//            });
//
//            throw new RuntimeException("Run Time exception");
//
//        } catch (Exception e) {
//        }


    }

    private void GetHomeData() {
        try {
            Call<Home_Model> call = retrofitInterface.GetHomeData("application/json", Common.auth_token,
                    Common.login_data.getData().getId());

            call.enqueue(new Callback<Home_Model>() {
                @Override
                public void onResponse(Call<Home_Model> call, Response<Home_Model> response) {


                    if (response.isSuccessful()) {

                        if (response.body().getStatus().equals("success")) {
                            //clear all previous data.........................
                            if (expandableModel_parents != null) {
                                expandableModel_parents.clear();
                            }
                            if (home_data_list1 != null) {
                                home_data_list1.clear();
                            }
                            if (home_data_list2 != null) {
                                home_data_list2.clear();
                            }
                            if (home_data_list3 != null) {
                                home_data_list3.clear();
                            }
                            if (home_data_list4 != null) {
                                home_data_list4.clear();
                            }
                            if (home_data_list5 != null) {
                                home_data_list5.clear();
                            }

                            //clear all previous data.........................

                            ArrayList<Home_PendingGroupAccessRequests_Model> PendingGroupAccessRequests = response.body().getPendingGroupAccessRequests();

                            ArrayList<Home_PendingProjectAccessRequests_Model> PendingProjectAccessRequests = response.body().getPendingProjectAccessRequests();

                            ArrayList<Home_ForumsJointheDiscussion_Model> ForumsJointheDiscussion = response.body().getForumsJointheDiscussion();

                            ArrayList<Home_ForumSubscriptions_Model> ForumSubscriptions = response.body().getForumSubscriptions();

                            ArrayList<Home_Initiative_Model> Initaive = response.body().getInitiatives();

                            for (int a = 0; a < PendingGroupAccessRequests.size(); a++) {
                                home_data_list1.add(new Home_Expandable_Child_Model(true, false, PendingGroupAccessRequests.get(a).getGroups__name(), PendingGroupAccessRequests.get(a).getGroupRequests__id(), 1));
                            }

                            for (int b = 0; b < PendingProjectAccessRequests.size(); b++) {
                                home_data_list2.add(new Home_Expandable_Child_Model(false, true, PendingProjectAccessRequests.get(b).getProjects__name(), PendingProjectAccessRequests.get(b).getProjectRequests__project_id(), 2));
                            }

                            List<Home_Expandable_Child_Model> Admin = new ArrayList<>();
                            List<Home_Expandable_Child_Model> charity = new ArrayList<>();
                            List<Home_Expandable_Child_Model> others = new ArrayList<>();

                            for (int c = 0; c < ForumsJointheDiscussion.size(); c++) {
                                //home_data_list3.add(new Home_Expandable_Child_Model(false,false,ForumsJointheDiscussion.get(c).getForumPosts__topic(),ForumsJointheDiscussion.get(c).getForumPosts__id()));
                                String category = ForumsJointheDiscussion.get(c).getForumPosts__category().toLowerCase();
                                if (category.equals("admin")) {
                                    Admin.add(new Home_Expandable_Child_Model(false, false, ForumsJointheDiscussion.get(c).getForumPosts__topic(), ForumsJointheDiscussion.get(c).getForumPosts__id(), 3));
                                } else if (category.equals("charity")) {
                                    charity.add(new Home_Expandable_Child_Model(false, false, ForumsJointheDiscussion.get(c).getForumPosts__topic(), ForumsJointheDiscussion.get(c).getForumPosts__id(), 3));
                                } else {
                                    others.add(new Home_Expandable_Child_Model(false, false, ForumsJointheDiscussion.get(c).getForumPosts__topic(), ForumsJointheDiscussion.get(c).getForumPosts__id(), 3));
                                }
                            }


                            //now make a new array containing all these three array's
                            home_data_list3.add(new Home_Expandable_Child_Model(false, false, "Admin", "-1", 3));
                            home_data_list3.addAll(Admin);

                            home_data_list3.add(new Home_Expandable_Child_Model(false, false, "Charity", "-2", 3));
                            home_data_list3.addAll(charity);

                            home_data_list3.add(new Home_Expandable_Child_Model(false, false, "Others", "-3", 3));
                            home_data_list3.addAll(charity);


                            for (int d = 0; d < ForumSubscriptions.size(); d++) {
                                home_data_list4.add(new Home_Expandable_Child_Model(false, false, ForumSubscriptions.get(d).getForumPosts__topic(), ForumSubscriptions.get(d).getForumPosts__id(), 4));
                            }

                            for (int e = 0; e < Initaive.size(); e++) {
                                home_data_list5.add(new Home_Expandable_Child_Model(false, false, Initaive.get(e).getInitiatives__name(), Initaive.get(e).getInitiatives__id(), 5));
                            }


                            updated_mainContentData();

                        } else {
                            loading_dialog.dismiss();
                        }


                    }
                }

                @Override
                public void onFailure(Call<Home_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void updated_data() {

        try {

            Home_Expandable_Parent_Model expandableModelParent_PendingGroup =
                    new Home_Expandable_Parent_Model(R.drawable.ic_forum_icon, "Pending Group Access Requests", home_data_list1);


            Home_Expandable_Parent_Model expandableModelParent_PendingProject =
                    new Home_Expandable_Parent_Model(R.drawable.ic_forum_icon, "Pending project Access Requests", home_data_list2);


            Home_Expandable_Parent_Model expandableModelParent_ForumJoin =
                    new Home_Expandable_Parent_Model(R.drawable.pinpost_icon, "Forums Join the Discussion", home_data_list3);


            Home_Expandable_Parent_Model expandableModelParent_ForumSubscription =
                    new Home_Expandable_Parent_Model(R.drawable.subscription_icon, "Forum Subscriptions", home_data_list4);

            Home_Expandable_Parent_Model expandableModelParent_Initiative =
                    new Home_Expandable_Parent_Model(R.drawable.initiative_icon, "Initiative  ", home_data_list5);


            expandableModel_parents.add(expandableModelParent_PendingGroup);
            expandableModel_parents.add(expandableModelParent_PendingProject);
            expandableModel_parents.add(expandableModelParent_Initiative);
            expandableModel_parents.add(expandableModelParent_ForumJoin);
            expandableModel_parents.add(expandableModelParent_ForumSubscription);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void Cancel_GroupJoinRequest(String id) {

        Home_CancelGroupJoinRequest_Model request_model = new Home_CancelGroupJoinRequest_Model();
        request_model.setGroup_requests_id(id);
        Call<Home_CancelGroupJoinResponse_Model> call = retrofitInterface.Home_Cancel_GroupJoinRequest("application/json", Common.auth_token, request_model);

        call.enqueue(new Callback<Home_CancelGroupJoinResponse_Model>() {
            @Override
            public void onResponse(Call<Home_CancelGroupJoinResponse_Model> call, Response<Home_CancelGroupJoinResponse_Model> response) {


                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("success")) {
                        GetHomeData();
                        Toast.makeText(MainActivity.this, "Group Join Request Cancelled.", Toast.LENGTH_SHORT).show();
                    } else {
                        loading_dialog.dismiss();
                    }


                }
            }

            @Override
            public void onFailure(Call<Home_CancelGroupJoinResponse_Model> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }

    private void Cancel_ProjectJoinRequest(String id) {

        try {

            Home_CancelProjectJoinRequest_Model request_model = new Home_CancelProjectJoinRequest_Model();
            request_model.setProject_id(id);
            request_model.setUser_id(Common.login_data.getData().getId());

            Call<Home_CancelProjectJoinResponse_Model> call = retrofitInterface.Home_Cancel_ProjectJoinRequest("application/json", Common.auth_token, request_model);

            call.enqueue(new Callback<Home_CancelProjectJoinResponse_Model>() {
                @Override
                public void onResponse(Call<Home_CancelProjectJoinResponse_Model> call, Response<Home_CancelProjectJoinResponse_Model> response) {

                    Log.d("WithHeader", "Status Code :" + response.code());


                    if (response.isSuccessful()) {

                        if (response.body().getStatus().equals("success")) {
                            GetHomeData();
                            Toast.makeText(MainActivity.this, "Project Join Request Cancelled.", Toast.LENGTH_SHORT).show();
                        } else {
                            loading_dialog.dismiss();
                        }


                    }
                }

                @Override
                public void onFailure(Call<Home_CancelProjectJoinResponse_Model> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }
}
