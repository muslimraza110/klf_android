package com.khojaleadership.KLF.Activities.Group;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Group.EditGroup_MemberList_Adapter;
import com.khojaleadership.KLF.Adapter.Group.EditGroup_PendingAccessRequests_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Group_Models.AddGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.EditGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.EditGroup_PendingAccessRequestsData_Model;
import com.khojaleadership.KLF.Model.Group_Models.EditGroup_PendingAccessRequests_Model;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupResponseMOdel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupRequestModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditGroup extends AppCompatActivity implements RecyclerViewClickListner {

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    String category;

    CheckBox visible, accept_donation, pending, passive;
    String visible_status = "0", accept_status = "0", pending_status = "0", passive_status = "0";

    TextView name, description, email, website, tags, phone, address1, address2, city, postal_code, province, country;
    TextView edit_group_name, edit_group_description;
    RichEditor mEditor;
    ScrollView scrollView;


    RecyclerView recyclerView;
    EditGroup_PendingAccessRequests_Adapter adapter;

    RecyclerView member_recyclerView;
    EditGroup_MemberList_Adapter member_adapter;

    TextView group_member_text, editGroup_pendingAccess_member_text;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean  action_left_flag = false, action_right_flag = false, action_centr_flag = false;

    GetGroupListDataModel group_data_detail=new GetGroupListDataModel();
    //ArrayList<String> cat_list = new ArrayList<>();

    List<GetGroupMemberListDataModel> add_member_list = new ArrayList<>();
    ArrayList<GetGroupMemberListDataModel> group_member_list = new ArrayList<>();
    ArrayList<EditGroup_PendingAccessRequestsData_Model> editGroup_PendingAccessRequest_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_group);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //getting data
        Intent intent= getIntent();
        group_data_detail=intent.getParcelableExtra("group_data_detail");
        //cat_list=intent.getStringArrayListExtra("cat_list");
        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        onGetPendingAccessRequestListFunction();


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Edit Group");



        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, EditGroup.this);

                finish();
            }
        });


        findViewById(R.id.in_edit_group_add_member_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditGroup.this, Add_Member_In_Group.class);
                i.putExtra("group_data_detail",group_data_detail);
                i.putParcelableArrayListExtra("add_member_list", (ArrayList<? extends Parcelable>) add_member_list);
                startActivity(i);
            }
        });


        findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (category.equals("Category")) {
                    Toast.makeText(EditGroup.this, "Please select Category.", Toast.LENGTH_SHORT).show();
                } else {


                    if (name.getText().toString().equalsIgnoreCase("") || name.getText().toString().isEmpty()) {
                        name.setError("Name should not be empty.");
                    } else if (email.getText().toString().equalsIgnoreCase("") || email.getText().toString().isEmpty()) {
                        email.setError("Email should not be empty.");
                    } else if (phone.getText().toString().equalsIgnoreCase("") || phone.getText().toString().isEmpty()) {
                        phone.setError("phone # should not be empty.");
                    } else if (address1.getText().toString().equalsIgnoreCase("") || address1.getText().toString().isEmpty()) {
                        address1.setError("Address should not be empty.");
                    } else {

                        loading_dialog.show();
                        onEditGroupBtnClick();

                    }


                }

            }
        });


    }


    public void initialize_Rich_editor() {
        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setEditorFontSize(22);

//        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setScrollbarFadingEnabled(false);
        //mEditor.setVerticalScrollbarOverlay(true);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(8);
//        mEditor.isScrollContainer();
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);


        //mEditor.setEditorFontColor(Color.BLACK);
        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Insert text here...");
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        //mEditor.setInputEnabled(false);


        scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag=false;
                            action_right_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);



                            action_left_flag=false;
                            action_right_flag=false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag=false;
                            action_left_flag=false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });

    }

    public void initialize_member_recyclerview() {

        try {

            group_member_text = findViewById(R.id.group_member_text);
            //setting group project list
            if (group_member_list != null) {
                if (group_member_list.size() == 0) {
                    group_member_text.setVisibility(View.VISIBLE);
                } else {
                    member_recyclerView = (RecyclerView) findViewById(R.id.editGroup_member_list_recyclerview);
                    member_recyclerView.setHasFixedSize(true);

                    member_adapter = new EditGroup_MemberList_Adapter(this, group_member_list, this);
                    member_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    member_recyclerView.setAdapter(member_adapter);
                }
            } else {
                group_member_text.setVisibility(View.VISIBLE);
            }
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    public void initialize_pendingAccessRequests_recyclerview() {
        editGroup_pendingAccess_member_text = findViewById(R.id.editGroup_pendingAccess_member_text);


        try {

            if (editGroup_PendingAccessRequest_list != null) {
                if (editGroup_PendingAccessRequest_list.size() == 0) {
                    editGroup_pendingAccess_member_text.setVisibility(View.VISIBLE);
                } else {
                    //setting group project list
                    recyclerView = (RecyclerView) findViewById(R.id.editGroup_pendingAccess_list_recyclerview);
                    recyclerView.setHasFixedSize(true);

                    adapter = new EditGroup_PendingAccessRequests_Adapter(this, editGroup_PendingAccessRequest_list, this);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(adapter);
                }
            } else {
                editGroup_pendingAccess_member_text.setVisibility(View.VISIBLE);
            }


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    public void intialize_view() {

        try {

            //member list recyclerview
            initialize_member_recyclerview();
            //pending access recyclerview
            initialize_pendingAccessRequests_recyclerview();

            //intialize rich editer text
            initialize_Rich_editor();

            edit_group_name = (TextView) findViewById(R.id.edit_group_name);
            edit_group_description = (TextView) findViewById(R.id.edit_group_description);

            name = (TextView) findViewById(R.id.g_name);
            description = (TextView) findViewById(R.id.g_description);
            email = (TextView) findViewById(R.id.g_main_email);
            website = (TextView) findViewById(R.id.g_website);

            //tags=(TextView)findViewById(R.id.g_tags);

            phone = (TextView) findViewById(R.id.g_phone);
            address1 = (TextView) findViewById(R.id.g_address1);

            address2 = (TextView) findViewById(R.id.g_address2);
            city = (TextView) findViewById(R.id.g_city);
            postal_code = (TextView) findViewById(R.id.g_postal_code);
            province = (TextView) findViewById(R.id.g_province);
            country = (TextView) findViewById(R.id.g_country);
            //about_us = (TextView) findViewById(R.id.g_about_us);


            visible = (CheckBox) findViewById(R.id.g_visible_radiobtn);
            accept_donation = (CheckBox) findViewById(R.id.g_accept_donation_radiobtn);
            pending = (CheckBox) findViewById(R.id.g_pending_radiobbtn);
            passive = (CheckBox) findViewById(R.id.g_passive_radiobbtn);

            if (Common.is_Admin_flag == false) {
                accept_donation.setVisibility(View.GONE);
                pending.setVisibility(View.GONE);
                passive.setVisibility(View.GONE);
            }
            visible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        visible_status = "1";
                    } else {
                        visible_status = "0";
                    }
                }
            });

            accept_donation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        accept_status = "1";
                    } else {
                        accept_status = "0";
                    }
                }
            });

            pending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        pending_status = "1";
                    } else {
                        pending_status = "0";
                    }
                }
            });

            passive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        passive_status = "1";
                    } else {
                        passive_status = "0";
                    }
                }
            });


            Spinner spinner = (Spinner) findViewById(R.id.edit_spinner);
            //create a list of items for the spinner.
            final ArrayAdapter<String> block_spinnerArrayAdapter = new ArrayAdapter<String>(
                    this, R.layout.spinner_item, Common.cat_list) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray

                        //tv.setTextColor(Color.BLACK);

                    } else {

                        //tv.setTextSize(22);
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };
            block_spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

            spinner.setAdapter(block_spinnerArrayAdapter);
            //Set the listener for when each option is clicked.
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Change the selected item's text color
                    category = parent.getItemAtPosition(position).toString();

                    Log.d("EditGroupNew", "Category in spinner :" + category);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.pure_grey));
                    ((TextView) view).setTextSize(12);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            //setting values for first time in edit mode

            GetGroupListDataModel edit_object = group_data_detail;


            if (edit_object.getCategories__id().equals("2")) {
                //category = "Business";
                spinner.setSelection(0);
            } else if (edit_object.getCategories__id().equals("3")) {
                //category = "Charity";
                spinner.setSelection(1);
            }


            if (edit_object.getGroups__hidden().equals("0")) {
                visible.setChecked(false);
            } else {
                visible.setChecked(true);
            }

            if (edit_object.getGroups__donations().equals("0")) {
                accept_donation.setChecked(false);
            } else {
                accept_donation.setChecked(true);
            }

            if (edit_object.getGroups__pending().equals("0")) {
                pending.setChecked(false);
            } else {
                pending.setChecked(true);
            }

            if (edit_object.getGroups__passive().equals("0")) {
                passive.setChecked(false);
            } else {
                passive.setChecked(true);
            }


            edit_group_name.setText(edit_object.getGroups__name());
            edit_group_description.setText(edit_object.getGroups__description());


            name.setText(edit_object.getGroups__name());
            description.setText(edit_object.getGroups__description());
            email.setText(edit_object.getGroups__email());
            website.setText(edit_object.getGroups__website());
            phone.setText(edit_object.getGroups__phone());
            address1.setText(edit_object.getGroups__address());

            address2.setText(edit_object.getGroups__address2());
            city.setText(edit_object.getGroups__city());
            postal_code.setText(edit_object.getGroups__postal_code());
            province.setText(edit_object.getGroups__region());
            country.setText(edit_object.getGroups__country());
            //about_us.setText(edit_object.getGroups__bio());
            mEditor.setHtml(edit_object.getGroups__bio());


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



    }


    private void onGetPendingAccessRequestListFunction() {

        try {

            Call<EditGroup_PendingAccessRequests_Model> call = retrofitInterface.EditGroupPendingAccessRequests("application/json", Common.auth_token,
                    group_data_detail.getGroups__id());


            call.enqueue(new Callback<EditGroup_PendingAccessRequests_Model>() {
                @Override
                public void onResponse(Call<EditGroup_PendingAccessRequests_Model> call, Response<EditGroup_PendingAccessRequests_Model> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        EditGroup_PendingAccessRequests_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        ArrayList<EditGroup_PendingAccessRequestsData_Model> list = new ArrayList<>();
                        list = responseModel.getData();



                        if (list != null) {
                            //pd.dismiss();
                             if (editGroup_PendingAccessRequest_list !=null){
                                editGroup_PendingAccessRequest_list.clear();
                            }
                            editGroup_PendingAccessRequest_list = list;

                            onGetMemberListFunction();
                            //intialize_view();
                        }


                        //Common.ids=IDS;
                    }
                }

                @Override
                public void onFailure(Call<EditGroup_PendingAccessRequests_Model> call, Throwable t) {
                    onGetMemberListFunction();
                 }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    private void onGetMemberListFunction() {

        try {
            Call<GetGroupMemberListModel> call = retrofitInterface.getMemberListInEditGroup("application/json", Common.auth_token,
                    group_data_detail.getGroups__id());
            call.enqueue(new Callback<GetGroupMemberListModel>() {
                @Override
                public void onResponse(Call<GetGroupMemberListModel> call, Response<GetGroupMemberListModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        GetGroupMemberListModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        ArrayList<GetGroupMemberListDataModel> list = new ArrayList<>();
                        list = responseModel.getData();

                        if (list != null) {
                            //pd.dismiss();
                            loading_dialog.dismiss();

                            add_member_list = list;

                            if (group_member_list != null) {
                                if (group_member_list.size() > 0) {
                                    group_member_list.clear();
                                }
                            }

                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).getIsmember() == 1) {
                                    group_member_list.add(list.get(i));
                                }

                            }

                            intialize_view();
                        }


                    }
                }

                @Override
                public void onFailure(Call<GetGroupMemberListModel> call, Throwable t) {
                    intialize_view();
                 }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    private void onEditGroupBtnClick() {

        try {

            EditGroupRequestModel requestModel = new EditGroupRequestModel();

            if (category.equals("Businesses")) {
                category = "2";
            } else if (category.equals("Charities")) {
                category = "3";
            }
            requestModel.setCategory_id(category);
            requestModel.setName(name.getText().toString());
            requestModel.setDescription(description.getText().toString());
            requestModel.setEmail(email.getText().toString());
            requestModel.setPhone(phone.getText().toString());
            requestModel.setAddress(address1.getText().toString());
            requestModel.setAddress2(address2.getText().toString());
            requestModel.setWebsite("https:" + website.getText().toString());
            requestModel.setCity(city.getText().toString());
            requestModel.setRegion(province.getText().toString());
            requestModel.setCountry(country.getText().toString());
            requestModel.setPostal_code(postal_code.getText().toString());
            requestModel.setBio(mEditor.getHtml());  // 0 due to some issues
            requestModel.setHidden(visible_status);
            requestModel.setPending(pending_status);
            requestModel.setVote_decision("");
            requestModel.setDonations(accept_status);
            requestModel.setPassive(passive_status);
            requestModel.setUser_id(Common.login_data.getData().getId());

            String is_admin;
            if (Common.is_Admin_flag==true) {
                is_admin = "1";
            } else {
                is_admin = "0";
            }

            requestModel.setIs_admin(is_admin);
            requestModel.setGroup_id(group_data_detail.getGroups__id());


            Call<AddGroupResponseModel> call = retrofitInterface.EditGroup("application/json", Common.auth_token,
                    requestModel);

            call.enqueue(new Callback<AddGroupResponseModel>() {
                @Override
                public void onResponse(Call<AddGroupResponseModel> call, Response<AddGroupResponseModel> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        AddGroupResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {
                            loading_dialog.dismiss();
                            Common.Toast_Message(EditGroup.this,"Group updated successfully");
                            Intent i=new Intent(EditGroup.this,GroupsMainScreen.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            //Confirm_Dialog("Group", "Record updated successfully", "OK");
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<AddGroupResponseModel> call, Throwable t) {

                }

            });


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        // custom dialog
        final Dialog dialog = new Dialog(EditGroup.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(EditGroup.this, GroupsMainScreen.class);
                startActivity(intent);
                finish();
            }
        });


        dialog.show();
    }

    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {

        String group_id = group_data_detail.getGroups__id();
        String user_id = group_member_list.get(position).getId();

        try {
            switch (v.getId()) {
                case R.id.remove_member:

                    loading_dialog.show();
                    onRemoveMemberToGroupBtnClick(user_id, group_id);
                    break;

                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    private void onRemoveMemberToGroupBtnClick(String user_id, String group_id) {

        try {
            RemoveMemberFromGroupRequestModel requestModel = new RemoveMemberFromGroupRequestModel();

            requestModel.setUser_id(user_id);
            requestModel.setGroup_id(group_id);


            Call<RemoveMemberFromGroupResponseMOdel> responseCall = retrofitInterface.RemoveMemberInGroup("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<RemoveMemberFromGroupResponseMOdel>() {
                @Override
                public void onResponse(Call<RemoveMemberFromGroupResponseMOdel> call, Response<RemoveMemberFromGroupResponseMOdel> response) {

                    //loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        String status, message;

                        //loading_dialog.dismiss();

                        RemoveMemberFromGroupResponseMOdel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        onGetMemberListFunction();
                        Toast.makeText(EditGroup.this, "User removed successfully.", Toast.LENGTH_LONG).show();

                    } else {

                        onGetMemberListFunction();
                        Toast.makeText(EditGroup.this, "Member remove from group Failed.", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<RemoveMemberFromGroupResponseMOdel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(EditGroup.this, "Member remove from group Failed", Toast.LENGTH_LONG).show();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }


    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}

