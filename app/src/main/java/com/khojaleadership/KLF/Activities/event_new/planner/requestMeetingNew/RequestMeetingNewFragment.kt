package com.khojaleadership.KLF.Activities.event_new.planner.requestMeetingNew

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.khojaleadership.KLF.Activities.event_new.datePicker.DatePickerDialogFragment
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsViewModel
import com.khojaleadership.KLF.Activities.event_new.personSelection.PersonSelectionActivity
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Activities.event_new.timePicker.TimePickerDialogFragment
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.FragmentRequestMeetingNewBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class RequestMeetingNewFragment : Fragment(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private val viewModel: RequestMeetingNewViewModel by viewModels()
    private val parentViewModel: MeetingDetailsViewModel by activityViewModels()

    lateinit var binding: FragmentRequestMeetingNewBinding

    var scrolling = false

    var previousTokenInputMotionEventAction: Int? = null

    var clickedTimeEditText: EditText? = null

    private lateinit var timeSlotSpinnerAdapter: ArrayAdapter<EventTimeSlotUiModel>
    private lateinit var daysSpinnerAdapter: ArrayAdapter<EventDaysUiModel>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentRequestMeetingNewBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getDataFromIntent(requireActivity().intent)
        viewModel.getAllDelegates()
        setUpViews()
        viewModel.dayMilliseconds = 0
        viewModel.endTimeMilliseconds = 0
        viewModel.startTimeMilliseconds = 0
        setUpObservers()
    }

    private fun setUpObservers() {

        viewModel.selectedDelegates.observe(viewLifecycleOwner, Observer {
            binding.tokenAutocomplete.setText("")
            it?.let {
                val adapter: ArrayAdapter<PersonDetailUiModel> = ArrayAdapter<PersonDetailUiModel>(requireContext(), android.R.layout.simple_dropdown_item_1line, it)

                binding.tokenAutocomplete.setAdapter(adapter)
                it.forEach {
                    binding.tokenAutocomplete.addObjectAsync(it)
                }
            }
        })


//        parentViewModel.spinnerDaysData.observe(viewLifecycleOwner, Observer {
//            it?.let {
//
//                if (it.isEmpty()) {
////                    viewModel.resetUsers()
//                }
//                Timber.e("spinnerDate, ${it.size}")
//                showHideSpinners(show = it.isNotEmpty())
//                daysSpinnerAdapter.clear()
//                daysSpinnerAdapter.addAll(it)
//                binding.spinnerDate.adapter = daysSpinnerAdapter
//
//            }
//        })


        viewModel.messageEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let { message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.eventState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    parentViewModel.setEventState(it)
                }
                is Result.Success -> {
                    parentViewModel.showMeetingsFragment()
                    parentViewModel.setEventState(it)
                }
                is Result.Error -> {
                    parentViewModel.setEventState(it)
                }
            }
        })


        viewModel.multipleMeetingSentErrorEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                showErrorDialog(it)
            }
        })

        parentViewModel.updateDelegateSelectionEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                viewModel.updateDelegateSelection()
            }
        })

        parentViewModel.editMeetingEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {

                val allDelegates = viewModel.allDelegates?.map {
                    it.copy(isSelected = false)
                }
                it.subItems.forEach { item ->
                    allDelegates?.find { it.userId == item.personDetail.userId }?.isSelected = true
                }
                viewModel.authStore.delegatesToSelect = allDelegates
                viewModel.updateDelegateSelection()
                viewModel.meetingId = it.meetingId
                binding.btnSubmit.text = "Update"
                binding.edittextMeetingNotes.setText(it.meetingNotes)

                try {

                    binding.etDate.setText(it.date)

                    var dateStr = removeStThRdNd(it.date)
                    val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
                    val parsedDate = dateFormat.parse(dateStr)
                    viewModel.dayMilliseconds = parsedDate.time

                    val timeFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
                    val parsedStartTime = timeFormat.parse(it.startTime)
                    viewModel.startTimeMilliseconds = parsedStartTime.time

                    val parsedEndTime = timeFormat.parse(it.endTime)
                    viewModel.endTimeMilliseconds = parsedEndTime.time

                    val timeAmPmFormat = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                    binding.etEndTime.setText(timeAmPmFormat.format(Date(viewModel.endTimeMilliseconds)))
                    binding.etStartTime.setText(timeAmPmFormat.format(Date(viewModel.startTimeMilliseconds)))

                } catch (exception: Exception) {
                    Toast.makeText(requireContext(), "Parsing failed", Toast.LENGTH_SHORT).show()
                }
            }
        })

        parentViewModel.refreshRequestMeetingEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let {
                viewModel.meetingId = null
                viewModel.deselectAllDelegates()
                binding.edittextMeetingNotes.setText("")
                binding.etDate.setText("")
                binding.etStartTime.setText("")
                binding.etEndTime.setText("")
                binding.btnSubmit.setText("Submit")
            }
        })
    }

    private fun removeStThRdNd(date: String): String {

        return date.replace(regex = Regex("(?<=\\d)(rd|st|nd|th)\\b"), replacement = "")
    }

    private fun showErrorDialog(message: String) {
        val dialog = AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setPositiveButton("OK") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
        }
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpViews() {
        timeSlotSpinnerAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner_simple)
        binding.spinnerTime.adapter = timeSlotSpinnerAdapter
        binding.spinnerTime.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                Timber.e("OnItemSelected, $position")
//                parentViewModel.selectedDaySlotIndex?.let { dayIndex ->
//                    val timeSlotModel = parentViewModel.spinnerTimeData.value?.getOrNull(dayIndex)?.getOrNull(position)
//                    if (parentViewModel.selectedTimeSlot?.programDetailsId != timeSlotModel?.programDetailsId) {
//
//                        parentViewModel.selectedTimeSlot = timeSlotModel
//
//                    }
//
//                }
            }
        }



        daysSpinnerAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner_simple_2)
        binding.spinnerDate.adapter = daysSpinnerAdapter

        binding.spinnerDate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                Timber.e("$position")
//                parentViewModel.selectedDaySlotIndex = position
//                parentViewModel.spinnerTimeData.value?.let {
//                    it.getOrNull(position)?.let { list ->
//                        timeSlotSpinnerAdapter.clear()
//                        timeSlotSpinnerAdapter.addAll(list)
//                        if (list.isNotEmpty()) {
//                            binding.spinnerTime.adapter = timeSlotSpinnerAdapter
//                            parentViewModel.clickedRequestMeetingProgramDetailsId?.let { programDetailsId ->
//                                val timeIndex = parentViewModel.spinnerTimeData.value?.get(position)?.indexOfFirst {
//                                    it.programDetailsId == programDetailsId
//                                }
//                                parentViewModel.clickedRequestMeetingProgramDetailsId = null
//                                timeIndex?.let {
//                                    if (timeIndex != -1) {
//                                        binding.spinnerTime.setSelection(timeIndex)
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Timber.e("spinnerDate, Nothing")
            }
        }



        binding.tokenAutocomplete.allowCollapse(false)


        binding.tokenAutocomplete.isFocusable = false
        binding.tokenAutocomplete.isClickable = true

        binding.tokenAutocomplete.setOnClickListener {
            if (scrolling) {
                scrolling = false
            } else {
                val intent = Intent(requireContext(), PersonSelectionActivity::class.java)
                viewModel.authStore.delegatesToSelect = viewModel.allDelegates
                requireContext().startActivity(intent)
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.tokenAutocomplete.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
//                if (scrollY == 0 || scrollY > view?.measuredHeight ?: Int.MAX_VALUE) {
//                    scrolling = false
//                }else{
                scrolling = true
                lifecycleScope.launch {
                    delay(300)
                    scrolling = false
                }
//                }
            }
        }

        binding.btnSubmit.setOnClickListener {
            if (validate()) {
                viewModel.meetingId?.let {
                    viewModel.updateMeetingRequest(
                            meetingNotes = binding.edittextMeetingNotes.text?.toString() ?: "",
                            meetingEndTime = binding.etEndTime.text?.toString() ?: "",
                            meetingStartTime = binding.etStartTime.text?.toString() ?: ""
                    )
                } ?: run{
                    viewModel.sendMeetingRequest(
                            meetingNotes = binding.edittextMeetingNotes.text?.toString() ?: "",
                            meetingEndTime = binding.etEndTime.text?.toString() ?: "",
                            meetingStartTime = binding.etStartTime.text?.toString() ?: ""
                    )
                }
            }
        }


        binding.etDate.setOnClickListener {
            showDatePickerDialog()
        }
        binding.etEndTime.setOnClickListener {
            clickedTimeEditText = binding.etEndTime
            showTimePickerDialog()
        }
        binding.etStartTime.setOnClickListener {
            clickedTimeEditText = binding.etStartTime
            showTimePickerDialog()
        }

//        binding.edittextMeetingNotes.setOnTouchListener { view, event ->
//            view.parent.requestDisallowInterceptTouchEvent(true)
//            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP && view.hasFocus()) {
//                view.parent.requestDisallowInterceptTouchEvent(false)
//            }
//            return@setOnTouchListener false
//        }


    }

    private fun validate(): Boolean {
        if (binding.etDate.text.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select date.", Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.etStartTime.text.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select start time.", Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.etEndTime.text.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select end time.", Toast.LENGTH_SHORT).show()
            return false
        }
        if (viewModel.selectedDelegates.value.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select delegates.", Toast.LENGTH_SHORT).show()
            return false
        }
        if (binding.edittextMeetingNotes.text.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please enter meeting notes.", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }


    private fun showDatePickerDialog() {
        val datePicker = DatePickerDialogFragment()
        datePicker.setTargetFragment(this@RequestMeetingNewFragment, 0)
        if(!binding.etDate.text.isNullOrBlank()){
            val bundle = Bundle()
            bundle.putLong(DatePickerDialogFragment.TIMESTAMP, viewModel.dayMilliseconds)
            datePicker.arguments = bundle
        }
        datePicker.show(requireActivity().supportFragmentManager, "date picker")
    }

    private fun showTimePickerDialog() {
        val timePicker = TimePickerDialogFragment()
        timePicker.setTargetFragment(this@RequestMeetingNewFragment, 0)

        if(clickedTimeEditText == binding.etEndTime){
            if(!binding.etEndTime.text.isNullOrBlank()){
                val bundle = Bundle()
                bundle.putLong(TimePickerDialogFragment.TIMESTAMP, viewModel.endTimeMilliseconds)
                timePicker.arguments = bundle
            }
        }else{
            if(!binding.etStartTime.text.isNullOrBlank()){
                val bundle = Bundle()
                bundle.putLong(TimePickerDialogFragment.TIMESTAMP, viewModel.startTimeMilliseconds)
                timePicker.arguments = bundle
            }
        }

        timePicker.show(requireActivity().supportFragmentManager, "time picker")
    }

    private fun showHideSpinners(show: Boolean) {
        if (show) {
            binding.spinnerDate.visibility = View.VISIBLE
            binding.spinnerTime.visibility = View.VISIBLE
        } else {
            binding.spinnerDate.visibility = View.GONE
            binding.spinnerTime.visibility = View.GONE
        }
    }

    override fun onDateSet(datePicker: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        viewModel.dayMilliseconds = calendar.timeInMillis

        val date = SimpleDateFormat("EEE, dd'${Common.getDayNumberSuffix(dayOfMonth)}' MMM yyyy", Locale.getDefault())
                .format(Date(calendar.timeInMillis))
        binding.etDate.setText(date)
    }

    override fun onTimeSet(timePicker: TimePicker?, hourOfDay: Int, minute: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)

        if(clickedTimeEditText == binding.etEndTime){
            viewModel.endTimeMilliseconds = calendar.timeInMillis
        }else{
            viewModel.startTimeMilliseconds = calendar.timeInMillis
        }

        val time = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                .format(Date(calendar.timeInMillis))
        clickedTimeEditText?.setText(time)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                RequestMeetingNewFragment()
    }
}