package com.khojaleadership.KLF.Activities.Setting;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.khojaleadership.KLF.Activities.Splash_Login.LoginActivity;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Setting.ChangePasswordRequestModel;
import com.khojaleadership.KLF.Model.Setting.ChangePasswordResponseModel;
import com.khojaleadership.KLF.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Change_Password extends AppCompatActivity {

    Dialog loading_dialog;

    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView old_password, new_password, confirm_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__password);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Change Password");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Change_Password.this);

                finish();
            }
        });


        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //closeKeyboard();
        loading_dialog = Common.LoadingDilaog(this);

        old_password = (EditText) findViewById(R.id.old_password);
        new_password = (EditText) findViewById(R.id.new_password);
        //confirm_password = (EditText) findViewById(R.id.confirm_password);


        findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (old_password.getText().toString().isEmpty() ||
                        old_password.getText().toString().equalsIgnoreCase("")) {
                    old_password.setError("This field should not be empty.");
                } else if (new_password.getText().toString().isEmpty() ||
                        new_password.getText().toString().equalsIgnoreCase("")) {
                    new_password.setError("This field should not be empty.");
                } else if (!((new_password.getText().toString()).equals(old_password.getText().toString()))) {
                    Common.Toast_Message(Change_Password.this, "Password Does not match");
                } else {
                    printPasswordStats(new_password.getText().toString());
                }


            }
        });


    }


    public void printPasswordStats(String password) {
        int numOfSpecial = 0;
        int numOfLetters = 0;
        int numOfUpperLetters = 0;
        int numOfLowerLetters = 0;
        int numOfDigits = 0;

        byte[] bytes = password.getBytes();
        for (byte tempByte : bytes) {
            if (tempByte >= 33 && tempByte <= 47) {
                numOfSpecial++;
            }

            char tempChar = (char) tempByte;
            if (Character.isDigit(tempChar)) {
                numOfDigits++;
            }

            if (Character.isLetter(tempChar)) {
                numOfLetters++;
            }

            if (Character.isUpperCase(tempChar)) {
                numOfUpperLetters++;
            }

            if (Character.isLowerCase(tempChar)) {
                numOfLowerLetters++;
            }
        }

        if (numOfLetters + numOfDigits + numOfSpecial >= 6) {
            if (numOfDigits >= 1 && numOfUpperLetters >= 1) {
                loading_dialog.show();
                onChnagePasswordBtnClick(old_password.getText().toString(), new_password.getText().toString());
            } else {
                Common.Toast_Message(Change_Password.this, "Password must be contain at least 6 character, Including one Capital letter one number");
            }
        } else {
            Common.Toast_Message(Change_Password.this, "Password must be contain at least 6 character, Including one Capital letter one number");
        }

    }

    private void onChnagePasswordBtnClick(String new_password, String confirm_password) {

        ChangePasswordRequestModel requestModel = new ChangePasswordRequestModel();

        requestModel.setUser_id(Common.login_data.getData().getId());
        requestModel.setNewpassword(new_password);
        requestModel.setConfirmpassword(confirm_password);


        Call<ChangePasswordResponseModel> responseCall = retrofitInterface.ChangePassword("application/json", Common.auth_token, requestModel);

        responseCall.enqueue(new Callback<ChangePasswordResponseModel>() {
            @Override
            public void onResponse(Call<ChangePasswordResponseModel> call, Response<ChangePasswordResponseModel> response) {

                if (response.isSuccessful()) {


                    String status, message;

                    ChangePasswordResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("success")) {
                        loading_dialog.dismiss();
                        Toast.makeText(Change_Password.this, "Password Change Successfully.", Toast.LENGTH_SHORT).show();

                        Intent login = new Intent(Change_Password.this, LoginActivity.class);
                        startActivity(login);
                        finish();
                    } else {
                        Toast.makeText(Change_Password.this, "Password Change failed.", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(Change_Password.this, "Password Change failed.", Toast.LENGTH_SHORT).show();
                 }


            }

            @Override
            public void onFailure(Call<ChangePasswordResponseModel> call, Throwable t) {
                 Toast.makeText(Change_Password.this, "Password Change failed.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
