package com.khojaleadership.KLF.Activities.Project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Project.Add_Percentage_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.AddPercentageRequestModel;
import com.khojaleadership.KLF.Model.Project_Models.AddPercentageResponseModel;
import com.khojaleadership.KLF.Model.Project_Models.Percentage_List_Data_Model;
import com.khojaleadership.KLF.Model.Project_Models.Percentage_List_Model;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Percentage_Activity extends AppCompatActivity {


    RecyclerView recyclerView;
    Add_Percentage_Adapter adapter;

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    String group_ids="",percentage_values="";

    StringBuffer percentage_group_id_list;
    StringBuffer percentage_value_list;


    TextView header_title;

    //String ProjectId;

    ProjectsDataModel projects_data_detail = new ProjectsDataModel();

    ArrayList<Percentage_List_Data_Model> percentage_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__percentage_);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        header_title = (TextView) findViewById(R.id.c_header_title);
        header_title.setText("Percentage");

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Percentage_Activity.this);

                finish();
            }
        });

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        Intent intent = getIntent();
        projects_data_detail = intent.getParcelableExtra("ProjectData");

//        Bundle bundle=getIntent().getExtras();
//        ProjectId=bundle.getString("ProjectId");

        GetPercentageList();

        //save percentage
        findViewById(R.id.percentage_save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //getting string of group id's
                if (Common.percentage_group_id_list.size()>0) {
                    percentage_group_id_list = new StringBuffer(Common.percentage_group_id_list.get(0));

                    for (int i=1;i<Common.percentage_group_id_list.size();i++){
                        percentage_group_id_list.append(",");
                        percentage_group_id_list.append(Common.percentage_group_id_list.get(i));
                    }

                    group_ids=percentage_group_id_list.toString();
                }



                double percentage_total_value=0;

                //getting string of percentage values
                if (Common.percentage_value_list.size()>0) {
                    percentage_value_list = new StringBuffer(Common.percentage_value_list.get(0));
                    percentage_total_value=percentage_total_value+Double.valueOf(Common.percentage_value_list.get(0));
                    for (int i=1;i<Common.percentage_value_list.size();i++){
                        percentage_value_list.append(",");
                        percentage_value_list.append(Common.percentage_value_list.get(i));
                        //to calculate
                        if (Common.percentage_value_list.get(i)!=null) {
                            if (!(Common.percentage_value_list.get(i).equals("")) || !(Common.percentage_value_list.get(i).equalsIgnoreCase(""))) {
                                percentage_total_value = percentage_total_value + Double.valueOf(Common.percentage_value_list.get(i));
                            }
                        }
                    }

                    percentage_values=percentage_value_list.toString();
                }



                if (percentage_total_value==100) {
                    loading_dialog.show();
                    //total percentage always 100 ho gii
                    SavePercentage(projects_data_detail.getProjects__id(), group_ids, "100");
                }else {
                    Toast.makeText(Add_Percentage_Activity.this,"Percentage total value should be in 100",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    public void intialize_view(){
        recyclerView= (RecyclerView) findViewById(R.id.percentage_recyclerview);

        //getting data
//        getData();

        adapter = new Add_Percentage_Adapter(this,percentage_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }



    private void GetPercentageList() {
        Call<Percentage_List_Model> responseCall = retrofitInterface.GetPercentageList("application/json",Common.auth_token,projects_data_detail.getProjects__id());

        responseCall.enqueue(new Callback<Percentage_List_Model>() {
            @Override
            public void onResponse(Call<Percentage_List_Model> call, Response<Percentage_List_Model> response) {

                loading_dialog.dismiss();

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    Percentage_List_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();
                    if (status.equals("error")){
                        //data is null
                        Toast.makeText(Add_Percentage_Activity.this,"Server is not responding at the moment",Toast.LENGTH_SHORT).show();
                    }else if (status.equals("success")){

                        ArrayList<Percentage_List_Data_Model> list=responseModel.getData();

                        percentage_list=list;

                        intialize_view();
                    }


                } else {
                    Toast.makeText(Add_Percentage_Activity.this,"Server is not responding at the moment",Toast.LENGTH_SHORT).show();
                 }



            }

            @Override
            public void onFailure(Call<Percentage_List_Model> call, Throwable t) {
                loading_dialog.dismiss();
             }
        });


    }

    private void SavePercentage(String project_id,String group_id,String percentage) {

        AddPercentageRequestModel requestModel=new AddPercentageRequestModel();

        requestModel.setProject_id(project_id);
        requestModel.setGroups_id(group_id);
        requestModel.setPercentagevalue(percentage);



        Call<AddPercentageResponseModel> responseCall = retrofitInterface.AddPercentage("application/json",Common.auth_token,requestModel);

        responseCall.enqueue(new Callback<AddPercentageResponseModel>() {
            @Override
            public void onResponse(Call<AddPercentageResponseModel> call, Response<AddPercentageResponseModel> response) {

                loading_dialog.dismiss();

                if (response.isSuccessful()) {

                    String status, message;

                    AddPercentageResponseModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("error")){
                        //data is null
                        Toast.makeText(Add_Percentage_Activity.this,"Percentage added failed",Toast.LENGTH_SHORT).show();
                    }else if (status.equals("success")){

                        Toast.makeText(Add_Percentage_Activity.this,"Percentage added successfully",Toast.LENGTH_SHORT).show();
                        //GetPercentageList();
                        Intent i=new Intent(Add_Percentage_Activity.this,Edit_Project_Activity.class);
                        i.putExtra("ProjectData",projects_data_detail);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }


                } else {
                    Toast.makeText(Add_Percentage_Activity.this,"Server is not responding at the moment",Toast.LENGTH_SHORT).show();
                 }



            }

            @Override
            public void onFailure(Call<AddPercentageResponseModel> call, Throwable t) {
                loading_dialog.dismiss();
                 }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
