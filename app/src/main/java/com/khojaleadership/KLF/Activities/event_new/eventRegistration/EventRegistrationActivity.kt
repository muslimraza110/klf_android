package com.khojaleadership.KLF.Activities.event_new.eventRegistration

import android.graphics.Bitmap
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.webkit.*
import android.widget.Toast
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.databinding.ActivityEventRegistrationBinding

class EventRegistrationActivity : AppCompatActivity() {


    lateinit var binding: ActivityEventRegistrationBinding

    var registrationUrl = Common.LIVE_BASE_URL + "/summitEvents/registration_page.json"

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityEventRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setUpViews()
    }

    private fun setUpViews() {

        binding.webview.apply {
            setBackgroundColor(Color.TRANSPARENT)
            webViewClient = object : WebViewClient(){
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    binding.progressBar.visibility = View.VISIBLE
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    binding.progressBar.visibility = View.GONE
                }

                override fun onReceivedError(
                        view: WebView?,
                        request: WebResourceRequest?,
                        error: WebResourceError?
                ) {
                    super.onReceivedError(view, request, error)
                    Toast.makeText(context,"Cannot load page.", Toast.LENGTH_SHORT).show()
                    binding.progressBar.visibility = View.GONE
                }

            }
            settings.domStorageEnabled = true
            settings.setAppCacheEnabled(true)
            settings.loadsImagesAutomatically = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            loadUrl(registrationUrl)
        }
        binding.imageBack.setOnClickListener {
            finish()
        }

    }

}