package com.khojaleadership.KLF.Activities.event_new.notification

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.Gson
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.ResponseStatus
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.Helper.launchSafely
import com.khojaleadership.KLF.Model.event_new.PlannerNotificationsResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class PlannerNotificationsViewModel(application: Application) : AndroidViewModel(application)  {


    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")

    private val _notifications: MutableLiveData<List<PlannerNotificationUiModel>> = MutableLiveData()
    val notifications: LiveData<List<PlannerNotificationUiModel>> = _notifications

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    init {
        getNotifications()
    }


    private fun getNotifications() {

        _eventState.value = Result.Loading

        Common.login_data?.data?.id?.let { userId ->
            viewModelScope.launchSafely(
                    block = {

                        val response = eventsRepository.getPlannerNotifications(
                                userId = userId,
                                eventId = authStore.event?.eventId ?: ""
                        )

                        if(response.status == ResponseStatus.SUCCESS){
                            response.data?.let { list->
                                val tempList = list.map {
                                    mapToPlannerNotificationUiModel(it)
                                }.toMutableList()

                                _notifications.value = tempList

                                _eventState.value = Result.Success(
                                        message = response.message,
                                        data = Unit
                                )
                            } ?:run {
                                _notifications.value = listOf()
                                _eventState.value = Result.Success(
                                        message = response.message ?: Common.noRecordFoundMessage,
                                        data = Unit
                                )
                            }
                        }
                        else{
                            _eventState.value = Result.Error(
                                    message = response.message ?: Common.somethingWentWrongMessage
                            )
                        }

                    },
                    error = {
                        Timber.e(it)
                        _eventState.value = Result.Error(
                                message = Common.somethingWentWrongMessage
                        )
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(
                    message = Common.idNotFoundMessage
            )
        }

    }


    private fun mapToPlannerNotificationUiModel(data: PlannerNotificationsResponse): PlannerNotificationUiModel{
        return PlannerNotificationUiModel(
                eventDay = data.data?.event_day_index ?: 0,
                notificationString = data.description ?: "",
                hasRead = false,
                time = data.dateTime ?: ""
        )
    }
}