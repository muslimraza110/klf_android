package com.khojaleadership.KLF.Activities.Forum_Section;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.Forum.SendEblast_Screen4_Adapter;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForSendEblast;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Email_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;

public class SendEblastScreen4 extends AppCompatActivity implements RecyclerViewClickListner {
    SendEblast_Screen4_Adapter adapter;
    RecyclerView recyclerView;
    TextView header_title;
    TextView no_found;
    EditText filter_edit;

    public Boolean filter_flag = false;

    ArrayList<Send_Eblast_Child_Data_Model> sourceDataList = new ArrayList<>();
    ArrayList<Send_Eblast_Child_Data_Model> filterDataList = new ArrayList<>();

    private PinyinComparatorForSendEblast pinyinComparator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_eblast_screen4);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        ------------Exception handling------------------
        try {


            findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.hideKeyboard(v, SendEblastScreen4.this);

                    Common.send_eblast_email_list = Common.send_eblast_email_list_copy;


                    finish();
                }
            });

            Bundle bundle=getIntent().getExtras();
            String title=bundle.getString("ContactName");


            header_title = findViewById(R.id.c_header_title);
            //header_title.setText(Common.contact_name);
            header_title.setText(title);


            no_found = findViewById(R.id.no_found);

            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(false);


            pinyinComparator = new PinyinComparatorForSendEblast();
            sourceDataList = filledData(Common.send_eblast_child_data_list);
            Collections.sort(sourceDataList, pinyinComparator);


            if (Common.send_eblast_child_data_list != null) {
                if (Common.send_eblast_child_data_list.size() != 0) {

                    adapter = new SendEblast_Screen4_Adapter(sourceDataList, this, this);
                    recyclerView.setAdapter(adapter);
                } else {
                    no_found.setVisibility(View.VISIBLE);
                }

            } else {
                no_found.setVisibility(View.VISIBLE);
            }

            filter_edit = findViewById(R.id.filter_edit);
            filter_edit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    filterData(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            findViewById(R.id.done_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }

    private ArrayList<Send_Eblast_Child_Data_Model> filledData(ArrayList<Send_Eblast_Child_Data_Model> data) {

        ArrayList<Send_Eblast_Child_Data_Model> mSortList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            Send_Eblast_Child_Data_Model sortModel = new Send_Eblast_Child_Data_Model();


            String letters;//Display the initials of the pinyin
            String UserID, FirstName, LastName, Email, ProfileURL;
            String is_checked;

            sortModel.setUserID(data.get(i).getUserID());
            sortModel.setFirstName(data.get(i).getFirstName());
            sortModel.setLastName(data.get(i).getLastName());
            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setProfileURL(data.get(i).getProfileURL());
            sortModel.setIs_checked(data.get(i).getIs_checked());


            String pinyin_english = data.get(i).getFirstName();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {

                 sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();
                 if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }

        return mSortList;

    }


    private void filterData(String filterStr) {

        try {

            filter_flag = true;

            if (TextUtils.isEmpty(filterStr)) {
                filterDataList = sourceDataList;
                sourceDataList = filledData(Common.send_eblast_child_data_list);
                Collections.sort(sourceDataList, pinyinComparator);
            } else {

                filterDataList.clear();
                 for (Send_Eblast_Child_Data_Model sortModel : sourceDataList) {

                    String FirstName = sortModel.getFirstName();

                    if (FirstName.indexOf(filterStr.toString()) != -1
                            || FirstName.toLowerCase().contains(filterStr)
                            || FirstName.toUpperCase().contains(filterStr)) {

                        filterDataList.add(sortModel);
                    }

                }
            }

            Collections.sort(filterDataList, pinyinComparator);

            adapter.updateList(filterDataList);


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
            Log.d("NullExceptionIssue", "onRowClick: " + e.getMessage());
        }


    }

    @Override
    public void onRowClick(int position) {

        String is_checked_flag = "";
        if (filter_flag == true) {

            is_checked_flag = filterDataList.get(position).getIs_checked();
         } else {

            is_checked_flag = sourceDataList.get(position).getIs_checked();
        }


        if (is_checked_flag.equals("0")) {
            //email add kii
            Send_Eblast_Email_Model email = new Send_Eblast_Email_Model();
            email.setEmail(Common.send_eblast_child_data_list.get(position).getEmail());
            email.setId(Common.send_eblast_child_data_list.get(position).getUserID());
            Common.send_eblast_email_list.add(email);


            //data me flag 1 kia
            String UserID, FirstName, LastName, Email, ProfileURL;
            String is_checked;

            UserID = Common.send_eblast_child_data_list.get(position).getUserID();
            FirstName = Common.send_eblast_child_data_list.get(position).getFirstName();
            LastName = Common.send_eblast_child_data_list.get(position).getLastName();
            Email = Common.send_eblast_child_data_list.get(position).getEmail();
            ProfileURL = Common.send_eblast_child_data_list.get(position).getProfileURL();
            is_checked = "1";

            Send_Eblast_Child_Data_Model item = new Send_Eblast_Child_Data_Model("", UserID, FirstName, LastName, Email, ProfileURL, is_checked);
            Common.send_eblast_child_data_list.set(position, item);


        } else if (is_checked_flag.equals("1")) {


            if (Common.send_eblast_child_data_list != null) {

                for (int i = 0; i < Common.send_eblast_email_list.size(); i++) {

                    if ((Common.send_eblast_child_data_list.get(position).getUserID()).equals(Common.send_eblast_email_list.get(i).getId())) {
                        //email remove kii
                        Common.send_eblast_email_list.remove(i);
                        //list me flag 0 kia
                        String UserID, FirstName, LastName, Email, ProfileURL;
                        String is_checked;

                        UserID = Common.send_eblast_child_data_list.get(position).getUserID();
                        FirstName = Common.send_eblast_child_data_list.get(position).getFirstName();
                        LastName = Common.send_eblast_child_data_list.get(position).getLastName();
                        Email = Common.send_eblast_child_data_list.get(position).getEmail();
                        ProfileURL = Common.send_eblast_child_data_list.get(position).getProfileURL();
                        is_checked = "0";
                        Send_Eblast_Child_Data_Model item = new Send_Eblast_Child_Data_Model("", UserID, FirstName, LastName, Email, ProfileURL, is_checked);
                        Common.send_eblast_child_data_list.set(position, item);

                    }
                }
            }
        }

         if (Common.send_eblast_email_list.size() > 0) {

            for (int i = 0; i < Common.send_eblast_email_list.size(); i++) {
             }
        }

    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //back press py data change na ho
        Common.send_eblast_email_list = Common.send_eblast_email_list_copy;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
