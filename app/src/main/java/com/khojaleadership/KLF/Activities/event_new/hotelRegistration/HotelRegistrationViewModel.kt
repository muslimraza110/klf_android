package com.khojaleadership.KLF.Activities.event_new.hotelRegistration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HotelRegistrationViewModel : ViewModel() {

    private val _hotelPackages: MutableLiveData<List<HotelPackagesUiModel>> = MutableLiveData()
    val hotelPackages: LiveData<List<HotelPackagesUiModel>> = _hotelPackages


    private val _orderSummary: MutableLiveData<List<OrderSummaryUiModel>> = MutableLiveData()
    val orderSummary: LiveData<List<OrderSummaryUiModel>> = _orderSummary


    fun fetchData() {
        val tempList = listOf<HotelPackagesUiModel>(
                HotelPackagesUiModel(),
                HotelPackagesUiModel(),
                HotelPackagesUiModel(),
                HotelPackagesUiModel()
        )

        val tempList2 = listOf<OrderSummaryUiModel>(
                OrderSummaryUiModel(),
                OrderSummaryUiModel(),
                OrderSummaryUiModel(),
                OrderSummaryUiModel()
        )

        _hotelPackages.value = tempList
        _orderSummary.value = tempList2

    }
}