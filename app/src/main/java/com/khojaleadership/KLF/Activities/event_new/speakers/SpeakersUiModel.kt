package com.khojaleadership.KLF.Activities.event_new.speakers

import android.content.Context
import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemProfileBinding
import com.khojaleadership.KLF.databinding.LayoutListGroupHeaderBinding
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractExpandableHeaderItem
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.ExpandableViewHolder
import java.util.*

class SpeakersUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val userId: String,
        val summitEventsFacultyId: String,
        val name: String,
        val designation: String,
        val description: String,
        val facebook: String,
        val twitter: String,
        val linkedIn: String,
        val photoUrl: String,
        val isSpeaker: Boolean,
        val isDelegate: Boolean,
        val time: String,
        val date: String,
//        val availableSlotData: List<SummitEventDayAvailability>?,
        val onClick: (SpeakersUiModel) -> Unit,
        val onSocialIconClick: (String) -> Unit,
        var header: SpeakerHeaderItem,
        var context: Context
) : AbstractSectionableItem<SpeakersUiModel.ViewHolder, SpeakerHeaderItem>(
        header
) {


    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_profile
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                binding = ItemProfileBinding.bind(view),
                context = context
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }


    class ViewHolder(
            val binding: ItemProfileBinding,
            var context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SpeakersUiModel) {
            item.apply {
                binding.tvName.text = name

                binding.tvDesignation.text = designation
                binding.tvSummary.text = description
                binding.tvTime.text = time
                binding.tvDate.text = date

                Glide.with(context)
                        .load(photoUrl)
                        .centerCrop()
                        .into(binding.userImage)
                binding.root.setOnClickListener {
                    onClick.invoke(this)
                }
                binding.linkedInIcon.setOnClickListener {
                    onSocialIconClick.invoke(linkedIn)
                }
                binding.fbIcon.setOnClickListener {
                    onSocialIconClick.invoke(facebook)
                }

                if(linkedIn.trim().isEmpty()){
                    binding.linkedInIcon.isEnabled = false
                    binding.linkedInIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appLightGrey))
                }else{
                    binding.linkedInIcon.isEnabled = true
                    binding.linkedInIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appCyan))
                }

                if(facebook.trim().isEmpty()){
                    binding.fbIcon.isEnabled = false
                    binding.fbIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appLightGrey))
                }else{
                    binding.fbIcon.isEnabled = true
                    binding.fbIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appCyan))
                }
            }
        }

    }

}


class SpeakerHeaderItem(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        var title: String,
        var isOpened: Boolean
) : AbstractExpandableHeaderItem<SpeakerHeaderItem.ViewHolder, SpeakersUiModel>() {


    override fun isExpanded(): Boolean {
        return isOpened
    }

    init {
//        isExpanded = isOpened
//        setHidden(false);
//        setExpanded(true);
//        setSelectable(false);
    }

    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.layout_list_group_header
    }

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): ViewHolder {
        return ViewHolder(
                LayoutListGroupHeaderBinding.bind(view), adapter
        )
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder?.bind(this)
    }

    class ViewHolder(
            val binding: LayoutListGroupHeaderBinding,
            var adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ) : ExpandableViewHolder(binding.root, adapter, true) {

        fun bind(item: SpeakerHeaderItem) {
            binding.tvGroupName.text = item.title
            binding.groupIndicator.setOnClickListener {
                toggleExpansion()
                item.isOpened = !item.isOpened
                if (item.isOpened) {
                    binding.groupIndicator.setImageResource(R.drawable.action_drop_up)

                } else {
                    binding.groupIndicator.setImageResource(R.drawable.action_drop_down)
                }
            }

            if (item.isOpened) {
                binding.groupIndicator.setImageResource(R.drawable.action_drop_up)

            } else {
                binding.groupIndicator.setImageResource(R.drawable.action_drop_down)
            }


        }

    }
}