package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Adapter.Forum.Moderator_Adapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForModerator;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddModeratorRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddModeratorResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetModeratorDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetModeratorModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveModeratorRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveModeratorResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddModerator extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private Moderator_Adapter adapter;
    private List<GetModeratorDataModel> sourceDataList;
    List<GetModeratorDataModel> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;

    private PinyinComparatorForModerator pinyinComparator;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;


    String Category;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();

    List<GetModeratorDataModel> moderator_list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_moderator);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
        }


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Add Moderator");

        findViewById(R.id.done_add_moderator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, AddModerator.this);

                finish();
            }
        });

//        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Common.hideKeyboard(v, AddModerator.this);
//                finish();
//            }
//        });

        listner = this;


        try {
            initViews();
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    private void initViews() {

        loading_dialog = Common.LoadingDilaog(this);


        pinyinComparator = new PinyinComparatorForModerator();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        loading_dialog.show();
        //getSubTopicModerator(Common.sub_topic_edit_form_id);
        getSubTopicModerator(Integer.valueOf(sub_topic_data_detail.getId()));

        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);
        mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private List<GetModeratorDataModel> filledData(List<GetModeratorDataModel> data) {


        List<GetModeratorDataModel> mSortList = new ArrayList<>();


        for (int i = 0; i < data.size(); i++) {
            GetModeratorDataModel sortModel = new GetModeratorDataModel();

            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setId(data.get(i).getId());
            sortModel.setIs_moderator(data.get(i).getIs_moderator());

            String pinyin_english = data.get(i).getEmail();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {
                //group name is empty  added by some one.
                sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();
                if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }


        return mSortList;

    }

    private void filterData(String filterStr) {


        try {

            filter_flag = true;

            if (TextUtils.isEmpty(filterStr)) {
                filterDataList = sourceDataList;
                sourceDataList = filledData(moderator_list);
                Collections.sort(sourceDataList, pinyinComparator);
            } else {
                filterDataList.clear();

                for (GetModeratorDataModel sortModel : sourceDataList) {
                    String name = sortModel.getEmail();

                    if (name.indexOf(filterStr.toString()) != -1
                            || name.toLowerCase().startsWith(filterStr)
                            || name.toUpperCase().startsWith(filterStr)) {

                        filterDataList.add(sortModel);
                    }

                }
            }
            Collections.sort(filterDataList, pinyinComparator);
            adapter.updateList(filterDataList);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void getSubTopicModerator(int id) {


        try {

            Call<GetModeratorModel> call = retrofitInterface.getModerator("application/json", Common.auth_token, id);

            call.enqueue(new Callback<GetModeratorModel>() {
                @Override
                public void onResponse(Call<GetModeratorModel> call, Response<GetModeratorModel> response) {
                    int status_code = response.code();

                    ArrayList<GetModeratorDataModel> postListData = new ArrayList<>();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GetModeratorModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        postListData = responseModel.getData();

                    }

                    if (postListData != null) {

                        //pd.dismiss();
                        loading_dialog.dismiss();

                        if (moderator_list != null) {
                            moderator_list.clear();
                        }
                        moderator_list = postListData;

                        sourceDataList = filledData(moderator_list);
                        Collections.sort(sourceDataList, pinyinComparator);

                        manager = new LinearLayoutManager(getApplicationContext());
                        manager.setOrientation(LinearLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(manager);

                        adapter = new Moderator_Adapter(getApplicationContext(), sourceDataList, listner);
                        mRecyclerView.setAdapter(adapter);

                    }


                }

                @Override
                public void onFailure(Call<GetModeratorModel> call, Throwable t) {

                }

            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onRowClick(int position) {


    }

    @Override
    public void onViewClcik(int position, View v) {

        try {

            switch (v.getId()) {
                case R.id.whitelist_contact_checkbox:
                    if (filter_flag == true) {
                        moderator_list = filterDataList;
                    } else {
                        moderator_list = sourceDataList;
                    }


                    if (moderator_list.get(position).getIs_moderator() == 0) {
                        loading_dialog.show();
                        AddModerator(position);
                    } else if (moderator_list.get(position).getIs_moderator() == 1) {
                        loading_dialog.show();
                        RemoveModerator(position);
                    }


                    break;
                default:
                    break;
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }


    private void AddModerator(int position) {

        try {

            AddModeratorRequestModel requestModel = new AddModeratorRequestModel();
            requestModel.setPost_id(sub_topic_data_detail.getId());
            requestModel.setUser_id(moderator_list.get(position).getId());

            Call<AddModeratorResponseModel> call = retrofitInterface.AddModerator("application/json", Common.auth_token, requestModel);


            call.enqueue(new Callback<AddModeratorResponseModel>() {
                @Override
                public void onResponse(Call<AddModeratorResponseModel> call, Response<AddModeratorResponseModel> response) {
                    int status_code = response.code();

                    if (response.isSuccessful()) {
                        String status, message;

                        AddModeratorResponseModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        Log.d("AddModeratorIssue","status :"+status);
                        if (status.equals("success") ||
                                status.contains("success") ||
                                status == "success") {

                            getSubTopicModerator(Integer.valueOf(sub_topic_data_detail.getId()));
                            Toast.makeText(AddModerator.this, "Moderator added successfully", Toast.LENGTH_LONG).show();
                        } else {
                             loading_dialog.dismiss();
                            Toast.makeText(AddModerator.this, "This is already added", Toast.LENGTH_LONG).show();
                        }

                    }

                }

                @Override
                public void onFailure(Call<AddModeratorResponseModel> call, Throwable t) {
                 }

            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private void RemoveModerator(int position) {

        try {


            RemoveModeratorRequestModel requestModel = new RemoveModeratorRequestModel();
            requestModel.setPost_id(sub_topic_data_detail.getId());
            requestModel.setUser_id(moderator_list.get(position).getId());

            Call<RemoveModeratorResponseModel> call = retrofitInterface.RemoveModerator("application/json", Common.auth_token, requestModel);


            call.enqueue(new Callback<RemoveModeratorResponseModel>() {
                @Override
                public void onResponse(Call<RemoveModeratorResponseModel> call, Response<RemoveModeratorResponseModel> response) {
                    int status_code = response.code();

                    if (response.isSuccessful()) {
                        String status, message;

                        RemoveModeratorResponseModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success") ||
                                status.contains("success") ||
                                status == "success") {

                             loading_dialog.dismiss();


                            getSubTopicModerator(Integer.valueOf(sub_topic_data_detail.getId()));
                            Toast.makeText(AddModerator.this, "Moderator Remove successfully", Toast.LENGTH_LONG).show();
                        } else {
                             loading_dialog.dismiss();
                            Toast.makeText(AddModerator.this, "This is already Removed", Toast.LENGTH_LONG).show();
                        }

                    }

                }

                @Override
                public void onFailure(Call<RemoveModeratorResponseModel> call, Throwable t) {
                 }

            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}
