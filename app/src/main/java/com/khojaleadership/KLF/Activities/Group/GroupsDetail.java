package com.khojaleadership.KLF.Activities.Group;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Activities.Event.EventActivity_Updated;
import com.khojaleadership.KLF.Activities.Project.Add_Project_Activity;
import com.khojaleadership.KLF.Adapter.Group.GroupEventsAdapter;
import com.khojaleadership.KLF.Adapter.Group.GroupForumPostsAdapter;
import com.khojaleadership.KLF.Adapter.Group.GroupJoinRequestAdapter;
import com.khojaleadership.KLF.Adapter.Group.GroupMembersListAdapter;
import com.khojaleadership.KLF.Adapter.Group.GroupRelatedGroupAdapter;
import com.khojaleadership.KLF.Adapter.Group.Group_Projects_Exapandable_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Event_Model.GetEventListModel;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.ApproveGroupJoinRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.ApproveGroupJoinResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetAllGroup_ProjectListModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailEventListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailEventListModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailForumPostsDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailForumPostsModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailMembersListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailMembersListModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailPendingAccessRequestDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailPendingAccessRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailProjectDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailProjectModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailRelatedGroupDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailRelatedGroupModel;
import com.khojaleadership.KLF.Model.Group_Models.Group_GroupProjectChildModel;
import com.khojaleadership.KLF.Model.Group_Models.Group_GroupProjectParentModel;
import com.khojaleadership.KLF.Model.Group_Models.JoinGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.JoinGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveGroupJoinRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveGroupJoinResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupResponseMOdel;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Project_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupsDetail extends AppCompatActivity implements RecyclerViewClickListner {

    TextView group_name;
    TextView name, description, email, phone, address;
    WebView about_us;
    ImageView group_detail_img;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerView member_list_recyclerView;
    GroupMembersListAdapter member_list_adapter;
    TextView group_member_text;

    RecyclerView pending_access_recyclerView;
    GroupJoinRequestAdapter pending_access_adapter;
    LinearLayout pending_access_requests_section;

    RecyclerView group_forum_post_recyclerView;
    GroupForumPostsAdapter group_forum_post_adapter;
    TextView group_forum_post_text;

    RecyclerView event_list_recyclerView;
    GroupEventsAdapter event_list_adapter;
    TextView group_event_text;

    RecyclerView related_group_list_recyclerView;
    GroupRelatedGroupAdapter related_group_list_adapter;
    TextView related_group_text;


    ExpandableListView project_expandableListView;
    Group_Projects_Exapandable_Adapter project_adapter;
    TextView group_group_project_text;

    RelativeLayout group_join_icon, group_leave_icon, group_final_leave_layout;

    TextView header_title;


    String id = "";
    ImageView add_group_project_btn;


    String isAllGroupDetail = "-1", ViewProfileDetail = "-1", isProfileBusinessData = "-1";

    GetGroupListDataModel group_data_detail = new GetGroupListDataModel();
    GetAllGroup_ProjectListDataModel all_group_data_detail = new GetAllGroup_ProjectListDataModel();
    View_Profile_Bussiness_Project_Model user_profile_businessandprojects_data = new View_Profile_Bussiness_Project_Model();
    View_Profile_Charity_Project_Model user_profile_charitiesandprojects_data = new View_Profile_Charity_Project_Model();

    ArrayList<GetAllGroup_ProjectListDataModel> project_group_list = new ArrayList<>();
    ArrayList<GroupDetailMembersListDataModel> group_members_list = new ArrayList<>();
    ArrayList<GroupDetailPendingAccessRequestDataModel> pending_access_request_list = new ArrayList<>();
    ArrayList<GroupDetailForumPostsDataModel> group_forum_posts_list = new ArrayList<>();
    ArrayList<GroupDetailEventListDataModel> group_event_list = new ArrayList<>();
    ArrayList<GroupDetailRelatedGroupDataModel> group_related_group_list = new ArrayList<>();
    ArrayList<Group_GroupProjectParentModel> group_group_projects_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groups_detail_activity);

        loading_dialog = Common.LoadingDilaog(this);
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("View Group ");

        //getting data
        Intent intent = getIntent();
        group_data_detail = intent.getParcelableExtra("group_data_detail");
        all_group_data_detail = intent.getParcelableExtra("all_group_data_detail");
        user_profile_businessandprojects_data = intent.getParcelableExtra("ViewProfileBussinessData");
        user_profile_charitiesandprojects_data = intent.getParcelableExtra("ViewProfileCharityData");


        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            isAllGroupDetail = bundle.getString("isAllGroupDetail");
            if (isAllGroupDetail == null) {
                isAllGroupDetail = "-1";
            }

            ViewProfileDetail = bundle.getString("ViewProfileDetail");
            if (ViewProfileDetail == null) {
                ViewProfileDetail = "-1";
            }
            isProfileBusinessData = bundle.getString("isProfileBusinessData");
            if (isProfileBusinessData == null) {
                isProfileBusinessData = "-1";
            }
        }

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, GroupsDetail.this);

                if (isAllGroupDetail.equals("1")) {
                    //if (Common.viewprofile_group_detail==true){
                    finish();
                    // }
                } else {
                    Intent main = new Intent(GroupsDetail.this, GroupsMainScreen.class);
                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(main);
                    finish();
                }
            }
        });

        findViewById(R.id.floating_action_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GroupsDetail.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });


        group_join_icon = (RelativeLayout) findViewById(R.id.group_join_layout);
        group_leave_icon = (RelativeLayout) findViewById(R.id.group_leave_layout);
        group_final_leave_layout = findViewById(R.id.group_final_leave_layout);


        try {


            if (isAllGroupDetail.equals("0")) {
                //.....................................................................
                if (group_data_detail.getIs_member() == null ||
                        group_data_detail.getIs_member().equals("null")) {

                    group_join_icon.setVisibility(View.VISIBLE);
                    group_leave_icon.setVisibility(View.GONE);
                    group_final_leave_layout.setVisibility(View.GONE);
                } else if (group_data_detail.getIs_member().equals("0")) {

                    group_join_icon.setVisibility(View.GONE);
                    group_leave_icon.setVisibility(View.VISIBLE);
                    group_final_leave_layout.setVisibility(View.GONE);
                } else if (group_data_detail.getIs_member().equals("1")) {

                    group_join_icon.setVisibility(View.GONE);
                    group_leave_icon.setVisibility(View.GONE);
                    group_final_leave_layout.setVisibility(View.VISIBLE);
                }
                id = group_data_detail.getGroups__id();

                //.....................................................................
            } else if (isAllGroupDetail.equals("1")) {
                //.......................................................................
                if (ViewProfileDetail.equals("0")) {
                    id = all_group_data_detail.getGroup_id();
                } else if (ViewProfileDetail.equals("1")) {

                    //...................................................................
                    if (isProfileBusinessData.equals("1")) {
                        id = user_profile_businessandprojects_data.getGroups__id();
                    } else if (isProfileBusinessData.equals("0")) {
                        id = user_profile_charitiesandprojects_data.getGroups__id();
                    }
                }
                //...........................................................................
            }


            //on add project btn click
            add_group_project_btn = findViewById(R.id.add_group_project_btn);
            if (Common.is_Admin_flag == false) {
                add_group_project_btn.setVisibility(View.GONE);
            }
            add_group_project_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isAllGroupDetail.equals("0")) {
                        //Common.add_project_in_group_flag = true;
                        Intent add_project = new Intent(GroupsDetail.this, Add_Project_Activity.class);
                        add_project.putExtra("group_data_detail", group_data_detail);

                        add_project.putExtra("isAllGroupDetail", isAllGroupDetail);
                        add_project.putExtra("ViewProfileDetail", ViewProfileDetail);
                        add_project.putExtra("isProfileBusinessData", isProfileBusinessData);

                        add_project.putExtra("Group_Id", id);
                        add_project.putExtra("AddProjectInGroupFlag", "1");
                        startActivity(add_project);
                        //finish();

                    } else if (isAllGroupDetail.equals("1")) {
                        if (ViewProfileDetail.equals("0")) {
                            Intent add_project = new Intent(GroupsDetail.this, Add_Project_Activity.class);
                            add_project.putExtra("all_group_data_detail", all_group_data_detail);

                            add_project.putExtra("isAllGroupDetail", isAllGroupDetail);
                            add_project.putExtra("ViewProfileDetail", ViewProfileDetail);
                            add_project.putExtra("isProfileBusinessData", isProfileBusinessData);

                            add_project.putExtra("Group_Id", id);
                            add_project.putExtra("AddProjectInGroupFlag", "1");
                            startActivity(add_project);
                            //finish();
                        } else if (ViewProfileDetail.equals("1")) {
                            if (isProfileBusinessData.equals("1")) {

                                Intent add_project = new Intent(GroupsDetail.this, Add_Project_Activity.class);
                                add_project.putExtra("all_group_data_detail", all_group_data_detail);

                                add_project.putExtra("ViewProfileBussinessData", user_profile_businessandprojects_data);
                                add_project.putExtra("ViewProfileDetail", ViewProfileDetail);
                                add_project.putExtra("isProfileBusinessData", isProfileBusinessData);

                                add_project.putExtra("Group_Id", id);
                                add_project.putExtra("AddProjectInGroupFlag", "1");
                                startActivity(add_project);
                                //finish();

                            } else if (isProfileBusinessData.equals("0")) {
                                Intent add_project = new Intent(GroupsDetail.this, Add_Project_Activity.class);
                                add_project.putExtra("all_group_data_detail", all_group_data_detail);

                                add_project.putExtra("ViewProfileCharityData", user_profile_charitiesandprojects_data);
                                add_project.putExtra("ViewProfileDetail", ViewProfileDetail);
                                add_project.putExtra("isProfileBusinessData", isProfileBusinessData);

                                add_project.putExtra("Group_Id", id);
                                add_project.putExtra("AddProjectInGroupFlag", "1");
                                startActivity(add_project);
                                //finish();

                            }
                        }
                    }

                }
            });

            //now simply do it in less number of code lines
            loading_dialog.show();
            //for further use
            //Common.common_group_id = id;
            getGroupMembersList(id);
            group_join_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loading_dialog.show();
                    JoinGroupRequestFunction(id, Common.login_data.getData().getId());
                }
            });

            //leave group
            group_leave_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loading_dialog.show();
                    LeaveGroupAlternative(id, Common.login_data.getData().getId(), false);
                }
            });

            //leave group
            group_final_leave_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loading_dialog.show();
                    LeaveGroupAlternative(id, Common.login_data.getData().getId(), true);
                }
            });


            //on add event btn click
            findViewById(R.id.add_group_event_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent add_event = new Intent(GroupsDetail.this, EventActivity_Updated.class);
                    startActivity(add_event);

                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        if (isAllGroupDetail.equals("1")) {
            finish();
            // }
        } else {
            Intent main = new Intent(GroupsDetail.this, GroupsMainScreen.class);
            main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(main);
            finish();
        }
    }


    public void intialize_view() {

        try {

            group_member_text = findViewById(R.id.group_member_text);
            group_event_text = findViewById(R.id.group_event_text);
            group_forum_post_text = findViewById(R.id.group_forum_post_text);
            related_group_text = findViewById(R.id.related_group_text);
            pending_access_requests_section = findViewById(R.id.pending_access_requests_section);
            group_group_project_text = findViewById(R.id.group_group_project_text);

            //setting group members list
            if (group_members_list != null) {
                if (group_members_list.size() == 0) {
                    group_member_text.setVisibility(View.VISIBLE);
                } else {
                    group_member_text.setVisibility(View.GONE);
                }

                member_list_recyclerView = (RecyclerView) findViewById(R.id.group_members_list_recyclerview);
                member_list_recyclerView.setHasFixedSize(true);

                member_list_adapter = new GroupMembersListAdapter(this, group_members_list, this);
                member_list_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                member_list_recyclerView.setAdapter(member_list_adapter);
            } else {
                group_member_text.setVisibility(View.VISIBLE);
            }


            if (pending_access_request_list != null) {

                if (pending_access_request_list.size() == 0) {
                    pending_access_requests_section.setVisibility(View.GONE);
                } else {

                    pending_access_requests_section.setVisibility(View.GONE);
                    if (Common.is_Admin_flag == true || group_data_detail.getCreatedByUserID().equals(Common.login_data.getData().getId())) {

                        pending_access_requests_section.setVisibility(View.VISIBLE);

                        pending_access_recyclerView = (RecyclerView) findViewById(R.id.pending_access_requests_recyclerview);
                        pending_access_recyclerView.setHasFixedSize(true);

                        pending_access_adapter = new GroupJoinRequestAdapter(this, pending_access_request_list, this);
                        pending_access_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        pending_access_recyclerView.setAdapter(pending_access_adapter);
                    }
                }


                // }

            } else {
                pending_access_requests_section.setVisibility(View.GONE);
            }


            if (group_forum_posts_list != null) {
                //setting pending access request list

                if (group_forum_posts_list.size() == 0) {
                    group_forum_post_text.setVisibility(View.VISIBLE);
                } else {
                    group_forum_post_text.setVisibility(View.GONE);
                }

                group_forum_post_recyclerView = (RecyclerView) findViewById(R.id.group_forum_posts_recyclerview);
                group_forum_post_recyclerView.setHasFixedSize(true);

                group_forum_post_adapter = new GroupForumPostsAdapter(this, group_forum_posts_list, this);
                group_forum_post_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                group_forum_post_recyclerView.setAdapter(group_forum_post_adapter);
            } else {
                group_forum_post_text.setVisibility(View.VISIBLE);
            }


            if (group_event_list != null) {
                if (group_event_list.size() == 0) {
                    group_event_text.setVisibility(View.VISIBLE);
                } else {
                    group_event_text.setVisibility(View.GONE);
                }

                event_list_recyclerView = (RecyclerView) findViewById(R.id.group_event_list_recyclerview);
                event_list_recyclerView.setHasFixedSize(true);

                event_list_adapter = new GroupEventsAdapter(this, group_event_list, this);
                event_list_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                event_list_recyclerView.setAdapter(event_list_adapter);
            } else {
                group_event_text.setVisibility(View.VISIBLE);
            }

            if (group_related_group_list != null) {
                //setting group related group list

                if (group_related_group_list.size() == 0) {
                    related_group_text.setVisibility(View.VISIBLE);
                } else {
                    related_group_text.setVisibility(View.GONE);
                }

                related_group_list_recyclerView = (RecyclerView) findViewById(R.id.related_groups_recyclerview);
                related_group_list_recyclerView.setHasFixedSize(true);

                related_group_list_adapter = new GroupRelatedGroupAdapter(this, group_related_group_list, this);
                related_group_list_recyclerView.setLayoutManager(new LinearLayoutManager(this));
                related_group_list_recyclerView.setAdapter(related_group_list_adapter);
            } else {
                related_group_text.setVisibility(View.VISIBLE);
            }

            if (group_group_projects_list != null) {
                if (group_group_projects_list.size() == 0) {
                    group_group_project_text.setVisibility(View.VISIBLE);
                } else {
                    group_group_project_text.setVisibility(View.GONE);
                }
                project_view();
            } else {
                group_group_project_text.setVisibility(View.VISIBLE);
            }


            //setting other data
            name = (TextView) findViewById(R.id.detail_group_name);
            description = (TextView) findViewById(R.id.detail_group_description);
            // group_name = (TextView) findViewById(R.id.group_name);
            email = (TextView) findViewById(R.id.g_detail_email);
            phone = (TextView) findViewById(R.id.g_detail_phone);
            address = (TextView) findViewById(R.id.g_detail_address);

            about_us = (WebView) findViewById(R.id.g_about_us);
            about_us.getSettings().setDefaultFontSize(12);
            group_detail_img = findViewById(R.id.group_detail_img);


            if (isAllGroupDetail.equals("0")) {
                //.....................................................................


                if (group_data_detail != null) {

                    name.setText(group_data_detail.getGroups__name());
                    description.setText(group_data_detail.getGroups__description());
                    email.setText(group_data_detail.getGroups__email());
                    phone.setText(group_data_detail.getGroups__phone());
                    address.setText(group_data_detail.getGroups__address() + " " + group_data_detail.getGroups__address2() + " " + group_data_detail.getGroups__city() + " " + group_data_detail.getGroups__country());

                    if (group_data_detail.getGroups__bio().equals("") || group_data_detail.getGroups__bio().isEmpty()
                            || group_data_detail.getGroups__bio().equals("0")) {
                        about_us.loadData("Currently no data available.", "text/html; charset=utf-8", "UTF-8");
                    } else {
                        about_us.loadData(group_data_detail.getGroups__bio(), "text/html; charset=utf-8", "UTF-8");
                    }

                    Glide.with(getApplicationContext()).load(group_data_detail.getGroupimage()).into(group_detail_img);

                    //ye check baqi sbb me b lgana
                    if (group_data_detail.getCreatedByUserID().equals(Common.login_data.getData().getId())) {
                        add_group_project_btn.setVisibility(View.VISIBLE);
                    }
                }
                //.....................................................................
            } else if (isAllGroupDetail.equals("1")) {
                //.......................................................................
                if (ViewProfileDetail.equals("0")) {
                    if (all_group_data_detail != null) {
                        name.setText(all_group_data_detail.getName());
                        description.setText(all_group_data_detail.getDescription());
                        email.setText(all_group_data_detail.getEmail());
                        phone.setText("Not available");
                        address.setText(all_group_data_detail.getAddress() + " " + all_group_data_detail.getCity() + " " + all_group_data_detail.getCountry());


                        if (all_group_data_detail.getBio().equals("") || all_group_data_detail.getBio().isEmpty()
                                || all_group_data_detail.getBio().equals("0")) {
                            about_us.loadData("Currently no data available.", "text/html; charset=utf-8", "UTF-8");
                        } else {
                            about_us.loadData(all_group_data_detail.getBio(), "text/html; charset=utf-8", "UTF-8");
                        }

                        Glide.with(getApplicationContext()).load(all_group_data_detail.getGroups_image()).into(group_detail_img);
                    }
                } else if (ViewProfileDetail.equals("1")) {

                    //...................................................................
                    if (isProfileBusinessData.equals("1")) {

                        if (user_profile_businessandprojects_data != null) {
                            name.setText(user_profile_businessandprojects_data.getGroups__name());
                            description.setText(user_profile_businessandprojects_data.getGroups__description());
                            email.setText(user_profile_businessandprojects_data.getGroups__email());
                            phone.setText(user_profile_businessandprojects_data.getGroups__phone());
                            address.setText(user_profile_businessandprojects_data.getGroups__address() + " " + user_profile_businessandprojects_data.getGroups__address2() + " " + user_profile_businessandprojects_data.getGroups__city() + " " + user_profile_businessandprojects_data.getGroups__country());
                            if (user_profile_businessandprojects_data.getGroups__bio().equals("") || user_profile_businessandprojects_data.getGroups__bio().isEmpty()
                                    || user_profile_businessandprojects_data.getGroups__bio().equals("0")) {
                                about_us.loadData("Currently no data available.", "text/html; charset=utf-8", "UTF-8");
                            } else {
                                about_us.loadData(user_profile_businessandprojects_data.getGroups__bio(), "text/html; charset=utf-8", "UTF-8");
                            }

                            Glide.with(getApplicationContext()).load(user_profile_businessandprojects_data.getGroupimage()).into(group_detail_img);
                        }
                    } else if (isProfileBusinessData.equals("0")) {

                        if (user_profile_charitiesandprojects_data != null) {
                            name.setText(user_profile_charitiesandprojects_data.getGroups__name());
                            description.setText(user_profile_charitiesandprojects_data.getGroups__description());
                            email.setText(user_profile_charitiesandprojects_data.getGroups__email());
                            phone.setText(user_profile_charitiesandprojects_data.getGroups__phone());
                            address.setText(user_profile_charitiesandprojects_data.getGroups__address() + " " + user_profile_charitiesandprojects_data.getGroups__address2() + " " + user_profile_charitiesandprojects_data.getGroups__city() + " " + user_profile_charitiesandprojects_data.getGroups__country());
                            if (user_profile_charitiesandprojects_data.getGroups__bio().equals("") || user_profile_charitiesandprojects_data.getGroups__bio().isEmpty()
                                    || user_profile_charitiesandprojects_data.getGroups__bio().equals("0")) {
                                about_us.loadData("Currently no data available.", "text/html; charset=utf-8", "UTF-8");
                            } else {
                                about_us.loadData(user_profile_charitiesandprojects_data.getGroups__bio(), "text/html; charset=utf-8", "UTF-8");
                            }

                            Glide.with(getApplicationContext()).load(user_profile_charitiesandprojects_data.getGroupimage()).into(group_detail_img);
                        }
                    }

                }
                //...........................................................................
            }


            about_us.setVerticalScrollBarEnabled(true);
            about_us.setHorizontalScrollBarEnabled(true);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }


    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {


        try {

            if (v.getId() == R.id.accept_member) {
                GroupDetailPendingAccessRequestDataModel data = pending_access_request_list.get(position);
                loading_dialog.show();
                ApproveMemeberRequestFunction(data.getGroupRequests__group_id(), data.getGroupRequests__user_id(), data.getGroupRequests__id());
            }

            if (v.getId() == R.id.reject_member) {

                GroupDetailPendingAccessRequestDataModel data = pending_access_request_list.get(position);
                loading_dialog.show();
                RemoveMemeberRequestFunction(data.getGroupRequests__group_id(), data.getGroupRequests__user_id());

            }

            if (v.getId() == R.id.remove_member) {
                loading_dialog.show();

                GroupDetailMembersListDataModel member = group_members_list.get(position);
                RemoveMemberFromGroup(member.getGroupsUsers__group_id(), member.getUsers__id());

            }

            if (v.getId() == R.id.group_event_row) {
                loading_dialog.show();
                String event_id = group_event_list.get(position).getEvents__id();
                getAllEventsList(event_id);

            }

            if (v.getId() == R.id.group_related_group_cardview) {
                loading_dialog.show();
                String group_id = group_related_group_list.get(position).getId();
                getAllGroups(group_id);
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void getGroupMembersList(final String group_id) {

        try {

            Call<GroupDetailMembersListModel> call = retrofitInterface.getGroupMembersList("application/json", Common.auth_token, group_id);
            call.enqueue(new Callback<GroupDetailMembersListModel>() {
                @Override
                public void onResponse(Call<GroupDetailMembersListModel> call, Response<GroupDetailMembersListModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailMembersListModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GroupDetailMembersListDataModel> data_list = responseModel.getData();

                        if (data_list != null) {
                            if (group_members_list != null) {
                                group_members_list.clear();
                            }
                            group_members_list = data_list;
                        }

                        //new task here
                        getPendingAccessRequestList(group_id);

                    }

                }

                @Override
                public void onFailure(Call<GroupDetailMembersListModel> call, Throwable t) {

                    //new task here
                    getPendingAccessRequestList(group_id);
                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getPendingAccessRequestList(final String group_id) {

        try {

            Call<GroupDetailPendingAccessRequestModel> call = retrofitInterface.getPendingAccessRequests("application/json", Common.auth_token, group_id);

            call.enqueue(new Callback<GroupDetailPendingAccessRequestModel>() {
                @Override
                public void onResponse(Call<GroupDetailPendingAccessRequestModel> call, Response<GroupDetailPendingAccessRequestModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailPendingAccessRequestModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GroupDetailPendingAccessRequestDataModel> data_list = responseModel.getData();
                        if (data_list != null) {
                            if (pending_access_request_list != null) {
                                pending_access_request_list.clear();
                            }
                            pending_access_request_list = data_list;
                        }

                        getGroupForumPostsList(group_id);
                    }

                }

                @Override
                public void onFailure(Call<GroupDetailPendingAccessRequestModel> call, Throwable t) {
                    getGroupForumPostsList(group_id);

                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getGroupForumPostsList(final String group_id) {

        try {

            Call<GroupDetailForumPostsModel> call = retrofitInterface.getGroupForumPostList("application/json", Common.auth_token, group_id);

            call.enqueue(new Callback<GroupDetailForumPostsModel>() {
                @Override
                public void onResponse(Call<GroupDetailForumPostsModel> call, Response<GroupDetailForumPostsModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailForumPostsModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GroupDetailForumPostsDataModel> data_list = responseModel.getData();
                        if (data_list != null) {


                            if (group_forum_posts_list != null) {
                                group_forum_posts_list.clear();
                            }
                            group_forum_posts_list = data_list;
                        }

                        //new task here
                        getGroupEventList(group_id);

                    }

                }

                @Override
                public void onFailure(Call<GroupDetailForumPostsModel> call, Throwable t) {

                    //new task here
                    getGroupEventList(group_id);

                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getGroupEventList(String group_id) {

        try {

            Call<GroupDetailEventListModel> call = retrofitInterface.getGroupEventList_New("application/json", Common.auth_token, group_id);
            call.enqueue(new Callback<GroupDetailEventListModel>() {
                @Override
                public void onResponse(Call<GroupDetailEventListModel> call, Response<GroupDetailEventListModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailEventListModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GroupDetailEventListDataModel> data_list = responseModel.getData();


                        if (data_list != null) {

                            if (group_event_list != null) {
                                group_event_list.clear();
                            }
                            group_event_list = data_list;

                        }

                        //new task here
                        getGroupRelatedGroupList();

                    }

                }

                @Override
                public void onFailure(Call<GroupDetailEventListModel> call, Throwable t) {

                    //new task here
                    getGroupRelatedGroupList();

                    //loading_dialog.dismiss();
                }

            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    private void getAllEventsList(final String event_id) {

        try {

            Call<GetEventListModel> call = retrofitInterface.getAllEventList("application/json", Common.auth_token);

            call.enqueue(new Callback<GetEventListModel>() {
                @Override
                public void onResponse(Call<GetEventListModel> call, Response<GetEventListModel> response) {

                    if (response.isSuccessful()) {

                        String status, message;

                        status = response.body().getStatus();
                        message = response.body().getMessage();

                        if (status.equals("success")) {


                            ArrayList<GetEventsListDataModel> all_event_list = new ArrayList<>();
                            GetEventsListDataModel event_data = new GetEventsListDataModel();

                            all_event_list = response.body().getData();
                            for (int i = 0; i < all_event_list.size(); i++) {

                                if (all_event_list.get(i).getId().equals(event_id)) {
                                    //Common.event_data_detail = Common.all_event_list.get(i);
                                    event_data = all_event_list.get(i);

                                    i = all_event_list.size();
                                }

                            }

                            //Common.master_search_is_event = true;

                            Intent view_event = new Intent(GroupsDetail.this, EventActivity_Updated.class);
                            view_event.putExtra("EventData", event_data);
                            view_event.putExtra("isMasterSearch", "1");
                            startActivity(view_event);

                            loading_dialog.dismiss();
                        }
                    } else {
                    }

                }

                @Override
                public void onFailure(Call<GetEventListModel> call, Throwable t) {
                    loading_dialog.dismiss();

                    Intent view_event = new Intent(GroupsDetail.this, EventActivity_Updated.class);
                    startActivity(view_event);
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void getAllGroups(final String group_id) {

        try {

            Call<GetAllGroup_ProjectListModel> call = retrofitInterface.getProjectAllGroupList("application/json", Common.auth_token);

            call.enqueue(new Callback<GetAllGroup_ProjectListModel>() {
                @Override
                public void onResponse(Call<GetAllGroup_ProjectListModel> call, Response<GetAllGroup_ProjectListModel> response) {
                    if (response.isSuccessful()) {
                        String status, message;


                        GetAllGroup_ProjectListModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        ArrayList<GetAllGroup_ProjectListDataModel> list = new ArrayList<>();
                        list = responseModel.getData();


                        if (list != null) {
                            loading_dialog.dismiss();

                            if (project_group_list != null) {
                                project_group_list.clear();
                            }
                            project_group_list = list;

                            for (int i = 0; i < project_group_list.size(); i++) {

                                if (project_group_list.get(i).getGroup_id().equals(group_id)) {

                                    all_group_data_detail = project_group_list.get(i);

                                    i = project_group_list.size();
                                }
                            }

//                            Common.is_all_group_detail = true;
//                            Common.viewprofile_group_detail = false;
                            Intent view_event = new Intent(GroupsDetail.this, GroupsDetail.class);
                            view_event.putExtra("all_group_data_detail", all_group_data_detail);
                            view_event.putExtra("isAllGroupDetail", "1");
                            view_event.putExtra("ViewProfileDetail", "0");
                            view_event.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(view_event);

                        }


                    }
                }

                @Override
                public void onFailure(Call<GetAllGroup_ProjectListModel> call, Throwable t) {

//                    Common.is_all_group_detail = true;
//                    Common.viewprofile_group_detail = false;
//                    Intent view_event = new Intent(GroupsDetail.this, GroupsDetail.class);
//                    view_event.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(view_event);

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void getGroupRelatedGroupList() {

        try {

            Call<GroupDetailRelatedGroupModel> call = retrofitInterface.getGroupRelatedGroupList("application/json", Common.auth_token, Common.login_data.getData().getId(), id);
            final String finalId = id;
            call.enqueue(new Callback<GroupDetailRelatedGroupModel>() {
                @Override
                public void onResponse(Call<GroupDetailRelatedGroupModel> call, Response<GroupDetailRelatedGroupModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailRelatedGroupModel responseModel = response.body();


                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        ArrayList<GroupDetailRelatedGroupDataModel> data_list = responseModel.getData();


                        if (data_list != null) {

                            if (group_related_group_list != null) {
                                group_related_group_list.clear();
                            }

                            group_related_group_list = data_list;

                        }

                        //new task here
                        getGroupGroupProjectList(finalId);

                    }

                }

                @Override
                public void onFailure(Call<GroupDetailRelatedGroupModel> call, Throwable t) {

                    //new task here
                    getGroupGroupProjectList(finalId);

                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    private void getGroupGroupProjectList(String id) {

        try {

            Call<GroupDetailProjectModel> call = retrofitInterface.getGroupGroupProjectList("application/json", Common.auth_token, id);


            call.enqueue(new Callback<GroupDetailProjectModel>() {
                @Override
                public void onResponse(Call<GroupDetailProjectModel> call, Response<GroupDetailProjectModel> response) {
                    int status_code = response.code();


                    if (response.isSuccessful()) {
                        String status, message, totalcount, totalpages;

                        GroupDetailProjectModel responseModel = response.body();

                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        if (status.equals("success")) {

                            ArrayList<GroupDetailProjectDataModel> data_list = responseModel.getData();
                            //pd.dismiss();
                            loading_dialog.dismiss();

                            ArrayList<Group_GroupProjectParentModel> complete_list = new ArrayList<>();

                            for (int i = 0; i < data_list.size(); i++) {
                                String name, id;
                                name = data_list.get(i).getProject().getProjects__name();
                                id = data_list.get(i).getProject().getProjects__id();

                                ArrayList<Group_GroupProjectChildModel> child_list = new ArrayList<>();
                                for (int j = 0; j < data_list.get(i).getMembers().size(); j++) {
                                    String m_name, m_id, m_img;

                                    m_name = data_list.get(i).getMembers().get(j).getMember__name();
                                    m_id = data_list.get(i).getMembers().get(j).getMember_id();
                                    m_img = data_list.get(i).getMembers().get(j).getProfileImg__name();

                                    child_list.add(new Group_GroupProjectChildModel(m_name, m_id, m_img));
                                }

                                complete_list.add(new Group_GroupProjectParentModel(id, name, child_list));
                            }

                            if (group_group_projects_list != null) {
                                group_group_projects_list.clear();
                            }

                            group_group_projects_list = complete_list;


                        }

                        //new task here
                        intialize_view();

                    }

                }

                @Override
                public void onFailure(Call<GroupDetailProjectModel> call, Throwable t) {


                    //new task here
                    if (group_group_projects_list != null) {
                        group_group_projects_list.clear();
                    }

                    intialize_view();

                    loading_dialog.dismiss();

                }

            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    public void project_view() {

        try {

            project_expandableListView = (ExpandableListView) findViewById(R.id.group_group_projects_expandableview);
            project_adapter = new Group_Projects_Exapandable_Adapter(this, group_group_projects_list);
            project_expandableListView.setAdapter(project_adapter);
            project_expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                        project_expandableListView.collapseGroup(previousGroup);
                    }
                    previousGroup = groupPosition;
                }
            });

            loading_dialog.dismiss();
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    private void ApproveMemeberRequestFunction(String group_id, final String user_id, String pending_request_id) {

        try {

            ApproveGroupJoinRequestModel request = new ApproveGroupJoinRequestModel();
            request.setGroup_id(group_id);
            request.setUser_id(user_id);
            request.setPending_request_id(pending_request_id);

            Call<ApproveGroupJoinResponseModel> call = retrofitInterface.ApproveGroupJoinRequest("application/json", Common.auth_token, request);

            call.enqueue(new Callback<ApproveGroupJoinResponseModel>() {
                @Override
                public void onResponse(Call<ApproveGroupJoinResponseModel> call, Response<ApproveGroupJoinResponseModel> response) {

                    if (response.isSuccessful()) {
                        ApproveGroupJoinResponseModel model = response.body();

                        String status, message;
                        status = model.getStatus();
                        message = model.getMessage();

                        if (status.equals("success")) {

                            if (group_data_detail != null) {
                                if (user_id.equals(Common.login_data.getData().getId())) {
                                    //direct final visible

                                    group_join_icon.setVisibility(View.GONE);
                                    group_leave_icon.setVisibility(View.GONE);
                                    group_final_leave_layout.setVisibility(View.VISIBLE);
                                }
                            }

                            //loading_dialog.dismiss();

                            getGroupMembersList(id);
                            Toast.makeText(GroupsDetail.this, "Request is Approved.", Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ApproveGroupJoinResponseModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void RemoveMemeberRequestFunction(String group_id, final String user_id) {

        try {

            RemoveGroupJoinRequestModel request = new RemoveGroupJoinRequestModel();
            request.setGroup_id(group_id);
            request.setUser_id(user_id);

            Call<RemoveGroupJoinResponseModel> call = retrofitInterface.RemoveGroupJoinRequest("application/json", Common.auth_token, request);

            call.enqueue(new Callback<RemoveGroupJoinResponseModel>() {
                @Override
                public void onResponse(Call<RemoveGroupJoinResponseModel> call, Response<RemoveGroupJoinResponseModel> response) {

                    if (response.isSuccessful()) {
                        RemoveGroupJoinResponseModel model = response.body();

                        String status, message;
                        status = model.getStatus();
                        message = model.getMessage();

                        if (status.equals("success")) {
//                        loading_dialog.dismiss();

                            if (group_data_detail != null) {
                                if (user_id.equals(Common.login_data.getData().getId())) {
                                    //direct join visible
                                    group_join_icon.setVisibility(View.VISIBLE);
                                    group_leave_icon.setVisibility(View.GONE);
                                    group_final_leave_layout.setVisibility(View.GONE);
                                }
                            }


                            getGroupMembersList(id);
                            Toast.makeText(GroupsDetail.this, "Request is Rejected.", Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<RemoveGroupJoinResponseModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    private void JoinGroupRequestFunction(String group_id, String user_id) {

        try {

            JoinGroupRequestModel request = new JoinGroupRequestModel();
            request.setGroup_id(group_id);
            request.setUser_id(user_id);

            Call<JoinGroupResponseModel> call = retrofitInterface.JoinGroupRrequest("application/json", Common.auth_token, request);

            call.enqueue(new Callback<JoinGroupResponseModel>() {
                @Override
                public void onResponse(Call<JoinGroupResponseModel> call, Response<JoinGroupResponseModel> response) {

                    if (response.isSuccessful()) {
                        JoinGroupResponseModel model = response.body();

                        String status, message;
                        status = model.getStatus();
                        message = model.getMessage();

                        if (status.equals("success")) {
                            Toast.makeText(GroupsDetail.this, "Join Group Request has been sent.", Toast.LENGTH_LONG).show();

                            group_join_icon.setVisibility(View.GONE);
                            group_leave_icon.setVisibility(View.VISIBLE);
                            group_final_leave_layout.setVisibility(View.GONE);

                            // loading_dialog.show();
                            getGroupMembersList(id);
                        } else if (status.equals("error")) {
                            Toast.makeText(GroupsDetail.this, "You have not access to join this group.", Toast.LENGTH_LONG).show();
                            getGroupMembersList(id);
                        }

                    } else {
                    }

                }

                @Override
                public void onFailure(Call<JoinGroupResponseModel> call, Throwable t) {
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void RemoveMemberFromGroup(String group_id, final String user_id) {

        try {
            RemoveMemberFromGroupRequestModel request = new RemoveMemberFromGroupRequestModel();
            request.setGroup_id(group_id);
            request.setUser_id(user_id);

            Call<RemoveMemberFromGroupResponseMOdel> call = retrofitInterface.RemoveMemberFromGroup("application/json", Common.auth_token, request);


            call.enqueue(new Callback<RemoveMemberFromGroupResponseMOdel>() {
                @Override
                public void onResponse(Call<RemoveMemberFromGroupResponseMOdel> call, Response<RemoveMemberFromGroupResponseMOdel> response) {

                    if (response.isSuccessful()) {
                        RemoveMemberFromGroupResponseMOdel model = response.body();

                        String status, message;
                        status = model.getStatus();
                        message = model.getMessage();

                        if (status.equals("success")) {
                            //loading_dialog.dismiss();

                            if (group_data_detail != null) {
                                if (user_id.equals(Common.login_data.getData().getId())) {
                                    //direct join visible
                                    group_join_icon.setVisibility(View.VISIBLE);
                                    group_leave_icon.setVisibility(View.GONE);
                                    group_final_leave_layout.setVisibility(View.GONE);


                                }
                            }

                            getGroupMembersList(id);
                            Toast.makeText(GroupsDetail.this, "User removed successfully.", Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<RemoveMemberFromGroupResponseMOdel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void LeaveGroupAlternative(String group_id, String user_id, final Boolean is_left) {

        try {

            RemoveMemberFromGroupRequestModel request = new RemoveMemberFromGroupRequestModel();
            request.setGroup_id(group_id);
            request.setUser_id(user_id);

            Call<RemoveMemberFromGroupResponseMOdel> call = retrofitInterface.RemoveMemberFromGroup("application/json", Common.auth_token, request);


            call.enqueue(new Callback<RemoveMemberFromGroupResponseMOdel>() {
                @Override
                public void onResponse(Call<RemoveMemberFromGroupResponseMOdel> call, Response<RemoveMemberFromGroupResponseMOdel> response) {

                    loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        RemoveMemberFromGroupResponseMOdel model = response.body();

                        String status, message;
                        status = model.getStatus();
                        message = model.getMessage();

                        if (status.equals("success")) {
                            if (is_left == true) {
                                Toast.makeText(GroupsDetail.this, "Group Left Successfully.", Toast.LENGTH_LONG).show();

                                group_join_icon.setVisibility(View.VISIBLE);
                                group_leave_icon.setVisibility(View.GONE);
                                group_final_leave_layout.setVisibility(View.GONE);

                            } else if (is_left == false) {
                                Toast.makeText(GroupsDetail.this, "Group Join Request Cancel Successfully.", Toast.LENGTH_LONG).show();

                                group_join_icon.setVisibility(View.VISIBLE);
                                group_leave_icon.setVisibility(View.GONE);
                                group_final_leave_layout.setVisibility(View.GONE);
                            }

                            loading_dialog.show();
                            getGroupMembersList(id);
                        }

                    }
                }

                @Override
                public void onFailure(Call<RemoveMemberFromGroupResponseMOdel> call, Throwable t) {
                    loading_dialog.dismiss();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
