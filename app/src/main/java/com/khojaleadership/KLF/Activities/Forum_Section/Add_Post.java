package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView
        ;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddPostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddPostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.EditPostRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.EditPostResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListDataCommentsModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Post extends AppCompatActivity {

    TextView top_title;
    RichEditor mEditor;
    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Boolean flag = false;
    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;


    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();
    GetPostListDataCommentsModel_Updated sub_topic_post_detail = new GetPostListDataCommentsModel_Updated();

    String Subtopic_Id, Category, IsFromDashBoard, ModeratorFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__post);

        //hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //closeKeyboard();
        loading_dialog = Common.LoadingDilaog(this);


        //getting data
        topic_data_detail = getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");
        sub_topic_post_detail = getIntent().getParcelableExtra("SubtopicPostDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
            Subtopic_Id = bundle.getString("SubtopicId");
            IsFromDashBoard = bundle.getString("IsFromDashBoard");
            ModeratorFlag = bundle.getString("ModeratorFlag");
            if (IsFromDashBoard == null) {
                IsFromDashBoard = "0"; // when data is not returning from dash board....default value is 0
            }

        } else {
            IsFromDashBoard = "0";
        }

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, getApplicationContext());
                Common.hideKeyboard(v, Add_Post.this);
                Intent i = new Intent(Add_Post.this, Sub_Topic_Posts.class);
                i.putExtra("SubtopicId", Subtopic_Id);
                i.putExtra("TopicDataDetail", topic_data_detail);
                i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                i.putExtra("IsFromDashBoard", "1");
                i.putExtra("Category", Category);
                startActivity(i);
                finish();
            }
        });


        intialize_view();


    }

    public void intialize_view() {
        try {


            top_title = findViewById(R.id.c_header_title);
            mEditor = (RichEditor) findViewById(R.id.editor);
            mEditor.setFontSize(12);
            mEditor.setEditorFontSize(12);
            mEditor.setEditorHeight(250);
            mEditor.setScrollbarFadingEnabled(false);
            mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
            mEditor.setScrollBarSize(5);
            mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
            mEditor.canScrollVertically(0);
            mEditor.setVerticalScrollBarEnabled(true);
            mEditor.setMotionEventSplittingEnabled(true);
            mEditor.setEditorFontColor(Color.BLACK);
            mEditor.setPadding(10, 10, 10, 10);

            if (Common.addPost == false && Common.editPost == true) {
                top_title.setText("Edit Subtopic Post");
                mEditor.setHtml(sub_topic_post_detail.getForumPosts__content());
            } else if (Common.addPost == false && Common.editPost == false) {
                top_title.setText("Reply Post");
            } else if (Common.addPost == true && Common.editPost == false) {
                top_title.setText("Add Comment");
            } else {
                mEditor.setPlaceholder("Description*");
            }


            mEditor.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View p_v, MotionEvent p_event) {
                    // this will disallow the touch request for parent scroll on touch of child view
                    action_align_left = findViewById(R.id.action_align_left);
                    action_align_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_left_flag == false) {
                                action_left_flag = true;
                                action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                                action_centr_flag = false;
                                action_right_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                                action_align_right.setBackgroundResource(R.drawable.ic_right);

                                mEditor.setAlignLeft();
                            } else if (action_left_flag == true) {
                                action_left_flag = false;
                                action_align_left.setBackgroundResource(R.drawable.ic_left);
                            }

                        }
                    });

                    action_align_center = findViewById(R.id.action_align_center);
                    action_align_center.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_centr_flag == false) {
                                action_centr_flag = true;
                                action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                                action_left_flag = false;
                                action_right_flag = false;
                                action_align_left.setBackgroundResource(R.drawable.ic_left);
                                action_align_right.setBackgroundResource(R.drawable.ic_right);

                                mEditor.setAlignCenter();
                            } else if (action_centr_flag == true) {
                                action_centr_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                            }
                        }
                    });

                    action_align_right = findViewById(R.id.action_align_right);
                    action_align_right.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action_right_flag == false) {
                                action_right_flag = true;
                                action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                                action_centr_flag = false;
                                action_left_flag = false;
                                action_align_center.setBackgroundResource(R.drawable.ic_center);
                                action_align_left.setBackgroundResource(R.drawable.ic_left);

                                mEditor.setAlignRight();
                            } else if (action_right_flag == true) {
                                action_right_flag = false;
                                action_align_right.setBackgroundResource(R.drawable.ic_right);
                            }

                        }
                    });


                    return false;
                }
            });

            findViewById(R.id.publish_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (mEditor.getHtml() == null) {
                        Toast.makeText(Add_Post.this, "Description should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (mEditor.getHtml().equals("")) {
                        Toast.makeText(Add_Post.this, "Description should not be empty.", Toast.LENGTH_SHORT).show();
                    } else if (mEditor.getHtml().equalsIgnoreCase("")) {
                        Toast.makeText(Add_Post.this, "Description should not be empty.", Toast.LENGTH_SHORT).show();
                    } else {

                        //aik dafa button press py add kre
                        if (flag == false) {
                            flag = true;

                            if (Common.addPost == false && Common.editPost == true) {
                                loading_dialog.show();

                                Log.d("AddPost", "add post boolean false");
                                onEditSubTopicBtnClick();
                            } else if (Common.addPost == true && Common.editPost == false) {
                                loading_dialog.show();

                                Log.d("AddPost", "add post boolean true");
                                //for ppost screen i write
                                onAddSubTopicPostBtnClick();

                            } else if (Common.addPost == false && Common.editPost == false) {
                                loading_dialog.show();
                                onAddSubTopicPostBtnClick();
                            }
                        }

                    }

                }


            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    private void onAddSubTopicPostBtnClick() {

        try {

            AddPostRequestModel requestModel = new AddPostRequestModel();
            requestModel.setUser_id(Common.login_data.getData().getId());
            //requestModel.setParent_id(String.valueOf(Common.sub_topic_edit_form_id));
            requestModel.setParent_id(Subtopic_Id);


            requestModel.setLft("300");
            requestModel.setRght("300");
            requestModel.setContent(mEditor.getHtml());
//
//            if (Common.post_list_comment_data != null) {
//
//                if (Common.is_Admin_flag == true) {
//
//                    requestModel.setStatuspost("A");
//                } else {
//
//                    if (Common.post_list_comment_data.get(0).getForumPosts__moderated() != null) {
//                        if (Common.post_list_comment_data.get(0).getForumPosts__moderated().equals("1")) {
//                             requestModel.setStatuspost("P");
//                        } else if (Common.post_list_comment_data.get(0).getForumPosts__moderated().equals("0")) {
//                             requestModel.setStatuspost("A");
//                        }
//                    } else {
//                        requestModel.setStatuspost("A");
//                    }
//
//                }
//            }


            if (Common.is_Admin_flag == true) {
                requestModel.setStatuspost("A");
            } else {
                if (ModeratorFlag != null) {
                    if (ModeratorFlag.equals("1")) {
                        requestModel.setStatuspost("P");
                    } else if (ModeratorFlag.equals("0")) {
                        requestModel.setStatuspost("A");
                    }
                } else {
                    requestModel.setStatuspost("A");
                }

            }

            Call<AddPostResponseModel> responseCall = retrofitInterface.addSubTopicPost("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddPostResponseModel>() {
                @Override
                public void onResponse(Call<AddPostResponseModel> call, Response<AddPostResponseModel> response) {

                    if (response.isSuccessful()) {

                        loading_dialog.dismiss();
                        String status, message;


                        AddPostResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        Log.d("PostReplyIssue", "status :" + status);

                        Intent i = new Intent(Add_Post.this, Sub_Topic_Posts.class);
                        i.putExtra("SubtopicId", Subtopic_Id);
                        i.putExtra("TopicDataDetail", topic_data_detail);
                        i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                        i.putExtra("IsFromDashBoard", "1");
                        i.putExtra("Category", Category);
                        startActivity(i);
                        finish();

                    } else {
                    }


                }

                @Override
                public void onFailure(Call<AddPostResponseModel> call, Throwable t) {
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onEditSubTopicBtnClick() {

        try {

            EditPostRequestModel requestModel = new EditPostRequestModel();
            requestModel.setForum_post_id(sub_topic_post_detail.getForumPosts__id());
            requestModel.setTopic(sub_topic_post_detail.getForumPosts__topic());  //default

            requestModel.setContent(mEditor.getHtml());


            Call<EditPostResponseModel> responseCall = retrofitInterface.editSubTopicPost("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<EditPostResponseModel>() {
                @Override
                public void onResponse(Call<EditPostResponseModel> call, Response<EditPostResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        EditPostResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        Intent i = new Intent(Add_Post.this, Sub_Topic_Posts.class);
                        i.putExtra("SubtopicId", Subtopic_Id);
                        i.putExtra("TopicDataDetail", topic_data_detail);
                        i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                        i.putExtra("IsFromDashBoard", "1");
                        i.putExtra("Category", Category);
                        startActivity(i);
                        finish();

                    } else {
                    }
                }

                @Override
                public void onFailure(Call<EditPostResponseModel> call, Throwable t) {
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Add_Post.this, Sub_Topic_Posts.class);
        i.putExtra("SubtopicId", Subtopic_Id);
        i.putExtra("TopicDataDetail", topic_data_detail);
        i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
        i.putExtra("IsFromDashBoard", "1");
        i.putExtra("Category", Category);
        startActivity(i);
        finish();

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
