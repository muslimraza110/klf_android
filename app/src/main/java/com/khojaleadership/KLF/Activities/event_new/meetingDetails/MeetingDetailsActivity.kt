package com.khojaleadership.KLF.Activities.event_new.meetingDetails

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.khojaleadership.KLF.Activities.Splash_Login.SplashScreen
import com.khojaleadership.KLF.Adapter.event_new.meetingDetails.MeetingDetailsViewPagerAdapter
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivityMeetingDetailsBinding


class MeetingDetailsActivity : AppCompatActivity() {

    private val viewModel: MeetingDetailsViewModel by viewModels()

    private lateinit var pagerAdapter: MeetingDetailsViewPagerAdapter
    private lateinit var binding: ActivityMeetingDetailsBinding

    var imageViewAnimator: ObjectAnimator? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityMeetingDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.authStore.selectDelegateOkClicked = false

        if(viewModel.authStore.loginData == null){
            startActivity(Intent(this@MeetingDetailsActivity, SplashScreen::class.java))
            finish()
        }

        setUpViews()
        setUpObservers()
    }


    override fun onResume() {
        super.onResume()

        if (viewModel.authStore.selectDelegateOkClicked) {
            viewModel.updateDelegateSelection()
            viewModel.authStore.selectDelegateOkClicked = false
        }
    }


    private fun setUpObservers() {

        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.viewpager.visibility = View.VISIBLE
                    stopRotation()
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
//                    binding.tvError.text = it.message
                    stopRotation()
                }
            }
        })

        viewModel.showMeetingsFragmentEvent.observe(this, Observer {
            it?.consume()?.let {
                viewModel.refresh()
                binding.viewpager.currentItem = 0
            }
        })

        viewModel.showRequestMeetingFragmentEvent.observe(this, Observer {
            it?.consume()?.let {
                binding.viewpager.currentItem = 1
            }
        })




//        viewModel.showItineraryFragmentEvent.observe(this, Observer {
//            it?.consume()?.let {
//                binding.viewpager.setCurrentItem(0, true)
//            }
//        })
    }


    private fun setUpViews() {
        pagerAdapter = MeetingDetailsViewPagerAdapter(supportFragmentManager, lifecycle)

        changeSelectedTab(0)

        binding.viewpager.apply {
            adapter = pagerAdapter
            offscreenPageLimit = 1
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    changeSelectedTab(position)
                }
            })
            isUserInputEnabled = false
        }

        binding.buttonMeetings.setOnClickListener {
            binding.viewpager.setCurrentItem(0, true)
//            viewModel.enableSpinners()
        }

        binding.buttonRequestMeeting.setOnClickListener {
            binding.viewpager.setCurrentItem(1, true)
            viewModel.refreshRequestMeeting()

        }

        binding.imageBack.setOnClickListener {
            if (binding.viewpager.currentItem == 0) {

                stopRotation()
                if (isTaskRoot) {
                    val intent = Intent(this@MeetingDetailsActivity, SplashScreen::class.java)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                finish()
            } else {
                binding.viewpager.setCurrentItem(0, true)
//                viewModel.enableSpinners()
            }
        }

        binding.imageRefresh.setOnClickListener {
            startRotation(binding.imageRefresh)
            viewModel.refresh()
        }

    }


    override fun onBackPressed() {
        stopRotation()

        if (isTaskRoot) {
            startActivity(Intent(this@MeetingDetailsActivity, SplashScreen::class.java))
            finish()
        } else {
            super.onBackPressed()
        }
    }

    private fun startRotation(imageView: ImageView) {

        val imageViewObjectAnimator = ObjectAnimator.ofFloat(imageView,
                "rotation", 0f, 360f)
        imageViewObjectAnimator.repeatCount = ObjectAnimator.INFINITE
        imageViewObjectAnimator.repeatMode = ObjectAnimator.RESTART
        imageViewObjectAnimator.duration = 1000
        imageViewObjectAnimator.interpolator = LinearInterpolator()
        imageViewObjectAnimator.start()
        imageViewAnimator = imageViewObjectAnimator
    }

    private fun stopRotation() {
        binding.imageRefresh.rotation = 0f
        imageViewAnimator?.cancel()
        imageViewAnimator = null
    }

    private fun changeSelectedTab(position: Int) {
        if (position == 0) {
            binding.buttonMeetings.background = ContextCompat.getDrawable(this, R.drawable.linear_gradient_blue_cyan_round)
            binding.buttonMeetings.setTextColor(ContextCompat.getColor(this, R.color.appWhite))
            binding.imageRefresh.visibility =  View.VISIBLE
            binding.buttonRequestMeeting.background = ContextCompat.getDrawable(this, R.drawable.grey_outlined_bg)
            binding.buttonRequestMeeting.setTextColor(ContextCompat.getColor(this, R.color.appGrey))
        } else {
            binding.buttonRequestMeeting.background = ContextCompat.getDrawable(this, R.drawable.linear_gradient_blue_cyan_round)
            binding.buttonRequestMeeting.setTextColor(ContextCompat.getColor(this, R.color.appWhite))

            binding.buttonMeetings.background = ContextCompat.getDrawable(this, R.drawable.grey_outlined_bg)
            binding.buttonMeetings.setTextColor(ContextCompat.getColor(this, R.color.appGrey))
        }
        if (position == 1){
            binding.imageRefresh.visibility =  View.GONE
        }
    }

//    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
//        if (event.action == MotionEvent.ACTION_DOWN) {
//            val v = currentFocus
//            if (v is EditText) {
//                val outRect = Rect()
//                v.getGlobalVisibleRect(outRect)
//                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
//                    v.clearFocus()
//                    val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
//                }
//            }
//        }
//        return super.dispatchTouchEvent(event)
//    }

    companion object{
        const val KEY_USER_ID = "keyUserId"
        const val KEY_EVENT_ID = "keyEventId"
        const val KEY_SUMMIT_EVENT_FACULTY_ID = "keySummitEventFacultyIdkeySummitEventFacultyId"
        const val KEY_MEETING_PARENT_ID = "keyMeetingParentId"
    }
}