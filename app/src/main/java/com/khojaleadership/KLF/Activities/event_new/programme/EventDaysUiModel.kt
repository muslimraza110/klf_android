package com.khojaleadership.KLF.Activities.event_new.programme

import java.util.*

data class EventDaysUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
//        val dayId: String,
        val day: String,
        var isSelected: Boolean,
        val date: String
){
    override fun toString(): String {
        return date
    }
}

data class EventTimeSlotUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val programDetailsId: String,
        val time: String
){
    override fun toString(): String {
        return time
    }
}