package com.khojaleadership.KLF.Activities.Setting;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Adapter.Setting.View_Profile_Bussiness_Adapter;
import com.khojaleadership.KLF.Adapter.Setting.View_Profile_Charity_Adapter;
import com.khojaleadership.KLF.Adapter.Setting.View_Profile_Family_adapter;
import com.khojaleadership.KLF.Adapter.Setting.View_Profile_Pledges_Adapter;
import com.khojaleadership.KLF.Adapter.Setting.View_Profile_Business_ExpandableListView;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsModel;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Child_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Parent_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Child_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Parent_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Family_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Group_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Pleger_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_profile_User_Info_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class View_Profile_Activity_Updated extends AppCompatActivity implements RecyclerViewClickListner {

    TextView title;

    View_Profile_Bussiness_Adapter bussiness_adapter;
    View_Profile_Charity_Adapter charity_adapter;
    View_Profile_Family_adapter family_adapter;
    private View_Profile_Pledges_Adapter pledges_adapter;

    RecyclerView recyclerView_bussiness;
    RecyclerView recyclerView_Charities;
    RecyclerView recyclerView_family;
    RecyclerView recyclerView_pledger;


    private ExpandableListView mLv;
    private ExpandableListView charity_expandable_view;
    private View_Profile_Business_ExpandableListView mAdapter;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    TextView email, number, profile_secondry_number, profile_address, profile_website, profile_facebook;
    TextView header_name, header_description;
    ImageView view_profile_img;

    TextView bussiness_text, Charity_text, bussiness_expandable_text, Charity_expandable_text, family_text, Pledges_text;

    ArrayList<View_Profile_Pleger_Model> user_profile_pledges = new ArrayList<>();

    ArrayList<View_Profile_Bussiness_Project_Model> user_profile_businessandprojects = new ArrayList<>();
    ArrayList<View_Profile_Charity_Project_Model> user_profile_charitiesandprojects = new ArrayList<>();
    ArrayList<View_profile_User_Info_Model> user_profile_information_list = new ArrayList<>();
    ArrayList<View_Profile_Family_Model> user_prfile_family_list = new ArrayList<>();
    ArrayList<View_Profile_Group_Model> user_profile_groups = new ArrayList<>();

    ArrayList<View_Profile_Project_Model> user_profile_groupsproject = new ArrayList<>();

    ArrayList<View_Profile_Bussiness_Parent_Model> user_profile_business_projects = new ArrayList<>();
    ArrayList<View_Profile_Bussiness_Parent_Model> user_profile_charity_projects1 = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aa_activity_view__profile);

        loading_dialog = Common.LoadingDilaog(this);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //title
        title = findViewById(R.id.c_header_title);
        title.setText("View Profile");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, View_Profile_Activity_Updated.this);

                finish();
            }
        });


        //API to get Data
        //function in retrofit available  GetProfileData("1");
        loading_dialog.show();
        getProfileData();


    }


    public void intialize_view() {

        header_name = findViewById(R.id.header_name);
        header_description = findViewById(R.id.header_description);

        //user info
        email = (TextView) findViewById(R.id.profile_email);
        number = (TextView) findViewById(R.id.profile_number);
        profile_secondry_number = findViewById(R.id.profile_secondry_number);
        profile_address = findViewById(R.id.profile_address);
        profile_website = findViewById(R.id.profile_website);
        profile_facebook = findViewById(R.id.profile_facebook);

        view_profile_img = findViewById(R.id.view_profile_img);


        //TextView bussiness_text,Charity_text,bussiness_expandable_text,Charity_expandable_text,family_text,Pledges_text;
        bussiness_text = findViewById(R.id.bussiness_text);
        Charity_text = findViewById(R.id.Charity_text);
        bussiness_expandable_text = findViewById(R.id.bussiness_expandable_text);
        Charity_expandable_text = findViewById(R.id.Charity_expandable_text);
        family_text = findViewById(R.id.family_text);
        Pledges_text = findViewById(R.id.Pledges_text);


        //setting data
        if (user_profile_information_list != null) {
            if (user_profile_information_list.size() > 0) {

                if (user_profile_information_list.get(0).getUsers__email()!=null){
                    if (user_profile_information_list.get(0).getUsers__first_name().equals("") && user_profile_information_list.get(0).getUsers__last_name().equals("")
                    || user_profile_information_list.get(0).getUsers__first_name().equals(" ") && user_profile_information_list.get(0).getUsers__last_name().equals(" ")){
                        header_name.setText("Not Available");
                    }else {
                        header_name.setText(user_profile_information_list.get(0).getUsers__first_name() + " " + user_profile_information_list.get(0).getUsers__last_name());
                    }
                }else {
                    header_name.setText("Not Available");
                }

                if (user_profile_information_list.get(0).getUserDetails__description()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__description().equals("") || user_profile_information_list.get(0).getUserDetails__description().equals(" ") ){
                        header_description.setText("Not Available");
                    }else {
                        header_description.setText(user_profile_information_list.get(0).getUserDetails__description());
                    }
                }else {
                    header_description.setText("Not Available");
                }


                if (user_profile_information_list.get(0).getUsers__email()!=null){
                    if (user_profile_information_list.get(0).getUsers__email().equals("") || user_profile_information_list.get(0).getUsers__email().equals(" ") ){
                        email.setText("Not Available");
                    }else {
                        email.setText(user_profile_information_list.get(0).getUsers__email());
                    }
                }else {
                    email.setText("Not Available");
                }

                if (user_profile_information_list.get(0).getUserDetails__phone()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__phone().equals("") || user_profile_information_list.get(0).getUserDetails__phone().equals(" ")){
                        number.setText("Not Available");
                    }else {
                        number.setText(user_profile_information_list.get(0).getUserDetails__phone());
                    }
                }else {
                    number.setText("Not Available");
                }


                if (user_profile_information_list.get(0).getUserDetails__phone2()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__phone2().equals("") || user_profile_information_list.get(0).getUserDetails__phone2().equals(" ")){
                        profile_secondry_number.setText("Not Available");
                    }else {
                        profile_secondry_number.setText(user_profile_information_list.get(0).getUserDetails__phone2());
                    }
                }else {
                    profile_secondry_number.setText("Not Available");
                }

                if (user_profile_information_list.get(0).getUserDetails__address()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__address().equals("") || user_profile_information_list.get(0).getUserDetails__address().equals(" ")){
                        profile_address.setText("Not Available");

                    }else {
                        profile_address.setText(user_profile_information_list.get(0).getUserDetails__city()+" "+
                                user_profile_information_list.get(0).getUserDetails__country()+" "+
                                user_profile_information_list.get(0).getUserDetails__region()+" "+
                                user_profile_information_list.get(0).getUserDetails__address());
                    }
                }else {
                    profile_address.setText("Not Available");
                }
                if (user_profile_information_list.get(0).getUserDetails__website()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__website().equals("") || user_profile_information_list.get(0).getUserDetails__website().equals(" ")){
                        profile_website.setText("Not Available");
                    }else {
                        profile_website.setText(user_profile_information_list.get(0).getUserDetails__website());
                    }
                }else {
                    profile_website.setText("Not Available");
                }

                if (user_profile_information_list.get(0).getUserDetails__facebook()!=null){
                    if (user_profile_information_list.get(0).getUserDetails__facebook().equals("") || user_profile_information_list.get(0).getUserDetails__facebook().equals(" ")){
                        profile_facebook.setText("Not Available");
                    }else {
                        profile_facebook.setText(user_profile_information_list.get(0).getUserDetails__facebook());
                    }
                }else {
                    profile_facebook.setText("Not Available");
                }



                Glide.with(getApplicationContext()).load(user_profile_information_list.get(0).getProfileImg__name()).into(view_profile_img);
            }
        }


        //for the expandable Bussiness Projects
        if (user_profile_business_projects != null) {
            if (user_profile_business_projects.size() == 0) {
                bussiness_expandable_text.setVisibility(View.VISIBLE);
            } else {
                mLv = ((ExpandableListView) findViewById(R.id.lv));
                mAdapter = new View_Profile_Business_ExpandableListView(this, user_profile_business_projects, this);
                mLv.setAdapter(mAdapter);
                mLv.setFastScrollEnabled(true);
            }
        } else {
            bussiness_expandable_text.setVisibility(View.VISIBLE);
        }

        //for the expandable Charity Projects
        if (user_profile_charity_projects1 != null) {
            if (user_profile_charity_projects1.size() == 0) {
                Charity_expandable_text.setVisibility(View.VISIBLE);
            } else {
                charity_expandable_view = ((ExpandableListView) findViewById(R.id.charity_expandable));
                mAdapter = new View_Profile_Business_ExpandableListView(this, user_profile_charity_projects1, this);
                charity_expandable_view.setAdapter(mAdapter);
                charity_expandable_view.setFastScrollEnabled(true);
            }
        } else {
            Charity_expandable_text.setVisibility(View.VISIBLE);
        }


//        This is for the Bussinesses
        if (user_profile_businessandprojects != null) {
            if (user_profile_businessandprojects.size() == 0) {
                bussiness_text.setVisibility(View.VISIBLE);
            } else {
                recyclerView_bussiness = findViewById(R.id.recyclerview_bussiness);
                recyclerView_bussiness.setLayoutManager(new LinearLayoutManager(this));
                recyclerView_bussiness.setHasFixedSize(true);
                bussiness_adapter = new View_Profile_Bussiness_Adapter(this, user_profile_businessandprojects, this);
                recyclerView_bussiness.setAdapter(bussiness_adapter);
            }
        } else {
            bussiness_text.setVisibility(View.VISIBLE);
        }
//        This is for the Charities
        if (user_profile_charitiesandprojects != null) {
            if (user_profile_charitiesandprojects.size() == 0) {
                Charity_text.setVisibility(View.VISIBLE);
            } else {
                recyclerView_Charities = findViewById(R.id.recyclerview_Charities);
                recyclerView_Charities.setLayoutManager(new LinearLayoutManager(this));
                recyclerView_Charities.setHasFixedSize(true);
                charity_adapter = new View_Profile_Charity_Adapter(this, user_profile_charitiesandprojects, this);
                recyclerView_Charities.setAdapter(charity_adapter);
            }
        } else {
            Charity_text.setVisibility(View.VISIBLE);
        }

        // this is for the family recyclerview
        if (user_prfile_family_list != null) {
            if (user_prfile_family_list.size() == 0) {
                family_text.setVisibility(View.VISIBLE);
            } else {
                recyclerView_family = findViewById(R.id.family_recyclerview);
                recyclerView_family.setLayoutManager(new LinearLayoutManager(this));
                recyclerView_family.setHasFixedSize(true);
                family_adapter = new View_Profile_Family_adapter(this, user_prfile_family_list);
                recyclerView_family.setAdapter(family_adapter);
            }
        } else {
            family_text.setVisibility(View.VISIBLE);
        }

        // this is for the Pledges recyclerview
        if (user_profile_pledges != null) {
            if (user_profile_pledges.size() == 0) {
                Pledges_text.setVisibility(View.VISIBLE);
            } else {
                recyclerView_pledger = findViewById(R.id.Pledges_recyclerview);
                recyclerView_pledger.setLayoutManager(new LinearLayoutManager(this));
                recyclerView_pledger.setHasFixedSize(true);
                pledges_adapter = new View_Profile_Pledges_Adapter(this, user_profile_pledges);
                recyclerView_pledger.setAdapter(pledges_adapter);
            }
        } else {
            Pledges_text.setVisibility(View.VISIBLE);
        }

    }

    private void getProfileData() {

//        Intent intent= getIntent();
        //GetContactDataListModel Contactdata=intent.getParcelableExtra("Contactdata");

        // String contact_id= obj.getUsers__id();

        Bundle bundle = getIntent().getExtras();
        String isMasterSearch="0",view_profile_id="0";

        if (bundle!=null) {
            isMasterSearch = bundle.getString("isMasterSearch");
            view_profile_id=bundle.getString("view_profile_id");
        }else {
            isMasterSearch="0";
        }

        String id = "";
        //if 1 then from other activity if 0 then login user profile.
        if (isMasterSearch.equals("1")) {
            //id = Contactdata.getUsers__id();
            id=view_profile_id;
        } else if (isMasterSearch.equals("0")) {
            id = Common.login_data.getData().getId();
        }

//        if (Common.master_search_view_profile == true) {
//            id = Common.view_profile_id;
//        } else if (Common.master_search_view_profile == false) {
//            id = Common.login_data.getData().getId();
//        }

        Call<View_Profile_Model> responseCall = retrofitInterface.GetProfileData("application/json",Common.auth_token, id);

        responseCall.enqueue(new Callback<View_Profile_Model>() {
            @Override
            public void onResponse(Call<View_Profile_Model> call, Response<View_Profile_Model> response) {

                if (response.isSuccessful()) {

                    String status, message;

                    View_Profile_Model responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (user_profile_pledges !=null){
                        user_profile_pledges.clear();
                    }
                    if (user_profile_information_list !=null){
                        user_profile_information_list.clear();
                    }
                    if (user_prfile_family_list !=null){
                        user_prfile_family_list.clear();
                    }
                    if (user_profile_businessandprojects !=null){
                        user_profile_businessandprojects.clear();
                    }




                    if (user_profile_charitiesandprojects !=null){
                        user_profile_charitiesandprojects.clear();
                    }
                    if (user_profile_groups !=null){
                        user_profile_groups.clear();
                    }
                    if (user_profile_groupsproject !=null){
                        user_profile_groupsproject.clear();
                    }

                    //saving data
                    user_profile_pledges = responseModel.getPledges();
                    user_profile_information_list = responseModel.getUserinfo1();
                    user_prfile_family_list = responseModel.getFamilies();
                    user_profile_businessandprojects = responseModel.getBusinessandprojects();
                    user_profile_charitiesandprojects = responseModel.getCharitiesandprojects();
                    user_profile_groups = responseModel.getGroups();
                    user_profile_groupsproject = responseModel.getGroupsproject();

                    getBusinuesAndCharityGroups();


                } else {
                 }


            }

            @Override
            public void onFailure(Call<View_Profile_Model> call, Throwable t) {
                //loading_dialog.dismiss();

                getBusinuesAndCharityGroups();
             }
        });
    }


    private void onGetProjectListData() {


        Call<ProjectsModel> responseCall = retrofitInterface.getProjectsList("application/json",Common.auth_token);
        responseCall.enqueue(new Callback<ProjectsModel>() {
            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {

                if (response.isSuccessful()) {
                    //loading_dialog.dismiss();

                    String status, message;

                    ProjectsModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<ProjectsDataModel> project_data = responseModel.getData();
                   // Common.projects_data_list = project_data;

                    if (project_data != null) {
                        intialize_view();
                        loading_dialog.dismiss();
                    }

                } else {
                    intialize_view();
                    loading_dialog.dismiss();
                 }

            }

            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                loading_dialog.dismiss();
                intialize_view();
                 //Confirm_Dialog("Topic Delete", "Topic deletion failed.", "OK");
            }
        });


    }

    public void getBusinuesAndCharityGroups() {

        ArrayList<View_Profile_Group_Model> group_list = user_profile_groups;
        ArrayList<View_Profile_Project_Model> group_project_list = user_profile_groupsproject;

        ArrayList<View_Profile_Bussiness_Parent_Model> bussiness_model = new ArrayList<>();
        ArrayList<View_Profile_Bussiness_Parent_Model> charity_model1 = new ArrayList<>();

        ArrayList<View_Profile_Charity_Parent_Model> charity_model = new ArrayList<>();


        if (group_list!=null) {
            for (int i = 0; i < group_list.size(); i++) {
                String Groups__phone, Groups__address, Groups__address2, Groups__bio, Groups__groupimage;
                String group_id = group_list.get(i).getGroup_id();
                String catagory_id = group_list.get(i).getGroups__category_id();
                String name = group_list.get(i).getGroups__name();
                String desciption = group_list.get(i).getGroups__description();
                String email = group_list.get(i).getGroups__email();
                Groups__phone = group_list.get(i).getGroups__phone();
                Groups__address = group_list.get(i).getGroups__address();
                Groups__address2 = group_list.get(i).getGroups__address2();
                Groups__bio = group_list.get(i).getGroups__bio();
                Groups__groupimage = group_list.get(i).getGroups__groupimage();

                int is_business = -1;

                ArrayList<View_Profile_Bussiness_Child_Model> bussiness_child_list = new ArrayList<>();
                ArrayList<View_Profile_Bussiness_Child_Model> charity_child_list1 = new ArrayList<>();

                ArrayList<View_Profile_Charity_Child_Model> charity_child_list = new ArrayList<>();

                for (int j = 0; j < group_project_list.size(); j++) {

                    View_Profile_Bussiness_Child_Model bussinessChildModel = new View_Profile_Bussiness_Child_Model();
                    View_Profile_Bussiness_Child_Model charityChildModel1 = new View_Profile_Bussiness_Child_Model();

                    View_Profile_Charity_Child_Model charityChildModel = new View_Profile_Charity_Child_Model();

                    if (group_id.equals(group_project_list.get(j).getGroup_id())) {

                        if (catagory_id.equals("2")) {
                            is_business = 1;
                            bussinessChildModel.setId(group_project_list.get(j).getGroup_id());
                            bussinessChildModel.setName(group_project_list.get(j).getProjects__name());
                            bussinessChildModel.setProject_id(group_project_list.get(j).getProject_id());

                            bussiness_child_list.add(bussinessChildModel);
                        } else if (catagory_id.equals("3")) {
                            is_business = 0;

                            charityChildModel1.setId(group_project_list.get(j).getGroup_id());
                            charityChildModel1.setName(group_project_list.get(j).getProjects__name());
                            charityChildModel1.setProject_id(group_project_list.get(j).getProject_id());

                            charity_child_list1.add(charityChildModel1);

                        }

                    }
                }

                 if (is_business == 1) {
                    bussiness_model.add(new View_Profile_Bussiness_Parent_Model(group_id, name, desciption, email, Groups__phone, Groups__address, Groups__address2, Groups__bio, Groups__groupimage, bussiness_child_list));
                } else if (is_business == 0) {
                    charity_model1.add(new View_Profile_Bussiness_Parent_Model(group_id, name, desciption, email, Groups__phone, Groups__address, Groups__address2, Groups__bio, Groups__groupimage, charity_child_list1));
                }

            }
        }


        if (bussiness_model!=null) {
            for (int i = 0; i < bussiness_model.size(); i++) {

                for (int j = 0; j < bussiness_model.get(i).getChild_list().size(); j++) {
                   }

            }
        }


        if (charity_model1!=null) {
            for (int i = 0; i < charity_model1.size(); i++) {

                for (int j = 0; j < charity_model1.get(i).getChild_list().size(); j++) {
                   }

            }
        }


        user_profile_business_projects = bussiness_model;
        user_profile_charity_projects1 = charity_model1;


        loading_dialog.dismiss();
        intialize_view();
        //onGetProjectListData();


    }


    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {
        switch (v.getId()) {
            case R.id.profile_bussiness_row:
                View_Profile_Bussiness_Project_Model user_profile_businessandprojects_data=new View_Profile_Bussiness_Project_Model();

                //Common.user_profile_businessandprojects_data = user_profile_businessandprojects.get(position);
                user_profile_businessandprojects_data = user_profile_businessandprojects.get(position);

//                Common.is_all_group_detail = true;
//                Common.viewprofile_group_detail = true;
//                Common.is_viewProfile_group_business = true;

                Intent view_event = new Intent(View_Profile_Activity_Updated.this, GroupsDetail.class);
                view_event.putExtra("ViewProfileBussinessData",user_profile_businessandprojects_data);
                view_event.putExtra("isAllGroupDetail","1");
                view_event.putExtra("ViewProfileDetail","1");
                view_event.putExtra("isProfileBusinessData","1");
                startActivity(view_event);

                break;
            case R.id.profile_charity_row:
                View_Profile_Charity_Project_Model user_profile_charitiesandprojects_data=new View_Profile_Charity_Project_Model();

                //Common.user_profile_charitiesandprojects_data = user_profile_charitiesandprojects.get(position);

                user_profile_charitiesandprojects_data = user_profile_charitiesandprojects.get(position);

//                Common.is_all_group_detail = true;
//                Common.viewprofile_group_detail = true;
//                Common.is_viewProfile_group_business = false;

                Intent group = new Intent(View_Profile_Activity_Updated.this, GroupsDetail.class);
                group.putExtra("ViewProfileCharityData",user_profile_charitiesandprojects_data);
                group.putExtra("isAllGroupDetail","1");
                group.putExtra("ViewProfileDetail","1");
                group.putExtra("isProfileBusinessData","0");
                startActivity(group);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
