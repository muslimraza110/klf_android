package com.khojaleadership.KLF.Activities.event_new.planner.requestMeetingNew

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsActivity
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Helper.*
import com.khojaleadership.KLF.Model.event_new.PersonDetailResponse
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.EventsRepository
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class RequestMeetingNewViewModel(application: Application) : AndroidViewModel(application) {

    var meetingId: String? = null

    var dayMilliseconds: Long = 0
    var startTimeMilliseconds: Long = 0
    var endTimeMilliseconds: Long = 0

    private var summitEventFacultyId: String? = null
    private var eventId: String? = null
    private var userId: String? = null


    var allDelegates: List<PersonDetailUiModel>? = null

    private val _selectedDelegates: MutableLiveData<List<PersonDetailUiModel>> = MutableLiveData()
    val selectedDelegates: LiveData<List<PersonDetailUiModel>> = _selectedDelegates

    private val _eventState: MutableLiveData<Result<Unit>> = MutableLiveData()
    val eventState: LiveData<Result<Unit>> = _eventState

    private val _messageEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val messageEvent: LiveData<Event<String>> = _messageEvent

    private val _multipleMeetingSentErrorEvent: MutableLiveData<Event<String>> = MutableLiveData()
    val multipleMeetingSentErrorEvent: LiveData<Event<String>> = _multipleMeetingSentErrorEvent

    val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    private val eventsRepository = EventsRepository(authStore.loginData?.auth_token ?: "")



    fun getDataFromIntent(intent: Intent?) {
        userId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_USER_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_USER_ID)
        } else {
            authStore.loginData?.id
        }

        eventId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_EVENT_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_EVENT_ID)
        } else {
            authStore.event?.eventId
        }

        summitEventFacultyId = if (intent?.hasExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID) == true) {
            intent.getStringExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID)
        } else {
            authStore.event?.facultyId
        }

    }

    fun getAllDelegates() {
//        _eventState.value = Result.Loading
        allDelegates = listOf()
        _selectedDelegates.value = listOf()

        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.getDelegates(
                                searchText = "",
                                eventId = eventId ?: "",
                                userId = userId
                        )

                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->

                                allDelegates = mapToPersonDetailUiModel(data.all).filter {
                                    it.userId != userId
                                }

//                                _eventState.value = Result.Success(
//                                        data = Unit,
//                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
//                                )

                            } ?: run {
                                Timber.e(response.message)
                                allDelegates = listOf()
//                                _eventState.value = Result.Success(
//                                        data = Unit,
//                                        message = if (response.message.isNullOrEmpty()) Common.noRecordFoundMessage else response.message
//                                )
                            }


                        } else {
                            Timber.e(response.message)
//                            _eventState.value = Result.Error(
//                                    message = if (response.message.isNullOrEmpty()) Common.somethingWentWrongMessage else response.message
//                            )
                            _messageEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        Timber.e(it)
//                        _eventState.value = Result.Error(
//                                message = Common.somethingWentWrongMessage
//                        )
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
//            _eventState.value = Result.Error(
//                    message = Common.idNotFoundMessage
//            )
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }

    }


    fun sendMeetingRequest(meetingNotes: String, meetingStartTime: String, meetingEndTime: String) {
        _eventState.value = Result.Loading
        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.setSummitEventMeetingRequest(
                                requestedToIdList = _selectedDelegates.value?.joinToString(",") { it.summitEventsFacultyId }
                                        ?: "",
                                requestedById = summitEventFacultyId ?: "",
                                meetingNotes = meetingNotes,
                                userId = userId,
                                meetingVenue = "Meeting Venue",
                                meetingStartTime = meetingStartTime,
                                meetingEndTime = meetingEndTime,
                                meetingDate = getDateFromTimeStamp(dayMilliseconds),
                                eventId = eventId ?: ""
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            response.data?.let { data ->

                            }
                            _eventState.value = Result.Success(data = Unit, message = "")
                            _messageEvent.value = Event(response.message ?: "")
                            _selectedDelegates.value = listOf()
                        } else {
                            _eventState.value = Result.Error(message = "")
                            _multipleMeetingSentErrorEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        _eventState.value = Result.Error(message = "")
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(message = "")
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }
    }

    fun updateMeetingRequest(meetingNotes: String, meetingStartTime: String, meetingEndTime: String) {
        _eventState.value = Result.Loading
        userId?.let { userId ->
            viewModelScope.launchSafely(
                    block = {
                        val response = eventsRepository.updateWholeSummitEventMeetingRequest(
                                meetingId = meetingId ?: "",
                                requestedToIdList = _selectedDelegates.value?.joinToString(",") { it.summitEventsFacultyId }
                                        ?: "",
                                requestedById = summitEventFacultyId ?: "",
                                meetingNotes = meetingNotes,
                                userId = userId,
                                meetingVenue = "Meeting Venue",
                                meetingStartTime = meetingStartTime,
                                meetingEndTime = meetingEndTime,
                                meetingDate = getDateFromTimeStamp(dayMilliseconds),
                                eventId = eventId ?: ""
                        )
                        if (response.status == ResponseStatus.SUCCESS) {
                            meetingId = null
                            response.data?.let { data ->

                            }
                            _eventState.value = Result.Success(data = Unit, message = "")
                            _messageEvent.value = Event(response.message ?: "")
                            _selectedDelegates.value = listOf()
                        } else {
                            _eventState.value = Result.Error(message = "")
                            _multipleMeetingSentErrorEvent.value = Event(response.message
                                    ?: Common.somethingWentWrongMessage)
                        }
                    },
                    error = {
                        _eventState.value = Result.Error(message = "")
                        _messageEvent.value = Event(Common.somethingWentWrongMessage)
                    }
            )
        } ?: run {
            _eventState.value = Result.Error(message = "")
            _messageEvent.value = Event(Common.idNotFoundMessage)
        }
    }


    fun updateDelegateSelection() {
        allDelegates = authStore.delegatesToSelect
        _selectedDelegates.value = allDelegates?.filter {
            it.isSelected
        }
    }

    fun deselectAllDelegates() {
        allDelegates = allDelegates?.map {
            it.copy(
                    isSelected = false
            )
        }
        _selectedDelegates.value = allDelegates?.filter {
            it.isSelected
        }
    }

    private fun mapToPersonDetailUiModel(delegates: List<PersonDetailResponse?>?): List<PersonDetailUiModel> {
        return delegates?.mapNotNull {
            it?.let {
                PersonDetailUiModel(
                        userId = it.id ?: "",
                        summitEventsFacultyId = it.summitEventsFacultyId ?: "",
                        name = it.name ?: "",
                        shortDescription = it.shortDescription ?: "",
                        longDescription = it.longDescription ?: "",
                        twitter = it.twitterLink ?: "",
                        linkedIn = it.linkedInLink ?: "",
                        facebook = it.facebookLink ?: "",
                        photoUrl = it.userImage ?: "",
                        designation = it.designation ?: "",
                        company = "",
                        country = "",
//                        availableSlotData = it.meetingSlots,
                        isSelected = false,
                        isSpeaker = it.isSpeaker == "1",
                        isDelegate = it.isDelegate == "1"
                )
            }

        } ?: listOf()
    }

    private fun getDateFromTimeStamp(timeStamp: Long): String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                .format(Date(timeStamp))
    }

}