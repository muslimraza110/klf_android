package com.khojaleadership.KLF.Activities.event_new.userProfile

import android.app.AlertDialog
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.EventTimeSlotUiModel
import com.khojaleadership.KLF.Helper.Common
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ActivityUserProfileBinding
import com.khojaleadership.KLF.databinding.LayoutMeetingNotesBinding

class UserProfileActivity : AppCompatActivity() {


    private val viewModel: UserProfileViewModel by viewModels()
    lateinit var binding: ActivityUserProfileBinding
    lateinit var meetingNotesBinding: LayoutMeetingNotesBinding
    private val profileUrl = Common.LIVE_BASE_URL + "/summitEvents/facultyprofilepage.json"
    var alertDialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        binding = ActivityUserProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)


        getDataFromIntent()
        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {


        viewModel.sendMeetingRequestEvent.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    meetingNotesBinding.progressBar.visibility = View.VISIBLE
                    meetingNotesBinding.image.visibility = View.INVISIBLE
                    meetingNotesBinding.name.visibility = View.INVISIBLE
                    meetingNotesBinding.editText.isEnabled = false
                    meetingNotesBinding.imageClose.isEnabled = false
                }
                is Result.Success -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                    alertDialog?.dismiss()
                    alertDialog = null
                }
                is Result.Error -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                }
            }
        })

        viewModel.messageEvent.observe(this, Observer {
            it.consume()?.let { message ->
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun getDataFromIntent() {
        if (intent?.hasExtra(KEY_FACULTY_ID) == true) {
            viewModel.requestedToId = intent?.getStringExtra(KEY_FACULTY_ID) ?: ""
        }
        if (intent?.hasExtra(KEY_USER_ID) == true) {
            viewModel.userId = intent?.getStringExtra(KEY_USER_ID) ?: ""
        }
        if (intent?.hasExtra(KEY_IS_DELEGATE) == true) {
            viewModel.isDelegate = intent?.getBooleanExtra(KEY_IS_DELEGATE, false) ?: false
        }
        if (intent?.hasExtra(KEY_USER_FB) == true) {
            viewModel.facebookUrl = intent?.getStringExtra(KEY_USER_FB) ?: ""
        }
        if (intent?.hasExtra(KEY_USER_TWITTER) == true) {
            viewModel.twitterUrl = intent?.getStringExtra(KEY_USER_TWITTER) ?: ""
        }
        if (intent?.hasExtra(KEY_USER_LINKED_IN) == true) {
            viewModel.linkedInUrl = intent?.getStringExtra(KEY_USER_LINKED_IN) ?: ""
        }
        if (intent?.hasExtra(KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID) == true) {
            viewModel.programDetailsId = intent?.getStringExtra(KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID)
                ?: ""
        }

        if (viewModel.programDetailsId.isNullOrEmpty()) {
            viewModel.loadAvailableMeetingSlots()
        }

        if (viewModel.userId == Common.login_data?.data?.id ||
            viewModel.authStore.event?.isUserRegistered == null ||
            viewModel.authStore.event?.isUserRegistered == false || !viewModel.isDelegate) {
            binding.buttonRequestMeeting.visibility = View.GONE
        }

    }


    private fun setUpViews() {
        binding.imageBack.setOnClickListener {
            finish()
        }

        binding.imageFacebook.setOnClickListener {
            Common.loadUrlInBrowser(viewModel.facebookUrl,this)
        }
        binding.imageLinkedIn.setOnClickListener {
            Common.loadUrlInBrowser(viewModel.linkedInUrl,this)
        }
        binding.imageTwitter.setOnClickListener {
            Common.loadUrlInBrowser(viewModel.twitterUrl,this)
        }


        if (viewModel.facebookUrl.trim().isEmpty()) {
            binding.imageFacebook.isEnabled = false
            binding.imageFacebook.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appLightGrey))
        }

        if (viewModel.twitterUrl.trim().isEmpty()) {
            binding.imageTwitter.isEnabled = false
            binding.imageTwitter.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appLightGrey))
        }

        if (viewModel.linkedInUrl.trim().isEmpty()) {
            binding.imageLinkedIn.isEnabled = false
            binding.imageLinkedIn.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appLightGrey))
        }

        binding.buttonRequestMeeting.setOnClickListener {
            if (viewModel.programDetailsId.isNullOrEmpty()) {
                if (viewModel.areMeetingSlotsAvailable()) {
                    openMeetingNotesDialog()
                } else {
                    Toast.makeText(this, "User is not available in any time slot.", Toast.LENGTH_SHORT).show()
                }
            } else {
                if (viewModel.meetingRequestSent) {
                    Toast.makeText(this, "You have already sent this user a meeting request.", Toast.LENGTH_SHORT).show()

                } else {
                    openMeetingNotesDialog()
                }
            }

        }

        binding.webview.apply {
            setBackgroundColor(Color.TRANSPARENT)
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    binding.progressBar.visibility = View.VISIBLE
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    binding.progressBar.visibility = View.GONE
                }

                override fun onReceivedError(
                    view: WebView?,
                    request: WebResourceRequest?,
                    error: WebResourceError?
                ) {
                    super.onReceivedError(view, request, error)
                    Toast.makeText(context, "Cannot load page.", Toast.LENGTH_SHORT).show()
                    binding.progressBar.visibility = View.GONE
                }

            }
            settings.domStorageEnabled = true
            settings.setAppCacheEnabled(true)
            settings.loadsImagesAutomatically = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            loadUrl(profileUrl + "?faculty_id=${viewModel.userId}")
        }


    }

    private fun openMeetingNotesDialog() {

        meetingNotesBinding = LayoutMeetingNotesBinding.inflate(layoutInflater)
        alertDialog = AlertDialog.Builder(this).create()
        alertDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog?.setView(meetingNotesBinding.root)
        alertDialog?.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        alertDialog?.show()
        alertDialog?.setCanceledOnTouchOutside(false)

        meetingNotesBinding.imageClose.setOnClickListener {
            alertDialog?.dismiss()
        }


        if (intent?.hasExtra(KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID) == false) {

            val timeSpinnerAdapter = ArrayAdapter<EventTimeSlotUiModel>(this, R.layout.item_spinner_simple)

            meetingNotesBinding.spinnerTime.adapter = timeSpinnerAdapter
            meetingNotesBinding.spinnerTime.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.programDetailsId = viewModel.spinnerTimeData.value?.get(viewModel.dayIndex)?.get(position)?.programDetailsId
                }
            }


            val dateSpinnerAdapter = ArrayAdapter<EventDaysUiModel>(this, R.layout.item_spinner_simple)

            viewModel.spinnerDaysData.value?.let {
                dateSpinnerAdapter.addAll(it)
            }


            meetingNotesBinding.spinnerDate.adapter = dateSpinnerAdapter
            meetingNotesBinding.spinnerDate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.dayIndex = position
                    viewModel.spinnerTimeData.value?.let {
                        timeSpinnerAdapter.clear()
                        timeSpinnerAdapter.addAll(it[position])
                        meetingNotesBinding.spinnerTime.adapter = timeSpinnerAdapter
                    }
                }
            }

            meetingNotesBinding.spinnerDate.adapter.viewTypeCount


        } else {
            meetingNotesBinding.spinnerTime.visibility = View.GONE
            meetingNotesBinding.spinnerDate.visibility = View.GONE
        }

        meetingNotesBinding.layoutSendNotes.setOnClickListener {
            viewModel.sendMeetingRequest(meetingNotesBinding.editText.text?.toString() ?: "")
        }

    }

    companion object {
        const val KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID = "KEY_REQUEST_MEETING_PROGRAM_DETAILS_ID"
        const val KEY_FACULTY_ID = "KEY_FACULTY_ID"
        const val KEY_USER_FB = "KEY_USER_FB"
        const val KEY_USER_TWITTER = "KEY_USER_TWITTER"
        const val KEY_USER_LINKED_IN = "KEY_USER_LINKED_IN"
        const val KEY_USER_ID = "KEY_USER_ID"
        const val KEY_IS_DELEGATE = "KEY_IS_DELEGATE"
    }

}