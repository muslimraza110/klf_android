package com.khojaleadership.KLF.Activities.event_new.planner.myItinerary

import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import java.util.*

abstract class CheckAvailabilityUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val type: CheckAvailabilityViewType
) {
    override fun equals(other: Any?): Boolean {
        return other?.hashCode() == hashCode()
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

data class CheckAvailabilityDateUiModel(
        val dayModel: EventDaysUiModel
) : CheckAvailabilityUiModel(type = CheckAvailabilityViewType.DATE_HEADER)


data class CheckAvailabilityItemUiModel(
        val programDetailsId: String,
        val time: String,
        var status: String?,
        val dayModel: EventDaysUiModel,
        var lastMeetingStatus: String?,
        val sessionType: String,
        val hasJoinedABreakoutSession: Boolean
) : CheckAvailabilityUiModel(type = CheckAvailabilityViewType.ITEM_VIEW)

enum class CheckAvailabilityStatus(val value: String) {
    NO("0"),
    YES("1"),
    NOT_AVAILABLE("2")
//     is null initially which means user has not set the availability status

}

enum class CheckAvailabilityViewType {
    DATE_HEADER,
    ITEM_VIEW
}