package com.khojaleadership.KLF.Activities.Intiative;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.khojaleadership.KLF.Adapter.Intiative.Intiative_RecyclerView_Adapter;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.ActiveFragmentDataModel;
import com.khojaleadership.KLF.Model.ActiveFragmentModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Intiative_Activity_Updated extends AppCompatActivity implements RecyclerViewClickListner {

    RecyclerView recyclerView;
    Intiative_RecyclerView_Adapter adapter;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    ArrayList<ActiveFragmentDataModel> active_initiative_list = new ArrayList<>();
    ActiveFragmentDataModel intiative_data=new ActiveFragmentDataModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intiative___updated);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Initiatives");



        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Intiative_Activity_Updated.this);

                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        getActiveInitiativeListFunction();
    }

    private void getActiveInitiativeListFunction() {

        Call<ActiveFragmentModel> responseCall = retrofitInterface.getActiveInitiativeList("application/json",Common.auth_token);

        responseCall.enqueue(new Callback<ActiveFragmentModel>() {
            @Override
            public void onResponse(Call<ActiveFragmentModel> call, Response<ActiveFragmentModel> response) {

                loading_dialog.dismiss();
                if (response.isSuccessful()) {


                    String status, message;

                    ActiveFragmentModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    ArrayList<ActiveFragmentDataModel> list=new ArrayList<>();

                    list=responseModel.getData();

                    if (list!=null){
                        if (active_initiative_list!=null){
                            active_initiative_list.clear();
                        }
                        active_initiative_list=list;
                        intialize_view();
                    }

                } else {

                }


            }

            @Override
            public void onFailure(Call<ActiveFragmentModel> call, Throwable t) {

                loading_dialog.dismiss();
             }
        });
    }

    public void intialize_view(){

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new Intiative_RecyclerView_Adapter(active_initiative_list,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRowClick(int position) {

        try {

           // Common.intiative_data=active_initiative_list.get(position);
            //Common.intiative_id=active_initiative_list.get(position).getInitiatives__id();

            Intent i=new Intent(Intiative_Activity_Updated.this, Intiative_Activity_Detail_Updated.class);
            i.putExtra("InitiativeId",active_initiative_list.get(position).getInitiatives__id());
            startActivity(i);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    @Override
    public void onViewClcik(int position, View v) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
