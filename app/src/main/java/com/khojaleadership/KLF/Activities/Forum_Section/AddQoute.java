package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddQouteRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.AddQouteResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListDataCommentsModel_Updated;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import jp.wasabeef.richeditor.RichEditor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddQoute extends AppCompatActivity {

    RichEditor mEditor;
    Button add_qoute;
    Dialog loading_dialog;
    ScrollView scrollView;
    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Boolean flag = false;
    TextView header_title;

    ImageButton action_align_left, action_align_center, action_align_right;
    Boolean action_left_flag = false, action_right_flag = false, action_centr_flag = false;


    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();
    GetPostListDataCommentsModel_Updated sub_topic_post_detail = new GetPostListDataCommentsModel_Updated();

    String Subtopic_Id, Category, IsFromDashBoard, ModeratorFlag;

    //String Subtopic_Id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_qoute);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        closeKeyboard();
        loading_dialog = Common.LoadingDilaog(this);

        //getting data
        topic_data_detail = getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");
        sub_topic_post_detail = getIntent().getParcelableExtra("SubtopicPostDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
            Subtopic_Id = bundle.getString("SubtopicId");
            IsFromDashBoard = bundle.getString("IsFromDashBoard");
            ModeratorFlag = bundle.getString("ModeratorFlag");
            if (IsFromDashBoard == null) {
                IsFromDashBoard = "0"; // when data is not returning from dash board....default value is 0
            }

        } else {
            IsFromDashBoard = "0";
        }


        mEditor = (RichEditor) findViewById(R.id.editor);
        add_qoute = (Button) findViewById(R.id.add_qoute_btn);
        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(250);
        mEditor.setScrollbarFadingEnabled(false);
        mEditor.setScrollBarStyle(mEditor.SCROLLBARS_OUTSIDE_OVERLAY);
        mEditor.setScrollBarSize(5);
        mEditor.setOverScrollMode(mEditor.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mEditor.canScrollVertically(0);
        mEditor.setVerticalScrollBarEnabled(true);
        mEditor.setMotionEventSplittingEnabled(true);
        mEditor.setFontSize(12);
        mEditor.setEditorFontSize(12);
        mEditor.setEditorFontColor(Color.BLACK);
        mEditor.setPadding(10, 10, 10, 10);

        scrollView = (ScrollView) findViewById(R.id.parent_scrool_view);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mEditor.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        mEditor.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);

                action_align_left = findViewById(R.id.action_align_left);
                action_align_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_left_flag == false) {
                            action_left_flag = true;
                            action_align_left.setBackgroundResource(R.drawable.ic_left_hover);


                            action_centr_flag = false;
                            action_right_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignLeft();
                        } else if (action_left_flag == true) {
                            action_left_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                        }

                    }
                });

                action_align_center = findViewById(R.id.action_align_center);
                action_align_center.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_centr_flag == false) {
                            action_centr_flag = true;
                            action_align_center.setBackgroundResource(R.drawable.ic_center_hover);


                            action_left_flag = false;
                            action_right_flag = false;
                            action_align_left.setBackgroundResource(R.drawable.ic_left);
                            action_align_right.setBackgroundResource(R.drawable.ic_right);

                            mEditor.setAlignCenter();
                        } else if (action_centr_flag == true) {
                            action_centr_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                        }
                    }
                });

                action_align_right = findViewById(R.id.action_align_right);
                action_align_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (action_right_flag == false) {
                            action_right_flag = true;
                            action_align_right.setBackgroundResource(R.drawable.ic_right_hover);

                            action_centr_flag = false;
                            action_left_flag = false;
                            action_align_center.setBackgroundResource(R.drawable.ic_center);
                            action_align_left.setBackgroundResource(R.drawable.ic_left);

                            mEditor.setAlignRight();
                        } else if (action_right_flag == true) {
                            action_right_flag = false;
                            action_align_right.setBackgroundResource(R.drawable.ic_right);
                        }

                    }
                });


                return false;
            }
        });


        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Quote a Reply");

        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, AddQoute.this);
                Common.hideKeyboard(v, getApplicationContext());
                Intent i = new Intent(AddQoute.this, Sub_Topic_Posts.class);
                i.putExtra("SubtopicId", Subtopic_Id);
                i.putExtra("TopicDataDetail", topic_data_detail);
                i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                i.putExtra("IsFromDashBoard", "1");
                i.putExtra("Category", Category);
                startActivity(i);
                finish();
            }
        });


        try {

            final String quomaString = "<font size=\"4\">&quot;<b></font>" + sub_topic_post_detail.getForumPosts__content() + "<font size=\"4\"></b>&quot;</font>" + "<br> <br> <br>";
            mEditor.setHtml(quomaString);

            add_qoute = (Button) findViewById(R.id.add_qoute_btn);
            add_qoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mEditor.getHtml().equals(quomaString) ||
                            mEditor.getHtml() == null) {
                        Toast.makeText(AddQoute.this, "Comment should not be empty.", Toast.LENGTH_LONG).show();
                    } else {

                        //aik dafa button press kre
                        if (flag == false) {
                            flag = true;
                            loading_dialog.show();
                            onAddQouteBtnClick();
                        }

                    }

                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(AddQoute.this, Sub_Topic_Posts.class);
        i.putExtra("SubtopicId", Subtopic_Id);
        i.putExtra("TopicDataDetail", topic_data_detail);
        i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
        i.putExtra("IsFromDashBoard", "1");
        i.putExtra("Category", Category);
        startActivity(i);
        finish();
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    void onAddQouteBtnClick() {

        try {

            final AddQouteRequestModel requestModel = new AddQouteRequestModel();

            requestModel.setUser_id(Common.login_data.getData().getId());
            //requestModel.setParent_id(String.valueOf(Common.sub_topic_edit_form_id));
            requestModel.setParent_id(String.valueOf(Subtopic_Id));
            requestModel.setLft("300");
            requestModel.setRght("300");


            // requestModel.setTopic(title.getText().toString());
            requestModel.setContent("<quote>" + mEditor.getHtml() + "</quote>");


            if (Common.is_Admin_flag == true) {
                requestModel.setStatuspost("A");
            } else {
                if (ModeratorFlag != null) {
                    if (ModeratorFlag.equals("1")) {
                        requestModel.setStatuspost("P");
                    } else if (ModeratorFlag.equals("0")) {
                        requestModel.setStatuspost("A");
                    }
                } else {
                    requestModel.setStatuspost("A");
                }
            }

            Call<AddQouteResponseModel> responseCall = retrofitInterface.AddQoute("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddQouteResponseModel>() {
                @Override
                public void onResponse(Call<AddQouteResponseModel> call, Response<AddQouteResponseModel> response) {
                    if (response.isSuccessful()) {

                        String status, message;

                        AddQouteResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();
                        loading_dialog.dismiss();
                        Intent i = new Intent(AddQoute.this, Sub_Topic_Posts.class);
                        i.putExtra("SubtopicId", Subtopic_Id);
                        i.putExtra("TopicDataDetail", topic_data_detail);
                        i.putExtra("SubtopicDataDetail", sub_topic_data_detail);
                        i.putExtra("IsFromDashBoard", "1");
                        i.putExtra("Category", Category);
                        startActivity(i);
                        finish();

                        Common.Toast_Message(AddQoute.this, "Post Qouted successfully ");
                        //Confirm_Dialog("Quote", message, "OK");

                    } else {
                    }
                }

                @Override
                public void onFailure(Call<AddQouteResponseModel> call, Throwable t) {
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    void Confirm_Dialog(String title, String message, String btn) {

        TextView title_text, message_text, btn_text;

        // custom dialog
        final Dialog dialog = new Dialog(AddQoute.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aa_confirm_dialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.getWindow().setLayout(275, 350);


        title_text = (TextView) dialog.findViewById(R.id.custom_title);
        message_text = (TextView) dialog.findViewById(R.id.custom_message);
        btn_text = (TextView) dialog.findViewById(R.id.custom_btn);

        title_text.setText(title);
        message_text.setText(message);
        btn_text.setText(btn);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent post_intent = new Intent(AddQoute.this, Sub_Topic_Posts.class);
                startActivity(post_intent);
                finish();

            }
        });


        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
