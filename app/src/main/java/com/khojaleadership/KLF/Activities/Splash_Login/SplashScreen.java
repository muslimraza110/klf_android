package com.khojaleadership.KLF.Activities.Splash_Login;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.khojaleadership.KLF.Activities.Dashboard.MainActivity;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Helper.ConnectionDetector;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginRequestModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseDataModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseModel;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore;

import java.util.concurrent.atomic.AtomicReference;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class SplashScreen extends AppCompatActivity {
    SharedPreferences sp1;
    String unm, pass, role, logout;

    RetrofitInterface retrofitInterface = Common.initRetrofit();

    Dialog loading_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        //init gif loader
        loading_dialog = Common.LoadingDilaog(this);

        //init shared preferences
        sp1 = this.getSharedPreferences("Login", 0);

        //getting email and password
        unm = sp1.getString("Unm", null);
        pass = sp1.getString("Psw", null);
        role = sp1.getString("role", null);
        logout = sp1.getString("logout", null);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (new ConnectionDetector(SplashScreen.this).isConnectingToInternet()) {
                    //do your task
                    Common.internetConnection = true;

                    if (unm != null && pass != null && logout != null) {
                        if (logout.equals("0")) {
                            getUserLoginData(unm, pass);
                        } else if (logout.equals("1")) {
                            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                            finish();
                        }

                    } else {
                        startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                        finish();
                    }

                } else {
                    Common.internetConnection = false;
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    finish();
                }


                ImageView imageView = (ImageView) findViewById(R.id.image_view);
                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
                Glide.with(SplashScreen.this).load(R.drawable.splash_screen_gif_2).into(imageView);
                //SplashScreen.this.finish();
            }
        }, 1000 * 3);
    }

    //getting data from saved user and password
    public void getUserLoginData(String email, String password) {


        LoginRequestModel request = new LoginRequestModel();

        AtomicReference<String> fcmToken = new AtomicReference<>("");
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(this, token -> {
            if(token != null){
                fcmToken.set(token);
            }
            Timber.e(token);

            request.setPlatform("2");//for android devices
            request.setEmail(email);
            request.setPassword(password);
            request.setFcm_token(fcmToken.get());


            Call<LoginResponseModel> responseCall = retrofitInterface.getLoginAccess("application/json", request);

            responseCall.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                    int statusCode = response.code();

                    if (response.isSuccessful()) {
                        if (statusCode == 200) {


                            LoginResponseModel responseModel = response.body();
                            Common.login_data = responseModel;
                            new SharedPreferencesAuthStore(
                                    SplashScreen.this,
                                    new Gson()
                            ).setLoginData(responseModel.getData());


                            if (responseModel.getStatus().equals("success")) {
                                LoginResponseDataModel data = responseModel.getData();

                                Common.auth_token = data.getAuth_token();

                                if (data.getRole().equals("A")) {
                                    //Common.Role = "Admin";
                                    Common.is_Admin_flag = true;
                                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                } else if (data.getRole().equals("C")) {
                                    //Common.Role = "Member";
                                    Common.is_Admin_flag = false;
                                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }


                            } else if (responseModel.getStatus().equals("error")) {
                                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                                finish();
                            }


                        } else {
                            //Server is not reponding at the moment
                            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                            finish();
                        }
                    } else {
                        //Server is not reponding at the moment
                        startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    //Server is not reponding at the moment
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    finish();
                }
            });

        });



    }
}
