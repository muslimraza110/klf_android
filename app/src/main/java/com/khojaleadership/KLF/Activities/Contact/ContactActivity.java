package com.khojaleadership.KLF.Activities.Contact;

import android.app.Dialog;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.ContactPinyinComparator;
import com.khojaleadership.KLF.Contact_Layout.ContactSortAdapter;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Contact.GetContactDataListModel;
import com.khojaleadership.KLF.Model.Contact.GetContactListModel;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private ContactSortAdapter adapter;
    List<GetContactDataListModel> Common_Contact_list = new ArrayList<>();
    private List<GetContactDataListModel> sourceDataList;
    List<GetContactDataListModel> filterDataList = new ArrayList<>();

    private ContactPinyinComparator pinyinComparator;
    public Boolean filter_flag = false;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aa_activity_contact);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Contacts");
        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v,ContactActivity.this);
                finish();
            }
        });


        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        getContactListFunction();
        //initViews();
    }

    private void getContactListFunction() {

        Call<GetContactListModel> responseCall = retrofitInterface.getContactList("application/json", Common.auth_token);
        responseCall.enqueue(new Callback<GetContactListModel>() {
            @Override
            public void onResponse(Call<GetContactListModel> call, Response<GetContactListModel> response) {

                if (response.isSuccessful()) {
                    String status, message;

                    GetContactListModel responseModel = response.body();
                    status = responseModel.getStatus();
                    message = responseModel.getMessage();

                    if (status.equals("success")) {

//                        if (Common.main_contact_list != null) {
//                            Common.main_contact_list.clear();
//                        }

                        Common_Contact_list=responseModel.getData();
                        //Common.main_contact_list = responseModel.getData();

                        initViews();
                    }


                } else {
                    loading_dialog.dismiss();
                 }
            }

            @Override
            public void onFailure(Call<GetContactListModel> call, Throwable t) {
                initViews();
             }
        });

    }

    private void initViews() {

        try {
            pinyinComparator = new ContactPinyinComparator();

            sideBar = (SideBar) findViewById(R.id.sideBar);
            dialog = (TextView) findViewById(R.id.dialog);
            sideBar.setTextView(dialog);
            sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
                @Override
                public void onTouchingLetterChanged(String s) {
                    int position = adapter.getPositionForSection(s.charAt(0));

                    if (position != -1) {
                        manager.scrollToPositionWithOffset(position, 0);
                    }
                }
            });

            mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            //sourceDataList = filledData(Common.main_contact_list);
            sourceDataList = filledData(Common_Contact_list);

            Collections.sort(sourceDataList, pinyinComparator);


            manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(manager);

            adapter = new ContactSortAdapter(this, sourceDataList, this);
            mRecyclerView.setAdapter(adapter);

            loading_dialog.dismiss();

            mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);
            mClearEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    filterData(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }

    private List<GetContactDataListModel> filledData(List<GetContactDataListModel> data) {

        ArrayList<String> alphabets=new ArrayList<>();

        List<GetContactDataListModel> mSortList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            GetContactDataListModel sortModel = new GetContactDataListModel();

            sortModel.setUsers__title(data.get(i).getUsers__title());
            sortModel.setUsers__id(data.get(i).getUsers__id());
            sortModel.setUsers__role(data.get(i).getUsers__role());
            sortModel.setUsers__first_name(data.get(i).getUsers__first_name());
            sortModel.setUsers__last_name(data.get(i).getUsers__last_name());
            sortModel.setUsers__email(data.get(i).getUsers__email());
            sortModel.setUserDetails__title(data.get(i).getUserDetails__title());
            sortModel.setUserDetails__description(data.get(i).getUserDetails__description());
            sortModel.setUserDetails__address(data.get(i).getUserDetails__address());
            sortModel.setUserDetails__address2(data.get(i).getUserDetails__address2());
            sortModel.setUserDetails__phone(data.get(i).getUserDetails__phone());

            sortModel.setUserDetails__phone(data.get(i).getUserDetails__phone());
            sortModel.setUserDetails__email2(data.get(i).getUserDetails__email2());
            sortModel.setUserDetails__website(data.get(i).getUserDetails__website());
            sortModel.setUserDetails__facebook(data.get(i).getUserDetails__facebook());
            sortModel.setProfileimg(data.get(i).getProfileimg());

            String pinyin_english = data.get(i).getUsers__first_name();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {
                //group name is empty  added by some one.
                sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();
                if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }

        return mSortList;

    }

    private void filterData(String filterStr) {

        try {

            filter_flag = true;
            if (TextUtils.isEmpty(filterStr)) {
                filterDataList = sourceDataList;
//                sourceDataList = filledData(Common.main_contact_list);
                sourceDataList = filledData(Common_Contact_list);
                Collections.sort(sourceDataList, pinyinComparator);
            } else {
                filterDataList.clear();

                for (GetContactDataListModel sortModel : sourceDataList) {
                    String name = sortModel.getUsers__first_name();
                    if (name.indexOf(filterStr.toString()) != -1
                            || name.toLowerCase().contains(filterStr)
                            || name.toUpperCase().contains(filterStr)
                            || sortModel.getUsers__last_name().toLowerCase().contains(filterStr)
                            || sortModel.getUsers__last_name().toUpperCase().contains(filterStr)
                            || sortModel.getUserDetails__title().toLowerCase().contains(filterStr)
                            || sortModel.getUserDetails__title().toUpperCase().contains(filterStr)
                            || sortModel.getUsers__email().toLowerCase().contains(filterStr)
                            || sortModel.getUsers__email().toUpperCase().contains(filterStr)
                            || sortModel.getUserDetails__phone().toLowerCase().contains(filterStr)
                            || sortModel.getUserDetails__phone().toUpperCase().contains(filterStr)
                    ) {

                        filterDataList.add(sortModel);
                    }

                }
            }
            Collections.sort(filterDataList, pinyinComparator);
            adapter.updateList(filterDataList);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

    }
    @Override
    public void onRowClick(int position) {

        GetContactDataListModel data=new GetContactDataListModel();

        try {
            if (filter_flag == true) {
                data=filterDataList.get(position);
//                Common.main_contact_list_data = filterDataList.get(position);
            } else {
                data=sourceDataList.get(position);
//                Common.main_contact_list_data = sourceDataList.get(position);
            }

            //Common.master_search_view_profile = true;
            //Common.view_profile_id = Common.main_contact_list_data.getUsers__id();

            //....................contact screen changed
            Intent detail = new Intent(ContactActivity.this, View_Profile_Activity_Updated.class);
            //detail.putExtra("Contactdata",data);
            detail.putExtra("view_profile_id",data.getUsers__id());
            detail.putExtra("isMasterSearch","1");
            startActivity(detail);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public void onViewClcik(int position, View v) {
        //delete and edit functionality here
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }

}

