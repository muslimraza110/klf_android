package com.khojaleadership.KLF.Activities.Forum_Section;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Forum.WhitelistGroupAdapterUpdated;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForWhitelistGroup_Updated;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddGroupToWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.AddGroupToWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetWhitelistedGroup_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetWhitelistedGroup_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveGroupFromWhitelistRequestModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.RemoveGroupFromWhitelistResponseModel;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WhitelistGroupScreenUpdated extends AppCompatActivity implements RecyclerViewClickListner {

    private ClearEditText mClearEditText;

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;

    LinearLayoutManager manager;

    private WhitelistGroupAdapterUpdated adapter;
    private List<GetWhitelistedGroup_Data_Model> sourceDataList;
    List<GetWhitelistedGroup_Data_Model> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;

    private PinyinComparatorForWhitelistGroup_Updated pinyinComparator;
    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;

    String Category;
    GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();

    List<GetWhitelistedGroup_Data_Model> is_whitelisted_group_list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whitelist_contacts);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Whitelist Groups");


        topic_data_detail=getIntent().getParcelableExtra("TopicDataDetail");
        sub_topic_data_detail = getIntent().getParcelableExtra("SubtopicDataDetail");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Category = bundle.getString("Category");
        }

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, WhitelistGroupScreenUpdated.this);

                finish();
            }
        });

        findViewById(R.id.whitelist_contact_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listner = this;

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();

        GetIsWhitelistedGroupFunction();
        //onAlreadyWhitelistedContactFunction();
        //onContactBtnClick();
        //initViews();
    }


    private void initViews() {

        loading_dialog.dismiss();

        pinyinComparator = new PinyinComparatorForWhitelistGroup_Updated();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        //设置右侧SideBar触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        sourceDataList = filledData(is_whitelisted_group_list);
        // 根据a-z进行排序源数据
        Collections.sort(sourceDataList, pinyinComparator);

        //RecyclerView配置manager
        manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        adapter = new WhitelistGroupAdapterUpdated(getApplicationContext(), sourceDataList, listner);
        mRecyclerView.setAdapter(adapter);


        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);

        //根据输入框输入值的改变来过滤搜索
        mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }



    private List<GetWhitelistedGroup_Data_Model> filledData(List<GetWhitelistedGroup_Data_Model> data) {
        List<GetWhitelistedGroup_Data_Model> mSortList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            GetWhitelistedGroup_Data_Model sortModel = new GetWhitelistedGroup_Data_Model();

            String id,name,email,phone;
            int iswhitelisted;

            sortModel.setId(data.get(i).getId());
            sortModel.setName(data.get(i).getName());
            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setPhone(data.get(i).getPhone());
            sortModel.setIswhitelisted(data.get(i).getIswhitelisted());


            String pinyin_english = data.get(i).getName();


            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {
                //group name is empty  added by some one.
                sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();

                //正则表达式，判断首字母是否是英文字母
                if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }
            }


            mSortList.add(sortModel);
        }


        return mSortList;
    }

    private void filterData(String filterStr) {

        filter_flag = true;

        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(is_whitelisted_group_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (GetWhitelistedGroup_Data_Model sortModel : sourceDataList) {
                String name = sortModel.getName();


                if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().startsWith(filterStr)
                        || name.toUpperCase().startsWith(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
        // 根据a-z进行排序
        Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }

    private void GetIsWhitelistedGroupFunction() {

        try {

            Call<GetWhitelistedGroup_Model> call = retrofitInterface.getWhitelistedGroupsUpdated("application/json",Common.auth_token,
                    sub_topic_data_detail.getId());
            call.enqueue(new Callback<GetWhitelistedGroup_Model>() {
                @Override
                public void onResponse(Call<GetWhitelistedGroup_Model> call, Response<GetWhitelistedGroup_Model> response) {
                     if (response.isSuccessful()) {
                        String status, message;


                        GetWhitelistedGroup_Model responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                           ArrayList<GetWhitelistedGroup_Data_Model> list = new ArrayList<>();
                        list = responseModel.getData();

                        if (list != null) {

                            is_whitelisted_group_list = list;
                            initViews();
                        }

                    }
                }

                @Override
                public void onFailure(Call<GetWhitelistedGroup_Model> call, Throwable t) {

                    initViews();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }



    @Override
    public void onRowClick(int position) {
     }

    @Override
    public void onViewClcik(int position, View v) {

        GetWhitelistedGroup_Data_Model is_whitelisted_group_data_detail=new GetWhitelistedGroup_Data_Model();

        try {
            switch (v.getId()) {
                case R.id.whitelist_contact_checkbox:
                    if (filter_flag == true) {
                        is_whitelisted_group_data_detail = filterDataList.get(position);
                    } else {
                        is_whitelisted_group_data_detail = sourceDataList.get(position);
                    }

                    String group_id = is_whitelisted_group_data_detail.getId();
                    String post_id = sub_topic_data_detail.getId();
                    int flag=is_whitelisted_group_data_detail.getIswhitelisted();

                    if (flag == 0) {
                        //now add to white list
                         loading_dialog.show();
                        onAddGroupToWhitelistBtnClick(group_id,post_id);
                    } else if (flag == 1) {
                        //now remove to white list
                         loading_dialog.show();
                        onRemoveGroupFromWhitelistBtnClick(group_id,post_id);
                    }

                    break;
                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    private void onAddGroupToWhitelistBtnClick(String group_id,String post_id) {


        try {

            AddGroupToWhitelistRequestModel requestModel = new AddGroupToWhitelistRequestModel();

            requestModel.setGroup_id(group_id);
            requestModel.setPost_id(post_id);  //default



            Call<AddGroupToWhitelistResponseModel> responseCall = retrofitInterface.AddGroupToWhiteList("application/json",Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddGroupToWhitelistResponseModel>() {
                @Override
                public void onResponse(Call<AddGroupToWhitelistResponseModel> call, Response<AddGroupToWhitelistResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        //loading_dialog.dismiss();

                        AddGroupToWhitelistResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        GetIsWhitelistedGroupFunction();

                        Toast.makeText(WhitelistGroupScreenUpdated.this,"Group added to Whitelist successfully",Toast.LENGTH_SHORT).show();

                    } else {
                        loading_dialog.dismiss();
                        Toast.makeText(WhitelistGroupScreenUpdated.this,"Group added to Whitelist Failed",Toast.LENGTH_SHORT).show();
                     }

                }

                @Override
                public void onFailure(Call<AddGroupToWhitelistResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(WhitelistGroupScreenUpdated.this,"Group added to Whitelist Failed",Toast.LENGTH_SHORT).show();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    private void onRemoveGroupFromWhitelistBtnClick(String group_id, String post_id) {


        try {


            RemoveGroupFromWhitelistRequestModel requestModel = new RemoveGroupFromWhitelistRequestModel();

            requestModel.setGroup_id(group_id);
            requestModel.setPost_id(post_id);  //default


            Call<RemoveGroupFromWhitelistResponseModel> responseCall = retrofitInterface.RemoveGroupFromWhiteList("application/json",Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<RemoveGroupFromWhitelistResponseModel>() {
                @Override
                public void onResponse(Call<RemoveGroupFromWhitelistResponseModel> call, Response<RemoveGroupFromWhitelistResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        //loading_dialog.dismiss();

                        RemoveGroupFromWhitelistResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        GetIsWhitelistedGroupFunction();

                        Toast.makeText(WhitelistGroupScreenUpdated.this, "Group Remove to Whitelist successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        loading_dialog.dismiss();
                        Toast.makeText(WhitelistGroupScreenUpdated.this, "Group Remove to Whitelist Failed", Toast.LENGTH_SHORT).show();
                     }

                }

                @Override
                public void onFailure(Call<RemoveGroupFromWhitelistResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(WhitelistGroupScreenUpdated.this, "Group Remove to Whitelist Failed", Toast.LENGTH_SHORT).show();
                 }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
