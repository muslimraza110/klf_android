package com.khojaleadership.KLF.Activities.event_new.programme

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.userProfile.UserProfileActivity
import com.khojaleadership.KLF.Adapter.event_new.programme.EventProgrammeDaysAdapter
import com.khojaleadership.KLF.Adapter.event_new.programme.EventProgrammeDetailsAdapter
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import com.khojaleadership.KLF.databinding.ActivityEventProgrammeBinding

class EventProgrammeActivity : AppCompatActivity() {


    private lateinit var binding: ActivityEventProgrammeBinding
    private val viewModel: EventProgrammeViewModel by viewModels()

    private lateinit var daysAdapter: EventProgrammeDaysAdapter
    private lateinit var programmeDetailsAdapter: EventProgrammeDetailsAdapter

    private val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = application,
                gson = Gson()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Hide status bar
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        super.onCreate(savedInstanceState)
        binding = ActivityEventProgrammeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.daysData.observe(this, Observer {
            it?.let {
                if(it.isEmpty()){
                    binding.tvError.visibility = View.VISIBLE
                    binding.rvProgrammeDetails.visibility = View.INVISIBLE
                }else{
                    binding.tvError.visibility = View.GONE
                    binding.rvProgrammeDetails.visibility = View.VISIBLE
                }
                daysAdapter.submitList(it)
                daysAdapter.notifyDataSetChanged()
            }
        })

        viewModel.programmeDetails.observe(this, Observer {
            it?.let {
                programmeDetailsAdapter.submitList(it.programmeDetailItems)
                binding.tvDate.text = it.dayData.date
            }
        })


        viewModel.eventState.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.translucentView.visibility = View.VISIBLE
                    binding.rvProgrammeDetails.visibility = View.GONE
                    binding.rvDays.visibility = View.GONE
                }
                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvProgrammeDetails.visibility = View.VISIBLE
                    binding.rvDays.visibility = View.VISIBLE
                    binding.tvError.text = it.message
                }
                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.translucentView.visibility = View.GONE
                    binding.rvProgrammeDetails.visibility = View.GONE
                    binding.rvDays.visibility = View.GONE
                    binding.tvError.text = it.message
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setUpViews() {
        daysAdapter = EventProgrammeDaysAdapter(
                this,
                onClick = { day, position ->
                    viewModel.changeSelectedDay(position)
                }
        )
        daysAdapter.setHasStableIds(true)
        binding.rvDays.apply {
            adapter = daysAdapter
            layoutManager = LinearLayoutManager(this@EventProgrammeActivity, LinearLayoutManager.HORIZONTAL, false)
        }


        programmeDetailsAdapter = EventProgrammeDetailsAdapter(
                this,
                onHyperLinkClick = {
//                    authStore.lastSelectedPersonAvailability = it.availableSlotData
                    val intent = Intent(this, UserProfileActivity::class.java)
                    intent.putExtra(UserProfileActivity.KEY_FACULTY_ID, it.summitEventsFacultyId)
                    intent.putExtra(UserProfileActivity.KEY_USER_ID, it.userId)
                    intent.putExtra(UserProfileActivity.KEY_IS_DELEGATE, it.isDelegate)
                    intent.putExtra(UserProfileActivity.KEY_USER_FB, it.facebook)
                    intent.putExtra(UserProfileActivity.KEY_USER_TWITTER, it.twitter)
                    intent.putExtra(UserProfileActivity.KEY_USER_LINKED_IN, it.linkedIn)
                    startActivity(intent)
                }
        )
        programmeDetailsAdapter.setHasStableIds(true)
        binding.rvProgrammeDetails.apply {
            itemAnimator = null
            adapter = programmeDetailsAdapter
            layoutManager = LinearLayoutManager(this@EventProgrammeActivity)
        }

        binding.imageBack.setOnClickListener {
            finish()
        }


    }



}
