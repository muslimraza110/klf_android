package com.khojaleadership.KLF.Activities.event_new.planner.myItinerary

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.planner.EventPlannerViewModel
import com.khojaleadership.KLF.Activities.event_new.planner.sessions.SessionsActivity
import com.khojaleadership.KLF.Activities.event_new.userProfile.UserProfileActivity
import com.khojaleadership.KLF.Adapter.event_new.planner.MyItinerary.CheckMyAvailabilityAdapter
import com.khojaleadership.KLF.Adapter.event_new.planner.MyItinerary.MyItineraryDetailsAdapter
import com.khojaleadership.KLF.Adapter.event_new.programme.EventProgrammeDaysAdapter
import com.khojaleadership.KLF.Helper.Result
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.data.AuthStore
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import com.khojaleadership.KLF.databinding.LayoutMeetingNotesBinding
import com.khojaleadership.KLF.databinding.LayoutMeetingNotesNoSendBinding
import com.khojaleadership.KLF.databinding.MyItineraryFragmentBinding


class MyItineraryFragment : Fragment() {

    companion object {
        fun newInstance() = MyItineraryFragment()
    }

    private val viewModel: MyItineraryViewModel by viewModels()
    private val parentViewModel: EventPlannerViewModel by activityViewModels()
    lateinit var meetingNotesBinding: LayoutMeetingNotesBinding
    lateinit var meetingNotesNoSendBinding: LayoutMeetingNotesNoSendBinding


    private lateinit var binding: MyItineraryFragmentBinding

    private lateinit var daysAdapter: EventProgrammeDaysAdapter
    private lateinit var myItineraryAdapter: MyItineraryDetailsAdapter
    private lateinit var checkAvailabilityAdapter: CheckMyAvailabilityAdapter

    private var alertDialog: AlertDialog? = null


    private val authStore: AuthStore by lazy {
        SharedPreferencesAuthStore(
                context = requireContext(),
                gson = Gson()
        )
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = MyItineraryFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpViews()
        setUpObservers()
    }

    private fun setUpObservers() {

        parentViewModel.sendNotesEvent.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    meetingNotesBinding.progressBar.visibility = View.VISIBLE
                    meetingNotesBinding.image.visibility = View.INVISIBLE
                    meetingNotesBinding.name.visibility = View.INVISIBLE
                    meetingNotesBinding.editText.isEnabled = false
                    meetingNotesBinding.imageClose.isEnabled = false
                }
                is Result.Success -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                    alertDialog?.dismiss()
                    alertDialog = null

                }
                is Result.Error -> {
                    meetingNotesBinding.progressBar.visibility = View.GONE
                    meetingNotesBinding.image.visibility = View.VISIBLE
                    meetingNotesBinding.name.visibility = View.VISIBLE
                    meetingNotesBinding.editText.isEnabled = true
                    meetingNotesBinding.imageClose.isEnabled = true
                }
            }
        })


        parentViewModel.messageEvent.observe(viewLifecycleOwner, Observer {
            it?.consume()?.let { message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
        })

        parentViewModel.daysData.observe(viewLifecycleOwner, Observer {
            it?.let {

                if (it.isEmpty()) {
                    parentViewModel.changeViewPagerVisibility(false)
                } else {
                    parentViewModel.changeViewPagerVisibility(true)
                }

                daysAdapter.submitList(it)
                daysAdapter.notifyDataSetChanged()
                if (parentViewModel.isDataUpdated) {
                    // Hack
                    binding.rvDays.postDelayed(Runnable {
                        // or use other API
                        binding.rvDays.scrollToPosition(parentViewModel.lastSelectedDayIndex)

                        // give a delay of 0.3 second
                    }, 300)
                    parentViewModel.isDataUpdated = false
                }
            }
        })


        parentViewModel.myItinerary.observe(viewLifecycleOwner, Observer {
            it?.let {

                val myItineraryItems = it.myItineraryItems.toMutableList()
                val sessionModels = it.myItineraryItems.filterIsInstance<MyItinerarySessionUiModel>().filter { !it.isExpanded }

                sessionModels.forEach { sessionModel ->
                    myItineraryItems.removeAll {
                        it.type != MyItineraryItemType.SESSION_DETAILS && sessionModel.programDetailsId == (it as? MyItineraryRequestItemUiModel)?.programDetailsId
                    }
                }

//                val recyclerViewState: Parcelable? = binding.rvMyItinerary.layoutManager?.onSaveInstanceState()
                myItineraryAdapter.submitList(myItineraryItems) {
                    if (parentViewModel.clickedSessionPosition == 0)
                        binding.rvMyItinerary.scrollToPosition(0)
                }
//                if (parentViewModel.clickedSessionPosition == 0)
//                    binding.rvMyItinerary.postDelayed(Runnable {
//
//                        binding.rvMyItinerary.scrollToPosition(0)
//
//                        // give a delay of 10 milliseconds
//                    }, 10)

//                binding.rvMyItinerary.layoutManager?.onRestoreInstanceState(recyclerViewState)

                binding.tvDate.text = it.dayData.date
                if (parentViewModel.scrollToSpecificPosition) {
                    binding.rvMyItinerary.postDelayed({
                        binding.rvMyItinerary.smoothScrollToPosition(myItineraryItems.size - 1)
                    }, 500)
                    parentViewModel.scrollToSpecificPosition = false
                }
            }
        })


        parentViewModel.myAvailability.observe(viewLifecycleOwner, Observer {
            it?.let {
                checkAvailabilityAdapter.submitList(it)
            }
        })

        viewModel.checkAvailabilityToggle.observe(viewLifecycleOwner, Observer {
            it?.let {
                parentViewModel.isCheckAvailabilitySelected = it
                if (it) {
                    // Show check availability rv
                    binding.rvCheckAvailability.visibility = View.VISIBLE
                    binding.rvMyItinerary.visibility = View.GONE
                    binding.rvDays.visibility = View.GONE
                    binding.tvDate.visibility = View.GONE
                    binding.imageBack.visibility = View.VISIBLE

                    binding.buttonCheckAvailability.background = ContextCompat.getDrawable(requireContext(), R.drawable.linear_gradient_blue_cyan_round)
                    binding.buttonCheckAvailability.setTextColor(ContextCompat.getColor(requireContext(), R.color.appWhite))

                } else {
                    // Show my itinerary rv

                    parentViewModel.getEventProgramDetails()

                    binding.rvCheckAvailability.visibility = View.GONE
                    binding.rvMyItinerary.visibility = View.VISIBLE
                    binding.rvDays.visibility = View.VISIBLE
                    binding.tvDate.visibility = View.VISIBLE
                    binding.imageBack.visibility = View.INVISIBLE

                    binding.buttonCheckAvailability.background = ContextCompat.getDrawable(requireContext(), R.drawable.grey_outlined_bg)
                    binding.buttonCheckAvailability.setTextColor(ContextCompat.getColor(requireContext(), R.color.appGrey))

                }
            }
        })

        parentViewModel.checkAvailabilityToggle.observe(viewLifecycleOwner, Observer {
            it.consume()?.let {
                viewModel.toggleCheckAvailability()
            }
        })


    }

    private fun setUpViews() {
        daysAdapter = EventProgrammeDaysAdapter(
                requireContext(),
                onClick = { day, position ->
                    parentViewModel.changeSelectedDay(day, position)
                }
        )
        daysAdapter.setHasStableIds(true)
        binding.rvDays.apply {
            adapter = daysAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }


        myItineraryAdapter = MyItineraryDetailsAdapter(
                requireContext(),
                onRequestMeetingClick = { programDetailsId ->
                    parentViewModel.requestMeetingCheckboxClick(programDetailsId)
                },
                onHyperLinkClick = {
//                    authStore.lastSelectedPersonAvailability = it.availableSlotData
                    val intent = Intent(requireContext(), UserProfileActivity::class.java)
                    intent.putExtra(UserProfileActivity.KEY_FACULTY_ID, it.summitEventsFacultyId)
                    intent.putExtra(UserProfileActivity.KEY_USER_ID, it.userId)
                    intent.putExtra(UserProfileActivity.KEY_IS_DELEGATE, it.isDelegate)
                    intent.putExtra(UserProfileActivity.KEY_USER_FB, it.facebook)
                    intent.putExtra(UserProfileActivity.KEY_USER_TWITTER, it.twitter)
                    intent.putExtra(UserProfileActivity.KEY_USER_LINKED_IN, it.linkedIn)
                    startActivity(intent)
                },
                onCancelMeetingClick = { item ->
                    showCancelMeetingDialog(item)
                },
                onListIconClick = { item ->
//                    if (item.requestedBy == Common.login_data?.data?.id) {
                    openMeetingNotesDialog(item)
//                    } else {
//                        openMeetingNotesDialog(item.meetingNotes)

//                    }
                },
                onChatIconClick = { item ->
                    openMeetingNotesDialog(item)
                },
                onAcceptRejectClick = { accept, item ->
                    acceptRejectMeeting(accept, item)
                },
                onBtnBreakoutSessionsClick = {
//                    authStore.sessionsModel = it
//                    requireContext().startActivity(Intent(requireContext(), BreakoutSessionsActivity::class.java))
                },
                onSessionTextClick = {
                    authStore.sessionsModel = it
                    requireContext().startActivity(Intent(requireContext(), SessionsActivity::class.java))
                },
                onDropDownClick = {
                    parentViewModel.toggleIsExpanded(it)
                }
        )
        myItineraryAdapter.setHasStableIds(true)
        binding.rvMyItinerary.apply {
            itemAnimator = null
            adapter = myItineraryAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        binding.buttonCheckAvailability.setOnClickListener {
            viewModel.toggleCheckAvailability()
        }


        checkAvailabilityAdapter = CheckMyAvailabilityAdapter(
                onClick = { checkAvailabilityItemUiModel ->
                    if (checkAvailabilityItemUiModel.status != CheckAvailabilityStatus.NOT_AVAILABLE.value) {
                        parentViewModel.updateAvailability(checkAvailabilityItemUiModel)
                    }
                }
        )
        checkAvailabilityAdapter.setHasStableIds(true)
        binding.rvCheckAvailability.apply {
            itemAnimator = null
            adapter = checkAvailabilityAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        binding.imageBack.setOnClickListener {
            // Show my itinerary rv
            viewModel.toggleCheckAvailability()
        }

        binding.imageDropdown.setOnClickListener {
            if (binding.tvItinerarytext.maxLines == Integer.MAX_VALUE) {
                binding.tvItinerarytext.maxLines = 1
                binding.imageDropdown.setImageResource(R.drawable.action_drop_down)
            } else {
                binding.tvItinerarytext.maxLines = Integer.MAX_VALUE
                binding.imageDropdown.setImageResource(R.drawable.action_drop_up)
            }
        }
    }

    private fun acceptRejectMeeting(accept: Boolean, item: MyItineraryRequestItemUiModel) {

        if (accept) {
            parentViewModel.acceptOrRejectRequest(accept, item)
        } else {
            val message = "Are you sure you want to reject this meeting request?"
            val dialog = AlertDialog.Builder(requireContext())
                    .setMessage(message)
                    .setPositiveButton("YES") { dialog, which ->
                        parentViewModel.acceptOrRejectRequest(accept, item)
                    }
                    .setNegativeButton("NO") { dialog, which ->
                        dialog.dismiss()
                    }
                    .create()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
            }
            dialog.show()
        }

    }

    private fun openMeetingNotesDialog(meetingNotes: String) {

        meetingNotesNoSendBinding = LayoutMeetingNotesNoSendBinding.inflate(layoutInflater)
        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setView(meetingNotesNoSendBinding.root)
        alertDialog.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        alertDialog.show()
        alertDialog.setCanceledOnTouchOutside(false)


        meetingNotesNoSendBinding.editText.setText(meetingNotes)

        meetingNotesNoSendBinding.imageClose.setOnClickListener {
            alertDialog.dismiss()
        }


    }


    private fun showCancelMeetingDialog(item: MyItineraryRequestItemUiModel) {
        val message =
                if (item.type == MyItineraryItemType.REQUEST_RECEIVED || item.type == MyItineraryItemType.REQUEST_SENT)
                    "Are you sure you want to cancel this meeting request?"
                else "Are you sure you want to cancel this meeting?"
        val dialog = AlertDialog.Builder(requireContext())
                .setMessage(message)
                .setPositiveButton("YES") { dialog, which ->
                    parentViewModel.cancelMeeting(item)
                }
                .setNegativeButton("NO") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireContext(), R.color.appBlue))
        }
        dialog.show()
    }


    private fun openMeetingNotesDialog(item: MyItineraryRequestItemUiModel) {

        meetingNotesBinding = LayoutMeetingNotesBinding.inflate(layoutInflater)
        meetingNotesBinding.spinnerTime.visibility = View.GONE
        meetingNotesBinding.spinnerDate.visibility = View.GONE
        meetingNotesBinding.editText.setText(item.meetingNotes)
        meetingNotesBinding.name.text = "Update Notes"
        alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog?.setView(meetingNotesBinding.root)
        alertDialog?.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        alertDialog?.show()
        alertDialog?.setCanceledOnTouchOutside(false)


        meetingNotesBinding.imageClose.setOnClickListener {
            alertDialog?.dismiss()
        }

        meetingNotesBinding.layoutSendNotes.setOnClickListener {

            parentViewModel.updateMeetingNotes(
                    item = item,
                    meetingNotes = meetingNotesBinding.editText.text?.toString() ?: ""
            )

        }
    }


}
