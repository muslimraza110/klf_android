package com.khojaleadership.KLF.Activities.Group;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.khojaleadership.KLF.Adapter.Group.AddMemberInGroupAdapter;
import com.khojaleadership.KLF.Contact_Layout.ClearEditText;
import com.khojaleadership.KLF.Contact_Layout.PinyinComparatorForAddMember;
import com.khojaleadership.KLF.Contact_Layout.SideBar;
import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Group_Models.AddMemberToGroupRequestModel;
import com.khojaleadership.KLF.Model.Group_Models.AddMemberToGroupResponseModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListDataModel;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListModel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupResponseMOdel;
import com.khojaleadership.KLF.Model.Group_Models.RemoveMemberFromGroupRequestModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Member_In_Group extends AppCompatActivity implements RecyclerViewClickListner {
    private ClearEditText mClearEditText;
    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;
    LinearLayoutManager manager;
    private AddMemberInGroupAdapter adapter;
    private List<GetGroupMemberListDataModel> sourceDataList;
    List<GetGroupMemberListDataModel> filterDataList = new ArrayList<>();
    public Boolean filter_flag = false;
    private PinyinComparatorForAddMember pinyinComparator;

    Dialog loading_dialog;
    RetrofitInterface retrofitInterface = Common.initRetrofit();

    RecyclerViewClickListner listner;

    GetGroupListDataModel group_data_detail = new GetGroupListDataModel();
    List<GetGroupMemberListDataModel> add_member_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title = findViewById(R.id.c_header_title);
        header_title.setText("Add Member");

        //getting data
        Intent intent = getIntent();
        group_data_detail = intent.getParcelableExtra("group_data_detail");
        add_member_list = intent.getParcelableArrayListExtra("add_member_list");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Add_Member_In_Group.this);

                Intent i = new Intent(Add_Member_In_Group.this, EditGroup.class);
                i.putExtra("group_data_detail", group_data_detail);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        findViewById(R.id.add_member_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Add_Member_In_Group.this, EditGroup.class);
                i.putExtra("group_data_detail", group_data_detail);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        listner = this;

        loading_dialog = Common.LoadingDilaog(this);
        loading_dialog.show();
        onGetMemberListFunction(true);
    }


    private void initViews(Boolean isFirstTime) {

        loading_dialog.dismiss();

        pinyinComparator = new PinyinComparatorForAddMember();

        sideBar = (SideBar) findViewById(R.id.sideBar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));

                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        sourceDataList = filledData(add_member_list);
        Collections.sort(sourceDataList, pinyinComparator);

        manager = new LinearLayoutManager(getApplicationContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);

        if (isFirstTime==true) {
            adapter = new AddMemberInGroupAdapter(this, sourceDataList, listner);
            mRecyclerView.setAdapter(adapter);
        }else if (isFirstTime==false){
            adapter.updateList(sourceDataList);
        }


        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);

        mClearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private List<GetGroupMemberListDataModel> filledData(List<GetGroupMemberListDataModel> data) {
        List<GetGroupMemberListDataModel> mSortList = new ArrayList<>();


        for (int i = 0; i < data.size(); i++) {
            GetGroupMemberListDataModel sortModel = new GetGroupMemberListDataModel();

            sortModel.setTitle(data.get(i).getTitle());
            sortModel.setMiddle_name(data.get(i).getMiddle_name());
            sortModel.setLast_name(data.get(i).getLast_name());
            sortModel.setGender(data.get(i).getGender());
            sortModel.setChair_person(data.get(i).getChair_person());
            sortModel.setGender(data.get(i).getGroup_id());

            sortModel.setId(data.get(i).getId());
            sortModel.setFirst_name(data.get(i).getFirst_name());
            sortModel.setEmail(data.get(i).getEmail());
            sortModel.setRole(data.get(i).getRole());
            sortModel.setIsmember(data.get(i).getIsmember());
            sortModel.setProfile_image(data.get(i).getProfile_image());

            String pinyin_english = data.get(i).getFirst_name();
            if (pinyin_english == null || pinyin_english.equalsIgnoreCase("") || pinyin_english.isEmpty()) {


                sortModel.setLetters("#");
            } else {

                String sortString_english = pinyin_english.substring(0, 1).toUpperCase();
                if (sortString_english.matches("[A-Z]")) {
                    sortModel.setLetters(sortString_english.toUpperCase());
                } else {
                    sortModel.setLetters("#");
                }

            }


            mSortList.add(sortModel);
        }


        return mSortList;
    }

    private void filterData(String filterStr) {

        filter_flag = true;

        if (TextUtils.isEmpty(filterStr)) {
            filterDataList = sourceDataList;
            sourceDataList = filledData(add_member_list);
            Collections.sort(sourceDataList, pinyinComparator);
        } else {
            filterDataList.clear();

            for (GetGroupMemberListDataModel sortModel : sourceDataList) {
                String name = sortModel.getFirst_name();
                if (name.indexOf(filterStr.toString()) != -1
                        || name.toLowerCase().startsWith(filterStr)
                        || name.toUpperCase().startsWith(filterStr)
                        || sortModel.getLast_name().toLowerCase().startsWith(filterStr)
                        || sortModel.getLast_name().toUpperCase().startsWith(filterStr)) {

                    filterDataList.add(sortModel);
                }

            }
        }
        Collections.sort(filterDataList, pinyinComparator);
        adapter.updateList(filterDataList);
    }


    private void onGetMemberListFunction(final Boolean isFirstTime) {


        try {

            Call<GetGroupMemberListModel> call = retrofitInterface.getMemberListInEditGroup("application/json", Common.auth_token,
                    group_data_detail.getGroups__id());


            call.enqueue(new Callback<GetGroupMemberListModel>() {
                @Override
                public void onResponse(Call<GetGroupMemberListModel> call, Response<GetGroupMemberListModel> response) {
                    if (response.isSuccessful()) {
                        String status, message;


                        GetGroupMemberListModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();


                        ArrayList<GetGroupMemberListDataModel> list = new ArrayList<>();
                        list = responseModel.getData();


                        if (list != null) {
                            //pd.dismiss();
                            loading_dialog.dismiss();
                            if (add_member_list != null) {
                                add_member_list.clear();
                            }
                            add_member_list = list;

                            initViews(isFirstTime);
                        }


                    }
                }

                @Override
                public void onFailure(Call<GetGroupMemberListModel> call, Throwable t) {

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onAddMemberToGroupBtnClick(String user_id, String group_id) {


        try {

            AddMemberToGroupRequestModel requestModel = new AddMemberToGroupRequestModel();

            requestModel.setUser_id(user_id);
            requestModel.setGroup_id(group_id);


            Call<AddMemberToGroupResponseModel> responseCall = retrofitInterface.AddMemberInGroup("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<AddMemberToGroupResponseModel>() {
                @Override
                public void onResponse(Call<AddMemberToGroupResponseModel> call, Response<AddMemberToGroupResponseModel> response) {

                    if (response.isSuccessful()) {
                        String status, message;

                        //loading_dialog.dismiss();

                        AddMemberToGroupResponseModel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        onGetMemberListFunction(false);
                        Toast.makeText(Add_Member_In_Group.this, "Member added to group successfully", Toast.LENGTH_LONG).show();

                    } else {
                        //loading_dialog.dismiss();

                        Toast.makeText(Add_Member_In_Group.this, "Member added to group Failed", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<AddMemberToGroupResponseModel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(Add_Member_In_Group.this, "Member added to group Failed", Toast.LENGTH_LONG).show();
                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    private void onRemoveMemberToGroupBtnClick(String user_id, String group_id) {


        try {

            RemoveMemberFromGroupRequestModel requestModel = new RemoveMemberFromGroupRequestModel();

            requestModel.setUser_id(user_id);
            requestModel.setGroup_id(group_id);
            Call<RemoveMemberFromGroupResponseMOdel> responseCall = retrofitInterface.RemoveMemberInGroup("application/json", Common.auth_token, requestModel);
            responseCall.enqueue(new Callback<RemoveMemberFromGroupResponseMOdel>() {
                @Override
                public void onResponse(Call<RemoveMemberFromGroupResponseMOdel> call, Response<RemoveMemberFromGroupResponseMOdel> response) {

                    //loading_dialog.dismiss();
                    if (response.isSuccessful()) {
                        String status, message;

                        //loading_dialog.dismiss();

                        RemoveMemberFromGroupResponseMOdel responseModel = response.body();
                        status = responseModel.getStatus();
                        message = responseModel.getMessage();

                        onGetMemberListFunction(false);
                        Toast.makeText(Add_Member_In_Group.this, "Member remove from group successfully", Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(Add_Member_In_Group.this, "Member remove from group Failed", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<RemoveMemberFromGroupResponseMOdel> call, Throwable t) {
                    loading_dialog.dismiss();
                    Toast.makeText(Add_Member_In_Group.this, "Member remove from group Failed", Toast.LENGTH_LONG).show();
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }

    }

    @Override
    public void onRowClick(int position) {

    }

    @Override
    public void onViewClcik(int position, View v) {

        GetGroupMemberListDataModel add_member_list_detail = new GetGroupMemberListDataModel();
        try {
            switch (v.getId()) {
                case R.id.whitelist_contact_checkbox:
                    if (filter_flag == true) {
                        add_member_list_detail = filterDataList.get(position);
                    } else {
                        add_member_list_detail = sourceDataList.get(position);
                    }

                    String group_id = group_data_detail.getGroups__id();
                    String user_id = add_member_list_detail.getId();

                    int flag = add_member_list_detail.getIsmember();

                    if (flag == 0) {
                        //now add to white list
                        loading_dialog.show();
                        onAddMemberToGroupBtnClick(user_id, group_id);
                    } else if (flag == 1) {
                        //now remove to white list
                        loading_dialog.show();
                        onRemoveMemberToGroupBtnClick(user_id, group_id);
                    }

                    break;
                default:
                    break;
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Add_Member_In_Group.this, EditGroup.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
