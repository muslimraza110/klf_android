package com.khojaleadership.KLF.Activities.Mission;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.App;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.Mission.GetMissionDataModel;
import com.khojaleadership.KLF.Model.Mission.GetMissionModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mission_Activity extends AppCompatActivity {

    RetrofitInterface retrofitInterface = Common.initRetrofit();
    Dialog loading_dialog;

    WebView webView;
    ArrayList<GetMissionDataModel> mission_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_updated);

        loading_dialog = Common.LoadingDilaog(this);

        webView = (WebView) findViewById(R.id.mission_webview);


        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView header_title;
        header_title=findViewById(R.id.c_header_title);
        header_title.setText("Mission");

        //back btn
        findViewById(R.id.c_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideKeyboard(v, Mission_Activity.this);

                finish();
            }
        });



        //get MIssion Data
        loading_dialog.show();
        getMissionData();
    }

    private void getMissionData() {

        Call<GetMissionModel> call = retrofitInterface.getMissionList("application/json",Common.auth_token);

        call.enqueue(new Callback<GetMissionModel>() {
            @Override
            public void onResponse(Call<GetMissionModel> call, Response<GetMissionModel> response) {



                if (response.isSuccessful()) {
                    String status, message;

                    GetMissionModel responseModel=response.body();

                    status = responseModel.getStatus();
                    message = responseModel.getMessage();



                    ArrayList<GetMissionDataModel> list=new ArrayList<>();
                    list=responseModel.getData();

                    if (list != null) {
                        //pd.dismiss();
                        //loading_dialog.dismiss();
                        mission_list = list;

                        intialize_view();
                        loading_dialog.dismiss();
                    }

                }else {
                    loading_dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<GetMissionModel> call, Throwable t) {
                loading_dialog.dismiss();
            }
        });
    }

    public void intialize_view(){
        try {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(mission_list.get(0).getPages__content(), "text/html; charset=utf-8", "UTF-8");
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
    }
    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
    }
}
