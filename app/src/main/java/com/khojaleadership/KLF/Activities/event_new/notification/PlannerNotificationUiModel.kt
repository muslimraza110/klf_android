package com.khojaleadership.KLF.Activities.event_new.notification

import java.util.*

data class PlannerNotificationUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val notificationString: String,
        val time: String,
        var hasRead: Boolean,
        val eventDay: Int
//        val eventProgramDetailsId: String
)