package com.khojaleadership.KLF.Activities.event_new.programme

import java.io.Serializable
import java.util.*

data class PersonDetailUiModel(
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val userId: String,
        val summitEventsFacultyId: String,
        val name: String,
        val designation: String,
        val shortDescription: String,
        val longDescription: String,
        val facebook: String,
        val twitter: String,
        val linkedIn: String,
        val country: String,
        val company: String,
        val photoUrl: String,
        val isSpeaker: Boolean,
        val isDelegate: Boolean,
//        val availableSlotData: List<SummitEventDayAvailability>?,
        var isSelected: Boolean
) : Serializable {
    override fun toString(): String {
        return name
    }
}