package com.khojaleadership.KLF.Activities.DashBoard_New

import java.util.*

data class NavigationHomeUiModel (
        val id: Long = UUID.randomUUID().mostSignificantBits,
        val name: String
)