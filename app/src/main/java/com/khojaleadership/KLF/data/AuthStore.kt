package com.khojaleadership.KLF.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.MyItinerarySessionUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Activities.event_new.events.EventCardUiModel
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseDataModel
import com.khojaleadership.KLF.Model.event_new.SummitEventDayAvailability

interface AuthStore {

    var loginData: LoginResponseDataModel?

    var event: EventCardUiModel?

    var meetingRequestSent: Boolean

    var selectDelegateOkClicked: Boolean

    var lastSelectedPersonAvailability: List<SummitEventDayAvailability>?

    var sessionsModel: MyItinerarySessionUiModel?

    var delegatesToSelect: List<PersonDetailUiModel>?
}

class SharedPreferencesAuthStore(
        private val context: Context,
        private val gson: Gson
) : AuthStore {

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(
            SHARED_PREFERENCE_NAME,Context.MODE_PRIVATE
    )


    override var loginData: LoginResponseDataModel?
        get() {
            val json = sharedPreferences.getString(KEY_LOGIN_USER_DATA, "")
            json?.let {
                return gson.fromJson<LoginResponseDataModel>(json, LoginResponseDataModel::class.java)
            } ?: run {
                return null
            }
        }
        set(value) {

            value?.let {
                sharedPreferences.edit {
                    putString(KEY_LOGIN_USER_DATA, gson.toJson(value))
                }
            }?: run{
                sharedPreferences.edit {
                    putString(KEY_LOGIN_USER_DATA, null)
                }
            }

        }


    override var event: EventCardUiModel?
        get() {
            val json = sharedPreferences.getString(KEY_EVENT_DETAILS, "")
            json?.let {
                return gson.fromJson<EventCardUiModel>(json, EventCardUiModel::class.java)
            } ?: run {
                return null
            }
        }
        set(value) {
            value?.let {
                sharedPreferences.edit {
                    putString(KEY_EVENT_DETAILS, gson.toJson(value))
                }
            }?: run{
                sharedPreferences.edit {
                    putString(KEY_EVENT_DETAILS, null)
                }
            }
        }

    override var meetingRequestSent: Boolean
        get() = sharedPreferences.getBoolean(KEY_MEETING_REQUEST_SENT, false)
        set(value) {
            sharedPreferences.edit {
                putBoolean(KEY_MEETING_REQUEST_SENT, value)
            }
        }


    override var lastSelectedPersonAvailability: List<SummitEventDayAvailability>?
        get() {
            val json = sharedPreferences.getString(KEY_LAST_SELECTED_PERSON_AVAILABILITY, "")
            json?.let {
                val type = object : TypeToken<List<SummitEventDayAvailability>>() {}.type
                return gson.fromJson<List<SummitEventDayAvailability>>(json, type)
            } ?: run {
                return null
            }
        }
        set(value) {

            value?.let {
                sharedPreferences.edit {
                    putString(KEY_LAST_SELECTED_PERSON_AVAILABILITY, gson.toJson(value))
                }
            }?: run{
                sharedPreferences.edit {
                    putString(KEY_LAST_SELECTED_PERSON_AVAILABILITY, null)
                }
            }

        }


    override var sessionsModel: MyItinerarySessionUiModel?
        get() {
            val json = sharedPreferences.getString(KEY_SESSIONS_LIST_FOR_A_PROGRAM, "")
            json?.let {
                return gson.fromJson<MyItinerarySessionUiModel>(json, MyItinerarySessionUiModel::class.java)
            } ?: run {
                return null
            }
        }
        set(value) {

            value?.let {
                sharedPreferences.edit {
                    putString(KEY_SESSIONS_LIST_FOR_A_PROGRAM, gson.toJson(value))
                }
            }?: run{
                sharedPreferences.edit {
                    putString(KEY_SESSIONS_LIST_FOR_A_PROGRAM, null)
                }
            }

        }


    override var delegatesToSelect: List<PersonDetailUiModel>?
        get() {
            val json = sharedPreferences.getString(KEY_DELEGATES_TO_SELECT, "")
            json?.let {
                val type = object : TypeToken<List<PersonDetailUiModel>>() {}.type
                return gson.fromJson<List<PersonDetailUiModel>>(json, type)
            } ?: run {
                return null
            }
        }
        set(value) {
            value?.let {
                sharedPreferences.edit {
                    putString(KEY_DELEGATES_TO_SELECT, gson.toJson(value))
                }
            }?: run{
                sharedPreferences.edit {
                    putString(KEY_DELEGATES_TO_SELECT, null)
                }
            }
        }


    override var selectDelegateOkClicked: Boolean
        get() = sharedPreferences.getBoolean(KEY_SELECT_DELEGATES_OK_CLICKED, false)
        set(value) {
            sharedPreferences.edit {
                putBoolean(KEY_SELECT_DELEGATES_OK_CLICKED, value)
            }
        }

    companion object {
        private const val SHARED_PREFERENCE_NAME = "KLFSharedPreferences"
        private const val KEY_LOGIN_USER_DATA = "loginUserData"
        private const val KEY_EVENT_DETAILS = "eventDetails"
        private const val KEY_MEETING_REQUEST_SENT = "meetingRequestSentKey"
        private const val KEY_LAST_SELECTED_PERSON_AVAILABILITY = "lastSelectedPersonAvailability"
        private const val KEY_SESSIONS_LIST_FOR_A_PROGRAM = "sessionsListForAProgram"
        private const val KEY_DELEGATES_TO_SELECT = "delegatesToSelect"
        private const val KEY_SELECT_DELEGATES_OK_CLICKED = "selectDelegatesOkClicked"
    }
}