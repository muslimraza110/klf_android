package com.khojaleadership.KLF.data

import com.khojaleadership.KLF.Helper.Response
import com.khojaleadership.KLF.Model.event_new.*
import com.khojaleadership.KLF.api.ApiClient
import com.khojaleadership.KLF.api.EventsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class EventsRepository(authToken: String) {


    private val eventsApi: EventsApi = ApiClient.getEventsApi(authToken)
    private val mockApi: EventsApi = ApiClient.mockEventApi


    suspend fun setSummitEventsRegistration(
        userId: String,
        summitEventId: String
    ): Response<SummitEventsRegistrationResponse> {
        return withContext(Dispatchers.IO) {
            eventsApi.setSummitEventsRegistration(
                summitEventsRegistrationRequest = SummitEventsRegistrationRequest(userId, summitEventId)
            )
        }
    }



    suspend fun getCurrentEvents(
            userId: String
    ): Response<List<SingleEventResponse>> {
//                return Response(data = null,status = ResponseStatus.SUCCESS,message = "Data not found")

        return withContext(Dispatchers.IO) {
            eventsApi.getCurrentEventsList(userId)
        }
    }

    suspend fun getPreviousEvents(
            userId: String
    ): Response<List<SingleEventResponse>> {
//        return Response(data = null,status = ResponseStatus.FAILURE,message = "Data not found")

        return withContext(Dispatchers.IO) {
            eventsApi.getPreviousEventsList(userId)
        }
    }

    suspend fun getEventSections(
            eventId: String
    ): Response<List<EventSectionsResponse>> {
        return withContext(Dispatchers.IO) {
            eventsApi.getEventSections(eventId)
        }
    }


    suspend fun getSpeakersAndDelegatesForProgramDetailsId(
            eventId: String,
            programDetailsId: String,
            searchText: String,
            attendeeAvailabilityStatus: String,
            userId: String
    ): Response<SummitEventPeopleListResponse> {

        return withContext(Dispatchers.IO) {
            if (searchText.isNotEmpty()) {
                eventsApi.getSummitEventSpeakersDelegatesForProgramDetailsIdWithSearch(
                        eventId = eventId,
                        programDetailsId = programDetailsId,
                        searchText = searchText,
                        attendeeAvailabilityStatus = attendeeAvailabilityStatus,
                        user_id = userId
                )
            } else {
                eventsApi.getSummitEventSpeakersDelegatesForProgramDetailsId(
                        eventId = eventId,
                        programDetailsId = programDetailsId,
                        attendeeAvailabilityStatus = attendeeAvailabilityStatus,
                        user_id = userId
                )
            }
        }

    }

    suspend fun getDelegates(
            eventId: String,
            searchText: String,
            userId: String
    ): Response<SummitEventPeopleListResponse> {
//        return Response(data = null,status = ResponseStatus.SUCCESS,message = "Data not found")

        return withContext(Dispatchers.IO) {
            if (searchText.isNotEmpty()) {
                eventsApi.getSummitEventDelegatesWithSearch(
                        eventId = eventId,
                        user_id = userId,
                        isDelegate = "1",
                        searchText = searchText
                )
            } else {
                eventsApi.getSummitEventDelegates(
                        eventId = eventId,
                        user_id = userId,
                        isDelegate = "1"
                )
            }
        }

    }

    suspend fun getSpeakers(
            eventId: String,
            searchText: String,
            userId: String
    ): Response<SummitEventPeopleListResponse> {
        return withContext(Dispatchers.IO) {
            if (searchText.isNotEmpty()) {
                eventsApi.getSummitEventSpeakersWithSearch(
                        eventId = eventId,
                        user_id = userId,
                        isSpeaker = "1",
                        searchText = searchText
                )
            } else {
                eventsApi.getSummitEventSpeakers(
                        eventId = eventId,
                        user_id = userId,
                        isSpeaker = "1"
                )
            }
        }
    }


    suspend fun getProgrammeDetails(eventId: String, userId: String): Response<ProgrammeDetailResponse> {

//        return Response<ProgrammeDetailResponse>(data = null,status = ResponseStatus.FAILURE,message = "Data not found")
        return withContext(Dispatchers.IO) {
            eventsApi.getProgrammeDetails(
                    eventId = eventId,
                    sectionTag = "program",
                    userId = userId
//                authToken = Common.auth_token ?: ""
//                sectionTag = EventSectionTag.PROGRAMME.value
            )
        }
    }

    suspend fun getMyItineraryData(eventId: String, userId: String): Response<ProgrammeDetailResponse> {
//        return Response<ProgrammeDetailResponse>(data = null,status = ResponseStatus.SUCCESS,message = "No data found.")

        return withContext(Dispatchers.IO) {
            eventsApi.getMyItineraryData(
                    eventId = eventId,
                    userId = userId
            )
        }
    }


    suspend fun setSummitEventProgramAvailability(
            userId: String,
            programDetailsId: String,
            isAvailable: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.setSummitEventProgramCheckMyAvailability(
                    changeAvailabilityRequest = ChangeAvailabilityRequest(
                            userId = userId,
                            programDetailsId = programDetailsId,
                            isAvailable = isAvailable
                    )
            )
        }
    }


    suspend fun setSummitEventMeetingRequestStatus(
            requestedById: String,
            requestedToId: String,
            userId: String,
            programDetailsId: String,
            meetingStatus: String,
            meetingNotes: String,
            isUpdate: Boolean
    ): Response<MeetingStatusSetResponse> {
        return withContext(Dispatchers.IO) {
            eventsApi.setSummitEventMeetingRequestStatus(
                    meetingStatusSetRequest = MeetingStatusSetRequest(
                            programDetailsId = programDetailsId,
                            meetingNotes = meetingNotes,
                            meetingStatus = meetingStatus,
                            requestedById = requestedById,
                            requestedToId = requestedToId,
                            userId = userId,
                            isUpdate = isUpdate
                    )
            )
        }
    }


    suspend fun joinBreakoutSession(
            facultyId: String,
            sessionId: String,
            enroll: Boolean
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.joinBreakoutSession(
                    facultyId = facultyId,
                    sessionId = sessionId,
                    enroll = if (enroll) "1" else "0"
            )
        }
    }


    suspend fun setSummitEventMeetingRequest(
            requestedById: String,
            requestedToIdList: String,
            userId: String,
            eventId: String,
            meetingNotes: String,
            meetingDate: String,
            meetingStartTime: String,
            meetingEndTime: String,
            meetingVenue: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.setMeetingRequest(
                    setMeetingRequestBody = SetMeetingRequest(
                            meetingNotes = meetingNotes,
                            requestedById = requestedById,
                            requestedToIdList = requestedToIdList,
                            userId = userId,
                            eventId = eventId,
                            meetingDate = meetingDate,
                            meetingEndTime = meetingEndTime,
                            meetingStartTime = meetingStartTime,
                            meetingVenue = meetingVenue
                    )
            )
        }
    }


    suspend fun updateWholeSummitEventMeetingRequest(
            requestedById: String,
            requestedToIdList: String,
            userId: String,
            eventId: String,
            meetingNotes: String,
            meetingDate: String,
            meetingStartTime: String,
            meetingEndTime: String,
            meetingVenue: String,
            meetingId: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.updateWholeMeetingRequest(
                    updateMeetingRequestBody = UpdateWholeMeetingRequest(
                            meetingNotes = meetingNotes,
                            requestedById = requestedById,
                            requestedToIdList = requestedToIdList,
                            userId = userId,
                            eventId = eventId,
                            meetingDate = meetingDate,
                            meetingEndTime = meetingEndTime,
                            meetingStartTime = meetingStartTime,
                            meetingVenue = meetingVenue,
                            eventMeetingId = meetingId
                    )
            )
        }
    }

    suspend fun updateSummitEventMeetingRequest(
            userId: String,
            meetingChildId: String,
            newStatus: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.updateMeetingStatus(
                    updateMeetingRequest = UpdateMeetingRequest(
                            userId = userId,
                            newStatus = newStatus,
                            summitEventsMeetingRequestChildId = meetingChildId
                    )
            )
        }
    }

    suspend fun updateSummitEventMeetingNotes(
            userId: String,
            meetingParentId: String,
            notes: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.updateMeetingNotes(
                    updateMeetingNotesRequest = UpdateMeetingNotesRequest(
                            userId = userId,
                            notes = notes,
                            summitEventsMeetingRequestParentId = meetingParentId
                    )
            )
        }
    }

    suspend fun cancelSummitEventMeetingRequest(
            userId: String,
            meetingParentId: String
    ): Response<Any> {
        return withContext(Dispatchers.IO) {
            eventsApi.cancelMeeting(
                    cancelMeetingRequest = CancelMeetingRequest(
                            userId = userId,
                            summitEventsMeetingRequestParentId = meetingParentId
                    )
            )
        }
    }

    suspend fun getMeetings(
            eventId: String,
            userId: String,
            facultyId: String
    ): Response<List<MeetingsResponse>> {
        return withContext(Dispatchers.IO) {
            eventsApi.getMeetings(
                    eventId = eventId,
                    userId = userId,
                    facultyId = facultyId
            )
        }
    }

    suspend fun getPlannerNotifications(
            userId: String,
            eventId: String
    ): Response<List<PlannerNotificationsResponse>> {
//        return Response(
//                data = null,
//                message = "",
//                status = ResponseStatus.SUCCESS
//        )
        return withContext(Dispatchers.IO) {
            mockApi.getPlannerNotifications(
                    userId = userId,
                    eventId = eventId
            )
        }
    }


}



