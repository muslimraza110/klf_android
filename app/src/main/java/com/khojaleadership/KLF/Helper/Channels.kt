package com.khojaleadership.KLF.Helper

object Channels {
    const val ID_SUMMIT_EVENTS_NOTIFICATIONS = "SummitEventNotificationsChannel"
    const val NAME_SUMMIT_EVENTS_NOTIFICATIONS = "Summit Event Notifications"
}