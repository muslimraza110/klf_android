package com.khojaleadership.KLF.Helper

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Convenient method to launch Coroutines safely by wrapping in try/catch.
 */
fun CoroutineScope.launchSafely(
        block: suspend () -> Unit,
        error: (e: Exception) -> Unit = {}
): Job {
    return launch{
        try {
            block()
        } catch (jobCancelException: CancellationException) {
            Timber.e(jobCancelException)
        } catch (exp: Exception) {
            Timber.e(exp)
            error(exp)
        }
    }
}

fun String.toTitleCase(): String {
    var lastWasSpace = true
    return String(
            this.map {
                if (lastWasSpace) {
                    lastWasSpace = false
                    it.toUpperCase()
                } else {
                    if (it == ' ' || it == ',' || it == '-' || it == '_' || it == '/') {
                        lastWasSpace = true
                    }
                    it.toLowerCase()
                }
            }.toCharArray()
    )
}

