package com.khojaleadership.KLF.Helper

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent


class EditTextBackEvent : androidx.appcompat.widget.AppCompatEditText {
    private var mOnImeBack: EditTextImeBackListener? = null

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (event.keyCode == KeyEvent.KEYCODE_BACK &&
                event.action == KeyEvent.ACTION_UP) {
            if (mOnImeBack != null) mOnImeBack!!.onImeBack(this, this.text?.toString() ?: "")
        }
        return super.dispatchKeyEvent(event)
    }

    fun setOnEditTextImeBackListener(listener: EditTextImeBackListener?) {
        mOnImeBack = listener
    }
}

interface EditTextImeBackListener {
    fun onImeBack(ctrl: EditTextBackEvent, text: String)
}