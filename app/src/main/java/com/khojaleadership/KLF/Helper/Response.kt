package com.khojaleadership.KLF.Helper

import com.google.gson.annotations.SerializedName


data class Response<T>(
        @SerializedName("data")val data: T?,
        @SerializedName( "status") val status: String,
        @SerializedName("message") val message: String?
)


object ResponseStatus{
    const val SUCCESS = "success"
    const val FAILURE = "failure"
}

//@JsonClass(generateAdapter = true)
//class EmptyObject