package com.khojaleadership.KLF.Helper

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.R
import com.tokenautocomplete.TokenCompleteTextView


class PersonAutoCompleteView(context: Context, attrs: AttributeSet) : TokenCompleteTextView<PersonDetailUiModel>(context, attrs) {
    override fun getViewForObject(p0: PersonDetailUiModel?): View {
        val l = context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = l.inflate(R.layout.contact_token, parent as ViewGroup, false) as TextView
        view.findViewById<TextView>(R.id.name).text = p0?.name ?: ""
        return view
    }

    override fun defaultObject(p0: String?): PersonDetailUiModel? {
        return null
    }

    override fun shouldIgnoreToken(token: PersonDetailUiModel?): Boolean {
        return objects.contains(token)
    }

}