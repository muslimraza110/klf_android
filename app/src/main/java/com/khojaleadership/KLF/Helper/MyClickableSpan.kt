package com.khojaleadership.KLF.Helper

import android.content.Context
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.khojaleadership.KLF.R

class MyClickableSpan<T>(
        private val data: T,
        private val onHyperLinkClick: (T) -> Unit,
        private val context: Context
) : ClickableSpan() {

    override fun onClick(widget: View) {
        onHyperLinkClick.invoke(data)
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.color = ContextCompat.getColor(context, R.color.cyan)
    }
}