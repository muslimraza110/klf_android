package com.khojaleadership.KLF.Helper

import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.MeetingDetailsActivity
import com.khojaleadership.KLF.Model.event_new.NotificationBody
import com.khojaleadership.KLF.Model.event_new.NotificationPayload
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.data.SharedPreferencesAuthStore
import timber.log.Timber

class KlfFirebaseMessagingService : FirebaseMessagingService() {

    private val notificationManager by lazy {
        NotificationManagerCompat.from(applicationContext)
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Timber.d("New Push Notification Received")
        if (SharedPreferencesAuthStore(this, Gson()).loginData != null) {
        val notificationPayload = parseRemoteMessage(message.data)
        notificationPayload?.let {
            Timber.d(notificationPayload.toString())
            val notificationBody = parseRemoteNotification(message.data)
            sendNotification(notificationPayload, notificationBody)
        } ?: run {
            Timber.d("Notification Payload not parsed")
        }
        } else {
            Timber.d("User not logged in. Notification Discarded")
        }
    }

    private fun sendNotification(
            notificationPayload: NotificationPayload,
            notificationBody: NotificationBody
    ) {
//        if (notificationPayload.userId == authStore.user?.id) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                createGroupNotification(notificationPayload, notificationBody)
//            }
        sendChildNotification(notificationPayload, notificationBody)
//        } else {
//            Timber.d("The Notification we received for different user id so we discarded notification.")
//        }
    }

    //
    private fun sendChildNotification(
            notificationPayload: NotificationPayload,
            notificationBody: NotificationBody
    ) {
        val intent = Intent(this, MeetingDetailsActivity::class.java)
        intent.putExtra(MeetingDetailsActivity.KEY_EVENT_ID, notificationPayload.details?.eventId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_MEETING_PARENT_ID, notificationPayload.details?.meetingParentId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID, notificationPayload.details?.summitEventsFacultyId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_USER_ID, notificationPayload.details?.userId
                ?: "")

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val pendingIntent = PendingIntent.getActivity(
                this,
                System.currentTimeMillis().toInt(),
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        )

        val notification =
                NotificationCompat.Builder(this, Channels.ID_SUMMIT_EVENTS_NOTIFICATIONS).apply {
                    setContentTitle(notificationBody.title)
                    setContentText(notificationBody.body)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        setGroup(notificationPayload.details?.eventId
                                ?: System.currentTimeMillis().toString())
                    }
                    setStyle(NotificationCompat.BigTextStyle()
                            .bigText(notificationBody.body))
                    setSmallIcon(R.drawable.khoja_logo)
                    setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    setAutoCancel(true)
                    priority = NotificationCompat.PRIORITY_MAX
                    setContentIntent(pendingIntent)
                }.build()

        val notificationId = System.currentTimeMillis()
        notificationManager.notify(
                notificationId.toString(),
                notificationId.toInt(),
                notification
        )
    }


    private fun createGroupNotification(
            notificationPayload: NotificationPayload,
            notificationBody: NotificationBody
    ) {
        val intent = Intent(this, MeetingDetailsActivity::class.java)
        intent.putExtra(MeetingDetailsActivity.KEY_EVENT_ID, notificationPayload.details?.eventId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_MEETING_PARENT_ID, notificationPayload.details?.meetingParentId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_SUMMIT_EVENT_FACULTY_ID, notificationPayload.details?.summitEventsFacultyId
                ?: "")
        intent.putExtra(MeetingDetailsActivity.KEY_USER_ID, notificationPayload.details?.userId
                ?: "")
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        val pendingIntent = PendingIntent.getActivity(
                this,
                System.currentTimeMillis().toInt(),
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        )

        val notification =
                NotificationCompat.Builder(this, Channels.ID_SUMMIT_EVENTS_NOTIFICATIONS).apply {
                    setSmallIcon(R.drawable.khoja_logo)
                    setContentTitle(notificationBody.title)
                    setContentText("KLF")
                    setStyle(NotificationCompat.InboxStyle())
                    setGroupSummary(true)
                    setGroup(notificationPayload.details?.eventId
                            ?: System.currentTimeMillis().toString())
                    setAutoCancel(true)
                    priority = NotificationCompat.PRIORITY_MAX
                    setContentIntent(pendingIntent)
                }.build()

        val notificationId = System.currentTimeMillis()
        notificationManager.notify(
                notificationId.toString(),
                notificationId.toInt(),
                notification
        )
    }


    private fun parseRemoteMessage(data: Map<String, String>): NotificationPayload? {
//        val jsonObject = JSONObject(data["push_data"] ?: "{}")
        val adapter = Gson().getAdapter(NotificationPayload::class.java)
        return adapter.fromJson(data["push_data"] ?: "{}")
    }

    private fun parseRemoteNotification(data: Map<String, String>): NotificationBody {
        return NotificationBody(data["body"] ?: "Body", data["title"] ?: "TITLE")
    }

}