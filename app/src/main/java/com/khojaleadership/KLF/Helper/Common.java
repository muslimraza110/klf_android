package com.khojaleadership.KLF.Helper;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.khojaleadership.KLF.Interface.RetrofitInterface;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Expandable_Child_Model;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Data_Model;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Email_Model;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.Model.SplashLogin_Models.LoginResponseModel;
import com.khojaleadership.KLF.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import timber.log.Timber;


public class Common {
        public static String LIVE_BASE_URL = "https://api.khojaleadershipforum.org/";   //live url
//    public static String LIVE_BASE_URL = "https://stgapi.khojaleadershipforum.org/";   //staging url
//    public static String LIVE_BASE_URL = "http://172.16.201.137:85/KLF/API/klf_api/";
//    public static String LIVE_BASE_URL = "http://172.16.202.123:73/klf_api/";

    public static RetrofitInterface initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LIVE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        return retrofitInterface;
    }


    public static void hideKeyboard(View view, Context mctx) {
        InputMethodManager inputMethodManager = (InputMethodManager) mctx.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static Dialog LoadingDilaog(Context mctx) {
        // custom dialog
        Dialog loading_dialog;
        loading_dialog = new Dialog(mctx);
        loading_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading_dialog.setContentView(R.layout.loading);
        loading_dialog.setCanceledOnTouchOutside(false);
        return loading_dialog;
    }

    public static void Toast_Message(Context mctx, String message) {
        Toast.makeText(mctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void Toast_Error_Message(Context mctx, String message) {
        Toast toast;
        toast = new Toast(mctx);
        toast.setText(message);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
//        Toast.makeText(mctx, message, Toast.LENGTH_SHORT).show();
    }

    public static String auth_token;
    public static Boolean is_Admin_flag;
    public static Boolean is_Online;
    public static Boolean internetConnection;
    public static Boolean addPost;
    public static Boolean editPost;

    //..................Login data...........................
    public static LoginResponseModel login_data;

    //..................Dash Board data......................
    public static Home_Expandable_Child_Model Home_MainContent_data;


    //..................Forum section data...........................
    public static String Subtopic_Id;
    public static String Category;
    public static GetTopicDataModel topic_data_detail = new GetTopicDataModel();
    public static GetSubTopicDataModel sub_topic_data_detail = new GetSubTopicDataModel();

    public static Boolean send_eblast_falg = false;
    public static Boolean send_eblast_post = false;

    public static String eblast_label = "", eblast_subject = "", eblast_message = "";
    public static String send_eblast_list;

    // ------------- set_summit_events_registration---------------------
    public static String  summitEventsRegistrationLink ;
    //this is for run time addition and subtraction of email
    public static ArrayList<Send_Eblast_Child_Data_Model> send_eblast_child_data_list = new ArrayList<>();
    public static ArrayList<Send_Eblast_Child_Data_Model> send_eblast_child_data_list_copy = new ArrayList<>();

    public static ArrayList<Send_Eblast_Email_Model> send_eblast_email_list = new ArrayList<>();
    public static ArrayList<Send_Eblast_Email_Model> send_eblast_email_list_copy = new ArrayList<>();

    //..................Project section data...........................
    //for run time checking is total value ==100
    public static ArrayList<String> percentage_group_id_list = new ArrayList<>();
    public static ArrayList<String> percentage_value_list = new ArrayList<>();

    //..................Event section data...........................
    //due to customized calenderview i used static
    public static ArrayList<GetEventsListDataModel> events_by_day = new ArrayList<>();

    //..................Group section data...........................
    public static ArrayList<String> cat_list = new ArrayList<>();


    public static String somethingWentWrongMessage = "Something went wrong.";
    public static String idNotFoundMessage = "User ID not found.";
    public static String invalidUrlMessage = "Invalid Url";
    public static String noRecordFoundMessage = "No record found.";
    public static String noRegistrationLinkFound = "No Registration Link Found.";

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static void setSizeOfActivityAsDialog(WindowManager windowManager, Context context, Window window) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        float height = displayMetrics.heightPixels - Common.pxFromDp(context, 64f);
        float width = displayMetrics.widthPixels - Common.pxFromDp(context, 32f);

        window.setLayout((int) width, (int) height);
    }

    public static void loadUrlInBrowser(String url, Context context) {
        try {
            new URL(url);
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (MalformedURLException urlMalformedExceptionL) {
            Timber.e("Invalid Url: $url");
            Toast.makeText(context, Common.invalidUrlMessage, Toast.LENGTH_SHORT).show();
        }
    }


    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }


    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
