package com.khojaleadership.KLF.Helper

import android.content.Context
import android.text.TextPaint
import android.text.style.UnderlineSpan
import androidx.core.content.ContextCompat
import com.khojaleadership.KLF.R

class CyanUnderlinedSpan(
        private val context: Context
): UnderlineSpan() {
    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.color = ContextCompat.getColor(context, R.color.cyan)
    }
}