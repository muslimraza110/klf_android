package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Family_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Edit_Profile_Family_RecyclerviewAdapter extends RecyclerView.Adapter<Edit_Profile_Family_RecyclerviewAdapter.ViewHolderclass> {


    Context mctx;
    ArrayList<View_Profile_Family_Model> models;

    public Edit_Profile_Family_RecyclerviewAdapter(Context mctx, ArrayList<View_Profile_Family_Model> models) {
        this.mctx = mctx;
        this.models = models;
    }

    @NonNull
    @Override
    public ViewHolderclass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater  = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.edit_profile_family_recyclerview_item,parent,false);

        return new ViewHolderclass(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderclass holder, int position) {


        try{
            View_Profile_Family_Model item=models.get(position);

            if (TextUtils.isEmpty(item.getRelatedUsers__first_name()) && TextUtils.isEmpty(item.getRelatedUsers__last_name())) {

            }else if (item.getRelatedUsers__first_name().equals("null") && item.getRelatedUsers__last_name().equals("null")) {

            }else {
                holder.tv_username.setText(item.getRelatedUsers__first_name()+" "+item.getRelatedUsers__last_name());
                holder.tv_username.setPaintFlags(holder.tv_username.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }


            if (TextUtils.isEmpty(item.getUserFamilies__relation())) {

            }else if (item.getUserFamilies__relation().equals("null")) {

            }else {
                holder.tv_relation.setText(item.getUserFamilies__relation()+" of ");
            }


            Glide.with(mctx).load(item.getProfileImg__name()).into(holder.img_user);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolderclass extends RecyclerView.ViewHolder {


        ImageView img_user;
        ImageView img_edit;
        ImageView img_delete;
        TextView tv_relation;
        TextView tv_username;
        
        LinearLayout cardView;



        public ViewHolderclass(@NonNull View itemView) {
            super(itemView);

            img_delete = itemView.findViewById(R.id.img_delete);
            img_edit = itemView.findViewById(R.id.img_edite);
            img_user= itemView.findViewById(R.id.img_imageview);


            tv_username= itemView.findViewById(R.id.tv_name);
            tv_relation= itemView.findViewById(R.id.tv_relation);
            cardView= itemView.findViewById(R.id.cardview);


        }
    }


}
