package com.khojaleadership.KLF.Adapter.event_new.hotelRegistration

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.hotelRegistration.HotelPackagesUiModel
import com.khojaleadership.KLF.databinding.ItemHotelPkgBinding


class HotelPackagesAdapter : ListAdapter<HotelPackagesUiModel, HotelPackagesAdapter.ViewHolder>(HotelPackagesDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return HotelPackagesViewHolder(
                ItemHotelPkgBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: HotelPackagesUiModel)
    }

    class HotelPackagesViewHolder(val binding: ItemHotelPkgBinding) : ViewHolder(binding.root) {
        override fun bind(item: HotelPackagesUiModel) {
        }
    }


}

class HotelPackagesDiffUtil : DiffUtil.ItemCallback<HotelPackagesUiModel>() {
    override fun areItemsTheSame(
            oldItem: HotelPackagesUiModel,
            newItem: HotelPackagesUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: HotelPackagesUiModel,
            newItem: HotelPackagesUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
