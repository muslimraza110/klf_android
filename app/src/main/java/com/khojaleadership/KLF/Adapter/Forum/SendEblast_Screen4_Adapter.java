package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Child_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class SendEblast_Screen4_Adapter extends RecyclerView.Adapter<SendEblast_Screen4_Adapter.RViewholder> {
    ArrayList<Send_Eblast_Child_Data_Model> child_data_list;
    Context context;
    RecyclerViewClickListner listner;

    public SendEblast_Screen4_Adapter(ArrayList<Send_Eblast_Child_Data_Model> child_data_list, Context context, RecyclerViewClickListner listner) {
        this.child_data_list = child_data_list;
        this.context = context;
        this.listner = listner;
    }


    @NonNull
    @Override
    public RViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.send_eblast_screen4_recylerview_items, parent,false);

        return new RViewholder(view,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull RViewholder holder, int position) {

        try {
            holder.textView.setText(child_data_list.get(position).getFirstName()+" "+child_data_list.get(position).getLastName());

            holder.checkBox.setChecked(false);
            if (child_data_list.get(position).getIs_checked().equals("1")){
                holder.checkBox.setChecked(true);
            }else {
                holder.checkBox.setChecked(false);
            }


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return child_data_list.size();
    }

    public void updateList(ArrayList<Send_Eblast_Child_Data_Model> list){
        this.child_data_list= list;
        notifyDataSetChanged();
    }

    public class RViewholder extends RecyclerView.ViewHolder {


        TextView textView;
        ImageView image;
        CheckBox checkBox;

        public RViewholder(@NonNull View itemView, final RecyclerViewClickListner listner) {
            super(itemView);


            textView =itemView.findViewById(R.id.name);
            image =itemView.findViewById(R.id.imageView);
            checkBox =itemView.findViewById(R.id.checkbox);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = checkBox.isChecked();
                    // Do something here.
                    if (listner != null) {
                         listner.onRowClick(getAdapterPosition());

                    }
                }
            });

        }

    }
}
