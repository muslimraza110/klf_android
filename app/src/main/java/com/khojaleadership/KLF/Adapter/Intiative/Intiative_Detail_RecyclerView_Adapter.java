package com.khojaleadership.KLF.Adapter.Intiative;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Pledgers_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;

public class Intiative_Detail_RecyclerView_Adapter extends RecyclerView.Adapter<Intiative_Detail_RecyclerView_Adapter.RecyclerviewViewHolder> {


    List<Intiative_Detail_Data_Pledgers_Model> modelList = new ArrayList<>();

    public Intiative_Detail_RecyclerView_Adapter(List<Intiative_Detail_Data_Pledgers_Model> modelList) {
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public RecyclerviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.intiative_detail_updated_recyclerview1, parent, false);

        return new RecyclerviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewViewHolder holder, int position) {


        try {
            Intiative_Detail_Data_Pledgers_Model item = modelList.get(position);

            holder.tv_right1.setText(item.getFundraising());
            holder.tv_right2.setText(item.getCharity());


            if (Common.is_Admin_flag == true) {

                if (item.getUsers__first_name() == null && item.getUsers__last_name() == null) {

                } else if (item.getUsers__first_name() == null && item.getUsers__last_name() != null) {
                    holder.tv_center.setText(item.getUsers__last_name());
                } else if (item.getUsers__first_name() != null && item.getUsers__last_name() == null) {
                    holder.tv_center.setText(item.getUsers__first_name());
                } else {
                    holder.tv_center.setText(item.getUsers__first_name() + " " + item.getUsers__last_name());
                }


            } else {
                if (item.getFundraisingPledges__anonymous().equals("1")) {

                    if (Common.login_data.getData().getId().equals(item.getFundraisingPledges__user_id())) {
                        if (item.getUsers__first_name() == null && item.getUsers__last_name() == null) {
                            holder.tv_center.setText(" *(Anonymus)");
                        } else if (item.getUsers__first_name() == null && item.getUsers__last_name() != null) {
                            holder.tv_center.setText(item.getUsers__last_name() + " *(Anonymus)");

                        } else if (item.getUsers__first_name() != null && item.getUsers__last_name() == null) {
                            holder.tv_center.setText(item.getUsers__first_name() + " *(Anonymus)");

                        } else {
//                            holder.tv_center.setText(item.getUsers__first_name() + " " + item.getUsers__last_name() + " *(Anonymus)");
                            holder.tv_center.setText(" *(Anonymus)");
                        }

                    } else {
                        holder.tv_center.setText("(Anonymus)");
                    }
                } else {

                    if (item.getUsers__first_name() == null && item.getUsers__last_name() == null) {

                    } else if (item.getUsers__first_name() == null && item.getUsers__last_name() != null) {
                        holder.tv_center.setText(item.getUsers__last_name());
                    } else if (item.getUsers__first_name() != null && item.getUsers__last_name() == null) {
                        holder.tv_center.setText(item.getUsers__first_name());
                    } else {
                        holder.tv_center.setText(item.getUsers__first_name() + " " + item.getUsers__last_name());
                    }

                }
            }

            Log.d("PledgeIssue", "fundraising :" + item.getFundraising());
            Log.d("PledgeIssue", "Charity :" + item.getCharity());

            if (Common.is_Admin_flag == true) {
                holder.tv_left1.setText(item.getFundraisingPledges__amount() + " USD");
            } else {
                if (item.getFundraisingPledges__hide_amount().equals("1")) {
                    Log.d("PledgeIssue", "hide ammount 1");
                    if ((Common.login_data.getData().getId()).equals(item.getFundraisingPledges__user_id())) {
                        Log.d("PledgeIssue", "user id =");
                        holder.tv_left1.setText("");
//                        holder.tv_left1.setText(item.getFundraisingPledges__amount() + " USD " + " *(Anonymus)");
                    } else if (!((Common.login_data.getData().getId()).equals(item.getFundraisingPledges__user_id()))) {
                        Log.d("PledgeIssue1", "Pledge value Anonymus");
//                        holder.tv_left1.setText("(Anonymus)");
                        holder.tv_left1.setText("");
                    }
                } else {
                    Log.d("PledgeIssue", "hide ammount 0");
                    holder.tv_left1.setText(item.getFundraisingPledges__amount() + " USD");
                }
            }

            Log.d("PledgeIssue", "......................................");
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class RecyclerviewViewHolder extends RecyclerView.ViewHolder {

        TextView tv_right1;
        TextView tv_right2;
        TextView tv_center;
        TextView tv_left1;


        public RecyclerviewViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_right1 = itemView.findViewById(R.id.tv_right1);
            tv_right2 = itemView.findViewById(R.id.tv_right2);
            tv_center = itemView.findViewById(R.id.tv_Center);
            tv_left1 = itemView.findViewById(R.id.tv_left1);

        }
    }
}
