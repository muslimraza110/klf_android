package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Activities.Group.GroupsDetail;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Parent_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Project_Model;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Project_Model;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.Activities.Project.View_Projects_Detail_Updated;

import java.util.ArrayList;
import java.util.List;

public class View_Profile_Business_ExpandableListView implements ExpandableListAdapter {

    private Context context;
    private List<View_Profile_Bussiness_Parent_Model> brands_model = new ArrayList<>();
    RecyclerViewClickListner listner;


    public View_Profile_Business_ExpandableListView(Context context, List<View_Profile_Bussiness_Parent_Model> brands_model, RecyclerViewClickListner listner) {
        this.context = context;
        this.brands_model = brands_model;
        this.listner = listner;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return brands_model.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return brands_model.get(groupPosition).getChild_list().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return brands_model.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return brands_model.get(groupPosition).getChild_list().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {

        try {
            ParentHolder parentHolder = null;

            final View_Profile_Bussiness_Parent_Model parent_model=brands_model.get(groupPosition);

            if (convertView == null) {
                LayoutInflater userInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = userInflater.inflate(R.layout.aa_view_profile_busi_charity_parent_view, parent,false);
                convertView.setVerticalScrollBarEnabled(true);

                parentHolder = new ParentHolder();
                convertView.setTag(parentHolder);

            } else {
                parentHolder = (ParentHolder) convertView.getTag();
            }


            parentHolder.brandName = (TextView) convertView.findViewById(R.id.faq_parent_text);
            parentHolder.brandName.setText(parent_model.getBussiness_name());



            parentHolder.indicator = (ImageView) convertView.findViewById(R.id.image_indicator);
            if (isExpanded) {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_up);
            } else {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_down);
            }

            parentHolder.layout_text=convertView.findViewById(R.id.layout_text);
            parentHolder.layout_dropdown=convertView.findViewById(R.id.layout_dropdown);

            parentHolder.layout_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,"Text click",Toast.LENGTH_SHORT).show();
                    String Groups__id,Groups__category_id,Groups__name,Groups__description,Groups__email,groupimage;
                    String Groups__address,Groups__address2,Groups__phone,Groups__bio;
                    String GroupsUsers__id,Groups__city,Groups__country;
            //this is jugaar
                    View_Profile_Bussiness_Project_Model model_b=new View_Profile_Bussiness_Project_Model();
                    model_b.setGroups__id(parent_model.getId());
                    model_b.setGroups__name(parent_model.getBussiness_name());
                    model_b.setGroups__description(parent_model.getGroups__description());
                    model_b.setGroups__email(parent_model.getGroups__email());
                    model_b.setGroups__phone(parent_model.getGroups__phone());
                    model_b.setGroups__address(parent_model.getGroups__address()+" "+parent_model.getGroups__address2());
                    model_b.setGroups__bio(parent_model.getGroups__bio());
                    model_b.setGroupimage(parent_model.getGroups__groupimage());


                    View_Profile_Charity_Project_Model model_c=new View_Profile_Charity_Project_Model();
                    model_c.setGroups__id(parent_model.getId());
                    model_c.setGroups__name(parent_model.getBussiness_name());
                    model_c.setGroups__description(parent_model.getGroups__description());
                    model_c.setGroups__email(parent_model.getGroups__email());
                    model_c.setGroups__phone(parent_model.getGroups__phone());
                    model_c.setGroups__address(parent_model.getGroups__address()+" "+parent_model.getGroups__address2());
                    model_c.setGroups__bio(parent_model.getGroups__bio());
                    model_c.setGroupimage(parent_model.getGroups__groupimage());

//                    Common.user_profile_businessandprojects_data =model_b;
//                    Common.user_profile_charitiesandprojects_data=model_c;

//                    Common.is_all_group_detail = true;
//                    Common.viewprofile_group_detail = true;
//                    Common.is_viewProfile_group_business = false;

                    Intent group = new Intent(context, GroupsDetail.class);
                    group.putExtra("ViewProfileCharityData",model_c);
                    group.putExtra("isAllGroupDetail","1");
                    group.putExtra("ViewProfileDetail","1");
                    group.putExtra("isProfileBusinessData","0");
                    context.startActivity(group);

                }
            });
            parentHolder.layout_dropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isExpanded) ((ExpandableListView) parent).collapseGroup(groupPosition);
                    else ((ExpandableListView) parent).expandGroup(groupPosition, true);

                }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {
            String title = brands_model.get(groupPosition).getChild_list().get(childPosition).getName();
            final String id=brands_model.get(groupPosition).getChild_list().get(childPosition).getProject_id();

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.aa_view_profile_busi_charity_child_view, parent,false);
            }

            TextView textTitle = (TextView) convertView.findViewById(R.id.view_profile_child_name);
            textTitle.setText(title);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Common.is_project_detail=false;
//                    Common.project_id_for_detail=id;

                    Intent view_event=new Intent(context, View_Projects_Detail_Updated.class);
                    view_event.putExtra("ProjectId",id);
                    view_event.putExtra("isProjectDetail","0");
                    context.startActivity(view_event);

                }
            });
      throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    private static class ChildHolder {
         TextView name;
         RecyclerView recyclerView;
    }

    private static class ParentHolder {
        TextView brandName;
        ImageView indicator;

        LinearLayout layout_text,layout_dropdown;
    }
}


