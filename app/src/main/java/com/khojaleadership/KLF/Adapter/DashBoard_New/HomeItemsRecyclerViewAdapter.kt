package com.khojaleadership.KLF.Adapter.DashBoard_New

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.DashBoard_New.NavigationHomeUiModel
import com.khojaleadership.KLF.databinding.ItemHomeNavigationBinding

class HomeItemsRecyclerViewAdapter : ListAdapter<NavigationHomeUiModel, HomeItemsRecyclerViewAdapter.ViewHolder>(HomeItemsRecyclerViewDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return NavigationItemViewHolder(
                ItemHomeNavigationBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


//    override fun getItemViewType(position: Int): Int {
//        return when(getItem(position).itemType){
//            HomeEventsItemType.EVENT->{
//                R.layout.item_home_event
//            }
//            HomeEventsItemType.PROJECT->{
//                R.layout.item_home_project
//            }
//            else->{
//                R.layout.item_home_funding
//            }
//        }
//    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: NavigationHomeUiModel)
    }

    class NavigationItemViewHolder(val binding: ItemHomeNavigationBinding) : ViewHolder(binding.root) {
        override fun bind(item: NavigationHomeUiModel) {
            binding.name.text = item.name
        }
    }


}

class HomeItemsRecyclerViewDiffUtil : DiffUtil.ItemCallback<NavigationHomeUiModel>() {
    override fun areItemsTheSame(
            oldItem: NavigationHomeUiModel,
            newItem: NavigationHomeUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: NavigationHomeUiModel,
            newItem: NavigationHomeUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
