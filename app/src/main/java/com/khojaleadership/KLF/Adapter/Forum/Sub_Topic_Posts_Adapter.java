package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Forum_Section.MyWebView;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.PostModels.GetPostListDataCommentsModel_Updated;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Sub_Topic_Posts_Adapter extends RecyclerView.Adapter<Sub_Topic_Posts_Adapter.viewHolder> {
    Context mctx;
    ArrayList<GetPostListDataCommentsModel_Updated> list;
    RecyclerViewClickListner listner;


    public Sub_Topic_Posts_Adapter(Context mctx, ArrayList<GetPostListDataCommentsModel_Updated> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mctx);
        View v = inflater.inflate(R.layout.subtopic_post_new, null);

        return new viewHolder(v, listner);
    }

    @Override
    public void onBindViewHolder(@NonNull final viewHolder viewHolder, final int i) {

        try {

            GetPostListDataCommentsModel_Updated item = list.get(i);

            if (item != null) {
                String name = item.getUsers__first_name() + " " + item.getUsers__last_name();
                viewHolder.user_name.setText(name);

                viewHolder.last_login.setText(item.getUsers__last_login());
                viewHolder.date.setText("Posted on " + item.getForumPosts__created());

                viewHolder.like_text.setText("(" + item.getLikescount() + ")");
                viewHolder.dislike_text.setText("(" + item.getDislikescount() + ")");
                viewHolder.post_count.setText("Posts :" + item.getPosts());

                Glide.with(mctx.getApplicationContext()).load(item.getProfileImg__name()).into(viewHolder.post_img);


                //visibilty checks
                //1 agr role member h comment me tu send eblast ni aye ga ye sbb k lieye h
                if (item.getUsers__role() != null) {
                    if (item.getUsers__role().equals("C")) {
                        viewHolder.send_eblast_icon.setVisibility(View.GONE);
                    }
                }

                //4 agr user member h orr visibilty p h tu sirf apni post ko edit delete krr skta h
                if (Common.is_Admin_flag == false) {
                    viewHolder.edit_icon.setVisibility(View.GONE);
                    viewHolder.delete_icon.setVisibility(View.GONE);
                    viewHolder.send_eblast_icon.setVisibility(View.GONE);

                    if (Common.login_data.getData().getId().equals(item.getForumPosts__user_id())) {
                        viewHolder.edit_icon.setVisibility(View.VISIBLE);
                        viewHolder.delete_icon.setVisibility(View.VISIBLE);
                    }

                }


                if (Common.is_Admin_flag == true) {
                    viewHolder.send_eblast_icon.setVisibility(View.VISIBLE);
                }

//                //3 agr visibilty admin h tu like dislike subscribe only show hona and add comment ki option ni h
//                if (Common.post_list_comment_data.get(0).getForumPosts__visibility() != null) {
//                    if (Common.post_list_comment_data.get(0).getIs_moderator() == 1 && Common.is_Admin_flag == true) {
//                        viewHolder.send_eblast_icon.setVisibility(View.VISIBLE);
//                    }
//
//                    if (Common.post_list_comment_data.get(0).getForumPosts__visibility().equals("A")) {
//                        //or aggr user member h tu
//                        if (Common.is_Admin_flag == false) {
//                            viewHolder.edit_icon.setVisibility(View.GONE);
//                            viewHolder.delete_icon.setVisibility(View.GONE);
//                            viewHolder.send_eblast_icon.setVisibility(View.GONE);
//                            viewHolder.add_qoute_icon.setVisibility(View.GONE);
//                        }
//                    }
//                }


                //3 agr visibilty admin h tu like dislike subscribe only show hona and add comment ki option ni h
                if (list.get(0).getForumPosts__visibility() != null) {
                    if (list.get(0).getIs_moderator() == 1 && Common.is_Admin_flag == true) {
                        viewHolder.send_eblast_icon.setVisibility(View.VISIBLE);
                    }

                    if (list.get(0).getForumPosts__visibility().equals("A")) {
                        //or aggr user member h tu
                        if (Common.is_Admin_flag == false) {
                            viewHolder.edit_icon.setVisibility(View.GONE);
                            viewHolder.delete_icon.setVisibility(View.GONE);
                            viewHolder.send_eblast_icon.setVisibility(View.GONE);
                            viewHolder.add_qoute_icon.setVisibility(View.GONE);
                        }
                    }
                }


                //4 agr user member h orr visibilty p h tu sirf apni post ko edit delete krr skta h
                if (Common.is_Admin_flag == false) {
                    viewHolder.edit_icon.setVisibility(View.GONE);
                    viewHolder.delete_icon.setVisibility(View.GONE);

                    if (Common.login_data.getData().getId().equals(item.getForumPosts__user_id())) {
                        viewHolder.edit_icon.setVisibility(View.VISIBLE);
                        viewHolder.delete_icon.setVisibility(View.VISIBLE);
                    }

                }

                viewHolder.myWebView.loadData(item.getForumPosts__content(), "text/html; charset=utf-8", "UTF-8");
                viewHolder.myWebView.setVerticalScrollBarEnabled(true);
                viewHolder.myWebView.setHorizontalScrollBarEnabled(true);
                viewHolder.myWebView.setWebViewClient(new WebViewClient());


                //already set images of like and dislike
                if (item.getIslike() == 1) {
                    viewHolder.like_icon.setImageResource(R.drawable.ic_thumb_up_blue_24dp);
                }


                if (item.getIsdislike() == 1) {
                    viewHolder.dislike_icon.setImageResource(R.drawable.ic_thumb_down_blue_24dp);
                }


//                Common.l_count = Integer.valueOf(list.get(i).getLikescount());
//                Common.d_count = Integer.valueOf(list.get(i).getDislikescount());

                //approve btn visibilty ON
                if (item.getForumPosts__status() != null) {
                    if (item.getForumPosts__status().equals("P") && list.get(0).getIs_moderator() == 1) {
                        viewHolder.approve_btn.setVisibility(View.VISIBLE);
                    } else if (item.getForumPosts__status().equals("P") && list.get(0).getIs_moderator() == 0) {
                        if (Common.is_Admin_flag == true) {
                            if (item.getForumPosts__status().equals("P")) {
                                viewHolder.approve_btn.setVisibility(View.VISIBLE);
                            } else {
                                viewHolder.approve_btn.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        viewHolder.approve_btn.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.approve_btn.setVisibility(View.GONE);
                }

            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(ArrayList<GetPostListDataCommentsModel_Updated> new_list) {
        list = new_list;
        notifyDataSetChanged();
    }

    public void AddData(ArrayList<GetPostListDataCommentsModel_Updated> new_list) {
        for (GetPostListDataCommentsModel_Updated item : new_list) {
            list.add(item);
        }
        notifyDataSetChanged();
    }

    class viewHolder extends RecyclerView.ViewHolder {
        TextView user_name, last_login, date;
        MyWebView myWebView;

        LinearLayout edit_icon, add_qoute_icon, send_eblast_icon, delete_icon;
        LinearLayout like_btn, dislike_btn, approve_btn;


        TextView like_text, dislike_text, post_count;

        ImageView like_icon, dislike_icon;
        ImageView post_img;


        public viewHolder(@NonNull View itemView, final RecyclerViewClickListner listner) {
            super(itemView);

            user_name = (TextView) itemView.findViewById(R.id.p_user_name);
            last_login = (TextView) itemView.findViewById(R.id.p_user_last_login);
            date = (TextView) itemView.findViewById(R.id.p_day_date);
            like_text = (TextView) itemView.findViewById(R.id.p_like_text);
            dislike_text = (TextView) itemView.findViewById(R.id.p_dislike_text);
            post_count = (TextView) itemView.findViewById(R.id.p_post_count);
            like_icon = (ImageView) itemView.findViewById(R.id.like_icon);
            dislike_icon = (ImageView) itemView.findViewById(R.id.dislike_icon);
            post_img = (ImageView) itemView.findViewById(R.id.post_img);


            like_btn = (LinearLayout) itemView.findViewById(R.id.like_btn);
            like_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });

            dislike_btn = (LinearLayout) itemView.findViewById(R.id.dislike_btn);
            dislike_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });


            edit_icon = (LinearLayout) itemView.findViewById(R.id.post_edit_icon);
            edit_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });


            add_qoute_icon = (LinearLayout) itemView.findViewById(R.id.post_add_qoute_icon);
            add_qoute_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });

            delete_icon = (LinearLayout) itemView.findViewById(R.id.post_delete_icon);
            delete_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });

            approve_btn = (LinearLayout) itemView.findViewById(R.id.post_approve_icon);
            approve_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });


            send_eblast_icon = (LinearLayout) itemView.findViewById(R.id.send_eblast_layout);
            send_eblast_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), view);
                    }
                }
            });


            myWebView = itemView.findViewById(R.id.myWebview);
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);


        }
    }

}
