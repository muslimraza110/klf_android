package com.khojaleadership.KLF.Adapter.event_new.planner.breakoutSessions

import android.content.Context
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.planner.sessions.SessionItemUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Helper.CyanUnderlinedSpan
import com.khojaleadership.KLF.Helper.MyClickableSpan
import com.khojaleadership.KLF.Model.event_new.SessionType
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemSessionBinding

class SessionsAdapter(
    private val context: Context,
    private val onJoinLeaveBtnClick: (SessionItemUiModel) -> Unit,
    private val onHyperLinkClick: (PersonDetailUiModel) -> Unit,
    private val sessionType: String,
    private val onMeetingLinkClick: (String) -> Unit,
    private val onMeetingLinkLongClick: (String) -> Unit
) : ListAdapter<SessionItemUiModel, SessionsAdapter.ViewHolder>(SessionsDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return BreakoutSessionsViewHolder(
            binding = ItemSessionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            context = context,
            onJoinLeaveBtnClick = onJoinLeaveBtnClick,
            onHyperLinkClick = onHyperLinkClick,
            sessionType = sessionType,
            onMeetingLinkClick = onMeetingLinkClick,
            onMeetingLinkLongClick = onMeetingLinkLongClick
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: SessionItemUiModel)
    }

    class BreakoutSessionsViewHolder(
        private val binding: ItemSessionBinding,
        private val context: Context,
        private val onJoinLeaveBtnClick: (SessionItemUiModel) -> Unit,
        private val onHyperLinkClick: (PersonDetailUiModel) -> Unit,
        private val sessionType: String,
        private val onMeetingLinkClick: (String) -> Unit,
        private val onMeetingLinkLongClick: (String) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: SessionItemUiModel) {
            item.apply {
//                binding.tvDescription.text = description
                binding.tvTitle.text = title

                binding.tvMeetingLink.text = ""
                if(zoomMeetingUrl.isNotEmpty()){
                    binding.tvMeetingLink.visibility = View.VISIBLE
                    val zoomMeetingString = "Zoom Meeting Link: "
                    var completeText = SpannedString("")
                    val zoomMeetingUrlSpannableString = SpannableString(zoomMeetingUrl)
                    zoomMeetingUrlSpannableString.setSpan(
                        CyanUnderlinedSpan(context),
                        0,
                        zoomMeetingUrl.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    completeText = TextUtils.concat(
                        zoomMeetingString,
                        zoomMeetingUrlSpannableString
                    ) as SpannedString
                    binding.tvMeetingLink.text = buildSpannedString {
                        bold {
                            append(zoomMeetingString)
                        }
                        append(zoomMeetingUrlSpannableString)
                    }
                    binding.tvMeetingLink.setOnLongClickListener {
                        onMeetingLinkLongClick.invoke(zoomMeetingUrl)
                        true
                    }
                    binding.tvMeetingLink.setOnClickListener {
                        onMeetingLinkClick.invoke(zoomMeetingUrl)
                    }
                }else{
                    binding.tvMeetingLink.visibility = View.GONE
                }

                if (sessionType == SessionType.BREAKOUT.value) {
                    binding.tvRoomLimit.text = "Room Limit: $roomLimit"
                    binding.tvSeatsAvailable.text = "Available Seats: ${roomLimit - roomOccupied}"

                    if (iAmEnrolled) {
                        binding.btnJoin.text = "Leave"
                        binding.btnJoin.background = ContextCompat.getDrawable(
                            context,
                            R.drawable.red_btn_bg
                        )
                    } else {
                        binding.btnJoin.text = "Join"
                        binding.btnJoin.background = ContextCompat.getDrawable(
                            context,
                            R.drawable.blue_btn_bg_new
                        )
                    }

                    binding.btnJoin.setOnClickListener {
                        onJoinLeaveBtnClick(this)
                    }
                } else {
                    binding.btnJoin.visibility = View.GONE
                    binding.tvRoomLimit.visibility = View.GONE
                    binding.tvSeatsAvailable.visibility = View.GONE
                }


                // For clickable text in textView

                val confirmedSpeakerText = if (item.speakers.isNotEmpty()) {
                    val str =   Html.fromHtml(" \n\n <br> <b> Confirmed Speaker:</b> \n \n")
                    if (item.speakers.size == 1) str else str
                } else ""
                val confirmedDelegatesText = if (item.delegates.isNotEmpty()) {
                    if (item.delegates.size == 1) "\n\nConfirmed Delegate:" else "\n\nConfirmed Delegates:"
                } else ""

                var speakersSpannableText = SpannedString("")
                for (speaker in item.speakers) {
                    val speakerName = SpannableString(speaker.name)


                    speakerName.setSpan(
                        MyClickableSpan(speaker, onHyperLinkClick, context),
                        0,
                        speakerName.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )

                    val spannedSpeakerName = TextUtils.concat(
                        speakerName,
                        if (speaker.designation.isNotEmpty())
                            ", ${speaker.designation}." else ""
//                                ", ${speaker.company}" +
//                                ", ${speaker.country}" +
                    ) as SpannedString

                    val spaceSpannedString = SpannableString("\n\n")
                    spaceSpannedString.setSpan(
                        RelativeSizeSpan(0.45f),
                        0,
                        spaceSpannedString.length,
                        0
                    ) // set size


                    speakersSpannableText = TextUtils.concat(
                        speakersSpannableText,
                        spaceSpannedString,
                        spannedSpeakerName
                    ) as SpannedString

                }

                speakersSpannableText = TextUtils.concat(
                    item.description,
                    confirmedSpeakerText,
                    speakersSpannableText
                ) as SpannedString

                var delegatesSpannableText = SpannedString("")

                for (delegate in item.delegates) {
                    val delegateName = SpannableString(delegate.name)


                    delegateName.setSpan(
                        MyClickableSpan(delegate, onHyperLinkClick, context),
                        0,
                        delegateName.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )

                    val spannedSpeakerName = TextUtils.concat(
                        delegateName,
                        if (delegate.designation.isNotEmpty())
                            ", ${delegate.designation}." else ""
//                                ", ${delegate.company}" +
//                                ", ${delegate.country}" +
                    ) as SpannedString

                    val spaceSpannedString = SpannableString("\n\n")
                    spaceSpannedString.setSpan(
                        RelativeSizeSpan(0.45f),
                        0,
                        spaceSpannedString.length,
                        0
                    ) // set size


                    delegatesSpannableText = TextUtils.concat(
                        delegatesSpannableText,
                        spaceSpannedString,
                        spannedSpeakerName
                    ) as SpannedString

                }

                speakersSpannableText = TextUtils.concat(
                    speakersSpannableText,
                    confirmedDelegatesText,
                    delegatesSpannableText
                ) as SpannedString



                binding.tvDescription.text = speakersSpannableText
                binding.tvDescription.movementMethod = LinkMovementMethod.getInstance()


            }
        }
    }
}

class SessionsDiffUtil : DiffUtil.ItemCallback<SessionItemUiModel>() {
    override fun areItemsTheSame(
        oldItem: SessionItemUiModel,
        newItem: SessionItemUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: SessionItemUiModel,
        newItem: SessionItemUiModel
    ): Boolean {
        return oldItem == newItem
    }
}

