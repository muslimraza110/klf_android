package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.Project_Member_Data_List;
import com.khojaleadership.KLF.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class IsProjectMemberListAdapter extends RecyclerView.Adapter<IsProjectMemberListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<Project_Member_Data_List> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    public IsProjectMemberListAdapter(Context context, List<Project_Member_Data_List> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.whitelist_contact_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description= (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description= (TextView) view.findViewById(R.id.wlc_sub_description);
        viewHolder.ic_user_image_icon=view.findViewById(R.id.ic_user_image_icon);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        try {

            //Boolean already_checked=false;
            int section = getSectionForPosition(position);

            holder.name.setText(mData.get(position).getTitle()+" "+mData.get(position).getFirst_name()+" "+mData.get(position).getLast_name());
            holder.description.setText(this.mData.get(position).getEmail());
            holder.sub_description.setText(this.mData.get(position).getRole());

            Glide.with(mContext)
                    .load(mData.get(position).getProfile_image())
                    .into(holder.ic_user_image_icon);

            //strt me false krna
            holder.add_whitelist_contact.setChecked(false);

            if (mData.get(position).getIsmember()==1){
                holder.add_whitelist_contact.setChecked(true);
            }else if (mData.get(position).getIsmember()==0){
                holder.add_whitelist_contact.setChecked(false);
            }


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }


    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<Project_Member_Data_List> list){
        this.mData = list;
        notifyDataSetChanged();
    }

    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tag,name, description,sub_description;
        CheckBox add_whitelist_contact;

        CircleImageView ic_user_image_icon;
        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            add_whitelist_contact=(CheckBox)itemView.findViewById(R.id.whitelist_contact_checkbox);

            add_whitelist_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = add_whitelist_contact.isChecked();
                    // Do something here.
                    if (listner != null) {
                         listner.onViewClcik(getAdapterPosition(), view);

                    }
                }
            });




        }
    }

}
