package com.khojaleadership.KLF.Adapter.Intiative;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Fundraising_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class IntiativeFundraising_Adapter extends RecyclerView.Adapter<IntiativeFundraising_Adapter.MyViewHolder> {

    Context context;
    ArrayList<Intiative_Detail_Data_Fundraising_Model> arrayList;

    public IntiativeFundraising_Adapter(Context context, ArrayList<Intiative_Detail_Data_Fundraising_Model> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fundraising_recyclerview_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        try {
            Intiative_Detail_Data_Fundraising_Model fund_model=arrayList.get(position);

            //setting fundraising data
            holder.fund_name.setText(fund_model.getFundraisings__name());
            if (fund_model.getFundraisings__type().equals("M")) {
                holder.fund_type.setText("Match");
            }else if (fund_model.getFundraisings__type().equals("P")) {
                holder.fund_type.setText("Pledge");
            }else if (fund_model.getFundraisings__type().equals("B")) {
                holder.fund_type.setText("Block");
            }
            holder.fund_ammount.setText(fund_model.getTotalfundraising() +" USD");

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView fund_name, fund_type, fund_ammount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            //fundraising
            fund_name = itemView.findViewById(R.id.fund_name);
            fund_type = itemView.findViewById(R.id.fund_type);
            fund_ammount = itemView.findViewById(R.id.fund_ammount);

        }
    }
}
