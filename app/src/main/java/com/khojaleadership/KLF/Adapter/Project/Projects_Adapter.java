package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Projects_Adapter extends RecyclerView.Adapter<Projects_Adapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<ProjectsDataModel> list;
   RecyclerViewClickListner listner;

    public Projects_Adapter(Context mctx, ArrayList<ProjectsDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.projects_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {
        try {
            ProjectsDataModel item=list.get(i);
            myviewHolderClass.tv_title.setText(item.getProjects__name());
            myviewHolderClass.tv_admin.setText("Admin");
            myviewHolderClass.tv_city.setText(item.getProjects__city()+","+item.getProjects__country());

            if (Common.is_Admin_flag==false){
                if (Common.login_data.getData().getId().equals(item.getCreators__id())){
                    myviewHolderClass.edit.setVisibility(View.VISIBLE);
                    myviewHolderClass.delete.setVisibility(View.VISIBLE);
                }else {
                    myviewHolderClass.edit.setVisibility(View.GONE);
                    myviewHolderClass.delete.setVisibility(View.GONE);
                }
            }else {
                myviewHolderClass.edit.setVisibility(View.VISIBLE);
                myviewHolderClass.delete.setVisibility(View.VISIBLE);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView tv_title,tv_admin,tv_city;
        ImageView img_viewIcon;
        ImageView ic_delete;

        LinearLayout edit,delete;
        CardView cardView;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            tv_title  = (TextView) itemView.findViewById(R.id.tv_title);
            tv_admin  = (TextView) itemView.findViewById(R.id.tv_Admin);
            tv_city  = (TextView) itemView.findViewById(R.id.tv_city);

            edit= (LinearLayout) itemView.findViewById(R.id.edit_project_layout);
            delete= (LinearLayout) itemView.findViewById(R.id.delete_project_layout);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });

            cardView = (CardView)itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });

        }
    }
}

