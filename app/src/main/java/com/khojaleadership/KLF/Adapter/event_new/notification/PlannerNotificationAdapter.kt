package com.khojaleadership.KLF.Adapter.event_new.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.notification.PlannerNotificationUiModel
import com.khojaleadership.KLF.databinding.ItemPlannerNotificationsBinding

class PlannerNotificationAdapter(
        private val context: Context,
        private val onNotificationClick: (PlannerNotificationUiModel) -> Unit
) : ListAdapter<PlannerNotificationUiModel, PlannerNotificationAdapter.ViewHolder>(PlannerNotificationDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return PlannerNotificationViewHolder(
                ItemPlannerNotificationsBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ),
                context,
                onNotificationClick
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: PlannerNotificationUiModel)
    }

    class PlannerNotificationViewHolder(
            private val binding: ItemPlannerNotificationsBinding,
            private val context: Context,
            private val onNotificationClick: (PlannerNotificationUiModel) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: PlannerNotificationUiModel) {

            item.apply {
                binding.tvNotification.text = notificationString

                binding.tvTime.text = time

                binding.root.setOnClickListener{
                    onNotificationClick.invoke(this)
                }
            }

        }
    }
}

class PlannerNotificationDiffUtil : DiffUtil.ItemCallback<PlannerNotificationUiModel>() {
    override fun areItemsTheSame(
            oldItem: PlannerNotificationUiModel,
            newItem: PlannerNotificationUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: PlannerNotificationUiModel,
            newItem: PlannerNotificationUiModel
    ): Boolean {
        return oldItem == newItem
    }
}

