package com.khojaleadership.KLF.Adapter.event_new.meetingDetails

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.khojaleadership.KLF.Activities.event_new.meetingDetails.meetings.MeetingsFragment
import com.khojaleadership.KLF.Activities.event_new.planner.requestMeetingNew.RequestMeetingNewFragment

class MeetingDetailsViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int = 2


    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> MeetingsFragment()
        1 -> RequestMeetingNewFragment()
        else -> throw IllegalArgumentException("Position not defined")
    }
}