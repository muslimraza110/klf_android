package com.khojaleadership.KLF.Adapter.Event;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Event_List_Adapter extends RecyclerView.Adapter<Event_List_Adapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<GetEventsListDataModel> list;
   RecyclerViewClickListner listner;

    public Event_List_Adapter(Context mctx, ArrayList<GetEventsListDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.events_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {


        try{
            GetEventsListDataModel item=list.get(i);
            myviewHolderClass.event_date.setText(item.getStart_date());
            myviewHolderClass.event_name.setText(item.getName());

            if (Common.is_Admin_flag==false){
                myviewHolderClass.edit_event.setVisibility(View.GONE);
            }

            if (item.getUser_id().equals(Common.login_data.getData().getId())){
                myviewHolderClass.edit_event.setVisibility(View.VISIBLE);
            }
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView event_date,event_name;
        RelativeLayout edit_event;
        LinearLayout cardView;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            event_date= (TextView) itemView.findViewById(R.id.event_date);
            event_name= (TextView) itemView.findViewById(R.id.event_name);
            edit_event=  itemView.findViewById(R.id.edit_event);

            edit_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });


            cardView = itemView.findViewById(R.id.event_cardview);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });

        }
    }
}

