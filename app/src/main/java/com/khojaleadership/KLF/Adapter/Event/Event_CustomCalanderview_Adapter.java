package com.khojaleadership.KLF.Adapter.Event;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Event_CustomCalanderview_Adapter extends ArrayAdapter {

    List<Date> dates;
    Calendar currentDate;
    List<GetEventsListDataModel> events;
    LayoutInflater inflater;
    RecyclerViewClickListner listner;


    public Event_CustomCalanderview_Adapter(@NonNull Context context, List<Date> dates, Calendar currentDate, List<GetEventsListDataModel> events, RecyclerViewClickListner listner) {
        super(context, R.layout.event_single_cell_layout);

        this.dates = dates;
        this.currentDate = currentDate;
        this.events = events;
        inflater = LayoutInflater.from(context);
        this.listner=listner;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.event_single_cell_layout, parent, false);

        }

        try{
            Date monthDate = dates.get(position);
            Calendar dateCalendar = Calendar.getInstance();
            dateCalendar.setTime(monthDate);
            int DayNo = dateCalendar.get(Calendar.DAY_OF_MONTH);
            int displayMonth = dateCalendar.get(Calendar.MONTH)
                    + 1;
            int displayYear = dateCalendar.get(Calendar.YEAR);
            int currentMonth = currentDate.get(Calendar.MONTH) + 1;
            int currentYear = currentDate.get(Calendar.YEAR);


            if (displayMonth == currentMonth && displayYear == currentYear) {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.blue));

            } else {
                view.setBackgroundColor(Color.parseColor("#9a9a9a"));

            }

            TextView Day_Number = view.findViewById(R.id.calendar_day);
            TextView EventNumber = view.findViewById(R.id.events_id);
            Day_Number.setText(String.valueOf(DayNo));
            Day_Number.setTextColor(Color.WHITE);

            Calendar eventCalendar = Calendar.getInstance();

            ArrayList<String> arrayList = new ArrayList<String>();
            final ArrayList<GetEventsListDataModel> event_list=new ArrayList<>();

            arrayList.clear();
            event_list.clear();

            for (int i = 0;i < events.size();i++){

                String[] split=(events.get(i).getStart_date()).split(" ");

                eventCalendar.setTime(ConvertStringToDate(split[0]));
                if(DayNo == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH)+1
                        && displayYear == eventCalendar.get(Calendar.YEAR)){

                    event_list.add(events.get(i));
                    arrayList.add(events.get(i).getName());


                    EventNumber.setText(arrayList.size()+" Events");


                }

            }

//            Event_CustomCalanderview calanderview = null;
//            calanderview.PassEventsByDay(event_list);
//
           // Common.events_by_day=event_list;


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Common.events_by_day=event_list;
                    if (listner!=null){
                        listner.onViewClcik(position,view);
                    }
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }

        //Common.events_by_day=event_list;


        return view;
    }


    private Date ConvertStringToDate(String eventDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(eventDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }


    @Override
    public int getCount() {
        return dates.size();
    }


    @Override
    public int getPosition(@Nullable Object item) {
        return dates.indexOf(item);
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return dates.get(position);
    }
}
