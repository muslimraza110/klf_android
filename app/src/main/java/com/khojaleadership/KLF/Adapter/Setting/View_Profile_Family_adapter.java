package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Family_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;


public class View_Profile_Family_adapter extends RecyclerView.Adapter<View_Profile_Family_adapter.Family_viewholder> {


    Context mctx;
    ArrayList<View_Profile_Family_Model> family_models;


    public View_Profile_Family_adapter(Context mctx, ArrayList<View_Profile_Family_Model> family_models) {
        this.mctx = mctx;
        this.family_models = family_models;
    }

    @NonNull
    @Override
    public Family_viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.aa_view_profile_family_recylerview_items, parent, false);


        return new Family_viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Family_viewholder holder, int position) {

        try {
            final View_Profile_Family_Model familyModel = family_models.get(position);

            if (TextUtils.isEmpty(familyModel.getRelatedUsers__first_name()) && TextUtils.isEmpty(familyModel.getRelatedUsers__last_name())) {

            } else if (familyModel.getRelatedUsers__first_name().equals("null") && familyModel.getRelatedUsers__last_name().equals("null")) {

            } else {
                holder.tv_relation_name.setText(familyModel.getRelatedUsers__first_name() + " " + familyModel.getRelatedUsers__last_name());
                holder.tv_relation_name.setPaintFlags(holder.tv_relation_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }


            if (TextUtils.isEmpty(familyModel.getUserFamilies__relation())) {

            } else if (familyModel.getUserFamilies__relation().equals("null")) {

            } else {
                holder.tv_relation_type.setText(familyModel.getUserFamilies__relation() + " of ");
            }

            //holder.img_family.setImageResource(familyModel.getImg_famliy());
            Glide.with(mctx).load(familyModel.getProfileImg__name()).into(holder.img_family);

            holder.family_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mctx, View_Profile_Activity_Updated.class);
                    i.putExtra("view_profile_id",familyModel.getRelatedUsers__id());
                    i.putExtra("isMasterSearch","1");
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mctx.startActivity(i);

                }
            });
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
        }


    }

    @Override
    public int getItemCount() {
        return family_models.size();
    }

    public class Family_viewholder extends RecyclerView.ViewHolder {

        ImageView img_family;
        TextView tv_relation_name;
        TextView tv_relation_type;

        LinearLayout cardView;

        LinearLayout family_layout;


        public Family_viewholder(@NonNull View itemView) {
            super(itemView);


            img_family = itemView.findViewById(R.id.img_family);
            tv_relation_name = itemView.findViewById(R.id.tv_relation_name);
            tv_relation_type = itemView.findViewById(R.id.tv_relation_type);

            family_layout = itemView.findViewById(R.id.family_layout);

            cardView = itemView.findViewById(R.id.cardview);
        }
    }
}
