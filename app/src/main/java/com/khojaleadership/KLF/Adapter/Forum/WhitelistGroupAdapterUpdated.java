package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetWhitelistedGroup_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class WhitelistGroupAdapterUpdated extends RecyclerView.Adapter<WhitelistGroupAdapterUpdated.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetWhitelistedGroup_Data_Model> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    public WhitelistGroupAdapterUpdated(Context context, List<GetWhitelistedGroup_Data_Model> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner = listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.whitelist_contact_recyclerview_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description = (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description = (TextView) view.findViewById(R.id.wlc_sub_description);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        try {

            //Boolean already_checked=false;
            int section = getSectionForPosition(position);


            holder.name.setText(this.mData.get(position).getName());
            holder.description.setText(this.mData.get(position).getEmail());
            holder.sub_description.setText(this.mData.get(position).getPhone());


            //strt me false krna
            holder.add_whitelist_contact.setChecked(false);
            if (mData.get(position).getIswhitelisted() == 1) {
                holder.add_whitelist_contact.setChecked(true);
            } else if (mData.get(position).getIswhitelisted() == 0) {
                holder.add_whitelist_contact.setChecked(false);
            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }


    }


    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /**
     * itemClick
     */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }


    /**
     * Provide data to the Activity refresh
     *
     * @param list
     */
    public void updateList(List<GetWhitelistedGroup_Data_Model> list) {
        this.mData = list;
        notifyDataSetChanged();
    }

    /**
     * Get the Char ASCII value of the first letter of
     * the classification according to the current position of the ListView
     */
    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }


    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tag, name, description, sub_description;
        CheckBox add_whitelist_contact;
        CircleImageView imageView;

        public ViewHolder(View itemView, final RecyclerViewClickListner listner) {
            super(itemView);

            imageView = itemView.findViewById(R.id.ic_user_image_icon);
            add_whitelist_contact = (CheckBox) itemView.findViewById(R.id.whitelist_contact_checkbox);

            add_whitelist_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = add_whitelist_contact.isChecked();
                    // Do something here.
                    if (listner != null) {
                         listner.onViewClcik(getAdapterPosition(), view);

                    }
                }
            });


        }
    }

}
