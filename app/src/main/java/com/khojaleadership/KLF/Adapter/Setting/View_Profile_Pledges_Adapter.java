package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Activities.Intiative.Intiative_Activity_Detail_Updated;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Pleger_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;


public class View_Profile_Pledges_Adapter extends RecyclerView.Adapter<View_Profile_Pledges_Adapter.Charity_viewHolder> {


    Context mctx;
    ArrayList<View_Profile_Pleger_Model> pleger_models;

    public View_Profile_Pledges_Adapter(Context mctx, ArrayList<View_Profile_Pleger_Model> pleger_models) {
        this.mctx = mctx;
        this.pleger_models = pleger_models;
    }

    @NonNull
    @Override
    public Charity_viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.aa_view_profile_pledger_recyclerview_item,parent,false);


        return new Charity_viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Charity_viewHolder holder, int position) {

        try {
            final View_Profile_Pleger_Model item= pleger_models.get(position);
            holder.v_p_pledger_name.setText(item.getPledge__name());
            holder.v_p_pledger_ammount.setText(item.getPledge__amount());

            holder.complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (item.getIs_deleted()==1){

                    }else if (item.getIs_deleted()==0){
//                        Common.intiative_id = item.getPledge__initiative_id();

                        Intent intent=new Intent(mctx, Intiative_Activity_Detail_Updated.class);
                        intent.putExtra("InitiativeId", item.getPledge__initiative_id());
                        mctx.startActivity(intent);

                    }
                }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return pleger_models.size();
    }

    public class Charity_viewHolder extends RecyclerView.ViewHolder {

        TextView v_p_pledger_name,v_p_pledger_ammount;
        LinearLayout complete_row;

        public Charity_viewHolder(@NonNull View itemView) {
            super(itemView);
            v_p_pledger_name= itemView.findViewById(R.id.v_p_pledger_name);
            v_p_pledger_ammount= itemView.findViewById(R.id.v_p_pledger_ammount);

            complete_row=itemView.findViewById(R.id.complete_row);
        }
    }
}
