package com.khojaleadership.KLF.Adapter.event_new.planner

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.MyItineraryFragment
import com.khojaleadership.KLF.Activities.event_new.planner.requestMeetingNew.RequestMeetingNewFragment

class PlannerViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int = 2


    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> MyItineraryFragment()
//        1 -> RequestMeetingFragment()
        1 -> RequestMeetingNewFragment()
        else -> throw IllegalArgumentException("Position not defined")
    }
}