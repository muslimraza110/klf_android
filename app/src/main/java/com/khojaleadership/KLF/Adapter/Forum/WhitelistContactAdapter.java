package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.IsWhiteListed_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class WhitelistContactAdapter extends RecyclerView.Adapter<WhitelistContactAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<IsWhiteListed_Data_Model> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    String IsWhitelistContacts;

    public WhitelistContactAdapter(Context context, List<IsWhiteListed_Data_Model> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.whitelist_contact_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description= (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description= (TextView) view.findViewById(R.id.wlc_sub_description);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        try {

             int section = getSectionForPosition(position);


            holder.name.setText(this.mData.get(position).getFirst_name());
            holder.description.setText(this.mData.get(position).getEmail());
            holder.sub_description.setText(this.mData.get(position).getRole());




            //strt me false krna
            holder.add_whitelist_contact.setChecked(false);

//            if (==false){
//
//            }else {

                if (mData.get(position).getIswhitelisted()==1){
                    holder.add_whitelist_contact.setChecked(true);
                }else if (mData.get(position).getIswhitelisted()==0){
                    holder.add_whitelist_contact.setChecked(false);
                }


         //   }


            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
            Log.d("NullExceptionIssue", "onRowClick: "+e.getMessage());
        }

    }


    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<IsWhiteListed_Data_Model> list){
        this.mData = list;
        notifyDataSetChanged();
    }


    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }


    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tag,name, description,sub_description;
        CheckBox add_whitelist_contact;
        CircleImageView imageView;
        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            imageView=itemView.findViewById(R.id.ic_user_image_icon);
            add_whitelist_contact=(CheckBox)itemView.findViewById(R.id.whitelist_contact_checkbox);

            add_whitelist_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = add_whitelist_contact.isChecked();
                    // Do something here.
                    if (listner != null) {
                        Log.d("AddContactToWhite", "onCheckedChanged on row click");
                        listner.onViewClcik(getAdapterPosition(), view);

                    }
                }
            });




        }
    }

}
