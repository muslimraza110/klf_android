package com.khojaleadership.KLF.Adapter.event_new.planner.MyItinerary

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.CheckAvailabilityItemUiModel
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.CheckAvailabilityStatus
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.R

class CheckAvailabilityExpandableAdapter(
        private val context: Context,
        private var listDataHeader: List<EventDaysUiModel>,
        private var listChildData: LinkedHashMap<EventDaysUiModel, List<CheckAvailabilityItemUiModel>>,
        private var onClick: (EventDaysUiModel, CheckAvailabilityItemUiModel) -> Unit
) : BaseExpandableListAdapter() {
    override fun getGroup(groupPosition: Int): String {
        return listDataHeader[groupPosition].date
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            val inflater = this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.item_check_availability_date, null)
        }
        view?.findViewById<TextView>(R.id.tv_date)?.text = getGroup(groupPosition)

        if (isExpanded) {
            view?.findViewById<ImageView>(R.id.group_indicator)?.setImageResource(R.drawable.action_drop_up)
            view?.findViewById<LinearLayout>(R.id.layout_yes)?.visibility = View.VISIBLE
            view?.findViewById<LinearLayout>(R.id.layout_no)?.visibility = View.VISIBLE
            view?.findViewById<LinearLayout>(R.id.layout_not_available)?.visibility = View.VISIBLE
            view?.invalidate()
        } else {
            view?.findViewById<ImageView>(R.id.group_indicator)?.setImageResource(R.drawable.action_drop_down)
            view?.findViewById<LinearLayout>(R.id.layout_yes)?.visibility = View.INVISIBLE
            view?.findViewById<LinearLayout>(R.id.layout_no)?.visibility = View.INVISIBLE
            view?.findViewById<LinearLayout>(R.id.layout_not_available)?.visibility = View.INVISIBLE
            view?.invalidate()
        }

        // Remove to see dropdown icon
        view?.findViewById<ImageView>(R.id.group_indicator)?.visibility = View.INVISIBLE

        return view!!
    }


    override fun getChildrenCount(groupPosition: Int): Int {
        return listChildData[listDataHeader[groupPosition]]?.size ?: 0
    }

    override fun getChild(groupPosition: Int, childPosition: Int): CheckAvailabilityItemUiModel? {
        return listChildData.get(listDataHeader[groupPosition])?.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            val inflater = this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.item_check_availablity, null)
        }
        val item = getChild(groupPosition, childPosition)
        item?.apply {
            when (status) {
                CheckAvailabilityStatus.NO.value -> {
                    view?.findViewById<ImageView>(R.id.image_status)?.setImageResource(R.drawable.check_cross_red)
                }
                CheckAvailabilityStatus.YES.value -> {
                    view?.findViewById<ImageView>(R.id.image_status)?.setImageResource(R.drawable.check_tick_green)
                }
                CheckAvailabilityStatus.NOT_AVAILABLE.value -> {
                    view?.findViewById<ImageView>(R.id.image_status)?.setImageResource(R.drawable.check_minus_black)
                }
                null -> {
                    view?.findViewById<ImageView>(R.id.image_status)?.setImageResource(R.drawable.check_empty_black)
                }
            }

            view?.findViewById<TextView>(R.id.tv_time)?.text = time


            view?.setOnClickListener {
                onClick.invoke(listDataHeader[groupPosition], this)
            }
        }
        return view!!
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return listDataHeader.size
    }

    fun updateList(map: LinkedHashMap<EventDaysUiModel, List<CheckAvailabilityItemUiModel>>) {
        this.listChildData = map
        this.listDataHeader = map.keys.toList()
        notifyDataSetChanged()
    }
}