package com.khojaleadership.KLF.Adapter.Event;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Model.Event_Model.GetEventsListDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Event_Dialoge_Adapter extends RecyclerView.Adapter<Event_Dialoge_Adapter.MyViewHolder> {

    Context context;
    ArrayList<GetEventsListDataModel> arrayList;

    public Event_Dialoge_Adapter(Context context, ArrayList<GetEventsListDataModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_custom_dialog_recyclerview_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try{

            GetEventsListDataModel events = arrayList.get(position);
            String[] split=events.getStart_date().split(" ");

            holder.date.setText(split[0]);
            holder.time.setText(split[1]);

            holder.name.setText(events.getFirst_name()+" "+events.getLast_name()+","+events.getName());
            holder.location.setText(events.getCity()+","+events.getCountry());

            holder.webView.loadData(events.getDescription(), "text/html; charset=utf-8", "UTF-8");
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,date, time, location;
        WebView webView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.tv_userName);
            date = itemView.findViewById(R.id.spinner_date);
            location = itemView.findViewById(R.id.tv_location);
            time = itemView.findViewById(R.id.tv_time);

            webView = itemView.findViewById(R.id.webview);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

        }
    }
}
