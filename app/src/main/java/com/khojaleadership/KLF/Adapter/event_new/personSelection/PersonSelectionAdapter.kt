package com.khojaleadership.KLF.Adapter.event_new.personSelection

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemPersonSelectionBinding

class PersonSelectionAdapter(
        private val context: Context,
        private val onPersonClick: (PersonDetailUiModel) -> Unit
) : ListAdapter<PersonDetailUiModel, PersonSelectionAdapter.ViewHolder>(PersonSelectionDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return PersonSelectionViewHolder(
                ItemPersonSelectionBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ),
                context,
                onPersonClick
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: PersonDetailUiModel)
    }

    class PersonSelectionViewHolder(
            private val binding: ItemPersonSelectionBinding,
            private val context: Context,
            private val onPersonClick: (PersonDetailUiModel) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: PersonDetailUiModel) {

            item.apply {
                binding.tvDesignation.text = designation
                binding.tvName.text = name
                binding.root.setOnClickListener {
                    onPersonClick.invoke(this)
                }
                if(isSelected){
                    binding.root.setBackgroundColor(ContextCompat.getColor(context, R.color.highlightColor))
                }else{
                    binding.root.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
                }
            }

        }
    }
}

class PersonSelectionDiffUtil : DiffUtil.ItemCallback<PersonDetailUiModel>() {
    override fun areItemsTheSame(
            oldItem: PersonDetailUiModel,
            newItem: PersonDetailUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: PersonDetailUiModel,
            newItem: PersonDetailUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
