package com.khojaleadership.KLF.Adapter.Intiative;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Data_Projects_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class IntiativeProject_Adapter extends RecyclerView.Adapter<IntiativeProject_Adapter.MyViewHolder> {

    Context context;
    String project_ammount;
    ArrayList<Intiative_Detail_Data_Projects_Model> arrayList;

    public IntiativeProject_Adapter(Context context, String project_ammount, ArrayList<Intiative_Detail_Data_Projects_Model> arrayList) {
        this.context = context;
        this.project_ammount = project_ammount;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.intiative_project_recyclerview_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        try {
            Intiative_Detail_Data_Projects_Model proj_model = arrayList.get(position);

//            holder.proj_name.setText(proj_model.getProjects__name());
//            holder.proj_percentage.setText(proj_model.getInitiativesProjects__percentage());
//            holder.proj_ammount.setText(project_ammount+" USD");   //basic info me se value leni....shaheer
//            holder.proj_charity.setText("Still pending?");


            holder.proj_name.setText(proj_model.getProjects__name());
            holder.proj_percentage.setText(proj_model.getInitiativesProjects__percentage() +" %");
//            holder.proj_ammount.setText("USD "+ project_ammount);   //basic info me se value leni....shaheer
//            holder.proj_charity.setText("Still pending?");
            holder.proj_ammount.setText("USD "+ proj_model.getProjectFundraisings__amount());

            holder.proj_charity.setText(Html.fromHtml(proj_model.getGroups_details()));

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView proj_name, proj_percentage, proj_ammount, proj_charity;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            //project
            proj_name = (TextView) itemView.findViewById(R.id.proj_name);
            proj_percentage = (TextView) itemView.findViewById(R.id.proj_percentage);
            proj_ammount = (TextView) itemView.findViewById(R.id.proj_amount);
            proj_charity = (TextView) itemView.findViewById(R.id.proj_charity);
        }
    }
}
