package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Charity_Project_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;


public class View_Profile_Charity_Adapter extends RecyclerView.Adapter<View_Profile_Charity_Adapter.Charity_viewHolder> {


    Context mctx;
    ArrayList<View_Profile_Charity_Project_Model> charity_models;
    RecyclerViewClickListner listner;

    public View_Profile_Charity_Adapter(Context mctx, ArrayList<View_Profile_Charity_Project_Model> charity_models, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.charity_models = charity_models;
        this.listner = listner;
    }

    @NonNull
    @Override
    public Charity_viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.aa_view_profile_charity_recyclerview_items,parent,false);


        return new Charity_viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Charity_viewHolder holder, int position) {

        try {
            final View_Profile_Charity_Project_Model charity_model = charity_models.get(position);


            holder.tv_title.setText("Title not availbe");
            holder.tv_name.setText(charity_model.getGroups__name());
             Glide.with(mctx).load(charity_model.getGroupimage()).into(holder.img_bussiness);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    @Override
    public int getItemCount() {
        return charity_models.size();
    }

    public class Charity_viewHolder extends RecyclerView.ViewHolder {


        ImageView img_bussiness;
        TextView tv_name;
        TextView tv_title;

        LinearLayout profile_charity_row;

        public Charity_viewHolder(@NonNull View itemView) {
            super(itemView);



            img_bussiness = itemView.findViewById(R.id.img_Charity);
            tv_name = itemView.findViewById(R.id.tv_name_Charity);
            tv_title= itemView.findViewById(R.id.tv_title_Charity);

            profile_charity_row = itemView.findViewById(R.id.profile_charity_row);
            profile_charity_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });


        }
    }
}
