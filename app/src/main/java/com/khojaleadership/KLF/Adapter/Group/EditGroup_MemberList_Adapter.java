package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class EditGroup_MemberList_Adapter extends RecyclerView.Adapter<EditGroup_MemberList_Adapter.MyviewHolderClass> {
    Context mctx;
   ArrayList<GetGroupMemberListDataModel> list;
   RecyclerViewClickListner listner;

    public EditGroup_MemberList_Adapter(Context mctx, ArrayList<GetGroupMemberListDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.group_members_list_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {
        try{

            GetGroupMemberListDataModel item = list.get(i);
            myviewHolderClass.name.setText(item.getFirst_name() + " " + item.getLast_name());
            myviewHolderClass.name.setPaintFlags(myviewHolderClass.name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            String id=list.get(i).getId();
            //.........................now check..........................


            if (Common.is_Admin_flag==true){
                myviewHolderClass.delete_member.setVisibility(View.VISIBLE);
            }
            Glide.with(mctx.getApplicationContext()).load(list.get(i).getProfile_image()).into(myviewHolderClass.group_member_img);
            final String finalId = id;
            myviewHolderClass.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Common.master_search_view_profile=true;
//                    Common.view_profile_id= finalId;

                    Intent i=new Intent(mctx, View_Profile_Activity_Updated.class);
                    i.putExtra("view_profile_id",finalId);
                    i.putExtra("isMasterSearch","1");
                    //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mctx.startActivity(i);
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {
        TextView name;
        ImageView delete_member;
        ImageView group_member_img;
        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            name=(TextView)itemView.findViewById(R.id.group_list_member_name);
            group_member_img=(ImageView)itemView.findViewById(R.id.group_member_img);
            delete_member=(ImageView)itemView.findViewById(R.id.remove_member);
            delete_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

        }
    }
}

