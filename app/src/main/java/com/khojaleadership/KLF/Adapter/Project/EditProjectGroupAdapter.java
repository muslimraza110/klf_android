package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectDataModel;
import com.khojaleadership.KLF.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProjectGroupAdapter extends RecyclerView.Adapter<EditProjectGroupAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetAlreadyGroup_ProjectDataModel> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    public EditProjectGroupAdapter(Context context, List<GetAlreadyGroup_ProjectDataModel> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.edit_project_group_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description= (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description= (TextView) view.findViewById(R.id.wlc_sub_description);
        viewHolder.group_img=view.findViewById(R.id.group_img);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        try{
            //Boolean already_checked=false;
            int section = getSectionForPosition(position);

            /// / If the current position is equal to the position of the Char of
            // the first letter of the classification, it is considered to be the first time
            if (position == getPositionForSection(section)) {
                holder.tag.setVisibility(View.VISIBLE);
                holder.tag.setText(mData.get(position).getLetters());
            } else {
                holder.tag.setVisibility(View.GONE);
            }

            holder.name.setText(this.mData.get(position).getName());

            holder.add_group.setChecked(false);
            if (mData.get(position).getIsSelected().equals("1")){
                holder.add_group.setChecked(true);
            }

            Glide.with(mContext).load(mData.get(position).getProfileimg()).into(holder.group_img);


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }


    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<GetAlreadyGroup_ProjectDataModel> list){
        this.mData = list;
        notifyDataSetChanged();
    }

    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tag,name, description,sub_description;
        CheckBox add_group;

        CircleImageView group_img;

        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            add_group=(CheckBox)itemView.findViewById(R.id.group_checkbox);


            add_group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = add_group.isChecked();
                    // Do something here.
                    if (listner != null) {
                         listner.onViewClcik(getAdapterPosition(), view);

                    }
                }
            });




        }
    }

}
