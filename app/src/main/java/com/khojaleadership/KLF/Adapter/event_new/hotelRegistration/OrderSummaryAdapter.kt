package com.khojaleadership.KLF.Adapter.event_new.hotelRegistration

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.hotelRegistration.OrderSummaryUiModel
import com.khojaleadership.KLF.databinding.ItemOrderSummaryBinding

class OrderSummaryAdapter : ListAdapter<OrderSummaryUiModel, OrderSummaryAdapter.ViewHolder>(OrderSummaryDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return HotelPackagesViewHolder(
                ItemOrderSummaryBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: OrderSummaryUiModel)
    }

    class HotelPackagesViewHolder(val binding: ItemOrderSummaryBinding) : ViewHolder(binding.root) {
        override fun bind(item: OrderSummaryUiModel) {
        }
    }


}

class OrderSummaryDiffUtil : DiffUtil.ItemCallback<OrderSummaryUiModel>() {
    override fun areItemsTheSame(
            oldItem: OrderSummaryUiModel,
            newItem: OrderSummaryUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: OrderSummaryUiModel,
            newItem: OrderSummaryUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
