package com.khojaleadership.KLF.Adapter.Resource;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Resource.Khoja_Summit_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Khoja_Summit_Adapter extends RecyclerView.Adapter<Khoja_Summit_Adapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<Khoja_Summit_Data_Model> list;
   RecyclerViewClickListner listner;

    public Khoja_Summit_Adapter(Context mctx, ArrayList<Khoja_Summit_Data_Model> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        //View listitem = layoutInflater.inflate(R.layout.khoja_summit_recyclerview_items,viewGroup,false);

        View listitem = layoutInflater.inflate(R.layout.khoja_summit_recyclerview_item_updated,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {

        try {
            Khoja_Summit_Data_Model item=list.get(i);
            myviewHolderClass.title.setText(item.getPosts__name());
            myviewHolderClass.date.setText(item.getPosts__created());

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    public void updateList(ArrayList<Khoja_Summit_Data_Model> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView title,date;
        //RelativeLayout edit_button,delete_button;
        LinearLayout complete_row;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            title= (TextView) itemView.findViewById(R.id.sumit_title);
            date= (TextView) itemView.findViewById(R.id.sumit_date);

            complete_row = (LinearLayout) itemView.findViewById(R.id.complete_row);
            complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });

        }
    }
}

