package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.TopicModels.GetTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Topic_Adapter extends RecyclerView.Adapter<Topic_Adapter.MyviewHolder> {

    Context mctx;
    RecyclerViewClickListner listner;

    //private ArrayList<Admin_Forum_Model> list;
    private ArrayList<GetTopicDataModel> list;

    public Topic_Adapter(Context mctx, RecyclerViewClickListner listner, ArrayList<GetTopicDataModel> list) {
        this.mctx = mctx;
        this.listner = listner;
        this.list = list;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(mctx);
        View listitem = layoutInflater.inflate(R.layout.admin_forum_recyclerview_item, viewGroup, false);
        MyviewHolder myviewHolder = new MyviewHolder(listitem);
        return myviewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder myviewHolder, int i) {



        try{

            final GetTopicDataModel model = list.get(i);

            myviewHolder.tv_forum.setText(model.getSubforums__title());
             if (Common.is_Admin_flag==false){
                myviewHolder.delete_topic_btn.setVisibility(View.GONE);
            }else {
                //myviewHolder.delete_topic_btn.setVisibility(View.VISIBLE);
            }

            if (Common.login_data.getData().getId()==list.get(i).getSubforums__user_id() ||
                    Common.login_data.getData().getId().equals(list.get(i).getSubforums__user_id())){
                myviewHolder.delete_topic_btn.setVisibility(View.VISIBLE);
                Log.d("AddRemove", "visible id :"+i);
            }


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(ArrayList<GetTopicDataModel> new_list){
        list=new_list;
        notifyDataSetChanged();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        TextView tv_forum;
        ImageView img_add;
        CardView linearLayout;

        LinearLayout delete_topic_btn;


        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            tv_forum = (TextView) itemView.findViewById(R.id.tv_formText);

            img_add = (ImageView) itemView.findViewById(R.id.topic_delete);



            //linearLayout = (CardView) itemView.findViewById(R.id.linearLayout);

            delete_topic_btn=(LinearLayout)itemView.findViewById(R.id.delete_topic_btn);
            delete_topic_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });


        }
    }
}
