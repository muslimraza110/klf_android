package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.EditGroup_PendingAccessRequestsData_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class EditGroup_PendingAccessRequests_Adapter extends RecyclerView.Adapter<EditGroup_PendingAccessRequests_Adapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<EditGroup_PendingAccessRequestsData_Model> list;
   RecyclerViewClickListner listner;

    public EditGroup_PendingAccessRequests_Adapter(Context mctx, ArrayList<EditGroup_PendingAccessRequestsData_Model> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
       // View listitem = layoutInflater.inflate(R.layout.editgroup_pendingrequests_recyclerview_items,viewGroup,false); aa_group_project_pending_access_request_recyclerview_items
        View listitem = layoutInflater.inflate(R.layout.editgroup_pendingrequests_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {


        try{
            EditGroup_PendingAccessRequestsData_Model item=list.get(i);
            myviewHolderClass.name.setText(item.getUsers__first_name()+" "+item.getUsers__last_name());

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView name;
        TextView accept_member,delete_member;
        CardView cardView;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            name= (TextView) itemView.findViewById(R.id.join_group_member_name);

            accept_member=itemView.findViewById(R.id.accept_member);
            delete_member=itemView.findViewById(R.id.reject_member);

            accept_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

            delete_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });


        }
    }
}

