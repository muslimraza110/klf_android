package com.khojaleadership.KLF.Adapter.event_new.eventSections

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.Activities.event_new.eventSections.EventSectionTag
import com.khojaleadership.KLF.Activities.event_new.eventSections.EventSectionUIModel
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemEventSectionBinding


class EventSectionsAdapter(
        var eventSectionList: List<EventSectionUIModel>,
        private val context: Context,
        private val onClick: ((String) -> Unit)
) : RecyclerView.Adapter<EventSectionsAdapter.EventSectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventSectionViewHolder {
        return EventSectionViewHolder(
                ItemEventSectionBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ),
                context,
                onClick
        )
    }

    override fun getItemCount(): Int {
        return eventSectionList.size
    }

    override fun onBindViewHolder(holder: EventSectionViewHolder, position: Int) {
        holder.bind(eventSectionList[position])
    }

    class EventSectionViewHolder(
            private val binding: ItemEventSectionBinding,
            private val context: Context,
            private val onClick: ((String) -> Unit)
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: EventSectionUIModel) {
            item.apply {
                binding.tvName.text = name
                binding.tvDescription.text = description
                if (tag == "initiative"){
                    binding.tvDescription.text = ""
                }
                loadAppropriateImage(binding.backgroundImage, tag)
                binding.root.setOnClickListener {
                    onClick.invoke(tag)
                }

            }
        }

        private fun loadAppropriateImage(imageView: ImageView, tag: String) {
            when (tag) {
                EventSectionTag.SPEAKERS.value -> {
                    Glide.with(context)
                            .load(R.drawable.time_100_summit)
                            .centerCrop()
                            .into(imageView)
                }
                EventSectionTag.DELEGATES.value -> {
                    Glide.with(context)
                            .load(R.drawable.solarplaza)
                            .centerCrop()
                            .into(imageView)
                }
                EventSectionTag.PLANNER.value -> {
                    Glide.with(context)
                            .load(R.drawable.business_meeting)
                            .centerCrop()
                            .into(imageView)
                }
                EventSectionTag.PROGRAMME.value -> {
                    Glide.with(context)
                            .load(R.drawable.nsi_summit_2019)
                            .centerCrop()
                            .into(imageView)
                }
                EventSectionTag.MEETING_DETAILS.value -> {
                    Glide.with(context)
                            .load(R.drawable.nsi_summit_2019)
                            .centerCrop()
                            .into(imageView)
                }
                EventSectionTag.INTIATIVE_DETAILS.value -> {
                    Glide.with(context)
                        .load(R.drawable.solarplaza)
                        .centerCrop()
                        .into(imageView)
                }

            }

        }
    }
}