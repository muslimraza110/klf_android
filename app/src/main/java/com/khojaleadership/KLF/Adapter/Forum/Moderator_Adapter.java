package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetModeratorDataModel;
import com.khojaleadership.KLF.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Moderator_Adapter extends RecyclerView.Adapter<Moderator_Adapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetModeratorDataModel> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    public Moderator_Adapter(Context context, List<GetModeratorDataModel> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = mInflater.inflate(R.layout.add_moderator_recyclerview_items, parent,false);
        View view = mInflater.inflate(R.layout.whitelist_contact_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description= (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description= (TextView) view.findViewById(R.id.wlc_sub_description);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        try {
            int section = getSectionForPosition(position);



            holder.name.setText("");
            holder.description.setText(this.mData.get(position).getEmail());
            holder.sub_description.setText("");


            if (mData.get(position).getIs_moderator()==1 ){
                holder.add_moderator.setChecked(true);
            }else if (mData.get(position).getIs_moderator()==0){
                holder.add_moderator.setChecked(false);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }


    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<GetModeratorDataModel> list){
        this.mData = list;
        notifyDataSetChanged();
    }

    /**
     * Get the Char ASCII value of the first letter of
     * the classification according to the current position of the ListView
     */
    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    /**
     * Get the position of the first occurrence of the first letter based on
     * the Char ASCII value of the first letter of the classification
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tag,name, description,sub_description;
        CheckBox add_moderator;
        CircleImageView imageView;

        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            imageView=itemView.findViewById(R.id.ic_user_image_icon);
            add_moderator=(CheckBox)itemView.findViewById(R.id.whitelist_contact_checkbox);

            add_moderator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });



        }
    }

}
