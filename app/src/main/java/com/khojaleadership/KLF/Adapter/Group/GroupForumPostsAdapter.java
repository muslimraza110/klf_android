package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailForumPostsDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class GroupForumPostsAdapter extends RecyclerView.Adapter<GroupForumPostsAdapter.viewHolder>{

    Context mctx;
    ArrayList<GroupDetailForumPostsDataModel> list;
    RecyclerViewClickListner listner;

    public GroupForumPostsAdapter(Context mctx, ArrayList<GroupDetailForumPostsDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.group_forum_posts_recyclerview_items, viewGroup, false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {

        try{
            GroupDetailForumPostsDataModel item=list.get(i);
            viewHolder.name.setText(item.getForumPosts__topic());

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.group_forum_post_name);
        }
    }
}
