package com.khojaleadership.KLF.Adapter.KhojaCare;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Model.Khoja_Care_Models.Khoja_care_parent_model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;


public class Khoja_care_expandable_adapter implements ExpandableListAdapter {



    private Context context;
    private List<Khoja_care_parent_model> parent_model = new ArrayList<Khoja_care_parent_model>();

    public Khoja_care_expandable_adapter(Context context, List<Khoja_care_parent_model> parent_model) {
        this.context = context;
        this.parent_model = parent_model;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return parent_model.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parent_model.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return parent_model.get(groupPosition).getModel_child().get(childPosition);
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        try {
            Parent_holder parentHolder =null;

            Khoja_care_parent_model expandableModelParent = (Khoja_care_parent_model) getGroup(groupPosition);

            if (convertView == null){

                LayoutInflater inflater =(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.khoja_care_item_parent,null,false);
                convertView.setVerticalScrollBarEnabled(true);

                parentHolder = new Parent_holder();
                convertView.setTag(parentHolder);
            }
            else {
                parentHolder = (Parent_holder) convertView.getTag();

            }

            parentHolder.parent_group_text = (TextView) convertView.findViewById(R.id.text_group);
            parentHolder.parent_group_text.setText(expandableModelParent.Title);


            parentHolder.indicator = (ImageView) convertView.findViewById(R.id.image_indicator);

            if (isExpanded){
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_up);
            }
            else {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_down);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {
            Child_holder child_holder= null;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home_child_container_recyclerview,null,false);
                child_holder = new Child_holder();
                convertView.setTag(child_holder);
            } else {
                child_holder = (Child_holder) convertView.getTag();
            }
            child_holder.child_recyclerview = (RecyclerView) convertView.findViewById(R.id.recyclerview_Container);
            LinearLayoutManager layoutManager =new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
            child_holder.child_recyclerview.setLayoutManager(layoutManager);

             Khoja_care_chid_adapter chid_adapter = new Khoja_care_chid_adapter(context, parent_model.get(groupPosition).getModel_child());

            child_holder.child_recyclerview.setAdapter(chid_adapter);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    public static class Parent_holder{
        TextView parent_group_text;
        ImageView indicator, title_image;
    }

    public static class Child_holder{
        static RecyclerView child_recyclerview;
    }

}
