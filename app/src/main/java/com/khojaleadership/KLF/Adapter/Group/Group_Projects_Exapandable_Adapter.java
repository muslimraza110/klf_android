package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Activities.Project.View_Projects_Detail_Updated;
import com.khojaleadership.KLF.Model.Group_Models.Group_GroupProjectParentModel;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;

import java.util.ArrayList;
import java.util.List;


public class Group_Projects_Exapandable_Adapter implements ExpandableListAdapter {


    private Context context;
    private List<Group_GroupProjectParentModel> parent_model = new ArrayList<>();

    public Group_Projects_Exapandable_Adapter(Context context, List<Group_GroupProjectParentModel> parent_model) {
        this.context = context;
        this.parent_model = parent_model;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return parent_model.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return parent_model.get(groupPosition).getChild_data().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parent_model.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_model.get(groupPosition).getChild_data().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {

        try {
            Parent_holder parentHolder = null;

            final Group_GroupProjectParentModel parent_item = parent_model.get(groupPosition);

            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.group_project_expandable_parent_view, parent, false);
                convertView.setVerticalScrollBarEnabled(true);

                parentHolder = new Parent_holder();
                convertView.setTag(parentHolder);
            } else {
                parentHolder = (Parent_holder) convertView.getTag();

            }

            parentHolder.parent_group_text = (TextView) convertView.findViewById(R.id.name);
            parentHolder.parent_group_text.setText(parent_item.getProject_name());

            parentHolder.indicator = (ImageView) convertView.findViewById(R.id.image_indicator);

            parentHolder.text_layout=convertView.findViewById(R.id.layout_text);
            parentHolder.indicator_layout=convertView.findViewById(R.id.layout_dropdown);

            if (isExpanded) {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_up);
            } else {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_down);
            }

            parentHolder.text_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Common.is_project_detail=false;
//                    Common.project_id_for_detail=parent_item.getProject_id();
                    Intent view_event = new Intent(context, View_Projects_Detail_Updated.class);
                    view_event.putExtra("ProjectId",parent_item.getProject_id());
                    view_event.putExtra("isProjectDetail","0");
                    context.startActivity(view_event);
                }
            });

            parentHolder.indicator_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isExpanded) ((ExpandableListView) parent).collapseGroup(groupPosition);
                    else ((ExpandableListView) parent).expandGroup(groupPosition, true);
                }
            });

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        try {
            if (parent_model.get(groupPosition).getChild_data().get(childPosition) != null) {

                String title = parent_model.get(groupPosition).getChild_data().get(childPosition).getName();

                if (convertView == null) {
                    convertView = LayoutInflater.from(context).inflate(R.layout.group_detail_project_member_recyclerview_items, parent, false);
                }

                final TextView textTitle = (TextView) convertView.findViewById(R.id.group_detail_project_member_name);
                textTitle.setText(title);

                textTitle.setPaintFlags(textTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                ImageView group_detail_project_member_img=convertView.findViewById(R.id.group_detail_project_member_img);
                String img=parent_model.get(groupPosition).getChild_data().get(childPosition).getImg();
                Glide.with(context).load(img).into(group_detail_project_member_img);

                textTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Common.master_search_view_profile=true;
//                        Common.view_profile_id=parent_model.get(groupPosition).getChild_data().get(childPosition).getId();

                        Intent intent=new Intent(context, View_Profile_Activity_Updated.class);
                        intent.putExtra("view_profile_id",parent_model.get(groupPosition).getChild_data().get(childPosition).getId());
                        intent.putExtra("isMasterSearch","1");
                        context.startActivity(intent);
                    }
                });


            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return groupId;
    }


    public static class Parent_holder {
        TextView parent_group_text;
        ImageView indicator, title_image;
        LinearLayout text_layout,indicator_layout;
    }

}
