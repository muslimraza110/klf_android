package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDetailUsersDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class View_Projects_members_Adapter extends RecyclerView.Adapter<View_Projects_members_Adapter.viewHolder>{
    Context mctx;
    ArrayList<ProjectsDetailUsersDataModel> list;
    RecyclerViewClickListner listner;


    public View_Projects_members_Adapter(Context mctx, ArrayList<ProjectsDetailUsersDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        //View view = inflater.inflate(R.layout.view_projects_members_recyclerview_items,viewGroup,false);
        View view = inflater.inflate(R.layout.v_p_member_recycl_item,viewGroup,false);

        return new viewHolder(view,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        try {
            ProjectsDetailUsersDataModel item=list.get(i);
            if (TextUtils.isEmpty(item.getUsers__first_name()) && TextUtils.isEmpty(item.getUsers__last_name())){
                viewHolder.member_name.setText("");
            }else if (TextUtils.isEmpty(item.getUsers__last_name())){
                viewHolder.member_name.setText(item.getUsers__first_name());
            }else if (TextUtils.isEmpty(item.getUsers__first_name())){
                viewHolder.member_name.setText(item.getUsers__last_name());
            }else {
                viewHolder.member_name.setText(item.getUsers__title()+" "+item.getUsers__first_name()+" "+item.getUsers__last_name());
            }

            viewHolder.member_name.setPaintFlags(viewHolder.member_name.getPaintFlags() |Paint.UNDERLINE_TEXT_FLAG);

            Glide.with(mctx).load(item.getProfileImg__name()).into(viewHolder.member_image);

            if (Common.is_Admin_flag==false){
                viewHolder.remove_member.setVisibility(View.GONE);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        TextView member_name;

        ImageView member_image;
        ImageView remove_member;

        public viewHolder(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            member_name= itemView.findViewById(R.id.project_member_name);
            member_image  = itemView.findViewById(R.id.project_member_img);
            remove_member= itemView.findViewById(R.id.remove_member);

            remove_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

            member_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });

        }
    }
}
