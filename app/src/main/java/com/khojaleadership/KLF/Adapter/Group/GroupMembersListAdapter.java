package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailMembersListDataModel;
import com.khojaleadership.KLF.R;
import com.khojaleadership.KLF.Activities.Setting.View_Profile_Activity_Updated;

import java.util.ArrayList;

public class GroupMembersListAdapter extends RecyclerView.Adapter<GroupMembersListAdapter.viewHolder>{

    Context mctx;
    ArrayList<GroupDetailMembersListDataModel> list;
    RecyclerViewClickListner listner;

    public GroupMembersListAdapter(Context mctx, ArrayList<GroupDetailMembersListDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.group_members_list_recyclerview_items, viewGroup, false);

        return new viewHolder(view,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {


        try{

            GroupDetailMembersListDataModel item = list.get(i);

            viewHolder.name.setText(item.getUsers__first_name() + " " + item.getUsers__last_name());
            String id = "";
            viewHolder.name.setPaintFlags(viewHolder.name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            id=list.get(i).getUsers__id();
            //.........................now check..........................
            if (id!=null) {
                if (id != Common.login_data.getData().getId()) {
                    viewHolder.delete_member.setVisibility(View.GONE);
                }
            }

            if (Common.is_Admin_flag==true){
                viewHolder.delete_member.setVisibility(View.VISIBLE);
            }


            Glide.with(mctx).load(list.get(i).getProfileImg__name()).into(viewHolder.group_member_img);



            final String finalId = id;
            viewHolder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                     Common.master_search_view_profile=true;
//                    Common.view_profile_id= finalId;

                    Intent i=new Intent(mctx, View_Profile_Activity_Updated.class);
                    i.putExtra("view_profile_id",finalId);
                    i.putExtra("isMasterSearch","1");
                    //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mctx.startActivity(i);
                }
            });

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView delete_member;
        ImageView group_member_img;
        public viewHolder(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.group_list_member_name);
            group_member_img=(ImageView)itemView.findViewById(R.id.group_member_img);
            delete_member=(ImageView)itemView.findViewById(R.id.remove_member);
            delete_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });
        }
    }
}
