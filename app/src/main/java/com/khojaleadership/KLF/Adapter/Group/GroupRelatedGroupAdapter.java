package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailRelatedGroupDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class GroupRelatedGroupAdapter extends RecyclerView.Adapter<GroupRelatedGroupAdapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<GroupDetailRelatedGroupDataModel> list;
   RecyclerViewClickListner listner;

    public GroupRelatedGroupAdapter(Context mctx, ArrayList<GroupDetailRelatedGroupDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.group_relatedgroup_recyclerview_item,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {


        try{
            GroupDetailRelatedGroupDataModel item=list.get(i);
            myviewHolderClass.related_group_name.setText(item.getName());
            myviewHolderClass.related_group_name.setPaintFlags(myviewHolderClass.related_group_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            myviewHolderClass.group_related_group_img.setBackground(null);
            Glide.with(mctx).load(item.getGroup_image()).into(myviewHolderClass.group_related_group_img);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView related_group_name;
        ImageView group_related_group_img;
        LinearLayout cardView;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            related_group_name= (TextView) itemView.findViewById(R.id.group_related_group_name);
            group_related_group_img= itemView.findViewById(R.id.group_related_group_img);

            cardView = itemView.findViewById(R.id.group_related_group_cardview);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

        }
    }
}

