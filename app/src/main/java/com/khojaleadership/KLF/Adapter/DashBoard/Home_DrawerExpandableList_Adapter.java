package com.khojaleadership.KLF.Adapter.DashBoard;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.ClickListner;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Parent_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Home_DrawerExpandableList_Adapter extends BaseExpandableListAdapter {

    private Context mctx;
    private ArrayList<Home_Parent_Model> list;
    private int[] img_array;
    ClickListner listner;


    public Home_DrawerExpandableList_Adapter(Context mctx, ArrayList<Home_Parent_Model> list, int[] img_array, ClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.img_array = img_array;
        this.listner = listner;
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return list.get(groupPosition).getChildList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {


        return list.get(groupPosition).getChildList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        try{


            String title = list.get(groupPosition).getParent_name();
            if (convertView == null) {
                convertView = LayoutInflater.from(mctx).inflate(R.layout.aa_dashboard_drawer_parent_items, parent,false);
            }
            TextView textTitle = (TextView) convertView.findViewById(R.id.listTitle);
            textTitle.setText(title);

            ImageView menu_icon = (ImageView) convertView.findViewById(R.id.menu_icon);
            menu_icon.setImageResource(img_array[groupPosition]);

            ImageView menu_down_up_icon = (ImageView) convertView.findViewById(R.id.menu_arrow_icon);
            final int size = list.get(groupPosition).getChildList().size();
             if (size > 0) {
                 if (isExpanded) {
                    menu_down_up_icon.setImageResource(R.drawable.drop_up_blue);
                } else {
                    menu_down_up_icon.setImageResource(R.drawable.dropdown_blue);
                }
            } else {
                menu_down_up_icon.setImageResource(0);
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        try{
            String title = list.get(groupPosition).getChildList().get(childPosition).getChild_name();

            if (convertView == null) {
                convertView = LayoutInflater.from(mctx).inflate(R.layout.aa_dashboard_drawer_child_items, null);
            }

            TextView textTitle = (TextView) convertView.findViewById(R.id.expandableListItem);
            textTitle.setText(title);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onRowClick(groupPosition, childPosition, v);

                    }
                }
            });


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
