package com.khojaleadership.KLF.Adapter.DashBoard;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.ClickListner;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Expandable_Parent_Model;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Expandable_Child_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;



public class Home_Expandable_Adapter implements ExpandableListAdapter{



    private Context context;
    private List<Home_Expandable_Parent_Model> parent_model = new ArrayList<Home_Expandable_Parent_Model>();
    //MainActivity mainActivity;
    ClickListner listner;

    public Home_Expandable_Adapter(Context context, List<Home_Expandable_Parent_Model> parent_model, ClickListner listner) {
        this.context = context;
        this.parent_model = parent_model;
        this.listner = listner;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return parent_model.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return parent_model.get(groupPosition).getModel_child().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parent_model.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_model.get(groupPosition).getModel_child().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        try {
            Parent_holder parentHolder =null;

            Home_Expandable_Parent_Model expandableModelParent = (Home_Expandable_Parent_Model) getGroup(groupPosition);

            if (convertView == null){

                LayoutInflater inflater =(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.aa_dashboard_expandable_parent_item,parent,false);
                convertView.setVerticalScrollBarEnabled(true);

                parentHolder = new Parent_holder();
                convertView.setTag(parentHolder);
            }
            else {
                parentHolder = (Parent_holder) convertView.getTag();

            }

            parentHolder.parent_group_text = (TextView) convertView.findViewById(R.id.text_group);
            parentHolder.parent_group_text.setText(expandableModelParent.Title);


            parentHolder.title_image = (ImageView) convertView.findViewById(R.id.title_image);
            parentHolder.title_image.setImageResource(parent_model.get(groupPosition).images);


            parentHolder.indicator = (ImageView) convertView.findViewById(R.id.image_indicator);

            if (isExpanded){
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_up);
            }
            else {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_down);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {

            if (parent_model.get(groupPosition).getModel_child().get(childPosition)!=null) {
                String title = parent_model.get(groupPosition).getModel_child().get(childPosition).getChild_Model();

                if (convertView == null) {
                    convertView = LayoutInflater.from(context).inflate(R.layout.aa_dashboard_expandable_child_view, parent,false);
                }

                final TextView textTitle = (TextView) convertView.findViewById(R.id.main_content_child_name);
                textTitle.setText(title);

                TextView main_cancel_request_layout = convertView.findViewById(R.id.Cancel_btn);

                main_cancel_request_layout.setVisibility(View.GONE);

                final Home_Expandable_Child_Model model=parent_model.get(groupPosition).getModel_child().get(childPosition);

                if (model.getIs_group_access_request() == true && model.getIs_project_access_request() == false) {
                    main_cancel_request_layout.setVisibility(View.VISIBLE);
                }else if (model.getIs_group_access_request() == false && model.getIs_project_access_request() == true) {
                    main_cancel_request_layout.setVisibility(View.VISIBLE);
                }

                //Forum join the disscussion k lieye..... 575757
                textTitle.setTypeface(textTitle.getTypeface(), Typeface.NORMAL);

                if (model.id!=null) {
                    if (model.id.equals("-1")) {
                         //textTitle.setTextColor(getResources().getColor(R.color.Church_Grey));
                        textTitle.setTextColor(context.getResources().getColor(R.color.black));
                        textTitle.setTypeface(textTitle.getTypeface(), Typeface.BOLD);
                    } else if (model.id.equals("-2")) {
                         textTitle.setTypeface(textTitle.getTypeface(), Typeface.BOLD);
                        textTitle.setTextColor(context.getResources().getColor(R.color.black));
                        //                textTitle.setTextColor(Color.parseColor("#000"));
                    } else if (model.id.equals("-3")) {
                         textTitle.setTypeface(textTitle.getTypeface(), Typeface.BOLD);
                        textTitle.setTextColor(context.getResources().getColor(R.color.black));
//                textTitle.setTextColor(Color.parseColor("#000"));
                    } else {

                        textTitle.setTextColor(context.getResources().getColor(R.color.pure_grey));
                        textTitle.setTypeface(textTitle.getTypeface(), Typeface.NORMAL);
                    }
                }

                textTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (model.getParent_number() == 5) {

                            if (listner != null) {
                                Common.Home_MainContent_data = parent_model.get(groupPosition).getModel_child().get(childPosition);
                                listner.onRowClick(groupPosition, childPosition, view);
                            }
                            //..................5 intiative...............
                        }else if (model.getParent_number() == 4) {

                            if (listner != null) {
                                Common.Home_MainContent_data = parent_model.get(groupPosition).getModel_child().get(childPosition);
                                listner.onRowClick(groupPosition, childPosition, view);
                            }

                            //..................4 forum subscription...............

                        }else if (model.getParent_number() == 3) {

                            if (model.id.equals("-1")) {
                            } else if (model.id.equals("-2")) {
                            } else if (model.id.equals("-3")) {
                            } else {
                                if (listner != null) {
                                    Common.Home_MainContent_data = parent_model.get(groupPosition).getModel_child().get(childPosition);
                                    listner.onRowClick(groupPosition, childPosition, view);
                                }
                            }
                        }
                    }


                });


                main_cancel_request_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (listner != null) {
                            Common.Home_MainContent_data=parent_model.get(groupPosition).getModel_child().get(childPosition);
                            listner.onRowClick(groupPosition, childPosition, v);
                        }
                    }
                });

            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return groupId;
    }


    public static class Parent_holder{
        TextView parent_group_text;
        ImageView indicator, title_image;
    }


}
