package com.khojaleadership.KLF.Adapter.Project;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;


public class EditProjectAdapter extends RecyclerView.Adapter<EditProjectAdapter.ViewHolder>{

    Context mctx;
    ArrayList<GetAlreadyGroup_ProjectDataModel> list;
    RecyclerViewClickListner listner;


    public EditProjectAdapter(Context mctx, ArrayList<GetAlreadyGroup_ProjectDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner=listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.edit_project_recyclerview_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem,listner);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try{
            final GetAlreadyGroup_ProjectDataModel myListData = list.get(position);
            holder.textView.setText(myListData .getName());
            Glide.with(mctx).load(myListData.getProfileimg()).into(holder.group_img);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
         }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;
        LinearLayout remove_group;

        ImageView group_img;
        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
            group_img=itemView.findViewById(R.id.group_img);

            remove_group=(LinearLayout)itemView.findViewById(R.id.remove_group_from_project);
            remove_group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }

                }
            });


        }
    }
}
