package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Model.Project_Models.Percentage_List_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Add_Percentage_Adapter extends RecyclerView.Adapter<Add_Percentage_Adapter.MyviewHolderClass> {


    Context mctx;
    ArrayList<Percentage_List_Data_Model> list;

    ArrayList<String> group_id_list=new ArrayList<>();
    ArrayList<String> percentage_value_list=new ArrayList<>();

    String group_id;
    String percentage_value;

    Boolean text_changed=false;

    public Add_Percentage_Adapter(Context mctx, ArrayList<Percentage_List_Data_Model> list) {
        this.mctx = mctx;
        this.list = list;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.percentage_recyclerview_items, viewGroup, false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {

        Percentage_List_Data_Model item = list.get(i);

        try {

            myviewHolderClass.name.setText(item.getGroups__name());
            //percentage always 0 show krwani
            myviewHolderClass.value.setText("0");
//            if (item.getProjectsGroups__percent()!=null) {
//                myviewHolderClass.value.setText("0");
//                String[] aa = item.getProjectsGroups__percent().split("\\.");
//                if (aa!=null) {
//                    if (aa.length>0) {
//                        myviewHolderClass.value.setText(aa[0]);
//                    }
//                }
//            }else {
//                myviewHolderClass.value.setText("0");
//            }


            group_id=item.getGroups__id();
            //saving in group list
            group_id_list.add(group_id);



            String s = myviewHolderClass.value.getText().toString();


            myviewHolderClass.value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int pos, int i1, int i2) {
                    text_changed=true;

                     if (charSequence.toString().equalsIgnoreCase("")){
                        percentage_value="0";
                    }else {
                        percentage_value = charSequence.toString();
                    }
                    percentage_value_list.set(i,percentage_value);
                    //percentage_value_list.add(percentage_value);

                    for (int z=0;z<percentage_value_list.size();z++) {
                     }

                    //setting when value changed
                    Common.percentage_value_list=percentage_value_list;

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            if (text_changed==false){
                percentage_value_list.add(s);
            }



            Common.percentage_group_id_list=group_id_list;
            Common.percentage_value_list=percentage_value_list;


            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView name;
        EditText value;

        public MyviewHolderClass(@NonNull View itemView) {
            super(itemView);


            name = (TextView) itemView.findViewById(R.id.percentage_name);
            value = (EditText) itemView.findViewById(R.id.percentage_value);


        }
    }
}

