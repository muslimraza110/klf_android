package com.khojaleadership.KLF.Adapter.KhojaCare;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Activities.Project.View_Projects_Detail_Updated;
import com.khojaleadership.KLF.Model.Khoja_Care_Models.Khoja_care_child_model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;


public class Khoja_care_chid_adapter extends RecyclerView.Adapter<Khoja_care_chid_adapter.Child_ViewHolder> {


    Context context;
    List<Khoja_care_child_model> list_child = new ArrayList<Khoja_care_child_model>();


    public Khoja_care_chid_adapter(Context context, List<Khoja_care_child_model> list_child) {
        this.context = context;
        this.list_child = list_child;
    }


    @NonNull
    @Override
    public Child_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view =inflater.inflate(R.layout.khoja_care_item_child,parent,false);

        Child_ViewHolder viewHolder = new Child_ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Child_ViewHolder holder, final int position) {

        try {
            holder.tv_name.setText(list_child.get(position).getProjects__name());
            holder.tv_status.setText("Approved");
            holder.tv_description.setText(list_child.get(position).getCharity());
            holder.tv_creator.setText(list_child.get(position).getCreators__first_name()+" "+list_child.get(position).getCreators__last_name());
            holder.tv_city.setText(list_child.get(position).getProjects__city()+","+list_child.get(position).getProjects__country());


            holder.relative_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Common.is_project_detail=false;
//                    Common.project_id_for_detail=list_child.get(position).getProjects__id();

                    Intent view_event=new Intent(context, View_Projects_Detail_Updated.class);
                    view_event.putExtra("ProjectId",list_child.get(position).getProjects__id());
                    view_event.putExtra("isProjectDetail","0");
                    context.startActivity(view_event);
                }
            });
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return list_child.size();
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class Child_ViewHolder extends RecyclerView.ViewHolder{

        TextView tv_name;
        TextView tv_status;
        TextView tv_description;
        TextView tv_creator;
        TextView tv_city;


        LinearLayout relative_layout;

        public Child_ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_Name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_creator = itemView.findViewById(R.id.tv_creator);
            tv_city = itemView.findViewById(R.id.tv_city);


            relative_layout = itemView.findViewById(R.id.relative_layout);
        }
    }
}