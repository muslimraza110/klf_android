package com.khojaleadership.KLF.Adapter.DashBoard;

import android.content.Context;
import android.database.DataSetObserver;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.khojaleadership.KLF.Model.DashBoard_Model.OfflineExpandableParentModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;

public class OfflineExpandableAdapter implements ExpandableListAdapter {

    private Context context;
    private List<OfflineExpandableParentModel> brands_model = new ArrayList<OfflineExpandableParentModel>();

    public OfflineExpandableAdapter(Context context, List<OfflineExpandableParentModel> brands_model) {
        this.context = context;
        this.brands_model = brands_model;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return brands_model.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return brands_model.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return brands_model.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        try {
            ParentHolder parentHolder = null;

            OfflineExpandableParentModel expandableModel = (OfflineExpandableParentModel) getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater userInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = userInflater.inflate(R.layout.mainscreen_item_parent, null);
                convertView.setVerticalScrollBarEnabled(true);

                parentHolder = new ParentHolder();
                convertView.setTag(parentHolder);

            } else {
                parentHolder = (ParentHolder) convertView.getTag();
            }


            parentHolder.brandName = (TextView) convertView.findViewById(R.id.text_brand);
            parentHolder.brandName.setText(expandableModel.TitleName);


            parentHolder.title_image = (ImageView) convertView.findViewById(R.id.title_image);

            parentHolder.title_image.setImageResource(brands_model.get(groupPosition).imageResource);

            parentHolder.indicator = (ImageView) convertView.findViewById(R.id.image_indicator);

            if (isExpanded) {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_up);

            } else {
                parentHolder.indicator.setImageResource(R.drawable.ic_drop_down);

            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        try {
            ChildHolder childHolder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.mainscreen_item_group_child, parent, false);
                childHolder = new ChildHolder();
                convertView.setTag(childHolder);
            } else {
                childHolder = (ChildHolder) convertView.getTag();
            }

            childHolder.VerticalRecyclerViews = (RecyclerView) convertView.findViewById(R.id.mobiles);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            childHolder.VerticalRecyclerViews.setLayoutManager(layoutManager);

            OfflineRecyclerviewAdapter VerticalItemListAdapter = new OfflineRecyclerviewAdapter(context, brands_model.get(groupPosition).mobiles);
            childHolder.VerticalRecyclerViews.setAdapter(VerticalItemListAdapter);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    private static class ChildHolder {
        static RecyclerView VerticalRecyclerViews;
    }

    private static class ParentHolder {
        TextView brandName;
        ImageView indicator, title_image;
    }
}

