package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListDataModel;
import com.khojaleadership.KLF.R;

import java.util.List;

public class AddMemberInGroupAdapter extends RecyclerView.Adapter<AddMemberInGroupAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetGroupMemberListDataModel> mData;
    private Context mContext;
    RecyclerViewClickListner listner;

    public AddMemberInGroupAdapter(Context context, List<GetGroupMemberListDataModel> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.add_member_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);
        viewHolder.tag = (TextView) view.findViewById(R.id.tag);
        viewHolder.name = (TextView) view.findViewById(R.id.wlc_name);
        viewHolder.description= (TextView) view.findViewById(R.id.wlc_description);
        viewHolder.sub_description= (TextView) view.findViewById(R.id.wlc_sub_description);
        viewHolder.user_image=view.findViewById(R.id.user_image);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        try{

             int section = getSectionForPosition(position);



            holder.name.setText(this.mData.get(position).getTitle()+" "+this.mData.get(position).getFirst_name()+" "+this.mData.get(position).getLast_name());
            holder.description.setText(this.mData.get(position).getEmail());
            holder.sub_description.setText(this.mData.get(position).getRole());


            Glide.with(mContext)
                    .load(mData.get(position).getProfile_image())
                    .into(holder.user_image);

            holder.add_whitelist_contact.setChecked(false);
            if (mData.get(position).getIsmember()==1) {
                Log.d("ADDMember","user already member "+mData.get(position).getFirst_name());
                holder.add_whitelist_contact.setChecked(true);
            }



            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }


    }


    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<GetGroupMemberListDataModel> list){
        this.mData = list;
        notifyDataSetChanged();
    }

    /**
     * Get the Char ASCII value of the first letter of
     * the classification according to the current position of the ListView
     */
    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    /**
     * Get the position of the first occurrence of the first letter based on
     * the Char ASCII value of the first letter of the classification
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tag,name, description,sub_description;
        CheckBox add_whitelist_contact;

        ImageView user_image;
        public ViewHolder(View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            add_whitelist_contact=(CheckBox)itemView.findViewById(R.id.whitelist_contact_checkbox);
            add_whitelist_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = add_whitelist_contact.isChecked();
                    // Do something here.
                    if (listner != null) {
                         listner.onViewClcik(getAdapterPosition(), view);

                    }
                }
            });




        }
    }

}
