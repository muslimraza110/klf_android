package com.khojaleadership.KLF.Adapter.Intiative;

import android.animation.ObjectAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.ActiveFragmentDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Intiative_RecyclerView_Adapter extends RecyclerView.Adapter<Intiative_RecyclerView_Adapter.RecyclerView_ViewHolder> {
    ArrayList<ActiveFragmentDataModel> api_data;
    RecyclerViewClickListner listner;

    public Intiative_RecyclerView_Adapter(ArrayList<ActiveFragmentDataModel> api_data, RecyclerViewClickListner listner) {
        this.api_data = api_data;
        this.listner = listner;
    }

    @NonNull
    @Override
    public RecyclerView_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.updated_initiatives_reyclerview_items, parent, false);

        return new RecyclerView_ViewHolder(view, listner);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView_ViewHolder holder, int position) {


        try{

            ActiveFragmentDataModel item = api_data.get(position);
             holder.tv_title.setText(item.getInitiatives__name());
            holder.tv_amount.setText("USD "+item.getAmount());

            if(Common.is_Admin_flag == true){
                holder.tv_admin.setText(item.getUsers__first_name()+" "+item.getUsers__last_name());
            }


            Double progress_value=0.0;
            if (Double.valueOf(item.getFundraisings__amount())==0){

            }else {
                progress_value = (Double.valueOf(item.getAmount_collected()) / Double.valueOf(item.getFundraisings__amount()))*100;
            }

            int value= Integer.valueOf(progress_value.intValue());

            ObjectAnimator.ofInt(holder.progressBar, "progress", value)
                    .setDuration(3500)
                    .start();
            //holder.progressBar.setProgress(value);
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }



    }

    @Override
    public int getItemCount() {
        return api_data.size();
    }

    public class RecyclerView_ViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressBar;

        TextView tv_title, tv_admin, tv_amount;
        LinearLayout intiative_row;

        public RecyclerView_ViewHolder(@NonNull View itemView, final RecyclerViewClickListner listner) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_admin = itemView.findViewById(R.id.tv_admin);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            progressBar = itemView.findViewById(R.id.progressBar);


            intiative_row = (LinearLayout) itemView.findViewById(R.id.intiative_row);
            intiative_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner != null) {
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });


        }
    }
}
