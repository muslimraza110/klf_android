package com.khojaleadership.KLF.Adapter.event_new.programme

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.SpannedString
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Activities.event_new.programme.ProgrammeDetailsItemUiModel
import com.khojaleadership.KLF.Helper.MyClickableSpan
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemProgrammeBinding


class EventProgrammeDetailsAdapter(
        private val context: Context,
        private val onHyperLinkClick: (PersonDetailUiModel) -> Unit
) : ListAdapter<ProgrammeDetailsItemUiModel, EventProgrammeDetailsAdapter.ViewHolder>(EventProgrammeDetailsDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ProgrammeDetailsItemViewHolder(
                ItemProgrammeBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ),
                context,
                onHyperLinkClick
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: ProgrammeDetailsItemUiModel)
    }

    class ProgrammeDetailsItemViewHolder(
            private val binding: ItemProgrammeBinding,
            private val context: Context,
            private val onHyperLinkClick: (PersonDetailUiModel) -> Unit

    ) : ViewHolder(binding.root) {
        override fun bind(item: ProgrammeDetailsItemUiModel) {

            binding.tvTitle.text = item.title
            binding.tvTitle2.text = item.title
            binding.tvTime.text = item.time


            // For clickable text in textView

            val confirmedSpeakerText = if (item.speakers.isNotEmpty()) {
                if (item.speakers.size == 1) "\n\n\nConfirmed Speaker:" else "<b>\n\n\nConfirmed Speakers:"
            } else ""
            val confirmedDelegatesText = if (item.delegates.isNotEmpty()) {
                if (item.delegates.size == 1) "\n\n\nConfirmed Delegate:" else "\n\n\nConfirmed Delegates:"
            } else ""

            var speakersSpannableText = SpannedString("")
            for (speaker in item.speakers) {
                val speakerName = SpannableString(speaker.name)


                speakerName.setSpan(MyClickableSpan(speaker, onHyperLinkClick, context), 0, speakerName.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                val spannedSpeakerName = TextUtils.concat(speakerName,
                        if (speaker.designation.isNotEmpty())
                            ", ${speaker.designation}." else ""
//                                ", ${speaker.company}" +
//                                ", ${speaker.country}" +
                ) as SpannedString

                val spaceSpannedString = SpannableString("\n\n")
                spaceSpannedString.setSpan(RelativeSizeSpan(0.45f), 0, spaceSpannedString.length, 0) // set size

                speakersSpannableText = TextUtils.concat(speakersSpannableText, spaceSpannedString, spannedSpeakerName) as SpannedString

            }

            speakersSpannableText = TextUtils.concat(item.description, confirmedSpeakerText, speakersSpannableText) as SpannedString

            var delegatesSpannableText = SpannedString("")

            for (delegate in item.delegates) {
                val delegateName = SpannableString(delegate.name)


                delegateName.setSpan(MyClickableSpan(delegate, onHyperLinkClick, context), 0, delegateName.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                val spannedSpeakerName = TextUtils.concat(delegateName,
                        if (delegate.designation.isNotEmpty())
                            ", ${delegate.designation}." else ""
//                                ", ${delegate.company}" +
//                                ", ${delegate.country}" +
                ) as SpannedString

                val spaceSpannedString = SpannableString("\n\n")
                spaceSpannedString.setSpan(RelativeSizeSpan(0.45f), 0, spaceSpannedString.length, 0) // set size


                delegatesSpannableText = TextUtils.concat(delegatesSpannableText, spaceSpannedString, spannedSpeakerName) as SpannedString

            }

            speakersSpannableText = TextUtils.concat(speakersSpannableText, confirmedDelegatesText, delegatesSpannableText) as SpannedString


            binding.tvDescription.text = speakersSpannableText
            binding.tvDescription.movementMethod = LinkMovementMethod.getInstance()



            expandContractView(binding, item, context)

            binding.imageDropdown.setOnClickListener {
                item.isExpanded = !item.isExpanded
                expandContractView(binding, item, context)
            }
        }


        private fun expandContractView(binding: ItemProgrammeBinding, item: ProgrammeDetailsItemUiModel, context: Context) {
            if (item.isExpanded) {
                binding.cardView.visibility = View.VISIBLE
//                binding.tvTitle2.visibility = View.INVISIBLE
//                binding.tvTitle.visibility = View.VISIBLE
                binding.imageClickable.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round_top)
                binding.imageDropdown.setImageResource(R.drawable.action_drop_up)

            } else {

                binding.cardView.visibility = View.GONE
//                binding.tvTitle2.visibility = View.VISIBLE
//                binding.tvTitle.visibility = View.GONE
                binding.imageClickable.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round)
                binding.imageDropdown.setImageResource(R.drawable.action_drop_down)


            }
        }
    }


}

class EventProgrammeDetailsDiffUtil : DiffUtil.ItemCallback<ProgrammeDetailsItemUiModel>() {
    override fun areItemsTheSame(
            oldItem: ProgrammeDetailsItemUiModel,
            newItem: ProgrammeDetailsItemUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: ProgrammeDetailsItemUiModel,
            newItem: ProgrammeDetailsItemUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
