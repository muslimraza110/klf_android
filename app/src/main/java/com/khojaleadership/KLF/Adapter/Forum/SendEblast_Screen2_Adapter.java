package com.khojaleadership.KLF.Adapter.Forum;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.SendEblastModels.Send_Eblast_Parent_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class SendEblast_Screen2_Adapter extends RecyclerView.Adapter<SendEblast_Screen2_Adapter.RecyclerviewHolder> {


    ArrayList<Send_Eblast_Parent_Model> parent_list;
    RecyclerViewClickListner listner;

    public SendEblast_Screen2_Adapter(ArrayList<Send_Eblast_Parent_Model> parent_list, RecyclerViewClickListner listner) {
        this.parent_list = parent_list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.send_eblast_screen2_recyclerview_items,parent,false);


        return new RecyclerviewHolder(view,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerviewHolder holder, int position) {

        try {
            holder.textView.setText(parent_list.get(position).getParent_name());

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    @Override
    public int getItemCount() {
        return parent_list.size();
    }

    public class RecyclerviewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView imageView;

        LinearLayout toword_screen3;

        public RecyclerviewHolder(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);

            textView = itemView.findViewById(R.id.textview);
            imageView = itemView.findViewById(R.id.imageview);

            toword_screen3=(LinearLayout)itemView.findViewById(R.id.forword_to_screen3);
            toword_screen3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });


        }
    }
}
