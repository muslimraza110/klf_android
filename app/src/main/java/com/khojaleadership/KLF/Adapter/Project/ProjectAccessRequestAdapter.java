package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.Project_Access_Request_List_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class ProjectAccessRequestAdapter extends RecyclerView.Adapter<ProjectAccessRequestAdapter.viewHolder>{

    Context mctx;
    ArrayList<Project_Access_Request_List_Data_Model> list;
    RecyclerViewClickListner listner;

    public ProjectAccessRequestAdapter(Context mctx, ArrayList<Project_Access_Request_List_Data_Model> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.aa_group_project_pending_access_request_recyclerview_items, viewGroup, false);

        return new viewHolder(view,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {

        try {
            if (list.size()>0) {
                Project_Access_Request_List_Data_Model item = list.get(i);
                viewHolder.name.setText(item.getFullname());
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView accept_member,delete_member;
        public viewHolder(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.join_group_member_name);
            accept_member=itemView.findViewById(R.id.accept_member);
            delete_member=itemView.findViewById(R.id.reject_member);

            accept_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

            delete_member.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

        }
    }
}
