package com.khojaleadership.KLF.Adapter.Forum;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetSubTopicDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Sub_Topic_Adapter extends RecyclerView.Adapter<Sub_Topic_Adapter.viewHolder>{
    Context mctx;
    ArrayList<GetSubTopicDataModel> list;
    RecyclerViewClickListner listner;

    public Sub_Topic_Adapter(Context mctx, ArrayList<GetSubTopicDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(mctx);
        View v=inflater.inflate(R.layout.sub_topics_items,null);

        return new viewHolder(v,listner);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {

        try {
            GetSubTopicDataModel item=list.get(i);

            //viewHolder.title.setText(item.getSub_topic_title());
            viewHolder.sub_topic_text.setText(item.getForumPosts__topic());

            viewHolder.like_text.setText("("+item.getLikes()+")");
            viewHolder.unlike_text.setText("("+item.getDislike()+")");
            viewHolder.comment_text.setText("("+item.getComments()+")");
            viewHolder.claim_text.setText("("+item.getPendingcomments()+")"); //changes required
            viewHolder.group_text.setText("("+item.getParticptatinguser()+")");

            if (Common.is_Admin_flag==false){
                viewHolder.edit_layout.setVisibility(View.GONE);
                viewHolder.delete_layout.setVisibility(View.GONE);
            }

            if (Common.login_data.getData().getId()==list.get(i).getForumPosts__user_id() ||
                    Common.login_data.getData().getId().equals(list.get(i).getForumPosts__user_id())){

                viewHolder.edit_layout.setVisibility(View.VISIBLE);
                viewHolder.delete_layout.setVisibility(View.VISIBLE);
                Log.d("AddRemove", "visible id :"+i);
            }

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }


    public void updateList(ArrayList<GetSubTopicDataModel> new_list){
        list=new_list;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public void AddItem(ArrayList<GetSubTopicDataModel> new_list){
        for (GetSubTopicDataModel item:new_list){
            list.add(item);
        }
        notifyDataSetChanged();
    }


    class viewHolder extends RecyclerView.ViewHolder{
        TextView title,sub_topic_text;
        TextView like_text,unlike_text,comment_text,claim_text,group_text;

        ImageView edit_icon;

        RelativeLayout edit_layout,delete_layout;

        public viewHolder(@NonNull View itemView,final RecyclerViewClickListner listner) {
             super(itemView);

             title=(TextView)itemView.findViewById(R.id.sub_topic_title_text);
             sub_topic_text=(TextView)itemView.findViewById(R.id.sub_topic_text);

             like_text=(TextView)itemView.findViewById(R.id.like_text);
             unlike_text=(TextView)itemView.findViewById(R.id.unlike_text);
             comment_text=(TextView)itemView.findViewById(R.id.comment_text);
             claim_text=(TextView)itemView.findViewById(R.id.claim_text);
             group_text=(TextView)itemView.findViewById(R.id.group_text);

            edit_layout=(RelativeLayout) itemView.findViewById(R.id.sub_topic_edit_layout);
            edit_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });


            delete_layout=(RelativeLayout) itemView.findViewById(R.id.sub_topic_delete_layout);
            delete_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

             itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (listner!=null){
                         listner.onRowClick(getAdapterPosition());
                     }
                 }
             });

         }
     }
}
