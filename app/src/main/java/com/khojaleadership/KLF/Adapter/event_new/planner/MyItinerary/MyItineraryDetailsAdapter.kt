package com.khojaleadership.KLF.Adapter.event_new.planner.MyItinerary

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.SpannedString
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.planner.sessions.SessionItemUiModel
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.*
import com.khojaleadership.KLF.Activities.event_new.programme.PersonDetailUiModel
import com.khojaleadership.KLF.Helper.MyClickableSpan
import com.khojaleadership.KLF.Model.event_new.SessionType
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.*

class MyItineraryDetailsAdapter(
        private val context: Context,
        private val onHyperLinkClick: (PersonDetailUiModel) -> Unit,
        private val onRequestMeetingClick: (String) -> Unit,
        private val onCancelMeetingClick: (MyItineraryRequestItemUiModel) -> Unit,
        private val onListIconClick: (MyItineraryRequestItemUiModel) -> Unit,
        private val onChatIconClick: (MyItineraryRequestItemUiModel) -> Unit,
        private val onAcceptRejectClick: (Boolean, MyItineraryRequestItemUiModel) -> Unit,
        private val onSessionTextClick: (MyItinerarySessionUiModel) -> Unit,
        private val onDropDownClick: (MyItinerarySessionUiModel) -> Unit,
        private val onBtnBreakoutSessionsClick: (List<SessionItemUiModel>) -> Unit
) : ListAdapter<MyItineraryItemUiModel, MyItineraryDetailsAdapter.ViewHolder>(EventProgrammeDetailsDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            R.layout.item_programme -> {
                SessionDetailsItemViewHolder(
                        ItemProgrammeBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                        ),
                        context,
                        onHyperLinkClick
                )
            }
            R.layout.item_my_itinerary_session -> {
                SessionViewHolder(
                        ItemMyItinerarySessionBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                        ),
                        onRequestMeetingClick,
                        onSessionTextClick,
                        onDropDownClick,
                        context
                )
            }
            R.layout.item_my_itinerary_request_sent -> {
                RequestSentViewHolder(
                        ItemMyItineraryRequestSentBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                        ),
                        onCancelMeetingClick,
                        onListIconClick,
                        onChatIconClick,
                        onBtnBreakoutSessionsClick
                )
            }
            R.layout.item_my_itinerary_request_accepted -> {
                RequestAcceptedViewHolder(
                        ItemMyItineraryRequestAcceptedBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                        ),
                        onCancelMeetingClick,
                        onListIconClick,
                        onBtnBreakoutSessionsClick
                )
            }
            R.layout.item_my_itinerary_request_receive -> {
                RequestReceivedViewHolder(
                        ItemMyItineraryRequestReceiveBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                        ),
                        onListIconClick,
                        onAcceptRejectClick,
                        onBtnBreakoutSessionsClick
                )
            }
            else -> {
                throw IllegalArgumentException("layout not find")
            }

        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).type) {
            MyItineraryItemType.SESSION_DETAILS -> R.layout.item_programme
            MyItineraryItemType.SESSION -> R.layout.item_my_itinerary_session
            MyItineraryItemType.REQUEST_SENT -> R.layout.item_my_itinerary_request_sent
            MyItineraryItemType.REQUEST_ACCEPTED -> R.layout.item_my_itinerary_request_accepted
            MyItineraryItemType.REQUEST_RECEIVED -> R.layout.item_my_itinerary_request_receive
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }


    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: MyItineraryItemUiModel)
    }


    class SessionViewHolder(
            val binding: ItemMyItinerarySessionBinding,
            private val onRequestMeetingClick: (String) -> Unit,
            private val onSessionTextClick: (MyItinerarySessionUiModel) -> Unit,
            private val onDropDownClick: (MyItinerarySessionUiModel) -> Unit,
            private val context: Context
    ) : ViewHolder(binding.root) {
        override fun bind(item: MyItineraryItemUiModel) {
            (item as MyItinerarySessionUiModel).apply {
                binding.tvTime.text = time
//                binding.viewBox.setOnClickListener {
//                    onRequestMeetingClick.invoke(programDetailsId)
//                }

//                binding.viewBox.isEnabled = checkMyAvailabilityIsAvailable == CheckAvailabilityStatus.YES.value
//                if (checkMyAvailabilityIsAvailable == CheckAvailabilityStatus.YES.value) {
//                    binding.viewBox.setBackgroundColor(ContextCompat.getColor(context, R.color.appWhite))
//                } else {
//                    binding.viewBox.setBackgroundColor(ContextCompat.getColor(context, R.color.appCheckboxDisabled))
//                }
//                if (canRequestMeeting == RequestMeeting.NO.value) {
//                    binding.viewBox.visibility = View.GONE
//                    binding.tvRequestMeeting.visibility = View.GONE
//                } else {
//                    binding.viewBox.visibility = View.VISIBLE
//                    binding.tvRequestMeeting.visibility = View.VISIBLE
//                }
//
//                if (sessionType == SessionType.BREAKOUT.value) {
//                    binding.btnViewSessions.visibility = View.VISIBLE
//                } else {
//                    binding.btnViewSessions.visibility = View.INVISIBLE
//                }



                if (isExpanded) {
                    binding.imageDropdown.setImageResource(R.drawable.action_drop_up)

//                    val marginLayoutParams = binding.root.layoutParams as ViewGroup.MarginLayoutParams
//                    marginLayoutParams.bottomMargin = App.convertDpToPixel(4f, context).roundToInt()
                }else{
                    binding.imageDropdown.setImageResource(R.drawable.action_drop_down)

//                    val marginLayoutParams = binding.root.layoutParams as ViewGroup.MarginLayoutParams
//                    marginLayoutParams.bottomMargin = App.convertDpToPixel(16f, context).roundToInt()
                }

                binding.tvSession.setOnClickListener {
                    onSessionTextClick.invoke(this)
                }

                if(meetingRequestCount > 0){
                    binding.imageDropdown.visibility = View.VISIBLE
                }else{
                    binding.imageDropdown.visibility = View.GONE
                }

                binding.imageDropdown.setOnClickListener {
                    onDropDownClick.invoke(this)
                }

                if(sessionType == SessionType.PLENARY.value){
                    binding.tvSession.text = "More Info"
                }else{
                    if(sessionsList.any { it.iAmEnrolled }){
                        binding.tvSession.text = "Change Session"
                    }else{
                        binding.tvSession.text = "Select Session"
                    }
                }
            }
        }
    }


    class RequestSentViewHolder(
            val binding: ItemMyItineraryRequestSentBinding,
            private val onCancelMeetingClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onListIconClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onChatIconClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onBtnBreakoutSessionsClick: (List<SessionItemUiModel>) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: MyItineraryItemUiModel) {
            (item as MyItineraryRequestItemUiModel).apply {
                binding.tvName.text = name
                binding.tvDesignation.text = designation
                binding.tvTime.text = time
                binding.buttonCancelMeeting.setOnClickListener {
                    onCancelMeetingClick.invoke(this)
                }

                binding.imageList.setOnClickListener {
                    onListIconClick.invoke(this)
                }

                binding.imageChat.setOnClickListener {
                    onChatIconClick.invoke(this)
                }

//                if (sessionType == SessionType.BREAKOUT.value) {
//                    binding.btnViewSessions.visibility = View.VISIBLE
//                } else {
//                    binding.btnViewSessions.visibility = View.INVISIBLE
//                }
//
//                binding.btnViewSessions.setOnClickListener {
//                    onBtnBreakoutSessionsClick.invoke(sessionsList)
//                }
            }
        }
    }

    class RequestAcceptedViewHolder(
            val binding: ItemMyItineraryRequestAcceptedBinding,
            private val onCancelMeetingClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onListIconClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onBtnBreakoutSessionsClick: (List<SessionItemUiModel>) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: MyItineraryItemUiModel) {
            (item as MyItineraryRequestItemUiModel).apply {
                binding.tvName.text = name
                binding.tvDesignation.text = designation
                binding.tvTime.text = time

                binding.buttonCancelMeeting.setOnClickListener {
                    onCancelMeetingClick.invoke(this)
                }

                binding.imageList.setOnClickListener {
                    onListIconClick.invoke(this)
                }

//                if (sessionType == SessionType.BREAKOUT.value) {
//                    binding.btnViewSessions.visibility = View.VISIBLE
//                } else {
//                    binding.btnViewSessions.visibility = View.INVISIBLE
//                }
//
//                binding.btnViewSessions.setOnClickListener {
//                    onBtnBreakoutSessionsClick.invoke(sessionsList)
//                }
            }
        }
    }


    class RequestReceivedViewHolder(
            val binding: ItemMyItineraryRequestReceiveBinding,
            private val onListIconClick: (MyItineraryRequestItemUiModel) -> Unit,
            private val onAcceptRejectClick: (Boolean, MyItineraryRequestItemUiModel) -> Unit,
            private val onBtnBreakoutSessionsClick: (List<SessionItemUiModel>) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: MyItineraryItemUiModel) {
            (item as MyItineraryRequestItemUiModel).apply {
                binding.tvName.text = name
                binding.tvDesignation.text = designation
                binding.tvTime.text = time

                binding.imageList.setOnClickListener {
                    onListIconClick.invoke(this)
                }

                binding.layoutAccept.setOnClickListener {
                    onAcceptRejectClick.invoke(true, this)
                }

                binding.layoutReject.setOnClickListener {
                    onAcceptRejectClick.invoke(false, this)
                }

//                if (sessionType == SessionType.BREAKOUT.value) {
//                    binding.btnViewSessions.visibility = View.VISIBLE
//                } else {
//                    binding.btnViewSessions.visibility = View.INVISIBLE
//                }
//
//                binding.btnViewSessions.setOnClickListener {
//                    onBtnBreakoutSessionsClick.invoke(sessionsList)
//                }
            }
        }
    }


    class SessionDetailsItemViewHolder(
            val binding: ItemProgrammeBinding,
            val context: Context,
            val onHyperLinkClick: (PersonDetailUiModel) -> Unit

    ) : ViewHolder(binding.root) {
        override fun bind(item: MyItineraryItemUiModel) {

            (item as MyItineraryPlenarySessionItemUiModel).apply {

                binding.tvTitle.text = item.title
                binding.tvTitle2.text = item.title
                binding.tvTime.text = item.time


                // For clickable text in textView

                val confirmedSpeakerText = if (item.speakers.isNotEmpty()) {
                    if (item.speakers.size == 1) "\n\n\nConfirmed Speaker:" else "\n\n\nConfirmed Speakers:"
                } else ""
                val confirmedDelegatesText = if (item.delegates.isNotEmpty()) {
                    if (item.delegates.size == 1) "\n\n\nConfirmed Delegate:" else "\n\n\nConfirmed Delegates:"
                } else ""

                var speakersSpannableText = SpannedString("")
                for (speaker in item.speakers) {
                    val speakerName = SpannableString(speaker.name)


                    speakerName.setSpan(MyClickableSpan(speaker, onHyperLinkClick, context), 0, speakerName.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                    val spannedSpeakerName = TextUtils.concat(speakerName,
                            if (speaker.designation.isNotEmpty())
                                ", ${speaker.designation}." else ""
//                                ", ${speaker.company}" +
//                                ", ${speaker.country}" +
                    ) as SpannedString

                    val spaceSpannedString = SpannableString("\n\n")
                    spaceSpannedString.setSpan(RelativeSizeSpan(0.45f), 0, spaceSpannedString.length, 0) // set size


                    speakersSpannableText = TextUtils.concat(speakersSpannableText, spaceSpannedString, spannedSpeakerName) as SpannedString

                }

                speakersSpannableText = TextUtils.concat(item.description, confirmedSpeakerText, speakersSpannableText) as SpannedString

                var delegatesSpannableText = SpannedString("")

                for (delegate in item.delegates) {
                    val delegateName = SpannableString(delegate.name)


                    delegateName.setSpan(MyClickableSpan(delegate, onHyperLinkClick, context), 0, delegateName.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                    val spannedSpeakerName = TextUtils.concat(delegateName,
                            if (delegate.designation.isNotEmpty())
                                ", ${delegate.designation}." else ""
//                                ", ${delegate.company}" +
//                                ", ${delegate.country}" +
                    ) as SpannedString

                    val spaceSpannedString = SpannableString("\n\n")
                    spaceSpannedString.setSpan(RelativeSizeSpan(0.45f), 0, spaceSpannedString.length, 0) // set size


                    delegatesSpannableText = TextUtils.concat(delegatesSpannableText, spaceSpannedString, spannedSpeakerName) as SpannedString

                }

                speakersSpannableText = TextUtils.concat(speakersSpannableText, confirmedDelegatesText, delegatesSpannableText) as SpannedString


//            item.delegates?.let { moderator->
//                val moderatorName = SpannableString(moderator.name)
//
//                moderatorName.setSpan(MyClickableSpan(moderator,onHyperLinkClick,context), 0, moderatorName.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//                val spannedModeratorName = TextUtils.concat(moderatorName, ", ${moderator.designation}, ${moderator.company}, ${moderator.country}.") as SpannedString
//                speakersSpannableText = TextUtils.concat(speakersSpannableText,confirmedDelegatesText,"\n\n",spannedModeratorName) as SpannedString
//            }

                binding.tvDescription.text = speakersSpannableText
                binding.tvDescription.movementMethod = LinkMovementMethod.getInstance()

                expandContractView(binding, item, context)

                binding.imageDropdown.setOnClickListener {
                    item.isExpanded = !item.isExpanded
                    expandContractView(binding, item, context)
                }

            }

        }


        private fun expandContractView(binding: ItemProgrammeBinding, item: MyItineraryPlenarySessionItemUiModel, context: Context) {
            if (item.isExpanded) {
                binding.cardView.visibility = View.VISIBLE
//                binding.tvTitle2.visibility = View.INVISIBLE
//                binding.tvTitle.visibility = View.VISIBLE
                binding.imageClickable.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round_top)
                binding.imageDropdown.setImageResource(R.drawable.action_drop_up)

            } else {

                binding.cardView.visibility = View.GONE
//                binding.tvTitle2.visibility = View.VISIBLE
//                binding.tvTitle.visibility = View.GONE
                binding.imageClickable.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round)
                binding.imageDropdown.setImageResource(R.drawable.action_drop_down)


            }
        }
    }

}

class EventProgrammeDetailsDiffUtil : DiffUtil.ItemCallback<MyItineraryItemUiModel>() {
    override fun areItemsTheSame(
            oldItem: MyItineraryItemUiModel,
            newItem: MyItineraryItemUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: MyItineraryItemUiModel,
            newItem: MyItineraryItemUiModel
    ): Boolean {
        return oldItem == newItem
    }
}