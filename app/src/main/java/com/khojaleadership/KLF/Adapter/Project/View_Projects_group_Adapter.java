package com.khojaleadership.KLF.Adapter.Project;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Project_Models.ProjectsDetailGroupDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class View_Projects_group_Adapter extends RecyclerView.Adapter<View_Projects_group_Adapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<ProjectsDetailGroupDataModel> list;
   RecyclerViewClickListner listner;

    public View_Projects_group_Adapter(Context mctx, ArrayList<ProjectsDetailGroupDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.view_projects_related_group_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {

        try {
            ProjectsDetailGroupDataModel item=list.get(i);
            myviewHolderClass.group_name.setText(item.getGroups__name());

            Glide.with(mctx).load(item.getGroup_image()).into(myviewHolderClass.imageView);
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView group_name;
        CircleImageView imageView;
        LinearLayout project_group_row;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);
            group_name  = (TextView) itemView.findViewById(R.id.project_group_name);
            imageView=(CircleImageView) itemView.findViewById(R.id.project_group_image);

            project_group_row=itemView.findViewById(R.id.project_group_row);
            project_group_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });
        }
    }
}

