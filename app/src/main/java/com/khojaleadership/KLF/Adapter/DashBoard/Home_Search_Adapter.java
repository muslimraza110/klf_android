package com.khojaleadership.KLF.Adapter.DashBoard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.DashBoard_Model.Home_Main_Search_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class Home_Search_Adapter extends RecyclerView.Adapter<Home_Search_Adapter.viewHolder>{

    Context mctx;
    ArrayList<Home_Main_Search_Model> list;
    RecyclerViewClickListner listner;

    public Home_Search_Adapter(Context mctx, ArrayList<Home_Main_Search_Model> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.aa_dashboard_search_recyclerview_items, viewGroup, false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {

        try {
            Home_Main_Search_Model item=list.get(i);
            viewHolder.name.setText(item.getName());
            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }

    }

    public void updateList(ArrayList<Home_Main_Search_Model> list){
        this.list= list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RelativeLayout complete_row;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.main_search_name);
            complete_row=itemView.findViewById(R.id.complete_row);
            complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}
