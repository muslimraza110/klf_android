package com.khojaleadership.KLF.Adapter.DashBoard;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.khojaleadership.KLF.Model.DashBoard_Model.OfflineExpandableChildModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;
import java.util.List;

public class OfflineRecyclerviewAdapter extends RecyclerView.Adapter<OfflineRecyclerviewAdapter.ViewHolder> {

    private Context context;
    private List<OfflineExpandableChildModel> mobiles = new ArrayList<OfflineExpandableChildModel>();

    public OfflineRecyclerviewAdapter(Context context, List<OfflineExpandableChildModel> mobiles) {
        this.context = context;
        this.mobiles = mobiles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cardView = inflater.inflate(R.layout.mainscreen_item_child, null, false);
        ViewHolder viewHolder = new ViewHolder(cardView);
        viewHolder.mobileImage = (ImageView) cardView.findViewById(R.id.image_mobile);
        viewHolder.modelName = (TextView) cardView.findViewById(R.id.text_mobile_model);
//        viewHolder.price = (TextView) cardView.findViewById(R.id.text_mobile_price);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            ImageView mobileImageView = (ImageView) holder.mobileImage;
            mobileImageView.setImageResource(mobiles.get(position).imageResource);

            TextView modelTextView = (TextView) holder.modelName;
            modelTextView.setText(mobiles.get(position).modelName);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }



    }

    @Override
    public int getItemCount() {
        return mobiles.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mobileImage;
        TextView modelName;


        public ViewHolder(View itemView) {
            super(itemView);
            mobileImage = (ImageView) itemView.findViewById(R.id.image_mobile);
            modelName = (TextView) itemView.findViewById(R.id.text_mobile_model);

        }
    }

}
