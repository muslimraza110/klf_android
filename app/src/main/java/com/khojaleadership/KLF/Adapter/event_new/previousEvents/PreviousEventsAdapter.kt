package com.khojaleadership.KLF.Adapter.event_new.previousEvents

import android.content.Context
import android.text.SpannableString
import android.text.SpannedString
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.Activities.event_new.events.EventCardUiModel
import com.khojaleadership.KLF.Helper.CustomTypefaceSpan
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemEventCardBinding

class PreviousEventsAdapter(
        private val context: Context,
        private val isUpcomingEvent: Boolean,
        private val onViewDetailsClick: (EventCardUiModel)-> Unit
) : RecyclerView.Adapter<PreviousEventsAdapter.EventCardViewHolder>() {

    var eventList = mutableListOf<EventCardUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventCardViewHolder {
        return EventCardViewHolder(
                ItemEventCardBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent, false
                ),
                context,
                isUpcomingEvent,
                onViewDetailsClick
        )
    }

    override fun getItemCount(): Int {
        return eventList.size
    }

    override fun onBindViewHolder(holder: EventCardViewHolder, position: Int) {
        holder.bind(eventList[position])
    }

    class EventCardViewHolder(
            val binding: ItemEventCardBinding,
            val context: Context,
            private val isUpcomingEvent: Boolean,
            private val onViewDetailsClick: (EventCardUiModel)-> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: EventCardUiModel) {

            item.apply {
                Glide.with(context)
                        .load(eventImage)
//                        .placeholder(R.drawable.building)
                        .into(binding.imageView)
                binding.tvAddress2.text = city
                binding.tvDate.text = date
//                binding.tvPrice.text = price


                /* Check if Price Start or End have some values NOR hide the price label */
                binding.tvPrice.visibility = View.VISIBLE
                if (priceRangeStart != "0" && priceRangeEnd == "0")
                {
                    binding.tvPrice.text = "$${priceRangeStart}"
                }
                else if (priceRangeStart == "0" && priceRangeEnd != "0")
                {
                    binding.tvPrice.text = "$${priceRangeEnd}"
                }
                else if (priceRangeStart != "0" && priceRangeEnd != "0")
                {
                    binding.tvPrice.text = "$${priceRangeStart} - $${priceRangeEnd}"
                }
                else {
                    binding.tvPrice.text = "$${priceRangeStart} - $${priceRangeEnd}"
                    binding.tvPrice.visibility = View.GONE
                }


                val completeString: SpannedString

                val eventName = SpannableString(name)
                eventName.setSpan(
                        CustomTypefaceSpan(
                                "",
                                ResourcesCompat.getFont(context, R.font.poppins_semibold)!!
                        ),
                        0,
                        name.length,
                        0
                )

                eventName.setSpan(
                        RelativeSizeSpan(1.1f), 0, name.length, 0
                )

                val eventAddress = SpannableString(address)
                eventAddress.setSpan(
                        CustomTypefaceSpan(
                                "",
                                ResourcesCompat.getFont(context, R.font.poppins_regular)!!
                        ),
                        0,
                        address.length,
                        0
                )

                completeString = TextUtils.concat(eventName, "\n", eventAddress) as SpannedString

                binding.tvAddress.text = completeString

                if(isUpcomingEvent){
                    binding.tvViewDetails.setOnClickListener {
                        onViewDetailsClick.invoke(this)
                    }
                    binding.tvViewDetails.visibility = View.VISIBLE
                }else{
                    binding.tvViewDetails.visibility = View.INVISIBLE
                }

            }



        }
    }

    fun updateList(list: MutableList<EventCardUiModel>) {
        eventList = list
        notifyDataSetChanged()
    }
}