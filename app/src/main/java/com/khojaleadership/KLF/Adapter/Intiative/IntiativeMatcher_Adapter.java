package com.khojaleadership.KLF.Adapter.Intiative;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Model.Intiative_Models.Intiative_Detail_Fundraising_Matcher_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class IntiativeMatcher_Adapter extends RecyclerView.Adapter<IntiativeMatcher_Adapter.MyViewHolder> {

    Context context;
    ArrayList<Intiative_Detail_Fundraising_Matcher_Model> arrayList;

    public IntiativeMatcher_Adapter(Context context, ArrayList<Intiative_Detail_Fundraising_Matcher_Model> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_events_rowlayout,parent,false);

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.intiative_matcher_recyclerview_items, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        try {
            Intiative_Detail_Fundraising_Matcher_Model proj_model = arrayList.get(position);

            if (Common.is_Admin_flag==true){
                holder.matcher_user_name.setText(proj_model.getMatcher__name());
            }else {
                if (proj_model.getMatcher__anonymous().equals("1")){
                    holder.matcher_user_name.setText("<Anonymous>");
                }else {
                    holder.matcher_user_name.setText(proj_model.getMatcher__name());
                }
            }


            //holder.matcher_user_name.setText(proj_model.getMatcher__name());

            if (Common.is_Admin_flag==true){
                holder.matcher_ammount.setText(proj_model.getMatcher__amount()+" USD");
            }else {
                if (proj_model.getMatcher__hide_amount().equals("1")){
                    holder.matcher_ammount.setText("<Hidden>");
                }else {
                    holder.matcher_ammount.setText(proj_model.getMatcher__amount()+" USD");
                }
            }

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView matcher_user_name, matcher_ammount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            //project
            matcher_user_name = (TextView) itemView.findViewById(R.id.matcher_user_name);
            matcher_ammount = (TextView) itemView.findViewById(R.id.matcher_ammount);

        }
    }
}
