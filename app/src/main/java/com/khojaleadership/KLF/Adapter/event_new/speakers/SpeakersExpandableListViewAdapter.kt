package com.khojaleadership.KLF.Adapter.event_new.speakers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.khojaleadership.KLF.Activities.event_new.speakers.SpeakersUiModel
import com.khojaleadership.KLF.R

class SpeakersExpandableListViewAdapter(
        private val context: Context,
        private var listDataHeader: List<String>,
        private var listChildData: HashMap<String, List<SpeakersUiModel>>,
        private val onClick: (SpeakersUiModel) -> Unit,
        private val onFacebookClick: (String) -> Unit,
        private val onLinkedInIconClick: (String) -> Unit

) : BaseExpandableListAdapter() {
    override fun getGroup(groupPosition: Int): String {
        return listDataHeader[groupPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            val inflater = this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.layout_list_group_header, null)
        }
        view?.findViewById<TextView>(R.id.tv_group_name)?.text = getGroup(groupPosition)

        if (isExpanded) {
            view?.findViewById<ImageView>(R.id.group_indicator)?.setImageResource(R.drawable.action_drop_up)
        } else {
            view?.findViewById<ImageView>(R.id.group_indicator)?.setImageResource(R.drawable.action_drop_down)
        }

        return view!!
    }


    override fun getChildrenCount(groupPosition: Int): Int {
        return listChildData[listDataHeader[groupPosition]]?.size ?: 0
    }

    override fun getChild(groupPosition: Int, childPosition: Int): SpeakersUiModel? {
        return listChildData[listDataHeader[groupPosition]]?.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            val inflater = this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.item_profile, null)
        }

        val item = getChild(groupPosition, childPosition)
        item?.apply {
            view?.findViewById<TextView>(R.id.tv_name)?.text = name
            view?.findViewById<TextView>(R.id.tv_date)?.text = date
            view?.findViewById<TextView>(R.id.tv_time)?.text = time
            view?.findViewById<TextView>(R.id.tv_designation)?.text = designation
            view?.findViewById<TextView>(R.id.tv_summary)?.text = description

            view?.findViewById<ImageView>(R.id.user_image)?.let{
                Glide.with(context)
                        .load(photoUrl)
                        .centerCrop()
                        .into(it)
            }
            view?.setOnClickListener {
                onClick.invoke(this)
            }
            view?.findViewById<ImageView>(R.id.linked_in_icon)?.setOnClickListener {
                onLinkedInIconClick.invoke(twitter)
            }
            view?.findViewById<ImageView>(R.id.fb_icon)?.setOnClickListener {
                onSocialIconClick.invoke(facebook)
            }
        }

        return view!!
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return listDataHeader.size
    }

    fun updateList(map: HashMap<String, List<SpeakersUiModel>>) {
        this.listChildData = map
        this.listDataHeader = map.keys.toList()
        notifyDataSetChanged()
    }
}