package com.khojaleadership.KLF.Adapter.DashBoard_New

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.DashBoard_New.EventHomeUiModel
import com.khojaleadership.KLF.Activities.DashBoard_New.HomeEventsItemType
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemHomeEventBinding
import com.khojaleadership.KLF.databinding.ItemHomeFundingBinding
import com.khojaleadership.KLF.databinding.ItemHomeProjectBinding

class EventRecyclerViewAdapter(
        private val onRegsiterClick: (String) -> Unit,
        private val onEventClick: (String) -> Unit,
        private val onDonateClick: (String) -> Unit,
        private val onSupportClick: (String) -> Unit

) : ListAdapter<EventHomeUiModel, EventRecyclerViewAdapter.ViewHolder>(EventRecyclerViewDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (viewType) {
            R.layout.item_home_event -> {
                return EventViewHolder(
                        ItemHomeEventBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                        onRegsiterClick,
                        onEventClick
                )
            }
            R.layout.item_home_project -> {
                return ProjectViewHolder(
                        ItemHomeProjectBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                        onDonateClick
                )
            }
            R.layout.item_home_funding -> {
                return FundingViewHolder(
                        ItemHomeFundingBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                        onSupportClick
                )
            }
            else ->{
                throw  IllegalArgumentException("Layout Not find")
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).itemType) {
            HomeEventsItemType.EVENT -> {
                R.layout.item_home_event
            }
            HomeEventsItemType.PROJECT -> {
                R.layout.item_home_project
            }
            HomeEventsItemType.FUNDING -> {
                R.layout.item_home_funding
            }
        }
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: EventHomeUiModel)
    }

    class EventViewHolder(
            val binding: ItemHomeEventBinding,
            private val onRegsiterClick: (String) -> Unit,
            private val onEventClick: (String) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: EventHomeUiModel) {

            binding.root.setOnClickListener {
                onEventClick.invoke("")
            }

            binding.btnRegister.setOnClickListener {
                onRegsiterClick.invoke("")
            }
        }
    }

    class ProjectViewHolder(
            binding: ItemHomeProjectBinding,
            private val onDonateClick: (String) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: EventHomeUiModel) {

        }
    }

    class FundingViewHolder(
            binding: ItemHomeFundingBinding,
            private val onSupportClick: (String) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: EventHomeUiModel) {

        }
    }

}

class EventRecyclerViewDiffUtil : DiffUtil.ItemCallback<EventHomeUiModel>() {
    override fun areItemsTheSame(
            oldItem: EventHomeUiModel,
            newItem: EventHomeUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: EventHomeUiModel,
            newItem: EventHomeUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
