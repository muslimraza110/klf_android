package com.khojaleadership.KLF.Adapter.event_new.programme

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.programme.EventDaysUiModel
import com.khojaleadership.KLF.Helper.App
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemProgrammeDayBinding
import kotlin.math.roundToInt

class EventProgrammeDaysAdapter(
        private val context: Context,
        private val onClick: (day: String, position: Int) -> Unit
) : ListAdapter<EventDaysUiModel, EventProgrammeDaysAdapter.ViewHolder>(EventProgrammeDaysDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ProgrammeDayViewHolder(
                ItemProgrammeDayBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ),
                onClick
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position, context, itemCount)
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: EventDaysUiModel, position: Int, context: Context, itemCount: Int)
    }

    class ProgrammeDayViewHolder(
            val binding: ItemProgrammeDayBinding,
            val onClick: (day: String, position: Int) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: EventDaysUiModel, position: Int, context: Context, itemCount: Int) {

            val displayMetrics: DisplayMetrics = context.resources.displayMetrics
            val dpWidth = displayMetrics.widthPixels / displayMetrics.density

            val recyclerviewMarginInDp = 16f

            val viewWidthInDp = ((dpWidth - (recyclerviewMarginInDp * 2) - 16) / 2) - 20

            val viewWidthInPx = App.convertDpToPixel(viewWidthInDp, context).roundToInt()
            binding.root.layoutParams.width = viewWidthInPx

            if (position != 0) {
                val marginLayoutParams = binding.root.layoutParams as ViewGroup.MarginLayoutParams
                marginLayoutParams.marginStart = App.convertDpToPixel(16f, context).roundToInt()
            }

            binding.root.setOnClickListener {
                onClick.invoke(item.day, position)
            }

            if (item.isSelected) {
                binding.root.background = ContextCompat.getDrawable(context, R.drawable.linear_gradient_blue_cyan_round)
                binding.tvDay.setTextColor(ContextCompat.getColor(context, R.color.textSecondary))
            } else {
                binding.root.background = ContextCompat.getDrawable(context, R.drawable.grey_outlined_bg)
                binding.tvDay.setTextColor(ContextCompat.getColor(context, R.color.appGrey))
            }

            binding.tvDay.text = item.day
        }
    }

}

class EventProgrammeDaysDiffUtil : DiffUtil.ItemCallback<EventDaysUiModel>() {
    override fun areItemsTheSame(
            oldItem: EventDaysUiModel,
            newItem: EventDaysUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: EventDaysUiModel,
            newItem: EventDaysUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
