package com.khojaleadership.KLF.Adapter.Setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Setting.View_Profile_Models.View_Profile_Bussiness_Project_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;


public class View_Profile_Bussiness_Adapter extends RecyclerView.Adapter<View_Profile_Bussiness_Adapter.BussinessViewHolder> {

    Context mctx;
    ArrayList<View_Profile_Bussiness_Project_Model> bussiness_models;
    RecyclerViewClickListner listner;

    public View_Profile_Bussiness_Adapter(Context mctx, ArrayList<View_Profile_Bussiness_Project_Model> bussiness_models, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.bussiness_models = bussiness_models;
        this.listner = listner;
    }

    @NonNull
    @Override
    public BussinessViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.aa_view_profile_bussinuess_recyclerview_items,parent,false);


        return new BussinessViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BussinessViewHolder holder, int position) {

        try {

            final View_Profile_Bussiness_Project_Model bussinessModel = bussiness_models.get(position);

//        holder.tv_title.setText(bussinessModel.getGroups__name());

            holder.tv_title.setText("Title not available");
            holder.tv_name.setText(bussinessModel.getGroups__name());

//        holder.img_bussiness.setImageResource(bussinessModel.getImg_bussiness());

            Glide.with(mctx).load(bussinessModel.getGroupimage()).into(holder.img_bussiness);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }


    @Override
    public int getItemCount() {

        return bussiness_models.size();

    }



    public class BussinessViewHolder extends RecyclerView.ViewHolder {

         ImageView img_bussiness;
         TextView tv_name;
         TextView tv_title;

        LinearLayout profile_bussiness_row;

        public BussinessViewHolder(@NonNull View itemView) {
            super(itemView);

            img_bussiness = itemView.findViewById(R.id.img_bussiness);
            tv_name = itemView.findViewById(R.id.tv_name_bussiness);
            tv_title= itemView.findViewById(R.id.tv_title_bussienss);

            profile_bussiness_row= itemView.findViewById(R.id.profile_bussiness_row);
            profile_bussiness_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),view);
                    }
                }
            });

        }
    }

}
