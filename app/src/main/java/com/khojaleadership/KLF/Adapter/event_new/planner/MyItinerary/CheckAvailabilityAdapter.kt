package com.khojaleadership.KLF.Adapter.event_new.planner.MyItinerary

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.khojaleadership.KLF.Activities.event_new.planner.myItinerary.*
import com.khojaleadership.KLF.Model.event_new.MeetingRequestStatusTypes
import com.khojaleadership.KLF.Model.event_new.SessionType
import com.khojaleadership.KLF.R
import com.khojaleadership.KLF.databinding.ItemCheckAvailabilityDateBinding
import com.khojaleadership.KLF.databinding.ItemCheckAvailablityBinding

class CheckMyAvailabilityAdapter(
        val onClick: (CheckAvailabilityItemUiModel) -> Unit
) : ListAdapter<CheckAvailabilityUiModel, CheckMyAvailabilityAdapter.ViewHolder>(CheckAvailabilityDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (viewType) {
            R.layout.item_check_availability_date -> {
                return CheckAvailabilityDateViewHolder(
                        ItemCheckAvailabilityDateBinding.inflate(
                                LayoutInflater.from(parent.context), parent, false
                        )
                )
            }
            R.layout.item_check_availablity -> {
                return CheckAvailabilityItemViewHolder(
                        ItemCheckAvailablityBinding.inflate(
                                LayoutInflater.from(parent.context), parent, false
                        ),
                        onClick
                )
            }
            else -> {
                throw  IllegalArgumentException("Layout Not found")
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).type) {
            CheckAvailabilityViewType.DATE_HEADER -> {
                R.layout.item_check_availability_date
            }
            CheckAvailabilityViewType.ITEM_VIEW -> {
                R.layout.item_check_availablity
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: CheckAvailabilityUiModel)
    }

    class CheckAvailabilityItemViewHolder(
            val binding: ItemCheckAvailablityBinding,
            val onClick: (CheckAvailabilityItemUiModel) -> Unit
    ) : ViewHolder(binding.root) {
        override fun bind(item: CheckAvailabilityUiModel) {
            (item as CheckAvailabilityItemUiModel).apply {

                binding.tvTime.text = time

                binding.imageStatus.setOnClickListener {
                    onClick.invoke(this)
                }

                when (status) {
                    CheckAvailabilityStatus.NO.value -> {
                        binding.imageStatus.setImageResource(R.drawable.check_cross_red)
                        if (sessionType == SessionType.BREAKOUT.value && hasJoinedABreakoutSession) {
                            binding.imageStatus.alpha = 0.4F
                            binding.imageStatus.isEnabled = false
                        } else {
                            binding.imageStatus.alpha = 1.0F
                            binding.imageStatus.isEnabled = true
                        }
                    }
                    CheckAvailabilityStatus.YES.value -> {
                        binding.imageStatus.setImageResource(R.drawable.check_tick_green)
                        if (lastMeetingStatus == MeetingRequestStatusTypes.MEETING_ACCEPTED.value ||
                                lastMeetingStatus == MeetingRequestStatusTypes.MEETING_REQUESTED.value) {
                            binding.imageStatus.alpha = 0.4F
                            binding.imageStatus.isEnabled = false
                        } else {
                            binding.imageStatus.alpha = 1F
                            binding.imageStatus.isEnabled = true
                        }
                    }
                    CheckAvailabilityStatus.NOT_AVAILABLE.value -> {
                        binding.imageStatus.setImageResource(R.drawable.check_minus_black)
                        binding.imageStatus.alpha = 1.0F
                        binding.imageStatus.isEnabled = false
                    }
                    null -> {
                        binding.imageStatus.setImageResource(R.drawable.check_empty_black)
                        binding.imageStatus.alpha = 1.0F
                        binding.imageStatus.isEnabled = true
                    }
                }

            }
        }
    }

    class CheckAvailabilityDateViewHolder(
            val binding: ItemCheckAvailabilityDateBinding
    ) : ViewHolder(binding.root) {
        override fun bind(item: CheckAvailabilityUiModel) {
            binding.groupIndicator.visibility = View.GONE
            (item as CheckAvailabilityDateUiModel).apply {
                binding.tvDate.text = "${dayModel.day} - ${dayModel.date}"
            }
        }
    }


}

class CheckAvailabilityDiffUtil : DiffUtil.ItemCallback<CheckAvailabilityUiModel>() {
    override fun areItemsTheSame(
            oldItem: CheckAvailabilityUiModel,
            newItem: CheckAvailabilityUiModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: CheckAvailabilityUiModel,
            newItem: CheckAvailabilityUiModel
    ): Boolean {
        return oldItem == newItem
    }
}