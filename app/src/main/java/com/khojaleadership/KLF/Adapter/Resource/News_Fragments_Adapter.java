package com.khojaleadership.KLF.Adapter.Resource;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Helper.Common;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Resource.News_Fragments_Data_Model;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class News_Fragments_Adapter extends RecyclerView.Adapter<News_Fragments_Adapter.MyviewHolder> {

    Context mctx;
    RecyclerViewClickListner listner;
    private ArrayList<News_Fragments_Data_Model> list;
    Boolean isActiveNews;

//    public News_Fragments_Adapter(Context mctx, RecyclerViewClickListner listner, ArrayList<News_Fragments_Data_Model> list) {
//        this.mctx = mctx;
//        this.listner = listner;
//        this.list = list;
//    }


    public News_Fragments_Adapter(Context mctx, RecyclerViewClickListner listner, ArrayList<News_Fragments_Data_Model> list, Boolean isActiveNews) {
        this.mctx = mctx;
        this.listner = listner;
        this.list = list;
        this.isActiveNews = isActiveNews;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(mctx);
        View listitem = layoutInflater.inflate(R.layout.news_fragments_recyclerview_items, viewGroup, false);
        MyviewHolder myviewHolder = new MyviewHolder(listitem);
        return myviewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder myviewHolder, int i) {

        try {
            final News_Fragments_Data_Model model = list.get(i);

            myviewHolder.title.setText(model.getPosts__topic());
            myviewHolder.user_name.setText(model.getPosts__name());


            //member k lieye gaib krna icons ko
            if (Common.is_Admin_flag==false){
                myviewHolder.archieve_icon.setVisibility(View.GONE);
            }

//            if (Common.is_active_news==true){
//                myviewHolder.archieve_icon.setImageResource(R.drawable.ic_archived_green);
//            }else if (Common.is_active_news==false){
//                myviewHolder.archieve_icon.setImageResource(R.drawable.ic_archive);
//            }

            if (isActiveNews==true){
                myviewHolder.archieve_icon.setImageResource(R.drawable.ic_archived_green);
            }else if (isActiveNews==false){
                myviewHolder.archieve_icon.setImageResource(R.drawable.ic_archive);
            }

            Glide.with(mctx.getApplicationContext()).load(model.getProfileimage()).into(myviewHolder.news_img);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(ArrayList<News_Fragments_Data_Model> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        TextView title,user_name;
        LinearLayout add_to_archive_btn;
        LinearLayout complete_row;

        ImageView archieve_icon,news_img;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            title= (TextView) itemView.findViewById(R.id.active_title);
            user_name= (TextView) itemView.findViewById(R.id.active_userName);
            complete_row=itemView.findViewById(R.id.news_complete_row);

            archieve_icon=(ImageView) itemView.findViewById(R.id.archieve_icon);
            news_img=itemView.findViewById(R.id.news_img);

            add_to_archive_btn=(LinearLayout)itemView.findViewById(R.id.add_to_archive_btn);
            add_to_archive_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });


            complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });


        }
    }
}
