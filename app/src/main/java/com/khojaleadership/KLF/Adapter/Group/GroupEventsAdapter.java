package com.khojaleadership.KLF.Adapter.Group;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GroupDetailEventListDataModel;
import com.khojaleadership.KLF.R;

import java.util.ArrayList;

public class GroupEventsAdapter extends RecyclerView.Adapter<GroupEventsAdapter.MyviewHolderClass> {


    Context mctx;
   ArrayList<GroupDetailEventListDataModel> list;
   RecyclerViewClickListner listner;

    public GroupEventsAdapter(Context mctx, ArrayList<GroupDetailEventListDataModel> list, RecyclerViewClickListner listner) {
        this.mctx = mctx;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyviewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater =LayoutInflater.from(viewGroup.getContext());
        View listitem = layoutInflater.inflate(R.layout.group_events_recyclerview_items,viewGroup,false);
        MyviewHolderClass holderClass = new MyviewHolderClass(listitem,listner);

        return holderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolderClass myviewHolderClass, final int i) {


        try{
            GroupDetailEventListDataModel item=list.get(i);
            myviewHolderClass.event_name.setText(item.getEvents__name());
            myviewHolderClass.group_event_date.setText(item.getEvents__start_date());
            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolderClass extends RecyclerView.ViewHolder {

        TextView event_name;
        TextView group_event_date;
        RelativeLayout cardView;

        public MyviewHolderClass(@NonNull View itemView,final RecyclerViewClickListner listner) {
            super(itemView);


            event_name= (TextView) itemView.findViewById(R.id.group_event_name);
            group_event_date=itemView.findViewById(R.id.group_event_date);
            cardView = itemView.findViewById(R.id.group_event_row);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

        }
    }
}

