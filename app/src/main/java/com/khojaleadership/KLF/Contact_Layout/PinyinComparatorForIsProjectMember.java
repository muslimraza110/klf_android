package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Project_Models.Project_Member_Data_List;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparatorForIsProjectMember implements Comparator<Project_Member_Data_List> {
    @Override
    public int compare(Project_Member_Data_List o1, Project_Member_Data_List o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        } else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }

    }

}