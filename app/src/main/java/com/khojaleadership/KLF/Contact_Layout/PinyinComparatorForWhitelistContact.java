package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.IsWhiteListed_Data_Model;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparatorForWhitelistContact implements Comparator<IsWhiteListed_Data_Model> {
    @Override
    public int compare(IsWhiteListed_Data_Model o1, IsWhiteListed_Data_Model o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        } else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }

    }

}