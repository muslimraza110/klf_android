package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Forum_Models.SubtopicModels.GetModeratorDataModel;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparatorForModerator implements Comparator<GetModeratorDataModel> {
    @Override
    public int compare(GetModeratorDataModel o1, GetModeratorDataModel o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        } else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }

    }

}
