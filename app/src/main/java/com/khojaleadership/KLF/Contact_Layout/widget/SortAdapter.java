package com.khojaleadership.KLF.Contact_Layout.widget;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;
import com.khojaleadership.KLF.R;

import java.util.List;

import com.khojaleadership.KLF.Helper.Common;

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetGroupListDataModel> mData;
    private Context mContext;
    private RecyclerViewClickListner listner;

    public SortAdapter(Context context, List<GetGroupListDataModel> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner = listner;
    }

    @Override
    public SortAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.aa_group_recyclerview_items2, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, listner);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SortAdapter.ViewHolder holder, final int position) {
        try {
            int section = getSectionForPosition(position);
            if (position == getPositionForSection(section)) {
                holder.tvTag.setVisibility(View.VISIBLE);
                ;
                holder.tvTag.setText(mData.get(position).getLetters());
            } else {
                holder.tvTag.setVisibility(View.GONE);
            }


            //hide view
            if (Common.is_Admin_flag==false) {
                holder.group_delete_icon.setVisibility(View.GONE);
                holder.group_edit_icon.setVisibility(View.GONE);
            }


            if (mData.get(position).getCreatedByUserID() == null) {
                //issue in api
            } else {

                if (mData.get(position).getCreatedByUserID().equals(Common.login_data.getData().getId())) {
                    Log.d("EditGroupIssue", "Id :" + mData.get(position).getCreatedByUserID());
                    holder.group_delete_icon.setVisibility(View.VISIBLE);
                    holder.group_edit_icon.setVisibility(View.VISIBLE);
                    ///issue
                }
            }


            //later have to work on this

//        //in case user id matches the group id means group admin is user
//        if (mData.get(position).getCreatedByUserID()==null){
//            //due to developer fault.
//        }else {
//            if (mData.get(position).getCreatedByUserID() == Common.login_data.getData().getId() ||
//                    mData.get(position).getCreatedByUserID().equals(Common.login_data.getData().getId())) {
//                holder.edit_icon.setVisibility(View.VISIBLE);
//            }
//        }



            holder.tvName.setText(this.mData.get(position).getGroups__name());
            holder.tvDescription.setText(this.mData.get(position).getGroups__description());
            Glide.with(mContext.getApplicationContext()).load(mData.get(position).getGroupimage()).into(holder.group_img);

            throw new RuntimeException("Run Time exception");
        } catch (Exception e) {
            Log.d("NullExceptionIssue", "onRowClick: " + e.getMessage());
        }


    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /**
     * itemClick
     */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }


    /**
     * Provide data to the Activity refresh
     *
     * @param list
     */
    public void updateList(List<GetGroupListDataModel> list) {
        this.mData = list;
        notifyDataSetChanged();
    }

    /**
     * Get the Char ASCII value of the first letter of
     * the classification according to the current position of the ListView
     */
    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    /**
     * Get the position of the first occurrence of the first letter based on
     * the Char ASCII value of the first letter of the classification
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTag, tvName, tvDescription;
        // ImageView edit_icon;

        RelativeLayout group_edit_icon, group_delete_icon;
        ImageView group_img;

        CardView complete_row;
        public ViewHolder(View view, final RecyclerViewClickListner listner) {
            super(view);

            tvTag = (TextView) view.findViewById(R.id.tag);
            tvName = (TextView) view.findViewById(R.id.name);
            tvDescription = (TextView) view.findViewById(R.id.tv_description);

            group_img = view.findViewById(R.id.group_img);

//            edit_icon=(ImageView)view.findViewById(R.id.edit_icon);
//            //eye_icon=(ImageView)view.findViewById(R.id.eye_icon);
//            edit_icon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (listner!=null){
//                        listner.onViewClcik(getAdapterPosition(),v);
//                    }
//                }
//            });


            group_edit_icon = (RelativeLayout) view.findViewById(R.id.group_edit_btn);
            //eye_icon=(ImageView)view.findViewById(R.id.eye_icon);
            group_edit_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });

            group_delete_icon = (RelativeLayout) view.findViewById(R.id.group_delete_btn);
            group_delete_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listner != null) {
                        listner.onViewClcik(getAdapterPosition(), v);
                    }
                }
            });


//            eye_icon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listner!=null){
//                        listner.onViewClcik(getAdapterPosition(),v);
//                    }
//                }
//            });

            complete_row=view.findViewById(R.id.complete_row);
            complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner != null) {
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });


//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (listner!=null){
//                        listner.onRowClick(v);
//                    }
//                }
//            });

        }
    }

}
