package com.khojaleadership.KLF.Contact_Layout;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.khojaleadership.KLF.Interface.RecyclerViewClickListner;
import com.khojaleadership.KLF.Model.Contact.GetContactDataListModel;
import com.khojaleadership.KLF.R;

import java.util.List;

public class ContactSortAdapter extends RecyclerView.Adapter<ContactSortAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<GetContactDataListModel> mData;
    private Context mContext;
    private RecyclerViewClickListner listner;

    public ContactSortAdapter(Context context, List<GetContactDataListModel> data, RecyclerViewClickListner listner) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
        this.listner=listner;
    }

    @Override
    public ContactSortAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.contact_screen_recyclerview_items, parent,false);
        ViewHolder viewHolder = new ViewHolder(view,listner);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ContactSortAdapter.ViewHolder holder, final int position) {

        try {
            int section = getSectionForPosition(position);

            holder.tvName.setText(mData.get(position).getUsers__title()+" "+mData.get(position).getUsers__first_name()+" "+this.mData.get(position).getUsers__last_name());

            //set other 2
            holder.title1.setText(this.mData.get(position).getUserDetails__phone());
            holder.title2.setText(this.mData.get(position).getUsers__email());


            Glide.with(mContext.getApplicationContext()).load(mData.get(position).getProfileimg()).into(holder.user_img);

            throw new RuntimeException("Run Time exception");
        }catch (Exception e){
         }


    }

    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() :0;
    }

    public Object getItem(int position) {
        return mData.get(position);
    }


    /** itemClick */
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }



    /**
     * Provide data to the Activity refresh
     * @param list
     */
    public void updateList(List<GetContactDataListModel> list){
        this.mData = list;
        notifyDataSetChanged();
    }

    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);

            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTag, tvName,title1,title2;
        LinearLayout edit_icon,delete_icon;

        ImageView user_img;
        RelativeLayout complete_row;

        public ViewHolder(View view,final RecyclerViewClickListner listner) {
            super(view);

            tvTag = (TextView) view.findViewById(R.id.tag);
            tvName = (TextView) view.findViewById(R.id.contact_name);
            title1=(TextView)view.findViewById(R.id.contact_title1);
            title2=(TextView)view.findViewById(R.id.contact_title2);

            edit_icon=(LinearLayout) view.findViewById(R.id.contact_edit_icon);
            delete_icon=(LinearLayout) view.findViewById(R.id.contact_delete_icon);

            user_img=view.findViewById(R.id.contact_img);

            edit_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });

            delete_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onViewClcik(getAdapterPosition(),v);
                    }
                }
            });



            complete_row=(RelativeLayout)view.findViewById(R.id.contact_row_layout);
            complete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        listner.onRowClick(getAdapterPosition());
                    }
                }
            });

        }
    }

}
