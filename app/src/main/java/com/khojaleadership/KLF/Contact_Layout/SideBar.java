package com.khojaleadership.KLF.Contact_Layout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 *  this is all aboute Sidebar letter A-Z sort
 */
public class SideBar extends View {
	/**Touch event*/
	private OnTouchingLetterChangedListener onTouchingLetterChangedListener;


	public static String[] b = { "A", "B", "C", "D", "E", "F", "G", "H", "I",
			"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
			"W", "X", "Y", "Z", "#" };

	/**The selected state after the finger is squatted, used to mark the click to store the subscript in the alphabet array*/


	private int choose = -1;

	private Paint paint = new Paint();

	private TextView mTextDialog;


	public SideBar(Context context) {
		super(context);
	}

	public SideBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SideBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	public void setTextView(TextView textDialog) {
		this.mTextDialog = textDialog;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int height = getHeight();
		int width = getWidth();
		int singleHeight = height / b.length;
		for (int i = 0; i < b.length; i++) {



			//Drawn font color
			//paint.setColor(Color.WHITE);
			paint.setColor(Color.rgb(123, 0, 123));
			//font size
			paint.setTextSize(18);


			//Bold font
			paint.setTypeface(Typeface.DEFAULT_BOLD);
			//Anti-aliasing
			paint.setAntiAlias(true);
			//Anti-shake, the image is softer
			paint.setDither(true);

			//Determine if the subscript of the clicked letter is equal to i,
            // then the style of drawing the clicked letter will be set for highlighting
			if (i == choose) {
				//The color of the letters
				paint.setColor(Color.parseColor("#bbb000"));
				//True is bold, false is non-bold
				paint.setFakeBoldText(true);
			}


            //The x coordinate is equal to the middle - half the width of the string
 			float xPos = width / 2 - paint.measureText(" "+b[i]) / 2;//Get the X coordinate of the starting point of the drawn letter text
			float yPos = singleHeight * i + singleHeight;//Get the Y coordinate of the starting point of the drawn letter text


            //Start to draw each letter
			canvas.drawText(b[i], xPos, yPos, paint);

			/// draw a letter needs to reset the brush object
			paint.reset();
		}

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {//Rewrite view's touch event distribution method
		final int action = event.getAction();

		//
        //Since only the Y-axis coordinates are involved,
        // only the y-coordinate is obtained.
		final float y = event.getY();

		//oldChoose is used to record the subscript
        // in the alphabet array of the last click of the letter
		final int oldChoose = choose;

		final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;


		final int c = (int) (y / getHeight() * b.length);

		switch (action) {
			case MotionEvent.ACTION_UP:
				invalidate();

				if (mTextDialog != null) {
					mTextDialog.setVisibility(View.INVISIBLE);
				}
				break;
			default:
				if (oldChoose != c) {
					if (c >= 0 && c < b.length) {
						if (listener != null) {
							listener.onTouchingLetterChanged(b[c]);
						}
						if (mTextDialog != null) {
							mTextDialog.setText(b[c]);
							mTextDialog.setVisibility(View.INVISIBLE);
						}
						choose = c;
						///// / Refresh the next frame animation, redraw the View
						invalidate();
					}
				}
				break;
		}
		return true;
	}

	public void setOnTouchingLetterChangedListener(OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
		this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
	}

	public interface OnTouchingLetterChangedListener {
		void onTouchingLetterChanged(String s);
	}

}