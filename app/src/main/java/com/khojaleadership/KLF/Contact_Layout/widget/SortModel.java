package com.khojaleadership.KLF.Contact_Layout.widget;

public class SortModel {

    private String name;
    private String letters;//Display the initials of the pinyin

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

}
