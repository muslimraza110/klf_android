package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Group_Models.GetGroupListDataModel;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparator implements Comparator<GetGroupListDataModel> {

	@Override
	public int compare(GetGroupListDataModel o1, GetGroupListDataModel o2) {
		if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
			return -1;
		} else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
			return 1;
		} else {
			return o1.getLetters().compareTo(o2.getLetters());
		}
	}

}
