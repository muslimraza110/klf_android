package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Project_Models.GetAlreadyGroup_ProjectDataModel;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparatorForEditProjectGroupSection implements Comparator<GetAlreadyGroup_ProjectDataModel> {
    @Override
    public int compare(GetAlreadyGroup_ProjectDataModel o1, GetAlreadyGroup_ProjectDataModel o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        } else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }

    }

}
