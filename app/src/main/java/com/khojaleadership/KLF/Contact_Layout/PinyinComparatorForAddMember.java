package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Group_Models.GetGroupMemberListDataModel;

import java.util.Comparator;

/**
 * Comparator class, mainly based on ASCII code to compare and sort data
 */
public class PinyinComparatorForAddMember implements Comparator<GetGroupMemberListDataModel> {
    @Override
    public int compare(GetGroupMemberListDataModel o1, GetGroupMemberListDataModel o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        } else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }

    }

}
