package com.khojaleadership.KLF.Contact_Layout;

import com.khojaleadership.KLF.Model.Contact.GetContactDataListModel;

import java.util.Comparator;


public class ContactPinyinComparator implements Comparator<GetContactDataListModel> {

	@Override
	public int compare(GetContactDataListModel o1, GetContactDataListModel o2) {
		if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
			return -1;
		} else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
			return 1;
		} else {
			return o1.getLetters().compareTo(o2.getLetters());
		}
	}

}
